
./simple

There are two models:

simple-wireless.tcl
   This is two wireless nodes with a single FTP application.



wired-cum-wireless-sim.tcl
   This is 2 BSs, 3 wireless nodes, and 2 wired nodes.



./example-wifi

wifiTCPUDP.tcl
   This is a WiFI single AP model.  

Input files:

These you will likely change....
goruns.dat
numberCMs.tcl
traffic.tcl

And the following you might not need to change....
TCLdefines.tcl
fec.tcl
loss.tcl



output files:

Correlated Loss Monitor
DSCorrLossMon.out
  fprintf(out_file, "%d %d %d %d %d %10.8f %10.8f %10.8f %10.8f\n", numberLost+numberReceived, numberReceived, numberLost, numberDups, numberOutOfOrder, avgLatency, totalLossRate, MBLmetric, MILDmetric);


UDPstats.out
  Script routine: 
    $ns at $printtime "dumpFinalUDPStats  label starttime udpsink outputfile"
  Output format:
  puts $f "$label $bytesRxed $PktsRxed $PktsDropped $PktsOutOfOrder $dropRate $thruput $AvgJitter $AvgLatency"


Macstats.out:

Refer to mac-802_11.cc, method dump80211MacStats() for details. But in
summary:
mac_id:  identifies the node.This is passed via tcl.  So the mac_id is NOT the
mac address.  It is the CxID / flow ID.  We identify the AP as mac_id 100.  
time: time of stats capture
TxBW:  based on total number of bytes transmitted by the station
RxBW:  based on the total number of bytes received by the station
BytesRxed/PacketsRxed:  
Pct Data/Mgmt/Cnt Bytes:  These three fields represent the percentage of all
data that was received by the station that is data, management, or control.
Examples of control:  ACKs and RTS/CTS.  
BytesSent/PacketsSent :
Collisions:  These three fields show number of collisions.  We find this in
one of three ways. The first is the total number.  The second is collisions
while in rx_state MAC_RECV.  The third is collisions when rx_state is
MAC_COLL. This attempts to handle when the station senses a collision 
after a collision just occurred.

//15 fields
    fprintf(fp,"%s\t\t%6.1f %10.0f %10.0f %16.0f\t%16.0f\t%3.2f\t%3.2f\t%3.2f\t%16.0f\t%16.0f\t%3.2f\t%3.2f\t%3.2f\t%3.2f\n",
       mac_id,now,TxBWConsumed,RxBWConsumed,MacStats.numberBytesReceived,MacStats.numberPacketsReceived,
       PercentageData, PercentageMgmt, PercentageCnt, MacStats.numberBytesSent,MacStats.numberPacketsSent, collisionRate, Rxutilization, ActualRxutilization,Txutilization);

3mac_id    time      TxBW(bps) RxBW          BytesRxed       PacketsRxed Pct Data/Mgmt/Cnt Bytes         BytesSent           PacketSent ....COLLISIONS... RxUtil ActualRxU  TxUtil
100		 300.5    5840624    3134670        117746051	          129300	2.14	0.00	0.89	       219388449	          177806	0.00	0.65	0.13	36.34
0		 300.5     237694    1817594         68273393	           78054	49.29	0.00	0.12	         8928393	           44797	0.00	1.81	1.78	0.65
1		 300.5          0    1990263         74759241	           49970	50.82	0.00	0.00	               0	               0	0.00	19.67	16.79	0.00
2		 300.5          0    1970590         74020289	           49474	50.82	0.00	0.00	               0	               0	0.00	19.63	16.62	0.00



APFECSINKstats.out
~                                                                                                                  
 fprintf(fp,"%lf\t%d\t%d\t%9.0f\t%9.0f\t%2.3f\t%2.3f\t%2.3f\t%2.3f\t%2.3f\t%d\t%d\t%d\t%d\t%d\t%12.0f\t%12.0f\t%d\t%d\t%d\t%2.3f\t%d\t%d\t%d\n",
    curr_time,streamID,numberBlockTimeouts, BW, APPThroughput, Efficiency1,Efficiency2,rawLossRate, finalLossRate1, finalLossRate2,
    numberInOrderPacketArrivals, numberOutOfOrderPacketArrivals, numberBlockQDrops, numberOutOfOrderQDrops,
    totalPacketsRxed,totalDataBytesRxed,totalRedundantDataBytesRxed, totalFECDrops,totalFECRestores, numberBlockTimeouts,Efficiency,typeFEC,totalPackets, totalPacketsRxed);

    curr_time,streamID,numberBlockTimeouts, BW, APPThroughput, Efficiency1,Efficiency2,rawLossRate, finalLossRate1, finalLossRate2,
       200.000000  1   0      554718      421848   0.766   0.760   0.246   0.272   0.265   
    numberInOrderPacketArrivals, numberOutOfOrderPacketArrivals, numberBlockQDrops, numberOutOfOrderQDrops,
       6050    3371    0   0   
    totalPacketsRxed,totalDataBytesRxed,totalRedundantDataBytesRxed, totalFECDrops,totalFECRestores, numberBlockTimeouts,Efficiency,typeFEC,totalPackets, totalPacketsRxed);
       9421        10546200         2869760    0   22  0   0.000   1   7277    9421 



APFECSOURCEstats.out

  fprintf(fp,"%lf\t%d\t%d\t%9.0f\t%9.0f\t%2.2f\t%d\t%d \n",
    curr_time,streamID,numberBlockTimeouts, BW, APPThroughput, Efficiency,totalPacketsSent,totalDataPacketsSent);

200.000000	1	0	  1009135	   768040	0.76	17139	13715 
200.000000	2	0	        0	        0	0.00	0	0 
200.000000	3	0	        0	        0	0.00	0	0 
200.000000	4	0	        0	        0	0.00	0	0 
200.000000	5	0	        0	        0	0.00	0	0 


//traces UDP stream AFTER fec              
if (streamID == 1) {
   fp = fopen("APFECRECV1.out", "a+");
  fprintf(fp,"%f %d %d %d %d %d %d %d %d %f %f\n", 
     now,  streamID, rh->contentType, rh->seqno(), rh->rawSeqNo_, APFEChighestSeqNo, 
      (h->size()-APPFEC_PAC KET_OVERHEAD),totalPackets, lossCount,jitter, delay);
}

/*This is the raw arrivals to the sink - so its before APPFEC is applied to correct for errors */
VideoTrace1Arrived.out - from appfec-sink.cc
MonitorVideoTrace...
    fp = fopen(mptr, "a+");
     fprintf(fp,"%f %d %d %d %d %d %d %d %d %d %d %d %d %d %d %f\n", now, streamID, rh->contentType, rh->seqno(),rh->rawSeqNo_,APFEChighestRawSeqNo,h->size(),totalPacketsRxed, lossCount, rh->blockno_,rh->blockIndex_,rh->N_,rh->K_,numberPacketsInBlock,numberBytesInBlock,delay);





# 1: Freespace; 2:Two Ray Ground Reflection; 3:Shadowing
set PROP_MODEL 3;

#Tx power of the radio (.02 is 54Mbps, .100 is 
set POWER_LEVEL 0.100

#This detects signal with a sensitivity of -108dBm
set CS_POWER_THRESH 1.559e-11

#This sets the Rx sensitivity- when a frame arrives, if the Pw associated with the signal 
#exceeds this threshold, the radio will correctly Rx the frame.  Else, it drops the frame.
#This is set to -94dBm.  To model 54Mbps, is should be -71 dBm
set RX_POWER_THRESH 3.652e-10

#For the shadowing fading/prop model, this is a 'good' setting.  Bad goes up to  5
# This is the rate at which signal deteriorates with distance
set PATHLOSS_EXPONENT 2.4

#For the shadowing fading/prop model, this is a 'good' setting.  Bad goes up to 12
#This models fading....increasing this param increases the random variation
 set SHADOWING_DEVIATION 4.0






#802.11g default parameters

#Collision Threshold
Phy/WirelessPhy set CPThresh_                10.0   

#Carrier sense power threshold
#Phy/WirelessPhy set CSThresh_                6.31e-12    ;#-82 dBm Wireless interface sensitivity (sensitivity defined in the standard)
#Should be identical to the rx noise floor
Phy/WirelessPhy set CSThresh_                6.31e-12    ;#-112 dBm

#Rx power threshold
#should be a little less than the rx sensitivity and a larger than the CSThresh_
#  Note: rx sensitivity range
#       –94 dBm @ 1 Mbps
#       –71 dBm @ 54 Mbps
#Phy/WirelessPhy set RXThresh_                6.31e-12    ;#-82 dBm Wireless interface sensitivity (sensitivity defined in the standard)
Phy/WirelessPhy set RXThresh_                6.31e-12    ;#-112dBm 

#Units for Pt_ are watts
Phy/WirelessPhy set Pt_                      0.001      # 1 mW or  0 dBm
#Phy/WirelessPhy set Pt_                     0.100      # 100 mW or 20 dBm

Phy/WirelessPhy set freq_                    5.18e+9

Phy/WirelessPhy set noise_floor_             2.512e-13   ;#-96 dBm for 10MHz bandwidth
Phy/WirelessPhy set L_                       1.0         ;#default radio circuit gain/loss
Phy/WirelessPhy set PowerMonitorThresh_      1.259e-13   ;#-99dBm power monitor  sensitivity
Phy/WirelessPhy set HeaderDuration_          0.000020    ;#20 us
Phy/WirelessPhy set BasicModulationScheme_   0
Phy/WirelessPhy set PreambleCaptureSwitch_   1
Phy/WirelessPhy set DataCaptureSwitch_       0
Phy/WirelessPhy set SINR_PreambleCapture_    2.5118;     ;# 4 dB
Phy/WirelessPhy set SINR_DataCapture_        100.0;      ;# 10 dB
Phy/WirelessPhy set trace_dist_              1e6         ;# PHY trace until distance of 1 Mio. km ("infinty")
Phy/WirelessPhy set PHY_DBG_                 0

#Rx sensitivity for Ciscos cardbus adapter
# –94 dBm @ 1 Mbps
# –93 dBm @ 2 Mbps
# –92 dBm @ 5.5 Mbps
# –86 dBm @ 6 Mbps
# –86 dBm @ 9 Mbps
# –90 dBm @ 11 Mbps
# –86 dBm @ 12 Mbps
# –86 dBm @ 18 Mbps
# –84 dBm @ 24 Mbps
# –80 dBm @ 36 Mbps
# –75 dBm @ 48 Mbps
# –71 dBm @ 54 Mbps

#Cisco card spec
#20 dBm (100 mW) @ 1, 2, 5.5, and 11 Mbps
#18 dBm (63 mW) @ 1, 2, 5.5, 6, 9, 11, 12, 18, and 24 Mbps
#17 dBm (50 mW) @ 1, 2, 5.5, 6, 9, 11, 12, 18, 24, and 36 Mbps
#15 dBm (30 mW) @ 1, 2, 5.5, 6, 9, 11, 12, 18, 24, 36, and 48 Mbps
#13 dBm (20 mW) @ 1, 2, 5.5, 6, 9, 11, 12, 18, 24, 36, 48, and 54 Mbps
#10 dBm (10 mW) @ 1, 2, 5.5, 6, 9, 11, 12, 18, 24, 36, 48, and 54 Mbps

Mac/802_11 set CWMin_                        15
Mac/802_11 set CWMax_                        1023
Mac/802_11 set SlotTime_                     0.000009
Mac/802_11 set SIFS_                         0.000016
Mac/802_11 set ShortRetryLimit_              7
Mac/802_11 set LongRetryLimit_               4
Mac/802_11 set HeaderDuration_               0.000020
Mac/802_11 set SymbolDuration_               0.000004
Mac/802_11 set BasicModulationScheme_        0
Mac/802_11 set use_802_11a_flag_             true
Mac/802_11 set RTSThreshold_                 2346
Mac/802_11 set MAC_DBG                       0



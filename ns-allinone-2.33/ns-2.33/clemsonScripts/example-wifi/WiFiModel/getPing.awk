#Change the number in the third paran to indicate the column in the data file to operate upon.
#If the columns are separated with tabs, change the first paran to "\t"
BEGIN {
 FS = " "
 totalCount = 0;
 sampleSum = 0;
 sampleCount=0;
 lossCount =0;
 LR = 0.0;
 RTT = 0.0;
 highestSNUM=0;
 lossAdjustment = 0;
 lastSNUM=0;
} 
{
  nl++;
  totalCount++;
  s=s+$2;
  if ($3 > highestSNUM) {
    highestSNUM=$3;
  }
  if ($2 > 0) {
    sampleCount++;
    sampleSum = sampleSum + $2;
  }
  else {
    lossCount++;
  }
  if ($3 <= lastSNUM) {
    lastSNUM = $3;
    printf("ping:  OUT OF ORDER ?? lastSNUM:%d, currentSNUM:%d \n",lastSNUM,$3);
  }
}
END {
#This is the old way...and is wrong as it includes RTT=0 samples
#print "average ping RTT (ms):" s/nl
  lossAdjustment = highestSNUM - totalCount;
  if (lossAdjustment > 0) {
    lossCount = lossCount + lossAdjustment;
  }
  RTT = sampleSum / sampleCount;
  LR = lossCount / totalCount;
  printf("ping: #Iterations:%d, #Samples:%d, #losses:%d:  RTT:%3.3f (ms),  LossRate:%3.3f (lossAdjustment:%d)  \n",totalCount,sampleCount,lossCount, RTT, LR, lossAdjustment);
}

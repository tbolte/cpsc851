#
#  The output is as follows:
#     timestamp 	max_qnp_ 	min_qnp_ 	qnp_  	util
#	0.000000  	0  		0 		0  	0.000
#	1.000000  	0  		0 		0  	0.712
#	2.000000  	1  		0 		1  	0.679
#	3.000000  	1  		1 		1	0.665
#
#   max_qnp_:  During the current interval, this is the maximum queue size in packets
#   min_qnp_:  During the current interval, this is the minimum queue size in packets
#   qnp_    :  Current number of packets in the queue
#   util:      During the current interval, we compute the utilization of the channel.  We
#              divide the total number of bytes sent by the total number of
#		bytes that could possibly be sent (based on the channel capacity).  
#
#
####################################################################################
proc Trace80211eQueue {mac ns WLSNode interval fname } {
#	      puts "Trace80211eQueue: open using file $fname "
	proc internalTrace80211eQueue { mac ns WLSNode interval fname } {
#         $mac dump-80211e-queue-stats $mac $WLSNode $fname 
          $mac dump-80211e-queue-stats $fname
		  set now [expr [$ns now]]
		  set next_interval [expr ($now + $interval)]
#		  puts "Trace80211eQueue: Inovking queue stats at $now for $mac"
          $ns at $next_interval "internalTrace80211eQueue $mac $ns $WLSNode $interval $fname "
	}
	$ns at 0.0 "internalTrace80211eQueue $mac $ns $WLSNode $interval $fname"
}


proc Dump80211MacStats {mac ns logtime mac_id fname} {
	proc internalDump80211MacStats { mac ns mac_id fname } {
          $mac dump-80211-mac-stats $fname $mac_id
#		  set now [expr [$ns now]]
#		  set next_interval [expr ($now + $interval)]
#         $ns at $next_interval "internalTrace80211MacStats $mac $ns $WLSNode $interval $fname "
	}
	$ns at $logtime "internalDump80211MacStats $mac $ns $mac_id $fname"
}


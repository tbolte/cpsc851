### 
# wifiTCPUDP.tcl : The number of wireless nodes is based on numberCMs.tcl. 
#                  From the file goruns.dat, we get X_MAX and Y_MAX
#
#            W(0) 0.0.0
#             -
#             - 100Mbps, 2ms
#             -
#           W(1) 0.1.0
#             -
#             - 100Mbps, 2ms
#             -
#           BS(0) 
#             -
#             -  dataRate_
#             -
#     Node_(0)    Node_(1)    Node_(2) ..... (numberCMs)
#      monitor     node 1                        node numberCMs
#
#     So if numberCMs is 5,  we actually create 6 mobile nodes (not including the BS)
#
#   Parameters:
#      set numberCMs_P [lindex $argv 0]
#      set PATHLOSS_P [lindex $argv 1]
#      set CBR_PACKETSIZEP [lindex $argv 2]
#      set CBR_RATE_P [lindex $argv 3]
#      set N_P [lindex $argv 4]
#      set K_P [lindex $argv 5]
#      set LOSS_RATE1_P [lindex $argv 6]
#
#      If there are 0 params, it gets config data from:
#          numberCMs.tcl, traffic.tcl, loss.tcl,  fec.tcl and goruns.dat
# 
#
#  For all cases the 'client' side is determined by the tcl variable
#     TRAFFIC_DIRECTION:  0 means upstream, 1 means downstream
#
#  To turn on the different flows at node n1
#      set doPINGMONITOR 1
#
#  Notes:
# -If numberCMs.tcl is 1, then 1 BS and 2 NNs (wireless nodes) are created
#     But we will only create UDP flows starting with the second wireless node
###########################################################

source TCLdefines.tcl
source numberCMs.tcl
source goruns.dat
source loss.tcl
source fec.tcl
source traffic.tcl
source networks.tcl
source cleanup.tcl
source ping-helper.tcl
source qm.tcl
#source IEEE802-11a.tcl


set WINDOW 600

#set QUEUE_CAPACITY 512
set QUEUE_CAPACITY 512

cleanup

puts "Start a run this many args $argc "

set numberCMs_P $numberCMs;
set PATHLOSS_P -1.0;
set CBR_PACKETSIZEP 0;
set CBR_RATE_P 0;
set N_PARAM_P 10;
set K_PARAM_P 8;
set LOSS_RATE1_P 0.0;


if {$argc == 0} {
#Get all params from sourced files
  set PATHLOSS_P $PATHLOSS;
  set CBR_PACKETSIZEP $CBR_PACKETSIZE;
  set CBR_RATE_P $CBR_RATE;
  set N_PARAM_P $N_PARAM;
  set K_PARAM_P $K_PARAM;
  set LOSS_RATE1_P $LOSS_RATE1;
}
if {$argc == 1} {
  set numberCMs_P [lindex $argv 0]
}

if {$argc == 7} {
  set numberCMs_P [lindex $argv 0]
  set PATHLOSS_P [lindex $argv 1]
  set CBR_PACKETSIZEP [lindex $argv 2]
  set CBR_RATE_P [lindex $argv 3]
  set N_PARAM_P [lindex $argv 4]
  set K_PARAM_P [lindex $argv 5]
  set LOSS_RATE1_P [lindex $argv 6]
}

set numberCMs $numberCMs_P;
incr numberCMs;

#Set to 1 to turn on the ping
set doPINGMONITOR 1


#Set to 1 to turn on the US and DS VOIP loss monitor
set doVOIPLOSSMONITOR 0
set doVOIPLOSSMONITORDS 1
set doVOIPLOSSMONITORUS 0

#Set to 1 to turn on the US and DS CORRloss monitor
set doCORRLOSSMONITOR 1
set doCORRLOSSMONITORDS 1
set doCORRLOSSMONITORUS 0


#Also see the file traffic.tcl

#if 1, all UDP.  If 0, all TCP
set ALL_UDP 1

#if 1, all UDP.  If 0, all TCP
#set UNICAST_UDP 0
set UNICAST_UDP 1

#Values enumerated in TCLdefines.tcl
set UDP_TRAFFIC $CBR_UDP_TRAFFIC
#set UDP_TRAFFIC $APFEC_UDP_TRAFFIC

#If 1, all application flow is DS (else 0 upstream)
set TRAFFIC_DIRECTION 1


#If 1, it is a multicast flow
if {$numberMulticastFlows > 0 } {
  set MULTICAST 1
} else {
  set MULTICAST 0
}

puts "Start a run with N: $N_PARAM_P, K: $K_PARAM_P, LOSS_RATE1: $LOSS_RATE1_P "

if {$ALL_UDP == 1} {
  puts "Start a run with $numberCMs CMs, all UDP traffic (#unicast:$numberUnicastFlows, #multicast:$numberMulticastFlows)  and direction $TRAFFIC_DIRECTION "
} elseif {$ALL_UDP == 0} {
  puts "Start a run with $numberCMs CMs, all TCP traffic (#unicast:$numberUnicastFlows, #multicast:$numberMulticastFlows)  and direction $TRAFFIC_DIRECTION "
} else {
    puts "ERROR:  ALL_UDP not set correctly: $ALL_UDP "
    exit 0
}


global opt
#set opt(nodelist)   NodeList
set opt(chan)       Channel/WirelessChannel
#set opt(prop)       Propagation/TwoRayGround

set opt(netif)      Phy/WirelessPhy
#set opt(netif)      Phy/WirelessPhyExt

set opt(mac)        Mac/802_11
#set opt(mac)        Mac/802_11Ext

set opt(ifq)        Queue/DropTail/PriQueue
set opt(ll)         LL
set opt(ant)        Antenna/OmniAntenna

set opt(x)          [expr $MAX_X * 2]
set opt(y)          [expr $MAX_Y * 2]
set opt(ifqlen)     $QUEUE_CAPACITY  
#set opt(ifqlen)     50   
set opt(tr)         wifiTRACE.tr
set opt(namtr)      wifiTRACE.nam
#set opt(tr)         wired-and-wireless.tr
#set opt(namtr)      wired-and-wireless.nam
set opt(nn)         $numberCMs                      
set opt(adhocRouting)   NOAH                     
#set opt(adhocRouting)   DSDV                      
set opt(cp)             ""                        
set opt(sc)             "../mobility/scene/scen-3-test"   
#set opt(stop)            20                          
set num_wired_nodes      2
set num_bs_nodes         1

#Uncomment to disable RTS-CTS
Mac/802_11 set RTSThreshold_ 3000

Mac/802_11 set CWMin_         31
Mac/802_11 set CWMax_         1023
#Mac/802_11 set CWMin_         1
#Mac/802_11 set CWMax_         16
#Mac/802_11 set CWMin_         16
#Mac/802_11 set CWMax_         1024
# Mac/802_11 set SlotTime_      0.000020        ;# 20us for 802.11b operation only
 Mac/802_11 set SlotTime_      0.000009        ;# 9us for 802.11g only
# Mac/802_11 set SIFS_          0.000010        ;# 10us
 Mac/802_11 set SIFS_          0.000016        ;# 16us
 Mac/802_11 set PreambleLength_        144             ;# 144 bit
 Mac/802_11 set PLCPHeaderLength_      48              ;# 48 bits
# Mac/802_11 set PLCPDataRate_  1.0e6           ;# 1Mbps
 Mac/802_11 set PLCPDataRate_  10.0e6           ;# 10 Mbps
 Mac/802_11 set ShortRetryLimit_       7               ;# retransmittions
 Mac/802_11 set LongRetryLimit_        4               ;# retransmissions


#The propagation model type (PROP_MODEL) can be any of the following
#1 FREESPACE :  assumes ideal, freespace propagation
#2 TWORAY :  Two-Ray Ground Reflection
#3 #SHADOW :   Shadowing model

set PROP_MODEL 3;

#The PHY_TYPE defines the physical layer for the simulation
# 1 is WIFI  (802.11b)
# 2 is WIFI  (802.11g)
set PHY_TYPE  2;  

if { $PHY_TYPE == 1} {
  Mac/802_11 set dataRate_ 11Mb
  Mac/802_11 set PreambleLength_ 72

# Initialize the SharedMedia interface with parameters to make
# it work like the 914MHz Lucent WaveLAN DSSS radio interface
Phy/WirelessPhy set CPThresh_ 10.0
Phy/WirelessPhy set CSThresh_ 1.559e-11
Phy/WirelessPhy set RXThresh_ 3.652e-10
Phy/WirelessPhy set bandwidth_ 2e6
Phy/WirelessPhy set Pt_ 0.2818 

#Phy/WirelessPhy set freq_ 914e+6
#Pick channel 13 :
Phy/WirelessPhy set freq_ 2.472e9
Phy/WirelessPhy set L_ 1.0

} elseif {$PHY_TYPE == 2} {

#Inlcude IEEE802-11g.tcl data file
#Initially assumes ideal propagation, 54Mbps operation

#  Mac/802_11 set basicRate_ 24Mb     ;#for 802.11g only operation
  Mac/802_11 set basicRate_ 6Mb     ;#more realistic for 802.11g operation
#  Mac/802_11 set basicRate_ 2Mb    ;#for 802.11bg operation

  Mac/802_11 set dataRate_ 54Mb;  #update to 600mb
  Mac/802_11 set PreambleLength_ 72

  #Collision Threshold
  Phy/WirelessPhy set CPThresh_ 10.0
  #Carrier Sense Power
  Phy/WirelessPhy set CSThresh_ 1.559e-11    ;#-108 dBm
#  Phy/WirelessPhy set CSThresh_ 6.31e-12    ;#-112 dBm

  #Receive Power threshold 
#  Phy/WirelessPhy set RXThresh_ 7.94e-8    ;#-71 dBm -  54Mbps

# Rx sensitivity -94 dBm@1Mbps, -71 dBm@54Mbps
  Phy/WirelessPhy set RXThresh_ 3.652e-10    ;#-94 dBm

#  Phy/WirelessPhy set RXThresh_ 7.94e-10    ;#-91 dBm
#  Phy/WirelessPhy set RXThresh_ 6.31e-12    ;#-112 dBm

  Phy/WirelessPhy set bandwidth_ 10e6

  #Transmit Power level:  Cisco   Cardbus and 1200AP advertise tx power level 
  #for 54 Mbps OFDM of 13 dBm (20mW)
  #Note the range will be 100 feet or less.  If drop down to a more robust modulation (2 Mbps) range is > 400 feet

#Units are dB 
#  Phy/WirelessPhy set Pt_ 0.130   ;# 130 mwatts or  21 dBm

  Phy/WirelessPhy set Pt_ 0.100   ;# 100 mwatts or 20 dBm 
#  Phy/WirelessPhy set Pt_ 0.020   ;#  20 mwatts or 13 dBm  (for 54Mbps OFDM)

#To effectively cause ideal channes...
#  Phy/WirelessPhy set Pt_ 10  ;#10,000 mwatts or 40 dBm

  #Pick channel 13 :
  Phy/WirelessPhy set freq_ 2.472e9

  #System loss factor (don't know what this is yet)
  Phy/WirelessPhy set L_ 1.0
}

# Pt_ is transmitted signal power. The propagation model and Pt_ determines
Antenna/OmniAntenna set Gt_ 1.0
if {$PROP_MODEL == 1} {
  set prop  [new Propagation/FreeSpace]
} elseif { $PROP_MODEL == 2} {
  set prop  [new Propagation/TwoRayGround]
} elseif { $PROP_MODEL == 3} {
  set prop  [new Propagation/Shadowing]
#  $prop set pathlossExp_ 2.0
#  $prop set std_db_ 4.0
#  $prop set dist0_ 1.0
#  $prop seed predef 0
  if {$PATHLOSS_P == -1.0} {
    puts "PROP model : set pathloss exp 2.5"
    $prop set pathlossExp_ 2.4
  } else {
    puts "PROP model : set pathloss exp $PATHLOSS_P"
    $prop set pathlossExp_ $PATHLOSS_P;
  }
  $prop set std_db_ 4.0
  $prop set dist0_ 1.0
  $prop seed predef 0
}



#set stoptime $opt(stop)
set tcpprinttime [expr $stoptime - .3]
set printtime [expr $stoptime - .5]
puts "stoptime : $stoptime and printtime : $printtime"


set ns_   [new Simulator]
if {$FLAT_ADDRESSING == 0} {
  # set up for hierarchical routing
  $ns_ node-config -addressType hierarchical
  #2 domains, 
  AddrParams set domain_num_  2         
  #number of clusters in each domain: 2, 1 
  lappend cluster_num 2 1                 
  AddrParams set cluster_num_ $cluster_num
  #number of nodes in each cluster: 1, 1, 4
  lappend eilastlevel 1 1  [expr $numberCMs + 1];              
  AddrParams set nodes_num_ $eilastlevel 
}
#
#  set tracefd  [open $opt(tr) w]
# uncomment to NOT trace
# can also set all Trace to NO 
  set tracefd  [open "/dev/null" w]

  $ns_ trace-all $tracefd
  $ns_ use-newtrace


  set topo   [new Topography]
  $topo load_flatgrid $opt(x) $opt(y)
  # god needs to know the number of all wireless interfaces
  create-god [expr $opt(nn) + $num_bs_nodes]

  #create wired nodes
  set temp {0.0.0 0.1.0}        
  for {set i 0} {$i < $num_wired_nodes} {incr i} {
      if {$FLAT_ADDRESSING == 0} {
        puts "Create wired node $i using HIERARCHICAL ADDRESSING"
        set W($i) [$ns_ node [lindex $temp $i]]
      } else {
        puts "Create wired node $i using FLAT_ADDRESSING"
        set W($i) [$ns_ node]
      }
  } 
  $ns_ node-config -adhocRouting $opt(adhocRouting) \
                 -llType $opt(ll) \
                 -macType $opt(mac) \
                 -ifqType $opt(ifq) \
                 -ifqLen $opt(ifqlen) \
                 -antType $opt(ant) \
                 -propInstance $prop \
                 -phyType $opt(netif) \
                 -channel [new $opt(chan)] \
                 -topoInstance $topo \
                 -wiredRouting ON \
                 -agentTrace OFF \
                 -routerTrace OFF \
                 -macTrace OFF 

   #making a list of HIERARCHICAL addresses in the wireless domain.. they range from 1.0.0 to 1.0.(numberCMs+2-1)
   if {$FLAT_ADDRESSING == 0} {
     for {set i 0} {$i < [expr $numberCMs+1] } {incr i} {
       set temp1($i) "1.0.[expr $i]"
       puts "$temp1($i)"
        lappend temp2 $temp1($i)
     }
     puts "wireless nodes: $temp2"

   }

   if {$FLAT_ADDRESSING == 0} {
      set BS(0) [$ns_ node [lindex $temp2 0]]
   } else {
      set BS(0) [$ns_ node]
   }
   puts "Create BS node,  number nn's: $opt(nn)"


   $BS(0) random-motion 0 
#Set the BS position
   $BS(0) set X_ 1.0
   $BS(0) set Y_ 1.0
   $BS(0) set Z_ 0.0
   $BS(0) type $baseStation

   puts "BS node X,Y:  [$BS(0) set X_], [$BS(0) set Y_]"

   set BSmac_ [$BS(0) getMac 0]
   Dump80211MacStats $BSmac_ $ns_ $stoptime 100 Macstats.out
  
  #configure for mobilenodes
  $ns_ node-config -ifqLen $QUEUE_CAPACITY

#So node_(0) will be the monitor station (flat address 3, mac address 1).
  if {$FLAT_ADDRESSING == 0} {
    for {set j 0} {$j < $opt(nn)} {incr j} {
      set node_($j) [ $ns_ node [lindex $temp2 [expr $j+1]] ]
      $node_($j) base-station [AddrParams addr2id [$BS(0) node-addr]]
      puts "Assign node $j (HIERARCHICAL ADDRESS:[AddrParams addr2id [$node_($j) node-addr]]) to base station of address [AddrParams addr2id [$BS(0) node-addr]]"
      set mac_($j) [$node_($j) getMac 0]
      Dump80211MacStats $mac_($j) $ns_ $stoptime $j Macstats.out
      $node_($j) type $mobileNode
    }
  } else {
    for {set j 0} {$j < $opt(nn)} {incr j} {
       set node_($j) [$ns_ node]
       $node_($j) base-station [$BS(0) node-addr]
       puts "Assign node $j (FLAT ADDRESS:[$node_($j) node-addr]) to base station of address [$BS(0) node-addr]"
       set mac_($j) [$node_($j) getMac 0]
       Dump80211MacStats $mac_($j) $ns_ $stoptime $j Macstats.out
       $node_($j) type $mobileNode
    }
  }
  #create links between wired and BS nodes
  $ns_ duplex-link $W(0) $W(1) 100Mb 2ms DropTail
  $ns_ duplex-link $W(1) $BS(0) 100Mb 2ms DropTail

  $ns_ queue-limit $W(1) $W(0) $QUEUE_CAPACITY
  $ns_ queue-limit $W(0) $W(1) $QUEUE_CAPACITY
  $ns_ queue-limit $W(1) $BS(0) $QUEUE_CAPACITY
  $ns_ queue-limit $BS(0) $W(1)  $QUEUE_CAPACITY

if {$ERROR_MODEL == 0 } {
  set loss_modulexnet [new ErrorModel]
  $loss_modulexnet set errModelId 2
  # .03 is 3%
  $loss_modulexnet set rate_ $LOSS_RATE1_P
 #$loss_modulexnet set rate_ 0.03
  #$loss_modulexnet set rate_ 0.0
  #$loss_modulexnet set rate_ 0.01
  #$loss_modulexnet set rate_ 0.02
  #$loss_modulexnet set rate_ 0.2

  puts "Using Bernoulli Loss ErrorModel, loss rate: $LOSS_RATE1_P"

  $loss_modulexnet unit pkt
  $loss_modulexnet ranvar [new RandomVariable/Uniform]
  $loss_modulexnet drop-target [new Agent/Null]
} elseif { $ERROR_MODEL == 1} {

  puts "Using TwoState ErrorModel,, avg LR: $LOSS_RATE1_P, goodAvg: $goodLossAvgDuration,  badAvg: $badLossAvgDuration"

  if { $LOSS_RATE1_P > 0} {
    
    set newrng_ [new RNG]
    $newrng_ seed $MYSEED

    #ratio of 100:25 for run length to achieve .2 loss rate
    #set goodLossAvgDuration 100.0
    #set badLossAvgDuration 25.0
    #ratio of 4:1 for time based mode to achieve .2 loss rate
    #set goodLossAvgDuration 4.0
    #set badLossAvgDuration 1.0
    set rv0 [new RandomVariable/Exponential]
    set rv1 [new RandomVariable/Exponential]
    $rv0 use-rng $newrng_
    $rv1 use-rng $newrng_
    #  $rv0 set avg_ $goodAvgTime
    #  $rv1 set avg_ $badAvgTime
    $rv0 set avg_ $goodLossAvgDuration
    $rv1 set avg_ $badLossAvgDuration

    #  set em [new ErrorModel/MyTwoState $myrates]
    #  set loss_modulexnet [new ErrorModel/TwoState $rv0 $rv1 time]
    #if not time
    set loss_modulexnet [new ErrorModel/TwoState $rv0 $rv1]
  } else {
    set loss_modulexnet [new ErrorModel]
    $loss_modulexnet set errModelId 2
    $loss_modulexnet set rate_  0.0
  }
}

$ns_ lossmodel $loss_modulexnet $W(0) $W(1)


#Set the node positions
if {$numberCMs > 1} {
  set xstep [expr [expr $MAX_X - $MIN_X]/[expr $numberCMs -1.0]]
  set ystep [expr [expr $MAX_Y - $MIN_Y]/[expr $numberCMs-1.0]]
} else {
  set xstep 5.0;
  set ystep 5.0;
}
#To set on a horizontal line
#  set ystep 0;


  puts "Set MN positions using xstep:$xstep, ystep:$ystep"
  for {set j 0} {$j < $numberCMs} {incr j} {
   set  node_pos_x($j) [expr $MIN_X + [expr $xstep * $j]]
   set  node_pos_y($j) [expr $MIN_Y + [expr $ystep * $j]]
    $node_($j) set X_ [expr $node_pos_x($j)]
    $node_($j) set Y_ [expr $node_pos_y($j)]
    $node_($j) set Z_ 0
    puts "Node #$j X:$node_pos_x($j) Y:$node_pos_y($j)"
    $node_($j) random-motion 0
  }

# MOBILITY
#Have node 0 move
# 500 causes some loss....
  for {set j 1} {$j < $numberCMs} {incr j} {
     set mobx [expr 20.0+[expr $j*10]]
     set moby [expr 20.0+[expr $j*10]]
     $ns_ at 10 "$node_($j) setdest  $mobx $moby 2.0"
  }
     $ns_ at 10 "$node_(0) setdest  100.0 100.0 10.0"

#  for {set i 0} {$i < $opt(nn)} {incr i} {
#      $ns_ initial_node_pos $node_($i) 20
#   }

  set f [open distances w]
  #By default we put the BS at (0,0,0)
  puts $f  "BS node X,Y:  [$BS(0) set X_], [$BS(0) set Y_], 0"
  for {set j 0} {$j < $numberCMs} {incr j} {
     puts $f "Mobile Node: $j $node_pos_x($j) $node_pos_y($j)"
  } 
  close $f;

  for {set i 0} {$i < $opt(nn) } {incr i} {
      $ns_ at $stoptime.0000010 "$node_($i) reset";
  }
  $ns_ at $stoptime.0000010 "$BS(0) reset";

  $ns_ at $stoptime.1 "puts \"NS EXITING...\" ; $ns_ halt"

  if {$FLAT_ADDRESSING == 1} {
    # Set up static routes since we are using NOAH
    set cmd "[$BS(0) set ragent_] routing $numberCMs"
    for {set i 0} {$i < $numberCMs} {incr i} {
       set to [$node_($i) node-addr]
       set hop [$node_($i) node-addr]
       set cmd "$cmd $to $hop"
    }
    eval $cmd

    # Now the wired Routing Table
    for {set i 0} {$i < $numberCMs} {incr i} {
       $ns_ at 0 "addExplicitRoute $W(0) $node_($i) $W(1)"
       $ns_ at 0 "addExplicitRoute $W(1) $node_($i) $BS(0)"
    }
  }


set protID 9
#set PACKETSIZE 1400
set PACKETSIZE 1000
#set PACKETSIZE 250
#set PACKETSIZE 1460
set BURSTTIME .08
#set BURSTTIME .008
set SHAPE 0
#Set to 1 if want ftp traffic instead of HTTP...
set ALL_TCP_FTP 1
if { $ALL_TCP_FTP == 1} {
  set SHAPE 0
} else {
  set SHAPE 1.1
}

# 1000 bytes and .008 seconds is 1 mbps
# 1000 bytes and .004 seconds is 2 mbps
# 1000 bytes and .002 seconds is 4 mbps
# 1000 bytes and .0008 seconds is 10 mbps
# 1000 bytes and .0004 seconds is 20 mbps
# 1000 bytes and .0003 seconds is 26.7 mbps
# 1000 bytes and .0002 seconds is 40 mbps
# 1000 bytes and .0001 seconds is 80 mbps

#24.0Mbps with 1000 byte packets,  36Mbps with 1400 byte packets
set PACKETSIZE 1460
set INTERVAL .000333
#12.0Mbps
#set INTERVAL .000666
#16.0Mbps
#set INTERVAL .0005
#8 Mbps
#set INTERVAL .001
#5.8 Mbps
#set INTERVAL .002

#2 Mbps, for 1460 byte packets, 0.006
#set INTERVAL .004
#set PACKETSIZE 1460
#set INTERVAL .006

#1 Mbps, for 1460 byte packets, 0.012
#set INTERVAL .008
#set PACKETSIZE 1460
#set INTERVAL .012
#750 Kbps
#set INTERVAL .0107
#500Kbps
#set INTERVAL .016

#128Kbps
#set INTERVAL .0625
#For packetsize 250 byes
#set INTERVAL .0156

#64Kbps
#set INTERVAL .125

set  CONCURRENTTCP 1
set BURSTTIME .1
set IDLETIME  1
set BURSTRATE  10000000
set FLOW_PRIORITY $PRIORITY_LOW


if {$CBR_PACKETSIZEP >0} {
  set PACKETSIZE $CBR_PACKETSIZEP;
  set UNICAST_PACKETSIZE $CBR_PACKETSIZEP;
  set MULTICAST_PACKETSIZE $CBR_PACKETSIZEP;
}
if { $CBR_RATE_P > 0 } {
  set INTERVAL [expr (($PACKETSIZE*8) /$CBR_RATE_P)];
  set UNICAST_INTERVAL $INTERVAL
  set MULTICAST_INTERVAL $INTERVAL
  puts "Use RATE: $CBR_RATE_P and PACKETSIZE: $PACKETSIZE and INTERVAL: $INTERVAL "  
} else {
  set UNICAST_INTERVAL [expr (($UNICAST_PACKETSIZE*8) /$UNICAST_RATE)];
  set MULTICAST_INTERVAL [expr (($MULTICAST_PACKETSIZE*8) /$MULTICAST_RATE)];
  puts "Use UNICAST PACKETSIZE: $UNICAST_PACKETSIZE, MULTICAST PAcKETSIZE: $MULTICAST_PACKETSIZE;  UNICAST INTERVAL: $UNICAST_INTERVAL, MULTICAST INTERVAL: $MULTICAST_INTERVAL "  
}


#First create the multicast flows....go group by group


  #Ids each cx
  set CXN 0;
  #Tracks the node index in the node_ array 
  set nodeIndex 0;

if {$MULTICAST == 1} {

    #If possible, do a check here to make sure that things add up
    set groupSum 0;
    for {set x 1} {$x <= $numGroups} {incr x} {
      set groupSum  [expr $groupSum + $group($x)];
    }
    puts "MULTICAST check OK, there are $numGroups ($numberMulticastFlows multicastFlows) with total number of receivers of $groupSum"

    #don't start a flow on the monitor station
    puts "MULTICAST turned on...., there are $numberUnicastFlows  unicast flows and $numberMulticastFlows multicast flows and $numberCMs number of stations"  
    for {set j 1} {$j <= $numGroups} {incr j} {

      puts "Group Number:$j:  size of group: $group($j), prior CXN is $CXN"


      incr  CXN 
#      set CXN $j
      set multicastID $j
      set flow_port [expr $j + 1000]
      set flowStartTime [expr 0.001 + [uniform 0 2]]

      incr nodeIndex;
      #We assume MULTICAST flows are UDP/CBR AND downstream
      puts "($j): Setup a UDP DOWNSTREAM MULTICAST, Node number: $nodeIndex,  CXN: $CXN, multicastID: $multicastID, group Number: $j,  size of group: $group($j)"
      BuildUDPCBRCMulticast $ns_ $W(0) $node_($nodeIndex)  1 $MULTICAST_PACKETSIZE $MULTICAST_INTERVAL $CXN $multicastID  $flow_port $flowStartTime $printtime $UDP_TRAFFIC $N_PARAM_P $K_PARAM_P
      for {set i 1} {$i <= [expr $group($j) - 1]} {incr i} {
        incr  CXN 
        incr nodeIndex;
        puts "($j,$i): Setup a UDP DOWNSTREAM MULTICAST SINK, Node number: $nodeIndex, CXN: [expr $CXN + 0], multicastID: $multicastID,  group Number: $j,  value of group: $group($j)"
        BuildUDPMulticastSink $ns_  $W(0)  $node_($nodeIndex)  1 $CXN $multicastID  $flow_port $flowStartTime $printtime $UDP_TRAFFIC $N_PARAM_P $K_PARAM_P
        $node_($nodeIndex) joinMulticastGroup $multicastID
      }
    }


#Next, create the unicast flows 
  incr nodeIndex;
  puts "Create $numberUnicastFlows unicast flows (nodeIndex is $nodeIndex, UNICAST_UDP is $UNICAST_UDP, UDP_TRAFFIC is $UDP_TRAFFIC, and TRAFFIC_DIRECTION is $TRAFFIC_DIRECTION) "
  for {set i $nodeIndex} {$i < [ expr $nodeIndex + $numberUnicastFlows]} {incr i} {

    incr  CXN 
    set flowStartTime [expr 0.001 + [uniform 0 2]]

    if { $UNICAST_UDP == 1 } {
      if { $TRAFFIC_DIRECTION == 0 } {
        puts "ALL UDP UPSTREAM, flow number $i, CXN number $CXN"  
        # value 0: CBR   value 1 EXP
        if {$UDP_TRAFFIC == 0} {
          BuildUDPCBRCxs $ns_ $node_($i) $W(0)  1 $UNICAST_PACKETSIZE $UNICAST_INTERVAL $CXN $flowStartTime $printtime 
        }
        if {$UDP_TRAFFIC == 1} {
          BuildUDPExpCxs $ns_ $node_($i) $W(0)  1 $UNICAST_PACKETSIZE $BURSTTIME $IDLETIME $BURSTRATE $CXN $flowStartTime $printtime 
        }
        if {$UDP_TRAFFIC == 2} {
          BuildUDPParetoCxs $ns_ $node_($i) $W(0)  1 $UNICAST_PACKETSIZE  $BURSTTIME $IDLETIME $BURSTRATE $SHAPE $CXN $flowStartTime $printtime 
        }
        if {$UDP_TRAFFIC == 3} {
          BuildAPFECCxs $ns_ $node_($i) $W(0)  1 $N_PARAM_P $K_PARAM_P $UNICAST_PACKETSIZE 0 $UNICAST_INTERVAL  $BURSTTIME $IDLETIME $BURSTRATE $CXN $flowStartTime $printtime 
        }
      } else {
        puts "ALL UDP DOWNSTREAM, flow number $i, CXN number $CXN"
        if {$UDP_TRAFFIC == 0} {
          BuildUDPCBRCxs $ns_ $W(0) $node_($i)  1 $UNICAST_PACKETSIZE $UNICAST_INTERVAL $CXN $flowStartTime $printtime 
        }
        if {$UDP_TRAFFIC == 1} {
          BuildUDPExpCxs $ns_ $W(0) $node_($i)  1 $UNICAST_PACKETSIZE $BURSTTIME $IDLETIME $BURSTRATE $CXN $flowStartTime $printtime 
        }
        if {$UDP_TRAFFIC == 2} {
          BuildUDPParetoCxs $ns_ $W(0) $node_($i) 1 $UNICAST_PACKETSIZE $BURSTTIME $IDLETIME $BURSTRATE $SHAPE $CXN $flowStartTime $printtime 
        }
        if {$UDP_TRAFFIC == 3} {
          BuildAPFECCxs $ns_ $W(0)  $node_($i) 1 $N_PARAM_P $K_PARAM_P $UNICAST_PACKETSIZE 0 $UNICAST_INTERVAL  $BURSTTIME $IDLETIME $BURSTRATE $CXN $flowStartTime $printtime 
        }
      }
    } else {
      set newFileOut "TCPthru$CXN.out"
      set statsFileOut "TCPstats.out"
      if {$TURN_ON_TCL_DEBUG == 1} {
        puts "Build a TCP Cx with out file  TCPthru$CXN.out"
      }
      # Use this for upstream traffic
      if { $TRAFFIC_DIRECTION == 0 } {
        puts "ALL TCP UPSTREAM, flow number $i"
        BuildTCPCx $ns_ $node_($i) $W(0)  1 $UNICAST_PACKETSIZE $BURSTTIME $IDLETIME $BURSTRATE $CXN  $flowStartTime $printtime $newFileOut  $WINDOW  $SHAPE  $protID
      } else {
      # Use this for downstream traffic
        puts "ALL TCP UNICAST IS  DOWNSTREAM, flow number $i"
#        BuildTCPCx $ns_ $W(0) $node_($i)  1 $UNICAST_PACKETSIZE $BURSTTIME $IDLETIME $BURSTRATE $CXN $flowStartTime $printtime $newFileOut  $WINDOW  $SHAPE  $protID
         BuildTCPCxWithMode  $ns_ $W(0) $node_($i)  $CONCURRENTTCP $PACKETSIZE $BURSTTIME $IDLETIME 0  $BURSTRATE $CXN $flowStartTime $printtime $newFileOut $statsFileOut  $WINDOW  $SHAPE  $protID $FTP_TRAFFIC_MODE $FLOW_PRIORITY




      }
    }
  }
  set nodeIndex [expr $nodeIndex + $numberUnicastFlows]
  puts "Created $numberUnicastFlows unicast flows (nodeIndex is NOW $nodeIndex "

#   for {set i 3} {$i < $numberMulticastFlows } {incr i} {
#       $ns_ at 20 "$node_($i) leaveMulticastGroup 2"
#   }

#   for {set i 4} {$i < $numberMuticastFlows } {incr i} {
#       $ns_ at 40 "$node_($i) joinMulticastGroup 2"
#   }

} else {
 
puts "MULTICAST turned OFF...."  
#don't start a flow on the monitor station
for {set i 1} {$i <= $numberUnicastFlows} {incr i} {

  incr CXN;
  set flowStartTime [expr 0.001 + [uniform 0 2]]
#  set flowStartTime 0.010

  if { $UNICAST_UDP == 1 } {
    if { $TRAFFIC_DIRECTION == 0 } {
      puts "ALL UDP UPSTREAM, flow number $i, CXN number $CXN"  
      # value 0: CBR   value 1 EXP
      if {$UDP_TRAFFIC == 0} {
        BuildUDPCBRCxs $ns_ $node_($i) $W(0)  1 $UNICAST_PACKETSIZE $UNICAST_INTERVAL $CXN $flowStartTime $printtime 
      }
      if {$UDP_TRAFFIC == 1} {
        BuildUDPExpCxs $ns_ $node_($i) $W(0)  1 $UNICAST_PACKETSIZE $BURSTTIME $IDLETIME $BURSTRATE $CXN $flowStartTime $printtime 
      }
      if {$UDP_TRAFFIC == 2} {
        BuildUDPParetoCxs $ns_ $node_($i) $W(0)  1 $UNICAST_PACKETSIZE  $BURSTTIME $IDLETIME $BURSTRATE $SHAPE $CXN $flowStartTime $printtime 
      }
      if {$UDP_TRAFFIC == 3} {
        BuildAPFECCxs $ns_ $node_($i) $W(0)  1 $N_PARAM_P $K_PARAM_P $UNICAST_PACKETSIZE 0 $UNICAST_INTERVAL  $BURSTTIME $IDLETIME $BURSTRATE $CXN $flowStartTime $printtime 
      }
    } else {
      puts "ALL UDP DOWNSTREAM, flow number $i, CXN number $CXN"
      if {$UDP_TRAFFIC == 0} {
        BuildUDPCBRCxs $ns_ $W(0) $node_($i)  1 $UNICAST_PACKETSIZE $UNICAST_INTERVAL $CXN $flowStartTime $printtime 
      }
      if {$UDP_TRAFFIC == 1} {
        BuildUDPExpCxs $ns_ $W(0) $node_($i)  1 $UNICAST_PACKETSIZE $BURSTTIME $IDLETIME $BURSTRATE $CXN $flowStartTime $printtime 
      }
      if {$UDP_TRAFFIC == 2} {
        BuildUDPParetoCxs $ns_ $W(0) $node_($i) 1 $UNICAST_PACKETSIZE $BURSTTIME $IDLETIME $BURSTRATE $SHAPE $CXN $flowStartTime $printtime 
      }
      if {$UDP_TRAFFIC == 3} {
        BuildAPFECCxs $ns_ $W(0)  $node_($i) 1 $N_PARAM_P $K_PARAM_P $UNICAST_PACKETSIZE 0 $UNICAST_INTERVAL  $BURSTTIME $IDLETIME $BURSTRATE $CXN $flowStartTime $printtime 
      }
    }

  } else {

    set newFileOut "TCPthru$CXN.out"
    set statsFileOut "TCPstats.out"
    if {$TURN_ON_TCL_DEBUG == 1} {
      puts "Build a TCP Cx with out file  TCPthru$CXN.out"
    }
    # Use this for upstream traffic
    if { $TRAFFIC_DIRECTION == 0 } {
      puts "ALL TCP UPSTREAM, flow number $i"
      BuildTCPCx $ns_ $node_($i) $W(0)  1 $UNICAST_PACKETSIZE $BURSTTIME $IDLETIME $BURSTRATE $CXN  $flowStartTime $printtime $newFileOut  $WINDOW  $SHAPE  $protID
    } else {
    # Use this for downstream traffic
      puts "ALL TCP DOWNSTREAM, flow number $i"
#      BuildTCPCx $ns_ $W(0) $node_($i)  1 $UNICAST_PACKETSIZE $BURSTTIME $IDLETIME $BURSTRATE $CXN $flowStartTime $printtime $newFileOut  $WINDOW  $SHAPE  $protID
      BuildTCPCxWithMode  $ns_ $W(0) $node_($i)  $CONCURRENTTCP $PACKETSIZE $BURSTTIME $IDLETIME 0  $BURSTRATE $CXN $flowStartTime $printtime $newFileOut $statsFileOut  $WINDOW  $SHAPE  $protID $FTP_TRAFFIC_MODE $FLOW_PRIORITY
    }
  }
}

  set nodeIndex $i;
  puts "Created NO MULTICAST, created this many  $numberUnicastFlows unicast flows (nodeIndex is NOW $nodeIndex) "
}

incr nodeIndex;
#At this point, nodeIndex should be pointing to the next node available for use
if {$doVOIPLOSSMONITOR > 0} {
#   l1 <<---l0 is Upstream (LMId 1 is US)
#   l3 ---> l2 is Downstream (LMId 2 is DS)

# 1 will cause an echo- this is the default behavior
set echoFlag 0

#specifying burst size
set bsize 1
#specify the message size and rate

# Setting for sending two 711 10ms chunks in one packet, :
#This is correct for G.711
set dsize 210
#This is appropriate when emulating a video stream

set l0 [new Agent/VOIP_mon]
$l0 set LMId 1
set l2 [new Agent/VOIP_mon]
$l2 set LMId 2
$l0 data-size $dsize
$l2 data-size $dsize

$l0 burst-size $bsize 
$l2 burst-size $bsize 

$l0 echo-mode $echoFlag
$l2 echo-mode $echoFlag 


#Client currently does not log any output
$l2 set-outfile DSLossMon.out
$ns_ attach-agent $node_(0) $l0
$ns_ attach-agent $node_(0) $l2
#   l1 <<---l0 is Upstream (LMId 1 is US)
#   l3 ---> l2 is Downstream (LMId 2 is DS)
# ping $p1 W(0) --  W(1) --------  BS --- node_(0) monitor p0

set l1 [new Agent/VOIP_mon]
$l1 set LMId  1
set l3 [new Agent/VOIP_mon]
$l3 set LMId 2

$l1 data-size $dsize
$l3 data-size $dsize
$l1 burst-size $bsize
$l3 burst-size $bsize

$l1 echo-mode $echoFlag
$l3 echo-mode $echoFlag 

$l1 set-outfile USLossMon.out

$ns_ attach-agent $W(0) $l1
$ns_ attach-agent $W(0) $l3

# send 2 G.711 pkts every 20 ms
set burst_delay  .02
#to send 1400 bytes at a constant rate of 768Kbps
#set burst_delay  .01466
#to send 1400 bytes at a constant rate of 3Mbps
#set burst_delay  .0037333

set pkt_delay 0

#set burst_delay  2
#set pkt_delay 0
# 1 ms
#set pkt_delay .10
set end_time [expr  $printtime - .05]


if {$doVOIPLOSSMONITORUS > 0} {
  puts "Started a US VOIP LOSSMONITOR, packetSize: $dsize,  Interval:$burst_delay, rate=[expr $dsize*8/$burst_delay]"
  $ns_ connect $l0 $l1
  startLburstProcess $ns_ $l0 $bsize $burst_delay $pkt_delay $end_time
  #dumping the stats for loss_monitor 0-1
  $ns_ at [expr $printtime - 0.1] "$l1 dumpresults 5"
}
if {$doVOIPLOSSMONITORDS > 0} {
  puts "Started a DS VOIP LOSSMONITOR, packetSize: $dsize,  Interval:$burst_delay, rate=[expr $dsize*8/$burst_delay]"
  $ns_ connect $l3 $l2
  startLburstProcess $ns_ $l3 $bsize $burst_delay $pkt_delay $end_time
  #dumping the stats for the loss monitor 3-2
  $ns_ at [expr $printtime - 0.1] "$l2 dumpresults 5"
}
}

#setup the ping process at n1.
if {$doPINGMONITOR > 0} {

  #Create two ping agents and attach them to the nodes n0 and n2
  set p0 [new Agent/Ping]
  $ns_ attach-agent $W(0) $p0

  set p1 [new Agent/Ping]
  $ns_ attach-agent $node_(0) $p1

  #Connect the two agents
  $ns_ connect $p0 $p1

  #get the ping process going ...
  #$ns_ at 2 "$p0 send"

  #can call startPingProcess which does a ping every interval
  #or have the recv schedule doPing ....
  startPingProcess $ns_ $p0 .5
  #startPingProcess $ns_ $p0 .005

  $ns_ at $printtime "doPingStats"
}


if {$doCORRLOSSMONITOR > 0} {

#Now create a Loss_mon process between n3 and n4
#create two Lmonitor agents and initialize them and attach them
#specifying burst size

# W(0)  -----   ----  node_(0)
#      ------------->  DS direction
set lossmonitorstarttime 1.55555
set lossmonitorstoptime $stoptime
set datasize $MULTICAST_PACKETSIZE
if { $CBR_RATE_P > 0 } {
  set sendrate $CBR_RATE_P 
} else  {
  set sendrate  768000.0
}

set cl0 [new Agent/Corr_Loss_Mon]
$cl0 data-size [expr $datasize]
# send-rate in bps
$cl0 send-rate [expr $sendrate]
#This is the sender (client)
#$ns attach-agent $n4 $cl0
$ns_ attach-agent $W(0)  $cl0



set cl1 [new Agent/Corr_Loss_Mon]
$cl1 data-size [expr $datasize]
set numerator [expr $lossmonitorstoptime - $lossmonitorstarttime]
set a [expr $datasize.*8]
set denominator [expr $a/$sendrate]
set arraysize [expr  ceil($numerator/$denominator)]
$cl1 max-size-arrival-array [expr $arraysize]
#$cl1 set-outfile task_2_1_$numTCPFLOWS.out
$cl1 set-outfile DSCorrLossMon.out
#This is the receiver (server)
#$ns attach-agent $n3 $cl1
$ns_ attach-agent $node_(0)  $cl1

if {$doCORRLOSSMONITORDS ==1} {
  $ns_ connect $cl0 $cl1
  $ns_ at $lossmonitorstarttime "$cl0 start"
  $ns_ at $lossmonitorstoptime "$cl0 stop"
}

#dumping the stats for loss_monitor 0-1
# prints: pkts_sent pkts_received pkts_lost duplicate_pkts outoforderarrival_pkts avgLatency avgLossRate MBLmetric MILDmetric
$ns_ at [expr $printtime - 0.1] "$cl1 dumpresults"

}


#TraceQueueLossRate $ns_ xnetLR  $qmon 20 xnetLR.out
#TraceQueueSize $ns_ 1  $n1 $n0 .1 queue1.out
#TraceQueueTraffic $ns_ 1  $n1 $n0 1 trafficxnet.out $LINKSPEED
#$ns_ at $stoptime "finish"

$ns_ at $stoptime "finish"

proc finish {} {
global ns_
    $ns_ flush-trace
#    close $tcp1trace 
    close $tracefd
    exit 0
}

  puts "Starting Simulation..."
  $ns_ run

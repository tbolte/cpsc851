# Contains tcl helper routines to build  cable networks
#
# Revisions:  
#  $A303: add random packetSize  to BuildTCPCx
#
#



####################################################################################
#function : BuildTCPCx
#
# explanation: this routine builds and starts a TCP connection between the src and destnode
#
# inputs:  we summarize the input params as folllows:
#
#  ns  
#  SrcNode  
#  DestNode 
#  Number  : this routine will create any number of TCP cx's 
#  burstTime  : pareto param
#  idleTime  : pareto param
#  burstRate  : pareto param
#  numberID :  a unique Cx id
#  stoptime : 
#  thruHandle :  a file handle
#  window  :the max tcp window 
#  pshape  : the pareto shape param
#  protID :  specifies the type of TCP protocol to use
#
####################################################################################
proc BuildTCPCx { ns  SrcNode  DestNode Number packetSize burstTime idleTime burstRate numberID starttime stoptime thruHandle window pshape protID} {

 exec rm -f $thruHandle


 set printtime $stoptime


 for {set i 0} {$i < $Number} {incr i} {
  puts "Create TCP Cx #  [expr $i+$numberID] (file $thruHandle), window : $window"
#PROTID
if { $protID == 1 } {
  set tcp [new Agent/TCP/Reno]
}
if { $protID == 9 } {
   puts "Create TCP Sack1  Cx #  [expr $i+$numberID] (file $thruHandle)"
   set tcp [new Agent/TCP/Sack1]
}
if { $protID == 2 } {
   set tcp [new Agent/TCP/Newreno]
}
if { $protID == 3 } {
  set tcp [new Agent/TCP/Vegas]
}
if { $protID == 7 } {
  set tcp [new Agent/TCP/Reno]
#RED
  $tcp set ecn_ 1
}
#set sink [new Agent/TCPSink/DelAck]
#  set sink [new Agent/TCPSink]
if {$protID != 8} {
  if { $protID == 9 } {
#    puts "Create a Sack1/DelAck sink"
    set sink [new Agent/TCPSink/Sack1/DelAck]
  } else {
    #set sink [new Agent/TCPSink]
    set sink [new Agent/TCPSink/DelAck]
  }
}

  $tcp set fid_  [expr $i+$numberID]
  $sink set fid_ [expr $i+$numberID]
  $sink set sinkId_ [expr $i+$numberID]
  $ns attach-agent $SrcNode $tcp
  $ns attach-agent $DestNode $sink
  $ns connect $tcp $sink

#$A303
#Add random packet size
  $tcp set packetSize_ $packetSize
#  $tcp set packetSize_ 1460
#  $tcp set packetSize_ [uniform 256  1460]
#  $tcp set packetSize_ 512

  $tcp set maxcwnd_ $window
  $tcp set window_ $window
  $tcp set cxId_ [expr $i+$numberID]
  $tcp set overhead_ 0.000020
  puts "Create TCP Cx  set cxID to #  [expr $i+$numberID]"

  if { $pshape == 0} {
     set ftp [$tcp attach-app FTP]    
     puts " Attach an FTP generator and start it a $starttime"
     $ns at $starttime  "$ftp start"
  } else {
    set gen [new Application/Traffic/Pareto]
    $gen attach-agent $tcp
    $gen set packetSize_ $packetSize
    $gen set burst_time_  $burstTime
    $gen set idle_time_ $idleTime
    $gen set rate_ $burstRate
    $gen set shape_  $pshape

    puts " Attach a pareto generator and start it a $starttime"
    $ns at $starttime  "$gen start"
  }

#Comment this next line if DO NOT want a TCP thruput trace
    TraceThroughput $ns $sink  1.0 $thruHandle
#Note:  cxID 1 is the WRT monitor and cxID 2 is the snum ack traced flow.
#  if  {[expr $i + $numberID] == 1} {
#    TraceThroughput $ns $sink  1.0 $thruHandle
#  }
#  if  {[expr $i + $numberID] == 2} {
#    TraceThroughput $ns $sink  1.0 $thruHandle
#  }
#  $ns at $printtime "printtimeouts [expr $i + $numberID] $tcp"
#  $ns at $printtime "printretrans [expr $i + $numberID] $tcp"
#  $ns at $printtime "printpkts [expr $i + $numberID]  $tcp"
#  $ns at $printtime "printbytesdelivered  [expr $i + $numberID] $sink"
  puts " BuildTCPCx:  will call dumpFinalTCPStats at time $printtime"
  $ns at $printtime "dumpFinalTCPStats  [expr $i + $numberID] 0 $tcp  $sink TCPstats.out"

 }

}

proc BuildTCPCx1 { ns  SrcNode  DestNode Number burstTime idleTime burstRate numberID stoptime thruHandle window pshape protID} {

 exec rm -f $thruHandle


 set printtime $stoptime


 for {set i 1} {$i < [expr $Number+1]} {incr i} {
#  puts "Create TCP Cx #  [expr $i+$numberID] (file $thruHandle)"
#PROTID
if { $protID == 1 } {
  set tcp [new Agent/TCP/Reno]
}
if { $protID == 2 } {
   set tcp [new Agent/TCP/Newreno]
}
if { $protID == 3 } {
  set tcp [new Agent/TCP/Vegas]
}
#if { $protID == 4 } {
#  set tcp [new Agent/TCP/Vegas1]
#}
#if { $protID == 5 } {
#  set tcp [new Agent/TCP/DCA]
#}
#if { $protID == 6 } {
#  set tcp [new Agent/TCP/Dual]
#}
if { $protID == 7 } {
  set tcp [new Agent/TCP/Reno]
#RED
  $tcp set ecn_ 1
}
set sink [new Agent/TCPSink/DelAck]
#  set sink [new Agent/TCPSink]
  $tcp set fid_  [expr $i+$numberID]
  $sink set fid_ [expr $i+$numberID]
  $sink set sinkId_ [expr $i+$numberID]
  $ns attach-agent $SrcNode $tcp
  $ns attach-agent $DestNode $sink
  $ns connect $tcp $sink

  $tcp set packetSize_ 1460
  $tcp set maxcwnd_ $window
  $tcp set window_ $window
  $tcp set cxId_ [expr $i+$numberID]
  $tcp set overhead_ 0.000020
#  puts "Create TCP Cx  set cxID to #  [expr $i+$numberID]"

  if { $pshape == 0} {
     set ftp [$tcp attach-app FTP]    
     $ns at 0.0 "$ftp start"
  } else {
    set gen [new Application/Traffic/Pareto]
    $gen attach-agent $tcp
    $gen set packetSize_ 1460
#12 ms at 10Mbps is about 11 packets
#    $gen set burst_time_ 12ms
#    $gen set idle_time_ 300ms
#    $gen set rate_ 10000k
    $gen set burst_time_  $burstTime
    $gen set idle_time_ $idleTime
    $gen set rate_ $burstRate
    $gen set shape_  $pshape

#    if  {[expr $i + $numberID] == 1038} {
#      $gen set mode  1
#    }
#  puts "For TCP Cx #  [expr $i+$numberID], attach pareto (burst time $burstTime; idleTime $idleTime"
    $ns at 0.0 "$gen start"
  }

#All DSLs ids are 10001 ...  DSLout1000z
#All Branch users are 1001 and up ... Officeout100x
#to create a tcp throughput file ..
#  TraceThroughput $ns $sink  1.0 $thruHandle
  if  {[expr $i + $numberID] > 10001} {
    if {[expr $i + $numberID] < 10010} {
#      $ns at $printtime "printtimeouts [expr $i + $numberID] $tcp"
#      $ns at $printtime "printretrans [expr $i + $numberID] $tcp"
#      $ns at $printtime "printpkts [expr $i + $numberID]  $tcp"
#      $ns at $printtime "printbytesdelivered  [expr $i + $numberID] $sink"
#      TraceThroughput $ns $sink  1.0 $thruHandle
    } 
  }
#to create thrux.out for a block ...
# to see how much data sent in first 42 seconds, 
#set printtime 2500
  
  if  {[expr $i + $numberID] >= 1000} {
    if {[expr $i + $numberID] < 1300} {
      puts "For TCP Cx #  [expr $i+$numberID] create output  file"
      $ns at $printtime "printtimeouts [expr $i + $numberID] $tcp"
      $ns at $printtime "printretrans [expr $i + $numberID] $tcp"
      $ns at $printtime "printpkts [expr $i + $numberID]  $tcp"
#      $ns at $printtime "printbytesdelivered  [expr $i + $numberID] $sink"
#      TraceThroughput $ns $sink  2.0 $thruHandle
    }
  }
#  if  {$i == 1} {
#    $ns at $printtime "printtimeouts [expr $i + $numberID] $tcp"
#    $ns at $printtime "printretrans [expr $i + $numberID] $tcp"
#    $ns at $printtime "printpkts [expr $i + $numberID]  $tcp"
#    $ns at $printtime "printbytesdelivered  [expr $i + $numberID] $sink"
#  }
 }

}

####################################################################################
#function : BuildTCPCxWithMode
#
# explanation: this routine builds and starts a TCP connection between the src and destnode
#
# inputs:  we summarize the input params as folllows:
#
#  ns  
#  SrcNode  
#  DestNode 
#  Number  : this routine will create any number of TCP cx's 
#  burstTime  : pareto param
#  idleTime  : pareto param
#  burstRate  : pareto param
#  numberID :  a unique Cx id
#  stoptime : 
#  thruHandle :  a file handle
#  window  :the max tcp window 
#  pshape  : the pareto shape param
#  protID :  specifies the type of TCP protocol to use
#
#  mode:  specifies the traffic generator to be used
#   0: exponential
#   1: CBR
#   2: FTP
#   3: Pareto
#   4:
####################################################################################
proc BuildTCPCxWithMode { ns  SrcNode  DestNode Number packetSize burstTime idleTime intervalTime burstRate numberID starttime stoptime thruHandle statsHandle window pshape protID mode priority} {

global TURN_ON_THROUGHPUT_MONITORS

if { $TURN_ON_THROUGHPUT_MONITORS == 1} {
 exec rm -f $thruHandle
}



 set printtime $stoptime

puts "BuildTCPCxWithMode: Create $Number UDP Connections, starting CxID:$numberID, mode:$mode and startime:$starttime stoptime:$stoptime, priority is $priority"

 for {set i 0} {$i < $Number} {incr i} {
  puts "Create TCP Cx #  [expr $i+$numberID] (file $thruHandle),  mode is $mode, priority is $priority"
#PROTID
if { $protID == 1 } {
  set tcp [new Agent/TCP/Reno]
}
if { $protID == 9 } {
#  puts "Create a SACk test flow.."
   set tcp [new Agent/TCP/Sack1]
}
if { $protID == 2 } {
   set tcp [new Agent/TCP/Newreno]
}
if { $protID == 3 } {
  set tcp [new Agent/TCP/Vegas]
}
if { $protID == 7 } {
  set tcp [new Agent/TCP/Reno]
#RED
  $tcp set ecn_ 1
}
#set sink [new Agent/TCPSink/DelAck]
#  set sink [new Agent/TCPSink]
if {$protID != 8} {
  if { $protID == 9 } {
#    puts "Create a Sack1/DelAck sink"
    set sink [new Agent/TCPSink/Sack1/DelAck]
  } else {
    #set sink [new Agent/TCPSink]
    set sink [new Agent/TCPSink/DelAck]
  }
}

  set CxID [expr $i+$numberID]

  $tcp set fid_  $CxID
  $sink set fid_  $CxID
  $sink set sinkId_ $CxID
  $sink set prio_ $priority
  $ns attach-agent $SrcNode $tcp
  $ns attach-agent $DestNode $sink
  $ns connect $tcp $sink

  $tcp set packetSize_  $packetSize
  $tcp set maxcwnd_ $window
  $tcp set window_ $window
  $tcp set cxId_ $CxID
  $tcp set overhead_ 0.000020

  $tcp set prio_ $priority

#$A303
#Add random packet size
  $tcp set packetSize_ $packetSize
#  $tcp set packetSize_ 1460
#  $tcp set packetSize_ [uniform 256  1460]
#  $tcp set packetSize_ 512

   if {$mode == 0} {
      set exp [new Application/Traffic/Exponential]
      $exp attach-agent $tcp
  #  $exp set packetSize_ 1460
      $exp set packetSize_ $packetSize
      $exp set burst_time_ $burstTime
      $exp set idle_time_ $idleTime
      $exp set rate_ $burstRate
      puts "Start an EXP TCP Cx  cxId_:$CxID, burst rate:$burstRate"
      $ns at $starttime "$exp start"
      $ns at $stoptime "$exp stop"
    } elseif {$mode == 1} {
      set cbr [new Application/Traffic/CBR]
      $cbr attach-agent $tcp
      $cbr set packetSize_ $packetSize
      $cbr set interval_ $intervalTime
      $cbr set mode_ 0
      puts "Start a CBR TCP Cx  cxId_:$CxID, interval:$intervalTime"
      $ns at $starttime "$cbr start"
      $ns at $stoptime "$cbr stop"
    } elseif {$mode == 2} {
       set ftp [$tcp attach-app FTP]    
       puts " Attach an FTP generator and start it a $starttime"
       $ns at $starttime  "$ftp start"
       $ns at $stoptime  "$ftp stop"
    } elseif {$mode == 3} {
      set gen [new Application/Traffic/Pareto]
      $gen attach-agent $tcp
      $gen set packetSize_ $packetSize
      $gen set burst_time_  $burstTime
      $gen set idle_time_ $idleTime
      $gen set rate_ $burstRate
      $gen set shape_  $pshape

      puts " Attach a pareto generator and start it a $starttime"
      $ns at $starttime  "$gen start"
      $ns at $stoptime  "$gen stop"
    } else {
      set ftp [$tcp attach-app FTP]    
      puts " By default, we Attach an FTP generator and start it a $starttime"
      $ns at $starttime  "$ftp start"
      $ns at $stoptime  "$ftp stop"
    }

  if { $TURN_ON_THROUGHPUT_MONITORS == 1} {
    TraceThroughput $ns $sink  1.0 $thruHandle
  }
  if { $TURN_ON_THROUGHPUT_MONITORS == 0} {
    exec rm -f $thruHandle
    if { $CxID == 1116} {
     TraceThroughput $ns $sink  1.0 $thruHandle
    }
    if { $CxID == 6121} {
      TraceThroughput $ns $sink  1.0 $thruHandle
    }
  }

  if { $CxID == 6046} {
    set tcpTraceFile [open tcp6046.tr w]
    setupTcpTracing $tcp  $tcpTraceFile
  }

  $ns at $printtime "dumpFinalTCPStats  [expr $i + $numberID] 0 $tcp  $sink $statsHandle"
 }

}

####################################################################################
#function : BuildStreamingFlow
#
####################################################################################
proc BuildStreamingFlow { ns  SrcNode  DestNode Number rate  numberID stoptime} {


 set printtime $stoptime


 for {set i 1} {$i < [expr $Number+1]} {incr i} {


  set newFileOut "UDPFlowout[expr $i+$numberID].out"
  exec rm -f $newFileOut
#  puts "Create UDP Cx #  [expr $i+$numberID] (file $newFileOut)"

  set udpf1 [new Agent/UDP]
  set udpsinkf1 [new Agent/UDPSink]
  $udpf1 set fid_ 400
  $udpsinkf1 set fid_ 400 

  $ns attach-agent $SrcNode $udpf1
  $ns attach-agent $DestNode $udpsinkf1
  $ns connect $udpf1 $udpsinkf1

  set cbr0 [new Application/Traffic/CBR]
  $cbr0 attach-agent $udpf1
  $cbr0 set rate_ $rate;
#set packetsize based on Heideman's paper on Realaudio models...
#  $cbr0 set packetSize_ 1460;
  $cbr0 set packetSize_ 254;
  $ns at 0.021 "$cbr0 start"
  UDPTraceThroughput $ns $udpsinkf1  2.0 $newFileOut

 }
}
####################################################################################
#function : BuildUDPEchoFlow
#
#  $A1 : 11/28/2004: if numberID+$i == 1, then set the cbr mode_ to 1
#          This way, only this CBR flow will create the two trace files
#             UDPSINK.recv and CBR.send (the rx and send side)
####################################################################################
proc BuildUDPEchoFlow { ns  SrcNode  DestNode Number rate  numberID starttime stoptime} {


 set printtime $stoptime


 for {set i 0} {$i < $Number} {incr i} {

  set newFileOut "UDPEchoFlowout[expr $i+$numberID].out"
  exec rm -f $newFileOut
  puts "Create UDP Echo Cx #  [expr $i+$numberID] (file $newFileOut)"

  set udpf1 [new Agent/UDP]
  set udpsinkf1 [new Agent/UDPSink]
  $udpf1 set fid_ [expr $numberID + $i]
  $udpsinkf1 set fid_ [expr $numberID + $i]
#  $udpf1 set fid_ 3000
#  $udpsinkf1 set fid_ 3000 

  $ns attach-agent $SrcNode $udpf1
  $ns attach-agent $DestNode $udpsinkf1
  $ns connect $udpf1 $udpsinkf1

  set cbr0 [new Application/Traffic/CBR]
  $cbr0 attach-agent $udpf1
  $cbr0 set rate_ $rate;
  $cbr0 set packetSize_ 64;  

#$A1 
  if { [expr $numberID + $i] == 1} {
#if set- this creates CBR.send
     $cbr0  set mode_ 1;
      puts "Create UDP Echo Cx set mode_ to 1 "
#if set- random delay between sends 
#     $cbr0  set random_ 1;
#      puts "Create UDP Echo Cx set random_ to 1 "
  }

  $ns at $starttime "$cbr0 start"
  UDPTraceThroughput $ns $udpsinkf1  2.0 $newFileOut

 }
}

####################################################################################
#function : BuildExpCxs
#
####################################################################################
proc BuildExpCxs { ns  SrcNode  DestNode Number burstTime idleTime burstRate numberID stoptime thruHandle window protID} {

 exec rm -f $thruHandle
 set printtime $stoptime


 for {set i 1} {$i < [expr $Number+1]} {incr i} {
  puts "Create EXP TCP Cx #  [expr $i+$numberID] (file $thruHandle)"
#PROTID
if { $protID == 1 } {
  set tcp [new Agent/TCP/Reno]
}
if { $protID == 2 } {
   set tcp [new Agent/TCP/Newreno]
}
if { $protID == 3 } {
  set tcp [new Agent/TCP/Vegas]
}
if { $protID == 4 } {
  set tcp [new Agent/TCP/Vegas1]
}
if { $protID == 5 } {
  set tcp [new Agent/TCP/DCA]
}
if { $protID == 6 } {
  set tcp [new Agent/TCP/Dual]
}
if { $protID == 7 } {
  set tcp [new Agent/TCP/Reno]
#RED
  $tcp set ecn_ 1
}
set sink [new Agent/TCPSink/DelAck]
#  set sink [new Agent/TCPSink]
  $tcp set fid_  [expr $i+$numberID]
  $sink set fid_ [expr $i+$numberID]
  $sink set sinkId_ [expr $i+$numberID]
  $ns attach-agent $SrcNode $tcp
  $ns attach-agent $DestNode $sink
  $ns connect $tcp $sink

  $tcp set packetSize_ 1460
  $tcp set maxcwnd_ $window
  $tcp set window_ $window
#  if { $numberID == 5000 } {
#    $tcp set maxcwnd_ 211
#    $tcp set window_ 211
#  }
  $tcp set cxId_ [expr $i+$numberID]
  $tcp set overhead_ 0.000020

#  set ftp [$tcp attach-app FTP]    
#  $ns at 0.2 "$ftp start"

  set exp [new Application/Traffic/Exponential]
  $exp attach-agent $tcp
  $exp set packetSize_ 1460
  $exp set burst_time_ $burstTime
  $exp set idle_time_ $idleTime
  $exp set rate_ $burstRate
  $ns at 0.2 "$exp start"

#to create a tcp throughput file ..
  TraceThroughput $ns $sink  1.0 $thruHandle
  $ns at $printtime "dumpFinalTCPStats  [expr $i+$numberID] 0 $tcp  $sink P2PTCPstats.out"

#  if  {[expr $i + $numberID -1] == 9001} {
#    $ns at $printtime "printtimeouts [expr $i + $numberID] $tcp"
#    $ns at $printtime "printretrans [expr $i + $numberID] $tcp"
#    $ns at $printtime "printpkts [expr $i + $numberID]  $tcp"
#    $ns at $printtime "printbytesdelivered  [expr $i + $numberID] $sink"
#    TraceThroughput $ns $sink  1.0 $thruHandle
#  }
#  if  {$i == 1} {
#    $ns at $printtime "printtimeouts [expr $i + $numberID] $tcp"
#    $ns at $printtime "printretrans [expr $i + $numberID] $tcp"
#    $ns at $printtime "printpkts [expr $i + $numberID]  $tcp"
#    $ns at $printtime "printbytesdelivered  [expr $i + $numberID] $sink"
#  }
 }
}


proc AgentTraceThroughput { ns  AgentSrc interval fname } {

set x [ $AgentSrc set agentSinkThroughput ]
puts "AgentTrace is  $x"


	proc AgentThroughputdump { ns newsrc interval fname } {
set x [ $newsrc set agentSinkThroughput ]
puts "AgentTrace: agentSinkThroughput  is  $x"
          set f [open $fname a]
          $ns at [expr [$ns now] + $interval] "AgentThroughputdump $ns $newsrc $interval $fname"
          puts [$ns now]/throughput-bytes=[$newsrc set agentSinkThroughput]
#          puts [ns now]/throughput=[expr ( ([$newsrc set agentSinkThroughput]/ $interval) * 8) ]
           set mythroughput [expr ( ([$newsrc set agentSinkThroughput]/ $interval) * 8) ]
#          puts "TraceThroughput:  [$ns now]  $mythroughput"
           puts $f "[$ns now]  $mythroughput"
          flush $f
#          $newsrc set agentSinkThroughput 0
          close $f
	}
	$ns at 0.0 "AgentThroughputdump $ns $AgentSrc $interval $fname"


}

proc UDPTraceThroughput { ns  AgentSrc interval fname } {
          puts "TraceThroughput:  start it up with $fname "

	proc UDPThroughputdump { ns newsrc interval fname } {
          set f [open $fname a]
          $ns at [expr [$ns now] + $interval] "UDPThroughputdump $ns $newsrc $interval $fname"
#          puts [$ns now]/throughput-bytes=[$newsrc set UDPSinkThroughput]
#          puts [ns now]/throughput=[expr ( ([$newsrc set UDPSinkThroughput]/ $interval) * 8) ]
           set mythroughput [expr ( ([$newsrc set UDPSinkThroughput]/ $interval) * 8) ]
#          puts "UDPTraceThroughput:  [$ns now]  [$newsrc set UDPSinkThroughput]  $mythroughput"

           puts $f "[$ns now]  $mythroughput"
          flush $f
          $newsrc set UDPSinkThroughput 0
          close $f
	}
	$ns at 0.0 "UDPThroughputdump $ns $AgentSrc $interval $fname"
}

#same as TCPTraceSendRate, just doesnt need the ns param
proc TraceTCPSendRate { AgentSrc interval fname } {

        set ns [Simulator instance]
	proc TraceTCPSendRatedump { newsrc interval fname } {
          set ns [Simulator instance]
          set f [open $fname a]
          $ns at [expr [$ns now] + $interval] "TraceTCPSendRatedump $newsrc $interval $fname"
#          puts [$ns now]/sendbytes=[$newsrc set TCPSendBytes]
#          puts [ns now]/sendrate=[expr ( ([$newsrc set TCPSendBytes]/ $interval) * 8) ]
           set bytesSent [$newsrc set TCPSendBytes]
           set mythroughput [expr ( ($bytesSent/ $interval) * 8) ]
#           set mythroughput [expr ( ([$newsrc set TCPSendBytes]/ $interval) * 8) ]
#          puts "TraceTCPSendrate:  [$ns now]  $mythroughput"
           puts $f "[$ns now]  $mythroughput $bytesSent"
          flush $f
          $newsrc set TCPSendBytes 0
          close $f
	}
	$ns at 1.0 "TraceTCPSendRatedump $AgentSrc $interval $fname"
}


proc TCPTraceSendRate { ns  AgentSrc interval fname } {

	proc TCPSendRatedump { ns newsrc interval fname } {
          set f [open $fname a]
          $ns at [expr [$ns now] + $interval] "TCPSendRatedump $ns $newsrc $interval $fname"
#          puts [$ns now]/sendbytes=[$newsrc set TCPSendBytes]
#          puts [ns now]/sendrate=[expr ( ([$newsrc set TCPSendBytes]/ $interval) * 8) ]
           set bytesSent [$newsrc set TCPSendBytes]
           set mythroughput [expr ( ($bytesSent/ $interval) * 8) ]
#           set mythroughput [expr ( ([$newsrc set TCPSendBytes]/ $interval) * 8) ]
#          puts "TraceTCPSendrate:  [$ns now]  $mythroughput"
           puts $f "[$ns now]  $mythroughput $bytesSent"
          flush $f
          $newsrc set TCPSendBytes 0
          close $f
	}
	$ns at 0.0 "TCPSendRatedump $ns $AgentSrc $interval $fname"
}

proc UDPTraceSendRate { ns  AgentSrc interval fname } {

	proc UDPSendRatedump { ns newsrc interval fname } {
          set f [open $fname a]
          $ns at [expr [$ns now] + $interval] "UDPSendRatedump $ns $newsrc $interval $fname"
#          puts [$ns now]/sendbytes=[$newsrc set UDPSendBytes]
#          puts [ns now]/sendrate=[expr ( ([$newsrc set UDPSendBytes]/ $interval) * 8) ]
           set mythroughput [expr ( ([$newsrc set UDPSendBytes]/ $interval) * 8) ]
#          puts "TraceUDPSendrate:  [$ns now]  $mythroughput"
           puts $f "[$ns now]  $mythroughput"
          flush $f
          $newsrc set UDPSendBytes 0
          close $f
	}
	$ns at 0.0 "UDPSendRatedump $ns $AgentSrc $interval $fname"
}




proc GetErrorModuleBytes { ns  ErrorModule interval fname } {

	proc ErrorModuleDump { ns Module interval fname } {
          set f [open $fname a]
          $ns at [expr [$ns now] + $interval] "ErrorModuleDump $ns $Module $interval $fname"
#          puts [$ns now]/ErrorModulebytes=[$Module set totalBytes]
#          puts [ns now]/sendrate=[expr ( ([$Module set totalBytes]/ $interval) * 8) ]
           set  totalBytes [$Module set totalBytes]
           puts $f "$totalBytes"
#           puts $f "[$ns now]  $totalBytes"
          flush $f
          $Module set totalBytes 0
          close $f
	}
	$ns at 0.0 "ErrorModuleDump $ns $ErrorModule $interval $fname"
}

proc TraceErrorModuleUtilization { ns ErrorModule interval linkID stoptime} {

	proc ErrorModuleUtilizationDump { ns Module interval } {
#          set f [open $fname a]
          $ns at [expr [$ns now] + $interval] "ErrorModuleUtilizationDump $ns $Module $interval"
           set totalBytes [$Module set totalBytes]

           set totalOnCount [$Module set totalOnCount]
           set totalOffCount [$Module set totalOffCount]
#          puts "[$ns now]/onCount = $onCount"
#          puts "[$ns now]/offCount = $offCount"
#          puts "[$ns now]/totalOnCount = $totalOnCount"
#          puts "[$ns now]/totalOffCount = $totalOffCount"
#           set  totalUsage [expr $onCount+ $offCount]
#           set  utilSample  [expr $onCount/$totalUsage]
#           puts $f "$utilSample"
#           puts $f "[$ns now]  $utilSample"
#           puts "[$ns now]  $utilSample"
#          flush $f
          if {$totalBytes == 0} {
            $Module set totalOffCount [expr $totalOffCount + 1]
          } else {
            $Module set totalOnCount [expr $totalOnCount + 1]
            $Module set totalBytes 0
          }
#          close $f
	}
	$ns at 0.0 "ErrorModuleUtilizationDump $ns $ErrorModule $interval "
        $ns at $stoptime "ComputeModuleUtilization $ns $ErrorModule $linkID "
}

proc ComputeModuleUtilization { ns ErrorModule linkID} {

           set totalOnCount  [$ErrorModule set totalOnCount]
           set totalOffCount [$ErrorModule set totalOffCount]
           set sum [expr $totalOnCount+$totalOffCount]
           set totalOnCount  [expr $totalOnCount * 100]
           set util  0.001
           if {$sum > 0 } {
            set util [expr $totalOnCount / $sum]
            puts "Utilization ($linkID): $util% ($totalOnCount / $sum)"
           } else {
            puts "Utilization ($linkID):  ERROR,  zero sum ????"
           }
}



####################################################################################
#function : TraceThroughput
# 
#  Simply gets and clears TCPSink variables ...
#
#  Dumps the throughput in bytes per second
#
####################################################################################

proc TraceThroughput { ns  tcpsinkSrc interval fname } {
#puts "We will open TraceThroughput file $fname"

	proc Throughputdump { ns src interval fname } {
          set f [open $fname a]
          $ns at [expr [$ns now] + $interval] "Throughputdump $ns $src $interval $fname"
#          puts [$ns now]/throughput-bytes=[$src set sinkThroughput]
#          puts [ns now]/throughput=[expr ( ([$src set sinkThroughput]/ $interval) * 8) ]
           set mythroughput [expr ( ([$src set sinkThroughput]/ $interval) * 8) ]
#          puts "TraceThroughput:  [$ns now]  $mythroughput"
#To see just bps throughput..
#           puts $f "[$ns now]  $mythroughput"
#to see bits per second AND bytes
           set tmpBytes [$src set sinkThroughput]
           puts $f "[$ns now]  $mythroughput $tmpBytes"
          flush $f
          $src set sinkThroughput 0
          close $f
	}
	$ns at [$ns now] "Throughputdump $ns $tcpsinkSrc $interval $fname"
#	$ns at 0.0 "Throughputdump $ns $tcpsinkSrc $interval $fname"
}

#Trace tcp cx things periodically 
proc TraceTCP { ns  tcp interval fname  } {

	proc TCPdump { ns src interval fname } {
         set tmpwnd [$src set window_]
         set tmpcwnd [$src set cwnd_]
#         puts "TraceTCP:  tmpwnd is $tmpwnd, cwnb is $tmpcwnd  "
         set f [open $fname a]
         $ns at [expr [$ns now] + $interval] "TCPdump $ns $src $interval $fname"
         puts $f "[$ns now]  $tmpwnd $tmpcwnd"
         flush $f
         close $f
	}
	$ns at 0.0 "TCPdump $ns $tcp $interval $fname"
}

#given a link, we need to get queue data ...
# this is a pain-  see ns-lib.tcl for the helper routines that do this
proc TraceQueueSize { ns label node1 node2 interval fname} {

  proc queuedump {ns mylabel node1 node2 myinterval fname } {
    set f [open $fname a]
    $ns at [expr [$ns now] + $myinterval] "queuedump $ns $mylabel $node1 $node2 $myinterval $fname"

    set cur_bytes [$ns get-queue-size $node1 $node2]
    set cur_pkts [$ns get-queue-length $node1 $node2]  
    set max_cur_bytes [$ns get-max-bytes $node1 $node2]  
    set min_cur_bytes [$ns get-min-bytes $node1 $node2]  
    set max_cur_pkts [$ns get-max-packets $node1 $node2]  
    set min_cur_pkts [$ns get-min-packets $node1 $node2]  
#    puts "link $mylabel cur_bytes:  $cur_bytes, cur_pkts: $cur_pkts"
#    puts "link $mylabel max_cur_bytes:  $max_cur_bytes, min_cur_bytes: $min_cur_bytes"
#    puts "link $mylabel max_cur_pkts:  $max_cur_pkts, min_cur_pkts: $min_cur_pkts"
    $ns set-max-packets $node1 $node2  $cur_pkts
    $ns set-min-packets $node1 $node2  $cur_pkts
#Because of the hack for TraceQueueDistribution,  don't modify the max-bytes or min-bytes
#    $ns set-max-bytes $node1 $node2  $cur_bytes
#    $ns set-min-bytes $node1 $node2  $cur_bytes
    puts $f "[$ns now]  $max_cur_pkts  $min_cur_pkts"
    close $f
  }
  $ns at 0.0 "queuedump $ns $label $node1 $node2  $interval $fname"
}

#trace the total bytes that leaves a queue, and calculate the utilization
#we assume the duration is current test time
proc TraceQueueTraffic { ns label node1 node2 interval fname linkspeed} {

  proc queuetrafficdump {ns mylabel node1 node2 myinterval fname mylinkspeed} {
    set f [open $fname a]
    $ns at [expr [$ns now] + $myinterval] "queuetrafficdump $ns $mylabel $node1 $node2 $myinterval $fname $mylinkspeed"

#    set curTime [$ns now]
    set curTime $myinterval
    if { $curTime != 0 } {
     set cur_bytes_in [$ns get-total-in $node1 $node2]
     set cur_bytes_out [$ns get-total-out $node1 $node2]
#     puts "TraceQueueTraffic: curTime is $curTime , cur bytes out = $cur_bytes_out"
     set max_out [expr $mylinkspeed * $curTime / 8.0]
     set util [expr $cur_bytes_out / $max_out + 0.0]
     set util [expr $util * 100]
#do the following if you want to reset the total out count.  Do this
#if measuring utilization on a period basis....
     $ns set-total-out $node1 $node2 0
#     puts "TraceQueueTraffic: max out is $max_out and util is $util"
     puts $f "[$ns now]  $cur_bytes_out $util%"
     close $f
    }
  }
  $ns at 0.0 "queuetrafficdump $ns $label $node1 $node2  $interval $fname $linkspeed"
}

#given a link, we need to get queue data ...
# This operates on the Integral object that is the queue mon bytes integral monitor
# This computes the avg queue size  in bytes and the avg delay
proc TraceQueueMonSize { ns integ link label interval fname} {

  proc queuemondump {ns integ link mylabel  myinterval fname } {
    set f [open $fname a]
    $ns at [expr [$ns now] + $myinterval] "queuemondump $ns $integ $link $mylabel  $myinterval $fname"

    set qsize [$integ set sum_]
#    puts "qsize is $qsize"
    set delay [expr 8 * $qsize / [[$link link] set bandwidth_]]
#   puts "[$ns now] delay=$delay"

    if {$myinterval != 0} {
      puts $f "[$ns now]  [expr $qsize / $myinterval] $delay"
    } else {
       puts  " error"
    }
#    puts $f "[$ns now]  $qsize $delay"


#reset sum_
    $integ set sum_ 0.0

    close $f
  }
  $ns at 0.0 "queuemondump $ns $integ $link $label  $interval $fname"
}



proc printutilization { linkid qmon linkspeed duration packetsize} {

    set departures [$qmon set pdepartures_]
    set maxdeparts [expr $linkspeed *  $duration / $packetsize + 0.0]
puts "departures = $departures"
puts "maxdeparts = $maxdeparts"
    set util [expr $departures * 100 + 0.00]
    set util [expr $util / $maxdeparts +  0.00]

    puts "link $linkid link utilization is $util% "
}


#To get the % of protocols at a link...
#Turn on MONITOR in drop-tail.cc.
#we overload the following variables (i.e., we hack it):
# Note:  make sure that the TCP constructor issues a set_pkttype(PT_DCA)
#       of (PT_VEGAS).
#
#  qSize : # DCA flows
#  qLength : #Other TCP flows
#  maxQSize : # UDP
#  minQSize : # other flows
#
proc TraceQueueDistribution { ns label node1 node2 interval fname} {

  proc queuedistdump {ns mylabel node1 node2 myinterval fname } {
    set f [open $fname a]
    $ns at [expr [$ns now] + $myinterval] "queuedistdump $ns $mylabel $node1 $node2 $myinterval $fname"

    set cur_bytes [$ns get-queue-size $node1 $node2]
    set cur_pkts [$ns get-queue-length $node1 $node2]  
    set max_cur_bytes [$ns get-max-bytes $node1 $node2]  
    set min_cur_bytes [$ns get-min-bytes $node1 $node2]  

    $ns set-max-bytes $node1 $node2  0
    $ns set-min-bytes $node1 $node2  0
    $ns set-queue-size $node1 $node2 0
    $ns set-queue-length $node1 $node2  0
    puts $f  "$mylabel: [$ns now] $cur_bytes  $max_cur_bytes  $min_cur_bytes"
    close $f
  }
  $ns at 0.0 "queuedistdump $ns $label $node1 $node2  $interval $fname"
}



#########################################################################################
# GetLossModuleStats
#
#  This function gets the loss module's actual loss rate
#
#########################################################################################
#given a link, we need to get queue data ...
# this is a pain-  see ns-lib.tcl for the helper routines that do this
proc GetLossModuleStats { ns label loss} {

  set totalPkts [$loss set totalPkts]
  set totalDrops [$loss set totalDrops]
  set dropRate [expr $totalDrops / $totalPkts]
  puts "for Link $label, the packet loss rate was $dropRate "

}



#Helper  function to output link utilization and bw estimate
#2/18/99: Currently not working...
proc trace-util {ns linkhandle interval f} {
	if { $f != "" } {
         set l-est [$linkhandle load-est];
         set l-utl [$linkhandle link-utlzn];
         puts $f "[$ns now] $l-est  $l-utl" 
	}
	$ns at [expr [$ns now]+$interval] "trace-util $ns $linkhandle $interval $f" 
}

proc finish_new_run {file  fname } {


        exec rm -f temp.out
	set f [open temp.out w]
	puts $f "TitleText: $file"
	puts $f "Device: Postscript"
	puts $f \"throughput
	flush $f
	exec cat $fname >@ $f
	close $f
#        exec xgraph -bb -x time -y throughput temp.out 

}

proc dumpFinalUDPStats {label starttime udpsink outputFile} {
  set f [open $outputFile a]
  set ns [Simulator instance]
  set bytesDel  [$udpsink set UDPSinkNumberBytesDelivered]
  set PktsDel  [$udpsink set UDPSinkTotalPktsReceived]
  set PktsDropped  [$udpsink set UDPSinkNumberPktsDropped]
  set PktsOutOfOrder  [$udpsink set UDPSinkPktsOutOfOrder]
  set AvgJitter  [$udpsink set UDPAvgJitter]
  set AvgLatency  [$udpsink set UDPAvgLatency]
  set tmpTime [expr [$ns now] - $starttime] 
  set thruput [expr $bytesDel*8.0/$tmpTime + 0.0]
#  puts "dumpFinalUDPStats: bytesDel is $bytesDel, tmpTime is $tmpTime and thruput is $thruput (output file: $outputFile)"
  set tmpX [ expr $PktsDropped*1.0 + $PktsDel*1.0]
  if {$tmpX > 0} {
     set dropRate [expr $PktsDropped*1.0 / $tmpX + 0.00]
   } else { 
     set dropRate 0
   }
#   puts "$label $bytesDel $arrivals $lossno $dropRate $meanRTT $notimeouts $toFreq $thruput  0 0 0"
#make the number of fields the same as with dumpFinalTFRC.... DO NOT CHANGE!!!! MATLAB needs this.
   puts $f "$label $bytesDel $PktsDel $PktsDropped $PktsOutOfOrder $dropRate $thruput $AvgJitter $AvgLatency"
   flush $f
   close $f

}


proc dumpTCPStats {label tcp tcpsink outputFile} {
  set f [open $outputFile a]
  set bytesDel  [$tcpsink set numberBytesDelivered]
  set notimeouts   [expr [$tcp set nTimeouts_] + 0.0]
  set lossno [ expr [$tcp set nRexmits_] +  0.0]
  set drops [expr $lossno * 100]
  set arrivals [expr [$tcp set ack_] + $lossno + 0.0]
  if {$arrivals > 0} {
     set dropRate [expr $drops / $arrivals + 0.00]
   } else { 
     set dropRate 0
   }
   set toFreq 0.0
   if {$lossno > 0.0} {
#puts "notimeouts is $notimeouts and lossno is $lossno"
     set timeoutdrops [expr $notimeouts * 100]
     set toFreq [expr $timeoutdrops / $lossno + 0.00] 
   }
#   puts "$label $bytesDel $arrivals $lossno $dropRate $notimeouts $toFreq"
   puts $f "$label $bytesDel $arrivals $lossno $dropRate $notimeouts $toFreq"
   flush $f
   close $f
}


proc dumpFinalTCPStats {label starttime tcp tcpsink outputFile} {
  set f [open $outputFile a]
  set ns [Simulator instance]
  set bytesDel  [$tcpsink set numberBytesDelivered]
  set tmpTime [expr [$ns now] - $starttime] 
  set thruput [expr $bytesDel*8.0/$tmpTime + 0.0]
# puts "dumpFinalTCPStats: bytesDel is $bytesDel, tmpTime is $tmpTime and thruput is $thruput"
  set notimeouts   [expr [$tcp set nTimeouts_] + 0.0]
  set lossno [ expr [$tcp set nRexmits_] +  0.0]
  set drops [expr $lossno * 100]
  set arrivals [expr [$tcp set ack_] + $lossno + 0.0]
  if {$arrivals > 0} {
     set dropRate [expr $drops / $arrivals + 0.00]
   } else { 
     set dropRate 0
   }
   set toFreq 0.0
   if {$lossno > 0.0} {
#puts "notimeouts is $notimeouts and lossno is $lossno"
     set timeoutdrops [expr $notimeouts * 100]
     set toFreq [expr $timeoutdrops / $lossno + 0.00] 
   }
  set meanSampleCounter  [$tcp set FinalLossRTTMeanCounter]
  set TotalRTTSamples  [$tcp set FinalRTT]
  if {$meanSampleCounter > 0} {
    set meanRTT [expr $TotalRTTSamples / $meanSampleCounter + 0.00]
   } else { 
     set meanRTT 0
   }
#   puts "$label $bytesDel $arrivals $lossno $dropRate $meanRTT $notimeouts $toFreq $thruput  0 0 0"
#make the number of fields the same as with dumpFinalTFRC.... DO NOT CHANGE!!!! MATLAB needs this.
   puts $f "$label $bytesDel $arrivals $lossno $dropRate $notimeouts $toFreq $meanRTT $thruput 0 0 0"
#   puts [$f  format "%d %d %d %d %2.2f %d %d %3.6f %10d " $label $bytesDel $arrivals $lossno $dropRate $notimeouts $toFreq $meanRTT $thruput 0 0 0]
   flush $f
   close $f

}

proc dumpTFRCStats {label trfc trfcsink outputFile} {
  set f [open $outputFile a]
  set bytesDel  [$trfcsink set numberBytesDelivered]
  set notimeouts   [expr [$tcp set nTimeouts_] + 0.0]
  set lossno [ expr [$tcp set nRexmits_] +  0.0]
  set drops [expr $lossno * 100]
  set arrivals [expr [$tcp set ack_] + $lossno + 0.0]
  if {$arrivals > 0} {
     set dropRate [expr $drops / $arrivals + 0.00]
   } else { 
     set dropRate 0
   }
   set toFreq 0.0
   if {$lossno > 0.0} {
#puts "notimeouts is $notimeouts and lossno is $lossno"
     set timeoutdrops [expr $notimeouts * 100]
     set toFreq [expr $timeoutdrops / $lossno + 0.00] 
   }
#   puts "$label $bytesDel $arrivals $lossno $dropRate $notimeouts $toFreq"
   puts $f "$label $bytesDel $arrivals $lossno $dropRate $notimeouts $toFreq"
   flush $f
   close $f
}

proc dumpFinalTFRCStats {label starttime trfc trfcsink outputFile} {
  set ns [Simulator instance]
  set f [open $outputFile a]
  set bytesDel  [$trfcsink set numberBytesDelivered]
  set tmpTime [expr [$ns now] - $starttime] 
  set thruput [expr $bytesDel*8.0/$tmpTime]
# puts "dumpFinalTFRCStats: bytesDel is $bytesDel, tmpTime is $tmpTime and thruput is $thruput"

  set packetSent  [$trfc set totalPktsSent]
  set totalLosses  [$trfc set totalLosses]
  set totalLossEvents  [$trfc set totalLossEvents]

  set meanSampleCounter  [$trfc set FinalLossRTTMeanCounter]
  set TotalLossEventRateSamples  [$trfc set FinalLossEventRate]
  set TotalRTTSamples  [$trfc set FinalRTT]
  set numberTOs  [$trfc set totalTimeouts]

  if {$totalLosses > 0.0} {
#puts "notimeouts is $notimeouts and lossno is $lossno"
     set timeoutdrops [expr $numberTOs * 100]
     set toFreq [expr $timeoutdrops / $totalLosses + 0.00]
  }


  if {$meanSampleCounter > 0} {
    set meanLER [expr $TotalLossEventRateSamples / $meanSampleCounter + 0.00]
    set meanRTT [expr $TotalRTTSamples / $meanSampleCounter + 0.00]
   } else { 
     set meanLER 0
     set meanRTT 0
   }
  set meanLER [expr $meanLER * 100]
  set totalLossRate [expr [expr $totalLosses*1.0]*100/$packetSent + 0.0]
  set totalLossEventRate [expr [expr $totalLossEvents*1.0]*100/$packetSent + 0.0]
  
#   puts "$label $bytesDel $packetSent $totalLosses $totalLossRate $numberTOs $toFreq $totalLossEvents $totalLossEventRate $numberTOs $meanLER $meanRTT $thruput"
#   puts "$label $bytesDel $packetSent $totalLosses $totalLossRate $numberTOs $toFreq $meanRTT $thruput $totalLossEvents $totalLossEventRate $meanLER"
   puts $f "$label $bytesDel $packetSent $totalLosses $totalLossRate $numberTOs $toFreq  $meanRTT $thruput $totalLossEvents $totalLossEventRate $meanLER"
   flush $f
   close $f
}


proc printpkts { label tcp } {
	puts "tcp $label total_packets_acked [$tcp set ack_]"
}

proc printbytesdelivered { label tcpsink } {
	puts "tcpsink  $label bytes delvered   [$tcpsink set numberBytesDelivered]"
}

proc printtimeouts { label tcp } {
	puts "tcp $label total_timeouts  [$tcp set nTimeouts_]"
}
proc printretrans { label tcp } {
	puts "tcp $label total_retransmits  [$tcp set nRexmits_]"
        set lossno [$tcp set nRexmits_]
        set drops [expr $lossno * 100]
        set arrivals [expr [$tcp set ack_] + $lossno + 0.0]
	puts "tcp $label total packets sent  $arrivals"
        if {$arrivals > 0} {
          set dropRate [expr $drops / $arrivals + 0.00]
        } else { 
          set dropRate 0
        }
          puts "tcp $label pkt drop rate is $dropRate% "
}


#see ns-compat.tcl.  We do what linkhelper did as it caught link stats..
#in ns1.4, we used the link stats qmon support.
#here, the caller passes us a qmon object, we can reequest any
#of the info described by  the QueueMonitor class
proc printdrops { linkid qmon } {
    puts "Summary of qmon drops : "
    set drops [$qmon set pdrops_]
    set drops [expr $drops * 100 + 0.00]
    set arrivals [$qmon set parrivals_]
    if {$arrivals > 0} {
      set dropRate [expr $drops / $arrivals + 0.00]
    }
    puts "link $linkid  pkt drop rate is $dropRate% "
    puts "link $linkid total_drops [$qmon set pdrops_] "
    puts "link $linkid total_packets that arrived [$qmon set parrivals_]"
    puts "link $linkid total bytes that arrived [$qmon set barrivals_]"
    puts "link $linkid total bytes currently in queue  [$qmon set size_]"
    puts "link $linkid total packets currently in queue  [$qmon set pkts_]"
 
    puts "link $linkid total packets that departed  [$qmon set pdepartures_]"

}

proc TraceQueueLossRate { ns label qmon interval fname} {

  proc queuelossrate {ns mylabel myqmon myinterval fname } {
    set f [open $fname a]
    $ns at [expr [$ns now] + $myinterval] "queuelossrate $ns $mylabel $myqmon $myinterval $fname"

    set drops [$myqmon set pdrops_]
    set drops [expr $drops * 100 + 0.00]
    set arrivals [$myqmon set parrivals_]
    set dropRate 0
    if {$arrivals > 0} {
      set dropRate [expr $drops / $arrivals + 0.00]
    }


    $myqmon set pdrops_ 0
    $myqmon set bdrops_ 0
    $myqmon set parrivals_ 0
    $myqmon set barrivals_ 0
    puts $f "[$ns now]  $dropRate"
    set drops [$myqmon set pdrops_]

    close $f
  }
  $ns at 0.0 "queuelossrate $ns $label $qmon  $interval $fname"
}

#A better version of TraceQueueLossRate 
#This dumps the loss rate, the queue level over time
proc TraceQueueStats { ns label qmon interval fname} {

  proc queuestats {ns mylabel myqmon myinterval fname } {
    set f [open $fname a]
    $ns at [expr [$ns now] + $myinterval] "queuestats $ns $mylabel $myqmon $myinterval $fname"

    set size [$myqmon set size_]
    set drops [$myqmon set pdrops_]
    set drops [expr $drops * 100 + 0.00]
    set arrivals [$myqmon set parrivals_]
    set dropRate 0
    if {$arrivals > 0} {
      set dropRate [expr $drops / $arrivals + 0.00]
    }


    $myqmon set pdrops_ 0
    $myqmon set bdrops_ 0
    $myqmon set parrivals_ 0
    $myqmon set barrivals_ 0
    puts $f "[$ns now]  $dropRate $size"
    set drops [$myqmon set pdrops_]

    close $f
  }
  $ns at 0.0 "queuestats $ns $label $qmon  $interval $fname"
}



proc printutilization { linkid qmon linkspeed duration packetsize} {

    set departures [$qmon set pdepartures_]
    set maxdeparts [expr $linkspeed *  $duration / $packetsize + 0.0]
puts "departures = $departures"
puts "maxdeparts = $maxdeparts"
    set util [expr $departures * 100 + 0.00]
    set util [expr $util / $maxdeparts +  0.00]

    puts "link $linkid link utilization is $util% "
}

proc printstop { stoptime } {
	puts "stop-time $stoptime"
}


#
# Dump the queueing delay on the n0->n1 link
# to stdout every second of simulation time.
# Note:  This is not correct-  I want to be able to obtain the
#        avg packet waiting time and the link utilization.... TODO
#
proc dumpqueuedelay { ns qmon integ link interval } {
	$ns at [expr [$ns now] + $interval] "dumpqueuedelay $ns $qmon $integ $link $interval"
	set delay [expr 8 * [$integ set sum_] / [[$link link] set bandwidth_]]
	puts "[$ns now] delay=$delay"

        set sample [$qmon set pkts_]
        puts "dumpqueuedelay:  pkts size is $sample "
}



proc startPingProcess { ns pinger interval} {

  proc doPing {ns  mypinger myinterval} {
    global totalPings totalPingDrops

     

#      puts "doPing: rn:$randnumber;  myinterval: $myinterval;  total sent:  $totalPings;  total dropped: $totalPingDrops"

#    set $totalPings [expr $totalPings + 1.0]
    incr totalPings
#     puts "doPing:  total sent:  $totalPings;  total dropped: $totalPingDrops"


    $mypinger send
#add randomness
    $ns at [expr [$ns now]  + $myinterval + [uniform .001 .009]] "doPing $ns $mypinger  $myinterval"
  }
  $ns at 0.0 "doPing $ns $pinger $interval"
}


#See code change in tcp.cc  traceVar to dump just time and rtt
proc setupTcpTracing { tcp tcptrace { sessionFlag false } } {
	if {$tcptrace == 0} {
		return
	}
	enableTcpTracing $tcp $tcptrace
}

proc enableTcpTracing { tcp tcptrace } {
	if {$tcptrace == 0} {
		return
	}
	$tcp attach $tcptrace
	$tcp trace "tcpRTT_" 
#	$tcp trace "t_seqno_" 
#	$tcp trace "rtt_" 
#	$tcp trace "srtt_" 
#	$tcp trace "rttvar_" 
#	$tcp trace "backoff_" 
#	$tcp trace "dupacks_" 
#	$tcp trace "ack_" 
	$tcp trace "cwnd_"
#	$tcp trace "ssthresh_" 
#	$tcp trace "maxseq_" 
#	$tcp trace "seqno_"
#	$tcp trace "exact_srtt_"
#	$tcp trace "avg_win_"
#	$tcp trace "nrexmit_"
}

####################################################################################
#function : BuildTCPExpCxs
#           Modified from BuildExpCxs
#
####################################################################################
proc BuildTCPExpCxs { ns  SrcNode  DestNode Number burstTime idleTime burstRate numberID starttime stoptime window protID} {
 set printtime $stoptime
 for {set i 1} {$i < [expr $Number+1]} {incr i} {
  set newFileOut "TCPEXP[expr $numberID+$i-1].out"
  exec rm -f $newFileOut
  puts "Create EXP TCP Cx #  [expr $i+$numberID-1] (file $newFileOut)"

#PROTID
if { $protID == 1 } {
  set tcp [new Agent/TCP/Reno]
}
if { $protID == 2 } {
   set tcp [new Agent/TCP/Newreno]
}
if { $protID == 3 } {
  set tcp [new Agent/TCP/Vegas]
}
if { $protID == 4 } {
  set tcp [new Agent/TCP/Vegas1]
}
if { $protID == 5 } {
  set tcp [new Agent/TCP/DCA]
}
if { $protID == 6 } {
  set tcp [new Agent/TCP/Dual]
}
if { $protID == 7 } {
  set tcp [new Agent/TCP/Reno]
#RED
  $tcp set ecn_ 1
}
set sink [new Agent/TCPSink/DelAck]
#  set sink [new Agent/TCPSink]
  $tcp set fid_  [expr $i+$numberID]
  $sink set fid_ [expr $i+$numberID]
  $sink set sinkId_ [expr $i+$numberID]
  $ns attach-agent $SrcNode $tcp
  $ns attach-agent $DestNode $sink
  $ns connect $tcp $sink

  $tcp set packetSize_ 1460
  $tcp set maxcwnd_ $window
  $tcp set window_ $window
  $tcp set cxId_ [expr $i+$numberID]
  $tcp set overhead_ 0.000020

#  set ftp [$tcp attach-app FTP]    
#  $ns at 0.2 "$ftp start"

  set exp [new Application/Traffic/Exponential]
  $exp attach-agent $tcp
  $exp set packetSize_ 1460
  $exp set burst_time_ $burstTime
  $exp set idle_time_ $idleTime
  $exp set rate_ $burstRate
  $ns at $starttime "$exp start"

#to create a tcp throughput file ..
  TraceThroughput $ns $sink  1.0 $newFileOut
  $ns at $printtime "dumpFinalTCPStats  [expr $i+$numberID] $starttime $tcp  $sink TCPstats.out"
 }
}

####################################################################################
#function : BuildAPFECCxs
#  trafficMode:  0:CBR, 1:EXP; 2:PARETO; 3:Trace file
####################################################################################
proc BuildAPFECCxs { ns  SrcNode  DestNode Number N_param K_param packetSize trafficMode interval burstTime idleTime burstRate numberID starttime stoptime} {

 set printtime $stoptime

 for {set i 0} {$i < $Number} {incr i} {
  set Cxid [expr $numberID+$i]
  set newFileOut "UDPCBR[expr $Cxid + 0].out"

#  set newFileOut "APFEC[expr $numberID+$i].out"
  exec rm -f $newFileOut
  puts "Create APFEC UDP Cx #  [expr $i+$numberID] (file $newFileOut)"

  set udp [new Agent/APFECSOURCE]
  set sink [new Agent/APFECSINK]
  set flowID [expr $i+$numberID]
  $sink set SinkId_  $flowID
  $udp set class_ 2
  $sink set fid_ $flowID
  $udp set cxId_ $flowID
# set  largest allowed message size to fit in one IP packet
  $udp set packetSize_ 1460
#  $udp set packetSize_ $packetSize
  $udp set N_ $N_param
  $udp set K_ $K_param
  $udp initstream $flowID $N_param $K_param
  $sink initstream $flowID $N_param $K_param
  $ns attach-agent $SrcNode $udp
  $ns attach-agent $DestNode $sink
  $ns connect $udp $sink
  $ns at $stoptime "$udp dumpstats APFECSOURCEstats.out"
  $ns at $stoptime "$sink dumpstats APFECSINKstats.out"
  #to create a throughput file ..
  puts " ..... schedule dumpFinalUDPStats for $stoptime"
  UDPTraceThroughput $ns $sink 1.0 $newFileOut
  $ns at $stoptime "dumpFinalUDPStats  $Cxid  $starttime $sink UDPstats.out"

  if { $trafficMode == 0 } {
    set cbr [new Application/Traffic/CBR]
    $cbr attach-agent $udp
    $cbr set packetSize_ $packetSize
    $cbr set interval_ $interval
    $cbr set mode_ 0

    $ns at $starttime "$cbr start"
  }
  if { $trafficMode == 1 } {
  set exp [new Application/Traffic/Exponential]
  $exp attach-agent $udp
  $exp set packetSize_ $packetSize
  $exp set burst_time_ $burstTime
  $exp set idle_time_ $idleTime
  $exp set rate_ $burstRate
  $ns at $starttime "$exp start"
 }
}

}

####################################################################################
#function : BuildUDPExpCxs
#Modified by Bo
####################################################################################
proc BuildUDPExpCxs { ns  SrcNode  DestNode Number packetSize burstTime idleTime burstRate numberID starttime stoptime} {

 set printtime $stoptime


 for {set i 1} {$i < [expr $Number+1]} {incr i} {
  set newFileOut "UDPEXP[expr $numberID+$i-1].out"
  exec rm -f $newFileOut
  puts "Old Create EXP UDP Cx #  [expr $i+$numberID-1] (file $newFileOut)"

  set udp [new Agent/UDP]
  set sink [new Agent/UDPSink]
  $sink set SinkId_  [expr $i+$numberID]
  $udp set class_ 2
  $sink set fid_ [expr $i+$numberID]
  $udp set cxId_ [expr $i+$numberID]
  $udp set packetSize_ 1460
  $ns attach-agent $SrcNode $udp
  $ns attach-agent $DestNode $sink
  $ns connect $udp $sink

  set exp [new Application/Traffic/Exponential]
  $exp attach-agent $udp
  $exp set packetSize_ $packetSize
  $exp set burst_time_ $burstTime
  $exp set idle_time_ $idleTime
  $exp set rate_ $burstRate
  $ns at $starttime "$exp start"
#to create a throughput file ..
  puts " ..... schedule dumpFinalUDPStats for $stoptime"
  UDPTraceThroughput $ns $sink 1.0 $newFileOut
  $ns at $stoptime "dumpFinalUDPStats  [expr $i+$numberID] $starttime $sink UDPstats.out"
 }

}

####################################################################################
#function : BuildUDPParetoCxs
####################################################################################
proc BuildUDPParetoCxs { ns  SrcNode  DestNode Number packetSize burstTime idleTime burstRate shape numberID starttime stoptime} {

 set printtime $stoptime

 for {set i 1} {$i < [expr $Number+1]} {incr i} {
  set newFileOut "UDPPARETO[expr $numberID+$i-1].out"
  exec rm -f $newFileOut
  puts "Old Create PARETO UDP Cx #  [expr $i+$numberID-1] (file $newFileOut)"

  set udp [new Agent/UDP]
  set sink [new Agent/UDPSink]
  $sink set SinkId_  [expr $i+$numberID]
  $udp set class_ 2
  $sink set fid_ [expr $i+$numberID]
  $udp set cxId_ [expr $i+$numberID]
  $udp set packetSize_ 1460
  $ns attach-agent $SrcNode $udp
  $ns attach-agent $DestNode $sink
  $ns connect $udp $sink

 set gen [new Application/Traffic/Pareto]
 $gen attach-agent $udp
 $gen set packetSize_ $packetSize
 $gen set burst_time_ $burstTime
 $gen set idle_time_ $idleTime
 $gen set rate_  $burstRate
 $gen set shape_  $shape
 $ns at $starttime "$gen start"
#to create a throughput file ..
 UDPTraceThroughput $ns $sink 1.0 $newFileOut
 $ns at $stoptime "dumpFinalUDPStats  [expr $i+$numberID] $starttime $sink UDPstats.out"

 }
}


####################################################################################
#function : BuildUDPCBRCxs
####################################################################################
proc BuildUDPCBRCxs { ns  SrcNode  DestNode Number packetSizeParam intervalParam numberID starttime stoptime} {

 set printtime $stoptime


 for {set i 1} {$i < [expr $Number+1]} {incr i} {
  set Cxid [expr $numberID+$i-1]
  set newFileOut "UDPCBR[expr $Cxid + 0].out"
  exec rm -f $newFileOut
  puts "Create CBRUDP Cx #  $Cxid  (file $newFileOut), packetSize:$packetSizeParam, interval:$intervalParam, starttime:$starttime"

  set udp [new Agent/UDP]
  set sink [new Agent/UDPSink]
  $sink set SinkId_  $Cxid
  $udp set class_ 2
  $sink set fid_ $Cxid
  $udp set cxId_ $Cxid
  $udp set packetSize_ 1460
  $ns attach-agent $SrcNode $udp
  $ns attach-agent $DestNode $sink
  $ns connect $udp $sink

  set cbr [new Application/Traffic/CBR]
  $cbr attach-agent $udp
#roughly 1.5Mbps
  $cbr set packetSize_ $packetSizeParam
  $cbr set interval_ $intervalParam
#  $cbr set packetSize_ 500
#  $cbr set interval_ 0.008
  $cbr set mode_ 0

  $ns at $starttime "$cbr start"
#to create a throughput file ..
  UDPTraceThroughput $ns $sink 1.0 $newFileOut
  $ns at $stoptime "dumpFinalUDPStats  $Cxid $starttime $sink UDPstats.out"
 }

}


####################################################################################
#function : BuildUDPCxWithMode
#
# explanation: this routine builds and starts a TCP connection between the src and destnode
#
# inputs:  we summarize the input params as folllows:
#
#  ns  
#  SrcNode  
#  DestNode 
#  Number  : this routine will create any number of TCP cx's 
#  burstTime  : pareto param
#  idleTime  : pareto param
#  burstRate  : pareto param
#  numberID :  a unique Cx id
#  stoptime : 
#  thruHandle :  a file handle
#  pshape  : the pareto shape param
#  protID :  specifies the type of TCP protocol to use
#
#  mode:  specifies the traffic generator to be used
#   0: exponential
#   1: CBR
#   2: FTP
#   3: Pareto
#   4:
####################################################################################
proc BuildUDPCxWithMode { ns  SrcNode  DestNode Number packetSize burstTime idleTime intervalTime burstRate numberID starttime stoptime thruHandle statsHandle pshape mode priority} {

global TURN_ON_THROUGHPUT_MONITORS

if { $TURN_ON_THROUGHPUT_MONITORS == 1} {
 exec rm -f $thruHandle
}

 set printtime $stoptime

puts "BuildUDPCxWithMode: Create $Number UDP Connections, starting CxID:$numberID, mode:$mode and startime:$starttime stoptime:$stoptime, priority is $priority"

 for {set i 0} {$i < [expr $Number+0]} {incr i} {

  set udp [new Agent/UDP]
  set sink [new Agent/UDPSink]
  set myCXID [expr $i +  $numberID]
  $sink set SinkId_  $myCXID
  $udp set class_ 2
  $sink set fid_  $myCXID
  $udp set cxId_ $myCXID
  $udp set packetSize_ $packetSize
  $sink set prio_ $priority
  $udp set prio_ $priority
#  $udp set packetSize_ 1460
  $ns attach-agent $SrcNode $udp
  $ns attach-agent $DestNode $sink
  $ns connect $udp $sink


   if {$mode == 0} {
      set exp [new Application/Traffic/Exponential]
      $exp attach-agent $udp
  #  $exp set packetSize_ 1460
      $exp set packetSize_ $packetSize
      $exp set burst_time_ $burstTime
      $exp set idle_time_ $idleTime
      $exp set rate_ $burstRate
      puts "Start an EXP UDP Cx  cxId:$myCXID, burst rate:$burstRate"
      $ns at $starttime "$exp start"
      $ns at $stoptime "$exp stop"
    } elseif {$mode == 1} {
      set cbr [new Application/Traffic/CBR]
      $cbr attach-agent $udp
      $cbr set packetSize_ $packetSize
      $cbr set interval_ $intervalTime
      if { $myCXID == 12} {
         $cbr set mode_ 1
         puts "Create CBRUDP Cx #  $myCXID (file $thruHandle), mode_ 1, packetSize:$packetSize, interval:$intervalTime, starttime:$starttime"
      } else {
        $cbr set mode_ 0
        puts "Create CBRUDP Cx #  $myCXID (file $thruHandle), mode_ 0, packetSize:$packetSize, interval:$intervalTime, starttime:$starttime"
      }

      puts "Start a CBR UDP Cx  cxId:$myCXID, interval:$intervalTime"
      $ns at $starttime "$cbr start"
      $ns at $stoptime "$cbr stop"
    } elseif {$mode == 2} {
       puts " Attach an FTP generator -HARD ERROR  DOES NOT APPLY TO UDP "
    } elseif {$mode == 3} {
      set gen [new Application/Traffic/Pareto]
      $gen attach-agent $udp
      $gen set packetSize_ $packetSize
      $gen set burst_time_  $burstTime
      $gen set idle_time_ $idleTime
      $gen set rate_ $burstRate
      $gen set shape_  $pshape

      puts " Attach a pareto generator and start it a $starttime"
      $ns at $starttime  "$gen start"
      $ns at $stoptime  "$gen stop"
    } else {
      puts " BuildUDPCxWithMode:  HARD ERROR , bad mode: $mode"
    }

#    if { $myCXID == 13} {
       puts "Stop  the UDP Cx  cxId:$myCXID at $stoptime]"
       $ns at $stoptime  "$cbr stop"
#     }
#to create a throughput file ..
  if { $TURN_ON_THROUGHPUT_MONITORS == 1} {
    UDPTraceThroughput $ns $sink 1.0 $thruHandle
  }
  if { $TURN_ON_THROUGHPUT_MONITORS == 0} {
    exec rm -f $thruHandle
    if { $myCXID == 5121} {
     UDPTraceThroughput $ns $sink  1.0 $thruHandle
    }
    if { $myCXID == 5122} {
      UDPTraceThroughput $ns $sink  1.0 $thruHandle
    }
  }


  $ns at $stoptime "dumpFinalUDPStats  $myCXID $starttime $sink $statsHandle"
}
}



####################################################################################
#function : BuildUDPCBRMulticast
####################################################################################
proc BuildUDPCBRCMulticast { ns  SrcNode  DestNode Number packetSizeParam intervalParam numberID multicastID portID starttime stoptime trafficType N_param K_param} {

 set printtime $stoptime


 for {set i 0} {$i < $Number} {incr i} {

  set Cxid [expr $numberID+$i]
  set newFileOut "UDPCBR[expr $Cxid + 0].out"

  exec rm -f $newFileOut
  puts "Create CBRUDP Cx #  NodeID:$numberID $Cxid, MulticastID: $multicastID, (file $newFileOut), packetSize:$packetSizeParam, interval:$intervalParam, starttime:$starttime trafficType:$trafficType"

  if {$trafficType == 0} {
  set udp [new Agent/UDP]
  set sink [new Agent/UDPSink]
  set fname "UDPstats.out"
  } elseif {$trafficType == 3} {
    set udp [new Agent/APFECSOURCE]
    set sink [new Agent/APFECSINK]
    $udp set N_ $N_param
    $udp set K_ $K_param
    $udp initstream $Cxid $N_param $K_param
    $sink initstream $Cxid $N_param $K_param
#    set fname "APFECstats.out"
    set fname "UDPstats.out"
    $ns at $stoptime "$udp dumpstats APFECSOURCEstats.out"
    $ns at $stoptime "$sink dumpstats APFECSINKstats.out"
  } else {
    puts "Bad UDP_TRAFFIC value ?? ($UDP_TRAFFIC) "
    exit 0
  }
  $sink set SinkId_  $Cxid
  $udp set class_ 2
  $sink set fid_  $Cxid
  $udp set fid_ $Cxid
  $udp set multicast_id_ $multicastID
  $sink set multicast_id_ $multicastID
  #$udp set dst_port_ $portID
  #$sink set dst_port_ $portID
  $udp set cxId_ $Cxid
  $udp set packetSize_ 1460
  $ns attach-agent $SrcNode $udp
  $ns attach-agent $DestNode $sink
  $ns connect $udp $sink

  $DestNode joinMulticastGroup $multicastID

  set cbr [new Application/Traffic/CBR]
  $cbr attach-agent $udp
#roughly 1.5Mbps
  $cbr set packetSize_ $packetSizeParam
  $cbr set interval_ $intervalParam
#  $cbr set packetSize_ 500
#  $cbr set interval_ 0.008
  $cbr set mode_ 0
  $cbr set sport_ $portID
  $cbr set dport_ $portID

  $ns at $starttime "$cbr start"
#to create a throughput file ..
  UDPTraceThroughput $ns $sink 1.0 $newFileOut
  $ns at $stoptime "dumpFinalUDPStats  $Cxid  $starttime $sink $fname"
 }
}


#########################################################
# procedure: proc BuildUDPMulticastSink { ns  SrcNode  DestNode Number numberID multicastID portID starttime stoptime trafficType} {
#    This effectively adds nodes to a multicast UDP connection.  
#  
# inputs:
#   ns
#   SrcNode
#   DestNode
#   Number   : The number of flows to add
#   numberID  : the base cx id
#   multicastID : the multicast ID that is common to all flows
#   portID :  The destination port that is common to all flows
#   starttime 
#   stoptime 
#   trafficType :  UDP_TRAFFIC traffic types :  value 0: CBR   value 1 EXP  value 2 Pareto #   Value 3 will be APFEC
#
#########################################################
proc BuildUDPMulticastSink { ns  SrcNode  DestNode Number numberID multicastID portID starttime stoptime trafficType N_param K_param} {

 for {set i 0} {$i < $Number} {incr i} {
  set Cxid [expr $numberID+$i]
  set newFileOut "UDPCBR[expr $Cxid + 0].out"
  exec rm -f $newFileOut
  puts "Create CBRUDP SINK Cx: node number:$numberID  #  $Cxid (file $newFileOut), starttime: $starttime,  stoptime: $stoptime, traffictype: $trafficType"

  #if CBR
  if {$trafficType == 0} {
    set udp [new Agent/UDP]
    set sink [new Agent/UDPSink]
    set fname "UDPstats.out"
  } elseif {$trafficType == 3} {
    set udp [new Agent/APFECSOURCE]
    set sink [new Agent/APFECSINK]
    $udp set N_ $N_param
    $udp set K_ $K_param
    $udp initstream $Cxid $N_param $K_param
    $sink initstream $Cxid $N_param $K_param
#    set fname "APFECstats.out"
    set fname "UDPstats.out"
    $ns at $stoptime "$udp dumpstats APFECSOURCEstats.out"
    $ns at $stoptime "$sink dumpstats APFECSINKstats.out"

  } else {
    puts "Bad UDP_TRAFFIC value ?? ($UDP_TRAFFIC) "
    exit 0
  }
  $sink set SinkId_  $Cxid
  $udp set class_ 2
  $sink set fid_ $Cxid
  $udp set fid_ $Cxid
  $udp set cxId_ $Cxid
  $udp set multicast_id_ $multicastID
  $sink set multicast_id_ $multicastID
  #$udp set dst_port_ $portID
  #$sink set dst_port_ $portID

  $ns attach-agent $SrcNode $udp
  $ns attach-agent $DestNode $sink
  $ns connect $udp $sink

#to create a throughput file ..
  UDPTraceThroughput $ns $sink 1.0 $newFileOut
  $ns at $stoptime "dumpFinalUDPStats  $Cxid $starttime $sink $fname"
 }

}

#########################################################
# Returns the local link that leads to
# the next hop node with the passed
# node address parameter. If no link to
# the given node exists, the procedure
# returns -1.
#########################################################
Node instproc nexthop2link { nexthop } {
        #$self instvar link_
        set ns_ [Simulator instance]
        foreach {index link} [$ns_ array get link_] {
                set L [split $index :]
                set src [lindex $L 0]
                if {$src == [$self id]} {
                        set dst [lindex $L 1]
                        if {$dst == $nexthop} {
                                # Cost Debug
                                #puts "Src:$src Dst:$dst Link:$link"
                                #puts "[$link info class]"
                                # End Cost Debug
                                return $link
                        }
                }
        }
        return -1
}


#########################################################
# This procedure is used to add explicitly
# routes to a node, overriding the routing
# policy used (e.g. shortest path routing).
# Tested currently with static ns2 routing.
# Essentially, it is used to add policy-routing
# entries in realistic network topologies.
#
# Parameters:
#
# node: the ns2 node, to which the route
#      entry is added to. This parameter
#      is of type Node.
# dst: the destination, to which the route
#     entry refers to. This parameter
#      is of type Node.
# via: the next hope node, that the local node
#     will use to access the destination node.
#     This parameter is of type Node.
#
##########################################################
proc addExplicitRoute {node dst via } {
        set link2via [$node nexthop2link [$via node-addr]]
        if {$link2via != -1} {
                $node add-route [$dst node-addr] [$link2via head]
        } else {
                puts "Warning: No link exists between node [$node node-addr] and [$via node-addr]. Explicit route not added."
        }
}

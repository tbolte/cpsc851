#######################
#DOCSIS helper TCL routines
#
# Revisions
#
########################


####################################################################################
#function : TraceCMBW
# 
#  Calls DOCSIS CM method to get and reset bytes sent and received
#   and computes the throughput. This calls 
#          $mac dump-BW-cm $CMnode $fname   (see vlan.tcl)
#   which will call the CM objects dumpBWCM method (in mac-docsiscm.cc).
#
#  Dumps the throughput in bits per second as shown below
#   timestamp   BW upstream(bps)BW downstream(bps)      
#     0.000000    0.00    	0.00
#     1.000000    0.00  	215568.00
#     2.000000  1856928.00  	206216.00
#     3.000000  3232304.00  	201784.00
#     4.000000  3256752.00  	201080.00
#
####################################################################################
proc TraceCMBW {mac ns CMnode interval fname } {
#puts "We will open TraceCMBW file $fname"

	proc ThroughputCMdump { mac ns CMnode interval fname } {
#          puts "TraceCMBW:  Invoke ... [$ns now]"
          $mac dump-BW-cm $CMnode $fname
          $ns at [expr [$ns now] + $interval] "ThroughputCMdump $mac $ns $CMnode $interval $fname"
	}
	$ns at 0.0 "ThroughputCMdump $mac $ns $CMnode $interval $fname"
}


####################################################################################
#function : TraceCMTSBW
# 
#  Calls DOCSIS CMTS method to get and reset bytes sent and received
#   and computes the throughput. This calls 
#          $mac dump-BW-cmts $CMTSnode $fname   (see vlan.tcl)
#   which will call the CMTS objects dumpBWCMTS method (in mac-docsiscmts.cc).
#
#  Dumps the throughput in bytes per second as shown below
#   timestamp   BW upstream(bps)BW downstream(bps)      
#     0.000000    0.00    	0.00
#
#  Dumps the throughput in bits per second
#
####################################################################################
proc TraceCMTSBW {mac ns CMTSnode interval fname } {
#puts "We will open TraceCMTSBW file $fname"

	proc ThroughputCMTSdump { mac ns CMTSnode interval fname } {
#          puts "TraceCMTSBW:  Invoke ... [$ns now]"
#note: see mac-docsiscmts.cc,  the fname will be param 2
          $mac dump-BW-cmts $CMTSnode $fname
          $ns at [expr [$ns now] + $interval] "ThroughputCMTSdump $mac $ns $CMTSnode $interval $fname"
	}
	$ns at 0.0 "ThroughputCMTSdump $mac $ns $CMTSnode $interval $fname"
}

####################################################################################
#function : dumpCMStats
#  explanation:  Calls DOCSIS CM method to print stats.  Can be called periodically
#                during a simulation.  But typically will be called once at the end
#                of the simulation run.
#                It is invoked as follows:
#			$ns at $printtime "dumpCMStats $p $ns $n1 log.out"
#                Eventually this calls the CM objects dumpFinalCMStats method.
# Output:
# 	The following lines are produced with each invocation:
#		dumpFinalCMStats(10.100000) Total drops: 0;  loss rate: 0.000000 percent
#                     Total Packets received downstream: 4495, Total Packets sent upstream: 2314
#                     Total Bytes received downstream: 255768, Total Bytes sent upstream: 3530394
#                     Total Collisions: 0, Total Fragments: 0
#
#
####################################################################################
proc dumpCMStats {mac ns CMnode fname } {

 $mac dump-final-cm-stats $CMnode $fname
}

####################################################################################
#function : dumpCMTSStats
#
#  explanation:  Calls DOCSIS CMTS method to print stats.  Can be called periodically
#                during a simulation.  But typically will be called once at the end
#                of the simulation run.
#                It is invoked as follows:
#			$ns at $printtime "dumpCMTSStats $p $ns $n1 log.out"
#                Eventually this calls the CMTS objects dumpFinalCMTSStats method.
# Output:
# 	The following lines are produced with each invocation:
#
#	dumpFinalCMTSStats(10.100000) Total drops in DS: 0;  loss rate: 0.000000 percent
#                       Total Packets sent downstream: 4495, Total Packets received upstream: 3464
#                       Total Bytes sent downstream: 255768, Total Bytes received upstream: 3527338
#                       downstream Util: 0.668 percent, upstream Util: 54.569 percent
#
# 
#
####################################################################################
proc dumpCMTSStats {mac ns CMTSnode fname } {

  $mac dump-final-cmts-stats $CMTSnode $fname
}

####################################################################################
#function : TraceDOCSISQueue
#  Invoked as :
#	TraceDOCSISQueue $p $ns $n0 1.0 CMTS-DS-QUEUE.out $DOWNSPEED
# 
#  Calls DOCSIS CM or CMTS method to dumpCMQueueStats
#  Note that the cm 'cc' code will see the params as
#     2 fname 
#     3 dataRate

#
#  The output is as follows:
#     timestamp 	max_qnp_ 	min_qnp_ 	qnp_  	util
#	0.000000  	0  		0 		0  	0.000
#	1.000000  	0  		0 		0  	0.712
#	2.000000  	1  		0 		1  	0.679
#	3.000000  	1  		1 		1	0.665
#
#   max_qnp_:  During the current interval, this is the maximum queue size in packets
#   min_qnp_:  During the current interval, this is the minimum queue size in packets
#   qnp_    :  Current number of packets in the queue
#   util:      During the current interval, we compute the utilization of the channel.  We
#              divide the total number of bytes sent by the total number of
#		bytes that could possibly be sent (based on the channel capacity).  
#
#  NOTE:      This is only implemented for the CMTS downstream queue.  
# 		For the upstream, we would have to implement a queue monitor for each SID queue
#               since a CM will has a separate queue for each service ID.
#              This is TODO!!!
#
####################################################################################
proc TraceDOCSISQueue {mac ns CMnode interval fname } {
#           puts "TraceDOCSISQueue: open using file $fname "

	proc internalTraceDOCSISQueue { mac ns CMnode interval fname } {
#           puts "TraceDOCSISQueue: here with "
          $mac dump-docsis-queue-stats $CMnode $fname 
          $ns at [expr [$ns now] + $interval] "internalTraceDOCSISQueue $mac $ns $CMnode $interval $fname "
	}
	$ns at 0.0 "internalTraceDOCSISQueue $mac $ns $CMnode $interval $fname"
}


####################################################################################
#function : TraceDOCSISUtilization
#
# Invoked as:
# 	TraceDOCSISUtilization $p $ns $n0 1.0 CMTS-DS-util.out $DOWNSPEED $UPSPEED
# 
#  Calls DOCSIS CM or CMTS method to dumpCMUtilStats
#  Note that the cm 'cc' code will see the params as
#     2 fname 
#     3 dataRate
# 
# Output:
#       timestamp  downstream util    upstream util
#	0.000000 	0.000 		0.000
#	1.000000 	0.000 		0.000
#	2.000000 	0.000 		0.358
#	3.000000 	0.000 		0.994

#
####################################################################################
proc TraceDOCSISUtilization {mac ns CMnode interval fname DSdataRate USdataRate} {
#           puts "TraceDOCSISUtilization: open using file $fname and USdataRate $USdataRate, DSdataRate: $DSdataRate"

	proc internalTraceDOCSISUtil { mac ns CMnode interval fname DSdataRate USdataRate } {
          $mac dump-docsis-util-stats $CMnode $fname $DSdataRate $USdataRate
          $ns at [expr [$ns now] + $interval] "internalTraceDOCSISUtil $mac $ns $CMnode $interval $fname $DSdataRate $USdataRate"
	}
	$ns at 0.0 "internalTraceDOCSISUtil $mac $ns $CMnode $interval $fname $DSdataRate $USdataRate"
}

####################################################################################
#function : create_CMs
# 
#  This creates a set of CM's
#
# Inputs:
#
# Outputs: creates the global n which will hold each node.
#
####################################################################################
proc create_CMs { ns  baseNode numberCMs trafficPattern} {

global n

 for {set i 0} {$i < [expr $numberCMs]} {incr i} {
  puts "Create CM link #  [expr $i] "
  set n($i) [$ns node]
#  set newNode [$ns node]
#  $ns duplex-link $baseNode $newNode 1.5Mb 5ms DropTail
 }
}


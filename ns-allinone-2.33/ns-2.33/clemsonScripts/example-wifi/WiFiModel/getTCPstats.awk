#Change the number in the third paran to indicate the column in the data file to operate upon.
#If the columns are separated with tabs, change the first paran to "\t"
BEGIN { 
  FS = " "
  delayDS = 0;
  lossRateDS = 0;
  thruputDS = 0;
  smallThru = 10000000000;
  bigThru = 0;
  excludeFlowID = 222222;
} 
{
cxID = $1;
if ((excludeFlowID == 0)  || (cxID !=  excludeFlowID)) 
{
    x++;
    DS++;
    delayDS=delayDS+$8;
    lossRateDS=lossRateDS+$5;
    thruputDS=thruputDS+$9;
    if (bigThru < $9) {
      if ($9 > 0) {
        bigThru = $9;
      }
    }
    if (smallThru > $9) {
      if ($9 > 0) {
        smallThru = $9;
      }
    }
}
}
END {
printf("number Flows: (not includeing flow %d)  %d\n",excludeFlowID, x);
print "avg RTT: " delayDS/DS;
print "avg loss rate (percentage): " lossRateDS/DS; 
printf("avg thruput: %d, minThru:%d, maxThru:%d\n",thruputDS/DS,smallThru,bigThru);
printf("%d %3.3f %3.3f, %d, %d, %d\n",x,delayDS/DS,lossRateDS/DS,thruputDS/DS,smallThru,bigThru);
}

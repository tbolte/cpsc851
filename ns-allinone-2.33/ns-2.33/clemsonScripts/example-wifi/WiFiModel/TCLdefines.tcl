

#For 802.11 multicast extensions
set mobileNode           0
set baseStation          1


#Note:must agree with simsched.tcl and C++ code in globalDefines.h
#scheduling Q Types
set FIFOQ		0
set OrderedQ	1
set RedQ		2	
set BlueQ		3	
set AdaptiveRedQ 4
set DelayBasedRedQ 5
set AQM3Q		6
set BAQM		10


#scheduling disciplines
set FCFS	0
set RR		1
set WRR		2
set DRR		3
set SCFQ    4
set WFQ		5
set W2FQ	6

set HDRR    16
set	HOSTBASED 17
set TSNTS   18
set FAIRSHARE 19

set globalDRR 32
set channelDRR 33
set bondingGroupDRR 34
set bestDRR 35

set globalSCFQ 64
set channelSCFQ 65
set bondingGroupSCFQ 66
set bestSCFQ 67

#TS/NTS
set PRIORITY_HIGH 0
set PRIORITY_LOW 1

#FAIRSHARE
set PRIORITY_WELL_BEHAVED 0
set PRIORITY_NOT_WELL_BEHAVED 1

set FlowPriorityLOW -1
set FlowPriority0 0
set FlowPriority1 1
set FlowPriority2 2
set FlowPriority3 3
set FlowPriority4 4

set FlowQuantum0  3000
set FlowQuantum1  2000
set FlowQuantum2  1020
set FlowQuantum3  1000
set FlowQuantum4  500
set FlowQuantum5  100

set FlowWeightLP  .01
set FlowWeightHP  .002
set FlowWeight1  1.0
set FlowWeight2  0.9
set FlowWeight3  0.7
set FlowWeight4  0.5
set FlowWeight5  0.3
set FlowWeight6  0.1

set MAXp 0.10

set BG0 0
set BG1 1 
set BG2 2
set BG3 3
set BG4 4
set BG5 5
set BG6 6
set BG7 7
set BG8 8
set BG9 9
set BG10 10
set BG11 11
set BG12 12
set BG13 13
set BG14 14

#This is used when configuring up flows. 
set UGSSCHED 0
set RTPSSCHED 1
set BESCHED 2

#This is used when configuring up flows. These follow
#the ns pktype definitions found in ./common/packet.h
#This param is how a flow is mapped to particular packet flows.
set PT_TCP 0
set PT_UDP 1
set PT_CBR 2
set PT_ICMP  44
set PT_LOSS_MON 63

set PT_WILDCARD 255

set INACTIVE_FLOWID 0
set FLOWID_1 1
set FLOWID_2 2
set FLOWID_3 3

set PHS_SUPRESS_ALL   0
set PHS_SUPRESS_TCPIP 1
set PHS_SUPRESS_UDPIP 2
set PHS_SUPRESS_MAC   3
set PHS_NO_SUPRESSION 4


#in bits.... 2 packets
set  BUCKET_LENGTH 24448
set TICKSPERSLOT 4

#For the adaptive algorithm - this scales the range 
#A value of 1 implies no scaling
set BT_BKOFF 1
set BKOFF 1
set CMBKOFF 1

#define this in bps to be for the cable/lan network
#
#This is with 8% for FEC
#set UPSPEED 200000000
#set UPSPEED 4710000
#set UPSPEED 5120000
set UPSPEED 10240000
#This assumes  1.6Mbhs channel, 16Qam
#set RAW_UPSPEED 5120000
#set RAW_UPSPEED 200000000
set RAW_UPSPEED 10240000
#DOCSIS2.0 raw speed 30.72Mbps assumes 64Qam, 6.4Mzh channel
#set UPSPEED 30720000
#adjust for 8%FEC
#set RAW_UPSPEED 28262400

#Adjust for 4.7%FEC
#set DOWNSPEED 200000000
#set RAW_DOWNSPEED 200000000
#set DOWNSPEED 30340000
#set RAW_DOWNSPEED 30340000
#DOCSIS 2.0 with 256 Qam 40.455528MBPS
#set DOWNSPEED 40455520
#and after FEC
#set RAW_DOWNSPEED 38554118
set DOWNSPEED 42880000
set RAW_DOWNSPEED 42880000


#Used by BuildTCPCxWithMode and BuildUDPCxWithMode
set EXPONENTIAL_TRAFFIC_MODE  0
set CBR_TRAFFIC_MODE  1
set FTP_TRAFFIC_MODE  2
set PARETO_TRAFFIC_MODE  3

#Yet another encoding for possible UDP_TRAFFIC traffic types :  value 0: CBR   value 1 EXP  value 2 Pareto
#   Value 3 will be APFEC
set CBR_UDP_TRAFFIC 0
set EXP_UDP_TRAFFIC 1
set PARETO_UDP_TRAFFIC 2
set APFEC_UDP_TRAFFIC 3


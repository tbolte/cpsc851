#If these are set, they overide the UNICAST and MULTICAST settings below
#Set to 0 if don't want to use them
set CBR_PACKETSIZE	1400
set CBR_RATE 768000.0

set UNICAST_PACKETSIZE 1400
set UNICAST_RATE 768000

set MULTICAST_PACKETSIZE 1460
set MULTICAST_RATE 768000.0


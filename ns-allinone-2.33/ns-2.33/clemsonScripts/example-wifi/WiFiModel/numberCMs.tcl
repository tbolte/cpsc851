#Must be total number of stations.  If total of unicast+multicast flows > numberCMs 
#then more than 1 flow will be assigned per station
set numberCMs            6
set numberUnicastFlows   6
set numberMulticastFlows 0

set numGroups            1
set group(1)             5
set group(2)             0
set group(3)             0
set group(4)             0
set group(5)             0


####################################################################################
#function : cleanup
# 
####################################################################################

proc cleanup {} {
#puts "Deleting a bunch of *.out data files"
exec ./clean.script
exec rm -f WIRELESSPHYTRACE.out WIRELESSMACTRACE.out arrivals.out departures.out

exec rm -f LMPingDS.out LMPingUS.out
exec rm -f loss_mon_ser1.out loss_mon_ser2.out
exec rm -f DSCorrLossMon.out USCorrLossMon.out DSLossMon.out USLossMon.out
exec rm -f Macstats.out
exec rm -f wired-and-wireless.tr
exec rm -f out.tr tcpTrace.out
exec rm -f xnetLR.out xnetLR1.out
exec rm -f thru1.out thru2.out thru3.out thru4.out thru5.out thru1000S.out thru100C.out
exec rm -f queue1.out queue2.out
exec rm -f queuemon.out queue1mon.out
exec rm -f trafficxnet.out trafficxnet1.out trafficxnet2.out
exec rm -f TCPSend1.out SESSION_TRAF.out
exec rm -f ping1.out ping2.out LMping1.out LMping2.out
exec rm -f UDPstats.out TCPstats.out P2PTCPstats.out
exec rm -f CMstats.out CMTSstats.out
exec rm -f CM-BW-ns1.out CM-BW-CM1.out CM-BW-CM2.out CM-BW-CM3.out CM-BW-CM4.out CM-BW-CM5.out
exec rm -f CM-BW-CM6.out CM-BW-CM7.out CM-BW-CM8.out CM-BW-CM9.out CM-BW-CM10.out 
exec rm -f PACKET_TRACE_US.dat PACKET_TRACE_DS.dat
exec rm -f CMPACKET_TRACE_US.dat CMPACKET_TRACE_DS.dat
exec rm -f CM1QUEUE.out 
exec rm -f UGSJitterCM.out UGSJitterCMTS.out TIMINGS.dat

exec rm -f CMTS-BW.out
exec rm -f CMTS-DS-QUEUE.out CM-DS-QUEUE.out
exec rm -f CMTS-DS-util.out 

exec rm -f snumack.out UDPSINK.recv CBR.send CBR.recv
exec rm -f DSSIDstats.out
exec rm -f tcpsend1.out tcpsend2.out
exec rm -f CMTSMAP.out
exec rm -f CMMAP.out

exec rm -f UDPEXP*.out  TCPEXP*.out

}

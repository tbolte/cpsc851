function plotthruput(maxThroughput,numberFiles)
%
% Parameters:
%   maxThroughput:   if don't specify it, we figure it out
%   numberFiles:  you can specify 1 or 2 data files
%
% Input:
%        Uses : thru1.out and thru2.out
%  
%        plotthruput()
%        plotthruput(1500000,1) ; plots just thru1.out
%  
%


if (nargin == 0)
  maxThroughput=0;
  numberFiles = 2;
end


  load thru1.out
if (numberFiles > 1)
  load thru2.out
  send2 = thru2;
end
  send = thru1;

f=size(send)
i = f(1);
j = f(2);
for i = 1 : f(1)
   j = 1;
   x(i) = send(i,j);
   j = 2;
   y(i) = send(i,j);

end
fprintf(1,'thru: max sample = %9.6f \n',max(y));
if (max(y) > maxThroughput)
  maxThroughput=max(y);
  fprintf(1,'thru: max = %9.6f \n',maxThroughput);
end

fprintf(1,'avg send rate: %9.6f;  std: %9.6f \n',mean(y),std(y));

startTime=send(1,1)
stopTime = send(f(1),1)

plot(x,y,'k-'),axis([startTime stopTime 0  maxThroughput])
hold on;
grid on;

if (numberFiles ==1)
  return;
end

clear x;
clear y;
f=size(send2)
i = f(1);
j = f(2);
for i = 1 : f(1)
   j = 1;
   x(i) = send2(i,j);
   j = 2;
   y(i) = send2(i,j);

 end
fprintf(1,'thru: max sample = %9.6f \n',max(y));
if (max(y) > maxThroughput)
  maxThroughput=max(y);
  fprintf(1,'thru: max = %9.6f \n',maxThroughput);
end

fprintf(1,'avg send2 rate: %9.6f;  std: %9.6f \n',mean(y),std(y));


plot(x,y,'r--'),axis([startTime stopTime 0  maxThroughput])

%text(5e-5,.70,'---- : RTP Sim Data')
title('TCP Cx Throughput');
xlabel('Time (seconds) ')
ylabel('Bits/second')

%hold off


# Cullum Smith (gcsmith@clemson.edu)
# wan1-2.tcl
# CPSC 851 HW2

exec rm -f xnetLR.out
exec rm -f thru1.out
exec rm -f tcpsend1.out
exec rm -f queue1.out
exec rm -f trafficxnet.out
exec rm -f TCPstats.out
exec rm -f tcpTrace.out
exec rm -f ping1.out
exec rm -f snumack.out
exec rm -f UDPstats.out

set ns [new Simulator]

# LINKSPEED1, PROPDELAY1: links from routers to sources/sinks
# LINKSPEED2, PROPDELAY2: link between the two routers (bottleneck)
set LINKSPEED1 50Mb
set LINKSPEED2 10Mb
set PROPDELAY1 0.002
set PROPDELAY2 0.024

#This contains a bunch of helper scripts
source networks.tcl
source ping-helper.tcl


# inner routers
set r1 [$ns node]
set r2 [$ns node]

# souces
set n1 [$ns node]

# corresponding sinks
set s1 [$ns node]

set stoptime 500.2
set printtime 500.1
set testname test-wan1-2


# set topology: {n1,n2,n3,n4,n5}--{r1}--{r2}--{s1,s2,s3,s4,s5}
$ns duplex-link $r1 $n1 $LINKSPEED1 $PROPDELAY1 DropTail
$ns duplex-link $r1 $r2 $LINKSPEED2 $PROPDELAY2 DropTail
$ns duplex-link $r2 $s1 $LINKSPEED1 $PROPDELAY1 DropTail


# set up UDP cxn info
set udp1 [new Agent/UDP]
set sink1 [new Agent/UDPSink]

$udp1 set fid_ 1
$sink1 set fid_ 1

$ns attach-agent $n1 $udp1
$ns attach-agent $s1 $sink1
$ns connect $udp1 $sink1

$udp1 set packetSize_ 1460

set exp1 [new Application/Traffic/Exponential]
$exp1 attach-agent $udp1

# exponential traffic generator params
$exp1 set burst_time_ 1.5
$exp1 set idle_time_  .3
$exp1 set rate_ 11Mb
$exp1 set packetSize_ 1460


$ns at 1.2 "$exp1 start"

$ns at $printtime "dumpFinalUDPStats  1 1.0  $sink1 UDPstats.out"

$ns at $stoptime "finish"


proc finish {} {
global ns
    $ns flush-trace
    exit 0
}

$ns run

exit 0

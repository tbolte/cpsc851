# Cullum Smith (gcsmith@clemson.edu)
# wan1-1.tcl
# CPSC 851 HW2

exec rm -f xnetLR.out
exec rm -f thru1.out
exec rm -f tcpsend1.out
exec rm -f queue1.out
exec rm -f trafficxnet.out
exec rm -f TCPstats.out
exec rm -f tcpTrace.out
exec rm -f ping1.out
exec rm -f snumack.out
exec rm -f TCPstats.out

# get number of concurrent FTP connections as first command-line arg
# (default=1)
set connections 1

if { $argc == 1 } {
   set connections [lindex $argv 0]
}
puts "using $connections FTP connection(s)"

set ns [new Simulator]

# LINKSPEED1, PROPDELAY1: links from routers to sources/sinks
# LINKSPEED2, PROPDELAY2: link between the two routers (bottleneck)
set QUEUECAPACITY 20
set LINKSPEED1 100000000
set LINKSPEED2 1500000
set PROPDELAY1 0.002
set PROPDELAY2 0.024

#Sets the max TCP send window
set WINDOW  20

#This contains a bunch of helper scripts
source networks.tcl
source ping-helper.tcl


# inner routers
set r1 [$ns node]
set r2 [$ns node]

# souces
set n1 [$ns node]
set n2 [$ns node]
set n3 [$ns node]
set n4 [$ns node]
set n5 [$ns node]

# corresponding sinks
set s1 [$ns node]
set s2 [$ns node]
set s3 [$ns node]
set s4 [$ns node]
set s5 [$ns node]

set stoptime 500.2
set printtime 500.1
set testname test-wan1-1


# set topology: {n1,n2,n3,n4,n5}--{r1}--{r2}--{s1,s2,s3,s4,s5}
$ns duplex-link $r1 $n1 $LINKSPEED1 $PROPDELAY1 DropTail
$ns duplex-link $r1 $n2 $LINKSPEED1 $PROPDELAY1 DropTail
$ns duplex-link $r1 $n3 $LINKSPEED1 $PROPDELAY1 DropTail
$ns duplex-link $r1 $n4 $LINKSPEED1 $PROPDELAY1 DropTail
$ns duplex-link $r1 $n5 $LINKSPEED1 $PROPDELAY1 DropTail

$ns duplex-link $r1 $r2 $LINKSPEED2 $PROPDELAY2 DropTail

$ns duplex-link $r2 $s1 $LINKSPEED1 $PROPDELAY1 DropTail
$ns duplex-link $r2 $s2 $LINKSPEED1 $PROPDELAY1 DropTail
$ns duplex-link $r2 $s3 $LINKSPEED1 $PROPDELAY1 DropTail
$ns duplex-link $r2 $s4 $LINKSPEED1 $PROPDELAY1 DropTail
$ns duplex-link $r2 $s5 $LINKSPEED1 $PROPDELAY1 DropTail

# set the queue buffer size in packets
$ns queue-limit $r1 $r2 $QUEUECAPACITY
$ns queue-limit $r2 $r1 $QUEUECAPACITY


# set up TCP cxn info
set tcp1 [new Agent/TCP/Sack1]
set tcp2 [new Agent/TCP/Sack1]
set tcp3 [new Agent/TCP/Sack1]
set tcp4 [new Agent/TCP/Sack1]
set tcp5 [new Agent/TCP/Sack1]
set sink1 [new Agent/TCPSink/Sack1/DelAck]
set sink2 [new Agent/TCPSink/Sack1/DelAck]
set sink3 [new Agent/TCPSink/Sack1/DelAck]
set sink4 [new Agent/TCPSink/Sack1/DelAck]
set sink5 [new Agent/TCPSink/Sack1/DelAck]

$tcp1 set cxId_ 1
$tcp2 set cxId_ 2
$tcp3 set cxId_ 3
$tcp4 set cxId_ 4
$tcp5 set cxId_ 5
$sink1 set sinkId_ 1
$sink2 set sinkId_ 2
$sink3 set sinkId_ 3
$sink4 set sinkId_ 4
$sink5 set sinkId_ 5

$ns attach-agent $n1 $tcp1
$ns attach-agent $n2 $tcp2
$ns attach-agent $n3 $tcp3
$ns attach-agent $n4 $tcp4
$ns attach-agent $n5 $tcp5
$ns attach-agent $s1 $sink1
$ns attach-agent $s2 $sink2
$ns attach-agent $s3 $sink3
$ns attach-agent $s4 $sink4
$ns attach-agent $s5 $sink5
$ns connect $tcp1 $sink1
$ns connect $tcp2 $sink2
$ns connect $tcp3 $sink3
$ns connect $tcp4 $sink4
$ns connect $tcp5 $sink5

$tcp1 set window_ $WINDOW
$tcp1 set maxcwnd_ $WINDOW
$sink1 set maxcwnd_ $WINDOW
$sink1 set window_ $WINDOW
$tcp2 set window_ $WINDOW
$tcp2 set maxcwnd_ $WINDOW
$sink2 set maxcwnd_ $WINDOW
$sink2 set window_ $WINDOW
$tcp3 set window_ $WINDOW
$tcp3 set maxcwnd_ $WINDOW
$sink3 set maxcwnd_ $WINDOW
$sink3 set window_ $WINDOW
$tcp4 set window_ $WINDOW
$tcp4 set maxcwnd_ $WINDOW
$sink4 set maxcwnd_ $WINDOW
$sink4 set window_ $WINDOW
$tcp5 set window_ $WINDOW
$tcp5 set maxcwnd_ $WINDOW
$sink5 set maxcwnd_ $WINDOW
$sink5 set window_ $WINDOW

$tcp1 set packetSize_ 1460
$tcp2 set packetSize_ 1460
$tcp3 set packetSize_ 1460
$tcp4 set packetSize_ 1460
$tcp5 set packetSize_ 1460

set ftp1 [new Application/FTP]
set ftp2 [new Application/FTP]
set ftp3 [new Application/FTP]
set ftp4 [new Application/FTP]
set ftp5 [new Application/FTP]
$ftp1 attach-agent $tcp1
$ftp2 attach-agent $tcp2
$ftp3 attach-agent $tcp3
$ftp4 attach-agent $tcp4
$ftp5 attach-agent $tcp5

# set up ping between source1 and sink1
set pn1 [new Agent/Ping]
set ps1 [new Agent/Ping]

$ns attach-agent $n1 $pn1
$ns attach-agent $s1 $ps1

$ns connect $pn1 $ps1

$ns at $printtime "doPingStats"

startPingProcess $ns $pn1 .5


# set up queue between the two routers
set qmon [$ns monitor-queue $r1 $r2 ""]
set integ [$qmon get-bytes-integrator]

TraceQueueLossRate $ns xnetLR  $qmon 20 xnetLR.out
TraceQueueSize    $ns 1  $r1 $r2 .1 queue1.out
TraceQueueTraffic $ns 1  $r1 $r2  1 trafficxnet.out $LINKSPEED2


# activate varying number of FTP connections based on command line arg
if {$connections == 1 } {
	$ns at 1.2 "$ftp1 start"

	$ns at $printtime "dumpFinalTCPStats  1 1.0 $tcp1  $sink1 TCPstats.out"
} elseif {$connections == 2} {
	$ns at 1.2 "$ftp1 start"
	$ns at 1.2 "$ftp2 start"

	$ns at $printtime "dumpFinalTCPStats  1 1.0 $tcp1  $sink1 TCPstats.out"
	$ns at $printtime "dumpFinalTCPStats  1 1.0 $tcp2  $sink2 TCPstats.out"
} elseif {$connections == 3} {
	$ns at 1.2 "$ftp1 start"
	$ns at 1.2 "$ftp2 start"
	$ns at 1.2 "$ftp3 start"

	$ns at $printtime "dumpFinalTCPStats  1 1.0 $tcp1  $sink1 TCPstats.out"
	$ns at $printtime "dumpFinalTCPStats  1 1.0 $tcp2  $sink2 TCPstats.out"
	$ns at $printtime "dumpFinalTCPStats  1 1.0 $tcp3  $sink3 TCPstats.out"
} elseif {$connections == 4} {
	$ns at 1.2 "$ftp1 start"
	$ns at 1.2 "$ftp2 start"
	$ns at 1.2 "$ftp3 start"
	$ns at 1.2 "$ftp4 start"

	$ns at $printtime "dumpFinalTCPStats  1 1.0 $tcp1  $sink1 TCPstats.out"
	$ns at $printtime "dumpFinalTCPStats  1 1.0 $tcp2  $sink2 TCPstats.out"
	$ns at $printtime "dumpFinalTCPStats  1 1.0 $tcp3  $sink3 TCPstats.out"
	$ns at $printtime "dumpFinalTCPStats  1 1.0 $tcp4  $sink4 TCPstats.out"
} elseif {$connections == 5} {
	$ns at 1.2 "$ftp1 start"
	$ns at 1.2 "$ftp2 start"
	$ns at 1.2 "$ftp3 start"
	$ns at 1.2 "$ftp4 start"
	$ns at 1.2 "$ftp5 start"

	$ns at $printtime "dumpFinalTCPStats  1 1.0 $tcp1  $sink1 TCPstats.out"
	$ns at $printtime "dumpFinalTCPStats  1 1.0 $tcp2  $sink2 TCPstats.out"
	$ns at $printtime "dumpFinalTCPStats  1 1.0 $tcp3  $sink3 TCPstats.out"
	$ns at $printtime "dumpFinalTCPStats  1 1.0 $tcp4  $sink4 TCPstats.out"
	$ns at $printtime "dumpFinalTCPStats  1 1.0 $tcp5  $sink5 TCPstats.out"
} else {
   puts "OH NOES!!! only supports up to 5 FTP connections!!!"
   exit -1
}

$ns at $stoptime "finish"


proc finish {} {
global ns
    $ns flush-trace
    exit 0
}

$ns run

exit 0

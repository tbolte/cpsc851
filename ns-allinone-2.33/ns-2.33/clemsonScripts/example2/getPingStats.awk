#Change the number in the third paran to indicate the column in the data file to operate upon.
#If the columns are separated with tabs, change the first paran to "\t"
# This script computes statistics based on the input data file of RTT samples. Input file format:
#        0.090202538597014922 90.2 1  (first field timestamp of sample, second is RTT in ms, third is sequence number)
#        
# It displays to stdout: avgRTT, jitterAvg, lossRate,  numberSamples, numberOutofSeq
BEGIN { 
  FS = " "
  SUM = 0.0;
  numberSamples = 0;
  highestSeqNum=-1;
  numberOutofSeq = 0;
  firstTimeStamp=-1.0;
  totalNumberLines=0;
  
  jitterSample=0;
  jitterSum=0.0;
  numberJitterSamples=0;
  lastSample=-1.0;
  dropCount=0;
  lossRate=0.0;
} 

#For each line ...
{
  totalNumberLines++;
  if (firstTimeStamp == -1) {
    firstTimeStamp = $1;
  }
  if (highestSeqNum < 0) {
    highestSeqNum = ($3-1);
  }

#  printf("Last Sample:%3.3f, highestSeqNum:%d   %f  %f  %d \n",lastSample,highestSeqNum,$1,$2,$3); 
  if (lastSample > -1) {

    dropCount = $3 - highestSeqNum;
    if (dropCount > 1 ) {
      numberOutofSeq = numberOutofSeq + (dropCount -1);
#      printf("DROP: seq#:%d, dropCount:%d,  highestSeqNumber:%d, numberOutofSeq:%d\n",$3,dropCount,highestSeqNumber,numberOutofSeq); 
    }
    else {
      if ($2 == 0.0) {
        numberOutofSeq++;
#        printf("DROP: seq#:%d,  highestSeqNumber:%d\n",$3,highestSeqNumber); 
      } 
      else {
        numberSamples++;
        SUM = SUM + $2;
        jitterSample=$2-lastSample;
        jitterSum= jitterSum+abs(jitterSample);
        numberJitterSamples++;
      }
    }
  }
  if ($3 >  highestSeqNum) {
    highestSeqNum=$3
  }
  lastSample=$2;

#print "RTT: " $8;


#done
}

END {

  numberSamples = numberSamples+numberOutofSeq;

  lossRate=0.0;
  avgRTT=0.0;

  if (numberSamples > 0) {
    lossRate = numberOutofSeq/numberSamples;
    avgRTT = (SUM / 1000) / numberSamples;
  }

  if (numberJitterSamples > 0) {
    jitterAvg= (jitterSum / 1000) / numberJitterSamples;
  }

  printf("%3.6f %3.6f %2.6f %d %d\n", avgRTT, jitterAvg, lossRate,  numberSamples, numberOutofSeq); 

}



function abs(value)
{
  return (value<0?-value:value);
}


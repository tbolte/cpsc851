#Change the number in the third paran to indicate the column in the data file to operate upon.
#If the columns are separated with tabs, change the first paran to "\t"
BEGIN {
FS = " " 
MBL = 0.0;
MILD = 0.0;
LR = 0.0;
Latency = 0.0;
}

{
  LR= $7
  MBL = $8;
  MILD = $9;
  Latency=$6
}
END {
  printf("Correlated Loss: LR:%3.3f, MBL:%f, MILD:%f, Latency:%3.6f  \n", LR,MBL,MILD,Latency);
}



#Change the number in the third paran to indicate the column in the data file to operate upon.
#If the columns are separated with tabs, change the first paran to "\t"
BEGIN { 
  FS = " "
  delayDS = 0;
  lossRateDS = 0.0;
  thruputDS = 0;
  smallThru = 10000000000;
  bigThru = 0;
  excludeFlowID = 0;
} 
{
cxID = $1;
if ((excludeFlowID == 0)  || (cxID !=  excludeFlowID)) 
{
    x++;
    DS++;
    delayDS=delayDS+$8;
    lossRateDS=lossRateDS+$5;
    thruputDS=thruputDS+$9;
    if (bigThru < $9) {
      if ($9 > 0) {
        bigThru = $9;
      }
    }
    if (smallThru > $9) {
      if ($9 > 0) {
        smallThru = $9;
      }
    }
}
{sum+=$9; array[NR]=$9} 

}
END {
{
for(x=1;x<=NR;x++){sumsq+=((array[x]-(sum/NR))^2);}
std= sqrt(sumsq/NR)
#printf("based on %d samples, the std is %9.0f ",x, std);
}


#printf("number Flows: (not including flow %d)  %d\n",excludeFlowID, x);
#print "avg RTT: " delayDS/DS;
#print "avg loss percentage  " lossRateDS/DS; 
meanThroughput=thruputDS/DS;
error=std/meanThroughput;
printf("id meanDelay meanLR, aggrThru,       meanThru,   std,        error,  smallThru, bigThru\n");
printf("%d\t%3.3f\t%3.3f\t%d\t%d\t%9.0f\t%3.3f\t%d\t%d\n",x-1,delayDS/DS, lossRateDS/DS,sum,meanThroughput,std,error,smallThru,bigThru);
}

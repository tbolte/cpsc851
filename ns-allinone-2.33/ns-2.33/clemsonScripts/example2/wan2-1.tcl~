################################################################## 
#   wan2-1.tcl param1 param2
#           param1: number of cxs (default 1)
#           param2: seed  (default 1)
#                   if < 11, will move to this substream
#                   else assume its the desired seed of the defaultRNG
#       
#
#                     PROPDELAY1   PROPDELAY2  PROPDELAY1  
#                     LINKSPEED1   LINKSPEED2  LINKSPEED1
#                n0 --|                              |-- s0  monitor
#                n1 --| -------{r1}-------{r2}-------|-- s1
#                n2 --|                              |-- s2
#                n3 --|                              |-- s3
#               ... --|                              |-- ...
#            src_node_                              sink_node_
#
#################################################################

exec rm -f xnetLR.out
exec rm -f thru1.out
exec rm -f tcpsend1.out
exec rm -f queue1.out
exec rm -f trafficxnet.out
exec rm -f TCPstats.out
exec rm -f tcpTrace.out
exec rm -f ping1.out
exec rm -f snumack.out
exec rm -f TCPstats.out

set connections 1
set MYSEED 1
set queuetype 0
set QUEUECAPACITY 20

# get number of concurrent FTP connections as first command-line arg
if { $argc > 0 } {
   set connections [lindex $argv 0]
}
if { $argc > 1 } {
   set MYSEED [lindex $argv 1]
}
if { $argc > 2 } {
	set queuetype [lindex $argv 2]	
}	
if { $argc > 3 } {
	set QUEUECAPACITY [lindex $argv 3]
}

puts "using $connections FTP connection(s), MYSEED is $MYSEED"
puts "queue capacity: $QUEUECAPACITY"

if {$queuetype == 0} {
	puts "using DropTail"
}
if {$queuetype == 1} {
	puts "using RED"
}
if {$queuetype == 2} {
	puts "using Adaptive RED"
}

set ns [new Simulator]


#Init our random number generator
global defaultRNG
#if see MYSEED < 11 we will assume we are to use the NEXT SUBSTREAM API to set a guaranteed uncorrelated stream
if {$MYSEED < 11} {
  for {set j 1} {$j < $MYSEED} {incr j} {
    $defaultRNG next-substream
  }
} else {
  # seed the default RNG
  $defaultRNG seed $MYSEED
  puts "Setting seed of defaultRNG " 
}
#end init RNG

# LINKSPEED1, PROPDELAY1: links from routers to sources/sinks
# LINKSPEED2, PROPDELAY2: link between the two routers (bottleneck)
set LINKSPEED1 100000000
set LINKSPEED2 1500000
set PROPDELAY1 0.002
set PROPDELAY2 0.024

#Sets the max TCP send window
set WINDOW  20

#This contains a bunch of helper scripts
source networks.tcl
source ping-helper.tcl
source cleanup.tcl

#delete any output files from previous runs
cleanup

#Setup the Correlated loss monitor
#Set to 1 to turn ON CORRloss monitor
#     Set to 0 to turn if OFF
set doCORRLOSSMONITOR 1
set CBR_PACKETSIZE      1000
set CBR_RATE 768000.0
set UNICAST_PACKETSIZE 1000
set UNICAST_RATE 3000000.0
set MULTICAST_PACKETSIZE 1460
set MULTICAST_RATE 768000.0
#Done with Setup the Correlated loss monitor




# inner routers
set r1 [$ns node]
set r2 [$ns node]

set n0 [$ns node]
set s0 [$ns node]



set stoptime 500.2
set printtime 500.1
set testname test-wan1-1

# set topology: {n0}--{r1}--{r2}--{s0}
if {$queuetype == 0} {
	$ns duplex-link $r1 $r2 $LINKSPEED2 $PROPDELAY2 DropTail
	$ns duplex-link $r1 $n0 $LINKSPEED1 $PROPDELAY1 DropTail
	$ns duplex-link $r2 $s0 $LINKSPEED1 $PROPDELAY1 DropTail
}
if {$queuetype == 1} {
	$ns duplex-link $r1 $r2 $LINKSPEED2 $PROPDELAY2 RED
	$ns duplex-link $r1 $n0 $LINKSPEED1 $PROPDELAY1 RED
	$ns duplex-link $r2 $s0 $LINKSPEED1 $PROPDELAY1 RED
}
if {$queuetype == 2} {
	Queue/RED set adaptive_ 1

	$ns duplex-link $r1 $r2 $LINKSPEED2 $PROPDELAY2 RED
	$ns duplex-link $r1 $n0 $LINKSPEED1 $PROPDELAY1 RED
	$ns duplex-link $r2 $s0 $LINKSPEED1 $PROPDELAY1 RED
}

# set the queue buffer size in packets
$ns queue-limit $r1 $r2 $QUEUECAPACITY
$ns queue-limit $r2 $r1 $QUEUECAPACITY

# set up ping between s0 and n0
set pn1 [new Agent/Ping]
set ps1 [new Agent/Ping]

$ns attach-agent $n0 $pn1
$ns attach-agent $s0 $ps1

$ns connect $pn1 $ps1

$ns at $printtime "doPingStats"

startPingProcess $ns $pn1 .5


# set up queue between the two routers
set qmon [$ns monitor-queue $r1 $r2 ""]
set integ [$qmon get-bytes-integrator]

TraceQueueLossRate $ns xnetLR  $qmon $QUEUECAPACITY xnetLR.out
TraceQueueSize    $ns 1  $r1 $r2 .1 queue1.out
TraceQueueTraffic $ns 1  $r1 $r2  1 trafficxnet.out $LINKSPEED2

set CXN 100;

#The following creates an artificial packet loss process
#on all packets from r1 to r2.
# ERROR_MODEL values:
#  0 bernoulli
#  1 two state (also referred to as a Gilbert-Eliot loss model)
#  2 none
set ERROR_MODEL 2



#needed by GE model
set goodLossAvgDuration 153.5
set badLossAvgDuration 25.0

if {$ERROR_MODEL == 0 } {

  set LOSS_RATE1 0.05
  set loss_modulexnet [new ErrorModel]
  $loss_modulexnet set errModelId 2
  puts "Using Bernoulli Loss ErrorModel, loss rate: $LOSS_RATE1"

  $loss_modulexnet set rate_ $LOSS_RATE1

  $loss_modulexnet unit pkt
  $loss_modulexnet ranvar [new RandomVariable/Uniform]
  $loss_modulexnet drop-target [new Agent/Null]
} elseif { $ERROR_MODEL == 1} {

  set LOSS_RATE1 0.14
  puts "Using TwoState ErrorModel (seed:$MYSEED), avg LR: $LOSS_RATE1, goodAvg: $goodLossAvgDuration,  badAvg: $badLossAvgDuration"

  if { $LOSS_RATE1 > 0} {
    
    set newrng_ [new RNG]
    if {$MYSEED < 11} {
      for {set j 1} {$j < $MYSEED} {incr j} {
        $newrng_ next-substream
      }
    } else {
      # seed the default RNG
      $newrng_ seed $MYSEED
    }


    #ratio of 100:25 for run length to achieve .2 loss rate
    #set goodLossAvgDuration 100.0
    #set badLossAvgDuration 25.0
    #ratio of 4:1 for time based mode to achieve .2 loss rate
    #set goodLossAvgDuration 4.0
    #set badLossAvgDuration 1.0
    set rv0 [new RandomVariable/Exponential]
    set rv1 [new RandomVariable/Exponential]
    $rv0 use-rng $newrng_
    $rv1 use-rng $newrng_
    #  $rv0 set avg_ $goodAvgTime
    #  $rv1 set avg_ $badAvgTime
    $rv0 set avg_ $goodLossAvgDuration
    $rv1 set avg_ $badLossAvgDuration

    #  set em [new ErrorModel/MyTwoState $myrates]
    #  set loss_modulexnet [new ErrorModel/TwoState $rv0 $rv1 time]
    #if not time
    set loss_modulexnet [new ErrorModel/TwoState $rv0 $rv1]
  } else {
    set loss_modulexnet [new ErrorModel]
    $loss_modulexnet set errModelId 2
    $loss_modulexnet set rate_  0.0
  }
} else  {
  set LOSS_RATE1 0.0
  set loss_modulexnet [new ErrorModel]
  $loss_modulexnet set errModelId 2
  $loss_modulexnet set rate_ $LOSS_RATE1

  puts " Using NO artifical loss "

  $loss_modulexnet unit pkt
  $loss_modulexnet ranvar [new RandomVariable/Uniform]
}
$ns lossmodel $loss_modulexnet $r1 $r2
#End setting up the loss process


#Setup the desired number of TCP flows
for {set i 1} {$i < [expr 1+ $connections]} {incr i} {

  incr CXN;
  set flowStartTime [expr 0.001 + [uniform 0 2]]
  set src_node_($i) [$ns node]
  set sink_node_($i) [$ns node]
  puts "Create TCP CXN $CXN"

  if {$queuetype == 0} {
  $ns duplex-link $r1 $src_node_($i)  $LINKSPEED1 $PROPDELAY1 DropTail
  $ns duplex-link $r2 $sink_node_($i) $LINKSPEED1 $PROPDELAY1 DropTail
  }
  if {$queuetype > 0} {
  $ns duplex-link $r1 $src_node_($i)  $LINKSPEED1 $PROPDELAY1 RED
  $ns duplex-link $r2 $sink_node_($i) $LINKSPEED1 $PROPDELAY1 RED
  }

  set tcps_($i) [new Agent/TCP/Sack1]
  set sinks_($i) [new Agent/TCPSink/Sack1/DelAck]

  $tcps_($i) set cxId_ $CXN
  $sinks_($i) set sinkId_ $CXN

  $ns attach-agent $src_node_($i) $tcps_($i) 
  $ns attach-agent $sink_node_($i) $sinks_($i)

  $ns connect $tcps_($i) $sinks_($i)

  $tcps_($i) set window_ $WINDOW
  $tcps_($i) set maxcwnd_ $WINDOW
  $tcps_($i) set packetSize_ 1460
  $sinks_($i) set maxcwnd_ $WINDOW
  $sinks_($i) set window_ $WINDOW

  set ftps_($i) [new Application/FTP]
  $ftps_($i) attach-agent $tcps_($i)

  $ns at $flowStartTime "$ftps_($i) start"

  #You can comment this out if you do not need these....
  #TraceThroughput $ns $sinks_($i)  1.0 thru$i.out


  $ns at $printtime "dumpFinalTCPStats  $CXN 1.0 $tcps_($i)  $sinks_($i) TCPstats.out"

}
#Done Setting the desired number of TCP flows

#Setup the correlated loss monitor
if {$doCORRLOSSMONITOR > 0} {


# rx side (cl1)  n0 --|                              |-- s0  send side (cl0) 
#                n1 --| -------{r1}-------{r2}-------|-- s1

set lossmonitorstarttime 1.55555
set lossmonitorstoptime $stoptime
set datasize $UNICAST_PACKETSIZE

set sendrate $CBR_RATE

set cl0 [new Agent/Corr_Loss_Mon]
$cl0 data-size [expr $datasize]

#This is the sender (client)
$ns  attach-agent $n0  $cl0


set cl1 [new Agent/Corr_Loss_Mon]
$cl1 data-size [expr $datasize]

set numerator [expr $lossmonitorstoptime - $lossmonitorstarttime]
set a [expr $datasize.*8]
set denominator [expr $a/$sendrate]
set arraysize [expr  ceil($numerator/$denominator)]
$cl1 max-size-arrival-array [expr $arraysize]
$cl1 set-outfile DSCorrLossMon.out
#This is the receiver (server)
$ns  attach-agent $s0  $cl1

$cl0 send-rate [expr $sendrate]
$ns  connect $cl0 $cl1
$ns  at $lossmonitorstarttime "$cl0 start"
$ns  at $lossmonitorstoptime "$cl0 stop"
#dumping the stats for loss_monitor 0-1
# prints: pkts_sent pkts_received pkts_lost duplicate_pkts outoforderarrival_pkts avgLatency avgLossRate MBLmetric MILDmetric
$ns  at [expr $printtime - 0.1] "$cl1 dumpresults"

}
#Done Setting the correlated loss monitor


$ns at $stoptime "finish"


proc finish {} {
global ns
    $ns flush-trace
    exit 0
}

$ns run

exit 0

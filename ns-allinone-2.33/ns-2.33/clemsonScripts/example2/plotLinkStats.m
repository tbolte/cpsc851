function plotLinkStats(startTime,stopTime)
%param:  startTime     
%        stopTime (in  seconds)
%
%  This routine plots the stats associated with a link
%  over time. 
%   The queue level (queue1.out), the link utilization (trafficxnet.m)
%   and the loss rate (xnetLR.out)  are plotted.
%
%  Usage:
%      plotLinkStats() :  plots all data
%      plotLinkStats(0,50) : the first 50 seconds 
%

maxQueueLevel=0;

if (nargin == 0)
   startTime = 0;
   stopTime = 0;
end

load queue1.out

f=size(queue1);
i = f(1);
j = f(2);
for i = 1 : f(1)
   j = 1;
   x(i) = queue1(i,j);
   j = 2;
   y(i) = queue1(i,j);
   j = 3;
   z(i) = queue1(i,j);
end
maxQueueLevel=max(y);
stopTime = x(i);

subplot(3,1,1)
plot(x,y,'k-'),axis([startTime stopTime 0 maxQueueLevel])
hold on
plot(x,z,'k--'),axis([startTime stopTime 0 maxQueueLevel])
ylabel('Q Size ')
title('Bottleneck link 1 forward direction')
grid



clear f i j x y;
load trafficxnet.out
f=size(trafficxnet);
i = f(1);
j = f(2);
for i = 1 : f(1)
   j = 1;
   x(i) = trafficxnet(i,j);
   j = 3;
   y(i) = trafficxnet(i,j);
end

maxUtilLevel=max(y);
stopTime = x(i);

subplot(3,1,2)
plot(x,y,'k-'),axis([startTime stopTime 0 maxUtilLevel])
ylabel('Utilization(%) ')
grid


clear f i j x y;
load xnetLR.out
f=size(xnetLR);
%i = f(1)(%);
%j = f(2);
for i = 1 : f(1)
   j = 1;
   x(i) = xnetLR(i,j);
   j = 2;
   y(i) = xnetLR(i,j);
end

maxLRLevel=max(y);
stopTime = x(i);

subplot(3,1,3)
plot(x,y,'k-'),axis([startTime stopTime 0 maxLRLevel])
ylabel('Loss ')
grid





#
# Usage: ns rng-test.tcl [replication number]
#

if {$argc > 1} {
    puts "Usage: ns rng-test.tcl \[replication number\]"
    exit
}
set run 1
if {$argc == 1} {
    set run [lindex $argv 0]
}
if {$run < 1} {
    set run 1
}

# seed the default RNG
global defaultRNG
$defaultRNG seed 9999
set fname "rng_test.out"

# create the RNGs and set them to the correct substream
set arrivalRNG [new RNG]
set sizeRNG [new RNG]
for {set j 1} {$j < $run} {incr j} {
    $arrivalRNG next-substream
    $sizeRNG next-substream
}

# arrival_ is a exponential random variable describing the time between
# consecutive packet arrivals
set arrival_ [new RandomVariable/Exponential]
$arrival_ set avg_  0.5
#$arrival_ set avg_ .016
$arrival_ use-rng $arrivalRNG

# size_ is a uniform random variable describing packet sizes
set size_ [new RandomVariable/Uniform]
$size_ set min_ 100
$size_ set max_ 5000
$size_ use-rng $sizeRNG


# print  samples of the arrival and size RV's
set numberSamples 5000
set f [open $fname a]
for {set j 0} {$j < $numberSamples}  {incr j} {
#    puts [format "%-3.9f " [$arrival_ value]]
    puts $f [format "%-3.9f  %-4d;" [$arrival_ value] \
	    [expr round([$size_ value])]]
    flush $f
}
close $f



/* -*-	Mode:C++; c-basic-offset:8; tab-width:8; indent-tabs-mode:t -*- */
/*
 * Copyright (c) 1990, 1997 Regents of the University of California.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms are permitted
 * provided that the above copyright notice and this paragraph are
 * duplicated in all such forms and that any documentation,
 * advertising materials, and other materials related to such
 * distribution and use acknowledge that the software was developed
 * by the University of California, Lawrence Berkeley Laboratory,
 * Berkeley, CA.  The name of the University may not be used to
 * endorse or promote products derived from this software without
 * specific prior written permission.
 * THIS SOFTWARE IS PROVIDED ``AS IS'' AND WITHOUT ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, WITHOUT LIMITATION, THE IMPLIED
 * WARRANTIES OF MERCHANTIBILITY AND FITNESS FOR A PARTICULAR PURPOSE.
 */

#ifndef lint
static const char rcsid[] =
    "@(#) $Header: /nfs/jade/vint/CVSROOT/ns-2/tcp/tcp-reno.cc,v 1.36 2001/11/08 19:06:08 sfloyd Exp $ (LBL)";
#endif

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>

#include "ip.h"
#include "tcp.h"
#include "flags.h"

//To trace this module
//#define TCPRTTCA 0

#define TRACEACK 0
#define TRACED_ACK_CX 2
//To have the tcpRTT trace retrans, do this .  Else it traces dups...
#define TRACERETRANS 0

//TRACEBURST needs this to be right....this is for the WRT monitor
#define TRACED_CX 1

//JJM
//Used to trace the tcp cx associated with a given web sessionID (see webtraf.cc)
//#define TRACE_THROUGHPUT 0
//#define TRACED_THROUGHPUT_CX 1000

//RCS 2
//To trace a burst , see expoo.cc for the start
#define TRACEBURST 0

//#define TRACEWAITTIME 0
    //
#define MINSCALEFACTOR 2
#define MAXSCALEFACTOR 5
#define MOVINGTHRESHOLDWEIGHT .2
#define WAITINGTIMEWEIGHT .9
    //
#define   SLOWRATEINCREASE  0
#define   RATEINCREASE  1
#define   RATENOCHANGE  2
#define   RATEDECREASE  3
//
#define  SLOWDOWN1      4
#define  SLOWDOWN2      5
#define  CLEARPIPE      6
    //
    //
//#define DOS3 0
    //
    //

static class RttcaTcpClass : public TclClass {
public:
	RttcaTcpClass() : TclClass("Agent/TCP/Rttca") {}
	TclObject* create(int, const char*const*) {
		return (new RttcaTcpAgent());
	}
} class_RttcaTcpClass;

int RttcaTcpAgent::window()
{
	//
	// reno: inflate the window by dupwnd_
	//	dupwnd_ will be non-zero during fast recovery,
	//	at which time it contains the number of dup acks
	//
	int win = int(cwnd_) + dupwnd_;
	if (win > int(wnd_))
		win = int(wnd_);
	return (win);
}

double RttcaTcpAgent::windowd()
{
	//
	// reno: inflate the window by dupwnd_
	//	dupwnd_ will be non-zero during fast recovery,
	//	at which time it contains the number of dup acks
	//
	double win = cwnd_ + dupwnd_;
	if (win > wnd_)
		win = wnd_;
	return (win);
}

RttcaTcpAgent::RttcaTcpAgent() : TcpAgent(), dupwnd_(0)
{
//JJM
//  printf("\nRttcaTcp contructed at  %f", Scheduler::instance().clock());
}

void RttcaTcpAgent::recv(Packet *pkt, Handler*)
{
	hdr_tcp *tcph = hdr_tcp::access(pkt);

#ifdef notdef
	if (pkt->type_ != PT_ACK) {
		fprintf(stderr,
			"ns: confiuration error: tcp received non-ack\n");
		exit(1);
	}
#endif
        /* W.N.: check if this is from a previous incarnation */
        if (tcph->ts() < lastreset_) {
                // Remove packet and do nothing
                Packet::free(pkt);
                return;
        }

	//JJM
        int bytes = hdr_cmn::access(pkt)->size();

	++nackpack_;
	ts_peer_ = tcph->ts();

	if (hdr_flags::access(pkt)->ecnecho() && ecn_)
		ecn(tcph->seqno());
	recv_helper(pkt);

#ifdef TCPRTTCA
      if (cx_id_ == TRACED_CX)
        printf("\nTCPrttca:recv(%g): Packet/Ack of %d arrived, last_ack %d, sendTime (%d)= %g ",
           Scheduler::instance().clock(), tcph->seqno(), last_ack_,tcpSendTime[last_ack_ % maxcwnd_]);
#endif
#ifdef TRACEACK
      if (cx_id_ == TRACED_ACK_CX)
//         printf("\n2 %6.9f %d %d", Scheduler::instance().clock(), ((tcph->seqno()+1)*size_ + 1),bytes);
         printf("\n2 %6.9f %d ", Scheduler::instance().clock(), ((tcph->seqno()+1)*size_ + 1));
#endif

	if (tcph->seqno() > last_ack_) {
#ifdef TCPRTTCA
      if (cx_id_ == TRACED_CX)
        printf("\nTCPrttca:recv(%g): seqno ( %d) larger than last_ack (%d), nextTimedSeqno:%d",
           Scheduler::instance().clock(), tcph->seqno(), last_ack_,nextTimedSeqno);
#endif
		dupwnd_ = 0;
		recv_newack_helper(pkt);
		if (last_ack_ == 0 && delay_growth_) {
			cwnd_ = initial_window();
		}


//JJM : Got a new ack,  let's get a tcpRTT sample but
// only if the just received ack is > highest_ack_ AND if
//         the seqno is greater than the maxseq_
//   NOTE: the tcph->ts() contains the time the receiver
//         received the packet (his time is sync to system time)
//  nextTimedSeqno is nonzero when we find a dup ack.  We don't want
//  to use anymore rtt samples until the retransmitted pkt is ack'ed...
// The effect here is to try to time every pkt sent.  However, we only
// obtain a RTT sample for the snum corresponding to the actual snum
// being acked.  If the ack ack's more than 1 segment, we only use the RTT
// sample from the highest snum.

               if ((last_ack_ >= 0) && ((nextTimedSeqno == 0) || (tcph->seqno() >= nextTimedSeqno))) {
                  nextTimedSeqno = 0;
                  tcpRTT = Scheduler::instance().clock() -
                         tcpSendTime[last_ack_ % maxcwnd_];
                  FinalRTT_ += tcpRTT;
	          FinalLossRTTMeanCounter_ +=1;
		  //JJM TEMP
//            printf("\nTCPrttca:recv(%g):SAMPLING: new tcpRTT:%g;  total FinalRTT:%g, no Samples: %d",
//                       Scheduler::instance().clock(), (double)tcpRTT, FinalRTT_, FinalLossRTTMeanCounter_);
#ifdef TCPRTTCA
                  if (cx_id_ == TRACED_CX) {
                    printf("\nTCPrttca:recv(%g):SAMPLING: Ack of %d arrived. sendTime= %g, maxseq=%d,lastack=%d ,nextTimedSe=%d",
                       Scheduler::instance().clock(), tcph->seqno(),
                        tcpSendTime[last_ack_ % maxcwnd_],(int)maxseq_,last_ack_,nextTimedSeqno);
                    printf("\n         tcpRTT is %g, pkt ts %g",
                       (double) tcpRTT,(double)tcph->ts());
                  }
#endif
                }
                else {
                  // else this is a dup ack ...
                  // set tcpRTT to indicate a dup arrived.
#ifndef TRACERETRANS
                  tcpRTT = 0;
#endif
#ifdef TCPRTTCA
                  if (cx_id_ == TRACED_CX) {
                      printf("\nTCPrttca:recv(%g):NO SAMPLING: Ack of %d arrived. lastack=%d,netTimedSeqno=%d ",
                                Scheduler::instance().clock(),tcph->seqno(), last_ack_,nextTimedSeqno);
                  }
#endif
               }







	} else if (tcph->seqno() == last_ack_) {
		if (hdr_flags::access(pkt)->eln_ && eln_) {
			tcp_eln(pkt);
			return;
		}
		if (++dupacks_ == numdupacks_) {
			dupack_action();
			dupwnd_ = numdupacks_;
		} else if (dupacks_ > numdupacks_) {
			++dupwnd_;	// fast recovery
		} else if (dupacks_ < numdupacks_ && singledup_ ) {
			send_one();
		}


//JJM
#ifndef TRACERETRANS
                tcpRTT = 0;
                trace(&tcpRTT);
#endif

                if (nextTimedSeqno > 0)
                    nextTimedSeqno = (int)maxseq_ + 1;
                if (nextTimedSeqno == 0)
                    nextTimedSeqno = (int)maxseq_ + 1;
#ifdef TCPRTTCA
       if (cx_id_ == TRACED_CX)
                printf("\nTCPrttca:recv(%g): Dup Ack of %d arrived. dupacks now %d,nextTimedSeqno=%d ",
                     Scheduler::instance().clock(), tcph->seqno(),(int)dupacks_,nextTimedSeqno);
#endif

	}
	Packet::free(pkt);
#ifdef notyet
	if (trace_)
		plot();
#endif

	/*
	 * Try to send more data
	 */

	if (dupacks_ == 0 || dupacks_ > numdupacks_ - 1) {
		send_much(0, 0, maxburst_);


//JJM
#ifdef TRACEBURST
        if (cx_id_ == TRACED_CX) {
          if (highest_ack_ == maxseq_) {
//            printf("\n2 %f", Scheduler::instance().clock());
            FILE* fp = fopen("snumack.out", "a+");
            fprintf(fp,"\n2 %f", Scheduler::instance().clock());
            fclose(fp);

//So, we assume this is the end of a burst,  close the window now rather than wait for a timout
             globalBurstFlag = 0;
//12/22/2002:  no need,  the idle timeout does this
//             slowdown(CLOSE_CWND_ONE);
#ifdef TCPRTTCA
             if (cx_id_ == TRACED_CX)
                printf("\nTCPrttca:recv(%g): end of burst ,  reset globalBurstFlag ,  cwnd_ : %f",
                     Scheduler::instance().clock(), double(cwnd_));
#endif
          }

        }
#endif


       }
}

int
RttcaTcpAgent::allow_fast_retransmit(int last_cwnd_action_)
{
	return (last_cwnd_action_ == CWND_ACTION_DUPACK);
}

/*
 * Dupack-action: what to do on a DUP ACK.  After the initial check
 * of 'recover' below, this function implements the following truth
 * table:
 *  
 *      bugfix  ecn     last-cwnd == ecn        action  
 *  
 *      0       0       0                       reno_action
 *      0       0       1                       reno_action    [impossible]
 *      0       1       0                       reno_action
 *      0       1       1                       retransmit, return  
 *      1       0       0                       nothing 
 *      1       0       1                       nothing        [impossible]
 *      1       1       0                       nothing 
 *      1       1       1                       retransmit, return
 */ 
    
void
RttcaTcpAgent::dupack_action()
{
	int recovered = (highest_ack_ > recover_);
	int allowFastRetransmit = allow_fast_retransmit(last_cwnd_action_);
        if (recovered || (!bug_fix_ && !ecn_) || allowFastRetransmit) {
		goto reno_action;
	}

	if (ecn_ && last_cwnd_action_ == CWND_ACTION_ECN) {
		last_cwnd_action_ = CWND_ACTION_DUPACK;
		/* 
		 * What if there is a DUPACK action followed closely by ECN
		 * followed closely by a DUPACK action?
		 * The optimal thing to do would be to remember all
		 * congestion actions from the most recent window
		 * of data.  Otherwise "bugfix" might not prevent
		 * all unnecessary Fast Retransmits.
		 */
		reset_rtx_timer(1,0);
		output(last_ack_ + 1, TCP_REASON_DUPACK);
		return; 
	}

	if (bug_fix_) {
		/*
		 * The line below, for "bug_fix_" true, avoids
		 * problems with multiple fast retransmits in one
		 * window of data.
		 */
		return;
	}

reno_action:
	// we are now going to fast-retransmit and will trace that event
	trace_event("RENO_FAST_RETX");
	recover_ = maxseq_;
	last_cwnd_action_ = CWND_ACTION_DUPACK;
	slowdown(CLOSE_SSTHRESH_HALF|CLOSE_CWND_HALF);
	reset_rtx_timer(1,0);
	output(last_ack_ + 1, TCP_REASON_DUPACK);	// from top
	return;
}

void RttcaTcpAgent::timeout(int tno)
{
#ifdef TCPRTTCA
        if (cx_id_ == TRACED_CX) {
          printf("\nTCPrttca:timeout(%g): reno timeout called with tno of %d ",
                   Scheduler::instance().clock(),tno);
        }
#endif
	if (tno == TCP_TIMER_RTX) {
		dupwnd_ = 0;
		dupacks_ = 0;
		if (bug_fix_) recover_ = maxseq_;
		TcpAgent::timeout(tno);
	} else {
		timeout_nonrtx(tno);
	}
}

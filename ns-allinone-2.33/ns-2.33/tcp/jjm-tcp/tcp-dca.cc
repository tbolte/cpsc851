/* -*-	Mode:C++; c-basic-offset:8; tab-width:8; indent-tabs-mode:t -*- */
/*
 * Copyright (c) 1990, 1997 Regents of the University of California.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms are permitted
 * provided that the above copyright notice and this paragraph are
 * duplicated in all such forms and that any documentation,
 * advertising materials, and other materials related to such
 * distribution and use acknowledge that the software was developed
 * by the University of California, Lawrence Berkeley Laboratory,
 * Berkeley, CA.  The name of the University may not be used to
 * endorse or promote products derived from this software without
 * specific prior written permission.
 * THIS SOFTWARE IS PROVIDED ``AS IS'' AND WITHOUT ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, WITHOUT LIMITATION, THE IMPLIED
 * WARRANTIES OF MERCHANTIBILITY AND FITNESS FOR A PARTICULAR PURPOSE.
 *
 *  $A0; 10/26/2004.  Added HIGHSPEED option. If set to 1, then we
 *      do not do anything when a loss is detected. 
 *      Modify dupack_action so it does not reduce the cwnd ?
 *      On a timeout,  don't go to slow start
 */

#ifndef lint
static const char rcsid[] =
    "@(#) $Header: /nfs/jade/vint/CVSROOT/ns-2/tcp/tcp-reno.cc,v 1.36 2001/11/08 19:06:08 sfloyd Exp $ (LBL)";
#endif
//
#define SHOWREACTIONS 0

//If set to 0, normal tcp dca
//If set to 1, then we eliminate tcp's reaction to loss
#define HIGHSPEED 0

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <math.h>

#include "ip.h"
#include "tcp.h"
#include "flags.h"

//To trace this module
//#define TCPDCA 0
//To trace the REACTIONS and tcpRTT
//#define DCA 0
//

//To trace this module
//#define TCPDCA 0

    //If TRACEACK is defined, then we create the snum/ack output for this cx id
#define TRACEACK 0
#define TRACED_ACK_CX 2
//To have the tcpRTT trace retrans, do this .  Else it traces dups...
#define TRACERETRANS 0

//TRACEBURST needs this to be right....this is for the WRT monitor
    //And if TCPDCA is defined, printf's are enabled
#define TRACED_CX 1

//SElect only one ALGORITHM1 or ALGORITHM2.
// Algorithm one is similar to Dual-  every other RTT, do the CD decision
//#define  ALGORITHM1 0
//
//Algorithm 2  is sort of  like our measurmeent work.  We can react more
// than once/epoch (unlike the measurement analysis)
#define  ALGORITHM2 0
//
// Only for ALGORITHM2,    can react more than once / epoch
//Algo 3 based on Algo 2 except we only can react 1 /epoch.
//Undefein it and you can react > 1 per epoch
//Define it and you react 1 / epoch
//#define  ALGORITHM3 0
//  If DO NOT  define ALGORITHM3, then you can also define
//  ALGORITHM5.  If so then we react once every other RTT
//#define  ALGORITHM5 0
//
//

//Algo 4 : uses more dynamic DCA_threshold
//  Without this,  it's more like the measurement algorithm.  We
//  use the threshold:
//    DCA_threshold_ =
//           uncongestedLongAvg_ + (filterFactor_ * uncongestedLongStd_);
//#define  ALGORITHM4 0
//
//
//JJM
//Used to trace the tcp cx associated with a given web sessionID (see webtraf.cc)
//#define TRACE_THROUGHPUT 0
//#define TRACED_THROUGHPUT_CX 1000

//RCS 2
//To trace a burst , see expoo.cc for the start
#define TRACEBURST 0


static class DCATcpClass : public TclClass {
public:
	DCATcpClass() : TclClass("Agent/TCP/DCA") {}
	TclObject* create(int, const char*const*) {
		return (new DCATcpAgent());
	}
} class_DCA;

int DCATcpAgent::window()
{
	//
	// reno: inflate the window by dupwnd_
	//	dupwnd_ will be non-zero during fast recovery,
	//	at which time it contains the number of dup acks
	//
	int win = int(cwnd_) + dupwnd_;
	if (win > int(wnd_))
		win = int(wnd_);
	return (win);
}

double DCATcpAgent::windowd()
{
	//
	// reno: inflate the window by dupwnd_
	//	dupwnd_ will be non-zero during fast recovery,
	//	at which time it contains the number of dup acks
	//
	double win = cwnd_ + dupwnd_;
	if (win > wnd_)
		win = wnd_;
	return (win);
}

DCATcpAgent::DCATcpAgent() : TcpAgent(), dupwnd_(0)
{
//JJM
//  printf("\nDCA contructed at  %f", Scheduler::instance().clock());
#ifdef TCPDCA
        if (cx_id_ == TRACED_CX) {
          printf("\nTCPDCA:(%g): constructed a DCA agent",
                   Scheduler::instance().clock());
        }
#endif
min_rtt = 0;
max_rtt = 0;
/* this can be anything between 0 and 1 */
alpha = .25;
dual_threshold = 0;
v_inc_flag_ = 0; // if cwnd is allowed to incr for this rtt
v_begseq_ = 0;
epochOnFlag_ =0;
reactFlag_ = 0;
numberEpochs_ =0;
maxEpochLevel_ = 0;
uncongestedLongAvg_=0;
uncongestedLongStd_=0;
lastUncongestedLevel_=0;
longHistoryIndex_=0;
shortHistoryIndex_=0;
DCA_threshold_ = 0;
bind("longHistory_", &longHistory_);
bind("shortHistory_", &shortHistory_);
bind("filterFactor_", &filterFactor_);
bind("congestionReduction_", &congestionReduction_);
longAvg_ = new double[longHistory_];

int i;
for (i=0;i<longHistory_;i++)
   longAvg_[i] = 0;
shortAvg_ = new double[shortHistory_];

for (i=0;i<shortHistory_;i++)
  shortAvg_[i] = 0;
//       set_pkttype(PT_DCA);
             set_pkttype(PT_TCP);


}

void DCATcpAgent::recv(Packet *pkt, Handler*)
{
	hdr_tcp *tcph = hdr_tcp::access(pkt);
        int didWeReactFlag = 0;


#ifdef notdef
	if (pkt->type_ != PT_ACK) {
		fprintf(stderr,
			"ns: confiuration error: tcp received non-ack\n");
		exit(1);
	}
#endif
        /* W.N.: check if this is from a previous incarnation */
        if (tcph->ts() < lastreset_) {
                // Remove packet and do nothing
                Packet::free(pkt);
                return;
        }
        int sizeACK = tcph->seqno() - last_ack_;

	++nackpack_;
	ts_peer_ = tcph->ts();

	if (hdr_flags::access(pkt)->ecnecho() && ecn_)
		ecn(tcph->seqno());
	recv_helper(pkt);

#ifdef TCPDCA
   if (cx_id_ == TRACED_CX) {
     printf("\nTCPDCA:recv(%g): Ack of %d arrived, last_ack sendTime (%d)= %g ",
                Scheduler::instance().clock(), tcph->seqno(),
                last_ack_,tcpSendTime[last_ack_ % maxcwnd_]);
     printf("\n     seqno - last ack is %d ,cwnd = %g",sizeACK,(double)cwnd_);

  }
#endif
#ifdef TRACEACK
      if (cx_id_ == TRACED_ACK_CX)
         printf("\n2 %6.9f %d ", Scheduler::instance().clock(), ((tcph->seqno()+1)*size_ + 1));
#endif

	if (tcph->seqno() > last_ack_) {
#ifdef TCPDCA
      if (cx_id_ == TRACED_CX)
        printf("\nTCPDCA:recv(%g): seqno ( %d) larger than last_ack (%d), nextTimedSeqno:%d",
           Scheduler::instance().clock(), tcph->seqno(), last_ack_,nextTimedSeqno);
#endif
		dupwnd_ = 0;
		recv_newack_helper(pkt);
		if (last_ack_ == 0 && delay_growth_) {
			cwnd_ = initial_window();
		}


//JJM : Got a new ack,  let's get a tcpRTT sample but
// only if the just received ack is > highest_ack_ AND if
//         the seqno is greater than the maxseq_
//   NOTE: the tcph->ts() contains the time the receiver
//         received the packet (his time is sync to system time)
//  nextTimedSeqno is nonzero when we find a dup ack.  We don't want
//  to use anymore rtt samples until the retransmitted pkt is ack'ed...
// The effect here is to try to time every pkt sent.  However, we only
// obtain a RTT sample for the snum corresponding to the actual snum
// being acked.  If the ack ack's more than 1 segment, we only use the RTT
// sample from the highest snum.

               if ((last_ack_ >= 0) && ((nextTimedSeqno == 0) || (tcph->seqno() >= nextTimedSeqno))) {

//Now, only get tcpRTT sample if this is not a dup ack ....  and rtt not a TO
	         if ((sizeACK>1)  &&
                     ( (Scheduler::instance().clock() - tcpSendTime[last_ack_ % maxcwnd_])< 1)) {
                  double tmpShortAvg=0;
                  double tmpLongAvg=0;
                  double tmpLongAvgStd=0;
                  nextTimedSeqno = 0;
                  tcpRTT = Scheduler::instance().clock() - 
                         tcpSendTime[last_ack_ % maxcwnd_];

		  longAvg_[longHistoryIndex_%longHistory_] = (double)tcpRTT;
                  longHistoryIndex_++;
		  shortAvg_[shortHistoryIndex_%shortHistory_] = (double) tcpRTT;
                  shortHistoryIndex_++;
                 
                  monitorRttArray(shortAvg_,shortHistory_,&tmpShortAvg,&tmpLongAvgStd);
                  monitorRttArray(longAvg_,longHistory_,&tmpLongAvg,&tmpLongAvgStd);


    


#ifdef TCPDCA
                     if (cx_id_ == TRACED_CX) {
                         printf("\nTCPDCA:recv(%g) computed tcpRTT of %g",
                           Scheduler::instance().clock(),(double) tcpRTT);
                         dumpRttArray(longAvg_,longHistory_);
                         dumpRttArray(shortAvg_,shortHistory_);
                         printf("\nTCPDCA:recv(%g) shortAvg=%g (shorthistory=%d),  longAvg:  %g and std: %g",
                           Scheduler::instance().clock(),tmpShortAvg,shortHistory_,tmpLongAvg,tmpLongAvgStd);
                     }
#endif
#ifdef TCPDCA
                     if (cx_id_ == TRACED_CX) {
                       printf("\nTCPDCA:recv(%g) cwnd is %g",
                       Scheduler::instance().clock(),(double)cwnd_);
                     }
#endif

#ifdef ALGORITHM1
                  //Every other RTT, see if we need to reduce the cwnd...
                  // if so, put  us into CA
                DCA_threshold_ = tmpLongAvg + (filterFactor * tmpLongAvgStd);
		if(tcph->seqno() >= v_begseq_) {
                  if(!v_inc_flag_) {
                   if (tcpRTT > DCA_threshold_) {
                     didWeReactFlag = 1;
                     if(cwnd_ < ssthresh_) { 
                       ssthresh_ = 2;
                     }
                     cwnd_ -=(cwnd_ * congestionReduction);
                     if (cwnd_ < 2)
                      cwnd_ = 2;
#ifdef TCPDCA 
                     if (cx_id_ == TRACED_CX) {
                       printf("\nTCPDCA:recv(%g) REACT: New cwnd is %g, ssth=%d",
                       Scheduler::instance().clock(),(double)cwnd_,(int)ssthresh_);
                     }
#endif
                   }
                  }
                  v_begseq_ = t_seqno_; 
                  v_inc_flag_ = !v_inc_flag_ ;
#ifdef TCPDCA
                  if (cx_id_ == TRACED_CX) {
                    printf("\nTCPDCA:recv(%g)  Next seqno to trace:  %d", 
                       Scheduler::instance().clock(),v_begseq_);
                  }
#endif
                 }
#endif




#ifdef ALGORITHM2
                  //Every epoch,  see if we need to reduce the cwnd...
                  //but don't reduce more frequently than 1 / rtt
                  // if so, put  us into CA



               //inflate this to get going... else a small first rtt sample will cause
               // things to stick...

               if (lastUncongestedLevel_ == 0)
                 lastUncongestedLevel_ = tmpLongAvg * 2;

               if (epochOnFlag_ ==0 ) {
#ifdef TCPDCA
                 if (cx_id_ == TRACED_CX) {
                   printf("\nTCPDCA:recv(%g) epoch Not on , tcpRTT =%g, tmpLongAvg=%g:, tmpShortAvg=%g,lastUn=%g ",
                          Scheduler::instance().clock(),(double)tcpRTT,tmpLongAvg,tmpShortAvg,lastUncongestedLevel_);
                 }
#endif
                 if (tcpRTT > lastUncongestedLevel_) {

                   reactFlag_ = 0;
                   epochOnFlag_ = 1;
                   numberEpochs_++;
                   uncongestedLongAvg_ = tmpLongAvg;
                   uncongestedLongStd_ = tmpLongAvgStd;
                   maxEpochLevel_ = tcpRTT;
#if defined(TCPDCA) || defined(DCA) 
                 if (cx_id_ == TRACED_CX) {
                    printf("\nTCPDCA:recv(%g) Move to NEW epoch : ", Scheduler::instance().clock());
                 }
#endif
//Don't do CD check on entry,  wait....

                 }
                 else {
                   lastUncongestedLevel_ = tmpLongAvg;
#ifdef TCPDCA
                 if (cx_id_ == TRACED_CX) {
                   printf("\nTCPDCA:recv(%g)epoch not on, tcpRTT < lastuncong, new lastuncongested=%g ",
                          Scheduler::instance().clock(),lastUncongestedLevel_);
                 }
#endif
                 }
               }
               else {

                 //During an epoch...
#ifdef TCPDCA
                 if (cx_id_ == TRACED_CX) {
                    printf("\nTCPDCA:recv(%g) epoch On , tcpRTT =%g, tmpLongAvg=%g:, tmpShortAvg=%g,lastUn=%g ",
                          Scheduler::instance().clock(),(double) tcpRTT,tmpLongAvg,tmpShortAvg,lastUncongestedLevel_);
                 }
#endif
                 if (tcpRTT <=  ((lastUncongestedLevel_ + tmpLongAvg)/2) ) {
                   //terminate the epoch
                   epochOnFlag_ = 0;
                   reactFlag_ = 0;
                   lastUncongestedLevel_ = tmpLongAvg;
    
#if defined(TCPDCA) || defined(DCA) 
                   if (cx_id_ == TRACED_CX) {
                      printf("\nTCPDCA:recv(%g) Leaving Epoch  : ", Scheduler::instance().clock());
                   }
#endif

                 }
                 else {
                   //stay in the epoch
                   if (maxEpochLevel_ < tcpRTT)
                     maxEpochLevel_ = tcpRTT;

//Get a few samples before engaging
                   if (DCA_threshold_ == 0)
                      DCA_threshold_ = 5;
                   else
#ifdef ALGORITHM4
                     DCA_threshold_ = tmpLongAvg + (filterFactor_ * tmpLongAvgStd);
#else
                     DCA_threshold_ = uncongestedLongAvg_ + (filterFactor_ * uncongestedLongStd_);
#endif

		     //JJM
		     // transmission time of 250 pkts at 1gbps is just under 3 ms
//		     DCA_threshold_ = uncongestedLongAvg_ + .003;        

#if defined(TCPDCA) || defined(DCA) 
                   if (cx_id_ == TRACED_CX) {
                     printf("\nTCPDCA:recv(%g) Within an Epoch, tmpShortAvg=%g; DCA_threshold=%g, nextDecision=%g",
                     Scheduler::instance().clock(),tmpShortAvg,DCA_threshold_,nextDecision_);
                   }
#endif
                   if ((tmpShortAvg >  DCA_threshold_) && (Scheduler::instance().clock() > nextDecision_)) {
//So if ALGORITHM3 is NOT defined we will react 1 per RTT,  
//  if ALGORITHM5 is also defined we react at most every other RTT
#if !defined(ALGORITHM3) && defined(ALGORITHM5)
                     nextDecision_ = Scheduler::instance().clock()+ (2 * tmpShortAvg);
#else
                     nextDecision_ = Scheduler::instance().clock()+ tmpShortAvg;
#endif

                     if (reactFlag_ == 0) {
#if defined(TCPDCA) || defined(DCA) 
                       if (cx_id_ == TRACED_CX) {
                         printf("\nTCPDCA:recv(%g) BEFORE REACT: cwnd is %g, ssth=%d, reduction=%g",
                         Scheduler::instance().clock(),(double)cwnd_,(int)ssthresh_,congestionReduction_);
                       }
#endif
//JJM P : right now, I get different results when I run tcp-dca on pc02 or 
//at hme.  If we comment out the cwnd_ line below, results are the same...
//(and we don't execute this ...??
//goto myend;

                       didWeReactFlag = 1;
                       cwnd_ -= (double) ((double) cwnd_ * congestionReduction_);
                       if (cwnd_ < 2)
                        cwnd_ = 2;

#ifdef ALGORITHM3
//If do this then we will react only once per epoch.  Otherwise it might be >1 / epoch
                       reactFlag_ = 1;
#endif
#if defined(TCPDCA) || defined(DCA) 
                       if (cx_id_ == TRACED_CX) {
                         printf("\nTCPDCA:recv(%g) REACT: New cwnd is %g, ssth=%d",
                         Scheduler::instance().clock(),(double)cwnd_,(int)ssthresh_);
                       }
#endif
myend:              ;
                     }


                   }
                 }
               }


#endif
//end ALGORITHM2


#ifdef TCPDCA
                  if (cx_id_ == TRACED_CX) {
                    printf("\nTCPDCA:recv(%g):SAMPLING: Ack of %d arrived. sendTime= %g, maxseq=%d,lastack=%d ,nextTimedSe=%d",
                       Scheduler::instance().clock(), tcph->seqno(),
                        tcpSendTime[last_ack_ % maxcwnd_],(int)maxseq_,last_ack_,nextTimedSeqno);
                    printf("\n         tcpRTT is %g, pkt ts %g", 
                       (double) tcpRTT,(double)tcph->ts());
                  }
#endif
 

                 }
#ifdef SHOWREACTIONS
                 if (cx_id_ == TRACED_CX) {
                     printf("\n%g %g  %d %g %d %g %g %g",
                     Scheduler::instance().clock(), (double)tcpRTT,epochOnFlag_,DCA_threshold_,didWeReactFlag,
                      uncongestedLongStd_, filterFactor_, (double)cwnd_);
                 }
#endif
#if defined(TCPDCA) || defined(DCA) 
                 if (cx_id_ == TRACED_CX) {
                     printf("\n%g %g  %d %g",
                     Scheduler::instance().clock(), (double)tcpRTT,epochOnFlag_,DCA_threshold_);
                 }
#endif



                }
                else {
                  // else this is a dup ack ...
                  // set tcpRTT to indicate a dup arrived.
#ifndef TRACERETRANS
                  tcpRTT = 0;
#endif
#ifdef TCPDCA
                  if (cx_id_ == TRACED_CX) {
                      printf("\nTCPDCA:recv(%g):NO SAMPLING: Ack of %d arrived. lastack=%d,netTimedSeqno=%d ",
                                Scheduler::instance().clock(),tcph->seqno(), last_ack_,nextTimedSeqno);
                  }
#endif
               }

	} else if (tcph->seqno() == last_ack_) {

		if (hdr_flags::access(pkt)->eln_ && eln_) {
			tcp_eln(pkt);
			return;
		}
		if (++dupacks_ == numdupacks_) {
			dupack_action();
			dupwnd_ = numdupacks_;
		} else if (dupacks_ > numdupacks_) {
			++dupwnd_;	// fast recovery
		} else if (dupacks_ < numdupacks_ && singledup_ ) {
			send_one();
		}


//JJM
#ifndef TRACERETRANS
                tcpRTT = 0;
                trace(&tcpRTT);
#endif

                if (nextTimedSeqno > 0)
                    nextTimedSeqno = (int)maxseq_ + 1;
                if (nextTimedSeqno == 0) 
                    nextTimedSeqno = (int)maxseq_ + 1;
#ifdef TCPDCA 
               if (cx_id_ == TRACED_CX) printf("\nTCPDCA:recv(%g): Dup Ack of %d arrived. dupacks now %d,nextTimedSeqno=%d ",
                     Scheduler::instance().clock(), tcph->seqno(),(int)dupacks_,nextTimedSeqno);
#endif

	}
	Packet::free(pkt);
#ifdef notyet
	if (trace_)
		plot();
#endif

	/*
	 * Try to send more data
	 */

	if (dupacks_ == 0 || dupacks_ > numdupacks_ - 1) {
		send_much(0, 0, maxburst_);


//JJM
#ifdef TRACEBURST
        if (cx_id_ == TRACED_CX) {
          if (highest_ack_ == maxseq_) {
//            printf("\n2 %f", Scheduler::instance().clock());
            FILE* fp = fopen("snumack.out", "a+");
            fprintf(fp,"\n2 %f", Scheduler::instance().clock());
            fclose(fp);

//So, we assume this is the end of a burst,  close the window now rather than wait for a timout
             globalBurstFlag = 0;
//12/22/2002:  no need,  the idle timeout does this
//             slowdown(CLOSE_CWND_ONE);
#ifdef TCPDCA
             if (cx_id_ == TRACED_CX)
                printf("\nTCPDCA:recv(%g): end of burst ,  reset globalBurstFlag ,  cwnd_ : %f",
                     Scheduler::instance().clock(), double(cwnd_));
#endif
          }

        }
#endif


       }
}

int
DCATcpAgent::allow_fast_retransmit(int last_cwnd_action_)
{
	return (last_cwnd_action_ == CWND_ACTION_DUPACK);
}

/*
 * Dupack-action: what to do on a DUP ACK.  After the initial check
 * of 'recover' below, this function implements the following truth
 * table:
 *  
 *      bugfix  ecn     last-cwnd == ecn        action  
 *  
 *      0       0       0                       reno_action
 *      0       0       1                       reno_action    [impossible]
 *      0       1       0                       reno_action
 *      0       1       1                       retransmit, return  
 *      1       0       0                       nothing 
 *      1       0       1                       nothing        [impossible]
 *      1       1       0                       nothing 
 *      1       1       1                       retransmit, return
 */ 
    
void
DCATcpAgent::dupack_action()
{
	int recovered = (highest_ack_ > recover_);
	int allowFastRetransmit = allow_fast_retransmit(last_cwnd_action_);
        if (recovered || (!bug_fix_ && !ecn_) || allowFastRetransmit) {
		goto reno_action;
	}

	if (ecn_ && last_cwnd_action_ == CWND_ACTION_ECN) {
		last_cwnd_action_ = CWND_ACTION_DUPACK;
		/* 
		 * What if there is a DUPACK action followed closely by ECN
		 * followed closely by a DUPACK action?
		 * The optimal thing to do would be to remember all
		 * congestion actions from the most recent window
		 * of data.  Otherwise "bugfix" might not prevent
		 * all unnecessary Fast Retransmits.
		 */
		reset_rtx_timer(1,0);
		output(last_ack_ + 1, TCP_REASON_DUPACK);
		return; 
	}

	if (bug_fix_) {
		/*
		 * The line below, for "bug_fix_" true, avoids
		 * problems with multiple fast retransmits in one
		 * window of data.
		 */
		return;
	}

reno_action:
	// we are now going to fast-retransmit and will trace that event
	trace_event("RENO_FAST_RETX");
	recover_ = maxseq_;
	last_cwnd_action_ = CWND_ACTION_DUPACK;
//$A0 only do if NOT highspeed.  If highspeed,don't react ?
        if (HIGHSPEED == 0) { 
         slowdown(CLOSE_SSTHRESH_HALF|CLOSE_CWND_HALF);
        }
	reset_rtx_timer(1,0);
	output(last_ack_ + 1, TCP_REASON_DUPACK);	// from top
	return;
}

void DCATcpAgent::timeout(int tno)
{
#ifdef TCPDCA
        if (cx_id_ == TRACED_CX) {
          printf("\nTCPDCA:timeout(%g): DCA timeout called with tno of %d ",
                   Scheduler::instance().clock(),tno);
        }
#endif
	if (tno == TCP_TIMER_RTX) {
		dupwnd_ = 0;
		dupacks_ = 0;
		if (bug_fix_) recover_ = maxseq_;
		TcpAgent::timeout(tno);
	} else {
		timeout_nonrtx(tno);
	}
}

void DCATcpAgent::dumpRttArray(double* rttAvg_,int arraySize)
{
 int i;

  if (cx_id_ == TRACED_CX) {
      printf("\nTCPDCA:dumpRTTArray(%g) (size=%d)  : ", Scheduler::instance().clock(),arraySize);
      for (i=0; i<arraySize; i++) {
        printf(" %g, ",rttAvg_[i]);
      }
  }
}

void  DCATcpAgent::monitorRttArray(double* rttAvg_,int arraySize,double* arrayMean, double* arrayStd)
{
double mean=0;
double var=0;
double std= 0;
int i=0;
double sum=0;

//JJM P
//return;


    for (i=0; i<arraySize; i++) {
      if (rttAvg_[i] == 0)
         break;
      sum += rttAvg_[i];
    }
    mean = sum /i;
    *arrayMean = mean;
    if (arraySize == 2)
     return;

#ifdef TCPDCA            
     if (cx_id_ == TRACED_CX)
         printf("\nTCPDCA:monitorRttArray(%g): arraySize = %d, mean = %g ",
                     Scheduler::instance().clock(),arraySize,*arrayMean);
#endif


    sum = 0;
    for (i=0; i<arraySize; i++) {
      if (rttAvg_[i] == 0)
         break;
      sum += (rttAvg_[i] - mean)*(rttAvg_[i] - mean);
    }
    var = sum /i;
    std = sqrt(var);

 *arrayStd = std;

}



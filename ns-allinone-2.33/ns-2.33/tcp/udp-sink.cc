/* -*-	Mode:C++; c-basic-offset:8; tab-width:8; indent-tabs-mode:t -*- */
/*
 * Copyright (c) 1991-1997 Regents of the University of California.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *	This product includes software developed by the Computer Systems
 *	Engineering Group at Lawrence Berkeley Laboratory.
 * 4. Neither the name of the University nor of the Laboratory may be used
 *    to endorse or promote products derived from this software without
 *    specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 *  Rev 
 *    
 *   $A602 : bug fix found by Shashank
 */

#ifndef lint
static const char rcsid[] =
    "@(#) $Header: /usr/src/mash/repository/vint/ns-2/udp-sink.cc,v 1.28 1998/08/24 19:39:45 tomh Exp $ (LBL)";
#endif

#include "flags.h"
#include "ip.h"
#include "udp-sink.h"

//$304
#define  TRACE_SINK_ID 9999
FILE* fp;

//To trace this module 
//#define TRACEUDPSINK 0
#include "../mac/docsis/docsisDebug.h"



#define TRACEID 1

class Application;

static class UdpSinkClass : public TclClass {
public:
	UdpSinkClass() : TclClass("Agent/UDPSink") {}
	TclObject* create(int, const char*const*) {
		return (new UdpSink());
	}
} class_udpsink;

UdpSink::UdpSink() : Agent(PT_UDP)
{
        UDPSinkThroughput_bytes_ = 0;
        bind("UDPSinkThroughput",&UDPSinkThroughput_bytes_);
        UDPSinkNumberBytesDelivered_ = 0;
        bind("UDPSinkNumberBytesDelivered",&UDPSinkNumberBytesDelivered_);
        sink_id_ = 0;
        bind("SinkId_",&sink_id_);
        UDPSinkTotalPktsReceived_ = 0;
        bind("UDPSinkTotalPktsReceived",&UDPSinkTotalPktsReceived_);
        UDPSinkNumberPktsOutOfOrder_ = 0;
        bind("UDPSinkPktsOutOfOrder",&UDPSinkNumberPktsOutOfOrder_);
        UDPSinkNumberPktsDropped_ = 0;
        bind("UDPSinkNumberPktsDropped",&UDPSinkNumberPktsDropped_);


        numberSamples = 0;
		totalJitterCount = 0;
		totalLatencyCount = 0;
		avgJitter = 0;
		avgLatency = 0;
        highestSeqNo = 0;
        OutOfOrderEvents = 0;

        bind("UDPAvgJitter",&avgJitter);
        bind("UDPAvgLatency",&avgLatency);

        jitter = 0;
        lastDelay = 0;
        nextSeqNo = 0;
        lossCount = 0;
        totalPackets =0;

}


void UdpSink::recv(Packet* p, Handler*)
{
  double now = Scheduler::instance().clock();
  double delay = 0;

  hdr_rtp* rh = hdr_rtp::access(p);
  hdr_ip* iph = hdr_ip::access(p);
  int fid_ = iph->flowid();



  if (p == NULL)
    return;

  totalPackets++;

//$A602
//  UDPSinkThroughput_bytes_ += hdr_cmn::access(p)->size();
  UDPSinkThroughput_bytes_ += hdr_cmn::access(p)->size();
  UDPSinkNumberBytesDelivered_ += (hdr_cmn::access(p)->size()-20);
  UDPSinkTotalPktsReceived_++;


#ifdef TRACEUDPSINK 
   printf("\nUDPSink:%d:sinkID:%d;(%lf)  packet arrived: %d, lastDelay:%lf, timestamp:%lf,  nextSeqNo:%d, highest seq no:%lf, #dropped:%d, #OutOfOrder:%d", 
      fid_,sink_id_,now,rh->seqno(),lastDelay, hdr_cmn::access(p)->timestamp(),nextSeqNo, highestSeqNo,
           UDPSinkNumberPktsDropped_, UDPSinkNumberPktsOutOfOrder_);
#endif

  if (rh->seqno() > highestSeqNo) {
    highestSeqNo = rh->seqno();
  }

//        if (fid_ == TRACEID) {
		//$A401
//          if (sink_id_ ==  TRACE_SINK_ID ) {
          if (lastDelay == 0) {
            lastDelay = now - hdr_cmn::access(p)->timestamp();
            nextSeqNo = rh->seqno()+1;


#ifdef TRACEUDPSINK 
           printf("\nUDPSink:(fid:%d, sinkID::%d)%lf Init nextSeqNo to %d, lastDelay:%lf, hdr TS:%lf", 
                fid_,sink_id_,now,nextSeqNo,lastDelay, hdr_cmn::access(p)->timestamp());
#endif
          }
          else {
            if (rh->seqno() == nextSeqNo) {
              delay = now - hdr_cmn::access(p)->timestamp();
              jitter = delay - lastDelay; 
              if (jitter < 0)
               jitter = -1 * jitter;
#ifdef TRACEUDPSINK 
              printf("\nUDPSink:(sinkID:%d)JITTER:%lf (lastDelay:%lf, delay:%lf),timeNow:%lf,hdr timestamp:%lf", 
                sink_id_,jitter,lastDelay,delay,now, hdr_cmn::access(p)->timestamp());
#endif
        
              numberSamples++;
              totalJitterCount+=jitter;
              totalLatencyCount+=lastDelay;
              avgJitter = totalJitterCount / numberSamples;
              avgLatency = totalLatencyCount / numberSamples;
		 
			  //$A401
			  //Create a single file with the aggregate of all CBR flows
              if (sink_id_ ==  TRACE_SINK_ID ) {
                fp = fopen("CBR.recv", "a+");
                   fprintf(fp,"\n%d %f %f %f  %d %d %d", 
                   sink_id_, Scheduler::instance().clock(),jitter, delay,
                   lossCount,totalPackets,nextSeqNo); 
                 fclose(fp);
              }

#if 0
                fp = fopen("CBRJitter.dat", "a+");
                fprintf(fp,"\n%d %f %f %f  %d %d %d", 
                  sink_id_,Scheduler::instance().clock(),jitter, delay,
                  lossCount,totalPackets,nextSeqNo); 
                fclose(fp);
#endif

              lastDelay = delay;
              nextSeqNo = rh->seqno()+1;
            }
            else {
              //Note: this breaks if packets arrive out of order....
              // We estimate the true number lost by tracking the
              // highest seq minus total received
              // We estimate the true out of order in the tcl dumpFinal script
              // 
              lossCount = lossCount + (rh->seqno() - nextSeqNo);
              nextSeqNo = rh->seqno()+1;

              OutOfOrderEvents++;
              UDPSinkNumberPktsDropped_ = highestSeqNo - UDPSinkTotalPktsReceived_ ;
			  if (UDPSinkNumberPktsDropped_ < 0)
                UDPSinkNumberPktsDropped_ = 0;
              UDPSinkNumberPktsOutOfOrder_=  OutOfOrderEvents - UDPSinkNumberPktsDropped_;
			  if (UDPSinkNumberPktsOutOfOrder_ < 0)
			    UDPSinkNumberPktsOutOfOrder_  = 0;


#ifdef TRACEUDPSINK 
              printf("\nUDPSink:(sinkID:%d)(fid:%d)(%lf): packet loss (arrived:%d)  lossCount=%d,  totalPackets=%d, numberDropped:%lf", 
                sink_id_,fid_,now,rh->seqno(),lossCount,totalPackets,nextSeqNo,UDPSinkNumberPktsDropped_);
#endif
            }
          }

#ifdef TRACEUDPSINK 
        if (fid_ == TRACEID) {
           printf("\nUDPSink(flowID:%d, sinkID:%d)(%f)Rxed uid: %d ,  New sinkthroughput is %d,timsstamp=%f", 
              fid_,sink_id_,now, hdr_cmn::access(p)->uid(), UDPSinkThroughput_bytes_,
               hdr_cmn::access(p)->timestamp());
        }
#endif
	if (app_)
		app_->recv(hdr_cmn::access(p)->size());
	/*
	 * didn't expect packet (or we're a null agent?)
	 */
	Packet::free(p);
}

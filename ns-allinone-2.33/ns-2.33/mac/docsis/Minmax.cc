#include <stdio.h>
#include "Minmax.h"

//Minmax Constructor
Minmax::Minmax(int num_flows, int num_channels, int **channel_map, int *flow_demands, int *channel_capacity) 
{
	//printf("%d\t%d\t%d\n", *(*(channel_map+2)+1), *flow_demands, *channel_capacity);
	computeAllocation(num_flows, num_channels, channel_map, flow_demands, channel_capacity);
}

void Minmax::init() 
{

}

//Minmax Member Functions
int Minmax::computeAllocation(int num_flows, int num_channels, int **channel_map, int *flow_demands, int *channel_capacity)
{
	int i, j;
        int channels[num_channels];         /* indicates channels with BW available */
        int flows_connected[num_flows];     /* flows with connections to those channels */
        int current_flow_demands[num_flows];
	
	//printf("%d\t%d\t%d\n", *(channel_map[2]+1), *flow_demands, *channel_capacity);
	make_flow_network(num_flows, num_channels, channels, flows_connected, current_flow_demands, channel_map, flow_demands, channel_capacity);

        /* find fair channel allocations */
        find_allocation(num_flows, num_channels, channels, flows_connected, current_flow_demands, channel_map, flow_demands, channel_capacity);

        /* print out the flow network for channel assignments */
        for (i=0; i<nodes; i++)
        {
                for (j=0; j<nodes; j++)
                        printf("%d  ", flow[i][j]);
                printf("\n");
        }
        printf("\n");

        return 0;
}

void Minmax::make_flow_network(int num_flows, int num_channels, int channels[], int flows_connected[], int current_flow_demands[], int **channel_map, int *flow_demands, int *channel_capacity)
{
        int i, j;

        /* set number of nodes in flow network */
        nodes = num_flows + num_channels + 2;  /* add 2 for s and t */

        /* set number of edges in flow network */
        edges = 0;
        for (i=0; i<num_flows; i++)             /* edges in bipartite graph */
                for (j=0; j<num_channels; j++)
                        if ( *(channel_map[i]+j) == 1 )
                                edges++;
        edges += num_flows;                             /* add edges from source (s) */
        edges += num_channels;                  /* add edges to sink (t) */

        /* zero capacity matrix */
        for (i=0; i<nodes; i++)
                for (j=0; j<nodes; j++)
                        capacity[i][j] = 0;

        /* load capacity matrix */
        for (i=0; i<num_flows; i++)
        {
                capacity[0][i+1] = *(flow_demands+i);     /* load source flows */
                for (j=0; j<num_channels; j++)          /* load bipartite flows */
		{
                        if (*(channel_map[i]+j) == 1)
                                capacity[i+1][num_flows+1+j] = INFINITE;
        		//printf("%d\t", *(*(channel_map+i)+j));
		}
	}

	//printf("%d\t%d\t%d\n", *(*(channel_map+2)+1), *flow_demands, *channel_capacity);
        for (j=0; j<num_channels; j++)                  /* load sink flows */
                capacity[num_flows+1+j][num_flows+1+num_channels] = *(channel_capacity+j);
}

void Minmax::find_allocation(int num_flows, int num_channels, int channels[], int flows_connected[], int current_flow_demands[], int **channel_map, int *flow_demands, int *channel_capacity)
{
        int i;
        int lo = 0;
        int hi = 0;
        int fair_share;

        for (i=0; i<num_flows; i++)
                current_flow_demands[i] = *(flow_demands+i);

        /* find highest request */
        for (i=0; i<num_flows; i++)
                if (current_flow_demands[i] > hi)
                        hi = current_flow_demands[i];

        /* get the initial "fair" level */
        fair_share = find_fair_allocation(lo, hi, num_flows, num_channels, channels, flows_connected, current_flow_demands);

        /* allocate any remaining BW */
        while ( bw_available(num_flows, num_channels, channels, flows_connected, current_flow_demands)
                        && flows_available(num_flows, num_channels, channels, flows_connected, current_flow_demands, channel_map, flow_demands, channel_capacity) )
        {
                /* set unaccessible flow capacities to previous fair level */
                /* or the demand level, if less                            */
                for (i=0; i<num_flows; i++)
                        if (flows_connected[i] == 0)
                        {
                                current_flow_demands[i] = min(fair_share, current_flow_demands[i]);
                                capacity[0][i+1] = current_flow_demands[i];
                        }

                /* find remaining highest request */
                hi = 0;
                for (i=0; i<num_flows; i++)
                        if (current_flow_demands[i] > hi)
                                hi =current_flow_demands[i];

                /* find new fair level for remaining connected flows */
                fair_share = find_fair_allocation(fair_share+1, hi, num_flows, num_channels, channels, flows_connected, current_flow_demands);
        }
}

int Minmax::find_fair_allocation(int low, int high, int num_flows, int num_channels, int channels[], int flows_connected[], int current_flow_demands[])
{
        int i;
        int fair_level;
        int last_fair;

        while (low <= high)
        {
                fair_level = (low + high) / 2;

                /* set source flow capacities to fair level OR requested amount */
                for (i=0; i<num_flows; i++)
                        capacity[0][i+1] = min(fair_level, current_flow_demands[i]);

                find_maxflow();

                if ( all_fair(fair_level) )
                {
                        last_fair = fair_level;
                        low = fair_level + 1;
                }
                else
                {
                        high = fair_level - 1;
                }
        }

        /* have to redo the last one that worked */
        for (i=0; i<num_flows; i++)
                capacity[0][i+1] = min(last_fair, current_flow_demands[i]);

        find_maxflow();

        return last_fair;
}

int Minmax::all_fair(int level)
{
        int i;

        for (i=0; i<nodes; i++)
        {   /* check each source flow */
                if (capacity[0][i+1] != 0)
                {
                        if (capacity[0][i+1] != flow[0][i+1])
                        {
                                if (flow[0][i+1] != level)
                                        return 0;
                        }
                }
        }

        return 1;
}

int Minmax::bw_available(int num_flows, int num_channels, int channels[], int flows_connected[], int current_flow_demands[])
{
        int i;
        int available = 0;

        /* clear all channels */
        for (i=0; i<num_channels; i++)
                channels[i] = 0;

        /* find channels with remaining BW */
        for (i=0; i<num_channels; i++)
        {
                if ( flow[num_flows+1+i][num_flows+1+num_channels] <
                         (0.98 * (float)capacity[num_flows+1+i][num_flows+1+num_channels]) )
                {    /* The 0.98 is a kludge to deal with rounding   */
                         /*  errors.  If we don't switch to float we may */
                         /*  need to look at this further                */
                        channels[i] = 1;
                        available = 1;
                }
        }

        return available;
}

int Minmax::flows_available(int num_flows, int num_channels, int channels[], int flows_connected[], int current_flow_demands[], int **channel_map, int *flow_demands, int *channel_capacity)
{
        int i, j;
        int available = 0;

        /* clear all flows */
        for (i=0; i<num_flows; i++)
                flows_connected[i] = 0;

        for (i=0; i<num_flows; i++)
        { /* for each flow */
                for (j=0; j<num_channels; j++)
                { /* check each channel */
                        if ( (*(channel_map[i]+j) == 1) &&
                                 (channels[j] == 1) &&
                                 (flow[i+1][num_flows+j+1] != *(flow_demands+i)) )
                        {
                                flows_connected[i] = 1;
                                available = 1;
                        }
                }
        }

        return available;
}

int Minmax::find_maxflow(void)
{
        int i, j;
        int u;
        int residual;
        int max_flow;

        /* initialize to 0 flow */
        max_flow = 0;
        for (i=0; i<nodes; i++)
                for (j=0; j<nodes; j++)
                        flow[i][j] = 0;


        /* while still an augmenting path, */
    /*  increase the flow on that path */
        while ( augmenting_path() )
        {
                residual = INFINITE;    /* find residual of augmenting path */
                for (u=nodes-1; augment[u]>=0; u=augment[u])
                        residual = min(residual, capacity[augment[u]][u] - flow[augment[u]][u]);

                /* increase the flow along the path by the residual amount*/
                for (u=nodes-1; augment[u]>=0; u=augment[u])
                {
                        flow[augment[u]][u] += residual;
                        flow[u][augment[u]] -= residual;
                }

                max_flow += residual;
        }

        return max_flow;
}

int Minmax::min(int x, int y)
{
        return (x<y ? x : y);
}

int Minmax::augmenting_path(void)
{
        int u, v;
        for (u=0; u<nodes; u++)
                color[u] = WHITE;
        head = 0;
        tail = 0;
        enqueue(0);  /* source node */
        augment[0] = -1;

        while (head != tail)
        {
                u = dequeue();
                for (v=0; v<nodes; v++)
                        if ( (color[v] == WHITE) &&
                                 (capacity[u][v] - flow[u][v] > 0) )
                        {
                                enqueue(v);
                                augment[v] = u;
                        }
        }

        /* if the sink node is BLACK we found a path */
        return (color[nodes-1] == BLACK);
}

void Minmax::enqueue(int x)
{
        q[tail] = x;
        tail++;
        color[x] = GRAY;
}

int Minmax::dequeue(void)
{
        int x = q[head];
        head++;
        color[x] = BLACK;
        return x;
}


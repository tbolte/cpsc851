/************************************************************************
* File:  BAQM.h
*
* Purpose:
*   Inlude files for a base list object. A BAQM is a container
*   of AQM Elements.
*
*
* Revisions:
*   $A602 : 10-27-2009: monitors avg packet queue delay
*   $A605:  12-17-2009 : monitor avg drop rate, support delay-based CA algo.
*
***********************************************************************/
#ifndef BAQM_h
#define BAQM_h

#include "ListObj.h"

//adaptation commands
#define RETURN_TO_DEFAULT 0
#define INCREASE_AQM_QUEUE 1
#define DECREASE_AQM_QUEUE 2
#define INCREASE_CONGESTION_AVOIDANCE 3
#define DECREASE_CONGESTION_AVOIDANCE 4
#define MOVE_TO_OPTIMAL 5

class Packet;

class BAQM : public ListObj {

public:
  virtual ~BAQM();
  BAQM();
  virtual void resetAQMParams();
  virtual void initList(int maxAQMSize);
  virtual int addElement(ListElement& element);
  virtual ListElement *removeElement();    //dequeue and return
  void setAQMParams(int maxAQMSize, int priority, int minth, int maxth, int adaptiveMode, double maxp, double weightQ);
  virtual void updateStats();
  virtual void printStatsSummary();
  virtual double getAvgQLatency();
  virtual double getAvgQ();
  virtual double getAvgAvgQ();
  virtual double getDropP();
  virtual double getAvgDropP();
  virtual double getAvgMaxP();

  virtual void channelIdleEvent();
  virtual void channelStatus(double accessDelayMeasure, double channelUtilizationMeasure, double consumptionMeasure);

  int maxthCONFIGURED;
  int minthCONFIGURED;
  int maxth;
  int minth;
  double maxp;
  double configuredMaxp;
  double highestMaxp;
  double lowestMaxp;
  double weightQ;
  double filterTimeConstant;
  double configuredWeightQ;
  int flowPriority;


  double freeze_time;
  double incr1;
  double dec1;
  double lastUpdateTime;

  double numberIncrements;
  double numberDecrements;
  double numberDrops;
  double numberTotalChannelIdleEvents;
  double numberValidChannelIdleEvents;

  //$A602
  virtual void newArrivalUpdate(Packet *p);
  double queueLatencyTotalDelay;
  double queueLatencySamplesCount;
  double queueLatencyMonitorTotalArrivalCount;
  double avgPacketLatency;
  //$A605
  double avgPacketLoss;

  double avgQ;
  double dropP;
  double q_time;
  double avgDropP;
  double avgDropPSampleCount;
  double avgAvgQ;
  double avgAvgQPSampleCount;
  double avgMaxP;
  double avgMaxPSampleCount;

protected:


private:

  int targetQueueLevel;
  double targetDelayLevel;
  double alpha;
  double beta;
  double  optimalMinth;
  double optimalMaxth;
  
  int count;
  int gentleMode;
  int adaptiveMode;
  double avgTxTime;


  //states:  0 : NOT initialized
  //states:  1 : No congestion....
  //states:  2 : Local congestion
  //states:  3 : Network congestion
  int stateFlag;
  double latencyToThroughputRatio;
  double updateFrequency;
  int bytesQueued;
  double avgServiceRate;
  double byteArrivals;
  double byteDepartures;
  double lastRateSampleTime;
  double rateWeight;
  double avgArrivalRate;
  double localVsNetworkCongestionRatio;

  //$A605
  double lastLossRateSampleTime;
  int  intervalPacketArrivals;
  int  intervalPacketsDropped;


  double BAQMAdaptationFrequency;
  double BAQMLastAdaptationTime;

  int    BAQMUpdateCountThreshold;
  int    BAQMUpdateCount;
  double BAQMLastSampleTime;
  double BAQMByteArrivals;
  double BAQMByteDepartures;
  double avgBAQMArrivalRate;
  double avgBAQMServiceRate;
  double BAQMRateWeight;
  double maxBAQMArrivalRate;
  double maxBAQMServiceRate;
  double avgServiceRateHistory;
  double avgArrivalRateHistory;


  double BAQMQueueDelaySamples;
  double BAQMQueueLevelSamples;
  double BAQMAccessDelaySamples;
  double BAQMConsumptionSamples;
  double BAQMChannelUtilizationSamples;
  double BAQMWeight;
  double avgBAQMAccessDelay;
  double avgBAQMConsumption;
  double avgBAQMChannelUtilization;
  double BAQMsensitivity;
  double algorithmSensitivity1;
  double algorithmSensitivity2;
  double algorithmSensitivity3;

  double lastavgBAQMArrivalRateHistory;
  double lastavgBAQMServiceRateHistory;
  double avgBAQMSRHistory;
  double avgBAQMARHistory;
  double BAQMHistoryWeight;
  double BAQMHistoryCount;


  //$A602
  virtual double updateAvgQLatency();

  //$A605
  double updateLossRateAvg();

  double updateAvgQ();
  void updateRates();
  double computeDropP();
  int packetDrop(double dropP);

  int adjustState(double channelUtilization, double avgPacketDelay);
  double levelOfMice();
  double serviceRateHistory(double avgBAQMServiceRate, double avgBAQMArrivalRate);
  void adaptAQMParams(int reactFlag, double sensitivityLevel);

};


#endif

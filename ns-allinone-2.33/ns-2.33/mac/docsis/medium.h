/***************************************************************************
 * Module: medium
 *
 * Explanation:
 *   This file contains the abstraction for the physical medium. 
 *
 * Revisions:
 *
 *  TODO:
 *     -there should be one static, global medium object.  Each MAC has
 *      a reference to it (myMedium).
 *     -We need the object to be accessible by tcl. So this means it is derived
 *      from NsObject  (I think). 
************************************************************************/

//#include "mac-docsis.h"
#include "docsisDebug.h"
#include "channelAbstraction.h"
#include "channelProperties.h"
#include "channelStats.h"
#include "bi-connector.h"
#include "config.h"
#include "serviceFlowMgr.h"

#ifndef ns_medium_h
#define ns_medium_h

#include "packet.h"
#include "rtp.h"
#include "hdr-docsis.h"

//class NodeListData;
class MacDocsisCMTS;
class bondingGroupMgr;
//class serviceFlowMgr;

struct mediumStatsType
{
  u_int32_t  numberTransmitFramesDS;
  u_int32_t  numberTransmitFramesUS;
  double  numberTransmitBytesDS;
  double  numberTransmitBytesUS;
  double  numberrecvHandlers;
  double  numberrecvHandlerMACs;
};

/*************************************************************************
 ************************************************************************/
//class medium : NsObject
class medium : public BiConnector {

	public:
	medium();
    //virtual ~medium();
    void init(MacDocsisCMTS *myMacParam);
	
	channelAbstraction* myChannel;
	int numChannels; 
	int numDChannels;
	int numUChannels;
	int thisChannelID; 
	int nodeCount;
	int lastpkt;

	inline int numberDSChannels() { return numDChannels; }
	inline int numberUSChannels() { return numUChannels; }
	
    virtual int transmitFrame(Packet *p, int channelNumber, MacDocsis *myMacPtr);
	void setupChannels(int);
//#JIT002
	void setupEachChannel(int ChID, int direction, u_int32_t dataRate, double propDelay, u_int32_t OHBytes,
			        int FECOverhead, u_int32_t ticksPerMinislot, u_int32_t maxBurst, double lossRate);

	void getChannelProperties(struct channelPropertiesType  *channelProperties, int channelNumber);

	int getOverHead(int dataSize, int channelNumber);
	int getOverHead(Packet *p, int channelNumber);
    void addNodeToUSChannel(void);
    void addNodeToDSChannel(void);
    void addNodeToUSChannelUP(int);
    void addNodeToDSChannelUP(int);
	void recvHandler(Packet*, int channelNumber, NodeListData *);
	void recvHandlerMAC(Packet*, int channelNumber, MacDocsis *);
	//virtual void transmit(Packet*, MacDocsis*, int);	
	void transmitDown(Packet*,int,int,MacDocsis*);
	int transmitUp(Packet *,MacDocsis*,int);
	int findIdleChannel(int);
	void linkBondingGroupMgr(bondingGroupMgr *);
	void linkDSserviceFlowMgr(serviceFlowMgr *);

	void printStatsSummary();
	void getChannelStats(struct channelStatsType  *channelStats, int channelNumber);


 protected:
	int	command(int argc, const char*const* argv);
	
	MacDocsis* node_;
	int index_;
	double bandwidth_;                  

 private:
    int numberNodes;
    int state;
    MacDocsisCMTS *myMac;
	bondingGroupMgr *myBGMgr;
	serviceFlowMgr *myDSSFMgr;
    struct mediumStatsType myStats;

};

#endif /* __ns_medium_h__ */

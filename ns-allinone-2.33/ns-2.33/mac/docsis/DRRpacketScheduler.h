/***************************************************************************
 * Module:  pDRRpacketScheduler  Object
 *
 * Explanation:
 *   This file contains the class definition for the base packet scheduler
 *   and for several specific types of PS's.
 *
 * Revisions:
 *
 *  TODO:
 *
************************************************************************/

#ifndef ns_pDRRpacketSchedulerObject_h
#define ns_pDRRpacketSchedulerObject_h


#include "packet.h"
#include "globalDefines.h"

#include "packetScheduler.h"



class pDRRpacketSchedulerObject: public packetSchedulerObject
{
public:
  pDRRpacketSchedulerObject();
  ~pDRRpacketSchedulerObject();

  void init(int serviceDiscipline, serviceFlowMgr*, bondingGroupMgr *);
  Packet *selectPacket(int channelNumber, struct serviceFlowSet *mySFSet); 
  int  selectChannel(Packet *pkt, serviceFlowObject *mySFObj, struct channelSet *myChannelSet, int *channelSelection);

  int currentActiveSetIndexArray[MAX_CHANNELS];
  int lastSFFlowIDSelectedArray[MAX_CHANNELS];

private:

  int advanceToNextSF(int channelNumber, struct serviceFlowSet *mySFSet);


};


#endif /* __ns_pDRRpacketSchedulerObject_h__ */

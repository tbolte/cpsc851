/*****************************************************************************
 *       This file contains Channel Timer class definitions. 
 *
 * Revisions:
 *
 ****************************************************************************/

#ifndef ns_channelTimers_h
#define ns_channelTimers_h

#include "mac-docsis.h"

//$A500
//#include "channelMgr.h"
//#include "reseqFlowListElement.h"
//#include "reseqMgr.h"
class channelMgr;
class reseqFlowListElement;
class reseqMgr;



/*=======================CLASS DEFINITIONS============================*/

/*========================TIMER CLASSES===============================*/


/*************************************************************************
Base class for all the timer classes
*************************************************************************/
class baseTimer : public Handler 
{
 public: 
    baseTimer();

    virtual ~baseTimer() {};

    virtual void handle(Event *e) {};
    
    void start(Event *e, double time);

    virtual void stop(Event *e);
    
    inline int busy(void) 
    { 
	  return busy_; 
    }
    
    inline double expire(void) 
    {
	return((stime + rtime) - Scheduler::instance().clock());
    }
    
    Event TimerEvent_; 

 protected:
    int		busy_;
    int 	paused_;
    double	stime;	// start time
    double	rtime;	// remaining time
};




class TxChannelTimer : public baseTimer 
{

 public:

  TxChannelTimer(channelMgr *m, int channelNumberParam);
  
  ~TxChannelTimer();

    void start(Packet *p,  double time);
    void handle(Event *e);

 protected:
    channelMgr 	*myChMgr;
    Packet *p;
	int channelNumber;
};


#endif /* __channelTimers_h__ */

/************************************************************************
* File:  serviceFlowListElement.h
*
* Purpose:
*   Inlude files for the mac List Elements
*
* Revisions:
***********************************************************************/
#ifndef serviceFlowListElement_h
#define serviceFlowListElement_h

#include "packet.h"
#include "rtp.h"
#include "hdr-docsis.h"

#include "ListElement.h"
#include "ListObj.h"


//$A500
//#include "serviceFlowObject.h"
class serviceFlowObject;

class serviceFlowListElement: public  ListElement
{

private:
  serviceFlowObject *mySF;

public:
  serviceFlowListElement();
  serviceFlowListElement(serviceFlowObject *mySF);
  virtual ~serviceFlowListElement();
  int putServiceFlow(serviceFlowObject *SFObjParam);
  serviceFlowObject * getServiceFlow();

};

#endif


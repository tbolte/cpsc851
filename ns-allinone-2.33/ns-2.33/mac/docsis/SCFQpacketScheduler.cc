/***************************************************************************
 * Module: pSCFQpacketSchedulerObject
 *
 * Explanation:
 *   This file contains the class definition of the packet scheduler. 
 *
 * Revisions:
 *
 *
************************************************************************/


#include "SCFQpacketScheduler.h"
#include "serviceFlowObject.h"
#include "channelAbstraction.h"

#include "docsisDebug.h"
//#define TRACEME 0
//#define FILES_OK 0




/*************************************************************************
 ************************************************************************/
pSCFQpacketSchedulerObject::pSCFQpacketSchedulerObject() : packetSchedulerObject()
{
    pkt_rng = new UniformRandomVariable(0, 1);
}

/*************************************************************************
 ************************************************************************/
pSCFQpacketSchedulerObject::~pSCFQpacketSchedulerObject()
{
    delete pkt_rng;
}


void pSCFQpacketSchedulerObject::init(int serviceDisciplineParam, serviceFlowMgr * mySFMgrP, bondingGroupMgr *myBGMgrP)
{
   double curTime = Scheduler::instance().clock();
   FILE *fmap;
   int num_channels;
   int num_flows;
   int i, j;
   char ch_list[MAX_CHANNELS];

  packetSchedulerObject::init(serviceDisciplineParam, mySFMgrP, myBGMgrP);

  printf("pSCFQpacketSchedulerObject::init(%lf):  serviceDisciplineParam:%d, actual:%d \n", curTime,serviceDisciplineParam,serviceDiscipline);

   // clear channel_load variable
   for (i=0; i<MAX_CHANNELS; i++)
      channel_load[i] = 0;

#if 0
   fmap = fopen("channelMAP.dat", "r");
   if (fmap == NULL)
   {
      printf("Can't open channelMAP.dat\n");
      return;
   }

   printf("channelMAP.dat open\n");

   fscanf(fmap, "%d", &num_channels);
   fscanf(fmap, "%d", &num_flows);

   for (i=0; i<num_flows; i++)
   {
      fscanf(fmap, "%s", ch_list);
      for (j=0; j<num_channels; j++)
      {
         if (ch_list[j] == '1')
            channel_load[j]++;
      }
   }
 
   fclose(fmap);
#endif
 
   if (serviceDiscipline == globalSCFQ)
   {
      int j;

      for(j = 0; j<MAX_ALLOWED_SERVICE_FLOWS; j++)
      {
         serviceTags[0][j] = IDLE;
      }

      virTime[0] = 0;
   }


   else if (serviceDiscipline == channelSCFQ)
   {
      int i, j;

      for (i=0; i<MAX_CHANNELS; i++)
      {
         for(j = 0; j<MAX_ALLOWED_SERVICE_FLOWS; j++)
         {
            serviceTags[i][j] = IDLE;
         }

         virTime[i] = 0;
      }
   }




  switch (serviceDiscipline) 
  {

    case SCFQ:
      printf("pSCFQpacketSchedulerObject::init(%lf):  SCFQ defaultQuantum: %d \n", curTime,defaultQuantum);
      break;
    case globalSCFQ:
      printf("pSCFQpacketSchedulerObject::init(%lf):  globalSCFQ: defaultQuantum: %d \n", curTime,defaultQuantum);
      break;
    case channelSCFQ:
      printf("pSCFQpacketSchedulerObject::init(%lf):  channelSCFQ: defaultQuantum: %d \n", curTime,defaultQuantum);
      break;
    default:
      printf("pSCFQpacketSchedulerObject::init(%lf):  Unknown SCFQ discipline?? %d \n", curTime,serviceDiscipline);
      break;
  }


}


/***********************************************************************
*function: Packet *pSCFQpacketSchedulerObject::selectPacket(int channelNumber, struct serviceFlowSet *mySFSet)
*
*explanation: This method selects the next packet to be sent over the channel
*  specified by channelNumber. The routine is passed a set of eligible SFs in mySFSet.
*
* 
*inputs:
*  int channelNumber: specifies the channel
*  struct serviceFlowSet *mySFSet : Set of SF object handles that are associated with a BG
*         that includes the channelNumber
*
*outputs:
*  Returns a SUCCESS or FAILURE
*
*  The PS.out trace file format:
*    fprintf(fp,"%lf %d %d %d %12.6f",curTime,channelNumber,mySFSetCount,mySFCount,virTime);
*        fprintf(fp," (%d,%d,%12.6f);  ",tmpSFObj->flowID,tmpSFObj->packetsQueued(),
*    fprintf(fp," (selectedObj: OK: %d,%d);  ",selectedSFObj->flowID,selectedSFObj->packetsQueued());
*                                       serviceTags[channelNumber][tmpSFObj->flowID]);
*          11 475462.000000 (5,256,475454.000000);   (7,256,475454.000000);   (8,71,475469.000000);   
*                           (9,0,475454.000000);   (10,1,475446.000000);   (12,1,475437.000000);   (13,0,475461.000000);   (selectedObj: OK: 12,0);
*
***********************************************************************/
Packet *pSCFQpacketSchedulerObject::selectPacket(int channelNumber, struct serviceFlowSet *mySFSet)
{
   int i, j;
   double pktTxTime;
   int all_idle;
   int mySFSetCount = mySFSet->numberMembers;
   int mySFCount = mySFMgr->numberSFs;
   Packet *pkt = NULL;
   serviceFlowObject *selectedSFObj = NULL;
   double curTime = Scheduler::instance().clock();
   int  bestSf = -1;
   int bestSfFlowid = 0;
   //must be larger than largest simulation time
   double bestTag = 100000000;
   double bw_factor;

#ifdef FILES_OK
   FILE *fp;
   fp = fopen("PS.out", "a+");
#endif

#ifdef TRACEME 
 printf("SCFQpacketSchedulerObject::selectPacketlocal(%lf):(channelNumber:%d) SF set count:%d, totalSFs:%d \n", 
   curTime,channelNumber,mySFSetCount,mySFCount);
  if (mySFSetCount >0) {
    for (i=0;i<mySFSetCount;i++) {
      printf("  myflow %x flowdId %d and serviceTags %f \n",mySFSet->SFObjPtrs[i],mySFSet->SFObjPtrs[i]->flowID,serviceTags[0][mySFSet->SFObjPtrs[i]->flowID]);
    }
 }
#endif

#ifdef FILES_OK
  if (mySFSetCount >0) {
    fprintf(fp,"%lf %d %d %d ",curTime,channelNumber,mySFSetCount,mySFCount);
    serviceFlowObject *tmpSFObj = NULL;
    for (i=0;i<mySFSetCount;i++) {
      tmpSFObj = mySFSet->SFObjPtrs[i];
      if (tmpSFObj != NULL)
        fprintf(fp," (%d,%d);  ",tmpSFObj->flowID,tmpSFObj->packetsQueued());
      else
        fprintf(fp," (ERROR); ");
    }
  }
#endif



/***  global SCFQ code  ***/
if (serviceDiscipline == globalSCFQ)
{
   if (mySFSetCount > 0) 
   {  // there are flows with access to this channel
      serviceFlowObject *tmpSFObj = NULL;
      for (i=0;i<mySFSetCount;i++) 
      {  // for each flow with access to this channel
         tmpSFObj = mySFSet->SFObjPtrs[i];
         if(tmpSFObj->packetsQueued() > 0)
         {  // if queue is not empty
	    if(serviceTags[0][tmpSFObj->flowID] == IDLE)
            {  // this flow has been idle and is restarting
               pktTxTime = getTxTime(tmpSFObj, channelNumber);
               serviceTags[0][tmpSFObj->flowID] = virTime[0] + pktTxTime;
            }    
            
            if (serviceTags[0][tmpSFObj->flowID] < bestTag)
	    {
	       bestTag = serviceTags[0][tmpSFObj->flowID];
               bestSf = i;  //Remember flowid and i are different
               bestSfFlowid = tmpSFObj->flowID;
            }
         }  // end if queue is not empty
      }  // end for each flow
   }  // end if flows > 0


   if(bestTag != IDLE)
   {  // we found an active flow on this channel
      selectedSFObj = mySFSet->SFObjPtrs[bestSf];
      pkt = selectedSFObj->removePacket();
      virTime[0] = bestTag;
     
      // check to see if there is another pkt in the queue
      if (selectedSFObj->packetsQueued() > 0)
      {  // set serviceTag for next packet
         pktTxTime = getTxTime(selectedSFObj, channelNumber);
         serviceTags[0][selectedSFObj->flowID] += pktTxTime;
      }
      else
      {  // queue is empty, mark as idle
         serviceTags[0][selectedSFObj->flowID] = IDLE;
      }
   }
 
   // check to see if ALL flows are idle
   all_idle = 1;
   for (j=0; j<MAX_ALLOWED_SERVICE_FLOWS; j++)
   {
      if (serviceTags[0][j] != IDLE)
      {
         all_idle = 0;
         break;
      }
   }
   if (all_idle == 1)
      virTime[0] = 0;
}

/***  channel SCFQ code  ***/
else if (serviceDiscipline == channelSCFQ)
{
   if (mySFSetCount > 0) 
   {  // there are flows with access to this channel
      serviceFlowObject *tmpSFObj = NULL;
      int findex = mySFSetCount * pkt_rng->value();
      for (i=0;i<mySFSetCount;i++) 
      {  // for each flow with access to this channel
         if (findex == mySFSetCount)
         {
             findex = 0;
         }
         tmpSFObj = mySFSet->SFObjPtrs[findex];
         if(tmpSFObj->packetsQueued() > 0)
         {  // if queue is not empty
	    if(serviceTags[channelNumber][tmpSFObj->flowID] == IDLE)
            {  // this flow has been idle and is restarting
               pktTxTime = getTxTime(tmpSFObj, channelNumber);
               for (j=0; j<MAX_CHANNELS; j++)
                  serviceTags[j][tmpSFObj->flowID] = virTime[j] + pktTxTime;
            }    

#ifdef FILES_OK
            fprintf(fp," %d-%lf", tmpSFObj->flowID, serviceTags[channelNumber][tmpSFObj->flowID]);
#endif
            
            if (serviceTags[channelNumber][tmpSFObj->flowID] < bestTag)
	    {
	       bestTag = serviceTags[channelNumber][tmpSFObj->flowID];
               bestSf = findex;  //Remember flowid and i are different
               bestSfFlowid = tmpSFObj->flowID;
            }
         }  // end if queue is not empty
         findex++;
      }  // end for each flow
   }  // end if flows > 0


   if(bestTag != IDLE)
   {  // we found an active flow on this channel
      selectedSFObj = mySFSet->SFObjPtrs[bestSf];
      pkt = selectedSFObj->removePacket();
      virTime[channelNumber] = bestTag;
     
      // check to see if there is another pkt in the queue
      if (selectedSFObj->packetsQueued() > 0)
      {  // set serviceTag for next packet
         pktTxTime = getTxTime(selectedSFObj, channelNumber);
         for (j=0; j<MAX_CHANNELS; j++)
            serviceTags[j][selectedSFObj->flowID] += pktTxTime;
      }
      else
      {  // queue is empty, mark as idle
         for (j=0; j<MAX_CHANNELS; j++)
            serviceTags[j][selectedSFObj->flowID] = IDLE;
      }
   }
 
   // check to see if ALL flows are idle
   all_idle = 1;
   for (j=0; j<MAX_ALLOWED_SERVICE_FLOWS; j++)
   {
      if (serviceTags[channelNumber][j] != IDLE)
      {
         all_idle = 0;
         break;
      }
   }
   if (all_idle == 1)
      virTime[channelNumber] = 0;
}
 

  fflush(stdout);


#ifdef FILES_OK
  if (selectedSFObj != NULL) {
    fprintf(fp," (selectedObj: OK: %d,%d);  ",selectedSFObj->flowID,selectedSFObj->packetsQueued());
  }
  else
    fprintf(fp," (selectedObj: NONE);  ");
  fprintf(fp,"\n");

  fclose(fp);
#endif

 
   return pkt;

}



double pSCFQpacketSchedulerObject::getTxTime(serviceFlowObject *tmpSFObj, int channelNumber)
{
   double bw_factor;
   Packet *SFPkt = tmpSFObj->getPacket(0);
   struct hdr_cmn *hdr = HDR_CMN(SFPkt);
   int pktSize = hdr->size();
   if (channel_load[channelNumber] == 0)
      bw_factor = 1.0;
   else
      bw_factor = 1.0 / (double)channel_load[channelNumber];
   return (double)(pktSize*8) / (double)CHANNEL_CAPACITY * bw_factor;
}




/***********************************************************************
*function: int pSCFQpacketSchedulerObject::selectChannel(Packet *pkt, serviceFlowObject *mySFObj, struct channelSet *myChannelSet, int *channelSelection)
*
*explanation: This method selects the channel to use for the pkt transmission.
*             The logic is simple: select the first available channel in the channel set
* 
*inputs:
*  Packet *pkt : the pkt that is to be sent
*  serviceFlowObject *mySFObj : the SF associated with the packet
*  struct channelSet *myChannelSet : the set of channels in the BG assigned to the SF
*  int *channelSelection : This routine will fill in this with the selected channel
*
*outputs:
*  Returns a SUCCESS or FAILURE
*
*******************************************************************/
int pSCFQpacketSchedulerObject::selectChannel(Packet *pkt, serviceFlowObject *mySFObj, struct channelSet *myChannelSet, int *channelSelection)
{
  int channelNumber = 0;
  int rc = FAILURE;
  int myChannelSetCount = myChannelSet->numberMembers;
  double curTime = Scheduler::instance().clock();
  int i;


#ifdef TRACEME 
   printf("SCFQpacketSchedulerObject::selectChannel(%lf): Need to select channel,  channel set count:%d \n", curTime,myChannelSetCount);
  if (myChannelSetCount >0) {
    for (i=0;i<myChannelSetCount;i++) {
      printf("   %x ",myChannelSet->channels[i]);
    }
    printf("\n");
  }
#endif 

  if (myChannelSetCount >0) {
    for (i=0;i<myChannelSetCount;i++) {
      channelNumber = myChannelSet->channels[i];
     if (mySFMgr->myPhyInterface->getChannelStatus(channelNumber) == CHANNEL_IDLE) {
        rc = SUCCESS;
        break;
#ifdef TRACEME 
 printf("SCFQpacketSchedulerObject::selectChannel(%lf): First IDLE channel: %d\n", curTime,channelNumber);
#endif 
      }
    }
  }

  if ((channelSelection != NULL) && (rc == SUCCESS))
  {
    *channelSelection = channelNumber;
  }
  else
    *channelSelection = -1;



#ifdef TRACEME 
   if (rc == SUCCESS)
     printf("SCFQpacketSchedulerObject::selectChannel(%lf): Return SUCCESS channelSelection:%d \n", curTime,*channelSelection);
   else
     printf("SCFQpacketSchedulerObject::selectChannel(%lf): Return FAILURE channelSelection:%d \n", curTime,*channelSelection);
#endif
 
 return rc;

}



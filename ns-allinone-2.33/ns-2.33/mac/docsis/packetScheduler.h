/***************************************************************************
 * Module:  packetScheduler  Object
 *
 * Explanation:
 *   This file contains the class definition for the base packet scheduler
 *   and for several specific types of PS's.
 *
 * Revisions:
 *
 *  TODO:
 *
************************************************************************/

#ifndef ns_packetSchedulerObject_h
#define ns_packetSchedulerObject_h


#include "packet.h"
//#include "serviceFlowMgr.h"
#include "globalDefines.h"

#define BestPerspective 0
#define GlobalPerspective 1
#define ChannelPerspective 2
#define BondingGroupPerspective 3


class  serviceFlowMgr;
class  bondingGroupMgr;
class serviceFlowObject;

struct packetSchedulerStatsType
{
  double  numberSchedulingRequests;
};



/*************************************************************************
 ************************************************************************/
class packetSchedulerObject
{

public:
  packetSchedulerObject();
  virtual ~packetSchedulerObject();

  virtual void init(int serviceDiscipline, serviceFlowMgr*, bondingGroupMgr *);
//  virtual Packet *selectPacketLocal(int channelNumber, struct serviceFlowSet *mySFSet);
//  virtual Packet *selectPacketGlobal(int channelNumber, struct serviceFlowSet *mySFSet);
//  virtual Packet *selectPacketChannel(int channelNumber, struct serviceFlowSet *mySFSet);
//  virtual Packet *selectPacketBG(int channelNumber, struct serviceFlowSet *mySFSet);
  virtual Packet *selectPacket(int channelNumber, struct serviceFlowSet *mySFSet);
  virtual int  selectChannel(Packet *pkt, serviceFlowObject *mySFObj, struct channelSet *myChannelSet, int *channelSelection);

  virtual int adaptPacketScheduler(int opCode);
  int serviceDiscipline;
  int defaultQuantum;
  serviceFlowMgr *mySFMgr;
  bondingGroupMgr *myBGMgr;

protected:

private:

  packetSchedulerStatsType myStats;

};

class FCFSpacketSchedulerObject: public packetSchedulerObject
{
public:
  FCFSpacketSchedulerObject();
  ~FCFSpacketSchedulerObject();

  void init(int serviceDiscipline, serviceFlowMgr*, bondingGroupMgr *);
  Packet *selectPacket(int channelNumber, struct serviceFlowSet *mySFSet); 
  int  selectChannel(Packet *pkt, serviceFlowObject *mySFObj, struct channelSet *myChannelSet, int *channelSelection);


private:
  int numberSFs;
  int lastSFsent;



};

class RRpacketSchedulerObject: public packetSchedulerObject
{
public:
  RRpacketSchedulerObject();
  virtual ~RRpacketSchedulerObject();

  void init(int serviceDiscipline, serviceFlowMgr*, bondingGroupMgr *);
  virtual Packet *selectPacketLocal(int channelNumber, struct serviceFlowSet *mySFSet); 
//  virtual Packet *selectPacketLocal(int channelNumber, struct serviceFlowSet *mySFSet); 
  virtual Packet *selectPacket(int channelNumber, struct serviceFlowSet *mySFSet); 
  virtual int  selectChannel(Packet *pkt, serviceFlowObject *mySFObj, struct channelSet *myChannelSet, int *channelSelection);

  int nextSFToService;
  int lastSetIndexSelected;
  int lastSFFlowIDSelected;
  int lastSetIndexSelectedArray[MAX_CHANNELS];
  int lastSFFlowIDSelectedArray[MAX_CHANNELS];

private:

};


class DRRpacketSchedulerObject: public packetSchedulerObject
{
public:
  DRRpacketSchedulerObject();
  ~DRRpacketSchedulerObject();

  void init(int serviceDiscipline, serviceFlowMgr*, bondingGroupMgr *);
  Packet *selectPacket(int channelNumber, struct serviceFlowSet *mySFSet); 
  int  selectChannel(Packet *pkt, serviceFlowObject *mySFObj, struct channelSet *myChannelSet, int *channelSelection);
  int adaptPacketScheduler(int opCode);

  int currentActiveSetIndexArray[MAX_CHANNELS];
  int lastSFFlowIDSelectedArray[MAX_CHANNELS];


private:

  int advanceToNextSF(int channelNumber, struct serviceFlowSet *mySFSet);


};


class SCFQpacketSchedulerObject: public packetSchedulerObject
{
public:
  SCFQpacketSchedulerObject();
  ~SCFQpacketSchedulerObject();

  void init(int serviceDiscipline, serviceFlowMgr*, bondingGroupMgr *);
  Packet *selectPacket(int channelNumber, struct serviceFlowSet *mySFSet); 
  int  selectChannel(Packet *pkt, serviceFlowObject *mySFObj, struct channelSet *myChannelSet, int *channelSelection);
  int adaptPacketScheduler(int opCode);

private:

  int currentActiveSetIndexArray[MAX_CHANNELS];
  int lastSFFlowIDSelectedArray[MAX_CHANNELS];
  
  double virTime;
  double serviceTags[MAX_CHANNELS][MAX_ALLOWED_SERVICE_FLOWS];

  int advanceToNextSF(int channelNumber, struct serviceFlowSet *mySFSet);
  void updateServiceTag (int channelNumber, int bestSfFlowid, int channelSfSum, double virTime);

};

#endif /* __ns_packetSchedulerObject_h__ */

/***************************************************************************
 * Module: medium
 * 
 * Explanation:
 *   This file contains the medium object.
 *
 * Methods:
 *
 *
 * Revisions:
*   $A903 : bug in vlan.tcl- calls to :command "add-node-to-ds-channel" had bad third param
*
************************************************************************/
#include "medium.h"

#include "mac-docsis.h"
#include <math.h>

//uncomment for printf's
#include "docsisDebug.h"
//#define TRACEME 0

static class mediumClass: public TclClass {
	public:
		mediumClass() : TclClass("Medium") {}
		TclObject* create(int, const char*const*) {
			return (new medium);
		}
} class_medium;


medium::medium() : BiConnector() {
#ifdef TRACEME
	printf("medium:constructor %x\n", this);
#endif
	bandwidth_ = 0.0;
	nodeCount = 0;
	node_ = 0;
	bind_bw("bandwidth_", &bandwidth_);
	numDChannels = 0;
	numUChannels = 0;
	numChannels = 0;
	bzero((void *)&myStats,sizeof(struct mediumStatsType));

}


int medium::command(int argc, const char*const* argv) {
  int channelNumberParam = 0;

#ifdef TRACEME
	printf("medium:command: The argument count is :%d\n (cmd:%s)",argc,argv[1]);
#endif
	if (argc == 2) {
		Tcl& tcl = Tcl::instance();

		if(strcmp(argv[1], "id") == 0) {
			tcl.resultf("%d", index_);
			return TCL_OK;
		}
	}
	else if(argc == 3) {
		if (strcmp(argv[1], "setup-channels") == 0) {
	        setupChannels(atoi(argv[2]));
		    return TCL_OK;
		}

		TclObject *obj;
		if( (obj = TclObject::lookup(argv[2])) == 0) {
			fprintf(stderr, "%s lookup failed\n", argv[1]);
			return TCL_ERROR;
		}
		if (strcmp(argv[1], "node") == 0) {
			assert(node_ == 0); 
			node_ = (MacDocsis*) obj;
			return TCL_OK;
		}
		else if (strcmp(argv[1], "add-node-to-ds-channel") == 0) {
			node_ = (MacDocsis *)obj;

//$A903
#ifdef TRACEME
            printf("medium:command:  add-node-to-ds-channel: add all DS channels to node\n");
#endif
			addNodeToDSChannel();
			return TCL_OK;
		}
		else if (strcmp(argv[1], "add-node-to-us-channel") == 0) {
			node_ = (MacDocsis *)obj;
//$A903            channelNumberParam = atoi(argv[3]);
#ifdef TRACEME
            printf("medium:command:  add-node-to-us-channel:  add all US channels to this node  \n");
#endif
			addNodeToUSChannel();
			return TCL_OK;
		}
		else if (strcmp(argv[1], "link-bonding-group-manager") == 0) {
			linkBondingGroupMgr((bondingGroupMgr *)obj);
			return TCL_OK;
		}
	}
	else if(argc == 4) {
		TclObject *obj;
		if( (obj = TclObject::lookup(argv[2])) == 0) {
			fprintf(stderr, "%s lookup failed\n", argv[1]);
			return TCL_ERROR;
		}
		if (strcmp(argv[1], "add-node-to-us-channel-up") == 0) {
			node_ = (MacDocsis *)obj;
            channelNumberParam = atoi(argv[3]);
#ifdef TRACEME
            printf("medium:command:  add-node-to-us-channel-up:  channel is %d \n",channelNumberParam);
#endif
			addNodeToUSChannelUP(channelNumberParam);
			return TCL_OK;
		}
		else if (strcmp(argv[1], "add-node-to-ds-channel-up") == 0) {
			node_ = (MacDocsis *)obj;
            channelNumberParam = atoi(argv[3]);
#ifdef TRACEME
            printf("medium:command:  add-node-to-ds-channel-up:  channel is %d \n",channelNumberParam);
#endif
			addNodeToDSChannelUP(channelNumberParam);
			return TCL_OK;
		}
	}
	else if(argc == 5) {
	}
	else if(argc == 11) {
		if (strcmp(argv[1], "setup-each-channel") == 0) {
			setupEachChannel(atoi(argv[2]),atoi(argv[3]),atoi(argv[4]),atof(argv[5]),
						atoi(argv[6]),atoi(argv[7]),atoi(argv[8]),atoi(argv[9]),atof(argv[10]));
			return TCL_OK;
		}
	}
	return BiConnector::command(argc, argv);
}

void medium::getChannelStats(struct channelStatsType  *channelStats, int channelNumber)
{
 double curr_time = Scheduler::instance().clock();

#ifdef TRACEME
	printf("medium::getChannelStats(%lf)(channel:%d): \n",curr_time,channelNumber);
#endif

	//Update the utilization stats
    myChannel[channelNumber].myStats.channelUtilization = myChannel[channelNumber].getChannelUtilization();


	*channelStats = myChannel[channelNumber].myStats;
}


void medium::printStatsSummary()
{
//#ifdef TRACEME
	printf("medium::printStatsSummary: numberTransmitFramesDS:%d (bytes:%f), numberTransmitFramesUS:%d (bytes:%f), numberrecvHandlers:%f, numberrecvHandlerMACs:%f \n",
			        myStats.numberTransmitFramesDS,myStats.numberTransmitBytesDS,myStats.numberTransmitFramesUS,myStats.numberTransmitBytesUS, myStats.numberrecvHandlers,myStats.numberrecvHandlerMACs);
//#endif
}



void medium::init(MacDocsisCMTS *myMacParam)
{
#ifdef TRACEME
  printf("medium::init()(%lf): init medium!\n", Scheduler::instance().clock());
#endif
  myMac = myMacParam;

  myMac->setBGMgr(myBGMgr);

  numberNodes = 0;
  state =  CHANNEL_IDLE;
}

int medium::transmitFrame(Packet *p, int channelNumber, MacDocsis *myMacPtr)
{
	int rc  = 0;
	struct hdr_cmn *hdr = HDR_CMN(p);	
   	struct hdr_docsis* dh = HDR_DOCSIS(p);
	struct hdr_mac *mh = HDR_MAC(p);
	int origSize = hdr->size();
	int overhead;

	overhead = getOverHead(origSize,channelNumber);
	hdr->size() +=  overhead;

#if 0
    struct channelPropertiesType  channelProperties;
    getChannelProperties(&channelProperties,channelNumber);
	int x = (int) (ceil(hdr->size() * (channelProperties.FECPercentage/100)));
	hdr->size() +=  x;
	hdr->size() += channelProperties.overheadBytes;

#endif
	/*
	 * Handle outgoing packets
	 */
#ifdef TRACEME
  printf("medium::transmitFrame(%lf)[channelNumber:%d): orig size:%d, overhead:%d , new size : %d\n", 
		  Scheduler::instance().clock(),channelNumber,origSize,overhead,hdr->size());
#endif

	if(channelNumber < numDChannels) {
#ifdef TRACEME
			printf("medium: calling transmitDOWN %x CMTS -> CM\n",p);
#endif
	        myStats.numberTransmitFramesDS++;
	        myStats.numberTransmitBytesDS+= hdr->size();
			transmitDown(p,channelNumber,0,myMacPtr);
			return rc;
	} else {
#ifdef TRACEME
			printf("medium: calling transmitDOWN %x CM -> CMTS\n",p);
#endif
	        myStats.numberTransmitFramesUS++;
	        myStats.numberTransmitBytesUS+= hdr->size();
			transmitDown(p,channelNumber,1,myMacPtr);
			return rc;
	}
}

void medium::transmitDown(Packet *p, int channelN,int direction, MacDocsis *myMacPtr)
{
	struct hdr_cmn *hdr = HDR_CMN(p);
	double stime;
	NodeListData *temp;
	myChannel[channelN].mySendDownNodeList.setCurHeadElement();
	temp = myChannel[channelN].mySendDownNodeList.accessCurrentElement();
	int count;
	
	//$A401
	stime = myChannel[channelN].TX_Time(p);
	if(direction == CHANNEL_DIRECTION_UP) 
	{
	  //UPSTREAM
	  if(myChannel[channelN].getTxState() != CHANNEL_IDLE) {
			myChannel[channelN].myStats.numberCollisions++;
#ifdef TRACEME
            printf("medium::transmitDown(channelNumber:%d)(%lf): mac %s  NOT CHANNEL_IDLE  UPSTREAM COLLISION (total:%f), pkt size:%d \n", 
			 channelN,Scheduler::instance().clock(), myMacPtr->docsis_id, myChannel[channelN].myStats.numberCollisions,hdr->size());
#endif
//$A701
         if (hdr->size()> 17) {
            printf("medium::transmitDown(channelNumber:%d)(%lf): mac %s  HARD ERROR NOT CHANNEL_IDLE  UPSTREAM COLLISION (total:%f), pkt size:%d \n", 
			 channelN,Scheduler::instance().clock(), myMacPtr->docsis_id, myChannel[channelN].myStats.numberCollisions,hdr->size());
         }

         Packet::free(p);
         return;
	  }
		for(count = myChannel[channelN].mySendDownNodeList.getListSize(); count > 0;count--) {
			temp = myChannel[channelN].mySendDownNodeList.nextElement();
			if(temp->NodeListDataMac() == myMacPtr)
				temp->startCmTxDocsistimer(p,stime);
		}
#ifdef TRACEME
		printf("medium:transmitDOWN: starting the CmTxDocsisTimer Packet(size:%d) :%x medium %x, to fire at %lf\n",hdr->size(),p, this, stime);
#endif
	}
	else if(direction == CHANNEL_DIRECTION_DOWN)
	//DOWNSTREAM
	{
//$A801
//  docsis_dsehdr *chx = docsis_dsehdr::access(p);
//      printf("medium::transmitDOWN:(channelNumber:%d)(%lf): DOWNSTREAM pkt PSN:%d, pkt dsid:%d \n", 
//			   channelN,Scheduler::instance().clock(),chx->packet_sequence_number,chx->dsid);
#ifdef TRACEME
		printf("medium:transmitDOWN: starting the CmtsTxDocsisTimer Packet(size:%d) :%x, to fire at %lf\n",hdr->size(),p,stime);
#endif
		temp->startCmtsTxDocsistimer(p,stime);
	}
	else {
      printf("medium::transmitDOWN:channelNumber:%d)(%lf):  ERROR: direction is %d\n", 
			   channelN,Scheduler::instance().clock(),direction);
  	  exit(1);
	}

	myChannel[channelN].setTxStateSEND();
	myChannel[channelN].sendUp(p);
}



int medium::transmitUp(Packet *p,MacDocsis *obj,int direction)
{

}


void medium::recvHandlerMAC(Packet *p,int channelNumber, MacDocsis *mac)
{
	struct hdr_cmn *hdr = HDR_CMN(p);	
   	struct hdr_docsis* dh = HDR_DOCSIS(p);
	struct hdr_mac *mh = HDR_MAC(p);
    struct channelPropertiesType  channelProperties;


	myStats.numberrecvHandlerMACs++;

    getChannelProperties(&channelProperties,channelNumber);
	hdr->size() -= channelProperties.overheadBytes;
	int x =  (int) floor(hdr->size() / (1.0 + (channelProperties.FECPercentage/100)));
	hdr->size() = x;

#ifdef TRACEME
    printf("medium::recvHandlerMAC(channelNumber:%d)(%lf): calling mac %s (lossRate1:%2.3f)\n", 
    channelNumber,Scheduler::instance().clock(),mac->docsis_id,channelProperties.errorModelLossRate1);
#endif

    double randomValue  = Random::uniform(0,1);
    if (randomValue <= channelProperties.errorModelLossRate1) {
#ifdef TRACEME
      docsis_dsehdr *chx = docsis_dsehdr::access(p);
      printf("medium::recvHandlerMAC(channelNumber:%d)(%lf): Simulate packet loss: PSN:%d  random_value:%3.3f\n", 
          channelNumber,Scheduler::instance().clock(), chx->packet_sequence_number,randomValue);
#endif
      Packet::free(p);
    }
    else {
	  mac->myPhyInterface->RecvFrame(p, channelNumber);
    }
}


void medium::recvHandler(Packet *p, int channelNumber, NodeListData *temp)
{

	myStats.numberrecvHandlers++;


	//$A401
	double rxtime = myChannel[channelNumber].TX_Time(p);
//	MacDocsis *tempMac = temp->NodeListDataMac();
//	if ( strcmp(tempMac->docsis_id, "CMTS") == 0 ) {
//		rxtime = TX_Time(p, UPSTREAM);
//	} else {
//		rxtime = TX_Time(p, DOWNSTREAM);
//	}
	rxtime = rxtime - 0.00000091;
	if(rxtime <= 0)
		rxtime = 0.0000001;

	temp->startRxDocsistimer(p,rxtime);
}

void medium::setupChannels(int numberChannels){
	int i;
	int channelN = 0;
	numChannels = numberChannels;
    myChannel = new channelAbstraction[numChannels];

	for(i = 0; i<numChannels; i++)
	{
		myChannel[i].init(i);
		myChannel[i].setNodeCount(nodeCount);
	}
}


void medium::getChannelProperties(struct channelPropertiesType  *channelProperties, int channelNumber)
{
  int channelIndex = channelNumber;
  myChannel[channelNumber].getChannelProperties(channelProperties);
}


void medium::setupEachChannel(int ChID, int direction, u_int32_t dataRate, double propDelay, u_int32_t OHBytes, 
		int FECOverhead, u_int32_t ticksPerMinislot, u_int32_t maxBurst, double lossRate) 
{
	myChannel[ChID].setDirection(direction);
	myChannel[ChID].setChannelProperties(ChID,direction,dataRate,propDelay,OHBytes,FECOverhead,ticksPerMinislot,maxBurst,lossRate);
	if (direction == CHANNEL_DIRECTION_DOWN)
		numDChannels++;
	else
		numUChannels++;
#ifdef TRACEME //-------------------------------------------------------------------------
	if (direction == CHANNEL_DIRECTION_DOWN) {
#ifdef TRACEME
      printf("medium::setupEachChannel;(%lf): setup a downstream channel number:%d, dataRate:%ld, propDelay:%lf OHBytes:%d, ticksPerMiniSlot:%d\n", 
	     Scheduler::instance().clock(),ChID,dataRate,propDelay,OHBytes,ticksPerMinislot);
#endif
	} else {
#ifdef TRACEME
      printf("medium::setupEachChannel;(%lf): setup a upstream channel number:%d, dataRate:%ld, propDelay:%lf, OHBytes:%d, ticksPerMiniSlot:%d \n", 
	     Scheduler::instance().clock(),ChID,dataRate,propDelay,OHBytes,ticksPerMinislot);
#endif
	}
#endif //------------------------------------------------------------------------------------
}
	
/*************************************************************************
* Function:  void medium::addNodeToUSChannel(void){
*
* Explanation:
*   This is called when all US channels are to be added to the node
*
* Inputs:
*
* Outputs:
*
*
***********************************************************************/
void medium::addNodeToUSChannel(void){
	int i=0;	
#ifdef TRACEME
	printf("medium::addNodeToUSChannel:: Add all channels to this node %s \n ", node_->docsis_id);
#endif
	for(i=numDChannels;i<numChannels;i++){
		NodeListData *newNodeListElement = new NodeListData();
		newNodeListElement->NodeListDataInit(node_);
		myChannel[i].mySendDownNodeList.addElement(*newNodeListElement);
#ifdef TRACEME
        printf("medium::addNodeToUSChannel::   Assign channel (%d) to this Node: %s \n ",i,node_->docsis_id);
#endif
		newNodeListElement->NodeListDataChannelNumber(i,this);
	}
}

/*************************************************************************
* Function:  void medium::addNodeToDSChannel(void){
*
* Explanation:
*   This is called when all US channels are to be added to the node
*
* Inputs:
*
* Outputs:
*
*
***********************************************************************/
void medium::addNodeToDSChannel(void){
	int i=0;	
#ifdef TRACEME
	printf("medium::addNodeToDSChannel:: Add all channels to this node %s \n ", node_->docsis_id);
#endif
	for(i=0;i<numDChannels;i++){
		NodeListData *newNodeListElement = new NodeListData();
		newNodeListElement->NodeListDataInit(node_);
		myChannel[i].mySendDownNodeList.addElement(*newNodeListElement);
#ifdef TRACEME
        printf("medium::addNodeToDSChannel::   Assign channel (%d) to this Node: %s \n ",i,node_->docsis_id);
#endif
		newNodeListElement->NodeListDataChannelNumber(i,this);
	}

}

void medium::addNodeToUSChannelUP(int channelNumber){
	NodeListData *newNodeListElement = new NodeListData();
    newNodeListElement->NodeListDataInit(node_);
#ifdef TRACEME
	printf("medium:addNodeToUSChannelUP(%s):  create new NodeList UP element to represent this channel: %d\n",node_->docsis_id,channelNumber);
#endif
	newNodeListElement->NodeListDataChannelNumber(channelNumber,this);
	myChannel[channelNumber].mySendUpNodeList.addElement(*newNodeListElement);
}

void medium::addNodeToDSChannelUP(int channelNumber){
	NodeListData *newNodeListElement = new NodeListData();
    newNodeListElement->NodeListDataInit(node_);
#ifdef TRACEME
	printf("medium:addNodeToDSChannelUP(%s):  create new NodeList UP element to represent this channel: %d\n",node_->docsis_id,channelNumber);
#endif
	newNodeListElement->NodeListDataChannelNumber(channelNumber,this);
	myChannel[channelNumber].mySendUpNodeList.addElement(*newNodeListElement);
}


int medium::findIdleChannel(int inputDirection)
{
	int idleChannel = -1;
    int i=0;
		    
#ifdef TRACEME
	printf("medium:findIdleChannel(%lf): inputDirection:%d numDChannels %d numChannels %d\n", Scheduler::instance().clock(),inputDirection,numDChannels,numChannels);
#endif
	
	if(inputDirection==CHANNEL_DIRECTION_DOWN) {
	    for(i=0; i<numDChannels; i++) {
#ifdef TRACEME
	printf("medium:findIdleChannel(%lf): inputDirection:%d numDChannels %d numChannels %d\n", Scheduler::instance().clock(),inputDirection,numDChannels,numChannels);
#endif
			if(myChannel[i].getTxState() == CHANNEL_IDLE )
				idleChannel = i;
		}
	}
	if(inputDirection==CHANNEL_DIRECTION_UP) {
		idleChannel = numDChannels;
	}
#ifdef TRACEME
	printf("medium:findIdleChannel(%lf): inputDirection:%d numDChannels %d numChannels %d, idleChannel: %d\n", Scheduler::instance().clock(),inputDirection,numDChannels,numChannels,idleChannel);
#endif
	return(idleChannel);
}

void medium::linkBondingGroupMgr(bondingGroupMgr *myParam)
{
	myBGMgr = myParam;
}

void medium::linkDSserviceFlowMgr(serviceFlowMgr *myParam)
{
	myDSSFMgr = myParam;
	myDSSFMgr->setBGMgr(myBGMgr);
}

int medium::getOverHead(Packet *p, int channelNumber)
{
  int overhead = 0;
  int x = 0;
  struct channelPropertiesType  channelProperties;
  struct hdr_cmn *hdr = HDR_CMN(p);	

#ifdef TRACEME
	printf("medium:getOverHead(%lf): Compute and return overhead for channel %d\n", Scheduler::instance().clock(),channelNumber);
#endif

  getChannelProperties(&channelProperties,channelNumber);
  x = (int) (ceil(hdr->size() * (channelProperties.FECPercentage/100)));
  overhead = x +  channelProperties.overheadBytes;
#ifdef TRACEME
	printf("medium:getOverHead(%lf):  x=%d,  overhead=%d \n", Scheduler::instance().clock(),x,overhead);
#endif

  return overhead;
}

int medium::getOverHead(int dataSize, int channelNumber)
{
  int overhead = 0;
  int x = 0;
  struct channelPropertiesType  channelProperties;

#ifdef TRACEME
	printf("medium:getOverHead(%lf): Compute and return overhead for channel %d\n", Scheduler::instance().clock(),channelNumber);
#endif

  getChannelProperties(&channelProperties,channelNumber);
  x = (int) (ceil(dataSize * (channelProperties.FECPercentage/100)));
  overhead = x +  channelProperties.overheadBytes;
#ifdef TRACEME
	printf("medium:getOverHead(%lf):  x=%d,  overhead=%d \n", Scheduler::instance().clock(),x,overhead);
#endif

  return overhead;
}




/***************************************************************************
 * Module: resourceOptimizer
 *
 * Explanation:
 *   This file contains the class definition of the object that
 *   is responsible for optimizing system resources.
 * 
 * Revisions:
 *
************************************************************************/
#include "resourceOptimizer.h"

#include "hdr-docsis.h"
#include "mac-docsistimers.h"
#include "serviceFlowMgr.h"
#include "bondingGroupMgr.h"
#include "schedulerParams.h"
#include "schedulerObject.h"

//#define TRACEME 0


/*************************************************************
* routine: resourceOptimizer::resourceOptimizer()
*
* Function: constructor
*
* inputs: 
*
* outputs:
*
***************************************************************/
resourceOptimizer::resourceOptimizer()
{
#ifdef TRACEME
  printf("resourceOptimizer::constructor: \n");
#endif
  myTimer = NULL;

}

/*************************************************************
* routine: resourceOptimizer::resourceOptimizer()
*
* Function: destructor
*
* inputs: 
*
* outputs:
*
 ************************************************************************/
resourceOptimizer::~resourceOptimizer()
{
#ifdef TRACEME
  printf("resourceOptimizer::destructor: \n");
#endif
}



/*************************************************************
* routine: resourceOptimizer::resourceOptimizer()
*
* Function: this method optimizes the system.
*
* inputs: 
*
* outputs:
*  Returns a SUCCESS or FAILURE.
*
***************************************************************/
void resourceOptimizer::init(int directionP, MacDocsisCMTS *myMacP, CMTSserviceFlowMgr *mySFMgrP, bondingGroupMgr *myBGMgrP, schedulerObject *mySchedulerP)
{
  DSSchedulerParams * schedParams = DSSchedulerParams::instance();
  assert(schedParams != 0);
  
  direction = directionP;

  bzero((void *)&myStats,sizeof(struct resourceOptimizerStatsType));
#if 0
  optimizationPeriod = 5.0;
#else
  optimizationPeriod = schedParams->getMonitorPeriod();
#endif

  myScheduler = mySchedulerP;
  myBGMgr = myBGMgrP;
  myMac = myMacP;
  mySFMgr= mySFMgrP;
  myTimer = new optimizationTimer(this);

  myTimer->start(&myEvent,optimizationPeriod);

#ifdef TRACEME 
  printf("resourceOptimizer::init: direction:%d, period:%f  \n",direction,optimizationPeriod);
#endif
}

/*************************************************************
* routine: resourceOptimizer::resourceOptimizer()
*
* Function: this method optimizes the system.
*
* inputs: 
*
* outputs:
*  Returns a SUCCESS or FAILURE.
*
***************************************************************/
int resourceOptimizer::optimizeSystem() 
{
  int rc = SUCCESS;

  myStats.numberOptimizations++;
#ifdef TRACEME 
  printf("resourceOptimizer::optimizeSystem:  totalNumberOptimizations:%d \n",myStats.numberOptimizations);
#endif

//  myScheduler->adjustScheduler(ADJUST_PACKET_SCHEDULER);
  //This causes state (like rate monitors) to be updated
  mySFMgr->optimize(OPTIMIZER_HEARTBEAT);
  myBGMgr->optimize(ADJUST_BANDWIDTH_MANAGEMENT);

  myTimer->start(&myEvent,optimizationPeriod);

  return rc;
}

/*************************************************************
* routine: int resourceOptimizer::optimizerTimerHandler(Event *e)
*
* Function: this is the timer tick handler
*
* inputs: 
*
* outputs:
*  Returns a SUCCESS or FAILURE.
*
***************************************************************/
int resourceOptimizer::optimizerTimerHandler(Event *e)
{
int rc = SUCCESS;


#ifdef TRACEME 
  printf("resourceOptimizer::TimerHandler  \n");
#endif
  rc = resourceOptimizer::optimizeSystem() ;


  return rc;
}




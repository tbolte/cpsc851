/************************************************************************
* File:  REDQ.cc
*
* Purpose:
*  This module contains a basic list object.
*
* Revisions:
*
*  This module is outdated.  It has been replaced by BaseRED.cc
*  It should not be in the Makefile
*
************************************************************************/
#include "REDQ.h"
#include <iostream>
#include <math.h>
#include <random.h>

#include "object.h"

#include "globalDefines.h"
#include "docsisDebug.h"
//#define TRACEME 0
//#define FILES_OK 0

REDQ::REDQ() : ListObj()
{
#ifdef TRACEME
  printf("REDQ: constructed list \n");
#endif
}

REDQ::~REDQ()
{
#ifdef TRACEME
  printf("REDQ: destructed list \n");
#endif
}

void REDQ::initList(int maxListSize)
{

  ListObj::initList(maxListSize);

  flowPriority = 0;
  minth = 0;
  maxth = maxListSize;
  maxp = .10;
  weightQ = 1.0;
  gentleMode = 0;
  adaptive1Mode=0;
  count = -1;
  avgQ = 0.0;
  dropP = 0.0;
  avgDropP=0;
  avgDropPSampleCount=0;
  avgAvgQ = 0;
  avgAvgQPSampleCount= 0;
  q_time = 0.0;
  avgTxTime = (double)1500 * 8 / (double) 5000000;
#ifdef TRACEME
  printf("REDQ:init: max list size: %d avgTxTime:%lf  \n",maxListSize,avgTxTime);
#endif


}


/*************************************************************
* routine:
*   int  REDQ::addElement(ListElement& element)
*
* Function: this routine inserts the ListElement to the tail of 
* the list.
*           
* inputs: 
*    ListElement& element : the element to be inserted
*
* outputs:
*  Returns a SUCCESS or FAILURE.
*        Possible failures:
*             -malloc fails
*             -list already has  > MAXLISTSIZE elements
*
***************************************************************/
int  REDQ::addElement(ListElement& element)
{
  int rc = SUCCESS;
  double curTime =  Scheduler::instance().clock();

#ifdef TRACEME
   printf("REDQ:addElement(%lf) listsize:%d, avgQ:%3.1f, mint:%d, maxt:%d, dropP:%3.3f \n ",
      curTime,curListSize,avgQ,minth,maxth,dropP);
#endif

  avgQ = updateAvgQ();
#ifdef TRACEME
   printf("REDQ:addElement(%lf) UPDATE AvgQ listsize:%d, avgQ:%3.1f, mint:%d, maxt:%d, dropP:%3.3f \n ",
      curTime,curListSize,avgQ,minth,maxth,dropP);
#endif
  if ((minth<= avgQ) && (avgQ < maxth)) {
    count++;
    //update dropP;
    dropP = computeDropP();
    if ( packetDrop(dropP) == TRUE) {
      rc = FAILURE;
    }
  }
  else if (avgQ >= maxth) {
    rc = FAILURE;
  }
  else
    count=-1;

#ifdef TRACEME
   printf("REDQ:addElement(%lf)  rc:%d listsize:%d, avgQ:%3.1f, mint:%d, maxt:%d, dropP:%3.3f \n ",
      curTime,rc,curListSize,avgQ,minth,maxth,dropP);
#endif

  //rc is SUCCESS  indicting the pkt can be queued, else the caller should drop it
  if (rc == SUCCESS) {
    rc = ListObj::addElement(element);
  }
  else {
    count = 0;
  }

#ifdef TRACEME
   printf("REDQ:addElement(%lf) exit rc=%d, listsize:%d, avgQ:%3.1f, min-t:%d, max-t:%d, dropP:%3.3f \n ",
      curTime,rc,curListSize,avgQ,minth,maxth,dropP);
#endif

  return rc;
}

/*************************************************************
* routine: ListElement * ListObj::removeElement()
*
* Function:
*
* inputs: 
*
* outputs:
*   The element at the top of the list is removed and returned.
*
*   A NULL is returned if the list is empty
*
***************************************************************/
ListElement * REDQ::removeElement()
{
  ListElement *tmpPtr = NULL;
  double curTime =   Scheduler::instance().clock();

#ifdef TRACEME
  printf("REDQ::removeElement:(%lf) start size: %d\n", curTime, curListSize);
#endif

  tmpPtr = ListObj::removeElement();
  if (curListSize == 0) {
    q_time = curTime;
  }

  return tmpPtr;
}

//Called only when a new packet arrives  (which might be dropped....)
void REDQ::newArrivalUpdate(Packet *p)
{

// We want to monitor the true arrival rate to the queue...
// so ignore when a new packet arrives ....we only
// caree when it gets queued.
  return;
}



void REDQ::updateStats()
{
  double curTime =   Scheduler::instance().clock();

#ifdef TRACEME
  printf("REDQ:updateStats:(%lf), updated avg: %3.1f\n",curTime,avgQ);
#endif
}

void REDQ::printStatsSummary()
{
  double curTime =   Scheduler::instance().clock();

//#ifdef TRACEME
  printf("REDQ:ListObj:printStatsSummary:(%lf), avgAvgQ: %3.1f, avgDropP: %3.3f\n",curTime,getAvgAvgQ(), getAvgDropP());
//#endif
}


/*************************************************************
* routine: void REDQ::setAQMParams(int maxAQMSizeP, int priorityP, int minthP, int maxthP, double maxpP, double weightQP)
*
* Function: this routine adapts the AQM parameters.
*   The algorithm is TBD.
*           
* inputs: 
*
* outputs:
*
***************************************************************/
void REDQ::setAQMParams(int maxAQMSizeP, int priorityP, int minthP, int maxthP, int adaptiveMode, double maxpP, double weightQP)
{

  flowPriority = priorityP;
  MAXLISTSIZE = maxAQMSizeP;
  minth = minthP;
  maxth = maxthP;
  maxp = maxpP;
  weightQ = weightQP;

//For the 'new' RED config 
//  minth = 4;
//  maxth = maxAQMSizeP / 2;
//  maxp =  0.5;


//If the priority is > 0, eleveate the priority using algorithm params
  if (priorityP > 0) {
    maxp = maxpP / 2;
//      minth = MAXLISTSIZE/2;
//      maxth = MAXLISTSIZE;
  }
  if (priorityP < 0) {
    minth=1;
    maxth = 4;
//    minth=3;
//    maxth = 10;
    weightQ = .01;
  }

//#ifdef TRACEME
  printf("REDQ:setAQMParams: minth:%d  maxth:%d  maxp:%2.2f weightQ:%2.4f, priority:%d\n",minth,maxth,maxp,weightQ,flowPriority);
//#endif

}

/*************************************************************
* routine: void REDQ::adaptAQMParams()
*
* Function: this routine adapts the AQM parameters.
*   The algorithm is TBD.
*           
* inputs: 
*
* outputs:
*
***************************************************************/
void REDQ::adaptAQMParams()
{

#ifdef TRACEME
  printf("REDQ:adaptAQMParams: updated minth:%d  maxth:%d  maxp:%2.2f \n",minth,maxth,maxp);
#endif
}


double REDQ::getAvgQ()
{
  return(avgQ);
}

double REDQ::getAvgAvgQ()
{
  if (avgAvgQPSampleCount > 0)
    return(avgAvgQ/(double)avgAvgQPSampleCount);
  else
    return(0);
}

double REDQ::getDropP()
{
  return(dropP);
}

double REDQ::getAvgDropP()
{
  double tmpAvgDropP = 0.0;

  if (avgDropPSampleCount > 0)
    tmpAvgDropP = avgDropP / avgDropPSampleCount;
  
  return(tmpAvgDropP);
}


double REDQ::updateAvgQ()
{
  double returnAvg = 0.0;
  double curTime =   Scheduler::instance().clock();

#ifdef TRACEME
  printf("REDQ:updateAvgQ:(%lf)  curListSize:%d, current avgQ:%3.3f, weightQ:%2.4f, q_time:%3.6f \n",
       curTime,curListSize,avgQ,weightQ,q_time);
  fflush(stdout);
#endif


  if (curListSize == 0) {
    double quietTime = curTime - q_time;
    double power = quietTime/avgTxTime;
    double x=  pow((1-weightQ),power);
//    returnAvg = avgQ*x;
    returnAvg = 0;
#ifdef TRACEME
    printf("REDQ:updateAvgQ:(%lf) case of empty queue quietTime: %6.6f, power:%3.6f, x:%3.6f, returnAvg:%3.6f \n",
       curTime,quietTime,power,x,returnAvg);
#endif
  }
  else {
    returnAvg = (1-weightQ)*avgQ + weightQ*curListSize;
#ifdef TRACEME
    printf("REDQ:updateAvgQ:(%lf) case of queue len:%d, current avgQ:%3.3f, updated avg:%3.3f \n",
       curTime,curListSize, avgQ,returnAvg);
#endif
  }
  avgAvgQ+=returnAvg;
  avgAvgQPSampleCount++;

#ifdef TRACEME
  printf("REDQ:updateAvgQ:(%lf) curLen:%d, avgQ:%3.3f, returnAvg:%3.3f avgAvgQ:%3.3f (weightQ:%3.3f)\n",
       curTime,curListSize, avgQ, returnAvg,avgAvgQ/avgAvgQPSampleCount,weightQ);
#endif
  return returnAvg;

}

double REDQ::computeDropP()
{
  double pb;
  double pa;
  double curTime =   Scheduler::instance().clock();
  double y;

#ifdef TRACEME
  printf("REDQ:computDropP(%lf): avgQ:%2.2f, minth:%d, maxth:%d, count:%d \n",
         curTime,avgQ,minth,maxth,count);
#endif

  pb = maxp *  (avgQ - (double)minth) / (double)(maxth-minth);
  y =  (1-(double)count*pb);
  if (y < .00001) {
//#ifdef TRACEME
    printf("REDQ:computDropP(%lf): TROUBLE: y: %2.4f  pa:%2.4f pb:%2.4f\n",curTime,y, pa,pb);
//#endif
    y = .1;
  }
  pa = pb / y;
  if (pa > 1.0) {
//#ifdef TRACEME
    printf("REDQ:computDropP(%lf): TROUBLE: pa: %2.4f  pb:%2.4f\n",curTime,pa,pb);
//#endif
    pa = 1.0;
  }

  if (count > 1000) {
//#ifdef TRACEME
      printf("REDQ:computDropP(%lf): TROUBLE: count:%d pb: %2.4f  pa:%2.4f\n",curTime,count,pb,pa);
//#endif
      count = 10;
   }

  avgDropP+= pa;
  avgDropPSampleCount++;

#ifdef TRACEME
  printf("REDQ:computDropP(%lf):pb: %2.2f  pa:%2.2f, count:%d, avg:%2.2f\n",curTime,pb,pa,count, avgDropP/avgDropPSampleCount);
#endif

  return pa;

}

int REDQ::packetDrop(double dropP)
{
  int dropFlag = FALSE;
  double randNumber  = Random::uniform(0,1.0);
  double curTime =  Scheduler::instance().clock();

  if (randNumber <= dropP)
    dropFlag = TRUE;

#ifdef  FILES_OK 
  if (traceFlag == 1) {
    FILE* fp = NULL;
    fp = fopen("traceAQM.out", "a+");
    fprintf(fp,"%lf %d %3.1f %d %2.3f %2.3f %d \n",
      Scheduler::instance().clock(),listID,avgQ, curListSize, dropP, avgDropP/avgDropPSampleCount,dropFlag);
    fclose(fp);
  }
#endif
  
#ifdef TRACEME
  printf("REDQ:packetDrop(%lf): uniform:%2.2f, dropP:  %2.2f dropFlag:%d \n",curTime,randNumber,dropP,dropFlag);
#endif
  return dropFlag;

}

void REDQ::channelIdleEvent()
{

}

void REDQ::channelStatus(double accessDelayMeasure, double channelUtilizationMeasure, double consumptionMeasure)
{

}

/***************************************************************************
 * Module: pDRRpacketSchedulerObject
 *
 * Explanation:
 *   This file contains the class definition of the DRRpacket scheduler. 
 *   The two main methods are:
 *       -selectChannel(Packet *pkt, serviceFlowObject *mySFObj, struct channelSet *myChannelSet, int *channelSelection)
 *            This is called when a packet arrives at the scheduler for transmission. An available channel
 *            in the SF's channelset is selected.
 *       -selectPacket(int channelNumber, struct serviceFlowSet *mySFSet)
 *            This selects the next packet for transmission
 *     
 *
 * Revisions:
 *  $A810 : bug in selectPacket.
 *
************************************************************************/


#include "DRRpacketScheduler.h"
#include "serviceFlowObject.h"
#include "channelAbstraction.h"

#include "docsisDebug.h"
//#define TRACEME 0
//#define FILES_OK 0




/*************************************************************************
 ************************************************************************/
pDRRpacketSchedulerObject::pDRRpacketSchedulerObject() : packetSchedulerObject()
{
}

/*************************************************************************
 ************************************************************************/
pDRRpacketSchedulerObject::~pDRRpacketSchedulerObject()
{
}



void pDRRpacketSchedulerObject::init(int serviceDisciplineParam, serviceFlowMgr * paramDSSFMgr, bondingGroupMgr *myBGMgrParam)
{
  double curTime = Scheduler::instance().clock();
  int i;

  for (i=0;i<MAX_CHANNELS;i++) {
    currentActiveSetIndexArray[i] = 0;
    lastSFFlowIDSelectedArray[i] = 0;
  }

  packetSchedulerObject::init(serviceDisciplineParam, paramDSSFMgr,myBGMgrParam);

#ifdef TRACEME //---------------------------------------------------------
    printf("pDRRpacketSchedulerObject::init(%lf): set defaultQuantum to %d \n", curTime,defaultQuantum);
#endif

  switch (serviceDiscipline) 
  {

    case DRR:
      printf("pDRRpacketSchedulerObject::init(%lf):  DRR: defaultQuantum: %d \n", curTime,defaultQuantum);
      break;
    case bestDRR:
      printf("pDRRpacketSchedulerObject::init(%lf):  bestDRR: defaultQuantum: %d \n", curTime,defaultQuantum);
      break;
    case globalDRR:
      printf("pDRRpacketSchedulerObject::init(%lf):  globalDRR: defaultQuantum: %d \n", curTime,defaultQuantum);
      break;
    case channelDRR:
      printf("pDRRpacketSchedulerObject::init(%lf):  channelDRR: defaultQuantum: %d \n", curTime,defaultQuantum);
      break;
    default:
      printf("pDRRpacketSchedulerObject::init(%lf):  Unknown DRR discipline?? %d \n", curTime,serviceDiscipline);
      break;
  }
}



/***********************************************************************
*function: Packet *pDRRpacketSchedulerObject::selectPacket(int channelNumber, struct serviceFlowSet *mySFSet)
*
*explanation: This method selects the next packet to be sent.
*    The algorithm is to service the SFs that are in the SFSet
*    in round robin order.  
*
*    If there are 0 elements in the list, just exit with a NULL;
*    IF there is 1 element, service the queue.  Reset it's deficit counter.
* 
*inputs:
*  int channelNumber:  currently this is not used
*  struct serviceFlowSet *mySFSet:  the callers SET of SFs to chose from.
*      The method assumes that the SFs have a packet
*
*outputs:
*  Returns a packet to be sent next, else a NULL;
*
* Pseudo Code
*   Given CurrentActiveFlowSet
*   while (loopFlag > 0)
*     For all SFs in the set starting at currentActiveIndex[channelNumber]  that are ACTIVE
*     {
*         if (hdr->size() <= selectedSFObj->deficitCounter) {
*           get Pkt
*           selectedSFObj->deficitCounter-= hdr->size();
*            endOfRoundFlag = advanceToNextSF(channelNumber,mySFSet);
*                (which might increase all DCs if endOfRound)
*           if (selectedSFObj->packetsQueued()== 0)
*                selectedSFObj->deficitCounter = 0;
*         else
*            endOfRoundFlag = advanceToNextSF(channelNumber,mySFSet);
*    }
*    if did NOT find a packet but there is at least 1 ACTIVE flow loop again
*      loopFlag++
*     else
*       break
*  }  
*
************************************************************************/
Packet *pDRRpacketSchedulerObject::selectPacket(int channelNumber, struct serviceFlowSet *mySFSet)
{
  int mySFSetCount = mySFSet->numberMembers;
  int mySFCount = mySFMgr->numberSFs;
  int numberActiveSFs = 0;
  Packet *returnPkt = NULL;
  Packet *tmpPkt = NULL;
  Packet *foundPkt = NULL;
  serviceFlowObject *foundSFObj = NULL;
  serviceFlowObject *selectedSFObj = NULL;
  serviceFlowObject *tmpSFObj = NULL;
  packetListElement *myLE = NULL;
  int i;
  double curTime = Scheduler::instance().clock();
  struct hdr_cmn *hdr = NULL;
  struct hdr_cmn *foundHdr = NULL;
  int selectedPktSize=0;
  int endOfRoundFlag = 0;
  int DCUpdatedFlag  = 0;
  int loopFlag = 1;

#ifdef FILES_OK
//  char traceString[32];
//  char *tptr = traceString;
//  sprintf(tptr,"PS%d.out",??);
  FILE *fp;
  fp = fopen("PS.out", "a+");
#endif


#ifdef TRACEME 
  printf("pDRRpacketSchedulerObject::selectPacket(%lf):(channelNumber:%d) SF set count:%d, totalSFs:%d, currentIndex:%d, lastSFFlowIDSelected:%d \n", 
   curTime,channelNumber,mySFSetCount,mySFCount,currentActiveSetIndexArray[channelNumber],lastSFFlowIDSelectedArray[channelNumber]);
#endif 

while (loopFlag > 0) {

  selectedSFObj = NULL;
      //For all members in the set...starting with the member pointered to by
      // the currentActiveSetIndex find a SF 
#ifdef TRACEME 
  printf("pDRRpacketSchedulerObject::selectPacket(%lf): BEFORE:  Loop through the SF Set, currentActiveSetIndex:%d \n", curTime,currentActiveSetIndexArray[channelNumber]);
  for (i=0;i<mySFSetCount;i++) {
    selectedSFObj = mySFSet->SFObjPtrs[i];
    if (selectedSFObj != NULL) {
        printf("pDRRpacketSchedulerObject::selectPacket(%lf): SF flowid:%d, queue size:%d, deficit count:%d \n", 
        curTime,selectedSFObj->flowID,selectedSFObj->packetsQueued(),selectedSFObj->deficitCounter);
    }
    else
     printf("pDRRpacketSchedulerObject::selectPacket(%lf): HARD ERROR, null selectedSFObj  \n", curTime);
  }
#endif
  //Top Loop
  for (i=0;i<mySFSetCount;i++) {
        selectedSFObj = mySFSet->SFObjPtrs[currentActiveSetIndexArray[channelNumber]];
        if (selectedSFObj != NULL) {
          if (selectedSFObj->packetsQueued() > 0) {
             numberActiveSFs++;
 
#ifdef TRACEME 
              printf("pDRRpacketSchedulerObject::selectPacket(%lf): Found a SF (SF ID: %d)  with a packet (packetsQueued:%d) currentSetIndeSelected=%d (its DC:%d, and flowQuantum:%d) \n", 
                 curTime,selectedSFObj->flowID,selectedSFObj->packetsQueued(),currentActiveSetIndexArray[channelNumber],selectedSFObj->deficitCounter,selectedSFObj->flowQuantum);
#endif
            //For  each packet in the queue,
            myLE = (packetListElement *)selectedSFObj->myPacketList->head;
            tmpPkt = myLE->getPacket();
            hdr = HDR_CMN(tmpPkt);
            //Either select the current SF
            if (hdr->size() <= selectedSFObj->deficitCounter) {
              selectedSFObj->deficitCounter-= hdr->size();
              lastSFFlowIDSelectedArray[channelNumber] = selectedSFObj->flowID;
              returnPkt  = selectedSFObj->removePacket();
              hdr = HDR_CMN(returnPkt);
              selectedPktSize = hdr->size();
              //lastDequeueTime is the timestamp when the last pkt was dequeued
              selectedSFObj->lastDequeueTime = curTime;
              //lastPacketTime is the timestamp when the last time a pkt was transmitted
              selectedSFObj->lastPacketTime = curTime;
              //If this is the last pkt in the queue, clear DC
              if (selectedSFObj->packetsQueued()== 0)
                selectedSFObj->deficitCounter = 0;
              //$A810
              else {
                endOfRoundFlag = advanceToNextSF(channelNumber,mySFSet);
                if (endOfRoundFlag == 1) 
                   DCUpdatedFlag  =1;
              }
#ifdef TRACEME 
              printf("pDRRpacketSchedulerObject::selectPacket(%lf): Select CURRENT flowID:%d its queue size: %d, the next pkt size:%d, updated deficit:%d \n", 
               curTime,selectedSFObj->flowID,selectedSFObj->packetsQueued(),hdr->size(),selectedSFObj->deficitCounter);
#endif
              break;
            }
            //Or move to the next SF in the list 
            // but update the foundPkt - this is to point to the packet
            // that is queued by any active SF that has the highest
            //    deficitCount - pkt size
            else { 
              endOfRoundFlag = advanceToNextSF(channelNumber,mySFSet);
              if (endOfRoundFlag == 1) 
                 DCUpdatedFlag  =1;
            }
        }
        else {
          endOfRoundFlag = advanceToNextSF(channelNumber,mySFSet);
          if (endOfRoundFlag == 1) 
             DCUpdatedFlag  =1;
#ifdef TRACEME 
          printf("pDRRpacketSchedulerObject::selectPacket(%lf): Skip SF , updated  currentSetIndeSelected=%d \n", 
                 curTime,currentActiveSetIndexArray[channelNumber]);
#endif
        }
      }
      else {
        printf("pDRRpacketSchedulerObject::selectPacket(%lf): HARD ERROR 0 Bad SFObj Ptr, i:%d, mySFSetCount is %d \n", curTime,i,mySFSetCount);
        exit(1);
      }
  }


  //If we have not selected a packet AND there is at least one active SF then we need to loop again,  otherwise, break as we are done, NOTE: DCUpdatedFlag should be set by now
  //  if ((DCUpdatedFlag == 1) && (numberActiveSFs > 0) && (returnPkt == NULL)){
  if ((numberActiveSFs > 0) && (returnPkt == NULL)){

     if (DCUpdatedFlag != 1) {
       printf("pDRRpacketSchedulerObject::selectPacket(%lf): HARD ERROR?  We got to end of a round but did not select a packet AND there are %d active flows, currentIndex:%d  \n", 
                 curTime,numberActiveSFs,currentActiveSetIndexArray[channelNumber]);
       loopFlag=0;
       exit(1);
     } else {
       loopFlag++;
       DCUpdatedFlag  =0;
#ifdef TRACEME 
       printf("pDRRpacketSchedulerObject::selectPacket(%lf): We got to end of a round but did not select a packet AND there are %d active flows, currentIndex:%d, loopFlag:%d  \n", 
                 curTime,numberActiveSFs,currentActiveSetIndexArray[channelNumber],loopFlag);
#endif
     }
  }
  else {
    loopFlag=0;
#ifdef TRACEME 
     printf("pDRRpacketSchedulerObject::selectPacket(%lf): COMPLETED,  set loopFlag to 0  currentIndex:%d, loopFlag:%d  \n", 
                 curTime,currentActiveSetIndexArray[channelNumber],loopFlag);
#endif
  }

}  //end outer loop

#ifdef TRACEME 
  printf("pDRRpacketSchedulerObject::selectPacket(%lf): AFTER:  Loop through the SF Set, currentActiveSetIndex:%d \n", curTime,currentActiveSetIndexArray[channelNumber]);
  for (i=0;i<mySFSetCount;i++) {
    tmpSFObj = mySFSet->SFObjPtrs[i];
    if (selectedSFObj != NULL) {
        printf("pDRRpacketSchedulerObject::selectPacket(%lf): SF flowid:%d, queue size:%d, deficit count:%d, endOfRoundFlag:%d \n", 
        curTime,tmpSFObj->flowID,tmpSFObj->packetsQueued(),tmpSFObj->deficitCounter,endOfRoundFlag);
    }
    else
     printf("pDRRpacketSchedulerObject::selectPacket(%lf): HARD ERROR, null selectedSFObj  \n", curTime);
  }
  printf("pDRRpacketSchedulerObject::selectPacket(%lf):  SELECTED: flowID:%d, PktSize:%d, SFObj packetsQueued:%d \n", 
       curTime, selectedSFObj->flowID, selectedPktSize, selectedSFObj->packetsQueued());
#endif

#ifdef FILES_OK
  if (mySFSetCount >0) {
      fprintf(fp,"%lf %d %d %d %d %d",curTime,channelNumber,currentActiveSetIndexArray[channelNumber],lastSFFlowIDSelectedArray[channelNumber],mySFSetCount,mySFCount);
      tmpSFObj = NULL;
      for (i=0;i<mySFSetCount;i++) {
        tmpSFObj = mySFSet->SFObjPtrs[i];
        if (tmpSFObj != NULL) {
          if (tmpSFObj->packetsQueued() > 0) {
            fprintf(fp," (%d,%d,%d);  ",tmpSFObj->flowID,tmpSFObj->packetsQueued(), tmpSFObj->deficitCounter);
          }
        }
        else
          fprintf(fp," (ERROR); ");
      }
      if (returnPkt != NULL)  {
        fprintf(fp," (selectedObj: %d,%d,%d, %d, %d);  ",selectedSFObj->flowID, selectedPktSize, selectedSFObj->packetsQueued(), selectedSFObj->deficitCounter, endOfRoundFlag);
      }
      else
        fprintf(fp," (selectedObj: NONE, %d);  ",endOfRoundFlag);
      fprintf(fp,"\n");

      fclose(fp);
  }
#endif

  return returnPkt;

}



/***********************************************************************
*function: void pDRRpacketSchedulerObject::advanceToNextSF()
*
*explanation:
*  Private method to advance to next SF in the flow list
* 
*inputs:
*
*outputs:
*
*  Returns and endOfRoundFlag :  1 if end of a round,
*        else 0
*                                
*
************************************************************************/
int pDRRpacketSchedulerObject::advanceToNextSF(int channelNumber,struct serviceFlowSet *mySFSet)
{
  double curTime = Scheduler::instance().clock();
  int endOfRoundFlag = 0;
  int mySFSetCount = mySFSet->numberMembers;
  int i;
  serviceFlowObject *selectedSFObj = NULL;

  currentActiveSetIndexArray[channelNumber]++;
#ifdef TRACEME 
  printf("pDRRpacketSchedulerObject::advanceToNextSF(%lf): Using channelNumber%d, setSize:%d,  updated index is %d \n", 
    curTime,channelNumber,mySFSetCount,currentActiveSetIndexArray[channelNumber]);
#endif 

  if (currentActiveSetIndexArray[channelNumber] >= mySFSetCount) {
    currentActiveSetIndexArray[channelNumber] = 0;

#ifdef TRACEME 
      printf("pDRRpacketSchedulerObject::advanceToNextSF(%lf): ROUND COMPLETE, update DC (currentSetIndeSelected=%d) \n", 
          curTime,currentActiveSetIndexArray[channelNumber]);
#endif

    endOfRoundFlag = 1;
    //If this is a new round, increment all the DC's of active flows in the set
    for (i=0;i<mySFSetCount;i++) {
      selectedSFObj = mySFSet->SFObjPtrs[i];
      if (selectedSFObj->packetsQueued() > 0) {
        selectedSFObj->deficitCounter+= selectedSFObj->flowQuantum;
      }
      else {
        selectedSFObj->deficitCounter = 0;
      }
#ifdef TRACEME 
     printf("    (i:%d, flowID:%d, packetsQueued:%d, updatedDC:%d), ", 
          i,selectedSFObj->flowID,currentActiveSetIndexArray[channelNumber], selectedSFObj->deficitCounter);
#endif
    }
  }

#ifdef TRACEME 
  printf("\n");
#endif

  return endOfRoundFlag;
}



/***********************************************************************
*function: int pDRRpacketSchedulerObject::selectChannel(Packet *pkt, serviceFlowObject *mySFObj, struct channelSet *myChannelSet, int *channelSelection)
*
*explanation: This method selects the channel that will be used to
*    send the pkt.  
* 
*inputs:
*
*outputs:
*  Returns a SUCCESS or FAILURE
*
*******************************************************************/
int pDRRpacketSchedulerObject::selectChannel(Packet *pkt, serviceFlowObject *mySFObj, struct channelSet *myChannelSet, int *channelSelection)
{
  int channelNumber = 0;
  int rc = FAILURE;
  int myChannelSetCount = myChannelSet->numberMembers;
  double curTime = Scheduler::instance().clock();
  int i;


#ifdef TRACEME 
   printf("pDRRpacketSchedulerObject::selectChannel(%lf): Need to select channel,  channel set count:%d \n", curTime,myChannelSetCount);
  if (myChannelSetCount >0) {
    for (i=0;i<myChannelSetCount;i++) {
      printf("   %x ",myChannelSet->channels[i]);
    }
    printf("\n");
  }
#endif 

  if (myChannelSetCount >0) {
    for (i=0;i<myChannelSetCount;i++) {
      channelNumber = myChannelSet->channels[i];
      if (mySFMgr->myPhyInterface->getChannelStatus(channelNumber) == CHANNEL_IDLE) {
        rc = SUCCESS;
        break;
#ifdef TRACEME 
   printf("pDRRpacketSchedulerObject::selectChannel(%lf): First IDLE channel: %d\n", curTime,channelNumber);
#endif 
      }
    }
  }

  if ((channelSelection != NULL) && (rc == SUCCESS))
  {
    *channelSelection = channelNumber;
  }
  else
    *channelSelection = -1;

#ifdef TRACEME 
   if (rc == SUCCESS)
     printf("pDRRpacketSchedulerObject::selectChannel(%lf): Return SUCCESS channelSelection:%d \n", curTime,*channelSelection);
   else
     printf("pDRRpacketSchedulerObject::selectChannel(%lf): Return FAILURE channelSelection:%d \n", curTime,*channelSelection);
#endif 
  return rc;
}




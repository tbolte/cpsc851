/************************************************************************
* File:  NodeList.cc
*
* Purpose:
*  This module contains a basic list object.
*
* Version:
*    1.0:   12/20/2000
*
* Changes:
*
************************************************************************/
#include "NodeList.h"
#include <iostream>

#include "docsisDebug.h"
//#define TRACEME 0

NodeList::NodeList()
{
	MAXLISTSIZE = 0;     
	#ifdef TRACEME
		printf("NodeList: constructed list %p\n",this);
	#endif
}

NodeList::~NodeList()
{
	#ifdef TRACEME
		printf("NodeList: destructed list \n");
	#endif
}

void NodeList::initList(int maxListSize)
{

	MAXLISTSIZE = maxListSize;
	head = tail  = NULL;
	curListSize = 0;
	#ifdef TRACEME
		printf("NodeList:init: max list size: %d  this %p\n",MAXLISTSIZE,this);
	#endif
}

/*************************************************************
* routine:
*   int  NodeList::insertToList(NodeListData& element)
*
* Function: this routine inserts the NodeListData to the tail of 
* the list.
	*           
* inputs: 
*    NodeListData& element : the element to be inserted
*
* outputs:
*   rc : 1 is failure, else a 0
*        Possible failures:
*             -malloc fails
*             -list already has  > MAXLISTSIZE elements
*
***************************************************************/
int  NodeList::addElement(NodeListData& element)
{
	if (curListSize < MAXLISTSIZE) {
	//insert at the tail
		curListSize++;
		if (tail == NULL) {
			tail=head=&element;
		}
		else {
			tail->next = &element;
			tail = &element;
			tail->next = NULL;
		}
		
		return 0; //Return 0 on success
	}
	else {
		#ifdef TRACEME
			printf("NodeList:addElement: ERROR, list overflow , list size: %d, MAX : %d, this %p\n ",curListSize,MAXLISTSIZE,this);
		#endif
		return 1; //Return 1 on failure
	}
}

/*************************************************************
* routine: NodeListData * NodeList::removeElement()
*
* Function:
*
* inputs: 
*
* outputs:
*   The last element that was removed is returned (caller must
*   delete it. All other elements that are removed are deleted
*   by this routine.
*   A NULL is returned if the list is empty
*
***************************************************************/
NodeListData * NodeList::removeElement()
{
	NodeListData *tmpPtr = NULL;

	#ifdef TRACEME
		printf("NodeList::removeElement: start size: %d\n", this->getListSize());
	#endif

	if ((curListSize > 0) && (head != NULL)) {
		curListSize--;

		tmpPtr = head;
		head = tmpPtr->next;

		if (curListSize == 0)
			head = tail = NULL;
    }

	#ifdef TRACEME
    if (tmpPtr == NULL)
		printf("NodeList:removeElement: List Empty!!\n ");
	#endif

	#ifdef TRACEME	
		printf("NodeList::removeElement: exiting,   new list size: %d \n ",this->getListSize());
	#endif

    return tmpPtr;
}

int NodeList::getListSize ()
{
	return curListSize;
}

/*****************************************************************************
  * Next three functions used to start a search, iterate through the elements
  * and then to access the current element
  ****************************************************************************/


NodeListData *NodeList::nextElement() //Useful for traversing through a search
{
	NodeListData *tmpPtr = NULL;

	#ifdef TRACEME
		printf("NodeList::nextElement: start size: %d\n", this->getListSize());
	#endif

	if ((curListSize > 0) && (head != NULL)) {
	    tmpPtr = curElement;
	    curElement = tmpPtr->next;
	}

    return tmpPtr;
}


void NodeList::setCurHeadElement() //Useful for starting a search
{	
	curElement = head;
}


NodeListData * NodeList::accessCurrentElement() //Useful for search and access
{
	NodeListData *tmpPtr = NULL;
	tmpPtr = curElement;
	
	return tmpPtr;

}

NodeListData::NodeListData()
{
    next = NULL;
	prev = NULL;
	nodeNumber = 0;
}

void NodeListData::NodeListDataInit(MacDocsis *node)
{
	#ifdef TRACEME
		printf("NodeListData: constructed list Element \n");
	#endif
	//nodeNumber = index;
	myMac = node;
	chTxPkt_.setMacPointer(myMac);
	RxPkt_.setMacPointer(myMac);
	CmtsTxPkt_.setMacPointer(myMac);
	CmTxPkt_.setMacPointer(myMac);
}

/*************************************************************
* routine: void NodeListData::NodeListDataChannelNumber(int channelNumber,medium *medium)
*
* Function:
*   This sets the channelNumber to the Node List element 
*
* inputs: 
*   int channelNumber
*   medium *medium
*
* outputs:
*
***************************************************************/
void NodeListData::NodeListDataChannelNumber(int channelNumber,medium *medium)
{
	#ifdef TRACEME
		printf("NodeListData:NodeListDataChannelNumber:  set the channel number to %d \n", channelNumber);
	#endif
	chTxPkt_.setChannelNumber(channelNumber);
	RxPkt_.setChannelNumber(channelNumber);
	CmtsTxPkt_.setChannelNumber(channelNumber);
	CmTxPkt_.setChannelNumber(channelNumber);
	
	chTxPkt_.setMedPointer(medium);
	RxPkt_.setMedPointer(medium);
	CmtsTxPkt_.setMedPointer(medium);
	CmTxPkt_.setMedPointer(medium);
}

NodeListData::~NodeListData()
{
	#ifdef TRACEME
		printf("NodeListData: destructed list Element \n");
	#endif
}

/* Invoked from the channelAbstraction.cc sendUp method
 * 
 *
 */

void NodeListData::startChTxDocsistimer(Packet* p,double time)
{
	//Packet *tp = p->copy();
	chTxPkt_.setNode(this);
	chTxPkt_.start(p,time);
}
/* Invoked from the channelAbstraction.cc sendUp method
 * 
 *
 */

void NodeListData::startRxDocsistimer(Packet* p,double time)
{
	RxPkt_.start(p,time);
}

/* Invoked from the medium.cc transmitDown method
 * 
 *
 */

void NodeListData::startCmTxDocsistimer(Packet* p,double time)
{
	Packet *tp = p->copy();
	CmTxPkt_.start(tp,time);
}

/* Invoked from the medium.cc transmitDown method
 * 
 *
 */

void NodeListData::startCmtsTxDocsistimer(Packet* p,double time)
{
	Packet *tp = p->copy();
	CmtsTxPkt_.start(tp,time);
}

MacDocsis *NodeListData::NodeListDataMac(void)
{
	return myMac;
}

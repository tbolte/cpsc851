/***************************************************************************
 * Module: reseqMgr
 *
 * Explanation:
 *   This file contains the class definition of the object that
 *   packages reseq flows.  
 * 
 * Revisions:
*  $A800 :  6/18/2010 bug fix to allow a node to attach to a CM
 *     (It did not work before in DS)
 *
 *
************************************************************************/

#include "reseqMgr.h"


#include "packetMonitor.h"

#include "globalDefines.h"
#include "docsisDebug.h"
//#define TRACEME 0

#include "macPhyInterface.h"
#include "mac-docsistimers.h"


/*************************************************************************
 ************************************************************************/
reseqMgr::reseqMgr()
{
#ifdef TRACEME
  printf("reseqMgr::constructor: \n");
#endif

}



void reseqMgr::init(int direction,macPhyInterface *myPhyInterfaceParam)
{

//This is UPSTREAM if the node is the cmts, else DOWNSTREAM
  serviceFlowMgr::init(direction);

  if (direction == DOWNSTREAM)
    maxListSize = MAX_ALLOWED_SERVICE_FLOWS_CM;
  else
    maxListSize = MAX_ALLOWED_SERVICE_FLOWS;

  myOrderedSFList = new OrderedListObj();
  myOrderedSFList->initList(maxListSize);

  //init the element that contains the MGMT service flow
  MGMTServiceFlowLE.putServiceFlow(MGMTServiceFlow);
  MGMTServiceFlowLE.gapStartTime = 0;
  MGMTServiceFlowLE.frameCount = 0;
  MGMTServiceFlowLE.outOfOrderArrivalCount = 0;
  MGMTServiceFlowLE.nextExpectedPSN = 0;


  mySFList->setListSize(maxListSize);

  myPhyInterface = myPhyInterfaceParam;

  bzero((void *)&myStats,sizeof(struct reseqMgrStatsType));

#ifdef TRACEME 
  printf("reseqMgr::init:maxListSize:%d\n", maxListSize);
#endif

}

/******************************************************************
* Method: * int serviceFlowMgr::addServiceFlow(int32_t src_ip, int32_t dst_ip, packet_t pkt_type, u_int16_t flow_id)
*
*explanation:
*  This method adds a service flow to the list.  
*
*  Pseudo code:
*     -check to see if the service flow already exists
*     -create the new SF Object
*     -add it to the list
*     -return the new flow id
*
*
*inputs:
*
*outputs:
*  Returns the SF Object that is added to the list, else a NULL on error
*
************************************************************************/
serviceFlowObject * reseqMgr::addServiceFlow(u_int16_t flow_id, int numberChannels)
{
serviceFlowObject * myNewSFObject = NULL;
int rc = -1;


  reseqFlowListElement *mySFElement = new reseqFlowListElement();
  mySFElement->key = flow_id;
  mySFElement->init(this, numberChannels);
  if (direction == DOWNSTREAM)
  {
     myNewSFObject = new DSserviceFlowObject();
#ifdef TRACEME 
     printf("reseqMgr::addServiceFlow(%lf):DOWNSTREAM service flow created, flowID:%d \n", 
		   Scheduler::instance().clock(), flow_id);
#endif
  }
  else
  {
    myNewSFObject = new USserviceFlowObject();
#ifdef TRACEME 
    printf("reseqMgr::addServiceFlow(%lf):UPSTREAM service flow created, flowID:%d \n",
           Scheduler::instance().clock(),flow_id);
#endif
  }



  myNewSFObject->init(direction,myMac,this);
  myNewSFObject->flowID = flow_id;
  myNewSFObject->setQueueBehavior(MAX_RESEQ_QUEUE, FIFOQ,0,0, 0,0,0,0);

  //init the element
  mySFElement->putServiceFlow(myNewSFObject);
  mySFElement->gapStartTime = 0;
  mySFElement->frameCount = 0;
  mySFElement->outOfOrderArrivalCount = 0;
  mySFElement->nextExpectedPSN = 0;

//TEST CASE
#ifdef TRACEMEA
  mySFElement->key = 5;
  rc = myOrderedSFList->addElement(*(OrderedListElement *)mySFElement);
//  rc = myOrderedSFList->addElement(*((serviceFlowListElement *)mySFElement));
  printf("reseqMgr::addServiceFlow(%lf):add element to OrderedSFList rc:%d, size of list:%d (element key:%d)\n", 
		   Scheduler::instance().clock(),rc, myOrderedSFList->getListSize(),mySFElement->key);
  mySFElement->key = 4;
  rc = myOrderedSFList->addElement(*(OrderedListElement *)mySFElement);
//  rc = myOrderedSFList->addElement(*((serviceFlowListElement *)mySFElement));
  printf("reseqMgr::addServiceFlow(%lf):add element to OrderedSFList rc:%d, size of list:%d (element key:%d)\n", 
		   Scheduler::instance().clock(),rc, myOrderedSFList->getListSize(),mySFElement->key);
  mySFElement->key = 6;
  rc = myOrderedSFList->addElement(*(OrderedListElement *)mySFElement);
//  rc = myOrderedSFList->addElement(*((serviceFlowListElement *)mySFElement));
  printf("reseqMgr::addServiceFlow(%lf):add element to OrderedSFList rc:%d, size of list:%d (element key:%d)\n", 
		   Scheduler::instance().clock(),rc, myOrderedSFList->getListSize(),mySFElement->key);

  rc = myOrderedSFList->isElement(4);
  printf("reseqMgr::addServiceFlow(%lf): call isElement with flowID:%d,  rc is %d \n", 
		   Scheduler::instance().clock(),flow_id,rc);
  rc = myOrderedSFList->displayListElements();

  mySFElement->key = 4;
  rc = myOrderedSFList->removeElement(*mySFElement);
  printf("reseqMgr::addServiceFlow(%lf):remove element from OrderedSFList rc:%d, size of list:%d \n", 
		   Scheduler::instance().clock(),rc, myOrderedSFList->getListSize());
  rc = myOrderedSFList->displayListElements();
#endif

//  rc = mySFList->addElement(*((serviceFlowListElement *)mySFElement));
  rc = myOrderedSFList->addElement(*(OrderedListElement *)mySFElement);
  if (rc == SUCCESS)
  {
    numberSFs++;
    if (flow_id > highestFlowID)
      highestFlowID = flow_id;

#ifdef TRACEME 
     printf("reseqMgr::addServiceFlow(%lf):SUCCEEDED to add this service flow:%d \n", 
		   Scheduler::instance().clock(),flow_id);
#endif
  }
  else
  {
#ifdef TRACEME 
     printf("reseqMgr::addServiceFlow(%lf):FAILED to add this service flow:%d \n", 
		   Scheduler::instance().clock(),flow_id);
#endif
     rc = -1;
     delete myNewSFObject;
     delete mySFElement;
     myNewSFObject = NULL;
  }

  return myNewSFObject;
}

/******************************************************************
* Method: * int serviceFlowMgr::addServiceFlow(int32_t src_ip, int32_t dst_ip, packet_t pkt_type, u_int16_t flow_id)
*
*explanation:
*  This method adds a service flow to the list.  
*
*  Pseudo code:
*     -check to see if the service flow already exists
*     -create the new SF Object
*     -add it to the list
*     -return the new flow id
*
*
*inputs:
*
*outputs:
*  Returns a valid flow id.  Else returns a -1
*
************************************************************************/
int reseqMgr::addServiceFlow(int macaddr, int32_t src_ip, int32_t dst_ip, packet_t pkt_type, u_int16_t flow_id,int myBGIDParam, int numberChannels)
{
serviceFlowObject * myNewSFObject = NULL;
int rc = -1;


  reseqFlowListElement *mySFElement = new reseqFlowListElement();
  mySFElement->key = flow_id;
  mySFElement->init(this, numberChannels);
  
  if (direction == DOWNSTREAM)
  {
     myNewSFObject = new DSserviceFlowObject(macaddr,src_ip,dst_ip,pkt_type,flow_id,myBGIDParam);
#ifdef TRACEME 
     printf("reseqMgr::addServiceFlow(%lf):DOWNSTREAM flowID:%d, macaddr:%d, src_ip:%d, dst_ip:%d,  pkt_type:%d \n", 
		   Scheduler::instance().clock(), flow_id,macaddr,src_ip, dst_ip,pkt_type);
#endif
  }
  else
  {
    myNewSFObject = new USserviceFlowObject(macaddr,src_ip,dst_ip,pkt_type,flow_id,myBGIDParam);
#ifdef TRACEME 
     printf("reseqMgr::addServiceFlow(%lf):UPSTREAM flowID:%d, macaddr:%d, src_ip:%d, dst_ip:%d,  pkt_type:%d \n",
           Scheduler::instance().clock(),flow_id,macaddr,src_ip, dst_ip,pkt_type);
#endif
  }


  myNewSFObject->init(direction,myMac,this);


  //init the element
  mySFElement->putServiceFlow(myNewSFObject);
  mySFElement->gapStartTime = 0;
  mySFElement->frameCount = 0;
  mySFElement->outOfOrderArrivalCount = 0;
  mySFElement->nextExpectedPSN = 0;

//TEST CASE
#ifdef TRACEMEA
  mySFElement->key = 5;
  rc = myOrderedSFList->addElement(*(OrderedListElement *)mySFElement);
//  rc = myOrderedSFList->addElement(*((serviceFlowListElement *)mySFElement));
  printf("reseqMgr::addServiceFlow(%lf):add element to OrderedSFList rc:%d, size of list:%d (element key:%d)\n", 
		   Scheduler::instance().clock(),rc, myOrderedSFList->getListSize(),mySFElement->key);
  mySFElement->key = 4;
  rc = myOrderedSFList->addElement(*(OrderedListElement *)mySFElement);
//  rc = myOrderedSFList->addElement(*((serviceFlowListElement *)mySFElement));
  printf("reseqMgr::addServiceFlow(%lf):add element to OrderedSFList rc:%d, size of list:%d (element key:%d)\n", 
		   Scheduler::instance().clock(),rc, myOrderedSFList->getListSize(),mySFElement->key);
  mySFElement->key = 6;
  rc = myOrderedSFList->addElement(*(OrderedListElement *)mySFElement);
//  rc = myOrderedSFList->addElement(*((serviceFlowListElement *)mySFElement));
  printf("reseqMgr::addServiceFlow(%lf):add element to OrderedSFList rc:%d, size of list:%d (element key:%d)\n", 
		   Scheduler::instance().clock(),rc, myOrderedSFList->getListSize(),mySFElement->key);

  rc = myOrderedSFList->isElement(4);
  printf("reseqMgr::addServiceFlow(%lf): call isElement with flowID:%d,  rc is %d \n", 
		   Scheduler::instance().clock(),flow_id,rc);
  rc = myOrderedSFList->displayListElements();

  mySFElement->key = 4;
  rc = myOrderedSFList->removeElement(*mySFElement);
  printf("reseqMgr::addServiceFlow(%lf):remove element from OrderedSFList rc:%d, size of list:%d \n", 
		   Scheduler::instance().clock(),rc, myOrderedSFList->getListSize());
  rc = myOrderedSFList->displayListElements();
#endif

//  rc = mySFList->addElement(*((serviceFlowListElement *)mySFElement));
  rc = myOrderedSFList->addElement(*(OrderedListElement *)mySFElement);
  if (rc == SUCCESS)
  {
    rc = flow_id;
    numberSFs++;
    if (flow_id > highestFlowID)
      highestFlowID = flow_id;

#ifdef TRACEME 
     printf("reseqMgr::addServiceFlow(%lf):SUCCEEDED to add this service flow:%d, macaddr:%d, src_ip:%d, dst_ip:%d,  pkt_type:%d \n", 
		   Scheduler::instance().clock(),flow_id,macaddr,src_ip, dst_ip,pkt_type);
#endif
  }
  else
  {
#ifdef TRACEME 
     printf("reseqMgr::addServiceFlow(%lf):FAILED to add this service flow:%d, macaddr:%d, src_ip:%d, dst_ip:%d,  pkt_type:%d \n", 
		   Scheduler::instance().clock(),flow_id,macaddr,src_ip, dst_ip,pkt_type);
#endif
     rc = -1;
     delete myNewSFObject;
     delete mySFElement;
  }

  return rc;
}


/***********************************************************************
*function: int reseqMgr::frameArrival(Packet *p, int channelNumber)
*
*explanation:
*  This method is invoked when a frame arrives on the channel.
*  It implements the DOCSIS30 resequencing rules.
*
*  Code:
*    case 1:  queueSize=0, packetPSN==nextExpected
*             pass up the pkt, nextExpectedPSN++
*    case 2:  queueSize=0, packetPSN!=nextExpected
*             queue the pkt, start WAIT_TIMER
*    case 3:  queueSize>0, packetPSN!=nextExpected
*             queue the pkt
*              special case 1: if #queued > reseqWindow assume pkt is dropped
*              special case 2: if got an out of seq pkt on each channel, assume pkt is dropped
*    case 4:  queueSize>0, packetPSN==nextExpected
*             Begin loop:
*                pass up the pkt, nextExpectedPSN++
*               nextPkt=lowestPSN pkt in the queue
*               if nextPktPSN == nextExpectedPSN
*                 pass up the pkt, nextExpectedPSN++
*               else
*                 if queueSize>0 reset WAIT_TIMER
*                 break
*             End loop
*
*   WAIT_TIMER_HANDLER:
*             Begin loop:
*               nextExpectedPSN++
*               nextPkt=lowestPSN pkt in the queue
*               if nextPktPSN == nextExpectedPSN
*                 pass up the pkt, nextExpectedPSN++
*               else
*                 if queueSize>0 reset WAIT_TIMER
*                 break
*             End loop
*
*     
*inputs:
*   Packet *p : ptr to the packet object
*   int channelNumber:  Channel that just delivered the pkt
*
*outputs:
*  returns a  SUCCESS or FAILURE
*  It might will pass 1 or more frames up to the mac.
*
************************************************************************/
int reseqMgr::frameArrival(Packet *p, int channelNumber)
{
  int rc = SUCCESS;
  reseqFlowListElement *SFListElement = NULL;
  serviceFlowObject *mySFObj = NULL;
  int queueSize = 0;
  struct hdr_cmn *ch = HDR_CMN(p);
  int i;
  int fastRecoveryFlag;


#ifdef TRACE_DEBUG
  myMac->tracePoint("reseqMgr::frameArrival:",0,0);
#endif
  docsis_dsehdr *chx = docsis_dsehdr::access(p);

//Step 1  Find the service flow state information
  SFListElement = getReseqServiceFlow(p);
  if (SFListElement == NULL) {
    printf("reseqMgr::frameArrival(%f) ERROR, SFListElement is NULL\n",
		 Scheduler::instance().clock());
  printf("reseqMgr::frameArrival[%s](%f)  FAILURE1:  (cm addr:%d) (PSN:%d) on channel %d (Total %12.0f) \n",
		 myMac->docsis_id,Scheduler::instance().clock(), myMac->addr(),chx->packet_sequence_number, 
         channelNumber,myStats.totalNumberArrivals);
    rc = FAILURE;
    goto frameArrivalExit;
  }


  mySFObj = SFListElement->getServiceFlow();
  if (mySFObj == NULL) {
    printf("reseqMgr::frameArrival(%f) ERROR, mySFObj is NULL\n",
		 Scheduler::instance().clock());
  printf("reseqMgr::frameArrival[%s](%f)  FAILURE2:  (cm addr:%d) (PSN:%d) on channel %d (Total %12.0f) \n",
		 myMac->docsis_id,Scheduler::instance().clock(), myMac->addr(),chx->packet_sequence_number, 
         channelNumber,myStats.totalNumberArrivals);
    rc = FAILURE;
    goto frameArrivalExit;
  }

  myStats.totalNumberArrivals++;
  myStats.totalByteArrivals+= ch->size();
  mySFObj->myStats.numberByteArrivals += ch->size();
  mySFObj->myStats.numberPacketArrivals++;
  mySFObj->lastPacketTime = Scheduler::instance().clock();

  mySFObj->updateStats(p,channelNumber,mySFObj->myBGID);

#ifdef TRACEME 
  printf("reseqMgr::frameArrival[%s](%f) (cm addr:%d) (PSN:%d) on channel %d (Total %12.0f) \n",
		 myMac->docsis_id,Scheduler::instance().clock(), myMac->addr(),chx->packet_sequence_number, 
         channelNumber,myStats.totalNumberArrivals);
#endif


  if (mySFObj->reseqFlag == 0) {
//$A801
#ifdef TRACEME 
    printf("reseqMgr::frameArrival[%s](%f) MATCH This service flow (flowID:%d, SF dsid:%d,pkt dsid:%d, PSN:%d) is configured to NOT have resequencing!!) \n",
		 myMac->docsis_id,Scheduler::instance().clock(), mySFObj->flowID,mySFObj->dsid,(int) chx->dsid,chx->packet_sequence_number);
#endif
    myStats.numberFramesSentUp++;
    myMac->RecvFrame(p,channelNumber);
    SFListElement->nextExpectedPSN++;
    goto frameArrivalExit;
  }

  queueSize = mySFObj->packetsQueued();
#ifdef TRACEME 
  printf("reseqMgr::frameArrival(%f)  service flowID:%d,, ch dsid:%d, queue size:%d, next expected PSN:%d, this PSN:%d\n",
		 Scheduler::instance().clock(), mySFObj->flowID, chx->dsid, queueSize,  SFListElement->nextExpectedPSN,chx->packet_sequence_number);
  printf("     This SFObject's reseqWindow:%d,  packeQ size:%d and reseqTimeout:%3.3f \n",
          mySFObj->reseqWindow,mySFObj->myPacketList->MAXLISTSIZE, mySFObj->reseqTimeout);
#endif

  //First, check for fast recovery
  //Special case 2:  if we've received an out of order pkt on all channels assume the lowset PSN was dropped
  if (chx->packet_sequence_number != SFListElement->nextExpectedPSN) {
    SFListElement->outOfOrderChannels[channelNumber]++;

    fastRecoveryFlag = 1;
    for (i=0;i<channelNumber;i++) {
#ifdef TRACEME 
        printf("reseqMgr::frameArrival(%f) (flowID:%d) fast recovery check i:%d, outOfOrderChannels:%d \n",
		 Scheduler::instance().clock(), mySFObj->flowID,i, SFListElement->outOfOrderChannels[i]);
#endif
      if (SFListElement->outOfOrderChannels[i]== 0) {
        fastRecoveryFlag = 0;
        break;
      }
    }
    if (fastRecoveryFlag == 1) {
#ifdef TRACEME 
        printf("reseqMgr::frameArrival(%f) (flowID:%d)  DO FAST RECOVERY \n",
		 Scheduler::instance().clock(), mySFObj->flowID);
#endif
        myStats.numberFastRecoveryEvents++;
        //Just adjust the expected PSN
        SFListElement->nextExpectedPSN++;
        //reset the check for fast recovery
        for (i=0;i<channelNumber;i++)
          SFListElement->outOfOrderChannels[i]= 0;
    }
  }
  else  {
    //reset the check for fast recovery
    for (i=0;i<channelNumber;i++)
      SFListElement->outOfOrderChannels[i]= 0;

  }

  if (chx->packet_sequence_number == SFListElement->nextExpectedPSN) {


    if (queueSize == 0) {
      //case 1: If nothing is queued and this is the next expected PSN, Pass the frame up 
#ifdef TRACEME 
      printf("reseqMgr::frameArrival(%f)   CASE 1 service flowID:%d, send up PSN:%d\n",
		 Scheduler::instance().clock(), mySFObj->flowID, chx->packet_sequence_number);
#endif
      myStats.numberFramesSentUp++;
      myMac->RecvFrame(p,channelNumber);
      SFListElement->nextExpectedPSN++;
      goto frameArrivalExit;
    }
    else {
      //case 4: expected pkt arrives and there are packets queued
      //first send up this packet
#ifdef TRACEME 
      printf("reseqMgr::frameArrival(%f)   CASE 4 service flowID:%d, send up PSN:%d\n",
		 Scheduler::instance().clock(), mySFObj->flowID, chx->packet_sequence_number);
#endif
      myStats.numberFramesSentUp++;
      myMac->RecvFrame(p,channelNumber);
      SFListElement->nextExpectedPSN++;

      //next, send up as many as we can -- until the pkt PSN > nextExepcted
        packetListElement *myLE;
        Packet *tmpPkt=NULL;
        int i;
//      rc = flushReseqQueue(SFListElement);
        //For  each packet in the queue,
        myLE = (packetListElement *)SFListElement->mySF->myPacketList->head;
        for (i=0;i<queueSize;i++) {
             if (myLE == NULL) {
#ifdef TRACEME 
               printf("reseqMgr::frameArrival(%f) (i:%d) reached end of List \n",
		            Scheduler::instance().clock(),i);
#endif
               break;
             }
             tmpPkt = myLE->getPacket();
             chx = docsis_dsehdr::access(tmpPkt);
#ifdef TRACEME 
             printf("reseqMgr::frameArrival(%f) (i:%d)  look at this pkt  PSN:%d\n",
		            Scheduler::instance().clock(),i, chx->packet_sequence_number);
#endif
             if (chx->packet_sequence_number == SFListElement->nextExpectedPSN) {
                tmpPkt = SFListElement->mySF->removePacket();
#ifdef TRACEME 
                printf("reseqMgr::frameArrival(%f)   service flowID:%d, send up PSN:%d\n",
		           Scheduler::instance().clock(), mySFObj->flowID, chx->packet_sequence_number);
#endif
                myLE = (packetListElement *)SFListElement->mySF->myPacketList->head;
                myStats.numberFramesSentUp++;
                myMac->RecvFrame(tmpPkt,channelNumber);
                SFListElement->nextExpectedPSN++;
             }
             if (chx->packet_sequence_number > SFListElement->nextExpectedPSN) {
#ifdef TRACEME 
               printf("reseqMgr::frameArrival(%f) LOOP BREAK (i:%d)  flowID:%d\n",
		           Scheduler::instance().clock(), i,mySFObj->flowID);
#endif
               break;
             }
        }
        if (SFListElement->reseqState == ReseqStateTimerActive) {
          SFListElement->reseqState = ReseqStateIDLE;
          SFListElement->myReseqTimer->stop((Packet *)&SFListElement->reseqEvent);
        }
        goto frameArrivalExit;
    }
  }
  else 
  {
    myStats.totalOutOfOrderArrivalCount++;
#ifdef TRACEME 
    printf("reseqMgr::frameArrival(%f)   OUT OF ORDER packet, flowID:%d,  state:%d,  nextExpected:%d, this  PSN:%d\n",
           Scheduler::instance().clock(), mySFObj->flowID, SFListElement->reseqState,
           SFListElement->nextExpectedPSN,chx->packet_sequence_number);
    printf("         totalNumberArrivals: %10.0f  totalByteArrivals: %10.0f numberQueued: %10.0f  numberFailures: %10.0f numberFramesSentUp: %10.0f totalOutOfOrderArrivalCount: %10.0f \n", 
      myStats.totalNumberArrivals, myStats.totalByteArrivals, myStats.numberQueued, myStats.numberFailures, myStats.numberFramesSentUp, myStats.totalOutOfOrderArrivalCount);
#endif

 
    if (queueSize == 0) {
      //case 2: If nothing is queued and this is NOT the next expected PSN 
      if ( (SFListElement->nextExpectedPSN - chx->packet_sequence_number) <= mySFObj->reseqWindow) {
#ifdef TRACEME 
        printf("reseqMgr::frameArrival(%f)   CASE 2 service flowID:%d, Queue this packet: %d\n",
		 Scheduler::instance().clock(), mySFObj->flowID, chx->packet_sequence_number);
#endif

        rc = mySFObj->addPacket(p);
        if (rc == FAILURE) {
          Packet::free(p);
          goto frameArrivalExit;
        }
        myStats.numberQueued++;

        if (SFListElement->reseqState == ReseqStateIDLE) {
          SFListElement->reseqState = ReseqStateTimerActive;

#ifdef TRACEME 
          printf("reseqMgr::frameArrival(%f) FlowID:%d,    Start reseq WAIT TIMER for %3.3f seconds\n",
		 Scheduler::instance().clock(), mySFObj->flowID, mySFObj->reseqTimeout);
#endif
          SFListElement->myReseqTimer->start( (Event *)&SFListElement->reseqEvent, mySFObj->reseqTimeout);
          goto frameArrivalExit;
        }
        else {
//#ifdef TRACEME 
          printf("reseqMgr::frameArrival(%f)(flowID:%d)  HARD ERROR:  reseqTimer State not IDLE (dsid:%d, queue size:%d, next expected PSN:%d, this PSN:%d\n",
	    	 Scheduler::instance().clock(), mySFObj->flowID, chx->dsid, queueSize,  SFListElement->nextExpectedPSN,chx->packet_sequence_number);
//#endif
          exit(1);
        }
      }
      else {
//This path should never happen
//#ifdef TRACEME 
        printf("reseqMgr::frameArrival(%f)  HARD ERROR:  packet arrival out of order and GAP (%d) exceeds max allowed (%d)\n",
		  Scheduler::instance().clock(), mySFObj->flowID, (SFListElement->nextExpectedPSN - chx->packet_sequence_number),
          mySFObj->reseqWindow);
//#endif

        myStats.numberGAPExceededEvents++;
        exit(1);
//        rc = FAILURE;
//        Packet::free(p);
//        goto frameArrivalExit;
      }
    }
    else {
      //case 3: packets queued and this is NOT the next expected PSN 
//      if (queueSize <= mySFObj->reseqWindow) {
      if ( (SFListElement->nextExpectedPSN - chx->packet_sequence_number) <= mySFObj->reseqWindow) {
#ifdef TRACEME 
        printf("reseqMgr::frameArrival(%f)   CASE 3 service flowID:%d, queue the pkt (#in queue:%d)\n",
		  Scheduler::instance().clock(), mySFObj->flowID, chx->packet_sequence_number,mySFObj->packetsQueued());
#endif
        rc = mySFObj->addPacket(p);
        if (rc == FAILURE) {
          Packet::free(p);
          goto frameArrivalExit;
        }
        myStats.numberQueued++;
      }
      else {
#ifdef TRACEME 
        printf("reseqMgr::frameArrival(%f)   ERROR:  packet arrival out of order and GAP (%d) exceeds max allowed (%d)\n",
		  Scheduler::instance().clock(), mySFObj->flowID, (SFListElement->nextExpectedPSN - chx->packet_sequence_number),
          mySFObj->reseqWindow);
#endif
        myStats.numberGAPExceededEvents++;
        rc = FAILURE;
        Packet::free(p);
        goto frameArrivalExit;
      }
    }
  }

frameArrivalExit:
//$A800
 if (mySFObj != NULL) {
   queueSize = mySFObj->packetsQueued();
#ifdef TRACEME 
    printf("reseqMgr::frameArrival(%f) (flowID:%d)  Exit with queue size:%d and rc:%d\n",
		 Scheduler::instance().clock(), mySFObj->flowID, queueSize,rc);
#endif
  }
  if (rc == FAILURE)
    myStats.numberFailures++;
  return rc;
}

/***********************************************************************
*function: int reseqMgr::flushReseqQueue(int nextExpectedPSN)
*
*explanation:
*  This method 
*
*
*inputs:
*
*outputs:
*  returns the number of packets sent up the stack.  
*
************************************************************************/
int reseqMgr::flushReseqQueue(reseqFlowListElement * SFListElement)
{
  int rc = 0;
  serviceFlowObject *mySFObj = NULL;
  int queueSize = 0;
  int i;


#ifdef TRACEME 
  printf("reseqMgr::flusReseqQueue(%f) next ExpectedPSN:%d, queue level: %d \n",
		 Scheduler::instance().clock(),SFListElement->nextExpectedPSN, queueSize);
#endif

  //Look at each packet queued. Pass up any 
  for (i=0;i<queueSize;i++) {
//      myMac->RecvFrame(p,channelNumber);
  }

  return rc;
}


/***********************************************************************
*function: reseqFlowListElement *reseqMgr::getServiceFlow(Packet *p)
*
*explanation:
*  This method returns a reference to a reseqFlowListElement that
*  matches the packet based on a classifier match.  This method
*  returns a NULL if the SF does not exist in the current List.
 *   Examines the packet and matches it to a flow in the SF List.
*   The match is based solely on packet type or flow ID.
*
*inputs:
*   Packet *p : ptr to the packet object
*
*outputs:
*  returns a reference to a reseqFlowListElement
*  Returns a NULL if the list is empty or if there is no match
*
************************************************************************/
reseqFlowListElement *reseqMgr::getReseqServiceFlow(Packet *p)
{
  serviceFlowObject *SFObject = NULL;
  serviceFlowObject *tmpSFObject = NULL;
  reseqFlowListElement *SFListElement = NULL;
  reseqFlowListElement *tmpPtr = NULL;
  reseqFlowListElement *returnListElement = NULL;
  struct hdr_cmn *ch = HDR_CMN(p);
  struct hdr_ip *chip = HDR_IP(p);


#ifdef TRACEME 
   printf("reseqMgr::getServiceFlow(%lf): Try to match pkt: (src,dst,type):%d,%d,%d), #SFs:%d\n",
		    Scheduler::instance().clock(), chip->saddr(), chip->daddr(), ch->ptype(),numberSFs);
#endif

  if (IsFrameMgmt(p))
  {
    returnListElement = &MGMTServiceFlowLE;
  }      
  else
  {      

   //Go throught the list and look for an explicit match 
//  tmpPtr = (reseqFlowListElement *) mySFList->head;
  tmpPtr = (reseqFlowListElement *) myOrderedSFList->head;
  while (tmpPtr != NULL) 
  {
#ifdef TRACEME 
      printf("reseqMgr::getServiceFlow(%lf):Begin looking at the list, first element:%0x\n ",
		    Scheduler::instance().clock(),tmpPtr);
#endif
    tmpSFObject =  tmpPtr->getServiceFlow();
    if ((tmpSFObject != NULL) && (tmpSFObject->type == DATA)) 
    {
#ifdef TRACEME 
        printf("reseqMgr::getServiceFlow(%lf):Try to match pkt with this SF: flowID:%d \n ",
		    Scheduler::instance().clock(), tmpSFObject->flowID);
		tmpSFObject->printSFInfo();
#endif

//         if (tmpSFObject->match(p)) {
         if (tmpSFObject->matchPktType(p)) {
#ifdef TRACE 
           printf("reseqMgr::getServiceFlow: FOUND A MATCH with list element %0x \n",tmpPtr);
#endif

           SFObject = tmpSFObject;
           returnListElement = tmpPtr;
		   break;
         }
	}
    tmpPtr = (reseqFlowListElement *)tmpPtr->next;
  }

  if (SFObject == NULL)
  {

    //IF there was not an explicit match, let's look for the default reseq flow
//    tmpPtr = (reseqFlowListElement *) mySFList->head;
    tmpPtr = (reseqFlowListElement *) myOrderedSFList->head;
    while (tmpPtr != NULL) 
    {
#ifdef TRACEME 
      printf("reseqMgr::getServiceFlow(%lf):Begin looking at the list, first element:%0x\n ",
		    Scheduler::instance().clock(),tmpPtr);
#endif
      tmpSFObject =  tmpPtr->getServiceFlow();
      if ((tmpSFObject != NULL) && (tmpSFObject->type == DATA)) 
      {

         if (tmpSFObject->matchAddress(p)) {
#ifdef TRACE 
           printf("reseqMgr::getServiceFlow: FOUND A MATCH with list element %0x \n",tmpPtr);
#endif

           SFObject = tmpSFObject;
           returnListElement = tmpPtr;
		   break;
         }
	  }
      tmpPtr = (reseqFlowListElement *)tmpPtr->next;
    }

  }
#ifdef TRACEME 
  if (SFObject == NULL)
     printf("reseqMgr::getServiceFlow(%lf): DID NOT Find a match pkt\n",
		    Scheduler::instance().clock());
  else
     printf("reseqMgr::getServiceFlow(%lf): FOUND a match pkt: flowID:%d\n",
		    Scheduler::instance().clock(), SFObject->flowID);
#endif

  }
  return returnListElement;
}

void reseqMgr::getStats(struct reseqMgrStatsType *callersStats)
{
  *callersStats = myStats;
}


void reseqMgr::printStatsSummary(char *outputFile)
{
  serviceFlowObject *SFObject = NULL;
  serviceFlowObject *tmpSFObject = NULL;
  reseqFlowListElement *SFListElement = NULL;
  reseqFlowListElement *tmpPtr = NULL;

  printf("reseqMgr::printStatsSummary(%f)(mac:%s)  Number arrivals:%8.0f(#Bytes:%12.0f) Number passed up: %12.0f Number of Failures:%8.0f  NumberQueued:%12.0f TotalOutOfOrderArrivals:%8.0f Number GAP EXceeded Events:%8.0f  NumberFastRecoveryEvents:%8.0f\n",
		 Scheduler::instance().clock(), myMac->docsis_id,  myStats.totalNumberArrivals,myStats.totalByteArrivals, myStats.numberFramesSentUp,myStats.numberFailures, myStats.numberQueued, myStats.totalOutOfOrderArrivalCount,myStats.numberGAPExceededEvents,myStats.numberFastRecoveryEvents);

  if (direction == DOWNSTREAM)
    printf("reseqMgr::printStatsSummary(%lf): Number of DS Service Flows in the list: %d \n",
            Scheduler::instance().clock(), numberSFs);
  else
    printf("reseqMgr::printStatsSummary(%lf): Number of US Service Flows in the list: %d \n",
            Scheduler::instance().clock(), numberSFs);

  myOrderedSFList->displayListElements();

  //Go through the list and look for an explicit match 
//  tmpPtr = (reseqFlowListElement *) mySFList->head;
  tmpPtr = (reseqFlowListElement *) myOrderedSFList->head;
  while (tmpPtr != NULL)
  {
    tmpSFObject =  tmpPtr->getServiceFlow();
    if (tmpSFObject != NULL)
    {
        printf("reseqMgr::printStatsSummary(%lf):SF: flowID:%d (#timerHandlers:%d)\n ",
            Scheduler::instance().clock(), tmpSFObject->flowID, tmpPtr->numberTimerHandlerEvents);
        tmpSFObject->printStatsSummary(outputFile);
    }
    tmpPtr = (reseqFlowListElement *)tmpPtr->next;
  }

}



/***************************************************************************
 * Module: macPhyInterface
 *
 * Explanation:
 *   This file contains the class definition of the object that handls
 *   all aspects of the underlying phy interface.  This object is passed
 *   PDU's from the LLC of the MAC.  And at its lower layer it interacts
 *   with the medium object to send/recv PHY frames.
 *
 * TODO:  can remove the SFMgr pointers
 *
 * Revisions:
 *
************************************************************************/

#ifndef ns_macPhyInterface_h
#define ns_macPhyInterface_h

#include "packet.h"
#include "rtp.h"
#include "hdr-docsis.h"

#include "docsisMonitor.h"
#include "channelStats.h"

class medium;
class schedulerObject;
class serviceFlowMgr;
class serviceFlowObject;
class reseqMgr;
class outboundChannelMgr;

struct macPhyInterfaceStatsType
{
  double  numberSendFrames;
  double  numberRecvFrames;
  double  numberTxCompletions;
};



/*************************************************************************
 ************************************************************************/
class macPhyInterface
{
public:
  macPhyInterface();
  virtual ~macPhyInterface();

  virtual void init(int nodeNumber, medium *myMediumParam);
  virtual int SendFrame(Packet *p,int channelNumber,MacDocsis *myMacPtr);
  virtual int SendFrame(Packet *p,struct schedulerAssignmentType *schedAssignment, serviceFlowObject *mySF,MacDocsis *myMacPtr);
  virtual int SendFrame(Packet *p, serviceFlowObject *mySF, int channelNumber,MacDocsis *myMacPtr);
  virtual int SendFrame(struct schedulerAssignmentType *schedAssignment, serviceFlowObject *mySF, MacDocsis *myMacPtr);
  virtual int RecvFrame(Packet *p, int channelNumber);
  virtual int getChannelStatus(int channelNumber);
  virtual void printStatsSummary();
  virtual void getStats(struct macPhyInterfaceStatsType *callersStats);
  virtual void getChannelStats(struct channelStatsType  *channelStats, int channelNumber);

  virtual int getOverHead(Packet *p, int channelNumber);
  virtual int getOverHead(int bytes, int channelNumber);

  void getChannelProperties(struct channelPropertiesType  *channelProperties, int channelNumber);

  void setMyMac(MacDocsis *myMac);
  void setMyMedium(medium *myMediumParam);
  void setMyDSScheduler(schedulerObject *mySchedulerParam);
  void setMyUSScheduler(schedulerObject *mySchedulerParam);
  void setMyDSSFMgr(serviceFlowMgr *mySFMgrParam);
  void setMyUSSFMgr(serviceFlowMgr *mySFMgrParam);
  void linkMediumToDSSFMgr(void);
  int  getNumberDSChannels();
  int  getNumberUSChannels();


  virtual int TxCompletionHandler(Packet *p, int channelNumber);
  virtual int TxCompletionHandler(int channelNumber);

  //$A307
  reseqMgr *myReseqMgr;
  medium *myMedium;

  struct macPhyInterfaceStatsType myStats;
  int numberDSChannels;
  int numberUSChannels;

protected:


  MacDocsis *myMac;
  schedulerObject *myUSScheduler;
  schedulerObject *myDSScheduler;
  serviceFlowMgr *myUSSFMgr;
  serviceFlowMgr *myDSSFMgr;


  int nodeNumber;

  outboundChannelMgr *myOutChannelMgr;

  docsisMonitor *myDSMonitor;
  docsisMonitor *myUSMonitor;


private:

};

class CMTSmacPhyInterface: public macPhyInterface
{
public:
  CMTSmacPhyInterface();

  void init(int nodeNumber, medium *myMediumParam);
  int SendFrame(struct schedulerAssignmentType *schedAssignment, serviceFlowObject *mySF, MacDocsis *myMacPtr);
  int RecvFrame(Packet *p, int channelNumber);
  int TxCompletionHandler(Packet *p, int channelNumber);
  int TxCompletionHandler(int channelNumber);
  int getOverHead(Packet *p, int channelNumber);
  int getOverHead(int bytes, int channelNumber);

private:

};

class CMmacPhyInterface: public macPhyInterface
{
public:
  CMmacPhyInterface();
  
  void init(int nodeNumber, medium *myMediumParam);
  int SendFrame(Packet *p, int channelNumber, MacDocsis *myMacPtr);
  int RecvFrame(Packet *p, int channelNumber);
  int TxCompletionHandler(Packet *p, int channelNumber);
  int TxCompletionHandler(int channelNumber);
  int getOverHead(Packet *p, int channelNumber);
  int getOverHead(int bytes, int channelNumber);
  void printStatsSummary();

private:

};



#endif /* __ns_macPhyInterface_h__ */

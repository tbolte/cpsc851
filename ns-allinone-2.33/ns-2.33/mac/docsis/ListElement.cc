/************************************************************************
* File:  ListElement.cc
*
* Purpose:
*  This contains the base list element object.
*  And an IntegerListElement Object
*
* Version:
*    1.0:   12/20/2000
*
* Changes:
*
************************************************************************/
#include "ListElement.h"
#include <iostream>

#include "docsisDebug.h"
//#define TRACEME 0

/***********************************************************************
*function: 
*
*explanation:
*
*inputs:
*
*outputs:
************************************************************************/

ListElement::ListElement() 
{
#ifdef TRACEME
  printf("ListElement: constructed list Element \n");
#endif
  next = NULL;
  prev = NULL;

}

ListElement::~ListElement() 
{
#ifdef TRACEME
  printf("ListElement: destructed list Element \n");
#endif
}

int ListElement::putData(void* dataParm) 
{
  data = dataParm;
  return 0;
}

void * ListElement::getData() 
{
  return(data);
}


/***********************************************************************
*function: 
*
*explanation:
*
*inputs:
*
*outputs:
************************************************************************/
IntegerListElement::IntegerListElement() 
{
#ifdef TRACEME
  printf("IntegerListElement: constructed list Element \n");
#endif
  next = NULL;
  prev = NULL;

}

IntegerListElement::~IntegerListElement() 
{
#ifdef TRACEME
  printf("IntegerListElement: destructed list Element \n");
#endif
}

int IntegerListElement::putData(int dataParm) 
{
  data = dataParm;
  return 0;
}

int IntegerListElement::getData() 
{
  return(data);
}


/***********************************************************************
*function: 
*
*explanation:
*
*inputs:
*
*outputs:
************************************************************************/
OrderedListElement::OrderedListElement()  : ListElement()
{
#ifdef TRACEME
  printf("OrderedListElement: constructed list Element \n");
#endif

}

OrderedListElement::~OrderedListElement() 
{
#ifdef TRACEME
  printf("OrderedListElement: destructed list Element \n");
#endif
}

int OrderedListElement::putKey(int keyParam) 
{
  key = keyParam;
  return 0;
}

int OrderedListElement::getKey() 
{
  return(key);
}





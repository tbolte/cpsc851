/***************************************************************************
 * Module: channelStats
 *
 * Explanation:
 *   This file contains the channel status structure definition
 *
 * Objects:
 *
 * Revisions:
 *
************************************************************************/


#ifndef ns_channel_stats_h
#define ns_channel_stats_h


struct channelStatsType
{

  int channelNumber;
  u_int32_t channelCapacity;
  double channelUtilization;
  double numberCollisions;
  double util_total_bytes_US;    
  double util_total_bytes_DS;    
  double util_total_pkts_US;        
  double util_total_pkts_DS;        
  double util_bytes_US;          
  double util_bytes_DS;          
  double total_num_sent_frames; 
  double total_num_sent_bytes; 
  double total_num_rx_frames; 
  double total_num_rx_bytes; 
  double total_num_BW_bytesUP;   
  double total_num_BW_bytesDOWN; 
  double  total_packets_dropped;
};

#endif /* __ns_channel_stats_h__ */

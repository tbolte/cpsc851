/***************************************************************************
 * Module: bondingGroupObject
 *
 * Explanation:
 *   This file contains the class definition of a bonding group.
 *
 * Revisions:
 *   $A806: lots of revisions to support hierarchical scheduling
 *   $A906 :v1 of FAIRSHARE AQM
 *   $A910 :v1 of BROADBAND AQM
 *
 *  TODO:
 *
************************************************************************/

#ifndef ns_bondingGroupObject_h
#define ns_bondingGroupObject_h

#include "globalDefines.h"

#include "medium.h"


class ServiceFlowSetObject;
class macPhyInterface;
//class IntegerListObj;
class ListObj;
class aggregateSFObject;
class BGClassifierObject;
class packetSchedulerObject;

struct bondingGroupObjectStatsType
{
  int numberServiceFlows;
  double avgArrivalRate;
  double avgServiceRate;
  double avgLossRate;
  double avgPacketDelay;
};


#define FAIRSHARE_PENALTY_TIME 4.9

/*************************************************************************
 ************************************************************************/
class bondingGroupObject
{
public:
  bondingGroupObject();
  ~bondingGroupObject();

//  void init(int BGID,medium *myMedium, bondingGroupMgr *myBGMGr, int hierarchicalMode, int queueServiceDiscipline, int QSize, int QTypeParam, int numberAggregateQueues, double maxRateParam);
  void init(int BGID,medium *myMedium, bondingGroupMgr *myBGMGr);
  void setMyPhyInterface(CMTSmacPhyInterface *myPhyInterfaceParam);
  int addChannelToBG(int ChIDX);
  int  removeChannelFromBG(int chIDX);
  int checkChannelInBG(int chIDX);
  int isAnyChannelFree(void);
  int isChannelFree(int chIDX);
  int findFirstAvailableChannel();

  int getChannelSet(struct channelSet *mySet);
  int getSFSet(struct serviceFlowSet *mySet);
  int getCompleteSFSet(struct serviceFlowSet *mySet);
  int getCompleteAggSFSet(struct serviceFlowSet *mySet);
  int addSFToBG(serviceFlowObject *mySFObjParam);
  int  removeSFFromBG(serviceFlowObject *mySFObjParam);
  int isSFInBG(serviceFlowObject *mySFObjParam);

  int anyPacketsQueued();
  int packetsQueued();
  void configureBG(int hierarchicalModeParam, int QSize, int QType, int QDiscipline, int numberQueues, double serviceRate);

  void newArrivalUpdate(Packet *p, serviceFlowObject *mySFObject);
  void updateStats(Packet *p, int channelNumber, int bondingGroupNumber, serviceFlowObject *mySFObject);
  void printStatsSummary(char *outputFile);
  void printBondingGroupObject();
  int myBGID;
  int optimize(int opCode);

//$A806
  double BGmaxRate;
  int SchedQSize;
  //Type of AQM
  int SchedQType;
  double BGWeight;
  int BGPriority;
  int BGDefaultQuantum;
  double BGMAXp;
  //type of scheduling over the queues
  int queueServiceDiscipline;
  int numberAggregateQueues;
  int adaptiveMode;
  int nextAggSF_FlowID;

  int addPacket(Packet *p, serviceFlowObject *mySFObject);


  //This is the head of the array of aggregate flow lists
  aggregateSFObject *myAggSFObjects;
  BGClassifierObject *myClassifier;
  int numberSFs;
  int numberHiPriSFs;   // For Fairshare: the number can change dynamically
  int numberLoPriSFs;   // For Fairshare: the number can change dynamically

  packetSchedulerObject *myPktScheduler;
  double     avgServiceRate;
  bondingGroupMgr *myBGMgr;

private:

  int hierarchicalMode;
  int attributes;
  medium *myMedium;
  macPhyInterface *myPhyInterface;

//  ListObj *myChannelSet;
  IntegerListObj *myChannelSet;
  ListObj *mySFSet;

  bondingGroupObjectStatsType myStats;

  int adjustFairShareState();
  int adjustFairShareAQMState();
  int adjustBROADBANDAQMState();
  void   updateRates();
  double     avgArrivalRate;
  double     byteArrivals;
  double     byteDepartures;
  double     lastRateSampleTime;
  double  rateWeight;

//Common to Fairshare
  double Vcap;
  //number of optimization ticks before we react
  double controlFrequency;
  double arrivalCount;
  double lossCount;


//Params for Fairshare
  double GoodToBadRateThreshold;
  double BadToGoodRateThreshold;
  double FairShareCapacity;
  double FairShareCongestionThresh;
  float  myGoodToBadRateThresholdCoeff;
  float  myBadToGoodRateThresholdCoeff;

// $A906 
  int   numberSFlowsInState2;


};


#endif /* __ns_bondingGroupObject_h__ */

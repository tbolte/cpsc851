/***************************************************************************
 * Module: serviceFlowObject
 *
 * Explanation:
 *   This file contains the class definition of a service flow.
 * 
 *
 * Revisions:
 *
 *  Might want to use ns2's PacketQueue; instead of our ListObj
 *  $A601 : 10-11-2009 : avgQueueDelay is now based on the numberPacketsServiced
 *        counter.
 *  $A602 : 10-27-2009 : when new arrival, all list's new arrival method.
 *          And, when remove an element , update the queue packet delay
 *          monitor.   And, on channel status, get the avg packet latency
 *  $A800:  6-1-2010 fixed SF classifier (match) :now totally ignores S/D address
 *  $A806: Support hierarchical scheduling
 *  $A807 :  Added support to monitor rates  
 *  $A900 :  Added monitors for time flow spends in state 1 and 2
 *  $A901 :  Bug fix 
 *  $A902 :  An Aggregated SF will compute its final arrival/service rate assuming a timeInterval of entire sim time
 *  $A904 : Monitor rate of FS state transitions
 *  $A907  : Added CoDel AQM
 *  $A908  : Added PIE AQM
 *
************************************************************************/

#include "serviceFlowObject.h"
#include "bondingGroupObject.h"

#include "docsisDebug.h"
//#define TRACEME 0

#include "ListObj.h"
#include "AQMObj.h"
//#include "REDQ.h"
#include "BaseRED.h"
#include "BLUEQ.h"
#include "BAQM.h"
#include "CoDelAQM-TD.h"
#include "PIE-AQM.h"



serviceFlowObject::serviceFlowObject()
{

  constructionInit();
  macaddr = -1;
  flowID = -1;
  dsid = -1;
  myBGID = -1;
  classifier.src_ip = -1;
  classifier.dst_ip = -1;
  classifier.pkt_type = -1;
#ifdef TRACEME 
  printf("serviceFlowObject::constructor: \n");
#endif 
}

serviceFlowObject::serviceFlowObject(int macaddrParam,int32_t src_ip, int32_t dst_ip, packet_t pkt_type, u_int16_t flowIDParam, int myBGIDParam)
{

  constructionInit();
  macaddr = macaddrParam;
  flowID = flowIDParam;
//$A801
  dsid = -1;
  myBGID = myBGIDParam;
  classifier.src_ip = src_ip;
  classifier.dst_ip = dst_ip;
  classifier.pkt_type = pkt_type;

#ifdef TRACEME 
  printf("serviceFlowObject::constructor:, src_ip:%d, dst_ip:%d, pkt_type:%d myBG %d, flowID:%d, dsidParam:%d \n",
     src_ip,dst_ip,pkt_type,myBGID,flowID,dsid);
#endif 
}

serviceFlowObject::~serviceFlowObject()
{
#ifdef TRACEME 
  printf("serviceFlowObject::destructor: \n");
#endif 
}

void serviceFlowObject::constructionInit()
{
 macaddr = -1;
 flowID = 0;
//$A801
 dsid = -1;
 myBGID = 0;
 type = DATA_PKT;
 nextPSN=0;
 classifier.src_ip = 0;
 classifier.dst_ip = 0;
 classifier.pkt_type = 0;
 flowPriority = 0;
 minRate = 0;
//$A806
 maxRate =  0;
 minLatency = 0;
 SchedQType = FIFOQ;
 SchedQSize = 50;
 SchedServiceDiscipline = RR;
 reseqFlag = 0;
 reseqWindow = 1000;
 reseqTimeout = .020;
 deficitCounter = 0;
 flowQuantum = 1500;
 flowWeight = 1.0;

//$A807
  lastSchedulingEvent = 0.0;

 burstTimeScale = .01; 
 serviceRateBurstCounter = 0;
 largestServiceRateBurst = 0.0;
 lastBurstTimeCalc = 0.0;

  lastRateSampleTime = 0.0;
//$A807
  byteArrivals = 0;
  byteDepartures = 0;
  avgArrivalRate  = 0.0;
  avgServiceRate  = 0.0;
  rateWeight = 0.40;

//$A806
  hierarchicalMode = FALSE;
//$A902
  serviceFlowType = SINGLE_SF;

#ifdef TRACEME 
  printf("serviceFlowObject::construction:Init: \n");
#endif
}


void serviceFlowObject::init(int directionParam, MacDocsis *myMacParam,serviceFlowMgr * mySFMgrParam)
{
  direction = directionParam;
  myMac = myMacParam;
  mySFMgr = mySFMgrParam;
  myPacketCount = 0;
  myPacketList = new ListObj();
  myPacketList->initList(SERVICE_FLOW_OBJECT_MAX_PACKETS);
  bzero((void *)&myStats,sizeof(struct serviceFlowObjectStatsType));

  //To support monitoring loss rate (see dumpDOCSISQUEUEStats)
  monitorDropCount = 0;
  monitorPacketArrivalCount = 0;

  qnp_ = 0;
  qnb_ = 0;
  max_qnp_ = 0;                  /* Max # of pkts observed */
  max_qnb_ = 0;                  /* Max # of bytes observed */
  min_qnp_ = 0;                  /* Min # of pkts  observed */
  min_qnb_ = 0;                  /* Min # of bytes observed */
  queue_total_bytes_in = 0;
  queue_total_bytes_out = 0; 
  qlim_ =0;                    /* max queue size in packets */
  lastDumpTime = Scheduler::instance().clock();
  lastUtilDumpTime = Scheduler::instance().clock();

  deficitCounter = 0;
 burstTimeScale = .01; 
 serviceRateBurstCounter = 0;
 largestServiceRateBurst = 0.0;
 lastBurstTimeCalc = 0.0;

  totalPacketQueueTime = 0;
  packetQueueTimeCount = 0;

  lastPacketTime = 0.0;
  lastDequeueTime = 0.0;

  lastTimeServiced = 0.0;
  interServiceTime1Total = 0.0;
  interServiceTime1Count = 0;
  largestInterServiceTime1 = 0.0;
  interServiceTime2Total = 0.0;
  interServiceTime2Count = 0;
  largestInterServiceTime2 = 0.0;


//$A900
  myStats.lastStateChange = Scheduler::instance().clock();

#ifdef TRACEME 
  printf("serviceFlowObject::init:  direction: %d \n",direction);
  printf("     created packet List Object, head:%d, tail:%d \n",myPacketList->head,myPacketList->tail);
#endif 
}

/***********************************************************************
*function: void serviceFlowObject::setQueueBehavior(int maxQSize, int QType, int priorityP, int QParam1, int QParam2, int QParam3, double QParam4, double QParam5)
*
*explanation:  This method sets the tcl params to the queue
*
*inputs:
* int maxQSize
* int QType
* int priorityP
* int QParam1 : minth
* int QParam2 : maxth
* int QParam3 : adaptiveMode
* double QParam4 : maxp
* double QParam5  :  averaging filter time constant 
*          mySFObj->setQueueBehavior(mySFObj->SchedQSize, mySFObj->SchedQType, mySFObj->flowPriority, (int)y,(int)x,mySFObj->adaptiveMode,mySFObj->MAXp,mySFObj->flowWeight);
*
*outputs:
*
************************************************************************/
void serviceFlowObject::setQueueBehavior(int maxQSize, int QType, int priorityP, int QParam1, int QParam2, int QParam3, double QParam4, double QParam5)
{

  SchedQType = QType;
  SchedQSize = maxQSize;
  flowPriority = priorityP;

  if (myPacketList != NULL)
    delete myPacketList;

//#ifdef TRACEME 
//$A901
  if (direction == UPSTREAM)
    printf("serviceFlowObject::setQueueBehavior:  UPSTREAM DIRECTION flowID:%d, QSize:%d, QType:%d, priorityP:%d, param1:%d, param2:%d, param3:%d, param4:%2.2f, param5:%2.2f \n",
      flowID,maxQSize,QType,priorityP,QParam1,QParam2,QParam3,QParam4,QParam5);
  else
    printf("serviceFlowObject::setQueueBehavior:  DOWNSTREAM DIRECTION flowID:%d, QSize:%d, QType:%d, priorityP:%d, param1:%d, param2:%d, param3:%d, param4:%2.2f, param5:%2.2f \n",
      flowID,maxQSize,QType,priorityP,QParam1,QParam2,QParam3,QParam4,QParam5);

//#endif 
  switch (QType)
  {

    case FIFOQ:
//      myPacketList = new ListObj();
//      myPacketList->initList(maxQSize);
//      myPacketList->listID = flowID;
      myPacketList = new BaseRED();
      myPacketList->initList(maxQSize);
      myPacketList->listID = flowID;
      //For FIFOQ, need to set adaptiveMode to 3
      ((BaseRED *)myPacketList)->setAQMParams(maxQSize,flowPriority, QParam1,QParam2, 3, QParam4, QParam5);
      if ((flowID == TRACED_AggSFID1) || (flowID == TRACED_AggSFID2) || (flowID == TRACED_SFID) || (flowID == TRACED_SFID2) ) {
        ((BaseRED *)myPacketList)->traceFlag = 1;
//#ifdef TRACEME 
          printf("serviceFlowObject::setQueueBehavior: FIFOQ  SET traceFlag to 1 flowID:%d, QSize:%d, QType:%d, priorityP:%d, param1:%d, param2:%d, param3:%d, param4:%2.2f, param5:%2.2f \n",
                flowID,maxQSize,QType,priorityP,QParam1,QParam2,QParam3,QParam4,QParam5);
//#endif 
      }
      break;

    case OrderedQ:
      myPacketList = new OrderedListObj();
      myPacketList->initList(maxQSize);
      myPacketList->listID = flowID;
      break;

    case RedQ:
//      myPacketList = new REDQ();
      myPacketList = new BaseRED();
      myPacketList->initList(maxQSize);
      myPacketList->listID = flowID;
//      myPacketList->setAQMParams(maxQSize,QParam1,QParam2,QParam3,QParam4);
      ((BaseRED *)myPacketList)->setAQMParams(maxQSize,flowPriority, QParam1,QParam2, QParam3, QParam4, QParam5);
//      ((REDQ *)myPacketList)->setAQMParams(maxQSize,flowPriority, QParam1,QParam2, QParam3, QParam4, QParam5);
      if ((flowID == TRACED_AggSFID1) || (flowID == TRACED_AggSFID2) || (flowID == TRACED_SFID) || (flowID == TRACED_SFID2) ) {
        ((BaseRED *)myPacketList)->traceFlag = 1;
//        ((REDQ *)myPacketList)->traceFlag = 1;
//#ifdef TRACEME 
          printf("serviceFlowObject::setQueueBehavior: SET traceFlag to 1 flowID:%d, QSize:%d, QType:%d, priorityP:%d, param1:%d, param2:%d, param3:%d, param4:%2.2f, param5:%2.2f \n",
                flowID,maxQSize,QType,priorityP,QParam1,QParam2,QParam3,QParam4,QParam5);
//#endif 
      }
      break;

    case AdaptiveRedQ:
      myPacketList = new BaseRED();
      myPacketList->initList(maxQSize);
      myPacketList->listID = flowID;
//      myPacketList->setAQMParams(maxQSize,QParam1,QParam2,QParam3,QParam4);
      ((BaseRED *)myPacketList)->setAQMParams(maxQSize,flowPriority, QParam1,QParam2, QParam3, QParam4, QParam5);
//      if (flowID == TRACED_SFID)
      if ((flowID == TRACED_AggSFID1) || (flowID == TRACED_AggSFID2) || (flowID == TRACED_SFID) || (flowID == TRACED_SFID2) ) {
        ((BaseRED *)myPacketList)->traceFlag = 1;
      }
      break;

    case DelayBasedRedQ:
      myPacketList = new BaseRED();
      myPacketList->initList(maxQSize);
      myPacketList->listID = flowID;
//      myPacketList->setAQMParams(maxQSize,QParam1,QParam2,QParam3,QParam4);
      ((BaseRED *)myPacketList)->setAQMParams(maxQSize,flowPriority, QParam1,QParam2, QParam3, QParam4, QParam5);
//      if (flowID == TRACED_SFID)
      if ((flowID == TRACED_AggSFID1) || (flowID == TRACED_AggSFID2) || (flowID == TRACED_SFID) || (flowID == TRACED_SFID2) ) {
        ((BaseRED *)myPacketList)->traceFlag = 1;
//#ifdef TRACEME 
          printf("serviceFlowObject::setQueueBehavior: SET traceFlag to 1 flowID:%d, QSize:%d, QType:%d, priorityP:%d, param1:%d, param2:%d, param3:%d, param4:%2.2f, param5:%2.2f \n",
                flowID,maxQSize,QType,priorityP,QParam1,QParam2,QParam3,QParam4,QParam5);
//#endif 
      }
      break;


    case BlueQ:
      myPacketList = new BLUEQ();
      myPacketList->initList(maxQSize);
      myPacketList->listID = flowID;
      ((BLUEQ *)myPacketList)->setAQMParams(maxQSize,flowPriority,QParam1,QParam2, QParam3, QParam4, QParam5);
      if ((flowID == TRACED_AggSFID1) || (flowID == TRACED_AggSFID2) || (flowID == TRACED_SFID) || (flowID == TRACED_SFID2) ) {
        ((BLUEQ *)myPacketList)->traceFlag = 1;
       }
      break;

//$A907
    case CDAQM:
      myPacketList = new CoDelAQM();
      myPacketList->initList(maxQSize);
      myPacketList->listID = flowID;
      ((CoDelAQM *)myPacketList)->setAQMParams(maxQSize,flowPriority,QParam1,QParam2, QParam3, QParam4, QParam5);
      if ((flowID == TRACED_AggSFID1) || (flowID == TRACED_AggSFID2) || (flowID == TRACED_SFID) || (flowID == TRACED_SFID2) ) {
        ((CoDelAQM *)myPacketList)->traceFlag = 1;
       }
      break;

//$A908
    case PIAQM:
      myPacketList = new PIEAQM();
      myPacketList->initList(maxQSize);
      myPacketList->listID = flowID;
      ((PIEAQM *)myPacketList)->setAQMParams(maxQSize,flowPriority,QParam1,QParam2, QParam3, QParam4, QParam5);
      if ((flowID == TRACED_AggSFID1) || (flowID == TRACED_AggSFID2) || (flowID == TRACED_SFID) || (flowID == TRACED_SFID2) ) {
        ((PIEAQM *)myPacketList)->traceFlag = 1;
       }
      break;

    case BAQMQ:
      myPacketList = new BAQM();
      myPacketList->initList(maxQSize);
      myPacketList->listID = flowID;
      ((BAQM *)myPacketList)->setAQMParams(maxQSize,flowPriority, QParam1,QParam2, QParam3, QParam4, QParam5);
      if ((flowID == TRACED_AggSFID1) || (flowID == TRACED_AggSFID2) || (flowID == TRACED_SFID) || (flowID == TRACED_SFID2) ) {
        ((BAQM *)myPacketList)->traceFlag = 1;
      }
      break;


    default:
      myPacketList = new ListObj();
      myPacketList->initList(maxQSize);
      break;
  }

  return;
}

/***********************************************************************
*function: int serviceFlowObject::addPacket(Packet* p)
*
*explanation:  This method adds the pkt to the list of packets
* awaiting transmission 
* If hierarchical scheduling is used, then the flow needs to use
*  the bondingGroupObject's queues rather than the default queue
*  that comes in this SF Object.
*
*inputs:
*  Packet *p : reference to the packet that is ready for transmission
*
*outputs:
*  Returns a SUCCESS or FAILURe
*
************************************************************************/
int serviceFlowObject::addPacket(Packet* p)
{
  int rc = FAILURE;
  double curTime =   Scheduler::instance().clock();
  struct hdr_cmn *ch = HDR_CMN(p);
  serviceFlowObject *mySFObj = NULL;

  if (hierarchicalMode == TRUE ) {
#ifdef TRACEME 
    printf("serviceFlowObject::addPacket(%lf): SF is in hierarchical mode, we are to use bonding Group # :%d \n",
	  Scheduler::instance().clock(),myBGID);
#endif 
    rc = myBGObject->addPacket(p,this);
     //No need to maintain any stats....we will call the real SF object shortly
    if (rc == SUCCESS)
      myPacketCount++;
  }
  else {

   mySFObj = mySFMgr->getServiceFlow(p);
//  packetListElement *myPElement = new packetListElement(p);
  packetListElement *myPElement = new packetListElement(p, mySFObj->flowID);

#ifdef TRACEME 
   printf("serviceFlowObject::addPacket(%lf): flowID:%d Insert packet in SF Object list, just created packetListElement: %d, head:%d, tail %d, pkt sflow:%d  \n",
	  Scheduler::instance().clock(),flowID,myPElement,myPacketList->head,myPacketList->tail,mySFObj->flowID);
#endif 

  if (flowID==-1) {
    printf("serviceFlowObject::addPacket(%lf): HARD ERROR Inserting MGMT  packet  \n", Scheduler::instance().clock());
  }

   rc = myPacketList->addElement(*myPElement);

  if (rc == SUCCESS) {
  
    myPElement->packetEntryTime = curTime;
    myPacketCount++;
    myStats.numberAddPackets++;
#ifdef TRACEME 
  printf("serviceFlowObject::addPacket(%lf):  rc from list addELement: %d, listSize:%d \n",
	  Scheduler::instance().clock(),rc,myPacketList->getListSize());
#endif 
    if(myPacketList->getListSize() != myPacketCount) {
      printf("serviceFlowObject::addPacket(%lf):  HARD ERROR ADDPACKET for flowID %d listSize:%d not same as myPacketCount:%d \n",
	      Scheduler::instance().clock(),flowID,myPacketList->getListSize(),myPacketCount);
    }



    qnp_++;
    qnb_ += hdr_cmn::access(p)->size();
    queue_total_bytes_in += hdr_cmn::access(p)->size();
    if (max_qnp_ < qnp_) {
       max_qnp_ = qnp_;
     }
    if (max_qnb_ < qnb_) {
       max_qnb_ = qnb_;
     }
     if (min_qnp_ > qnp_ ) {
       min_qnp_ = qnp_;
     }
     if (min_qnb_ > qnb_ ) {
       min_qnb_ = qnb_;
     }


    }
    else {
      myStats.TotalPacketsDropped++;
      myStats.TotalBytesDropped += ch->size();
      monitorDropCount++;
      myStats.numberPacketsServiced = myStats.numberPacketArrivals - myStats.TotalPacketsDropped;
      myStats.numberBytesServiced = myStats.numberByteArrivals - myStats.TotalBytesDropped;
#ifdef TRACEME 
      printf("serviceFlowObject::addPacket(%lf):  addPacket failed - listSize:%d, total packet dropped count:%d (bytes:%f), numberPacketsServiced:%d \n",
	      Scheduler::instance().clock(),myPacketList->getListSize(), myStats.TotalPacketsDropped,myStats.TotalBytesDropped,myStats.numberPacketsServiced);
#endif 
//A$806
      delete myPElement;
    }

  }
  return(rc);
}

/***********************************************************************
*function: int serviceFlowObject::nsertPacketInTimeOrder(Packet* p, double packetEntryTime)
*
*explanation:  This method adds the pkt to the list of packets
* awaiting transmission - it places the pkt just after the pkt
* that whose enqueue time is just less than the queue time of this packet.
*
*  This method only applies when an Aggregate is being re sequenced.
*
*inputs:
*  Packet *p : reference to the packet that is ready for transmission
*  double packetEntryTime:  The original packet entry time to the queue system
*  int flowID:  The caler specifies the flow ID that should be in the element container.
*
*outputs:
*  Returns a SUCCESS or FAILURe
*
************************************************************************/
int serviceFlowObject::insertPacketInTimeOrder(Packet* p, double packetEntryTime, int flowIDP)
{
  int rc = FAILURE;
  double curTime =   Scheduler::instance().clock();
  struct hdr_cmn *ch = HDR_CMN(p);
  int listSize = myPacketList->getListSize();
  serviceFlowListElement *tmpPtr = NULL;
  serviceFlowObject *tmpSFObject = NULL;
  Packet *tmpPkt =  NULL;
  packetListElement *myLE = NULL;
  packetListElement *tmpPacketLE = NULL;
  int i;


#ifdef TRACEME 
    printf("serviceFlowObject::insertPacketInTimeOrder(%lf):BGID:%d, current list size:%d (myPacketCount:%d, list max:%d), packetEntryTime:%f \n",
	  Scheduler::instance().clock(),myBGID, listSize,myPacketCount,SchedQSize,packetEntryTime);
#endif 
  if (flowID==-1) {
    printf("serviceFlowObject::insertPacketInTimeOrder(%lf): HARD ERROR Inserting MGMT  packet  \n", Scheduler::instance().clock());
  }
//  packetListElement *myPElement = new packetListElement(p);
//NOTE:  Need to use the flowIDP NOT the flowID of this service flow -
// this is because this service flow might actually be an aggrgate SF
  myLE = new packetListElement(p, flowIDP);
  myLE->packetEntryTime = packetEntryTime;


#ifdef TRACEME 
    printf("serviceFlowObject::insertPacketInTimeOrder(%lf):  Insert packet in SF Object list, just created packetListElement: %d, head:%d, tail %d, pkt sflow:%d  \n",
	  Scheduler::instance().clock(),myLE,myPacketList->head,myPacketList->tail,flowIDP);
#endif 

   //Insert the new myLE into the existing list

   //This might be NULL if the queue is empty
   tmpPacketLE = (packetListElement *)myPacketList->head;
   if (listSize == 0) {
     rc = myPacketList->insertElementInFrontThisElement(*myLE,*tmpPacketLE);
   }
   else {
    //For each packet in the from list 
    for (i=0; i<listSize;i++) {
      if (tmpPacketLE != NULL) {
        tmpPacketLE = (packetListElement *) tmpPacketLE;
//        if (i < 10)
//           printf(" Look at pkt with entry time of %f \n ",tmpPacketLE->packetEntryTime);

        if (tmpPacketLE->packetEntryTime < packetEntryTime) {
#ifdef TRACEME
          printf("serviceFlowObject::insertPacketInTimeOrder(%lf): FOUND LOCATION, new packetEntryTime:%f, previous packetEntryTime:%f  \n",
	        Scheduler::instance().clock(),myLE->packetEntryTime, tmpPacketLE->packetEntryTime);
#endif
          //insert 
          rc = myPacketList->insertElementInFrontThisElement(*myLE,*tmpPacketLE);
          break;
        }
        tmpPacketLE = (packetListElement *)tmpPacketLE->next;
        //if NULL then we reached the end
        if (tmpPacketLE == NULL) {
          rc = myPacketList->insertElementInFrontThisElement(*myLE,*tmpPacketLE);
#ifdef TRACEME
          printf("serviceFlowObject::insertPacketInTimeOrder(%lf): FOUND LOCATION END OF QUEUE, index:%d, myLE packetEntryTime:%f, currentQsize:%d  \n",
	        Scheduler::instance().clock(),i, myLE->packetEntryTime, myPacketList->getListSize());
#endif
          break;
        }
      }
      else
        break;
    }
  }

	if (rc == SUCCESS) {
  
    myPacketCount++;
    myStats.numberAddPackets++;

    if(myPacketList->getListSize() != myPacketCount) {
      printf("serviceFlowObject::insertPacketInTimeOrder(%lf):  HARD ERROR ADDPACKET listSize:%d not same as myPacketCount:%d \n",
	      Scheduler::instance().clock(),myPacketList->getListSize(),myPacketCount);
    }



    qnp_++;
    qnb_ += hdr_cmn::access(p)->size();
    queue_total_bytes_in += hdr_cmn::access(p)->size();
    if (max_qnp_ < qnp_) {
       max_qnp_ = qnp_;
     }
    if (max_qnb_ < qnb_) {
       max_qnb_ = qnb_;
     }
     if (min_qnp_ > qnp_ ) {
       min_qnp_ = qnp_;
     }
     if (min_qnb_ > qnb_ ) {
       min_qnb_ = qnb_;
     }


    }
    else {
      myStats.TotalPacketsDropped++;
      myStats.TotalBytesDropped += ch->size();
      monitorDropCount++;
      myStats.numberPacketsServiced = myStats.numberPacketArrivals - myStats.TotalPacketsDropped;
      myStats.numberBytesServiced = myStats.numberByteArrivals - myStats.TotalBytesDropped;
#ifdef TRACEME 
      printf("serviceFlowObject::insertPacketInTimeOrder(%lf): failed - listSize:%d, total packet dropped count:%d (bytes:%f), numberPacketsServiced:%d \n",
	      Scheduler::instance().clock(),myPacketList->getListSize(), myStats.TotalPacketsDropped,myStats.TotalBytesDropped,myStats.numberPacketsServiced);
#endif 
//A$806
      delete myLE;
    }

  return(rc);
}

/***********************************************************************
*function: Packet *p serviceFlowObject::removePacket()
*
*explanation:  This method removes the pkt to the list of packets
* awaiting transmission 
*
*inputs:
*  Packet *p : reference to the packet that is ready for transmission
*
*outputs:
*  Returns a ptr to a packet, else a NULL
*
************************************************************************/
Packet * serviceFlowObject::removePacket()
{
  Packet *p = NULL;
  packetListElement *myPElement;
  double curTime =   Scheduler::instance().clock();
  double x;
  double interServiceTimeSample= 0;

#ifdef TRACEME 
  printf("serviceFlowObject::removePacket(%lf): ListID:%d,  Remove next packet in SF Object list (size:%d, size:%d)\n",
	  curTime,myPacketList->listID,myPacketCount,myPacketList->curListSize);
    if ((myPacketCount > 0) && (myPacketList->listID == 3)) {
          printf("serviceFlowObject::removePacket(%lf): ListID:%d,  HARD ERROR removePacket Remove next packet in SF Object list (size:%d, size:%d)\n", 
	                            curTime,myPacketList->listID,myPacketCount,myPacketList->curListSize);
          printShortSummary();
    }
#endif 


  interServiceTimeSample = curTime - lastTimeServiced;
  lastTimeServiced = curTime;
  if (curTime > SimStartupTime) {
    if (myPacketCount == 1) {
      if (largestInterServiceTime1 < interServiceTimeSample) 
       largestInterServiceTime1 = interServiceTimeSample;

      interServiceTime1Total += interServiceTimeSample;
      interServiceTime1Count++;
    }
    if (myPacketCount > 1) {
      if (largestInterServiceTime2 < interServiceTimeSample) 
       largestInterServiceTime2 = interServiceTimeSample;

      interServiceTime2Total += interServiceTimeSample;
      interServiceTime2Count++;
    }
  }
  

  myPElement = (packetListElement *) myPacketList->removeElement();

  if (myPElement != NULL)
  {
    myPacketCount--;
    if (myPacketCount < 0)
      myPacketCount  = 0;
 
  
    x = curTime - myPElement->packetEntryTime;
    if (x < 0)
      x = 0;
#ifdef TRACEME 
    if (x > 1.0) {
      printf("serviceFlowObject::removePacket(%lf): ListID:%d, HARD ERROR Packet delay was %f, interserviceSampleTime:%3.6f, curSize:%d \n", 
        curTime, myPacketList->listID , x,interServiceTimeSample,myPacketCount);
    }
#endif 

#ifdef TRACEME 
      printf("serviceFlowObject::removePacket(%lf): Packet delay was %f \n", curTime,x);
#endif 
    totalPacketQueueTime += x;
    packetQueueTimeCount++;
   
    p = myPElement->getPacket();
    qnp_--;
    if (qnp_ < 0)
      qnp_ = 0;
    qnb_ -= hdr_cmn::access(p)->size();
    if (qnb_ < 0)
      qnb_ = 0;

    delete myPElement;
    myStats.numberRemovePackets++;
  }
  return(p);
}


/***********************************************************************
*function: Packet *p serviceFlowObject::getPacket(index i)
*
*explanation:  This method returns a reference to the packet in the i'th index
*  in the queue.
*     Calling getPacket(0) returns the packet (if any) at the head 
*
*inputs:
*  Packet *p : reference to the packet that is ready for transmission
*
*outputs:
*  Returns a ptr to a packet, else a NULL
*
************************************************************************/
Packet * serviceFlowObject::getPacket(int position)
{
  Packet *p = NULL;
  packetListElement *myPElement;
  double curTime =   Scheduler::instance().clock();
  double x;
  double interServiceTimeSample= 0;

#ifdef TRACEME 
  printf("serviceFlowObject::getPacket(%lf): get the packet at the i'th position : %d (packetCount:%d \n",
	  curTime,position,myPacketCount);
#endif 
  if ((position+1) > myPacketCount)
    return p;

  myPElement = (packetListElement *) myPacketList->getElement(position);
  if (myPElement != NULL)
  {
    p = myPElement->getPacket();
  }
  return(p);
}

/***********************************************************************
*function: int serviceFlowObject::packetsQueued()
*
*explanation: This method returns the number of pkts awaiting transmission
* 
*inputs:
*
*outputs:
* Returns the value of variable myPacketCount
*
************************************************************************/
int serviceFlowObject::packetsQueued(void)
{
	return myPacketCount;
}

/***********************************************************************
*function: int serviceFlowObject::match(Packet* p)
*
*explanation:  This method returns a 1 if the packet matches the
*  classification for this service flow
*
* $A800:  modified algorithm:
*    If PKT TYPE is WILD CARD, match everything
*    else just match by finding the first PKT TYPE MATCh
*
*
*inputs:
*  Packet *p : reference to the packet in question
*
*
*outputs:
*  Returns a 1 if the packet matches, else a 0
*
************************************************************************/
int serviceFlowObject::match(Packet* p)
{
int rc = 0;
struct hdr_cmn *ch = HDR_CMN(p);
struct hdr_ip *chip = HDR_IP(p);
//$A801
docsis_dsehdr *chx = docsis_dsehdr::access(p);
struct hdr_mac* mh = HDR_MAC(p);



#ifdef TRACEME 
  printf("serviceFlowObject::match:(%lf): Packet type = %d src-ip = %d dst-ip = %d(PT_UDP:%d, PT_CBR:%d)\n",
	  Scheduler::instance().clock(),ch->ptype(),chip->saddr(),chip->daddr(),PT_UDP,PT_CBR);
  printf("serviceFlowObject::match: Try to match pkt: (src,dst,type):%d,%d,%d) with flow %d,%d,%d (flow dsid:%d, pkt dsid:%d)\n",
            chip->saddr(), chip->daddr(), ch->ptype(), classifier.src_ip,classifier.dst_ip,classifier.pkt_type,dsid, chx->dsid);
#endif     

//$A800
//  if (chx->dsid == dsid) {
#if 0
    if (classifier.pkt_type == FLOW_WILDCARD) {
      rc = 1;
    }
    else if (ch->ptype() == classifier.pkt_type) {
      rc = 1;
    }
//  }
#endif

  if ((ch->ptype() == classifier.pkt_type) && 
        (chip->daddr() == classifier.dst_ip))
  {
    rc= 1;
  }
  else {
    if ((classifier.pkt_type == FLOW_WILDCARD) && 
        (chip->daddr() == classifier.dst_ip))
    {
      rc= 1;
    }
  }

  myStats.numberMatchCalls++;

#ifdef TRACEME 
  if (ch->direction() == UPSTREAM) {
  printf("serviceFlowObject::match(%f): UPSTREAM MATCH RETURN:%d: (src,dst,type):%d,%d,%d) with flow %d,%d,%d (flow dsid:%d, pkt dsid:%d)\n",
	  Scheduler::instance().clock(),rc, chip->saddr(), chip->daddr(), ch->ptype(), classifier.src_ip,classifier.dst_ip,classifier.pkt_type,dsid, chx->dsid);
  } else {
  printf("serviceFlowObject::match(%f): DOWNSTREAM MATCH RETURN:%d: (src,dst,type):%d,%d,%d) with flow %d,%d,%d (flow dsid:%d, pkt dsid:%d)\n",
	  Scheduler::instance().clock(),rc, chip->saddr(), chip->daddr(), ch->ptype(), classifier.src_ip,classifier.dst_ip,classifier.pkt_type,dsid, chx->dsid);
  }
#endif     

  return rc;
}


/***********************************************************************
*function: int serviceFlowObject::match(Packet* p)
*
*explanation:  This method returns a 1 if the packet matches the
*  classification for this service flow
*
*inputs:
*  Packet *p : reference to the packet in question
*
*
*outputs:
*  Returns a 1 if the packet matches, else a 0
*
************************************************************************/
int serviceFlowObject::matchMACAddress(Packet* p, int direction)
{
int rc = 0;
struct hdr_cmn *ch = HDR_CMN(p);
struct hdr_ip *chip = HDR_IP(p);
//$A801
docsis_dsehdr *chx = docsis_dsehdr::access(p);
struct hdr_mac* mh = HDR_MAC(p);


#ifdef TRACEME 
  printf("serviceFlowObject::matchMACAddress:(%lf): DIrection:%d flowID:%d  Packet type = %d src-ip = %d dst-ip = %d(PT_UDP:%d, PT_CBR:%d)\n",
	  Scheduler::instance().clock(),direction, chip->flowid(), ch->ptype(),chip->saddr(),chip->daddr(),PT_UDP,PT_CBR);
#endif 
  
#ifdef TRACEME 
  printf("serviceFlowObject::matchMACAddress: Try to match pkt: (src,dst,type):%d,%d,%d) with flow %d,%d,%d\n",
            chip->saddr(), chip->daddr(), ch->ptype(), classifier.src_ip,classifier.dst_ip,classifier.pkt_type);
#endif     

//$A801
  if (direction == DOWNSTREAM) {
    if (mh->macDA() == macaddr) 
      rc= 1;
  }
  else {
    if (mh->macSA() == macaddr) 
      rc= 1;
  }
#if 0
  if (direction == DOWNSTREAM) {
    if ((ch->ptype() == classifier.pkt_type) && 
        (chip->daddr() == classifier.dst_ip))
    {
      rc= 1;
    }
  }
  else {
//    if (ch->ptype() == classifier.pkt_type)  
    if ((ch->ptype() == classifier.pkt_type) && 
        (chip->saddr() == classifier.src_ip))
    {
      rc= 1;
    }
  }

#endif 

  myStats.numberMatchCalls++;

#ifdef TRACEME 
  if (ch->direction() == UPSTREAM) {
  printf("serviceFlowObject::match(%f): UPSTREAM MATCH RETURN:%d: (src,dst,type):%d,%d,%d) with flow %d,%d,%d (flow dsid:%d, pkt dsid:%d)\n",
	  Scheduler::instance().clock(),rc, chip->saddr(), chip->daddr(), ch->ptype(), classifier.src_ip,classifier.dst_ip,classifier.pkt_type,dsid, chx->dsid);
  } else {
  printf("serviceFlowObject::match(%f): DOWNSTREAM MATCH RETURN:%d: (src,dst,type):%d,%d,%d) with flow %d,%d,%d (flow dsid:%d, pkt dsid:%d)\n",
	  Scheduler::instance().clock(),rc, chip->saddr(), chip->daddr(), ch->ptype(), classifier.src_ip,classifier.dst_ip,classifier.pkt_type,dsid, chx->dsid);
  }
#endif     

  return rc;
}


/***********************************************************************
*function: int serviceFlowObject::matchAddress(Packet* p)
*
*explanation:  This method returns a 1 if the packet matches the
*  classification for this service flow
*
*inputs:
*  Packet *p : reference to the packet in question
*
*
*outputs:
*  Returns a 1 if the packet matches, else a 0
*
************************************************************************/
int serviceFlowObject::matchAddress(Packet* p)
{
  int rc = 0;
  struct hdr_cmn *ch = HDR_CMN(p);
  struct hdr_ip *chip = HDR_IP(p);

#ifdef TRACEME 
  printf("serviceFlowObject::matchAddress:(%lf): Packet type = %d src-ip = %d dst-ip = %d(PT_UDP:%d, PT_CBR:%d)\n",
	  Scheduler::instance().clock(),ch->ptype(),chip->saddr(),chip->daddr(),PT_UDP,PT_CBR);
#endif 
  
#ifdef TRACEME 
  printf("serviceFlowObject::matchAddress: Try to match pkt: (src,dst,type):%d,%d,%d) with flow %d,%d,%d\n",
              chip->saddr(), chip->daddr(), ch->ptype(), classifier.src_ip,classifier.dst_ip,classifier.pkt_type);
#endif     
  if ((chip->daddr() == classifier.dst_ip))
  {
    rc= 1;
  }

  myStats.numberMatchAddressCalls++;
  return rc;
}


/***********************************************************************
*function: int serviceFlowObject::matchAddress(Packet* p)
*
*explanation:  This method returns a 1 if the packet matches the
*  classification for this service flow
*
*inputs:
*  Packet *p : reference to the packet in question
*
*
*outputs:
*  Returns a 1 if the packet matches, else a 0
*
************************************************************************/
int serviceFlowObject::matchAddress(Packet* p, int direction)
{
  int rc = 0;
  struct hdr_cmn *ch = HDR_CMN(p);
  struct hdr_ip *chip = HDR_IP(p);

#ifdef TRACEME 
  printf("serviceFlowObject::matchAddress:(%lf): Direction: %d Packet type = %d src-ip = %d dst-ip = %d(PT_UDP:%d, PT_CBR:%d)\n",
	  Scheduler::instance().clock(),direction, ch->ptype(),chip->saddr(),chip->daddr(),PT_UDP,PT_CBR);
#endif 
  
#ifdef TRACEME 
  printf("serviceFlowObject::matchAddress: Try to match pkt: (src,dst,type):%d,%d,%d) with flow %d,%d,%d\n",
              chip->saddr(), chip->daddr(), ch->ptype(), classifier.src_ip,classifier.dst_ip,classifier.pkt_type);
#endif     
  if (direction == DOWNSTREAM) {
    if ((chip->daddr() == classifier.dst_ip))
    {
      rc= 1;
    }
  }
  else {
    if ((chip->saddr() == classifier.src_ip))
    {
      rc= 1;
    }

  }

  myStats.numberMatchAddressCalls++;
  return rc;
}

//$A801
/***********************************************************************
*function: int serviceFlowObject::matchPktType(Packet* p)
*
*explanation:  This method returns a 1 if the packet matches the
*  classification for this service flow
*
* $A800:  modified algorithm:
*    If PKT TYPE is WILD CARD, match everything
*    else just match by finding the first PKT TYPE MATCh
*
*
*inputs:
*  Packet *p : reference to the packet in question
*
*
*outputs:
*  Returns a 1 if the packet matches, else a 0
*
************************************************************************/
int serviceFlowObject::matchPktType(Packet* p)
{
int rc = 0;
struct hdr_cmn *ch = HDR_CMN(p);
struct hdr_ip *chip = HDR_IP(p);
//$A801
docsis_dsehdr *chx = docsis_dsehdr::access(p);
struct hdr_mac* mh = HDR_MAC(p);



#ifdef TRACEME 
  printf("serviceFlowObject::matchPkt:(%lf): flowID:%d  Packet type = %d src-ip = %d dst-ip = %d(PT_UDP:%d, PT_CBR:%d)\n",
	  Scheduler::instance().clock(),chip->flowid(), ch->ptype(),chip->saddr(),chip->daddr(),PT_UDP,PT_CBR);
  printf("serviceFlowObject::matchPktType: Try to match pkt: (src,dst,type):%d,%d,%d) with flow %d,%d,%d (flow dsid:%d, pkt dsid:%d)\n",
            chip->saddr(), chip->daddr(), ch->ptype(), classifier.src_ip,classifier.dst_ip,classifier.pkt_type,dsid, chx->dsid);
#endif     

    if (classifier.pkt_type == FLOW_WILDCARD) {
      rc = 1;
    }
    else if (ch->ptype() == classifier.pkt_type) {
      rc = 1;
    }

  myStats.numberMatchCalls++;

#ifdef TRACEME 
  if (ch->direction() == UPSTREAM) {
  printf("serviceFlowObject::matchPktType(%f): UPSTREAM MATCH RETURN:%d: (src,dst,type):%d,%d,%d) with flow %d,%d,%d (flow dsid:%d, pkt dsid:%d)\n",
	  Scheduler::instance().clock(),rc, chip->saddr(), chip->daddr(), ch->ptype(), classifier.src_ip,classifier.dst_ip,classifier.pkt_type,dsid, chx->dsid);
  } else {
  printf("serviceFlowObject::matchPktType(%f): DOWNSTREAM MATCH RETURN:%d: (src,dst,type):%d,%d,%d) with flow %d,%d,%d (flow dsid:%d, pkt dsid:%d)\n",
	  Scheduler::instance().clock(),rc, chip->saddr(), chip->daddr(), ch->ptype(), classifier.src_ip,classifier.dst_ip,classifier.pkt_type,dsid, chx->dsid);
  }
#endif     

  return rc;
}


u_int16_t serviceFlowObject::getNextPSN()
{
  return nextPSN++;
}

void serviceFlowObject::setMacHandle(MacDocsis *myMacParam)
{
	myMac = myMacParam;
}

//$A806
void serviceFlowObject::setBondingGroupObject(bondingGroupObject *myBGObjParam)
{
  myBGObject = myBGObjParam;
}

//$A806
void serviceFlowObject::setHierarchicalMode(int modeFlag)
{
  if (modeFlag == TRUE)
    hierarchicalMode = TRUE;
  else
    hierarchicalMode = FALSE;
}

void serviceFlowObject::setNextPSN(u_int16_t nextPSNParam)
{
  nextPSN = nextPSNParam;
}

int serviceFlowObject::getDirection()
{
}


void serviceFlowObject::getStats(struct serviceFlowObjectStatsType *callersStats)
{
	*callersStats = myStats;
}


int serviceFlowObject::getBondingGroup()
{
  return(myBGID);
}


/***********************************************************************
*function: void serviceFlowObject::newArrivalUpdate(Packet *p)
*
*explanation:  This method is invoked when a packet arrives at the 
*  top scheduler.  
*
*inputs:
*  Packet *p : reference to the packet that just arrived
*
*outputs:
*
************************************************************/
void serviceFlowObject::newArrivalUpdate(Packet *p)
{
  double curTime =  Scheduler::instance().clock();
  struct hdr_cmn *ch = HDR_CMN(p);

  if (hierarchicalMode == TRUE ) {
#ifdef TRACEME 
    printf("serviceFlowObject::newArrivalUpdate(%lf): SF is in hierarchical mode, we are to use bonding Group # :%d \n",
	  Scheduler::instance().clock(),myBGID);
#endif 
    myBGObject->newArrivalUpdate(p, this);
  }

  myStats.numberPacketArrivals++;
  myStats.numberByteArrivals += ch->size();
  monitorPacketArrivalCount++;
  lastPacketTime = curTime;
  myStats.numberPacketsServiced = myStats.numberPacketArrivals - myStats.TotalPacketsDropped;
  myStats.numberBytesServiced = myStats.numberByteArrivals - myStats.TotalBytesDropped;

//$A807
  byteArrivals += ch->size();

#ifdef TRACEME 
    printf("serviceFlowObject::newArrivalUpdate(%lf): Incremented SFid:%d  (SchedQType:%d)  numberByteArrivals to %lf\n",
	  Scheduler::instance().clock(),flowID,SchedQType,myStats.numberByteArrivals);
#endif 

//$A602
  if (SchedQType == BAQMQ)
    ((BAQM *)myPacketList)->newArrivalUpdate(p);

  if (SchedQType ==  RedQ) {
//    ((REDQ *)myPacketList)->newArrivalUpdate(p);
    ((BaseRED *)myPacketList)->newArrivalUpdate(p);
  }

#ifdef TRACEME
      if ((flowID == TRACED_AggSFID1) || (flowID == TRACED_AggSFID2) || (flowID == TRACED_SFID) || (flowID == TRACED_SFID2) ) {
      if (direction == UPSTREAM) {
        hdr_tcp *tcph = hdr_tcp::access(p);
        hdr_rtp* rh = hdr_rtp::access(p);
        printf("serviceFlowObject(%lf):newArrivalUpdate(%d): pktSize:%d  pktType:%d  numberPacketsServiced:%d  (numberPacketsServiced2:%d)", 
            Scheduler::instance().clock(),flowID,ch->size(),ch->ptype_,myStats.numberPacketsServiced,myStats.numberPacketsServiced2);
        if(ch->ptype_ == PT_TCP)
          printf(" ....TCP Sequence Num %d Ack No   %d\n",tcph->seqno(), tcph->ackno());
        else if(ch->ptype_ == PT_UDP)
          printf(".....UDP Sequence Num %d \n", rh->seqno());
        else
          printf("\n");
        }
    }
#endif

}

/***********************************************************************
*function: void serviceFlowObject::updateStats(Packet *p, int channelNumber, int bondingGroupNumber)
*
*explanation:  This method is invoked when a packet departs from a queue
*
*inputs:
*  Packet *p : reference to the packet that just arrived
*  int channelNumber:  the channel number that the pkt will use 
*  int bondingGroupNumber : the BG object ID associated with this SF
*
*outputs:
*
************************************************************/
void serviceFlowObject::updateStats(Packet *p, int channelNumber, int bondingGroupNumber)
{
  struct hdr_cmn *ch = HDR_CMN(p);
  struct hdr_ip *chip = HDR_IP(p);
  int x = channelNumber % MAX_CHANNELS;
  int y = bondingGroupNumber % MAX_BONDING_GROUPS;
  double curTime =  Scheduler::instance().clock();

//$A807
  byteDepartures += ch->size();

  if (hierarchicalMode == TRUE ) {
#ifdef TRACEME 
    printf("serviceFlowObject::updateStats(%lf): SF is in hierarchical mode, we are to use bonding Group # :%d \n",
	  Scheduler::instance().clock(),myBGID);
#endif 
    myBGObject->updateStats(p,channelNumber,bondingGroupNumber,this);
  }

  myPacketList->updateStats();


  myStats.numberPacketsServiced2++;

  myStats.channelArray[x]++;
  myStats.bondingGroupArray[y]++;
  myStats.lastUpdateTime = curTime;
//$A900
  if (myStats.firstUpdateTime == 0.0) {
    myStats.firstUpdateTime = curTime;
//$A901
    myStats.lastStateChange = curTime;
  }

  if (curTime > SimStartupTime) {

    //$A510
    //Update stats for avg packet delay monitor
    //$A601
    if (myStats.numberPacketsServiced > 0)
      myStats.avgQueueDelay = totalPacketQueueTime / myStats.numberPacketsServiced;
//    if (packetQueueTimeCount > 0)
//      myStats.avgQueueDelay = totalPacketQueueTime / packetQueueTimeCount;
    else
      myStats.avgQueueDelay=0.0;

    if (myStats.numberPacketArrivals > 0)
      myStats.avgLossRate =  ((double)myStats.TotalPacketsDropped) / (double)myStats.numberPacketArrivals;
    else
      myStats.avgLossRate=0.0;

    serviceRateBurstCounter += ch->size();


    if ( (curTime - lastBurstTimeCalc) > burstTimeScale) {
    
      double tmpBurstRate =  ((double)serviceRateBurstCounter * 8) / (curTime-lastBurstTimeCalc);
      if (tmpBurstRate > largestServiceRateBurst)
         largestServiceRateBurst = tmpBurstRate;

#ifdef TRACEME 
      printf("serviceFlowObject::updateStats(%lf):(lastUpdateTime:%lf) FlowID: %d (numberByteArrivals:%10.0f,#Packets:%d,#Arrivals:%d,avgQD:%3.3f,avgLR:%2.2f,  counter:%d, time since update:%f, computerd burst rate:%f, largest burst rate:%f) \n",
	       Scheduler::instance().clock(),myStats.lastUpdateTime,flowID,
           myStats.numberByteArrivals,myStats.numberPacketArrivals,myStats.numberPacketArrivals,myStats.avgQueueDelay,myStats.avgLossRate,
           serviceRateBurstCounter,(curTime-lastBurstTimeCalc), tmpBurstRate, largestServiceRateBurst); 
#endif

      lastBurstTimeCalc = curTime;
      serviceRateBurstCounter = 0;
      myStats.largestServiceRateBurst = largestServiceRateBurst;
    }
    else {
#ifdef TRACEME 
      printf("serviceFlowObject::updateStats(%lf): FlowID: %d, Update burstCounter to %d \n",
	    Scheduler::instance().clock(),flowID,serviceRateBurstCounter);
#endif
    }

  }

#ifdef TRACEME 
  double z = 0;
  if (packetQueueTimeCount > 0)
    z = totalPacketQueueTime / packetQueueTimeCount;

  printf("serviceFlowObject::updateStats(%lf): FlowID: %d, avgQueueDelay:%3.6f, Packet type = %d src-ip = %d dst-ip = %d, channelNumber:%d, bondingGroupNUmber:%d \n",
	  Scheduler::instance().clock(),flowID,z, ch->ptype(),chip->saddr(),chip->daddr(),channelNumber, bondingGroupNumber);
  printf("                                     (x,y):(%d,%d), channelArray: %d, bondingGroupArray:%d\n",
	  x,y, myStats.channelArray[x], myStats.bondingGroupArray[y]);
#endif 

#ifdef TRACEME
      if ((flowID == TRACED_AggSFID1) || (flowID == TRACED_AggSFID2) || (flowID == TRACED_SFID) || (flowID == TRACED_SFID2) ) {
      if (direction == UPSTREAM) {
        hdr_tcp *tcph = hdr_tcp::access(p);
        hdr_rtp* rh = hdr_rtp::access(p);
        printf("serviceFlowObject(%lf):updateStats(%d): pktSize:%d  pktType:%d  numberPacketsServiced: %d, (numberPacketsServiced2:%d)  channelArray: %d", 
             Scheduler::instance().clock(),flowID,ch->size(),ch->ptype_,myStats.numberPacketsServiced, myStats.numberPacketsServiced2, myStats.channelArray[x]);
        if(ch->ptype_ == PT_TCP)
          printf(" ....TCP Sequence Num %d Ack No   %d\n",tcph->seqno(), tcph->ackno());
        else if(ch->ptype_ == PT_UDP)
          printf(".....UDP Sequence Num %d \n", rh->seqno());
        else
          printf("\n");
      }
    }
#endif

}

void serviceFlowObject::printSFInfo()
{

  printf("serviceFlowObject::printSFInfo(%lf): flowID:%d, Total pkts arrived:%d,  #dropped:%d, direction:%d, type%d, macaddr:%d, src_ip:%d, dst_ip:%d, pkt_type:%d myBGID: %d\n",
		    Scheduler::instance().clock(),flowID,  myStats.numberPacketArrivals,myStats.TotalPacketsDropped, direction,type,macaddr, classifier.src_ip,classifier.dst_ip,classifier.pkt_type,myBGID);
}

void serviceFlowObject::channelStatus(double avgChannelAccesDelay, double avgTotalSlotUtilization, double avgMySlotUtilization)
{

  if (SchedQType ==  RedQ) {
    ((BaseRED *)myPacketList)->channelStatus(avgChannelAccesDelay, avgTotalSlotUtilization,  avgMySlotUtilization);
//    ((REDQ *)myPacketList)->channelStatus(avgChannelAccesDelay, avgTotalSlotUtilization,  avgMySlotUtilization);
  }
  if (SchedQType ==  AdaptiveRedQ) {
    ((BaseRED *)myPacketList)->channelStatus(avgChannelAccesDelay, avgTotalSlotUtilization,  avgMySlotUtilization);
  }
  if (SchedQType ==  BAQMQ) {
    ((BAQM *)myPacketList)->channelStatus(avgChannelAccesDelay, avgTotalSlotUtilization,  avgMySlotUtilization);
  }
}


void serviceFlowObject::channelIdleEvent()
{

  if (SchedQType ==  BlueQ) {
    ((BLUEQ *)myPacketList)->channelIdleEvent();
  }
  if (SchedQType ==  CDAQM) {
    ((CoDelAQM *)myPacketList)->channelIdleEvent();
  }
  if (SchedQType ==  PIAQM) {
    ((PIEAQM *)myPacketList)->channelIdleEvent();
  }
  if (SchedQType ==  BAQMQ) {
    ((BAQM *)myPacketList)->channelIdleEvent();
  }
}


void serviceFlowObject::printShortSummary()
{
  Packet *tmpPkt =  NULL;
  packetListElement *myLE = NULL;
  ListElement *tmpLE = NULL;
  int i;
  double curTime = Scheduler::instance().clock();
  double x;
  struct hdr_cmn *hdr = NULL;

  printf("serviceFlowObject::printShortSummary(%lf): ListID:%d, # elements:%d; ",curTime,myPacketList->listID,myPacketCount);

  if (myPacketCount == 0) 
    return;

  tmpLE = myPacketList->head;


  //For each packet in the queue....
  for (i=0; i<myPacketCount;i++) {
    if (tmpLE != NULL) {
      myLE = (packetListElement *)tmpLE;
      tmpPkt = myLE->getPacket();
      hdr = HDR_CMN(tmpPkt);
      x = curTime - myLE->packetEntryTime;
      printf(" (%d,%f), ",hdr->size(),x);
      tmpLE = tmpLE->next;
     }
  }
  printf(" \n");
}

void serviceFlowObject::printStatsSummary(char *outputFile)
{
  int i;
  double avgQDelay = 0;
  double totalQDelay = 0;
  double lossRate=0;
  double queueRate=0;
  double BWconsumed=0;
  double ArrivalRate=0;
  FILE *fp;
  double avgQueueLevel = myPacketList->getAverageQueueLevel();
  double avgInterServiceTime1 = 0;
  double avgInterServiceTime2 = 0;
  double curTime =   Scheduler::instance().clock();


  if (interServiceTime1Count > 0) {
    avgInterServiceTime1  = interServiceTime1Total / interServiceTime1Count;
  }
  if (interServiceTime2Count > 0) {
    avgInterServiceTime2  = interServiceTime2Total / interServiceTime2Count;
  }

//This is not the normal avgQDelay ..... 
  if (packetQueueTimeCount > 0) {
    avgQDelay = totalPacketQueueTime / packetQueueTimeCount;
  }
//This is the correct q delay  - 
  if (myStats.numberPacketsServiced > 0) {
    totalQDelay = totalPacketQueueTime / myStats.numberPacketsServiced;
  }

  if (myStats.numberPacketArrivals > 0) {
    lossRate = double ((double)myStats.TotalPacketsDropped / (double)myStats.numberPacketArrivals);
  }

  if (myStats.numberPacketsServiced > 0) {
    queueRate = double ((double)myStats.numberAddPackets / (double)myStats.numberPacketsServiced);
//    queueRate = double ((double)myStats.numberAddPackets / (double)myStats.numberPacketArrivals);
  }

  double timeInterval = myStats.lastUpdateTime - myStats.firstUpdateTime;
//$A900
  double TimeS1 =0.0;
  double TimeS2 =0.0;


  if (timeInterval < 4.0) {
    timeInterval = myStats.lastUpdateTime;
    printf("serviceFlowObject::printStatsSummary(%lf): HARD ERROR ? timeInterval too low... #ByteArrivals:%f, stats lastUpdateTime:%f, stats firstUpdateTime:%f, timeInterval:%f\n",
	  curTime,myStats.numberByteArrivals, myStats.lastUpdateTime, myStats.firstUpdateTime, timeInterval);
  }
//To see rates relative to this to end of simulation time
//timeInterval = myStats.lastUpdateTime;

//$A902
  if (serviceFlowType == AGGREGATED_SF) {
    timeInterval = curTime;
  }

  printf("serviceFlowObject::printStatsSummary(%lf):  #ByteArrivals:%f, stats lastUpdateTime:%f, stats firstUpdateTime:%f, timeInterval:%f\n",
	  curTime,myStats.numberByteArrivals, myStats.lastUpdateTime, myStats.firstUpdateTime, timeInterval);

  if (timeInterval> 0) {
    BWconsumed= myStats.numberBytesServiced * 8/ timeInterval;
    ArrivalRate= myStats.numberByteArrivals * 8/ timeInterval;


//$A900
    if (myStats.timeSpentInState1== -1.0) 
    {
      myStats.timeSpentInState1 = timeInterval;
    } else {
      int tmpX = myStats.lastUpdateTime - myStats.lastStateChange;
      if (tmpX < 0 )
         tmpX = 0;
      //Get monitor update for last time period
      if (flowPriority == PRIORITY_HIGH) {
//Why do this??....the flow might not be active.....
//        myStats.timeSpentInState1  +=   (curTime - myStats.lastStateChange);
        myStats.timeSpentInState1  +=   tmpX;
      }
      else {
//        myStats.timeSpentInState2  +=   (curTime - myStats.lastStateChange);
        myStats.timeSpentInState2  +=   tmpX;
      }

      TimeS1 = myStats.timeSpentInState1/timeInterval;
//$A901
      if (TimeS1 > 1.0)
        TimeS1=1.0;
      TimeS2 = myStats.timeSpentInState2/timeInterval;
      if (TimeS2 > 1.0)
        TimeS2=1.0;
    }
  }
  else {
    BWconsumed= 0.0;
    ArrivalRate= 0.0;
  }
  printf("serviceFlowObject::printStatsSummary(%lf): (flowID,BGID):(%d,%d)  TimeS1:%3.3f, TimeS2:%3.3f, instate1:%6.6f, instate2:%6.6f, timeinterval:%6.6f, lastStateChange:%6.6f\n", 
       curTime,flowID,myBGID,TimeS1, TimeS2, myStats.timeSpentInState1, myStats.timeSpentInState2,timeInterval,myStats.lastStateChange);

//$A904
  
  double rateState1to2 = 0.0;
  double rateState2to1 = 0.0;
  double rateAllSwitches = 0.0;

  if (timeInterval > 0) {
//Put into per hour units
    rateState1to2 =  3600 * myStats.countToState1to2/timeInterval;
    rateState2to1 =  3600 *  myStats.countToState2to1/timeInterval;
    rateAllSwitches =  3600  * (myStats.countToState1to2 + myStats.countToState2to1)/timeInterval;
  }

  printf("serviceFlowObject::printStatsSummary(%lf): (flowID,BGID):(%d,%d)  rateS1toS2:%3.6f, rateS2toS1:%3.6f, rateAll:%3.6f,  Count1to2:%6d, Count2to1:%6d, timeinterval:%6.6f\n", 
       curTime,flowID,myBGID,rateState1to2,rateState2to1, rateAllSwitches, myStats.countToState1to2, myStats.countToState2to1,timeInterval);



  printSFInfo();
  printf("serviceFlowObject::printStatsSummary(%lf): ByteArrivals:%f(#numberPacketsServiced::%d), BW:%12.0f, ArrivalRate:%12.0f, lossRate:%3.3f(#dropped:%d),queueRate:%3.3f(#queued:%d), avg Queue Delay:%3.6f,  totalQDelay:%3.6f, avgQLevel:%4.1f\n",
	  curTime,myStats.numberByteArrivals, myStats.numberPacketsServiced, BWconsumed,ArrivalRate,
	  lossRate,myStats.TotalPacketsDropped, queueRate,myStats.numberAddPackets,avgQDelay,totalQDelay,avgQueueLevel);


  printf("      numberPacketArrivals:%d,numberByteArrivals:%f, numberBytesServiced:%f, numberPacketsServiced:%d  totalPacketsDropped:%d\n",
	  myStats.numberPacketArrivals,myStats.numberByteArrivals,myStats.numberBytesServiced,myStats.numberPacketsServiced,myStats.TotalPacketsDropped);
  printf("      numberRemovePackets:%d,numberMatchCalls:%d, numberMatchAddressCalls:%d\n",
	  myStats.numberRemovePackets,myStats.numberMatchCalls,myStats.numberMatchAddressCalls);
  printf("      packetQueueTimeCount:%f,  totalPacketQueueTime:%f, largestServiceRateBurst:%f\n",
             packetQueueTimeCount,totalPacketQueueTime,largestServiceRateBurst);
  printf(" avgInterServiceTime1: %3.6f (count1:%d)  largestInterServiceTime1: %3.6f avgInterServiceTime2: %3.6f (count2:%d)  largestInterServiceTime2:%3.6f \n", 
      avgInterServiceTime1, interServiceTime1Count, largestInterServiceTime1, avgInterServiceTime2, interServiceTime2Count, largestInterServiceTime2);

  myPacketList->printStatsSummary();

  printf("serviceFlowObject::printStatsSummary(%lf): Channel Usage Analysis (numberPacketsServiced:%d, numberPacketsServiced2:%d : ",curTime,myStats.numberPacketsServiced, myStats.numberPacketsServiced2);
  for (i=0;i<MAX_CHANNELS;i++) 
  {
    if (myStats.channelArray[i] >0)
		printf("#%d:%d, ",i,myStats.channelArray[i]);
  }
  printf("   \n");

  printf("serviceFlowObject::printStatsSummary(%lf): Bonding Groups : ",curTime);
  for (i=0;i<MAX_BONDING_GROUPS;i++) 
  {
    if (myStats.bondingGroupArray[i] >0)
		printf("#%d:%d, ",i,myStats.bondingGroupArray[i]);
  }
  printf("   \n");

  fp = fopen(outputFile, "a+");

//  fprintf(fp,"flowID BGID     #Bytes #Packets BW  lossRate  totalQDelay avgQueueLevel <channels 0 - 7 usage>  <bonding group 0 - 7 usage> \n");


  fprintf(fp,"%5d\t%3d\t%3d\t%16.0f\t%12d\t%16.0f\t%16.0f\t%3.3f\t%3.6f\t%4.2f\t%16.0f ",
	  flowID,myBGID, flowPriority, myStats.numberBytesServiced, myStats.numberPacketsServiced, BWconsumed, ArrivalRate, lossRate,totalQDelay,avgQueueLevel,myStats.largestServiceRateBurst);
  fprintf(fp,"\t%3.6f\t%3.6f\t%3.6f\t%3.6f", avgInterServiceTime1, largestInterServiceTime1, avgInterServiceTime2, largestInterServiceTime2);
  if (SchedQType == RedQ) { 
//    REDQ *tmpPQPtr = (REDQ *)myPacketList;
    BaseRED *tmpPQPtr = (BaseRED *)myPacketList;
    fprintf(fp,"\t%3.3f\t%3.3f\t%3.3f\t%3.3f\t0.0", tmpPQPtr->getAvgAvgQ(),tmpPQPtr->getAvgAvgQLatency(),tmpPQPtr->getAvgAvgQLoss(),tmpPQPtr->getAvgDropP());
  }
  else if (SchedQType == AdaptiveRedQ) { 
    BaseRED *tmpPQPtr = (BaseRED *)myPacketList;
    fprintf(fp,"\t%3.3f\t%3.3f\t%3.3f\t%3.3f\t0.0", tmpPQPtr->getAvgAvgQ(),tmpPQPtr->getAvgAvgQLatency(),tmpPQPtr->getAvgAvgQLoss(),tmpPQPtr->getAvgDropP());
  }
  else if (SchedQType == BlueQ) {
    BLUEQ *tmpPQPtr = (BLUEQ *)myPacketList;
    fprintf(fp,"\t%3.3f\t0.0\t0.0\t0.0\t0.0", tmpPQPtr->getAvgAvgQ());
//    fprintf(fp,"\t%3.3f\t%3.3f\t%3.3f\t%3.3f\t0.0", tmpPQPtr->getAvgAvgQ(),tmpPQPtr->getAvgAvgQLatency(),tmpPQPtr->getAvgAvgQLoss(),tmpPQPtr->getAvgDropP());
  }
  else if (SchedQType == CDAQM) {
    CoDelAQM *tmpPQPtr = (CoDelAQM *)myPacketList;
    fprintf(fp,"\t%3.3f\t0.0\t0.0\t0.0\t0.0", tmpPQPtr->getAvgAvgQ());
//    fprintf(fp,"\t%3.3f\t%3.3f\t%3.3f\t%3.3f\t0.0", tmpPQPtr->getAvgAvgQ(),tmpPQPtr->getAvgAvgQLatency(),tmpPQPtr->getAvgAvgQLoss(),tmpPQPtr->getAvgDropP());
  }
  else if (SchedQType == PIAQM) {
    PIEAQM *tmpPQPtr = (PIEAQM *)myPacketList;
    fprintf(fp,"\t%3.3f\t0.0\t0.0\t0.0\t0.0", tmpPQPtr->getAvgAvgQ());
//    fprintf(fp,"\t%3.3f\t%3.3f\t%3.3f\t%3.3f\t0.0", tmpPQPtr->getAvgAvgQ(),tmpPQPtr->getAvgAvgQLatency(),tmpPQPtr->getAvgAvgQLoss(),tmpPQPtr->getAvgDropP());
  }
  else if (SchedQType == BAQMQ) {
    BAQM *tmpPQPtr = (BAQM *)myPacketList;
//    fprintf(fp,"\t%3.3f\t%3.3f\t%3.3f\t%3.3f\t0.0", tmpPQPtr->getAvgAvgQ(),tmpPQPtr->getAvgAvgQLatency(),tmpPQPtr->getAvgAvgQLoss(),tmpPQPtr->getAvgDropP());
    fprintf(fp,"\t%3.3f\t%3.3f\t%3.3f\t%3.3f\t0.0", tmpPQPtr->getAvgAvgQ(),tmpPQPtr->getAvgDropP(),tmpPQPtr->getAvgMaxP(),tmpPQPtr->getAvgQLatency());
  }
  else
    fprintf(fp,"\t0.0\t0.0\t0.0\t0.0\t0.0");

//For the first 10 channels ...
    for (i=0;i<12;i++) 
    {
      if (myStats.numberPacketsServiced2 > 0) 
        fprintf(fp,"\t%1.2f",(double)myStats.channelArray[i]/(double)myStats.numberPacketsServiced2);
      else
        fprintf(fp,"\t0.00");
    }

//This will show the frequency of use of BGs.
//    for (i=0;i<8;i++) 
//    {
//        if (myStats.numberPackets > 0) 
//          fprintf(fp," %0.2f ",(double)myStats.bondingGroupArray[i]/(double)myStats.numberPackets);
//        else
//          fprintf(fp," 0.00");
//    }

//$A904
//TimeS1: fraction of time spent in PBE, TimeS2: fraction spent in BE (i.e., timeout)
//rateState1to2 : rate of switches per second
//  fprintf(fp," %3.3f %3.3f  %3.6f %3.6f %3.6f %6.6f\n",TimeS1,TimeS2,rateState1to2,rateState2to1,rateAllSwitches,timeInterval);
//  fprintf(fp," %3.3f %3.3f  %3.6f %4f %4f %6.6f\n", myStats.timeSpentInState1, myStats.timeSpentInState2,rateState1to2,rateState2to1,rateAllSwitches,timeInterval);
// NEW - this is GONGBINGs
 fprintf(fp," %3.3f %3.3f  %3.6f %4f %4f %6.6f %u %u %u\n", myStats.timeSpentInState1, myStats.timeSpentInState2,rateState1to2,rateState2to1,rateAllSwitches,timeInterval,myStats.numberPriorityChanges, myStats.numberAtHiPriority, myStats.numberAtLoPriority);

  fclose(fp);
}

//$A807
/***********************************************************************
*function: double serviceFlowObject::updateRates()
*
*explanation: This updates and returns the number of bytes Serviced since the last update.
*
*inputs:
*
*outputs:
*  returns the number of bytes serviced since the last call to the method.
*
************************************************************************/
int serviceFlowObject::dumpQueues(char *outputFile)
{
  double curTime =   Scheduler::instance().clock();
  int listSize = myPacketList->getListSize();
  Packet *tmpPkt =  NULL;
  ListElement *tmpLE = NULL;
  packetListElement *myLE = NULL;
  int i;
  FILE *fp = fopen(outputFile, "a+");



//#ifdef TRACEME
  printf("serviceFlowObject::dumpQueues(%lf) dumping list for  flowID:%d  listSize:%d, output file: %s  \n", curTime,flowID,listSize, outputFile);
//#endif

  tmpLE = myPacketList->head;

  fprintf(fp,"serviceFlowObject::dumpQueues(%lf) dumping list for  flowID:%d  listSize:%d  \n", curTime,flowID,listSize);
  //For each packet in the from list 
  for (i=0; i<listSize;i++) {
    if (tmpLE != NULL) {
      myLE = (packetListElement *)tmpLE;
      //tmpPkt = myLE->getPacket();
      fprintf(fp,"%d\t%f\t%d\n",i,myLE->packetEntryTime, myLE->flowID);
      tmpLE = tmpLE->next;
    }
    else
      fprintf(fp,"%d\tNULL\n",i);
  }
  fclose(fp);
}

//$A807
/***********************************************************************
*function: double serviceFlowObject::updateRates()
*
*explanation: This updates and returns the number of bytes Serviced since the last update.
*
*inputs:
*
*outputs:
*  returns the number of bytes serviced since the last call to the method.
*
************************************************************************/
double serviceFlowObject::updateRates()
{
  double curTime =   Scheduler::instance().clock();
  double sampleTime = curTime - lastRateSampleTime;
  double ArrivalRateSample=0;
  double DepartureRateSample=0;
  double rc = byteDepartures;


#ifdef TRACEME
  printf("serviceFlowObject::updateRates:(%lf) flowID:%d  byteArrivals:%f, byteDepartures:%f, time since last rate sample:%f  \n", curTime,flowID, byteArrivals,byteDepartures,lastRateSampleTime);
#endif


  if (sampleTime > 0 )
    ArrivalRateSample = byteArrivals*8/sampleTime;

  avgArrivalRate = (1-rateWeight)*avgArrivalRate + rateWeight*ArrivalRateSample;

  if (sampleTime > 0)
    DepartureRateSample = byteDepartures*8/sampleTime;

  avgServiceRate = (1-rateWeight)*avgServiceRate + rateWeight*DepartureRateSample;

#ifdef TRACEME
    printf("serviceFlowObject::updateRates:(%lf) flowID:%d  avgArrivalRate:%f (sample:%f),  avgServiceRate:%f(sample:%f) \n",
       curTime,flowID,avgArrivalRate,ArrivalRateSample,avgServiceRate,DepartureRateSample);
#endif

  byteArrivals =0;
  byteDepartures =0;
  lastRateSampleTime = curTime;

  return rc;
}

DSserviceFlowObject::DSserviceFlowObject() : serviceFlowObject()
{


#ifdef TRACEME 
  printf("DSserviceFlowObject::constructor: \n");
#endif 
}

DSserviceFlowObject::DSserviceFlowObject(int macaddrParam, int32_t src_ip, int32_t dst_ip, packet_t pkt_type, u_int16_t flowIDParam, int myBGIDParam) : serviceFlowObject(macaddrParam, src_ip, dst_ip, pkt_type, flowIDParam,  myBGIDParam)
{
#ifdef TRACEME 
  printf("DSserviceFlowObject::constructor:, src_ip:%d, dst_ip:%d, pkt_type:%d myBG %d \n",src_ip,dst_ip,pkt_type,myBGIDParam);
#endif 


}

DSserviceFlowObject::~DSserviceFlowObject()
{
#ifdef TRACEME 
  printf("DSserviceFlowObject::destructor: \n");
#endif 
}

void DSserviceFlowObject::init(int direction, MacDocsis *myMacParam, serviceFlowMgr *mySFMgrParam)
{
#ifdef TRACEME 
  printf("DSserviceFlowObject::init:  \n");
#endif 

  serviceFlowObject::init(direction,myMacParam,mySFMgrParam);
  //
  // initialize token bucket params
  tokenq_ = new PacketQueue;
  mhSFToken_ = new CmtsSFTokenDocsisTimer(myMacParam);
  tokenqlen_ = 0;
  tokens_ = 0;
  rate_ = 0;
  bucket_ = 0;
  lastupdatetime_ = 0;
  init_ = 1;    // to require initialization
  ratecontrol = 0;

   //The new service flow must already exist in the CmRecord  AND it must not
   //yet exist in this list
  //Find the cindex/findex using classify
  cindex = myMac->findFlow(macaddr,classifier.src_ip,classifier.dst_ip,classifier.pkt_type,direction,&findex); 
#ifdef TRACEME 
  printf("DSserviceFlowObject:: after findFlow:  cindex:%d,  findex:%d  \n", cindex, findex);
#endif 
}


USserviceFlowObject::USserviceFlowObject()
{
#ifdef TRACEME 
  printf("USserviceFlowObject::constructor: \n");
#endif 
}
USserviceFlowObject::USserviceFlowObject(int macaddrParam, int32_t src_ip, int32_t dst_ip, packet_t pkt_type, u_int16_t flowIDParam, int myBGIDParam) : serviceFlowObject(macaddrParam, src_ip, dst_ip, pkt_type, flowIDParam, myBGIDParam)
{
#ifdef TRACEME 
  printf("USserviceFlowObject::constructor:, src_ip:%d, dst_ip:%d, pkt_type:%d myBG %d \n",src_ip,dst_ip,pkt_type,myBGIDParam);
#endif 
}

USserviceFlowObject::~USserviceFlowObject()
{
#ifdef TRACEME 
  printf("USserviceFlowObject::destructor: \n");
#endif 
}

void USserviceFlowObject::init(int direction, MacDocsis *myMacParam,serviceFlowMgr *mySFMgrParam)
{
#ifdef TRACEME 
  printf("USserviceFlowObject::init:  \n");
#endif 

  serviceFlowObject::init(direction,myMacParam,mySFMgrParam);

   //The new service flow must already exist in the CmRecord  AND it must not
   //yet exist in this list
  //Find the cindex/findex using classify
  cindex = myMac->findFlow(macaddr,classifier.src_ip,classifier.dst_ip,classifier.pkt_type,direction,&findex); 
#ifdef TRACEME 
  printf("USserviceFlowObject:: after findFlow:  cindex:%d,  findex:%d  \n", cindex, findex);
#endif 
}



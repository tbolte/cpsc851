/***************************************************************************
 * Module: docsisMonitor 
 * 
 * Explanation:
 *   This file contains the base class docsisMonitor.
 *   The monitor maintains two types of stats.  
 *       Long Term stats:  the counters are never cleared.
 *       Running stats:  the idea is that an external program can periodically
 *             obtain the current count and then can clear the counters.
 *
 * Methods:
 *
 *
 * Revisions:
 *
 *
************************************************************************/
#include "docsisMonitor.h"

#include <strings.h>
#include <errno.h>

//Comment this  out to remove all printfs that are not covered by the traceLevel
#include "docsisDebug.h"
//#define TRACEME 0

#define TRACE_STRING_LENGTH 255
docsisMonitor::docsisMonitor() : packetMonitor()
{
}

docsisMonitor::docsisMonitor(int direction) : packetMonitor(direction)
{
}

docsisMonitor::~docsisMonitor()
{
}

void docsisMonitor::init()
{
#ifdef TRACEME
  printf("docsisMonitor::init()(%lf): init docsisMonitor \n", Scheduler::instance().clock());
#endif
}

void docsisMonitor::init(int numberID, int direction)
{
#ifdef TRACEME
  printf("docsisMonitor::init()(%lf): init docsisMonitor numberID:%d\n", Scheduler::instance().clock(),numberID);
#endif
 bzero((void *)&myDOCSISStats,(size_t) (sizeof(struct docsisStatsType)));
 packetMonitor::init(numberID,direction);

}


/***********************************************************************
*function: int packetTrace(Packet *p, int numberID,char *traceOutput, int outputSize);
*
*explanation:
*  This creates a text trace line and returns it in a string
*
*inputs:
*   Packet *p
*   int numberID
*   char *traceOutput
*  
*
*outputs:
*  Returns 0 on success, a 1 on error
*
************************************************************************/
int docsisMonitor::packetTrace(Packet *p,char *traceOutput, int outputSize, int channelNumber)
{
  char traceString[TRACE_STRING_LENGTH];
  char commonString[TRACE_STRING_LENGTH];
  char *tptr = traceString;
  int i;

  int rc = 0;
  struct hdr_cmn* ch = HDR_CMN(p);        /* Common header part */
  hdr_tcp *tcph = hdr_tcp::access(p);     /* TCP header */
  hdr_rtp* rh = hdr_rtp::access(p);



  hdr_ping *pingh = hdr_ping::access(p);  /* Ping header */
  hdr_arp *ah = HDR_ARP(p);               /* arp hdr if it exists */
  hdr_loss_mon *lossmonh = hdr_loss_mon::access(p);  /* Ping header */
  hdr_ip *iph = hdr_ip::access(p);        /* IP header */
  struct hdr_docsisextd* eh = HDR_DOCSISEXTD(p);
  int * data = (int *)p->accessdata();

  int framesize = ch->size();
  int frag_index = -1;
  u_int16_t EHDR[12];
  u_int16_t piggyAmt=0;
  u_int16_t FD0=0;
  u_int16_t FD1=0;
  u_int16_t FD2=0;  
  u_int16_t FD3=0;
  u_int16_t FD4=0;
  u_int16_t FD5=0;
  char piggyInfo[16];

  struct hdr_docsis *dhdr = HDR_DOCSIS(p);

  u_int16_t flow_id;

//$A310
  docsis_dsehdr *chx = docsis_dsehdr::access(p);
  char DOCSIS30Info[32];

  sprintf(DOCSIS30Info," psn:%d, flowid:%d" ,chx->packet_sequence_number,chx->dsid);

  int  payloadSize= ch->size();

  sprintf(piggyInfo," PIGREQ: -1 ");


  if (dhdr->dshdr_.fc_type == DATA)
  {
      //first get any piggyback info
    if (eh != NULL) 
	{
		  piggyAmt=0;
          for (int i = 0; i < eh->num_hdr; i++)
          {
            if (eh->exthdr_[i].eh_type == 1)
              piggyAmt+= eh->exthdr_[i].eh_data[0];
          }
          sprintf(piggyInfo," PIGREQ:%d ",piggyAmt);
    }

    rc = packetMonitor::packetTrace(p,commonString,outputSize);
    sprintf(tptr,"%d %s",channelNumber,  commonString);

  }
  else if (dhdr->dshdr_.fc_type == MAC_SPECIFIC)
  {
    sprintf(commonString,"%d, %d, %lf, %d, %d ",
         channelNumber, numberID,Scheduler::instance().clock(), ch->size(),payloadSize);
    if(ch->ptype_ == PT_DOCSIS)
	{
	  //We no longer send this ptype...
	     sprintf(tptr,"%s, PT_DOCSIS, %d\n",
	    commonString, dhdr->dshdr_.fc_type);
	}
    else if(ch->ptype_ == PT_DOCSISFRAG)
    {
	  piggyAmt=0;
      for (int i = 0; i < eh->num_hdr; i++)
      {
        frag_index = -1;
        EHDR[i]= eh->exthdr_[i].eh_type; 
        if (eh->exthdr_[i].eh_type == 3)
          frag_index = i;
        if (eh->exthdr_[i].eh_type == 1)
        {
          piggyAmt+= eh->exthdr_[i].eh_data[0];
          frag_index = i;
        }
	    if (frag_index == -1) {
	        sprintf(tptr,"%s, BAD FRAGMENT1, %d\n",
	            commonString, eh->num_hdr);
	    }
	    else {
          flow_id = eh->exthdr_[frag_index].eh_data[5];
          FD0= eh->exthdr_[frag_index].eh_data[0];
          FD1= eh->exthdr_[frag_index].eh_data[1];
          FD2= eh->exthdr_[frag_index].eh_data[2];
          FD3= eh->exthdr_[frag_index].eh_data[3];
          FD4= eh->exthdr_[frag_index].eh_data[4];
          FD5= eh->exthdr_[frag_index].eh_data[5];

          if (eh->num_hdr > 1) {
            if (i==0) {
	          sprintf(tptr,"%s, FRAGMENT, %d, %d, %d, %d, %d, %d, %d, %d, ",
	          commonString, eh->num_hdr,eh->exthdr_[i].eh_type,FD0,FD1,FD2,FD3,FD4,FD5);
	         }
	         else { 
	          sprintf(tptr," %d, %d, %d, %d, %d, %d, %d",
	             eh->exthdr_[i].eh_type, FD0,FD1,FD2,FD3,FD4,FD5);
	         }
            }
            else {
	           sprintf(tptr,"%s, FRAGMENT, %d, %d, %d, %d, %d, %d, %d, %d\n",
	          commonString,eh->num_hdr,eh->exthdr_[i].eh_type,FD0,FD1,FD2,FD3,FD4,FD5);
            }
          }
       }
       sprintf(piggyInfo," PIGREQ:%d ",piggyAmt);
       if (eh->num_hdr > 1) {
          sprintf(tptr,"\n");
       }
    }
    else if(ch->ptype_ == PT_DOCSISEXT)
	{
	    sprintf(tptr,"%s, PT_DOCSISEXT, %d\n",
		    commonString, dhdr->dshdr_.fc_type);
	}
    else if(ch->ptype_ == PT_DOCSISMGMT)
	{
	    sprintf(tptr,"%s, PT_DOCSISMGMT, %d\n",
		    commonString, dhdr->dshdr_.fc_parm);
	}
    else if(ch->ptype_ == PT_DOCSISSTATUS)
	{
	    sprintf(tptr,"%s, PT_DOCSISSTATUS, %d\n",
		    commonString, dhdr->dshdr_.fc_parm);
	}
    else if(ch->ptype_ == PT_DOCSISCMUPDATE)
	{
	    sprintf(tptr,"%s, PT_DOCSISCMUPDATE, %d\n",
		    commonString, dhdr->dshdr_.fc_parm);
	}

    else if(ch->ptype_ == PT_DOCSISREQ)
	{
	        sprintf(tptr,"%s, CONTENTION REQUEST, %d\n",
		    commonString, dhdr->dshdr_.mac_param);
	  
	}
    else if(ch->ptype_ == PT_DOCSISCONCAT)
	{
	        sprintf(tptr,"%s, CONCAT FRAME, %d\n",
		    commonString, dhdr->dshdr_.mac_param);
	  
	}
    else if(ch->ptype_ == PT_DOCSISMAP)
	{
	    sprintf(tptr,"%s, PT_DOCSISMAP, %d\n",
		    commonString, dhdr->dshdr_.fc_type);
	  
	}      
    else
	{	  
	    sprintf(tptr, "%s, MGMT, ptype_ %d\n", 
		    commonString, ch->ptype_); 
	}
//    strcat(tptr,piggyInfo);
  }
  else
  {
	    sprintf(tptr, "%s, UNKNOWN???,  %d\n", 
		     commonString, ch->ptype_); 
  }

  strcat(tptr,piggyInfo);
//$A310
  strcat(tptr,DOCSIS30Info);


  //Finally, copy our trace buffer string to the caller's buffer
  int bufferSize=strlen(tptr);
  if (bufferSize > outputSize)
    bufferSize = outputSize;

  for (i=0;i<bufferSize;i++)
  {
    traceOutput[i] = tptr[i];
  }
  traceOutput[i] =  0x00;
#ifdef TRACEME
  bufferSize=strlen(traceOutput);
  printf("docsisMonitor:2:packetTrace(%lf):(id:%d):(ptype:%d)string message(size:%d):  %s  \n", Scheduler::instance().clock(),numberID,ch->ptype_, bufferSize,traceOutput);
#endif

  return rc;

}



/***********************************************************************
*function: int docsisMonitor::packetTrace(Packet *p)
*
*explanation:
*  This traces the packet to either standard out, an output file
*  or a binary file depending on the trace level.
*
*inputs:
*
*outputs:
************************************************************************/
int docsisMonitor::packetTrace(Packet *p, int channelNumber)
{

  int rc = 0;
  struct hdr_cmn* ch = HDR_CMN(p);        /* Common header part */
  char traceString[TRACE_STRING_LENGTH];

  FILE *fDS  = NULL;

  if (traceLevel == 0)
    return 0;

  if ((traceLevel & 0x01) != 0)
    fDS = stdout;
  if ( (traceLevel & 0x02) != 0)
    fDS = textFile;

  if ( (traceLevel & 0x04) != 0)
    //make the binary trace file entry
	binaryPacketTrace(p);



  packetTrace(p,traceString,TRACE_STRING_LENGTH,channelNumber);
  if (fDS != NULL) {
#ifdef TRACEME
        printf("docsisMonitor::packetTrace: dump to the fDS %d (this:%d)\n",fDS,this);
#endif


   int rc =  fprintf(fDS, "%s", traceString); 
   fflush(fDS);
#ifdef TRACEME
    printf("docsisMonitor::packetTrace: rc from fprintf: %d, errno:%d\n",rc,errno);
    printf("docsisMonitor::packetTrace: TRACE: %s \n",traceString);
#endif
  }

#ifdef TRACEME
  printf("docsisMonitor:3:packetTrace(%lf): %s\n", Scheduler::instance().clock(),traceString);
  printf("docsisMonitor::packetTrace:  the length of this was %d\n", strlen(traceString));
#endif

  return rc;
}

void docsisMonitor::binaryPacketTrace(Packet *p)
{
struct FREC_T pktRecord;
struct hdr_cmn* ch = HDR_CMN(p);        /* Common header part */
hdr_tcp *tcph = hdr_tcp::access(p);     /* TCP header */
hdr_ip *iph = hdr_ip::access(p);        /* IP header */

  pktRecord.tstamp = Scheduler::instance().clock();
  pktRecord.len = ch->size();
  pktRecord.packet_t = ch->ptype();

  pktRecord.direction = direction;

  pktRecord.saddr = iph->saddr();
  pktRecord.daddr = iph->daddr();
		
  if((ch->ptype_  == PT_ACK) || (ch->ptype_ == PT_TCP) || (ch->ptype_ == PT_FTP) || (ch->ptype_ == PT_HTTP))
  {
    pktRecord.seq = tcph->seqno();
    pktRecord.ack = tcph->ackno();
  }
  else
  {
    pktRecord.seq = 0;
    pktRecord.ack = 0;
  }

  pktRecord.off_win = 0;
  pktRecord.tcp_flags = 0;
  pktRecord.seglen = 0;
  pktRecord.snd_una = 0;
  pktRecord.snd_nxt  = 0;
  pktRecord.rcv_nxt = 0;
  pktRecord.snd_wnd = 0;
  pktRecord.snd_cwnd = 0;
 
  binaryByteCount+= pktRecord.len;
  binaryCount++;
#ifdef TRACEME
  printf("docsisMonitor::binaryPacketTrace(%lf): LOG tstamp:%lf,pkt len:%u, pkt #:%u (count%d,  byteCOunt:%u \n", Scheduler::instance().clock(),pktRecord.tstamp,
     pktRecord.len,pktRecord.direction,binaryCount, binaryByteCount);
#endif

  if (binaryFile != NULL)
    fwrite(&pktRecord,sizeof(struct FREC_T),1,binaryFile);
}

void docsisMonitor::clearStats()
{
 bzero((void *)&myDOCSISStats,(size_t)(sizeof(struct docsisStatsType)));
}

void docsisMonitor::updateStats(Packet *p)
{
  struct hdr_cmn* ch = HDR_CMN(p);        /* Common header part */
  int  framesize = ch->size();

  myDOCSISStats.frameByteCount+= framesize;
  myDOCSISStats.frameCount++;
  myDOCSISStats.runningFrameByteCount+= framesize;
  myDOCSISStats.runningFrameCount++;
}

/***********************************************************************
*function: void docsisMonitor::getStats(struct docsisStatsType *callersStats)
*
*explanation:
*
*inputs:
*
*outputs:
************************************************************************/
void docsisMonitor::getStats(struct docsisStatsType *callersStats)
{
  *callersStats = myDOCSISStats;
}


void docsisMonitor::printStatsSummary()
{

  printf("docsisMonitor:StatsTraceSummary:  byteCount:%f, packetCount:%d\n",
         myDOCSISStats.frameByteCount, myDOCSISStats.frameCount);

}

void docsisMonitor::clearRunningStats()
{
}


docsisCMTSMonitor::docsisCMTSMonitor() : docsisMonitor()
{
}


docsisCMTSMonitor::docsisCMTSMonitor(int direction) : docsisMonitor(direction)
{
}

docsisCMTSMonitor::~docsisCMTSMonitor()
{
}

/***********************************************************************
*function: 
*
*explanation:
*
*inputs:
*
*outputs:
************************************************************************/
void docsisCMTSMonitor::init(int numberID, int direction)
{
#ifdef TRACEME
  printf("docsisCMTSMonitor::init()(%lf): init docsisCMTSMonitor numberID:%d\n", Scheduler::instance().clock(),numberID);
#endif
 bzero((void *)&myDOCSISStats,(size_t) (sizeof(struct docsisStatsType)));
 docsisMonitor::init(numberID,direction);

}


void docsisCMTSMonitor::clearStats()
{
 bzero((void *)&myDOCSISStats,(size_t)(sizeof(struct docsisStatsType)));
}


void docsisCMTSMonitor::clearRunningStats()
{
}

docsisCMMonitor::docsisCMMonitor() : docsisMonitor()
{
}

docsisCMMonitor::docsisCMMonitor(int direction) : docsisMonitor(direction)
{
}

docsisCMMonitor::~docsisCMMonitor()
{
}

/***********************************************************************
*function: 
*
*explanation:
*
*inputs:
*
*outputs:
************************************************************************/
void docsisCMMonitor::init(int numberID, int direction)
{
#ifdef TRACEME
  printf("docsisCMMonitor::init()(%lf): init docsisCMTSMonitor numberID:%d\n", Scheduler::instance().clock(),numberID);
#endif
 bzero((void *)&myDOCSISStats,(size_t)(sizeof(struct docsisStatsType)));
 docsisMonitor::init(numberID,direction);
}

void docsisCMMonitor::clearStats()
{
 bzero((void *)&myDOCSISStats,(size_t)(sizeof(struct docsisStatsType)));
}



void docsisCMMonitor::clearRunningStats()
{
}





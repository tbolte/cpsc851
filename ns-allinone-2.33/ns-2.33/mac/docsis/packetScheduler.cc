/***************************************************************************
 * Module: packetSchedulerObject
 *
 * Explanation:
 *   This file contains the class definition of the packet scheduler. 
 *
 * Revisions:
 *
 *
************************************************************************/


#include "packetScheduler.h"
#include "serviceFlowObject.h"
#include "channelAbstraction.h"

#include "docsisDebug.h"
//#define TRACEME 0
//#define FILES_OK 0



/*************************************************************************
 ************************************************************************/
packetSchedulerObject::packetSchedulerObject()
{
  defaultQuantum = DEFAULT_FLOW_QUANTUM;
  mySFMgr = NULL;
  myBGMgr = NULL;
}

/*************************************************************************
 ************************************************************************/
packetSchedulerObject::~packetSchedulerObject()
{
}



void packetSchedulerObject::init(int serviceDisciplineParam, serviceFlowMgr * paramDSSFMgr, bondingGroupMgr *myBGMgrParam)
{
  serviceDiscipline = serviceDisciplineParam;
  mySFMgr = paramDSSFMgr;
  myBGMgr = myBGMgrParam;
  bzero((void *)&myStats,sizeof(struct packetSchedulerStatsType));
  defaultQuantum = DEFAULT_FLOW_QUANTUM;
//#ifdef TRACEME 
    printf("packetSchedulerObject::init: init: serviceDisciplineParam%d \n", serviceDisciplineParam);
//#endif 

}

int packetSchedulerObject::adaptPacketScheduler(int opCode)
{
  int RC = SUCCESS;

  return RC;
}

Packet *packetSchedulerObject::selectPacket(int channelNumber,struct serviceFlowSet *mySFSet)
{
  Packet *p = NULL;

  return NULL;
}

int packetSchedulerObject::selectChannel(Packet *pkt, serviceFlowObject *mySFObj, struct channelSet *myChannelSet, int *channelSelection)
{
  int rc = FAILURE;

  return rc;
}

/*************************************************************************
 ************************************************************************/
FCFSpacketSchedulerObject::FCFSpacketSchedulerObject()
{
}

/*************************************************************************
 ************************************************************************/
FCFSpacketSchedulerObject::~FCFSpacketSchedulerObject()
{
}



void FCFSpacketSchedulerObject::init(int serviceDisciplineParam, serviceFlowMgr * paramDSSFMgr, bondingGroupMgr *myBGMgrParam)
{

    

#ifdef TRACEME //---------------------------------------------------------
    printf("FCFSpacketSchedulerObject::init\n");
#endif 
    packetSchedulerObject::init(serviceDisciplineParam, paramDSSFMgr, myBGMgrParam);
}




/***********************************************************************
*function: Packet * seletPacket() 
*
*explanation: This method returns the number of pkts awaiting transmission
* 
*inputs:
*
*outputs:
*  Returns a packet to be sent next, else a NULL;
*
*   JJM WRONG : the SF objects in the SFset when they are removed
*     they should not be deleted.
************************************************************************/
Packet *FCFSpacketSchedulerObject::selectPacket(int channelNumber, struct serviceFlowSet *mySFSet)
{
	int mySFcount = mySFSet->numberMembers;
	Packet *pkt = NULL;
	serviceFlowObject *mySFObj = NULL;
	serviceFlowObject *selectedSFObj = NULL;
	serviceFlowListElement *tmpSFListElement = NULL;
	double curTime = Scheduler::instance().clock();
	double waitTime=0;
	double largestWaitTime=0;
	int i;

#ifdef TRACEME //---------------------------------------------------------
    printf("FCFSpacketSchedulerObject::selectPacket(%lf): SF count: %d \n", curTime,mySFcount);
	fflush(stdout);
#endif //---------------------------------------------------------------

    if (mySFcount > 0) {
      for (i=0;i<mySFcount;i++) {
	    //Select the SF with that has been waiting for service the longest
        mySFObj = mySFSet->SFObjPtrs[i];
	    waitTime = curTime - mySFObj->lastDequeueTime;
#ifdef TRACEME //---------------------------------------------------------
        printf("FCFSpacketSchedulerObject::selectPacket(%lf): Look at flow ID : %d (wait time:%f,largest:%f)\n", 
			  curTime,mySFObj->flowID,waitTime,largestWaitTime);
#endif //---------------------------------------------------------------
	    if (waitTime >  largestWaitTime) {
          largestWaitTime = waitTime;
		  selectedSFObj = mySFObj;
		}

	  }
	  if (selectedSFObj != NULL) {
	    pkt = selectedSFObj->removePacket();
	    //lastDequeueTime is the timestamp when the last pkt was dequeued
        selectedSFObj->lastDequeueTime = curTime;
	    //lastPacketTime is the timestamp when the last time a pkt was transmitted
        selectedSFObj->lastPacketTime = curTime;
#ifdef TRACEME
        printf("FCFSpacketSchedulerObject::selectPacket(%lf): selected flow ID : %d (pkt:%x) \n", 
			  curTime,selectedSFObj->flowID,waitTime,pkt);
#endif 
	  }
    }
	return pkt;

}

int FCFSpacketSchedulerObject::selectChannel(Packet *pkt, serviceFlowObject *mySFObj, struct channelSet *myChannelSet, int *channelSelection)
{
  int rc = FAILURE;

  return rc;
}



/*************************************************************************
 ************************************************************************/
RRpacketSchedulerObject::RRpacketSchedulerObject()
{
}

/*************************************************************************
 ************************************************************************/
RRpacketSchedulerObject::~RRpacketSchedulerObject()
{
}



void RRpacketSchedulerObject::init(int serviceDisciplineParam, serviceFlowMgr * paramDSSFMgr, bondingGroupMgr *myBGMgrParam)
{
  int i;

  lastSetIndexSelected = 0;
  lastSFFlowIDSelected = 0;
  for (i=0;i<MAX_CHANNELS;i++) {
    lastSetIndexSelectedArray[i] = 0;
    lastSFFlowIDSelectedArray[i] = 0;
  }

  packetSchedulerObject::init(serviceDisciplineParam, paramDSSFMgr,myBGMgrParam);
}




/***********************************************************************
*function: Packet *RRpacketSchedulerObject::selectPacket(int channelNumber, struct serviceFlowSet *mySFSet)
*
*explanation: This method selects the next packet to be sent.
*    The algorithm is to service the SFs that are in the SFSet
*    in round robin order.  The variable lastSetIndexSelected is used
*    to mark the last SF that was serviced last.
* 
*inputs:
*  int channelNumber:  currently this is not used
*  struct serviceFlowSet *mySFSet:  the callers SET of SFs to chose from.
*      The method assumes that the SFs have a packet
*
*outputs:
*  Returns a packet to be sent next, else a NULL;
*
************************************************************************/
Packet *RRpacketSchedulerObject::selectPacketLocal(int channelNumber, struct serviceFlowSet *mySFSet)
{
  int mySFSetCount = mySFSet->numberMembers;
  int mySFCount = mySFMgr->numberSFs;
  int highestFlowID = mySFMgr->getHighestServiceFlowID();
  Packet *pkt = NULL;
  serviceFlowObject *selectedSFObj = NULL;
  int i;
  double curTime = Scheduler::instance().clock();
#ifdef FILES_OK
  FILE *fp;
  fp = fopen("PS.out", "a+");
#endif



#ifdef TRACEME 
  printf("RRSpacketSchedulerObject::selectPacketlocal(%lf):(channelNumber:%d) SF set count:%d, totalSFs:%d, lastSetIndexSelected%d, lastSFFlowIDSelected:%d \n", 
   curTime,channelNumber,mySFSetCount,mySFCount,lastSetIndexSelected,lastSFFlowIDSelected);
  if (mySFSetCount >0) {
    for (i=0;i<mySFSetCount;i++) {
      printf("   %x ",mySFSet->SFObjPtrs[i]);
    }
    printf("\n");
  }
  fflush(stdout);
#endif 

#ifdef FILES_OK
  if (mySFSetCount >0) {
    fprintf(fp,"%lf %d %d %d %d %d",curTime,channelNumber,lastSetIndexSelected,lastSFFlowIDSelected,mySFSetCount,mySFCount);
    serviceFlowObject *tmpSFObj = NULL;
    for (i=0;i<mySFSetCount;i++) {
      tmpSFObj = mySFSet->SFObjPtrs[i];
      if (tmpSFObj != NULL)
        fprintf(fp," (%d,%d);  ",tmpSFObj->flowID,tmpSFObj->packetsQueued());
      else
        fprintf(fp," (ERROR); ");
    }
  }
#endif

  if (mySFSetCount > 0) {

    //Select the SF in the Set that is >=nextSFToService
    lastSetIndexSelected++;
    if (lastSetIndexSelected >= mySFSetCount)
      lastSetIndexSelected = 0;
    selectedSFObj = mySFSet->SFObjPtrs[lastSetIndexSelected];

//6-20-2009  The problem:  
// Let's say there are flows 4 5 6 7 that are back logged
//Then we see an occasional packet on flow 3
//So we might see in the working set   {3 4 5 6 7}.  We service 3 but then we
// service 5 skipping 4.   Why ?  Because we service service set index 0 and
// next service index 1.  But the flow 3 was index 0.  Once it's gone then 
// flow 4 will be index 0 and hence skipped 
//    Better algo will loop over all DS flows and select the next in sequence 
//    that is in the list
//    If we assume the list can change each iteration, we need to 
//        given lastSFSElected
//        for 0 to numberSFInSet
//            if nextSFInSet->FlowID > lastSFSelected
//              pick it,  set lastSFSelected to this flowID
//
    if (selectedSFObj != NULL) {
      if (mySFSetCount > 1) {
        if (lastSFFlowIDSelected == selectedSFObj->flowID) {
#ifdef TRACEME 
         printf("RRSpacketSchedulerObject::selectPacketlocal(%lf):  NOT RIGHT ? selected this SFObj:%x, flowID:%d its queue size: %d \n", 
          curTime,selectedSFObj,selectedSFObj->flowID,selectedSFObj->packetsQueued());
#endif 
         //Select the SF in the Set that is >=nextSFToService
         lastSetIndexSelected++;
         if (lastSetIndexSelected >= mySFSetCount)
           lastSetIndexSelected = 0;
         selectedSFObj = mySFSet->SFObjPtrs[lastSetIndexSelected];
         if (selectedSFObj == NULL) {
           printf("RRSpacketSchedulerObject::selectPacketlocal(%lf): HARD ERROR 1 NO SF Object selected but mySFSetCount is %d \n", curTime,mySFSetCount);
           exit(1);
         }
        }
      }

#ifdef TRACEME 
       printf("RRSpacketSchedulerObject::selectPacketlocal(%lf):selected this SFObj:%x, flowID:%d its queue size: %d \n", 
          curTime,selectedSFObj,selectedSFObj->flowID,selectedSFObj->packetsQueued());
#endif 
      lastSFFlowIDSelected = selectedSFObj->flowID;
      pkt = selectedSFObj->removePacket();
      //lastDequeueTime is the timestamp when the last pkt was dequeued
      selectedSFObj->lastDequeueTime = curTime;
      //lastPacketTime is the timestamp when the last time a pkt was transmitted
      selectedSFObj->lastPacketTime = curTime;
    }
    else{
      printf("RRSpacketSchedulerObject::selectPacketlocal(%lf): HARD ERROR 2 NO SF Object selected but mySFSetCount is %d \n", curTime,mySFSetCount);
      exit(1);
    }
  }
#ifdef TRACEME 
  else
    printf("RRSpacketSchedulerObject::selectPacketlocal(%lf): NO SF Object selected because mySFSetCount is %d \n", curTime,mySFSetCount);
#endif 

#ifdef FILES_OK
  if (selectedSFObj != NULL) {
    fprintf(fp," (selectedObj: %d,%d);  ",selectedSFObj->flowID,selectedSFObj->packetsQueued());
  }
  else
    fprintf(fp," (selectedObj: NONE);  ");
  fprintf(fp,"\n");

  fclose(fp);
#endif

  return pkt;

}

/***********************************************************************
*function: Packet *RRpacketSchedulerObject::selectPacket(int channelNumber, struct serviceFlowSet *mySFSet)
*
*explanation: This method selects the next packet to be sent.
*    The algorithm is to service the SFs that are in the SFSet
*    in round robin order.  The variable  lastSetIndexSelected is used
*    to mark the last SF that was serviced last.
* 
*    The assumption is that mySFSet will not change frequently -
*    Note that the list holds ALL SFs - even those that are not active.
* 
*inputs:
*  int channelNumber:  currently this is not used
*  struct serviceFlowSet *mySFSet:  the callers SET of SFs to chose from.
*      The method assumes that the SFs have a packet
*
*outputs:
*  Returns a packet to be sent next, else a NULL;
*
************************************************************************/
Packet *RRpacketSchedulerObject::selectPacket(int channelNumber, struct serviceFlowSet *mySFSet)
{
  int mySFSetCount = mySFSet->numberMembers;
  int mySFCount = mySFMgr->numberSFs;
  Packet *pkt = NULL;
  serviceFlowObject *selectedSFObj = NULL;
  int i;
  double curTime = Scheduler::instance().clock();
#ifdef FILES_OK
  FILE *fp;
  fp = fopen("PS.out", "a+");
#endif



#ifdef TRACEME 
  printf("RRSpacketSchedulerObject::selectPacket2(%lf):(channelNumber:%d) SF set count:%d, totalSFs:%d, lastSetIndexSelected%d, lastSFFlowIDSelected:%d \n", 
   curTime,channelNumber,mySFSetCount,mySFCount,lastSetIndexSelectedArray[channelNumber],lastSFFlowIDSelectedArray[channelNumber]);
  if (mySFSetCount >0) {
    for (i=0;i<mySFSetCount;i++) {
      printf("   %x ",mySFSet->SFObjPtrs[i]);
    }
    printf("\n");
  }
  fflush(stdout);
#endif 

#ifdef FILES_OK
  if (mySFSetCount >0) {
    fprintf(fp,"%lf %d %d %d %d %d",curTime,channelNumber,lastSetIndexSelectedArray[channelNumber],lastSFFlowIDSelectedArray[channelNumber],mySFSetCount,mySFCount);
    serviceFlowObject *tmpSFObj = NULL;
    for (i=0;i<mySFSetCount;i++) {
      tmpSFObj = mySFSet->SFObjPtrs[i];
      if (tmpSFObj != NULL)
        fprintf(fp," (%d,%d);  ",tmpSFObj->flowID,tmpSFObj->packetsQueued());
      else
        fprintf(fp," (ERROR); ");
    }
  }
#endif

  if (mySFSetCount > 0) {

    lastSetIndexSelectedArray[channelNumber]++;
    if (lastSetIndexSelectedArray[channelNumber] >= mySFSetCount)
      lastSetIndexSelectedArray[channelNumber] = 0;
//Loop through the set starting with
    for (i=0;i<mySFSetCount;i++) {
      selectedSFObj = mySFSet->SFObjPtrs[lastSetIndexSelectedArray[channelNumber]];
      if (selectedSFObj != NULL) {
         if (selectedSFObj->packetsQueued() > 0) {
           break;
         }
      }
      else {
       printf("RRSpacketSchedulerObject::selectPacket2(%lf): HARD ERROR 0 Bad SFObj Ptr, i:%d, mySFSetCount is %d \n", curTime,i,mySFSetCount);
       exit(1);
      }
      lastSetIndexSelectedArray[channelNumber]++;
      if (lastSetIndexSelectedArray[channelNumber] >= mySFSetCount)
        lastSetIndexSelectedArray[channelNumber] = 0;
    }
    selectedSFObj = mySFSet->SFObjPtrs[lastSetIndexSelectedArray[channelNumber]];

    if (selectedSFObj != NULL) {
#ifdef TRACEME 
       printf("RRSpacketSchedulerObject::selectPacket2(%lf):selected this SFObj:%x, flowID:%d its queue size: %d \n", 
          curTime,selectedSFObj,selectedSFObj->flowID,selectedSFObj->packetsQueued());
#endif 
   
    
//JJM WRONG - what if there was no packet?
      //lastDequeueTime is the timestamp when the last pkt was dequeued
      selectedSFObj->lastDequeueTime = curTime;
      //lastPacketTime is the timestamp when the last time a pkt was transmitted
      selectedSFObj->lastPacketTime = curTime;
    }
    else{
      printf("RRSpacketSchedulerObject::selectPacket2(%lf): HARD ERROR 2 NO SF Object selected but mySFSetCount is %d \n", curTime,mySFSetCount);
      exit(1);
    }
  }
#ifdef TRACEME 
  else
    printf("RRSpacketSchedulerObject::selectPacket2(%lf): NO SF Object selected because mySFSetCount is %d \n", curTime,mySFSetCount);
#endif 

#ifdef FILES_OK
  if (selectedSFObj != NULL) {
    fprintf(fp," (selectedObj: %d,%d);  ",selectedSFObj->flowID,selectedSFObj->packetsQueued());
  }
  else
    fprintf(fp," (selectedObj: NONE);  ");
  fprintf(fp,"\n");

  fclose(fp);
#endif

  return pkt;

}

/***********************************************************************
*function: int RRpacketSchedulerObject::selectChannel(Packet *pkt, serviceFlowObject *mySFObj, struct channelSet *myChannelSet, int *channelSelection)
*
*explanation: This method selects the next packet to be sent.
*    For RR scheduling, we simply select the curent Pkt
*    and tie to the first available channel in the Set.
* 
*inputs:
*
*outputs:
*  Returns a SUCCESS or FAILURE
*
*******************************************************************/
int RRpacketSchedulerObject::selectChannel(Packet *pkt, serviceFlowObject *mySFObj, struct channelSet *myChannelSet, int *channelSelection)
{
  int channelNumber = 0;
  int rc = FAILURE;
  int myChannelSetCount = myChannelSet->numberMembers;
  double curTime = Scheduler::instance().clock();
  int i;


#ifdef TRACEME 
   printf("RRSpacketSchedulerObject::selectChannel(%lf): Need to select channel,  channel set count:%d \n", curTime,myChannelSetCount);
  if (myChannelSetCount >0) {
    for (i=0;i<myChannelSetCount;i++) {
      printf("   %x ",myChannelSet->channels[i]);
    }
    printf("\n");
  }
#endif 

  if (myChannelSetCount >0) {
    for (i=0;i<myChannelSetCount;i++) {
      channelNumber = myChannelSet->channels[i];
      if (mySFMgr->myPhyInterface->getChannelStatus(channelNumber) == CHANNEL_IDLE) {
        rc = SUCCESS;
        break;
#ifdef TRACEME 
   printf("RRSpacketSchedulerObject::selectChannel%lf): First IDLE channel: %d\n", curTime,channelNumber);
#endif 
      }
    }
  }

  if ((channelSelection != NULL) && (rc == SUCCESS))
  {
    *channelSelection = channelNumber;
  }
  else
    *channelSelection = -1;

#ifdef TRACEME 
   if (rc == SUCCESS)
     printf("RRSpacketSchedulerObject::selectChannel(%lf): Return SUCCESS channelSelection:%d \n", curTime,*channelSelection);
   else
     printf("RRSpacketSchedulerObject::selectChannel(%lf): Return FAILURE channelSelection:%d \n", curTime,*channelSelection);
#endif 
  return rc;
}






/*************************************************************************
 ************************************************************************/
DRRpacketSchedulerObject::DRRpacketSchedulerObject()
{
}

/*************************************************************************
 ************************************************************************/
DRRpacketSchedulerObject::~DRRpacketSchedulerObject()
{
}

int DRRpacketSchedulerObject::adaptPacketScheduler(int opCode)
{
  int RC = SUCCESS;
  double curTime = Scheduler::instance().clock();

#ifdef TRACEME 
   printf("DRRSpacketSchedulerObject::adaptPacketSchedulerObjectl%lf): opCode %d\n", curTime,opCode);
#endif 

  return RC;
}


void DRRpacketSchedulerObject::init(int serviceDisciplineParam, serviceFlowMgr * paramDSSFMgr, bondingGroupMgr *myBGMgrParam)
{
  double curTime = Scheduler::instance().clock();
  int i;

  for (i=0;i<MAX_CHANNELS;i++) {
    currentActiveSetIndexArray[i] = 0;
    lastSFFlowIDSelectedArray[i] = 0;
  }




//#ifdef TRACEME //---------------------------------------------------------
    printf("DRRpacketSchedulerObject::init(%lf): set defaultQuantum to %d \n", curTime,defaultQuantum);
//#endif

  packetSchedulerObject::init(serviceDisciplineParam, paramDSSFMgr,myBGMgrParam);
}



/***********************************************************************
*function: Packet *DRRpacketSchedulerObject::selectPacket(int channelNumber, struct serviceFlowSet *mySFSet)
*
*explanation: This method selects the next packet to be sent.
*    The algorithm is to service the SFs that are in the SFSet
*    in round robin order.  
*
*    If there are 0 elements in the list, just exit with a NULL;
*    IF there is 1 element, service the queue.  Reset it's deficit counter.
* 
*inputs:
*  int channelNumber:  currently this is not used
*  struct serviceFlowSet *mySFSet:  the callers SET of SFs to chose from.
*      The method assumes that the SFs have a packet
*
*outputs:
*  Returns a packet to be sent next, else a NULL;
*
*
************************************************************************/
Packet *DRRpacketSchedulerObject::selectPacket(int channelNumber, struct serviceFlowSet *mySFSet)
{
  int mySFSetCount = mySFSet->numberMembers;
  int mySFCount = mySFMgr->numberSFs;
  int numberActiveSFs = 0;
  Packet *pkt = NULL;
  Packet *returnPkt = NULL;
  Packet *tmpPkt = NULL;
  Packet *foundPkt = NULL;
  serviceFlowObject *foundSFObj = NULL;
  serviceFlowObject *selectedSFObj = NULL;
  serviceFlowObject *tmpSFObj = NULL;
  packetListElement *myLE = NULL;
  int i;
  double curTime = Scheduler::instance().clock();
  struct hdr_cmn *hdr = NULL;
  struct hdr_cmn *foundHdr = NULL;
  int selectedPktSize=0;
  int endOfRoundFlag = 0;
  int DCUpdatedFlag  = 0;

#ifdef FILES_OK
  FILE *fp;
  fp = fopen("PS.out", "a+");
#endif


#ifdef TRACEME 
  printf("DRRSpacketSchedulerObject::selectPacket(%lf):(channelNumber:%d) SF set count:%d, totalSFs:%d, currentIndex:%d, lastSFFlowIDSelected:%d \n", 
   curTime,channelNumber,mySFSetCount,mySFCount,currentActiveSetIndexArray[channelNumber],lastSFFlowIDSelectedArray[channelNumber]);
#endif 

  selectedSFObj = NULL;
      //For all members in the set...starting with the member pointered to by
      // the currentActiveSetIndex find a SF 
#ifdef TRACEME 
       printf("DRRSpacketSchedulerObject::selectPacket(%lf): Loop through the SF Set and find one with >1 in queue \n", curTime);
#endif
  for (i=0;i<mySFSetCount;i++) {
        selectedSFObj = mySFSet->SFObjPtrs[currentActiveSetIndexArray[channelNumber]];
        if (selectedSFObj != NULL) {
          if (selectedSFObj->packetsQueued() > 0) {
             numberActiveSFs++;
 
#ifdef TRACEME 
              printf("DRRSpacketSchedulerObject::selectPacket(%lf): Found a SF (SF ID: %d)  with a packet (packetsQueued:%d) currentSetIndeSelected=%d (its DC:%d) \n", 
                 curTime,selectedSFObj->flowID,selectedSFObj->packetsQueued(),currentActiveSetIndexArray[channelNumber],selectedSFObj->deficitCounter);
#endif
            //For  each packet in the queue,
            myLE = (packetListElement *)selectedSFObj->myPacketList->head;
            tmpPkt = myLE->getPacket();
            hdr = HDR_CMN(tmpPkt);
            //Either select the current SF
            if (hdr->size() <= selectedSFObj->deficitCounter) {
              selectedSFObj->deficitCounter-= hdr->size();
              lastSFFlowIDSelectedArray[channelNumber] = selectedSFObj->flowID;
              returnPkt  = selectedSFObj->removePacket();
              hdr = HDR_CMN(returnPkt);
              selectedPktSize = hdr->size();
              //lastDequeueTime is the timestamp when the last pkt was dequeued
              selectedSFObj->lastDequeueTime = curTime;
              //lastPacketTime is the timestamp when the last time a pkt was transmitted
              selectedSFObj->lastPacketTime = curTime;
              //If this is the last pkt in the queue, clear DC
              if (selectedSFObj->packetsQueued()== 0)
                selectedSFObj->deficitCounter = 0;
#ifdef TRACEME 
              printf("DRRSpacketSchedulerObject::selectPacket(%lf): Select CURRENT flowID:%d its queue size: %d, the next pkt size:%d, updated deficit:%d \n", 
               curTime,selectedSFObj->flowID,selectedSFObj->packetsQueued(),hdr->size(),selectedSFObj->deficitCounter);
#endif
              break;
            }
            //Or move to the next SF in the list 
            // but update the foundPkt - this is to point to the packet
            // that is queued by any active SF that has the highest
            //    deficitCount - pkt size
            else { 
              endOfRoundFlag = advanceToNextSF(channelNumber,mySFSet);
              if (endOfRoundFlag == 1) 
                 DCUpdatedFlag  =1;
            }
        }
        else {
          endOfRoundFlag = advanceToNextSF(channelNumber,mySFSet);
          if (endOfRoundFlag == 1) 
             DCUpdatedFlag  =1;
#ifdef TRACEME 
          printf("DRRSpacketSchedulerObject::selectPacket(%lf): Skip SF , updated  currentSetIndeSelected=%d \n", 
                 curTime,currentActiveSetIndexArray[channelNumber]);
#endif
        }
      }
      else {
        printf("DRRSpacketSchedulerObject::selectPacket(%lf): HARD ERROR 0 Bad SFObj Ptr, i:%d, mySFSetCount is %d \n", curTime,i,mySFSetCount);
        exit(1);
      }
  }

  if ((DCUpdatedFlag == 1) && (numberActiveSFs > 0) && (returnPkt == NULL)){

     DCUpdatedFlag  =0;
#ifdef TRACEME 
     printf("DRRSpacketSchedulerObject::selectPacket(%lf): We got to end of a round but did not select a packet AND there are %d active flows, currentIndex:%d  \n", 
                 curTime,numberActiveSFs,currentActiveSetIndexArray[channelNumber]);
#endif
//Take the first SF from the beginning of the list

    selectedSFObj = NULL;
      //For all members in the set...starting with the member pointered to by
      // the currentActiveSetIndex find a SF 
#ifdef TRACEME 
       printf("DRRSpacketSchedulerObject::selectPacket(%lf): Loop through the SF Set and find one with >1 in queue \n", curTime);
#endif
    for (i=0;i<mySFSetCount;i++) {
      selectedSFObj = mySFSet->SFObjPtrs[currentActiveSetIndexArray[channelNumber]];
      if (selectedSFObj != NULL) {
#ifdef TRACEME 
         printf("DRRSpacketSchedulerObject::selectPacket(%lf): Look at flowID:%d, #inqueue:%d,  DC:%d (index:%d)\n",
             curTime,selectedSFObj->flowID,selectedSFObj->packetsQueued(),selectedSFObj->deficitCounter,currentActiveSetIndexArray[channelNumber]);
#endif
        if(selectedSFObj->packetsQueued() > 0) {
          myLE = (packetListElement *)selectedSFObj->myPacketList->head;
          tmpPkt = myLE->getPacket();
          hdr = HDR_CMN(tmpPkt);
          lastSFFlowIDSelectedArray[channelNumber] = selectedSFObj->flowID;
          returnPkt  = selectedSFObj->removePacket();
          hdr = HDR_CMN(returnPkt);
          selectedPktSize = hdr->size();
            //lastDequeueTime is the timestamp when the last pkt was dequeued
          selectedSFObj->lastDequeueTime = curTime;
          //lastPacketTime is the timestamp when the last time a pkt was transmitted
          selectedSFObj->lastPacketTime = curTime;
          //If this is the last pkt in the queue, clear DC
#ifdef TRACEME 
            printf("DRRSpacketSchedulerObject::selectPacket(%lf): Select first SF with DC=0  floID:%d its queue size: %d, the next pkt size:%d, updated deficit:%d \n", 
             curTime,selectedSFObj->flowID,selectedSFObj->packetsQueued(),hdr->size(),selectedSFObj->deficitCounter);
#endif
            break;

        }
        else
          endOfRoundFlag = advanceToNextSF(channelNumber,mySFSet);
      }
    }
#ifdef TRACEME 
     printf("DRRSpacketSchedulerObject::selectPacket(%lf): Return from fixing end of round issue with returnPkt %d  \n", 
                 curTime,returnPkt);
#endif
  }

#ifdef TRACEME
  if (mySFSetCount >0) {
     printf("DRRSpacketSchedulerObject::selectPacket(%lf):: %d %d %d %d %d",curTime,channelNumber,currentActiveSetIndexArray[channelNumber],lastSFFlowIDSelectedArray[channelNumber],mySFSetCount,mySFCount);
      tmpSFObj = NULL;
      for (i=0;i<mySFSetCount;i++) {
        tmpSFObj = mySFSet->SFObjPtrs[i];
        if (tmpSFObj != NULL) {
          if (tmpSFObj->packetsQueued() > 0) {
            printf(" (%d,%d,%d);  ",tmpSFObj->flowID,tmpSFObj->packetsQueued(), tmpSFObj->deficitCounter);
          }
        }
        else
          printf(" (ERROR); ");
      }
      if (pkt != NULL)  {
        printf(" (selectedObj: %d,%d,%d, %d);  ",selectedSFObj->flowID, selectedPktSize, selectedSFObj->packetsQueued(),endOfRoundFlag);
      }
      else
        printf(" (selectedObj: NONE, %d);  ",endOfRoundFlag);
      printf("\n");
  }
#endif

#ifdef FILES_OK
  if (mySFSetCount >0) {
      fprintf(fp,"%lf %d %d %d %d %d",curTime,channelNumber,currentActiveSetIndexArray[channelNumber],lastSFFlowIDSelectedArray[channelNumber],mySFSetCount,mySFCount);
      tmpSFObj = NULL;
      for (i=0;i<mySFSetCount;i++) {
        tmpSFObj = mySFSet->SFObjPtrs[i];
        if (tmpSFObj != NULL) {
          if (tmpSFObj->packetsQueued() > 0) {
            fprintf(fp," (%d,%d,%d);  ",tmpSFObj->flowID,tmpSFObj->packetsQueued(), tmpSFObj->deficitCounter);
          }
        }
        else
          fprintf(fp," (ERROR); ");
      }
      if (returnPkt != NULL)  {
        fprintf(fp," (selectedObj: %d,%d,%d, %d);  ",selectedSFObj->flowID, selectedPktSize, selectedSFObj->packetsQueued(),endOfRoundFlag);
      }
      else
        fprintf(fp," (selectedObj: NONE, %d);  ",endOfRoundFlag);
      fprintf(fp,"\n");

      fclose(fp);
  }
#endif

  return returnPkt;

}



/***********************************************************************
*function: void DRRpacketSchedulerObject::advanceToNextSF()
*
*explanation:
*  Private method to advance to next SF in the flow list
* 
*inputs:
*
*outputs:
*
*  Returns and endOfRoundFlag :  1 if end of a round,
*        else 0
*                                
*
************************************************************************/
int DRRpacketSchedulerObject::advanceToNextSF(int channelNumber,struct serviceFlowSet *mySFSet)
{
  double curTime = Scheduler::instance().clock();
  int endOfRoundFlag = 0;
  int mySFSetCount = mySFSet->numberMembers;
  int i;
  serviceFlowObject *selectedSFObj = NULL;

  currentActiveSetIndexArray[channelNumber]++;
#ifdef TRACEME 
  printf("DRRSpacketSchedulerObject::advanceToNextSF(%lf): Using channelNumber%d, setSize:%d,  updated index is %d \n", 
    curTime,channelNumber,mySFSetCount,currentActiveSetIndexArray[channelNumber]);
#endif 

  if (currentActiveSetIndexArray[channelNumber] >= mySFSetCount) {
    currentActiveSetIndexArray[channelNumber] = 0;

#ifdef TRACEME 
      printf("DRRSpacketSchedulerObject::advanceToNextSF(%lf): ROUND COMPLETE, update DC (currentSetIndeSelected=%d) \n", 
          curTime,currentActiveSetIndexArray[channelNumber]);
#endif

    endOfRoundFlag = 1;
    //If this is a new round, increment all the DC's of active flows in the set
    for (i=0;i<mySFSetCount;i++) {
      selectedSFObj = mySFSet->SFObjPtrs[i];
      if (selectedSFObj->packetsQueued() > 0) {
        selectedSFObj->deficitCounter+= selectedSFObj->flowQuantum;
      }
      else {
        selectedSFObj->deficitCounter = 0;
      }
#ifdef TRACEME 
     printf("    (i:%d, flowID:%d, packetsQueued:%d, updatedDC:%d), ", 
          i,selectedSFObj->flowID,currentActiveSetIndexArray[channelNumber], selectedSFObj->deficitCounter);
#endif
    }
  }

#ifdef TRACEME 
  printf("\n");
#endif

  return endOfRoundFlag;
}



/***********************************************************************
*function: Packet *DRRpacketSchedulerObject::selectPacket(Packet *pkt, serviceFlowObject *mySFObj, struct channelSet *myChannelSet)
*
*explanation: This method selects the next packet to be sent.
*    For RR scheduling, we simply select the curent Pkt
*    and tie to the first available channel in the Set.
* 
*inputs:
*
*outputs:
*  Returns a SUCCESS or FAILURE
*
*******************************************************************/
int DRRpacketSchedulerObject::selectChannel(Packet *pkt, serviceFlowObject *mySFObj, struct channelSet *myChannelSet, int *channelSelection)
{
  int channelNumber = 0;
  int rc = FAILURE;
  int myChannelSetCount = myChannelSet->numberMembers;
  double curTime = Scheduler::instance().clock();
  int i;


#ifdef TRACEME 
   printf("DRRSpacketSchedulerObject::selectChannel(%lf): Need to select channel,  channel set count:%d \n", curTime,myChannelSetCount);
  if (myChannelSetCount >0) {
    for (i=0;i<myChannelSetCount;i++) {
      printf("   %x ",myChannelSet->channels[i]);
    }
    printf("\n");
  }
#endif 

  if (myChannelSetCount >0) {
    for (i=0;i<myChannelSetCount;i++) {
      channelNumber = myChannelSet->channels[i];
      if (mySFMgr->myPhyInterface->getChannelStatus(channelNumber) == CHANNEL_IDLE) {
        rc = SUCCESS;
        break;
#ifdef TRACEME 
   printf("DRRSpacketSchedulerObject::selectChannel(%lf): First IDLE channel: %d\n", curTime,channelNumber);
#endif 
      }
    }
  }

  if ((channelSelection != NULL) && (rc == SUCCESS))
  {
    *channelSelection = channelNumber;
  }
  else
    *channelSelection = -1;

#ifdef TRACEME 
   if (rc == SUCCESS)
     printf("DRRSpacketSchedulerObject::selectChannel(%lf): Return SUCCESS channelSelection:%d \n", curTime,*channelSelection);
   else
     printf("DRRSpacketSchedulerObject::selectChannel(%lf): Return FAILURE channelSelection:%d \n", curTime,*channelSelection);
#endif 
  return rc;
}



/*************************************************************************
 ************************************************************************/
SCFQpacketSchedulerObject::SCFQpacketSchedulerObject()
{
}

/*************************************************************************
 ************************************************************************/
SCFQpacketSchedulerObject::~SCFQpacketSchedulerObject()
{
}


void SCFQpacketSchedulerObject::init(int serviceDisciplineParam, serviceFlowMgr * paramDSSFMgr, bondingGroupMgr *myBGMgrParam)
{
  
  double curTime = Scheduler::instance().clock();
  int i,j;

#ifdef TRACEME //---------------------------------------------------------
    printf("Yippee1 in SCFQ init \n");
#endif
  for (i=0;i<MAX_CHANNELS;i++) 
  {
		
           currentActiveSetIndexArray[i] = 0;
           lastSFFlowIDSelectedArray[i] = 0;
        for(j = 0;j<MAX_ALLOWED_SERVICE_FLOWS; j++)
		{
           serviceTags[i][j] = 100000000;
        }
  }

  virTime = 0;


#ifdef TRACEME //---------------------------------------------------------
    printf("SCFQpacketSchedulerObject::init(%lf): virtual time initialized to: %d \n", curTime,virTime);
#endif

  packetSchedulerObject::init(serviceDisciplineParam, paramDSSFMgr, myBGMgrParam);
 
}

int SCFQpacketSchedulerObject::adaptPacketScheduler(int opCode)
{
  int RC = SUCCESS;
  double curTime = Scheduler::instance().clock();

#ifdef TRACEME 
   printf("SCFQpacketSchedulerObject::adaptPacketSchedulerObjectl%lf): opCode %d\n", curTime,opCode);
#endif 

  return RC;
}

/***********************************************************************
*function: Packet *SCFQpacketSchedulerObject::selectPacket(int channelNumber, struct serviceFlowSet *mySFSet)
*
*explanation: This method selects the next packet to be sent over the channel
*  specified by channelNumber. The routine is passed a set of eligible SFs in mySFSet.
*
* 
*inputs:
*  int channelNumber: specifies the channel
*  struct serviceFlowSet *mySFSet : Set of SF object handles that are associated with a BG
*         that includes the channelNumber
*
*outputs:
*  Returns a SUCCESS or FAILURE
*
*  The PS.out trace file format:
*    fprintf(fp,"%lf %d %d %d %12.6f",curTime,channelNumber,mySFSetCount,mySFCount,virTime);
*        fprintf(fp," (%d,%d,%12.6f);  ",tmpSFObj->flowID,tmpSFObj->packetsQueued(),
*    fprintf(fp," (selectedObj: OK: %d,%d);  ",selectedSFObj->flowID,selectedSFObj->packetsQueued());
*                                       serviceTags[channelNumber][tmpSFObj->flowID]);
*          11 475462.000000 (5,256,475454.000000);   (7,256,475454.000000);   (8,71,475469.000000);   
*                           (9,0,475454.000000);   (10,1,475446.000000);   (12,1,475437.000000);   (13,0,475461.000000);   (selectedObj: OK: 12,0);
*
***********************************************************************/
Packet *SCFQpacketSchedulerObject::selectPacket(int channelNumber, struct serviceFlowSet *mySFSet)
{
  int mySFSetCount = mySFSet->numberMembers;
  int mySFCount = mySFMgr->numberSFs;
  Packet *pkt = NULL;
  serviceFlowObject *selectedSFObj = NULL;
  int i;
  double curTime = Scheduler::instance().clock();
  int  bestSf = -1;
  int j,channelSfSum = 0;
  int bestSfFlowid = 0;
  double bestTag = 100000000;
#ifdef FILES_OK
   FILE *fp;
   fp = fopen("PS.out", "a+");
#endif



#ifdef TRACEME 
 printf("SCFQpacketSchedulerObject::selectPacketlocal(%lf):(channelNumber:%d) SF set count:%d, totalSFs:%d \n", 
   curTime,channelNumber,mySFSetCount,mySFCount);
  if (mySFSetCount >0) {
    for (i=0;i<mySFSetCount;i++) {
      printf("  myflow %x flowdId %d and serviceTags %f \n",mySFSet->SFObjPtrs[i],mySFSet->SFObjPtrs[i]->flowID,serviceTags[channelNumber][mySFSet->SFObjPtrs[i]->flowID]);
    }
 }
#endif

#ifdef FILES_OK
  if (mySFSetCount >0) {
    fprintf(fp,"%lf %d %d %d %12.6f",curTime,channelNumber,mySFSetCount,mySFCount,virTime);
    serviceFlowObject *tmpSFObj = NULL;
    for (i=0;i<mySFSetCount;i++) {
      tmpSFObj = mySFSet->SFObjPtrs[i];
      if (tmpSFObj != NULL)
        fprintf(fp," (%d,%d,%12.6f);  ",tmpSFObj->flowID,tmpSFObj->packetsQueued(),
                                       serviceTags[channelNumber][tmpSFObj->flowID]);
      else
        fprintf(fp," (ERROR); ");
    }
  }
#endif


#ifdef TRACEME 
printf("before msSFSetCount > 0 if condition \n");
#endif  

if (mySFSetCount > 0) 
  {
    	
    serviceFlowObject *tmpSFObj = NULL;
    
#ifdef TRACEME 
    printf("in mySFSetCount > 0 \n");
#endif 

     for (i=0;i<mySFSetCount;i++) 
    {
       tmpSFObj = mySFSet->SFObjPtrs[i];
#ifdef TRACEME 
       printf("in for tmpSFObj selected for flowinno %d flowid %d packetsQueued %d\n",i,tmpSFObj->flowID,tmpSFObj->packetsQueued());
#endif
       if(tmpSFObj->packetsQueued() > 0)
       {
               
#ifdef TRACEME 
            printf("in packetQueued selected for flowinno %d  flowid %d serviceTag %f virtime %f \n",i,tmpSFObj->flowID,serviceTags[channelNumber][tmpSFObj->flowID],virTime);
#endif
            // This is the case when a flow is initialized
			if(serviceTags[channelNumber][tmpSFObj->flowID]== 100000000)
            {
				
#ifdef TRACEME 
                printf(" in service tag = 100000000, before virTime > 0 \n");
#endif
                if(virTime > 0)
 				{
					
#ifdef TRACEME 
                    printf("in virTime > 0 \n");
#endif
                   serviceTags[channelNumber][tmpSFObj->flowID]= 0;
                   updateServiceTag(channelNumber,tmpSFObj->flowID,mySFSetCount,virTime);
				}
				else
				{
                    
#ifdef TRACEME 
                    printf("else virTime > 0 \n");
#endif
					serviceTags[channelNumber][tmpSFObj->flowID]=0;
				}
            }    
            //?? Not sure, it somwhow happened that flow was initialized and it disappreared
            // and reappreared after a long time
            else if(serviceTags[channelNumber][tmpSFObj->flowID]< virTime)
            {
#ifdef TRACEME 
               printf("in disappearing flow \n");
#endif
               updateServiceTag(channelNumber,tmpSFObj->flowID,mySFSetCount,virTime);
            }
            

#ifdef TRACEME 
            printf("before < bestTag \n");
#endif
			if(serviceTags[channelNumber][tmpSFObj->flowID]<=bestTag)
			{
				bestTag = serviceTags[channelNumber][tmpSFObj->flowID];
                bestSf = i;//Remember flowid and i are different
                bestSfFlowid = tmpSFObj->flowID;
            }
        }
     }
  }


#ifdef TRACEME 
 printf("before bestTag if,  bestTag = %f bestSf %f and bestSFFlowid %d virTime %f\n",bestTag,bestSf,bestSfFlowid,virTime);
#endif
 // if bestTag has changed or also can keep if any packet queued flag.
 if(bestTag != 100000000)
 {
	selectedSFObj = mySFSet->SFObjPtrs[bestSf];
 	pkt = selectedSFObj->removePacket();
    virTime = bestTag;
#ifdef TRACEME 
    printf("in bestTag if,  bestTag = %f bestSf %f and bestSFFlowid %d virTime %f\n",bestTag,bestSf,bestSfFlowid,virTime);
#endif
    for(i=0;i<MAX_CHANNELS;i++)
	{
		for(j=0;j<MAX_ALLOWED_SERVICE_FLOWS;j++)
		{
			  if(serviceTags[i][j] != 100000000)
              {
					channelSfSum++;
              } 
        }
     

	if(serviceTags[i][bestSfFlowid] != 100000000)
     updateServiceTag(i,bestSfFlowid,channelSfSum,virTime);
     
      channelSfSum = 0;
    }
  
  }
 
  fflush(stdout);


#ifdef FILES_OK
  if (selectedSFObj != NULL) {
    fprintf(fp," (selectedObj: OK: %d,%d);  ",selectedSFObj->flowID,selectedSFObj->packetsQueued());
  }
  else
    fprintf(fp," (selectedObj: NONE);  ");
  fprintf(fp,"\n");

  fclose(fp);
#endif

 
  return pkt;

}

// pktSize can also be included in the service tag calculation

void SCFQpacketSchedulerObject::updateServiceTag (int channelNumber, int bestSfFlowid, int channelSfSum, double virTime)
{

//	if(serviceTags[channelNumber][bestSfFlowid] != 10000)
//    {
		serviceTags[channelNumber][bestSfFlowid] = channelSfSum +
        (virTime>serviceTags[channelNumber][bestSfFlowid]?virTime:serviceTags[channelNumber][bestSfFlowid]);
//    }

}



int SCFQpacketSchedulerObject::advanceToNextSF(int channelNumber,struct serviceFlowSet *mySFSet)
{

#ifdef TRACEME 
  printf("Yippie3 in advanceToNextSF\n");
#endif
  return 1;
}



/***********************************************************************
*function: int SCFQpacketSchedulerObject::selectChannel(Packet *pkt, serviceFlowObject *mySFObj, struct channelSet *myChannelSet, int *channelSelection)
*
*explanation: This method selects the channel to use for the pkt transmission.
*             The logic is simple: select the first available channel in the channel set
* 
*inputs:
*  Packet *pkt : the pkt that is to be sent
*  serviceFlowObject *mySFObj : the SF associated with the packet
*  struct channelSet *myChannelSet : the set of channels in the BG assigned to the SF
*  int *channelSelection : This routine will fill in this with the selected channel
*
*outputs:
*  Returns a SUCCESS or FAILURE
*
*******************************************************************/
int SCFQpacketSchedulerObject::selectChannel(Packet *pkt, serviceFlowObject *mySFObj, struct channelSet *myChannelSet, int *channelSelection)
{
  int channelNumber = 0;
  int rc = FAILURE;
  int myChannelSetCount = myChannelSet->numberMembers;
  double curTime = Scheduler::instance().clock();
  int i;

#ifdef TRACEME 
   printf("SCFQpacketSchedulerObject::selectChannel(%lf): Need to select channel,  channel set count:%d \n", curTime,myChannelSetCount);
  if (myChannelSetCount >0) {
    for (i=0;i<myChannelSetCount;i++) {
      printf("   %x ",myChannelSet->channels[i]);
    }
    printf("\n");
  }
#endif 

  if (myChannelSetCount >0) {
    for (i=0;i<myChannelSetCount;i++) {
      channelNumber = myChannelSet->channels[i];
     if (mySFMgr->myPhyInterface->getChannelStatus(channelNumber) == CHANNEL_IDLE) {
        rc = SUCCESS;
        break;
#ifdef TRACEME 
 printf("SCFQpacketSchedulerObject::selectChannel(%lf): First IDLE channel: %d\n", curTime,channelNumber);
#endif 
      }
    }
  }

  if ((channelSelection != NULL) && (rc == SUCCESS))
  {
    *channelSelection = channelNumber;
  }
  else
    *channelSelection = -1;



#ifdef TRACEME 
   if (rc == SUCCESS)
     printf("SCFQpacketSchedulerObject::selectChannel(%lf): Return SUCCESS channelSelection:%d \n", curTime,*channelSelection);
   else
     printf("SCFQpacketSchedulerObject::selectChannel(%lf): Return FAILURE channelSelection:%d \n", curTime,*channelSelection);
#endif
 
 return rc;


}




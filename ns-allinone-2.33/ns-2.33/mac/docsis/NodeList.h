#include <stdio.h>
#include "packet.h"
#include "hdr-docsis.h"
#include "mac-docsistimers.h"

#ifndef NODELISTDATA
#define NODELISTDATA
class NodeListData
{
	friend class CmtsTxPktDocsisTimer;
	friend class CmTxPktDocsisTimer;
	friend class ChTxPktDocsisTimer;
	friend class RxPktDocsisTimer;

	public:
	NodeListData();
	virtual ~NodeListData();
	void NodeListDataInit(MacDocsis*);
	void NodeListDataChannelNumber(int,medium*);
	MacDocsis * NodeListDataMac(void);
	void startCmtsTxDocsistimer(Packet *, double);
	void startCmTxDocsistimer(Packet *, double);
	void startRxDocsistimer(Packet *, double);
	void startChTxDocsistimer(Packet *, double);
	NodeListData *next;
	NodeListData *prev;
	MacDocsis *myMac; //Identification modules

	private:
	int nodeNumber; //Identification modules
	ChTxPktDocsisTimer chTxPkt_;
	CmtsTxPktDocsisTimer CmtsTxPkt_;
	CmTxPktDocsisTimer CmTxPkt_;
	RxPktDocsisTimer RxPkt_;
};


class NodeList {

	private:

	protected:

	NodeListData *head;
	NodeListData *tail;
	NodeListData *curElement;
	int curListSize;
	int  MAXLISTSIZE;

	public:

	virtual ~NodeList();
	NodeList();

	void initList(int);
	int addElement(NodeListData& element);
	void setCurHeadElement();
	int getListSize();
	
	NodeListData *accessCurrentElement();
	NodeListData *removeElement();
	NodeListData *nextElement();
};

#endif

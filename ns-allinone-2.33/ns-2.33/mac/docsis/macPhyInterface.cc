/***************************************************************************
 * Module: macPhyInterface
 *
 * Explanation:
 *   This file contains the class definition of the object that handls
 *   all aspects of the underlying phy interface.  This object is passed
 *   PDU's from the LLC of the MAC.  And at its lower layer it interacts
 *   with the medium object to send/recv PHY frames.
 *
 * Revisions:
 *  $A700 : 12/28/2009 : added trace files CMTSSEND.out and CMSEND.out
 *          These capture all frames sent DS by the CMTS and US by all CMs
 *  $A801 : 6/19/2010: separate a CMTS downstream service flow from
 *          the dssid concept.
 *
 *  TODO:
 *     Can remove the SF Mgr pointers - we don't use them
 *
************************************************************************/

#include "globalDefines.h"

#include "macPhyInterface.h"

//$A500
#include "medium.h"
#include "schedulerObject.h"
#include "serviceFlowMgr.h"
#include "serviceFlowObject.h"
#include "reseqMgr.h"
#include "channelMgr.h"


//uncomment for printf's
#include "docsisDebug.h"
//#define TRACEME 0
//#define TRACEMEFILE 0
//#define FILES_OK 0

//Creates a record of arrivals in Phyarrivals.dat... us plotArrivalDistribution.m
#define ARRIVAL_TRACE 0

#define MAX_OUTBOUND_CHANNEL_QUEUE 20
#define MAX_INBOUND_CHANNEL_QUEUE 20

macPhyInterface::macPhyInterface()
{
#ifdef TRACEME
printf("macPhyInterface: constructed \n");
#endif

}

macPhyInterface::~macPhyInterface()
{
#ifdef TRACEME
printf("macPhyInterface: Destructed \n");
#endif

}

void macPhyInterface::getStats(struct macPhyInterfaceStatsType *callersStats)
{
  *callersStats = myStats;

}


/***********************************************************************
*function: 
*
*explanation:
*
*inputs:
*
*outputs:
************************************************************************/
void macPhyInterface::init(int nodeNumberParam, medium *myMediumParam)
{
 
  nodeNumber = nodeNumberParam;
  int i;
  myMedium = myMediumParam;
  numberDSChannels = myMedium->numberDSChannels();
  numberUSChannels = myMedium->numberUSChannels();


  
  bzero((void *)&myStats,sizeof(struct macPhyInterfaceStatsType));

#ifdef TRACEME
  printf("macPhyInterface:init():  inited node %d (#DSChannels:%d,  #USChannels:%d \n",nodeNumber,numberDSChannels,numberUSChannels);
  fflush(stdout);
#endif


  myOutChannelMgr = new outboundChannelMgr();
  myOutChannelMgr->init(numberDSChannels, MAX_OUTBOUND_CHANNEL_QUEUE,myMedium, this);

}

/***********************************************************************
*function: int channelMgr::getState(int channelNumber)
*
*explanation:
*  This method returns the channel state.
*
*inputs:
*
*outputs:
*   Return CHANNEL_IDLE, CHANNEL_RECV, CHANNEL_SEND
************************************************************************/
int macPhyInterface::getChannelStatus(int channelNumber)
{
  int rc = 0;

 rc  = myOutChannelMgr->getState(channelNumber);
 return rc;
}

/***********************************************************************
*function: 
*
*explanation:
*
*inputs:
*
*outputs:
************************************************************************/
void macPhyInterface::getChannelProperties(struct channelPropertiesType  *channelProperties, int channelNumber)
{
  myMedium->getChannelProperties(channelProperties,channelNumber);

}

/***********************************************************************
*function: 
*
*explanation:
*
*inputs:
*
*outputs:
************************************************************************/
void macPhyInterface::getChannelStats(struct channelStatsType  *channelStats, int channelNumber)
{

  myMedium->getChannelStats(channelStats,channelNumber);
}

/***********************************************************************
*function: int  getNumberDSChannels()
*
*explanation:
*
*inputs:
*
*outputs:
************************************************************************/
int  macPhyInterface::getNumberDSChannels()
{

#ifdef TRACEME
  printf("macPhyInterface::getNumberDSChannels(%lf): return %d \n",
   Scheduler::instance().clock(),numberDSChannels);
  fflush(stdout);
#endif
  return numberDSChannels;
}

/***********************************************************************
*function: int  getNumberUSChannels()
*
*explanation:
*
*inputs:
*
*outputs:
************************************************************************/
int  macPhyInterface::getNumberUSChannels()
{
  return numberUSChannels;
}



/***********************************************************************
*function: 
*
*explanation:
*
*inputs:
*
*outputs:
************************************************************************/
int macPhyInterface::SendFrame(Packet *p, int channelNumber,MacDocsis *myMacPtr)
{
int rc = 0;

  myStats.numberSendFrames++;

#ifdef TRACEME
printf("macPhyInterface: SendFrame \n");
#endif

  return rc;
}

/***********************************************************************
*function: int macPhyInterface::SendFrame(Packet *p, schedulerAssignmentType *schedAssignment)
*
*explanation:
*
*inputs:
*
*outputs:
************************************************************************/
int macPhyInterface::SendFrame(Packet *p, schedulerAssignmentType *schedAssignment, serviceFlowObject *mySF,MacDocsis *myMacPtr)
{
int rc = 0;

  myStats.numberSendFrames++;

#ifdef TRACEME
  printf("macPhyInterface: SendFrame \n");
#endif
  return rc;
}

/***********************************************************************
*function: int macPhyInterface::SendFrame(schedulerAssignmentType *schedAssignment)
*
*explanation:
*
*inputs:
*
*outputs:
************************************************************************/
int macPhyInterface::SendFrame(schedulerAssignmentType *schedAssignment, serviceFlowObject *mySF,MacDocsis *myMacPtr)
{
int rc = 0;

  myStats.numberSendFrames++;

#ifdef TRACEME
  printf("macPhyInterface: SendFrame \n");
#endif
  return rc;
}

/***********************************************************************
*function: int macPhyInterface::SendFrame(Packet *p, serviceFlowObject *mySF, int channelNumber)
*
*explanation:
*
*inputs:
*
*outputs:
************************************************************************/
int macPhyInterface::SendFrame(Packet *p, serviceFlowObject *mySF, int channelNumber,MacDocsis *myMacPtr)
{
int rc = 0;

  myStats.numberSendFrames++;

#ifdef TRACEME
printf("macPhyInterface::SendFrame: channel:%d \n",channelNumber);
#endif

}

/***********************************************************************
*function: int macPhyInterface::RecvFrame(Packet *p, int channelNumber)
*
*explanation:
*  This is invoked by the medium when a frame arrives. 
*
*inputs:
*  Packet *p : the frame (still with PHY overhead)
*  int channelNumber
*
*outputs:
* returns SUCCESS or FAILURE
*
************************************************************************/
int macPhyInterface::RecvFrame(Packet *p, int channelNumber)
{
int rc = SUCCESS;

  myStats.numberRecvFrames++;

#ifdef TRACEME
printf("macPhyInterface::recvFrame: channel:%d \n",channelNumber);
#endif

  return rc;
}

/***********************************************************************
*function: int TxCompletionHandler(Packet *p, int channelNumber)
*
*explanation:
*
*inputs:
*
*outputs:
************************************************************************/
int macPhyInterface::TxCompletionHandler(Packet *p, int channelNumber)
{
int rc = 0;

  myStats.numberTxCompletions++;

#ifdef TRACEME
printf("macPhyInterface::TxCompletionHandler: channel:%d \n",channelNumber);
#endif
  return rc;
}

int macPhyInterface::TxCompletionHandler(int channelNumber)
{
int rc = 0;

  myStats.numberTxCompletions++;

#ifdef TRACEME
printf("macPhyInterface::TxCompletionHandler: channel:%d \n",channelNumber);
#endif
  return rc;
}



void macPhyInterface::printStatsSummary()
{
//#ifdef TRACEME
printf("macPhyInterface:printStatsSummary: numberSendFrames:%12.0f, numberRecvFrames:%12.0f, numberTxCompletions:%12.0f \n",
        myStats.numberSendFrames, myStats.numberRecvFrames,myStats.numberTxCompletions);
  myMedium->printStatsSummary();
   
//#endif

  printf("macPhyInterface:printStatsSummary:  DS BINARY Trace \n");
  myDSMonitor->getBinaryTraceSummary();
  printf("macPhyInterface:printStatsSummary:  US BINARY Trace \n");
  myUSMonitor->getBinaryTraceSummary();

}

void macPhyInterface::setMyMac(MacDocsis *myMacParam)
{
  myMac = myMacParam;
}


void macPhyInterface::setMyDSScheduler(schedulerObject *mySchedulerParam)
{ 
  myDSScheduler = mySchedulerParam;
}
void macPhyInterface::setMyUSScheduler(schedulerObject *mySchedulerParam)
{ 
  myUSScheduler = mySchedulerParam;
}

void macPhyInterface::setMyDSSFMgr(serviceFlowMgr *mySFMgrParam)
{
  myDSSFMgr = mySFMgrParam;
}

void macPhyInterface::setMyUSSFMgr(serviceFlowMgr *mySFMgrParam)
{
  myUSSFMgr = mySFMgrParam;
}

void macPhyInterface::linkMediumToDSSFMgr(void)
{
  myMedium->linkDSserviceFlowMgr(myDSSFMgr);
}


/***********************************************************************
*function: int macPhyInterface::getOverHead(Packet *p, int channelNumber)
*
*explanation:
*
*inputs:
*
*outputs:
************************************************************************/
int macPhyInterface::getOverHead(Packet *p, int channelNumber)
{
docsis_dsehdr *chx = docsis_dsehdr::access(p);    
hdr_cmn   *ch  = HDR_CMN(p);
hdr_docsis   *chd = HDR_DOCSIS(p);
docsis_chabstr *ch_abs = docsis_chabstr::access(p);    
int overhead = 0;


  return overhead;
}
/***********************************************************************
*function: int CMTSmacPhyInterface::getOverHead(Packet *p, int channelNumber)
*
*explanation:
*
*inputs:
*
*outputs:
************************************************************************/
int macPhyInterface::getOverHead(int dataSize, int channelNumber)
{
  int overhead;

  overhead = myMedium->getOverHead(dataSize,channelNumber);

  return overhead;
}


CMTSmacPhyInterface::CMTSmacPhyInterface()
{
}


/***********************************************************************
*function: void CMTSmacPhyInterface::init(int nodeNumberParam, int numberDSChannelsParam, int numberUSChannelsParam)
*
*explanation:
*
*inputs:
*
*outputs:
************************************************************************/
void CMTSmacPhyInterface::init(int nodeNumberParam, medium *myMediumParam)
{
 


  macPhyInterface::init(nodeNumberParam, myMediumParam);

#ifdef TRACEME
  printf("CMTSmacPhyInterface:init():  inited node %d (#DSChannels:%d,  #USChannels:%d \n",nodeNumberParam,numberDSChannels,numberUSChannels);
  fflush(stdout);
#endif


  myDSMonitor = new docsisMonitor();
  myUSMonitor = new docsisMonitor();

  myDSMonitor->init(nodeNumber,PT_SEND_DIRECTION);
  myUSMonitor->init(nodeNumber,PT_RECV_DIRECTION);

#ifdef FILES_OK
  //$A401
//    myDSMonitor->setTraceLevel( 0x00 | (packetMonitor_TRACE_TO_FILE));
//    myUSMonitor->setTraceLevel( 0x00 | (packetMonitor_TRACE_TO_FILE));
    myDSMonitor->setTraceLevel( 0x00 | (packetMonitor_TRACE_TO_FILE) | (packetMonitor_TRACE_TO_BINARY_FILE));
    myUSMonitor->setTraceLevel( 0x00 | (packetMonitor_TRACE_TO_FILE) | (packetMonitor_TRACE_TO_BINARY_FILE));


  myDSMonitor->setTextTraceFile("CMTSDSMAC.txt");
  myUSMonitor->setTextTraceFile("CMTSUSMAC.txt");

  myDSMonitor->setBinaryTraceFile("CMTSDSMAC.bin");
  myUSMonitor->setBinaryTraceFile("CMTSUSMAC.bin");
#endif


  myReseqMgr = new reseqMgr();
  myReseqMgr->setMacHandle(myMac);
  myReseqMgr->init(UPSTREAM,this);

}

/***********************************************************************
*function: int CMTSmacPhyInterface::SendFrame(Packet *p, schedAssignmentType *schedAssignment)
*
*explanation:
*  This method sends the packet over the channel(s). The schedAssignment might require
*  multiple channels to be used.
*
*  Overview 
*    Step 1: break into MPEG frames, add overhead and PSN
*    Step 2: update stats
*    Step 3: initiate transmission over all channels as required
*
*inputs:
*  Packet *p
*  schedAssignmentType *schedAssignment)
*
*outputs:
*  returns a 0 for success else a 1
*
************************************************************************/
int CMTSmacPhyInterface::SendFrame(schedulerAssignmentType *schedAssignment, serviceFlowObject *mySF, MacDocsis *myMacPtr)
{
  int rc = 0;
  int ExtraFrameOH = 0;
  int	payload_size = 179; // 188 MPEG2 - 4 header - 5 byte header for sequencing
  int channelNumber;
  u_int16_t  nextPSN;
  struct channelPropertiesType channelProperties;
  Packet *p = schedAssignment->myPkt;

  //begin old send frame
  docsis_dsehdr *chx = docsis_dsehdr::access(p);    
  hdr_cmn   *ch  = HDR_CMN(p);
  hdr_docsis   *chd = HDR_DOCSIS(p);
//$A801
  struct hdr_ip *chip = HDR_IP(p);

//$A700
  double curTime =   Scheduler::instance().clock();
  FILE* fp = NULL;
  

  myStats.numberSendFrames++;



#ifdef TRACEME
  printf("CMTSmacPhyInterface: SendFrame, starting packet size: %d, number assigned channels:%d \n", 
       ch->size(), schedAssignment->numberAssignments);

#endif

  //STEP 1 break into MPEG frames 
  //     Currrently send on an IP packet basis but add MPEG overhead
  int channelQLength = 0;
  int tmp = ch->size(); 
  int rem_overhead = 0;
  int psize;
  psize = tmp / payload_size;
  if ((tmp % payload_size) != 0)
    psize++;

//The scheduler assignment provides high level guidelines of max burst size 
//and if a packet is to be striped over multiple frames
//Currently we do not support striping ....
//  for (i=0; i<schedAssignment->numberAssignments;i++)
//  {

    channelNumber = schedAssignment->channelAssignments[0];
    channelQLength =myOutChannelMgr->myChannels->myPacketList->getListSize();

#ifdef TRACEME
  printf("CMTSmacPhyInterface: SendFrame, channel: %d, channel Q Length:%d \n",
      channelNumber,channelQLength);
#endif

  //TODO: we currently don't support queuing in the output channel mgt
 
    myMedium->getChannelProperties(&channelProperties, channelNumber);

	rem_overhead = payload_size*psize - ch->size();
	if (rem_overhead < 0)
		rem_overhead = 0;
	ExtraFrameOH = ((psize) * 4)+rem_overhead; 



    ch->size() += ExtraFrameOH;
    chd->dt_conv_overhead() = ExtraFrameOH;

    //JJM WRONG
//    ch->size() += channelProperties.overheadBytes;

      nextPSN = mySF->getNextPSN();
//$A801
//      chx->dsid = mySF->flowID;
      chx->dsid = schedAssignment->dsidAssignment;
      chx->packet_sequence_number = nextPSN;

	  

      chx->sequence_change_count = 0;		// assume no wrap
      if (chx->packet_sequence_number == 65535)
      {
        chx->sequence_change_count = 1;		// assume no wrap
        chx->packet_sequence_number = 0;
      }

//$A801
//      printf("CMTSmacPhyInterface::SendFrame(%lf): MATCH RETURN:  channel:%d, channelQLen:%d, PSN:%d (nextPSN:%d), dsid:%d (mySF:%d), scc:%d \n",
//	       Scheduler::instance().clock(),channelNumber,channelQLength, 
//           chx->packet_sequence_number,nextPSN,(int) chx->dsid,mySF->flowID,chx->sequence_change_count);
#ifdef TRACE_CMTS //-------------------------------------------------------------------------
      printf("CMTSmacPhyInterface::SendFrame(%lf): channel:%d, channelQLen:%d, PSN:%d (nextPSN:%d), dsid:%d (mySF:%d), scc:%d \n",
	       Scheduler::instance().clock(),channelNumber,channelQLength, 
           chx->packet_sequence_number,nextPSN,chx->dsid,mySF->flowID,chx->sequence_change_count);
      printf("macPhyInterface::SendFrame(%lf): MPEGs:%d,ExtraFrameOH:%d, final frame size:%d, dt_conv_overhead:%d \n",
	       Scheduler::instance().clock(), psize,ExtraFrameOH, ch->size(), chd->dt_conv_overhead());
#endif //------------------------------------------------------------------------------------

    myDSMonitor->packetTrace(p,channelNumber);
    myDSMonitor->updateStats(p);



//$A700
#ifdef FILES_OK
    fp = fopen("CMTSSENDTRACE.out", "a+");
    fprintf(fp,"%f\t%3d\t%3d\t%6d\t%6d\t%4d\t%3d\t%3d\t%3d\t%3d\n", curTime,myMacPtr->cm_id, channelNumber, chx->packet_sequence_number, chx->dsid, ch->size(), chd->dshdr_.fc_type,ch->ptype_,chip->saddr(),chip->daddr());

    fclose(fp);
#endif


    myOutChannelMgr->transmitFrame(p, channelNumber, myMacPtr);
  
  return rc;
}

/***********************************************************************
*function: 
*
*explanation:
*
*inputs:
*
*outputs:
************************************************************************/
int CMTSmacPhyInterface::RecvFrame(Packet *p, int channelNumber)
{
int rc = SUCCESS;
int dst,src;
struct hdr_mac* mh = HDR_MAC(p);
//struct hdr_docsis* dh = HDR_DOCSIS(p);
//struct hdr_cmn* ch = HDR_CMN(p);


  myStats.numberRecvFrames++;



  dst = mh->macDA(); 
  src = mh->macSA(); 

  /* Not a packet destinated to me. */
  if ((dst != MAC_BROADCAST) && (dst != (u_int32_t)myMac->addr()))
  {
      myMac->drop(p);
      return rc;
  }

#ifdef TRACEME
  printf("CMTSmacPhyInterface::RecvFrame(%lf): channel :%d \n", Scheduler::instance().clock(),channelNumber);
#endif

  myUSMonitor->packetTrace(p,channelNumber);
  myUSMonitor->updateStats(p);


#ifdef FILES_OK
#ifdef ARRIVAL_TRACE
  FILE* fp = NULL;
  struct hdr_cmn* ch = HDR_CMN(p);
  fp = fopen("Phyarrivals.out", "a+");
  fprintf(fp,"%lf %d %d \n",
          Scheduler::instance().clock(), ch->size(), channelNumber);
  fclose(fp);
#endif
#endif


//$A310  the CMTS does not resequence
    myMac->RecvFrame(p,0);

  return rc;

}

/***********************************************************************
*function: int TxCompletionHandler(Packet *p, int channelNumber)
*
*explanation:
*
*  This is invoked once a channel becomes idle.  
*  Algorithm:
*   free the pkt
*   invoke the MAC's scheduler object
*
*inputs:
*
*outputs:
************************************************************************/
int CMTSmacPhyInterface::TxCompletionHandler(Packet *p, int channelNumber)
{
int rc = 0;



#ifdef FILES_OK
#ifdef TRACEMEFILE
  FILE *fp;
  fp = fopen("CS.out", "a+");
  fprintf(fp, "%lf %d CHANNEL_IDLE %d \n",
      Scheduler::instance().clock(),channelNumber,myOutChannelMgr->getState(channelNumber));
  fclose(fp);
#endif
#endif

  myStats.numberTxCompletions++;
  myMac->CmtsSendHandler(channelNumber,p);



  return rc;
}

/***********************************************************************
*function: int TxCompletionHandler(Packet *p, int channelNumber)
*
*explanation:
*
*  This is invoked once a channel becomes idle.  
*  Algorithm:
*   free the pkt
*   invoke the MAC's scheduler object
*
*inputs:
*
*outputs:
************************************************************************/
int CMTSmacPhyInterface::TxCompletionHandler(int channelNumber)
{
int rc = 0;

  myStats.numberTxCompletions++;

  return rc;
}


/***********************************************************************
*function: int CMTSmacPhyInterface::getOverHead(Packet *p, int channelNumber)
*
*explanation:
*
*inputs:
*
*outputs:
************************************************************************/
int CMTSmacPhyInterface::getOverHead(Packet *p, int channelNumber)
{
  docsis_dsehdr *chx = docsis_dsehdr::access(p);    
  hdr_cmn   *ch  = HDR_CMN(p);
  hdr_docsis   *chd = HDR_DOCSIS(p);
  docsis_chabstr *ch_abs = docsis_chabstr::access(p);    
  int overhead;

  int ExtraFrameOH = 0;
  int	payload_size = 179; // 188 MPEG2 - 4 header - 5 byte header for sequencing
  //STEP 1 break into MPEG frames 
  //     Currrently send on an IP packet basis but add MPEG overhead
  int channelQLength = 0;
  int tmp = ch->size(); 
  int rem_overhead = 0;
  int psize;
  psize = tmp / payload_size;
  if ((tmp % payload_size) != 0)
    psize++;

  rem_overhead = payload_size*psize - ch->size();
  if (rem_overhead < 0)
    rem_overhead = 0;
  ExtraFrameOH = ((psize) * 4)+rem_overhead; 


  overhead = ExtraFrameOH + myMedium->getOverHead(p,channelNumber);

#ifdef TRACEME
  printf("CMTSmacPhyInterface::getOverHead(%lf): channel :%d, psie:%d, rem_overhead, ExtraFrameOH:%d, overhead:%d \n", 
		  Scheduler::instance().clock(),channelNumber,psize,rem_overhead,ExtraFrameOH,overhead);
#endif

  return overhead;
}

/***********************************************************************
*function: int CMTSmacPhyInterface::getOverHead(Packet *p, int channelNumber)
*
*explanation:
*
*inputs:
*
*outputs:
************************************************************************/
int CMTSmacPhyInterface::getOverHead(int dataSize, int channelNumber)
{
  int overhead;

  overhead = myMedium->getOverHead(dataSize,channelNumber);

  return overhead;
}


CMmacPhyInterface::CMmacPhyInterface()
{
}


/***********************************************************************
*function: void CMmacPhyInterface::init(int nodeNumberParam, int numberDSChannelsParam, int numberUSChannelsParam)
*
*explanation:
*
*inputs:
*
*outputs:
************************************************************************/
void CMmacPhyInterface::init(int nodeNumberParam, medium *myMediumParam)
{
 
  char traceString[32];
  char *tptr = traceString;


  macPhyInterface::init(nodeNumberParam, myMediumParam);

#ifdef TRACEME
  printf("CMmacPhyInterface:init():  inited node %d (#DSChannels:%d,  #USChannels:%d \n",nodeNumberParam,numberDSChannels,numberUSChannels);
#endif

  myDSMonitor = new docsisMonitor();
  myUSMonitor = new docsisMonitor();

  myDSMonitor->init(nodeNumber,PT_SEND_DIRECTION);
  myUSMonitor->init(nodeNumber,PT_RECV_DIRECTION);


  //To create a shared trace file used by all CMs
//  myDSMonitor->setTraceLevel( 0x00 | (packetMonitor_TRACE_TO_FILE) | (packetMonitor_TRACE_TO_BINARY_FILE));
//  myUSMonitor->setTraceLevel( 0x00 | (packetMonitor_TRACE_TO_FILE) | (packetMonitor_TRACE_TO_BINARY_FILE));
// myDSMonitor->setTextTraceFile("CMDSMAC.txt");
//  myUSMonitor->setTextTraceFile("CMUSMAC.txt");
//  myDSMonitor->setBinaryTraceFile("CMDSMAC.bin");
//  myUSMonitor->setBinaryTraceFile("CMUSMAC.bin");

  //TODO: Expose a CM parameter to turn on a trace to a file (txt or bin)
  //Uncomment to create just the txt trace files
//  myDSMonitor->setTraceLevel(packetMonitor_TRACE_TO_FILE );
//  myUSMonitor->setTraceLevel(packetMonitor_TRACE_TO_FILE );
//  sprintf(tptr,"CM%dDSMAC.txt",nodeNumberParam);
//  myDSMonitor->setTextTraceFile(tptr);
//  sprintf(tptr,"CM%dUSMAC.txt",nodeNumberParam);
//  myUSMonitor->setTextTraceFile(tptr);

#ifdef FILES_OK
  //Uncomment to create just the binary trace files
  myDSMonitor->setTraceLevel( 0x00 | (packetMonitor_TRACE_TO_FILE) | (packetMonitor_TRACE_TO_BINARY_FILE));
  myUSMonitor->setTraceLevel( 0x00 | (packetMonitor_TRACE_TO_FILE) | (packetMonitor_TRACE_TO_BINARY_FILE));
  sprintf(tptr,"CM%dDSMAC.txt",nodeNumberParam);
  myDSMonitor->setTextTraceFile(tptr);
  sprintf(tptr,"CM%dUSMAC.txt",nodeNumberParam);
  myUSMonitor->setTextTraceFile(tptr);
  sprintf(tptr,"CM%dDSMAC.bin",nodeNumberParam);
  myDSMonitor->setBinaryTraceFile(tptr);
  sprintf(tptr,"CM%dUSMAC.bin",nodeNumberParam);
  myUSMonitor->setBinaryTraceFile(tptr);
#endif

  myReseqMgr = new reseqMgr();
  myReseqMgr->setMacHandle(myMac);
  myReseqMgr->init(DOWNSTREAM,this);

}

/***********************************************************************
*function: 
*
*explanation:
*
*inputs:
*
*outputs:
************************************************************************/
int CMmacPhyInterface::SendFrame(Packet *p, int channelNumber, MacDocsis *myMacPtr)
{
int rc = 0;
double curTime =   Scheduler::instance().clock();
FILE* fp = NULL;
struct hdr_cmn* ch = HDR_CMN(p);
hdr_docsis* dhdr = HDR_DOCSIS(p);
docsis_dsehdr *chx = docsis_dsehdr::access(p);


//$A700
#ifdef FILES_OK
    fp = fopen("CMSENDTRACE.out", "a+");
    fprintf(fp,"%f\t%3d\t%3d\t%6d\t%6d\t%4d\t%3d\t%3d\n", curTime,myMac->cm_id, channelNumber, chx->packet_sequence_number, chx->dsid, ch->size(), dhdr->dshdr_.fc_type,ch->ptype_);

    fclose(fp);
#endif

#ifdef TRACEME
printf("CMmacPhyInterface:SendFrame(%lf) over channel Number %d, cm_id:%d, size:%d\n",curTime,channelNumber,myMac->cm_id,ch->size());
#endif

  myStats.numberSendFrames++;
  myUSMonitor->packetTrace(p,channelNumber);
  myUSMonitor->updateStats(p);
  myOutChannelMgr->transmitFrame(p, channelNumber, myMacPtr);

  return rc;
}

/***********************************************************************
*function: int CMmacPhyInterface::RecvFrame(Packet *p, int channelNumber)
*
*explanation:
*  This method is invoked when a frame arrives off a channel.
*
*  Code:
*    -check the Destination MAC addr,  if packet is not for this 
*      node drop it
*    -Update the stats (this would be DS stats)
*    -Remove PHY OH and MAC Convergence OH 
*    -If frame contains a MGMT packet, send straight up
*    -Else have the reseqMgr handle the arrival
*    
*
*inputs:
*  Packet *p
*  int channelNumber
*
*outputs:
*
************************************************************************/
int CMmacPhyInterface::RecvFrame(Packet *p, int channelNumber)
{
int rc = SUCCESS;
int dst,src;
struct hdr_mac* mh = HDR_MAC(p);
struct hdr_cmn* ch = HDR_CMN(p);
hdr_docsis   *chd = HDR_DOCSIS(p);


#ifdef TRACE_DEBUG
  myMac->tracePoint("CMMacPhyInterface:RecvFrame:",channelNumber,ch->size());
#endif


  myStats.numberRecvFrames++;


#ifdef TRACEME
  printf("CMmacPhyInterface::RecvFrame(%lf): packet arrived (p:0%x) channel :%d (this:%d) (total Rxed:%f)\n", 
		   Scheduler::instance().clock(),p,channelNumber,this,myStats.numberRecvFrames);
#endif


  dst = mh->macDA(); 
  src = mh->macSA(); 

  /* Not a packet destinated to me. */
  if ((dst != MAC_BROADCAST) && (dst != (u_int32_t)myMac->addr()))
  {
      myMac->drop(p);
      return rc;
  }


  myDSMonitor->packetTrace(p,channelNumber);
  myDSMonitor->updateStats(p);

  /* Deduct the mpeg convergence layer overhead */
  ch->size() -= chd->dt_conv_overhead();

//  if (myReseqMgr->IsFrameMgmt(p) == MGMT_PKT)
 // {
//    myMac->RecvFrame(p);
 // }
//  else
 // {
  //Implement resequencing, this passes up the frame
    rc = myReseqMgr->frameArrival(p,channelNumber);
 // }

#ifdef TRACE_DEBUG
  myMac->tracePoint("CMMacPhyInterface:RecvFrame:",rc,ch->size());
#endif

  return rc;

}

/***********************************************************************
*function: int TxCompletionHandler(Packet *p, int channelNumber)
*
*explanation:
*
*inputs:
*
*outputs:
************************************************************************/
int CMmacPhyInterface::TxCompletionHandler(Packet *p, int channelNumber)
{
int rc = 0;


  myStats.numberTxCompletions++;
  myMac->CmSendHandler(channelNumber,p);

  return rc;
}


/***********************************************************************
*function: int TxCompletionHandler(Packet *p, int channelNumber)
*
*explanation:
*
*inputs:
*
*outputs:
************************************************************************/
int CMmacPhyInterface::TxCompletionHandler(int channelNumber)
{
int rc = 0;

  myStats.numberTxCompletions++;

  return rc;
}


/***********************************************************************
*function: int CMmacPhyInterface::getOverHead(Packet *p, int channelNumber)
*
*explanation:
*
*inputs:
*
*outputs:
************************************************************************/
int CMmacPhyInterface::getOverHead(Packet *p, int channelNumber)
{
  docsis_dsehdr *chx = docsis_dsehdr::access(p);    
  hdr_cmn   *ch  = HDR_CMN(p);
  hdr_docsis   *chd = HDR_DOCSIS(p);
  docsis_chabstr *ch_abs = docsis_chabstr::access(p);    
  int overhead;

  overhead = myMedium->getOverHead(p,channelNumber);

  return overhead;
}



/***********************************************************************
*function: int CMmacPhyInterface::getOverHead(Packet *p, int channelNumber)
*
*explanation:
*
*inputs:
*
*outputs:
************************************************************************/
int CMmacPhyInterface::getOverHead(int dataSize, int channelNumber)
{
  int overhead;

  overhead = myMedium->getOverHead(dataSize,channelNumber);

  return overhead;
}


void CMmacPhyInterface::printStatsSummary()
{
  macPhyInterface::printStatsSummary();
  myReseqMgr->printStatsSummary("RESEQMGRstats.out");
}


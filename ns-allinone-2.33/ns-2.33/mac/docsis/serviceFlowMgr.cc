/***************************************************************************
 * Module: serviceFlowMgr
 *
 * Explanation:
 *   This file contains the class definition of the object that
 *   packages service flows.  
 * 
 *   A flow is identified if it matches the fields in the flow_classifier
 *   structure which currently is defined as:
 *
 *
 *   struct flow_classifier 
 *   {
 *      int32_t  src_ip;
 *      int32_t  dst_ip;
 *      packet_t pkt_type;
 *   };
 *
 * Revisions:
 *    $A510:  
 *     add support for a channelIdelEvent 
 *    $A601 : Add support for channelStatus
 *     $A701: support packets in >1 SFs
 *    $A801:  To support classification by separating service flows from
 *            filtering/accepting packets at a DOCSIS MAC node
 *  $A907  : Added CoDel AQM
 *  $A908  : Added PIE AQM
 *
************************************************************************/

#include "serviceFlowMgr.h"
#include "macPhyInterface.h"

#include "BaseRED.h"
//#include "REDQ.h"
#include "BLUEQ.h"
#include "BAQM.h"
#include "CoDelAQM-TD.h"
#include "PIE-AQM.h"


//$A500
#include "docsisDebug.h"
//#define TRACEME 0


/*************************************************************************
 ************************************************************************/
serviceFlowMgr::serviceFlowMgr()
{
#ifdef TRACEME
  printf("serviceFlowMgr::constructor: \n");
#endif
  myMac = NULL;
  myBGMgr = NULL;
  mySFList = NULL;
  MGMTServiceFlow = NULL;
  myPhyInterface = NULL;
  CmRecord = NULL;
}



void serviceFlowMgr::init(int directionParam) 
{
  direction = directionParam;
//  next_flowid = 3;
  numberSFs = 0;
  lastSFsent = -1;
  MGMTServiceFlow = new serviceFlowObject();
  highestFlowID = 0;

  //Creates the packetList
  MGMTServiceFlow->init(direction,myMac,this);

  MGMTServiceFlow->nextPSN = 0;
  MGMTServiceFlow->flowID = -1;
//$A801
  MGMTServiceFlow->dsid = 1;
  MGMTServiceFlow->type = MGMT_PKT;
  MGMTServiceFlow->macaddr = 0;
  MGMTServiceFlow->classifier.src_ip = 0;
  MGMTServiceFlow->classifier.dst_ip = 0;
  MGMTServiceFlow->classifier.pkt_type = 0;

  //JJM WRONG : For now assume mgmt uses BG 0
  //            and dsid 1
  MGMTServiceFlow->myBGID  = 0;


  mySFList = new ListObj();
  mySFList->initList(MAX_ALLOWED_SERVICE_FLOWS);

  bzero((void *)&myStats,sizeof(struct serviceFlowMgrStatsType));

#ifdef TRACEME 
  printf("serviceFlowMgr::init: direction:%d \n",direction);
#endif

}

int serviceFlowMgr::getNumberSFs() 
{
  return numberSFs;
}

int serviceFlowMgr::getHighestServiceFlowID()
{
  return highestFlowID;
}

//$A701
/***********************************************************************
*function: int serviceFlowMgr::anyPackets() 
*
*explanation: This method finds out for the caller if there are 
*     any packets in any SF waiting to be sent.
* 
*inputs:
*
*outputs:
*  Returns a TRUE  if there are packets, else a  FALSE
*
************************************************************************/
int serviceFlowMgr::anyPackets() 
{
  int mySFcount = getNumberSFs();
  serviceFlowObject *tmpSFObject = NULL;
  serviceFlowListElement *tmpPtr = NULL;
  int rc = FALSE;

#ifdef TRACEME 
    printf("serviceFlowMgr:anyPackets(%lf): SF count: %d \n", Scheduler::instance().clock(),mySFcount);
#endif 

   //Go through the list and look for an explicit match 
  tmpPtr = (serviceFlowListElement *) mySFList->head;
  while (tmpPtr != NULL) 
  {
    tmpSFObject =  tmpPtr->getServiceFlow();
    if (tmpSFObject != NULL) 
    {
		if (tmpSFObject->myPacketCount > 0) {
#ifdef TRACEME 
        printf("serviceFlowMgr:anyPackets(%lf): Found packets in SF %d SF PKT count: %d \n", 
           Scheduler::instance().clock(),tmpSFObject->flowID,tmpSFObject->myPacketCount);
#endif 
           rc = TRUE;
           break;
        }
	}
    tmpPtr = (serviceFlowListElement *)tmpPtr->next;
  }
  
   //If have not found any SF packets, check mgmt SF
  if (rc == FALSE) {
    if (MGMTServiceFlow->myPacketCount > 0) {
#ifdef TRACEME 
        printf("serviceFlowMgr:anyPackets(%lf): Found packets in MGMT SF(flowID:%d),  PKT count: %d \n", 
           Scheduler::instance().clock(),tmpSFObject->flowID,tmpSFObject->myPacketCount);
#endif 
      rc = TRUE;
    }
  }
  return rc;
}

void serviceFlowMgr::setMyPhyInterface(macPhyInterface *myPhyInterfaceParam)
{
  myPhyInterface = myPhyInterfaceParam;
}


void serviceFlowMgr::setMacHandle(MacDocsis *myMacParam)
{
	myMac =  myMacParam;
}

void serviceFlowMgr::setBGMgr(bondingGroupMgr * myBGMgrParam)
{
	myBGMgr =  myBGMgrParam;
}

void serviceFlowMgr::setCMRecord(struct cm_record *CmRecordParam)
{
  CmRecord = CmRecordParam;
}

void serviceFlowMgr::getStats(struct serviceFlowMgrStatsType *callersStats)
{
  double curTime = Scheduler::instance().clock();
  serviceFlowObject *SFObject = NULL;
  serviceFlowObject *tmpSFObject = NULL;
  serviceFlowListElement *SFListElement = NULL;
  serviceFlowListElement *tmpPtr = NULL;
  double lastUpdateTime = 0;;
  int totalPacketsServiced =0;
  double totalByteArrivals=0;
  double totalQueueDelay=0;
  double totalLossRate=0;
  double totalArrivalRate= 0;
  double totalServiceRate= 0;
  double flowTime=0;
  double lowestFlowTime=0;
  double highestFlowTime=0;

#ifdef TRACEME //---------------------------------------------------------
  printf("serviceFlowMgr::getStats (%lf): #Flows:%d  \n", curTime, numberSFs);
#endif //---------------------------------------------------------------

  myStats.numberServiceFlows = numberSFs;

   //Go through the list and look for an explicit match 
  tmpPtr = (serviceFlowListElement *) mySFList->head;
  while (tmpPtr != NULL) 
  {
    tmpSFObject =  tmpPtr->getServiceFlow();
    if (tmpSFObject != NULL) 
    {
      flowTime= tmpSFObject->myStats.lastUpdateTime - tmpSFObject->myStats.firstUpdateTime ;
      if (lowestFlowTime > flowTime)
        lowestFlowTime = flowTime;
      if (highestFlowTime < flowTime)
        highestFlowTime = flowTime;
  
      if (flowTime > 0) {
          totalArrivalRate += (8 * tmpSFObject->myStats.numberByteArrivals) / flowTime;
          totalServiceRate += (8 * tmpSFObject->myStats.numberBytesServiced) / flowTime;
      }
      totalPacketsServiced  += tmpSFObject->myStats.numberPacketsServiced;
      totalByteArrivals += tmpSFObject->myStats.numberByteArrivals;
      totalQueueDelay += tmpSFObject->myStats.avgQueueDelay;
      totalLossRate += tmpSFObject->myStats.avgLossRate;
#ifdef TRACEME //---------------------------------------------------------
      printf("serviceFlowMgr::getStats (%lf): look at this flow:%d,  flowTime:%lf,  ByteArrivals:%f, TotalByteArrivals:%f  lossRate:%3.2f, totalLossRate:%3.3f \n",
       curTime, tmpSFObject->flowID,flowTime, tmpSFObject->myStats.numberByteArrivals, totalByteArrivals,tmpSFObject->myStats.avgLossRate, totalLossRate);
#endif //---------------------------------------------------------------

	}
    tmpPtr = (serviceFlowListElement *)tmpPtr->next;
  }

  if (numberSFs > 0) {
    myStats.avgArrivalRate = totalArrivalRate / numberSFs;
    myStats.avgServiceRate = totalServiceRate / numberSFs;
    myStats.avgLossRate = totalLossRate / numberSFs;
    myStats.avgPacketDelay = totalQueueDelay / numberSFs;
  }

#ifdef TRACEME //---------------------------------------------------------
  printf("serviceFlowMgr::getStats (%lf): #Flows:%d  lowestFlowTime:%lf, highestFlowTime:%lf\n",
       curTime, myStats.numberServiceFlows, lowestFlowTime,highestFlowTime);
#endif //---------------------------------------------------------------

  *callersStats = myStats;

}

/***********************************************************************
*function: void serviceFlowMgr::printStatsShortSummary()
*
*explanation:
*
*inputs:
*
*outputs:
*
************************************************************************/
void serviceFlowMgr::printShortSummary()
{
  serviceFlowObject *SFObject = NULL;
  serviceFlowObject *tmpSFObject = NULL;
  serviceFlowListElement *SFListElement = NULL;
  serviceFlowListElement *tmpPtr = NULL;


  if (direction == DOWNSTREAM)
    printf("serviceFlowMgr::printShortSummary(%lf): Number of DS Service Flows in the list: %d \n",
		    Scheduler::instance().clock(), numberSFs);
  else
    printf("serviceFlowMgr::printShortSummary(%lf): Number of US Service Flows in the list: %d \n",
		    Scheduler::instance().clock(), numberSFs);


   //Go through the list and look for an explicit match 
  tmpPtr = (serviceFlowListElement *) mySFList->head;
  while (tmpPtr != NULL) 
  {
    tmpSFObject =  tmpPtr->getServiceFlow();
    if (tmpSFObject != NULL) 
    {
        printf("serviceFlowMgr::printStatsSummary(%lf):SF: flowID:%d \n ",
		    Scheduler::instance().clock(), tmpSFObject->flowID);
		tmpSFObject->printShortSummary();
	}
    tmpPtr = (serviceFlowListElement *)tmpPtr->next;
  }

}


/***********************************************************************
*function: void serviceFlowMgr::printStatsSummary()
*
*explanation:
*
*inputs:
*
*outputs:
*
************************************************************************/
void serviceFlowMgr::printStatsSummary(char *outputFile)
{
  serviceFlowObject *SFObject = NULL;
  serviceFlowObject *tmpSFObject = NULL;
  serviceFlowListElement *SFListElement = NULL;
  serviceFlowListElement *tmpPtr = NULL;


  if (direction == DOWNSTREAM)
    printf("serviceFlowMgr::printStatsSummary(%lf): Number of DS Service Flows in the list: %d \n",
		    Scheduler::instance().clock(), numberSFs);
  else
    printf("serviceFlowMgr::printStatsSummary(%lf): Number of US Service Flows in the list: %d \n",
		    Scheduler::instance().clock(), numberSFs);


   //Go through the list and look for an explicit match 
  tmpPtr = (serviceFlowListElement *) mySFList->head;
  while (tmpPtr != NULL) 
  {
    tmpSFObject =  tmpPtr->getServiceFlow();
    if (tmpSFObject != NULL) 
    {
        printf("serviceFlowMgr::printStatsSummary(%lf):SF: flowID:%d \n ",
		    Scheduler::instance().clock(), tmpSFObject->flowID);
		tmpSFObject->printStatsSummary(outputFile);
	}
    tmpPtr = (serviceFlowListElement *)tmpPtr->next;
  }

}

//$A807
/***********************************************************************
*function: void resetSFPriority();
*
*explanation:
*  This method sets the flowPriority of all SFs to 0.  This overrides
*  the initial tcl configuration of the priority field.
*   
*inputs:
*
*outputs:
*
************************************************************************/
void serviceFlowMgr::resetSFPriority()
{
  serviceFlowObject *tmpSFObject = NULL;
  serviceFlowListElement *tmpPtr = NULL;
  double curTime = Scheduler::instance().clock();

#ifdef TRACEME //---------------------------------------------------------
  printf("serviceFlowMgr::resetSFPriority(%lf): clearing the flowPriority of %d SFs  \n", curTime, numberSFs);
#endif //---------------------------------------------------------------

   //Go through the list and look for an explicit match 
  tmpPtr = (serviceFlowListElement *) mySFList->head;
  while (tmpPtr != NULL) 
  {
    tmpSFObject =  tmpPtr->getServiceFlow();
    tmpSFObject->flowPriority = 0;
    tmpPtr = (serviceFlowListElement *)tmpPtr->next;
  }
}

/***********************************************************************
*function: int serviceFlowMgr::optimize(int opCode)
*
*explanation:
*  This is called periodically to optimize the system.  
*
*inputs:
*  int opCode : indicates a specific type of optimization that is desired
*      Refer to globalDefines.h, but some possible values are:
*         #define OPTIMiZER_HEARTBEAT  0
*         #define ADJUST_PACKET_SCHEDULER 1
*         #define LOAD_BALANCE 2
*
*outputs:
*  returns a SUCCESS or FAILURE
*
************************************************************************/
int serviceFlowMgr::optimize(int opCode)
{
  int rc = SUCCESS;
  serviceFlowObject *tmpSFObject = NULL;
  serviceFlowListElement *tmpPtr = NULL;
  double curTime = Scheduler::instance().clock();

#ifdef TRACEME1 //---------------------------------------------------------
    printf("serviceFlowMgr::optimize(%lf): opCode: %d \n", Scheduler::instance().clock(),opCode);
#endif //---------------------------------------------------------------


  if (opCode == OPTIMIZER_HEARTBEAT) {
     //Go through the list of SFObjects and update their rates 
    tmpPtr = (serviceFlowListElement *) mySFList->head;
    while (tmpPtr != NULL) 
    {
      tmpSFObject =  tmpPtr->getServiceFlow();
      tmpSFObject->updateRates();
      tmpPtr = (serviceFlowListElement *)tmpPtr->next;
    }
  }

  return rc;
}

/***********************************************************************
*function: void serviceFlowMgr::dumpSFQueueLevels(char *outputFile)
*
*explanation:
*   This dumps the latest Queue monitor information into the outputFile.
*   NOTE:  This is used for the *QUEUE*.out data.
*
*inputs:
*
*outputs:
*
************************************************************************/
void serviceFlowMgr::dumpSFQueueLevels(char *outputFile)
{
  serviceFlowObject *tmpSFObject = NULL;
  serviceFlowListElement *tmpPtr = NULL;
  FILE *fp;
  double curTime = Scheduler::instance().clock();
  int numberPkts;
  double dropRate = 0.0;
  double totalDropRate = 0.0;

  if ((myBGMgr!= NULL) && (myBGMgr->getHierarchicalMode() == TRUE )) {
    myBGMgr->dumpSFQueueLevels(outputFile);
  }
  else {

#ifdef TRACEME //---------------------------------------------------------
  printf("serviceFlowMgr::dumpSFQueueLevels(%lf): Create file %s, Number of DS Service Flows in the list: %d \n",
       curTime,outputFile, numberSFs);
#endif //---------------------------------------------------------------

  fp = fopen(outputFile, "a+");

   //Go through the list and look for an explicit match 
  tmpPtr = (serviceFlowListElement *) mySFList->head;
  while (tmpPtr != NULL) 
  {
    tmpSFObject =  tmpPtr->getServiceFlow();
    if (tmpSFObject != NULL) 
    {
#ifdef TRACEME
      printf("serviceFlowMgr::dumpSFQueueLevels(%lf):SF: flowID:%d \n ",
	   curTime, tmpSFObject->flowID);
#endif 
      if (tmpSFObject->monitorPacketArrivalCount > 0) {
        dropRate = ((double)tmpSFObject->monitorDropCount) / (double)tmpSFObject->monitorPacketArrivalCount;
      } 

      if (tmpSFObject->myStats.numberPacketsServiced > 0) {
        totalDropRate = ((double)tmpSFObject->myStats.TotalPacketsDropped) / (double)tmpSFObject->myStats.numberPacketsServiced;
      } 


      numberPkts = tmpSFObject->packetsQueued();
      /* Print :  timestamp  max pkts  min pkts  cur pkts util */
//$A510  - this is for AQM packet lists
      if (tmpSFObject->SchedQType == RedQ) {
//        REDQ *tmpPQPtr = (REDQ *)tmpSFObject->myPacketList;
        BaseRED *tmpPQPtr = (BaseRED *)tmpSFObject->myPacketList;
        fprintf(fp,"%f\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%1.3f\t%1.3f\t%3.3f\t%3.3f\t%3.3f\t%3.3f\t%3.3f\t0.0\n",
           curTime,tmpSFObject->flowID,tmpSFObject->max_qnp_,tmpSFObject->min_qnp_,tmpSFObject->qnp_,
           tmpSFObject->max_qnb_,tmpSFObject->min_qnb_,tmpSFObject->qnb_,tmpSFObject->monitorPacketArrivalCount, tmpSFObject->monitorDropCount,dropRate,totalDropRate,
           tmpPQPtr->getAvgQ(), tmpPQPtr->getAvgAvgQ(),tmpPQPtr->getAvgAvgQLatency(),  tmpPQPtr->getDropP(), tmpPQPtr->getAvgDropP());

      }
      else if (tmpSFObject->SchedQType == AdaptiveRedQ) {
        BaseRED *tmpPQPtr = (BaseRED *)tmpSFObject->myPacketList;
        fprintf(fp,"%f\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%1.3f\t%1.3f\t%3.3f\t%3.3f\t%3.3f\t%3.3f\t%3.3f\t0.0\n",
           curTime,tmpSFObject->flowID,tmpSFObject->max_qnp_,tmpSFObject->min_qnp_,tmpSFObject->qnp_,
           tmpSFObject->max_qnb_,tmpSFObject->min_qnb_,tmpSFObject->qnb_,tmpSFObject->monitorPacketArrivalCount, tmpSFObject->monitorDropCount,dropRate,totalDropRate,
           tmpPQPtr->getAvgQ(), tmpPQPtr->getAvgAvgQ(),tmpPQPtr->getAvgAvgQLatency(),  tmpPQPtr->getDropP(), tmpPQPtr->getAvgDropP());
      }
      else if (tmpSFObject->SchedQType == DelayBasedRedQ) {
        BaseRED *tmpPQPtr = (BaseRED *)tmpSFObject->myPacketList;
        fprintf(fp,"%f\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%1.3f\t%1.3f\t%3.3f\t%3.3f\t%3.3f\t%3.3f\t%3.3f\t0.0\n",
           curTime,tmpSFObject->flowID,tmpSFObject->max_qnp_,tmpSFObject->min_qnp_,tmpSFObject->qnp_,
           tmpSFObject->max_qnb_,tmpSFObject->min_qnb_,tmpSFObject->qnb_,tmpSFObject->monitorPacketArrivalCount, tmpSFObject->monitorDropCount,dropRate,totalDropRate,
           tmpPQPtr->getAvgQ(), tmpPQPtr->getAvgAvgQ(),tmpPQPtr->getAvgAvgQLatency(),  tmpPQPtr->getDropP(), tmpPQPtr->getAvgDropP());
      }
      else if (tmpSFObject->SchedQType == BAQMQ) {
        BAQM *tmpPQPtr = (BAQM *)tmpSFObject->myPacketList;
        fprintf(fp,"%f\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%1.3f\t%1.3f\t%3.3f\t%3.3f\t%3.3f\t%3.3f\t%3.3f\t%3.3f\t0.0\n",
           curTime,tmpSFObject->flowID,tmpSFObject->max_qnp_,tmpSFObject->min_qnp_,tmpSFObject->qnp_,
           tmpSFObject->max_qnb_,tmpSFObject->min_qnb_,tmpSFObject->qnb_,tmpSFObject->monitorPacketArrivalCount, tmpSFObject->monitorDropCount,dropRate,totalDropRate,
           tmpPQPtr->getAvgQ(), tmpPQPtr->getAvgAvgQ(), tmpPQPtr->getDropP(), tmpPQPtr->getAvgDropP());
      }
      else if (tmpSFObject->SchedQType == BlueQ) {
        BLUEQ *tmpPQPtr = (BLUEQ *)tmpSFObject->myPacketList;
        fprintf(fp,"%f\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%1.3f\t%1.3f\t%3.3f\t%3.3f\t%3.3f\t%3.3f\t%3.3f\t0.0\n",
           curTime,tmpSFObject->flowID,tmpSFObject->max_qnp_,tmpSFObject->min_qnp_,tmpSFObject->qnp_,
           tmpSFObject->max_qnb_,tmpSFObject->min_qnb_,tmpSFObject->qnb_,tmpSFObject->monitorPacketArrivalCount, tmpSFObject->monitorDropCount,dropRate,totalDropRate,
           tmpPQPtr->getAvgQ(), tmpPQPtr->getAvgAvgQ(), tmpPQPtr->getDropP(), tmpPQPtr->getAvgDropP());

      }
//$A907
      else if (tmpSFObject->SchedQType == CDAQM) {
        CoDelAQM *tmpPQPtr = (CoDelAQM *)tmpSFObject->myPacketList;
        fprintf(fp,"%f\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%1.3f\t%1.3f\t%3.3f\t%3.3f\t%3.3f\t%3.3f\t%3.3f\t0.0\n",
           curTime,tmpSFObject->flowID,tmpSFObject->max_qnp_,tmpSFObject->min_qnp_,tmpSFObject->qnp_,
           tmpSFObject->max_qnb_,tmpSFObject->min_qnb_,tmpSFObject->qnb_,tmpSFObject->monitorPacketArrivalCount, tmpSFObject->monitorDropCount,dropRate,totalDropRate,
           tmpPQPtr->getAvgQ(), tmpPQPtr->getAvgAvgQ(), tmpPQPtr->getDropP(), tmpPQPtr->getAvgDropP());

      }
//$A908
      else if (tmpSFObject->SchedQType == PIAQM) {
        PIEAQM *tmpPQPtr = (PIEAQM *)tmpSFObject->myPacketList;
        fprintf(fp,"%f\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%1.3f\t%1.3f\t%3.3f\t%3.3f\t%3.3f\t%3.3f\t%3.3f\t0.0\n",
           curTime,tmpSFObject->flowID,tmpSFObject->max_qnp_,tmpSFObject->min_qnp_,tmpSFObject->qnp_,
           tmpSFObject->max_qnb_,tmpSFObject->min_qnb_,tmpSFObject->qnb_,tmpSFObject->monitorPacketArrivalCount, tmpSFObject->monitorDropCount,dropRate,totalDropRate,
           tmpPQPtr->getAvgQ(), tmpPQPtr->getAvgAvgQ(), tmpPQPtr->getDropP(), tmpPQPtr->getAvgDropP());

      }
      else {
        fprintf(fp,"%f\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%1.3f\t%1.3f\t0.0\t0.0\t0.0\t0.0\n",
           curTime,tmpSFObject->flowID,tmpSFObject->max_qnp_,tmpSFObject->min_qnp_,tmpSFObject->qnp_,
           tmpSFObject->max_qnb_,tmpSFObject->min_qnb_,tmpSFObject->qnb_,tmpSFObject->monitorPacketArrivalCount, tmpSFObject->monitorDropCount,dropRate,totalDropRate);
      }

#ifdef TRACEME
      printf("serviceFlowMgr::dumpSFQueueLevels:(%f):\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%1.3f\t%1.3f\n",
           curTime,tmpSFObject->flowID,tmpSFObject->max_qnp_,tmpSFObject->min_qnp_,tmpSFObject->qnp_,
           tmpSFObject->max_qnb_,tmpSFObject->min_qnb_,tmpSFObject->qnb_,tmpSFObject->monitorPacketArrivalCount, tmpSFObject->monitorDropCount,dropRate,totalDropRate);
#endif 

       tmpSFObject->lastDumpTime = curTime; /* Reset */
       tmpSFObject->monitorPacketArrivalCount = 0;
       tmpSFObject->monitorDropCount = 0;

       /* Reset min/max to current queue level */
       tmpSFObject->max_qnp_ = tmpSFObject->qnp_;
       tmpSFObject->min_qnp_ = tmpSFObject->qnp_;
       tmpSFObject->max_qnb_ = tmpSFObject->qnb_;
       tmpSFObject->min_qnb_ = tmpSFObject->qnb_;

	}
    tmpPtr = (serviceFlowListElement *)tmpPtr->next;
  }

  fclose(fp);
  }
}

/*************************************************************************
*function: int serviceFlowMgr::IsFrameMgmt(Packet* p)
*
*explanation:
*   Determines if teh packet is a DOCSIS Mgmt packet or NOT
*
*inputs:
*   Packet *p : ptr to the packet object
*
*outputs:
*   This returns a 0 if this frame contains data (DATA_PKT)
*    or a 1 if it is MGMT_PKT
*
*************************************************************************/
int serviceFlowMgr::IsFrameMgmt(Packet* p)
{
  struct hdr_docsis *ds = HDR_DOCSIS(p);

#ifdef TRACEME //---------------------------------------------------------
  printf("serviceFlowMgr:IsFrameMgr(%lf):  frame type is %d \n", 
      Scheduler::instance().clock(), ds->dshdr_.fc_type);
#endif //---------------------------------------------------------------
  if (ds->dshdr_.fc_type == DATA)
    {
      return DATA_PKT;
    }
  else if (ds->dshdr_.fc_type == MAC_SPECIFIC)
    {
      return MGMT_PKT;
    }
//$A405
//#ifdef TRACEME //---------------------------------------------------------
  printf("serviceFlowMgr:IsFrameMgr:(%lf) HARD ERROR: its neither DATA nor MAC_SPECIFIC\n", Scheduler::instance().clock());
//#endif //---------------------------------------------------------------
  return MGMT_PKT;
}


/***********************************************************************
*function: serviceFlowObject * serviceFlowMgr::getServiceFlow(Packet *p)
*
*explanation:
*  This method returns a reference to a serviceFlowObject.  This method
*  returns a NULL if the SF does not exist in the current List.
 *   Examines the packet and matches it to a flow in the SF List.
*
*inputs:
*   Packet *p : ptr to the packet object
*
*outputs:
*  returns a reference to a serviceFlowObject. 
*  Returns a NULL if the list is empty or if there is no match
*
*  Outdated as of 6/24/2010 -  use the CMTS or CM or Reseq types
************************************************************************/
serviceFlowObject * serviceFlowMgr::getServiceFlow(Packet *p)
{
  serviceFlowObject *SFObject = NULL;
  serviceFlowObject *tmpSFObject = NULL;
  serviceFlowListElement *SFListElement = NULL;
  serviceFlowListElement *tmpPtr = NULL;
  int i;
  struct hdr_cmn *ch = HDR_CMN(p);
  struct hdr_ip *chip = HDR_IP(p);


  if (IsFrameMgmt(p))
  {
    SFObject = MGMTServiceFlow;
#ifdef TRACEME 
   printf("serviceFlowMgr::getServiceFlow(%lf): matched MGMT FLOW to pkt: (src,dst,type):%d,%d,%d), #SFs:%d\n",
		    Scheduler::instance().clock(), chip->saddr(), chip->daddr(), ch->ptype(),numberSFs);
#endif
  }
  else
  {

#ifdef TRACEME 
   printf("serviceFlowMgr::getServiceFlow(%lf): Try to match pkt: (src,dst,type):%d,%d,%d), #SFs:%d\n",
		    Scheduler::instance().clock(), chip->saddr(), chip->daddr(), ch->ptype(),numberSFs);
#endif

   //Go throught the list and look for an explicit match 
  tmpPtr = (serviceFlowListElement *) mySFList->head;
  while (tmpPtr != NULL) 
  {
#ifdef TRACEME 
      printf("serviceFlowMgr::getServiceFlow(%lf):Begin looking at the list, first element:%0x\n ",
		    Scheduler::instance().clock(),tmpPtr);
#endif
    tmpSFObject =  tmpPtr->getServiceFlow();
    if ((tmpSFObject != NULL) && (tmpSFObject->type == DATA)) 
    {
#ifdef TRACEME 
        printf("serviceFlowMgr::getServiceFlow(%lf): List Index: %d Try to match pkt with this SF: flowID:%d \n ",
		    Scheduler::instance().clock(),i, tmpSFObject->flowID);
		tmpSFObject->printSFInfo();
#endif

//$A801
         if (tmpSFObject->matchMACAddress(p,direction)) {
//         if (tmpSFObject->match(p)) {
//$A510
//         if ( (tmpSFObject->match(p,DOWNSTREAM)) || (tmpSFObject->match(p,UPSTREAM))) {
#ifdef TRACEME 
           printf("serviceFlowMgr::getServiceFlow: FOUND A POSSIBLE MATCH with list element %0x \n",tmpPtr);
#endif

           SFObject = tmpSFObject;
		   break;
         }
	}
    tmpPtr = (serviceFlowListElement *)tmpPtr->next;
  }

  if (SFObject == NULL)
  {

    //IF there was not an explicit match, let's look for the default service flow
    tmpPtr = (serviceFlowListElement *) mySFList->head;
    while (tmpPtr != NULL) 
    {
#ifdef TRACEME 
      printf("serviceFlowMgr::getServiceFlow(%lf):Begin looking at the list, first element:%0x\n ",
		    Scheduler::instance().clock(),tmpPtr);
#endif
      tmpSFObject =  tmpPtr->getServiceFlow();
      if ((tmpSFObject != NULL) && (tmpSFObject->type == DATA)) 
      {

//$A510
         if ( (tmpSFObject->matchAddress(p,DOWNSTREAM)) || (tmpSFObject->matchAddress(p,UPSTREAM))) {
#ifdef TRACEME
           printf("serviceFlowMgr::getServiceFlow: FOUND A MATCH with list element %0x \n",tmpPtr);
#endif

           SFObject = tmpSFObject;
		   break;
         }
	  }
      else {
        printf("serviceFlowMgr::getServiceFlow(%lf):  No match here: tmpSFObject:%d,  tmpSFObject type:%d\n ",
		    Scheduler::instance().clock(),tmpSFObject, tmpSFObject->type );
      }
      tmpPtr = (serviceFlowListElement *)tmpPtr->next;
    }

  }
#ifdef TRACEME 
  if (SFObject == NULL)
     printf("serviceFlowMgr::getServiceFlow(%lf): DID NOT Find a match pkt\n",
		    Scheduler::instance().clock());
  else
     printf("serviceFlowMgr::getServiceFlow(%lf): FOUND a match pkt: flowID:%d\n",
		    Scheduler::instance().clock(), SFObject->flowID);
#endif

  }
  return SFObject;
}

/***********************************************************************
*function: serviceFlowObject * serviceFlowMgr::getServiceFlow(Packet *p, int type)
*
*explanation:
*  This method returns a reference to a serviceFlowObject.  This method
*  returns a NULL if the SF does not exist in the current List.
*
*
*inputs:
*   Packet *p:
*   int type:  values either DATA_PKT or MGMT_PKT.  If this is MGMT_PKT we need
*              to return the pre-existing MGMT service flow.
*
*outputs:
*   This returns a pointer to a the service flow object.
*   The caller should never delete this object and they are created/deleted
*   solely by this object.
*
************************************************************************/
serviceFlowObject * serviceFlowMgr::getServiceFlow(Packet *p, int type)
{
  serviceFlowObject *SFObject = NULL;

#ifdef TRACEME 
   printf("serviceFlowMgr::getServiceFlow(%lf): type:%d \n", 
		   Scheduler::instance().clock(),type);
#endif

  if (type == DATA_PKT)
  {
//$A801
    SFObject = getServiceFlow(p);
  }
  else
  {
    SFObject = MGMTServiceFlow;
  }
  return SFObject;
}


/***********************************************************************
*function: serviceFlowObject * serviceFlowMgr::addServiceFlow()
*
*explanation:
*  This method adds a service flow to the list.  
*    The caller is expected to fill in the necessary details
*
*  Pseudo code:
*     -check to see if the service flow already exists
*     -create the new SF Object
*     -add it to the list
*     -return the new flow id
*
*
*inputs:
*
*outputs:
*  Returns a handle to the service flow object, else a NULL
*
************************************************************************/
serviceFlowObject * serviceFlowMgr::addServiceFlow(u_int16_t flow_id)
{
serviceFlowObject * myNewSFObject = NULL;
int rc = -1;

//  next_flowid++;

  serviceFlowListElement *mySFElement = new serviceFlowListElement();
  if (direction == DOWNSTREAM)
  {
     myNewSFObject = new DSserviceFlowObject();
#ifdef TRACEME 
     printf("serviceFlowMgr::addServiceFlow(%lf):DOWNSTREAM  service flow object created, flow id:%d  \n", 
		   Scheduler::instance().clock(),flow_id);
#endif
  }
  else
  {
     myNewSFObject = new USserviceFlowObject();
#ifdef TRACEME 
     printf("serviceFlowMgr::addServiceFlow(%lf):UPSTREAM service flow object created, flow id:%d  \n", 
		   Scheduler::instance().clock(),flow_id);
#endif
  }


  myNewSFObject->init(direction,myMac,this);
  myNewSFObject->flowID = flow_id;


  //init the element
  mySFElement->putServiceFlow(myNewSFObject);

  rc = mySFList->addElement(*mySFElement);
  if (rc == SUCCESS)
  {
    numberSFs++;
    if (flow_id > highestFlowID)
      highestFlowID = flow_id;

#ifdef TRACEME 
     printf("serviceFlowMgr::addServiceFlow(%lf):SUCCEEDED to add this service flow:%d \n", 
		   Scheduler::instance().clock(),flow_id);
#endif
  }
  else
  {
#ifdef TRACEME 
     printf("serviceFlowMgr::addServiceFlow(%lf):FAILED to add this service flow:%d \n", 
		   Scheduler::instance().clock(),flow_id);
#endif
     rc = -1;
     delete myNewSFObject;
     delete mySFElement;
     myNewSFObject = NULL;
  }

  return myNewSFObject;
}


/***********************************************************************
*function: int serviceFlowMgr::addServiceFlow(int32_t src_ip, int32_t dst_ip, packet_t pkt_type, u_int16_t flow_id)
*
*explanation:
*  This method adds a service flow to the list.  
*
*  Pseudo code:
*     -check to see if the service flow already exists
*     -create the new SF Object
*     -add it to the list
*     -return the new flow id
*
*
*inputs:
*
*outputs:
*  Returns a valid flow id.  Else returns a -1
*
************************************************************************/
int serviceFlowMgr::addServiceFlow(int macaddr, int32_t src_ip, int32_t dst_ip, packet_t pkt_type, u_int16_t flow_id,int myBGIDParam)
{
serviceFlowObject * myNewSFObject = NULL;
int rc = -1;

//  next_flowid++;

  serviceFlowListElement *mySFElement = new serviceFlowListElement();
  if (direction == DOWNSTREAM)
  {
     myNewSFObject = new DSserviceFlowObject(macaddr,src_ip,dst_ip,pkt_type,flow_id,myBGIDParam);
#ifdef TRACEME 
     printf("serviceFlowMgr::addServiceFlow(%lf):DOWNSTREAM flowID:%d, macaddr:%d, src_ip:%d, dst_ip:%d,  pkt_type:%d \n", 
		   Scheduler::instance().clock(), flow_id,macaddr,src_ip, dst_ip,pkt_type);
#endif
  }
  else
  {
     myNewSFObject = new USserviceFlowObject(macaddr,src_ip,dst_ip,pkt_type,flow_id,myBGIDParam);
#ifdef TRACEME 
     printf("serviceFlowMgr::addServiceFlow(%lf):UPSTREAM flowID:%d, macaddr:%d, src_ip:%d, dst_ip:%d,  pkt_type:%d \n", 
		   Scheduler::instance().clock(),flow_id,macaddr,src_ip, dst_ip,pkt_type);
#endif
  }


  myNewSFObject->init(direction,myMac,this);


  //init the element
  mySFElement->putServiceFlow(myNewSFObject);

  rc = mySFList->addElement(*mySFElement);
  if (rc == SUCCESS)
  {
//$A310
//    next_flowid++;
    rc = flow_id;
    numberSFs++;
    if (flow_id > highestFlowID)
      highestFlowID = flow_id;

#ifdef TRACEME 
     printf("serviceFlowMgr::addServiceFlow(%lf):SUCCEEDED to add this service flow:%d, macaddr:%d, src_ip:%d, dst_ip:%d,  pkt_type:%d \n", 
		   Scheduler::instance().clock(),flow_id,macaddr,src_ip, dst_ip,pkt_type);
#endif
  }
  else
  {
#ifdef TRACEME 
     printf("serviceFlowMgr::addServiceFlow(%lf):FAILED to add this service flow:%d, macaddr:%d, src_ip:%d, dst_ip:%d,  pkt_type:%d \n", 
		   Scheduler::instance().clock(),flow_id,macaddr,src_ip, dst_ip,pkt_type);
#endif
     rc = -1;
     delete myNewSFObject;
     delete mySFElement;
  }

  return rc;
}

/***********************************************************************
*function: int serviceFlowMgr::removeServiceFlow(u_int16_t flowID)
*
*explanation:
*  This method  searches the SF list using the flow id as the key.  If
*  it finds a match it removes the element in the list. 
*  Else it returns a NULL;
*
*  This routine MUST delete the list element object.
*
*inputs:
*  u_int16_t flowID : the flow id to search for
*
*outputs:
*  Returns a SUCCESS or FAILURE
*
************************************************************************/
int serviceFlowMgr::removeServiceFlow(u_int16_t flowID)
{
  int rc = FAILURE;
  serviceFlowListElement *tmpPtr = NULL;
  serviceFlowObject *tmpSFObject = NULL;

#ifdef TRACEME 
  printf("serviceFlowMgr::removeServiceFlow(%lf):Begin looking for this flow:%d (numberSFs::%d)\n ",
	  Scheduler::instance().clock(),flowID, numberSFs);
#endif

   //Go throught the list and look for an explicit match 
  tmpPtr = (serviceFlowListElement *) mySFList->head;
  while (tmpPtr != NULL) 
  {
    tmpSFObject =  tmpPtr->getServiceFlow();
    if (tmpSFObject != NULL)  
    {
#ifdef TRACEME 
      printf("serviceFlowMgr::removeServiceFlow(%lf):  SF element flowid: :%d\n ",
		    Scheduler::instance().clock(), tmpSFObject->flowID);
      tmpSFObject->printSFInfo();
#endif
      if (tmpSFObject->flowID == flowID) {
#ifdef TRACEME 
        printf("serviceFlowMgr::removeServiceFlow(%lf):  FOUND IT flowid: :%d\n ",
		    Scheduler::instance().clock(), tmpSFObject->flowID);
#endif
		//Remove the  element pointed at by tmpPtr
		rc = mySFList->removeElement(tmpPtr);
		break;
      }
    }
    tmpPtr = (serviceFlowListElement *)tmpPtr->next;
  }
  return rc;
}



/***********************************************************************
*function: serviceFlowObject *  serviceFlowMgr::returnServiceFlowObject(u_int16_t flowID)
*
*explanation:
*  This method  searches the SF list using the flow id as the key.  If
*  it finds a match it returns a ptr to the SF object.  Else it 
*  returns a NULL;
*
*inputs:
*  u_int16_t flowID : the flow id to search for
*
*outputs:
*  Returns a SF Object else a NULL;
*
************************************************************************/
serviceFlowObject *  serviceFlowMgr::returnServiceFlowObject(u_int16_t flowID)
{
  serviceFlowListElement *tmpPtr = NULL;
  serviceFlowObject *tmpSFObject = NULL;

#ifdef TRACEME 
  printf("serviceFlowMgr::returnServiceFlowObject(%lf):Begin looking for this flow:%d (numberSFs::%d)\n ",
	  Scheduler::instance().clock(),flowID, numberSFs);
#endif

   //Go throught the list and look for an explicit match 
  tmpPtr = (serviceFlowListElement *) mySFList->head;
  while (tmpPtr != NULL) 
  {
    tmpSFObject =  tmpPtr->getServiceFlow();
    if (tmpSFObject != NULL)  
    {
#ifdef TRACEME 
      printf("serviceFlowMgr::returnServiceFlowObject(%lf):  SF element flowid: :%d\n ",
		    Scheduler::instance().clock(), tmpSFObject->flowID);
      tmpSFObject->printSFInfo();
#endif
      if (tmpSFObject->flowID == flowID) {
#ifdef TRACEME 
        printf("serviceFlowMgr::returnServiceFlowObject(%lf):  FOUND IT flowid: :%d\n ",
		    Scheduler::instance().clock(), tmpSFObject->flowID);
#endif
        return tmpSFObject;
      }
    }
    tmpPtr = (serviceFlowListElement *)tmpPtr->next;
  }
  return NULL;
}



/*************************************************************************
 ************************************************************************/
CMserviceFlowMgr::CMserviceFlowMgr()
{
#ifdef TRACEME
  printf("CMserviceFlowMgr::constructor: \n");
#endif

}



void CMserviceFlowMgr::init(int directionParam, int ackSuppressionFlag, int cm_idParam) 
{

  ACK_SUPPRESSION = ackSuppressionFlag;
  cm_id = cm_idParam;
  myCMMac  = (MacDocsisCM *)myMac;
#ifdef TRACEME
  printf("CMserviceFlowMgr::init: direction:%d, ackSuppressionFlag:%d, cm_id:%d \n",
  directionParam,ackSuppressionFlag,cm_id);
#endif
  serviceFlowMgr::init(directionParam);
}


/***********************************************************************
*function: serviceFlowObject * serviceFlowMgr::getServiceFlow(Packet *p)
*
*explanation:
*  This method returns a reference to a serviceFlowObject.  This method
*  returns a NULL if the SF does not exist in the current List.
 *   Examines the packet and matches it to a flow in the SF List.
*
*inputs:
*   Packet *p : ptr to the packet object
*
*outputs:
*  returns a reference to a serviceFlowObject. 
*  Returns a NULL if the list is empty or if there is no match
*
************************************************************************/
serviceFlowObject * CMserviceFlowMgr::getServiceFlow(Packet *p)
{
  serviceFlowObject *SFObject = NULL;
  serviceFlowObject *tmpSFObject = NULL;
  serviceFlowListElement *SFListElement = NULL;
  serviceFlowListElement *tmpPtr = NULL;
  int i;
  struct hdr_cmn *ch = HDR_CMN(p);
  struct hdr_ip *chip = HDR_IP(p);


  if (IsFrameMgmt(p))
  {
    SFObject = MGMTServiceFlow;
#ifdef TRACEME 
   printf("serviceFlowMgr::getServiceFlow(%lf): matched MGMT FLOW to pkt: (src,dst,type):%d,%d,%d), #SFs:%d\n",
		    Scheduler::instance().clock(), chip->saddr(), chip->daddr(), ch->ptype(),numberSFs);
#endif
  }
  else
  {

#ifdef TRACEME 
   printf("serviceFlowMgr::getServiceFlow(%lf): Try to match pkt: (src,dst,type):%d,%d,%d), #SFs:%d\n",
		    Scheduler::instance().clock(), chip->saddr(), chip->daddr(), ch->ptype(),numberSFs);
#endif

   //Go throught the list and look for an explicit match 
  tmpPtr = (serviceFlowListElement *) mySFList->head;
  while (tmpPtr != NULL) 
  {
#ifdef TRACEME 
      printf("serviceFlowMgr::getServiceFlow(%lf):Begin looking at the list, first element:%0x\n ",
		    Scheduler::instance().clock(),tmpPtr);
#endif
    tmpSFObject =  tmpPtr->getServiceFlow();
    if ((tmpSFObject != NULL) && (tmpSFObject->type == DATA)) 
    {
#ifdef TRACEME 
        printf("serviceFlowMgr::getServiceFlow(%lf): List Index: %d Try to match pkt with this SF: flowID:%d \n ",
		    Scheduler::instance().clock(),i, tmpSFObject->flowID);
		tmpSFObject->printSFInfo();
#endif

//$A801
         if (tmpSFObject->matchMACAddress(p,direction)) {
//         if (tmpSFObject->match(p)) {
//$A510
//         if ( (tmpSFObject->match(p,DOWNSTREAM)) || (tmpSFObject->match(p,UPSTREAM))) {
#ifdef TRACEME 
           printf("serviceFlowMgr::getServiceFlow: FOUND A POSSIBLE MATCH with list element %0x \n",tmpPtr);
#endif

           SFObject = tmpSFObject;
		   break;
         }
	}
    tmpPtr = (serviceFlowListElement *)tmpPtr->next;
  }

  if (SFObject == NULL)
  {

    //IF there was not an explicit match, let's look for the default service flow
    tmpPtr = (serviceFlowListElement *) mySFList->head;
    while (tmpPtr != NULL) 
    {
#ifdef TRACEME 
      printf("serviceFlowMgr::getServiceFlow(%lf):Begin looking at the list, first element:%0x\n ",
		    Scheduler::instance().clock(),tmpPtr);
#endif
      tmpSFObject =  tmpPtr->getServiceFlow();
      if ((tmpSFObject != NULL) && (tmpSFObject->type == DATA)) 
      {

//$A510
         if ( (tmpSFObject->matchAddress(p,DOWNSTREAM)) || (tmpSFObject->matchAddress(p,UPSTREAM))) {
#ifdef TRACEME
           printf("serviceFlowMgr::getServiceFlow: FOUND A MATCH with list element %0x \n",tmpPtr);
#endif

           SFObject = tmpSFObject;
		   break;
         }
	  }
      else {
        printf("serviceFlowMgr::getServiceFlow(%lf):  No match here: tmpSFObject:%d,  tmpSFObject type:%d\n ",
		    Scheduler::instance().clock(),tmpSFObject, tmpSFObject->type );
      }
      tmpPtr = (serviceFlowListElement *)tmpPtr->next;
    }

  }
#ifdef TRACEME 
  if (SFObject == NULL)
     printf("serviceFlowMgr::getServiceFlow(%lf): DID NOT Find a match pkt\n",
		    Scheduler::instance().clock());
  else
     printf("serviceFlowMgr::getServiceFlow(%lf): FOUND a match pkt: flowID:%d\n",
		    Scheduler::instance().clock(), SFObject->flowID);
#endif

  }
  return SFObject;
}

/***********************************************************************
*function: serviceFlowObject * serviceFlowMgr::getServiceFlow(Packet *p, int type)
*
*explanation:
*  This method returns a reference to a serviceFlowObject.  This method
*  returns a NULL if the SF does not exist in the current List.
*
*
*inputs:
*   Packet *p:
*   int type:  values either DATA_PKT or MGMT_PKT.  If this is MGMT_PKT we need
*              to return the pre-existing MGMT service flow.
*
*outputs:
*   This returns a pointer to a the service flow object.
*   The caller should never delete this object and they are created/deleted
*   solely by this object.
*
************************************************************************/
serviceFlowObject * CMserviceFlowMgr::getServiceFlow(Packet *p, int type)
{
  serviceFlowObject *SFObject = NULL;

#ifdef TRACEME 
   printf("serviceFlowMgr::getServiceFlow(%lf): type:%d \n", 
		   Scheduler::instance().clock(),type);
#endif

  if (type == DATA_PKT)
  {
//$A801
    SFObject = getServiceFlow(p);
  }
  else
  {
    SFObject = MGMTServiceFlow;
  }
  return SFObject;
}

/*************************************************************************

*************************************************************************/
int  CMserviceFlowMgr::insert_pkt(Packet* p,char tbindex)
{
  int numberRemoved = 0;
  plist node,tmp,pkt_list;
  int rc = 0;
  hdr_cmn   *ch  = HDR_CMN(p);
  serviceFlowObject *mySFObj = NULL;


  
  mySFObj = getServiceFlow(p);

#ifdef TRACEME //----------------------------------------------------------------
  printf("CMserviceFlowMgr::insert_pkt(%lf): insert a packet of size:%d,  and ptype:%d, pkt ptr:%d\n",
     Scheduler::instance().clock(),ch->size(),ch->ptype(), p);
#endif //---------------------------------------------------------------------------

  if (mySFObj == NULL) 
  {
    //handle this error
//#ifdef TRACEME //-------------------------------------------------------------------------
    printf("CMserviceFlowMgr::insert_pkt(%lf): HARD ERROR: Could not find this service flow:  packet of size:%d,  and ptype:%d\n",
     Scheduler::instance().clock(),ch->size(),ch->ptype());
//#endif //------------------------------------------------------------------------------------
//    return;
    exit(1);
  }
#ifdef TRACEME //-------------------------------------------------------------------------
      printf("CMserviceFlowMgr::insert_pkt(%lf): found mySFObj (flowid:%d), packet List size:%d head:%d,  tail:%d \n ",
         Scheduler::instance().clock(),mySFObj->flowID, mySFObj->myPacketList->curListSize,mySFObj->myPacketList->head, mySFObj->myPacketList->tail);
  printf("CMserviceFlowMgr:  :printSFInfo:  ");
  mySFObj->printSFInfo();
#endif //------------------------------------------------------------------------------------

  rc = mySFObj->addPacket(p);

#ifdef TRACEME
  printf("CMserviceFlowMgr::insert_pkt(%lf): flowID:%d,  return rc: %d  size of SF Objects packet List: %d\n",
         Scheduler::instance().clock(),mySFObj->flowID,rc,mySFObj->myPacketList->getListSize());
#endif
  if (rc == FAILURE) {
     myCMMac->MACstats.total_packets_dropped++; 
     myCMMac->UpFlowTable[tbindex].total_queue_drops++;
     myCMMac->UpFlowTable[tbindex].drop_count++;
#ifdef TRACEME
      printf("CMserviceFlowMgr::insert_pkt(%lf): flowID:%d, DROP PKT  total packets dropped:%d\n",
         Scheduler::instance().clock(),mySFObj->flowID,myCMMac->MACstats.total_packets_dropped);
#endif
     myCMMac->drop(p);	
     Packet::free(p);
  }

  return rc;
}

/*************************************************************************

*************************************************************************/
int CMserviceFlowMgr::len_queue(Packet *p, char tbindex)
{
  hdr_cmn   *ch  = HDR_CMN(p);
  serviceFlowObject *mySFObj = NULL;
  int rc = 0;

  mySFObj = getServiceFlow(p);

  if (mySFObj == NULL) 
  {
    //handle this error
//#ifdef TRACEME //-------------------------------------------------------------------------
    printf("CMserviceFlowMgr(%lf):len_queue: HARD ERROR: Could not find the SF , pkt size: %d, tbindex:%d\n",
       Scheduler::instance().clock(),ch->size(),tbindex);

//#endif //------------------------------------------------------------------------------------
  return 0;
//    exit(1);
  }
  else {
    rc = mySFObj->myPacketList->curListSize;
#ifdef TRACEME //-------------------------------------------------------------------------
      printf("CMserviceFlowMgr(%lf):len_queue: SF fowID:%d return len of %d  \n ", 
            Scheduler::instance().clock(),mySFObj->flowID, mySFObj->myPacketList->curListSize);
#endif //------------------------------------------------------------------------------------
  }
  return rc;	
}

/*************************************************************************

*************************************************************************/

//$A701
/***********************************************************************
*function: serviceFlowObject *CMserviceFlowMgr::nextSF()
*
*explanation: This method decides which SF to service next and returns
*    the object.
* 
*   TODO:  Service SFs in a RR manner.  Right now we assume there's 1 SF
*             (in addition to the MGMT SF)
*inputs:
*
*outputs:
*  Returns a SFObj or a NULL
*
************************************************************************/
serviceFlowObject *CMserviceFlowMgr::nextSF()
{
  Packet *myPkt = NULL;
  int mySFcount = getNumberSFs();
  serviceFlowObject *tmpSFObject = NULL;
  serviceFlowObject *returnSFObject = NULL;
  serviceFlowListElement *tmpPtr = NULL;

#ifdef TRACEME 
    printf("serviceFlowMgr:nextSF(%lf): SF count: %d \n", Scheduler::instance().clock(),mySFcount);
#endif 

//First, pull any mgmt packets
  if (MGMTServiceFlow->myPacketCount > 0) {
#ifdef TRACEME 
        printf("serviceFlowMgr:nextSF(%lf): Found packets in MGMT SF(flowID:%d),  PKT count: %d \n", 
           Scheduler::instance().clock(),MGMTServiceFlow->flowID,MGMTServiceFlow->myPacketCount);
#endif 
       //myPkt = deque_pkt(MGMTServiceFlow);
       returnSFObject = MGMTServiceFlow;
  }
  else {
     //Go through the list and look for an explicit match 
    tmpPtr = (serviceFlowListElement *) mySFList->head;
    while (tmpPtr != NULL) 
    {
      tmpSFObject =  tmpPtr->getServiceFlow();
      if (tmpSFObject != NULL) 
      {
		if (tmpSFObject->myPacketCount > 0) {
#ifdef TRACEME 
        printf("serviceFlowMgr:nextSF(%lf): Found packets in SF %d SF PKT count: %d \n", 
           Scheduler::instance().clock(),tmpSFObject->flowID,tmpSFObject->myPacketCount);
#endif 
          // myPkt = deque_pkt(tmpSFObject);
           returnSFObject = tmpSFObject;
           break;
        }
	  }
      tmpPtr = (serviceFlowListElement *)tmpPtr->next;
    }
  }
  return returnSFObject;
}


/***********************************************************************
*function: Packet * CMserviceFlowMgr::deque_pkt(serviceFlowObject *mySFObj)
*
*explanation: This method returns a pkt from this SF
* 
*inputs:
*
*outputs:
*  Returns a pkt or a NULL
*
************************************************************************/
Packet * CMserviceFlowMgr::deque_pkt(serviceFlowObject *mySFObj)
{
  Packet* returnPkt = NULL;
  double curTime = Scheduler::instance().clock();
  struct hdr_cmn *hdr = NULL;
  int selectedPktSize = 0;


  if (mySFObj == NULL) 
  {
    //handle this error
    printf("CMserviceFlowMgr(%lf):deque_pkt: HARD ERROR: Passed a bad SF Obj Ptr \n", curTime);
//    return;
    exit(1);
  }
  else {
#ifdef TRACEME //-------------------------------------------------------------------------
      printf("CMserviceFlowMgr(%lf):deque_pkt  (flowid:%d), packet List size:%d head:%d,  tail:%d \n ",
         Scheduler::instance().clock(),mySFObj->flowID, mySFObj->myPacketList->curListSize,mySFObj->myPacketList->head, mySFObj->myPacketList->tail);
#endif //------------------------------------------------------------------------------------


    returnPkt  = mySFObj->removePacket();
//$A510
    mySFObj->updateStats(returnPkt,8,mySFObj->getBondingGroup());

    hdr = HDR_CMN(returnPkt);
    selectedPktSize = hdr->size();
    //lastDequeueTime is the timestamp when the last pkt was dequeued
    mySFObj->lastDequeueTime = curTime;
    //lastPacketTime is the timestamp when the last time a pkt was transmitted
    mySFObj->lastPacketTime = curTime;
#ifdef TRACEME 
      printf("CMserviceFlowMgr::deque_pkt(%lf): Select CURRENT flowID:%d its queue size: %d, the  pkt size:%d, pkt ptr:%d \n",
             curTime,mySFObj->flowID,mySFObj->packetsQueued(),hdr->size(),returnPkt);
#endif
  }
      
  return returnPkt;
}

/***************************************************************************
 * method:  int CMserviceFlowMgr::filterACKPackets(Packet *pkt,char tbindex)
 * params:
 *    Packet *pkt : ptr to a packet that has arrived and is to be queued
 *    char tbindex : indicates the service-flow entry to which 
 *                   the packet has been mapped 
 *    
 * returns:  returns the number of ACK packets that are remmoved from the Queue.
 *
 * description: This routine is called only if you want to do ack filtering at a CM.
 *   Before the packet is inserted in the queue (to await upstream transmission)
 *   packets that are already in the queue are scanned. All older ACK packets for this
 *   TCP connection (identified by matching the flowid, src/dst addr/port)
 *   and with a seqno less than the seqno of this ACK are removed from the queue
 *   and the packets are freed.
***************************************************************************/
int CMserviceFlowMgr::filterACKPackets(Packet *pkt, char tbindex)
{
  int numberRemoved = 0;
  plist previousNode = 0; /* needed since this is not doubly linked */
  int i = 0;
  hdr_tcp *tcph = hdr_tcp::access(pkt);
  hdr_ip* iph = hdr_ip::access(pkt);
  int seqno =  tcph->seqno();  
  plist tmp,tmp1,pkt_list;
  
  pkt_list = myCMMac->UpFlowTable[tbindex].packet_list;
  
  tmp = pkt_list;
  tmp1=0;
  
  while (tmp)
    {
      i++; /* Increment so we know  we looked at this packet on the queue */

      struct hdr_cmn *ch = HDR_CMN(tmp->pkt);

      //printf("CMserviceFlowMgr(%d)::filterACKPackets: pkt type_: %d, size_:%d\n",
      //cm_id,ch->ptype_,ch->size_);
      
      if (ch->ptype_ == PT_ACK) 
	{ 
	  /* Only do this to an ACK packet that has no data */
	  myCMMac->UpFlowTable[tbindex].totalACKs++;
	  tcph = hdr_tcp::access(tmp->pkt);
	  iph = hdr_ip::access(tmp->pkt);
	  
	  //printf(" ... ACK:    seqno: %d, flowid:%d, saddr:%d, sport:%d, daddr:%d, dport:%d \n", 
	  //tcph->seqno(), iph->flowid(),iph->saddr(),iph->sport(),iph->daddr(),iph->dport());
	  
	  /*check to see if this packet tmp->pkt is from 
	    the same cx as p. Returns a true if so...*/
	  if (packetMatch(pkt,tmp->pkt)) 
	    {
	      /*Next remove the pkt in the queue if it has a seqno less than the seqno of pkt
		note: if the seqno's are the same, this is a dup ack and we want to leave it.*/
	      
	      //printf("CMserviceFlowMgr(%d)::filterACKPackets: the packets are the same cx (queued seqno:%d)\n",
	      //cm_id,seqno);
	      
	      if (tcph->seqno() < seqno) 
		{
		  //printf("CMserviceFlowMgr(%d)::filterACKPackets: Remove the packet with seqno:%d:\n",
		  //cm_id,tcph->seqno());
		  //remove tmp->pkt
		  
		  //Check for the case of this pkt is the only packet....		  
		  if (previousNode == 0) 
		    {
		      myCMMac->UpFlowTable[tbindex].packet_list = tmp->next;
		      //printf("CMserviceFlowMgr(%d)::filterACKPackets: Set the head to point to the next\n",cm_id);
		    }
		  else 
		    {
		      /* now if this is not the first packet */
		      previousNode->next = tmp->next;

		      //printf("CMserviceFlowMgr(%d)::filterACKPackets:adjust previous to to point to the next\n",cm_id);
		    }
		  /*free the packet - does this increment a counter?  We don't want it to... */
		  myCMMac->drop(tmp->pkt);	
		  
		  /*
		    now free the node but tmp needs to be set 
		    and then delete the removed node.
		    previousNode needs to remain unchanged
		  */
		  tmp1=tmp;
		  tmp = tmp->next;
		  free(tmp1);
		  numberRemoved++;
		  myCMMac->UpFlowTable[tbindex].totalACKsFiltered++;
		  continue;
		}
	    }
	  //else
	  //printf("CMserviceFlowMgr(%d)::filterACKPackets: the packets are NOT from the same cx\n",cm_id);
	}
      previousNode = tmp; 
      tmp = tmp->next;
    }
  
  if (myCMMac->UpFlowTable[tbindex].debug)
    printf("CMserviceFlowMgr(%d)::filterACKPackets: tbindex:%c,  number removed: %d,  #looked at:%d\n",
	   cm_id,tbindex,numberRemoved,i);
  
  return numberRemoved;  
}

/***************************************************************************
 * method:  int CMserviceFlowMgr::packetMatch(Packet *pkt1, Packet *pkt2, char tbindex)
 * params:
 *    Packet *pkt1 : ptr to a one packet 
 *    Packet *pkt2 : ptr to the second packet 
 *    
 * returns:  returns a 1 is pkt2 and pkt1 are from the same flow.
 *           Else a 0.
 *
 * description: This routine compares the 2 packets and sees it their
 *  flowid, src/dst addr/port all match.  If so, they are from the same flow.
 ***************************************************************************/
int CMserviceFlowMgr::packetMatch(Packet *pkt1, Packet *pkt2)
{
  int rc  = 0;
  struct hdr_cmn *ch = HDR_CMN(pkt1);
  hdr_tcp *tcph = hdr_tcp::access(pkt1);
  hdr_ip* iph = hdr_ip::access(pkt1);
  int saddr1 = iph->saddr();
  int32_t sport1 = iph->sport();
  int daddr1 = iph->daddr();
  int32_t dport1 = iph->dport();
  int  flowid1 =  iph->flowid();
  
  ch = HDR_CMN(pkt2);
  tcph = hdr_tcp::access(pkt2);
  iph = hdr_ip::access(pkt2);
  int saddr2 = iph->saddr();
  int32_t sport2 = iph->sport();
  int daddr2 = iph->daddr();
  int32_t dport2 = iph->dport();
  int  flowid2 =  iph->flowid();
  
  if (saddr1 != saddr2)
    goto local_error;	
  
  if (daddr1 != daddr2)
    goto local_error;	
  
  if (sport1 != sport2)
    goto local_error;	
  
  if (dport1 != dport2)
    goto local_error;	
  
  if (flowid1 != flowid2)
    goto local_error;	
  
  /* if we get here,  there's a match!! */
  rc = 1;
  
 local_error:  
  return rc;  
}



//$A601
/***********************************************************************
*function: void CMserviceFlowMgr:: channelStatus(double avgChannelAccesDelay, double avgTotalSlotUtilization, double avgMySlotUtilization)
*
*explanation:
*  This is invoked periodically with channel status information.
*
*inputs:
*  double avgChannelAccesDelay
*  double avgTotalSlotUtilization
*  double avgMySlotUtilization
*
*outputs:
*
************************************************************************/
void CMserviceFlowMgr:: channelStatus(double avgChannelAccesDelay, double avgTotalSlotUtilization, double avgMySlotUtilization)
{
  serviceFlowObject *SFObject = NULL;
  serviceFlowObject *tmpSFObject = NULL;
  serviceFlowListElement *SFListElement = NULL;
  serviceFlowListElement *tmpPtr = NULL;


  if (direction == DOWNSTREAM)
    return;

#ifdef TRACEME
    printf("serviceFlowMgr::channelStatus(%lf): Number of US Service Flows in the list: %d \n",
		    Scheduler::instance().clock(), numberSFs);
#endif


  tmpPtr = (serviceFlowListElement *) mySFList->head;
  while (tmpPtr != NULL) 
  {
    tmpSFObject =  tmpPtr->getServiceFlow();
    if (tmpSFObject != NULL) 
    {
//$A510
#ifdef TRACEME
       printf("serviceFlowMgr::channelStatus(%lf):SF: send event to this  flowID:%d \n ",
		    Scheduler::instance().clock(), tmpSFObject->flowID);
#endif
      tmpSFObject-> channelStatus(avgChannelAccesDelay, avgTotalSlotUtilization,avgMySlotUtilization);
	}
    tmpPtr = (serviceFlowListElement *)tmpPtr->next;
  }

}

/***********************************************************************
*function: void CMserviceFlowMgr::channelIdleEvent()
*
*explanation:
*  This is invoked when the US channel is not fully used. 
*
*inputs:
*
*outputs:
*
************************************************************************/
void CMserviceFlowMgr::channelIdleEvent()
{
  serviceFlowObject *SFObject = NULL;
  serviceFlowObject *tmpSFObject = NULL;
  serviceFlowListElement *SFListElement = NULL;
  serviceFlowListElement *tmpPtr = NULL;


  if (direction == DOWNSTREAM)
    return;

//$A510
#ifdef TRACEME
    printf("serviceFlowMgr::channelIdleEvent(%lf): Number of US Service Flows in the list: %d \n",
		    Scheduler::instance().clock(), numberSFs);
#endif


  tmpPtr = (serviceFlowListElement *) mySFList->head;
  while (tmpPtr != NULL) 
  {
    tmpSFObject =  tmpPtr->getServiceFlow();
    if (tmpSFObject != NULL) 
    {
//$A510
#ifdef TRACEME
       printf("serviceFlowMgr::channelIdleEvent(%lf):SF: send event to this  flowID:%d \n ",
		    Scheduler::instance().clock(), tmpSFObject->flowID);
#endif
      tmpSFObject->channelIdleEvent();
	}
    tmpPtr = (serviceFlowListElement *)tmpPtr->next;
  }

}



/*************************************************************************
 ************************************************************************/
CMTSserviceFlowMgr::CMTSserviceFlowMgr()
{
#ifdef TRACEME
  printf("CMTSserviceFlowMgr::constructor: \n");
#endif

}



void CMTSserviceFlowMgr::init(int directionParam)
{

//#ifdef TRACEME
  printf("CMTSserviceFlowMgr::init: direction:%d",directionParam);
//#endif
  serviceFlowMgr::init(directionParam);
}


/***********************************************************************
*function: serviceFlowObject * CMTSserviceFlowMgr::getServiceFlow(Packet *p)
*
*explanation:
*  This method returns a reference to a serviceFlowObject.  This method
*  returns a NULL if the SF does not exist in the current List.
 *   Examines the packet and matches it to a flow in the SF List.
*    The match is based ONLY on the mac dst or src address.
*
*inputs:
*   Packet *p : ptr to the packet object
*
*outputs:
*  returns a reference to a serviceFlowObject. 
*  Returns a NULL if the list is empty or if there is no match
*
************************************************************************/
serviceFlowObject * CMTSserviceFlowMgr::getServiceFlow(Packet *p)
{
  serviceFlowObject *SFObject = NULL;
  serviceFlowObject *tmpSFObject = NULL;
  serviceFlowListElement *SFListElement = NULL;
  serviceFlowListElement *tmpPtr = NULL;
  int i;
  struct hdr_cmn *ch = HDR_CMN(p);
  struct hdr_ip *chip = HDR_IP(p);


  if (IsFrameMgmt(p))
  {
    SFObject = MGMTServiceFlow;
#ifdef TRACEME 
   printf("CMTSserviceFlowMgr::getServiceFlow(%lf): matched MGMT FLOW to pkt: (src,dst,type):%d,%d,%d), #SFs:%d\n",
		    Scheduler::instance().clock(), chip->saddr(), chip->daddr(), ch->ptype(),numberSFs);
#endif
  }
  else
  {

#ifdef TRACEME 
   printf("serviceFlowMgr::getServiceFlow(%lf): Try to match pkt: (src,dst,type):%d,%d,%d), #SFs:%d\n",
		    Scheduler::instance().clock(), chip->saddr(), chip->daddr(), ch->ptype(),numberSFs);
#endif

   //Go throught the list and look for an explicit match 
  tmpPtr = (serviceFlowListElement *) mySFList->head;
  while (tmpPtr != NULL) 
  {
#ifdef TRACEME 
      printf("serviceFlowMgr::getServiceFlow(%lf):Begin looking at the list, first element:%0x\n ",
		    Scheduler::instance().clock(),tmpPtr);
#endif
    tmpSFObject =  tmpPtr->getServiceFlow();
    if ((tmpSFObject != NULL) && (tmpSFObject->type == DATA)) 
    {
#ifdef TRACEME 
        printf("serviceFlowMgr::getServiceFlow(%lf): List Index: %d Try to match pkt with this SF: flowID:%d \n ",
		    Scheduler::instance().clock(),i, tmpSFObject->flowID);
		tmpSFObject->printSFInfo();
#endif

//$A801
         if (tmpSFObject->matchMACAddress(p,direction)) {
//         if (tmpSFObject->match(p)) {
//$A510
//         if ( (tmpSFObject->match(p,DOWNSTREAM)) || (tmpSFObject->match(p,UPSTREAM))) {
#ifdef TRACEME 
           printf("serviceFlowMgr::getServiceFlow: FOUND A POSSIBLE MATCH with list element %0x \n",tmpPtr);
#endif

           SFObject = tmpSFObject;
		   break;
         }
	}
    tmpPtr = (serviceFlowListElement *)tmpPtr->next;
  }

  if (SFObject == NULL)
  {

    //IF there was not an explicit match, let's look for the default service flow
    tmpPtr = (serviceFlowListElement *) mySFList->head;
    while (tmpPtr != NULL) 
    {
#ifdef TRACEME 
      printf("serviceFlowMgr::getServiceFlow(%lf):Begin looking at the list, first element:%0x\n ",
		    Scheduler::instance().clock(),tmpPtr);
#endif
      tmpSFObject =  tmpPtr->getServiceFlow();
      if ((tmpSFObject != NULL) && (tmpSFObject->type == DATA)) 
      {

//$A510
         if ( (tmpSFObject->matchAddress(p,DOWNSTREAM)) || (tmpSFObject->matchAddress(p,UPSTREAM))) {
#ifdef TRACEME
           printf("serviceFlowMgr::getServiceFlow: FOUND A MATCH with list element %0x \n",tmpPtr);
#endif

           SFObject = tmpSFObject;
		   break;
         }
	  }
      else {
        printf("serviceFlowMgr::getServiceFlow(%lf):  No match here: tmpSFObject:%d,  tmpSFObject type:%d\n ",
		    Scheduler::instance().clock(),tmpSFObject, tmpSFObject->type );
      }
      tmpPtr = (serviceFlowListElement *)tmpPtr->next;
    }

  }
#ifdef TRACEME 
  if (SFObject == NULL)
     printf("serviceFlowMgr::getServiceFlow(%lf): DID NOT Find a match pkt\n",
		    Scheduler::instance().clock());
  else
     printf("serviceFlowMgr::getServiceFlow(%lf): FOUND a match pkt: flowID:%d\n",
		    Scheduler::instance().clock(), SFObject->flowID);
#endif

  }
  return SFObject;
}

/***********************************************************************
*function: serviceFlowObject * serviceFlowMgr::getServiceFlow(Packet *p, int type)
*
*explanation:
*  This method returns a reference to a serviceFlowObject.  This method
*  returns a NULL if the SF does not exist in the current List.
*
*
*inputs:
*   Packet *p:
*   int type:  values either DATA_PKT or MGMT_PKT.  If this is MGMT_PKT we need
*              to return the pre-existing MGMT service flow.
*
*outputs:
*   This returns a pointer to a the service flow object.
*   The caller should never delete this object and they are created/deleted
*   solely by this object.
*
************************************************************************/
serviceFlowObject * CMTSserviceFlowMgr::getServiceFlow(Packet *p, int type)
{
  serviceFlowObject *SFObject = NULL;

#ifdef TRACEME 
   printf("CMTSserviceFlowMgr::getServiceFlow(%lf): type:%d \n", 
		   Scheduler::instance().clock(),type);
#endif

  if (type == DATA_PKT)
  {
//$A801
    SFObject = getServiceFlow(p);
  }
  else
  {
    SFObject = MGMTServiceFlow;
  }
  return SFObject;
}



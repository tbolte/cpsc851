/*****************************************************************************
 *       This file contains Timer class definitions. 
 *
 * Revisions:
 *   $A301:  Move rxintr_ , txintr_  and channelNumber_ to base class. 
 *           Add RxPktTimer start method so it uses the rxintr_ and not
 *           the packet.
 *  $A304:   
 *     Add methods to init the timer with its mac pointer and its channel number
 *  $A805: 9-29-2010 - added support for resourceOptimization Timer
 *
 *
 ****************************************************************************/

#ifndef ns_mac_docsistimers_h
#define ns_mac_docsistimers_h

#include "docsisDebug.h"
//#define TRACEME 1
/*=======================CLASS DEFINITIONS============================*/

/*========================TIMER CLASSES===============================*/
//JJM WRONG :  remove any timers used by the medim and place in the channelTimers class
class medium;
class MacDocsis;
class NodeListData;
class reseqFlowListElement;
class reseqMgr;
//$A805
class resourceOptimizer;

/*************************************************************************
Base class for all the timer classes
*************************************************************************/
class MacDocsisTimer : public Handler 
{
 public: 
  MacDocsisTimer() 
    {
      busy_ =  paused_ = 0; stime = rtime = 0.0; 
	}
  MacDocsisTimer(MacDocsis* m) : mac(m)  
    {
      busy_ =  paused_ = 0; stime = rtime = 0.0; 
    }
    
    void setMacPointer(MacDocsis *macPtr);
    void setMedPointer(medium *medium);
    void setChannelNumber(int channelNumber);
	void setNode(NodeListData *node);

    virtual void handle(Event *e) {} ;
    
    void start(Packet *e, double time);
    virtual void stop(Packet *e);
    
    inline int busy(void) 
      { 
	return busy_; 
      }
    
    inline double expire(void) 
      {
	return((stime + rtime) - Scheduler::instance().clock());
      }
    
//$A301
    int		channelNumber_;
    Event txintr_; 
    Event rxintr_; 
    MacDocsis 	*mac;

 protected:
    int		busy_;
    int 	paused_;
    Event	intr;
    double	stime;	// start time
    double	rtime;	// remaining time
    //$A300
    Packet *p;
    medium *myMedium;
	NodeListData *element;
};

/*=================== Timers common to CMTS & CM =======================*/

/*************************************************************************
 Timers to control packet sending and receiving time.
*************************************************************************/
class RxPktDocsisTimer : public MacDocsisTimer 
{
 public:
  RxPktDocsisTimer() : MacDocsisTimer() 
  {
#ifdef TRACEME
  printf("RxPktDocsisTimer: constructor\n");
  fflush(stdout);
#endif
  }

  RxPktDocsisTimer(MacDocsis *m) : MacDocsisTimer(m) 
  {
#ifdef TRACEME
  printf("RxPktDocsisTimer: constructor\n");
  fflush(stdout);
#endif
  }
  RxPktDocsisTimer(MacDocsis *m, int channelNumber) : MacDocsisTimer(m) 
  {
    channelNumber_ = channelNumber;
#ifdef TRACEME
  printf("RxPktDocsisTimer: constructor, channelNumber: %d\n",channelNumber_);
  fflush(stdout);
#endif

  }

//$A301
    void start(Packet *p,  double time);
    void handle(Event *e);
};

/*************************************************************************

TODO:  Looks like we won't need this
*************************************************************************/
class TxPktDocsisTimer : public MacDocsisTimer 
{  
 public:
  TxPktDocsisTimer() : MacDocsisTimer() 
  {
#ifdef TRACEME
  printf("TxPktDocsisTimer: constructor\n");
  fflush(stdout);
#endif
  }

  TxPktDocsisTimer(MacDocsis *m) : MacDocsisTimer(m) 
  {
#ifdef TRACEME
  printf("TxPktDocsisTimer: constructor\n");
  fflush(stdout);
#endif
  }
    
    void start(Packet *p,  double time);
    void handle(Event *e);
};


class ChTxPktDocsisTimer : public MacDocsisTimer
{
public :
  ChTxPktDocsisTimer() : MacDocsisTimer() 
  {
#ifdef TRACEME
  printf("ChTxPktDocsisTimer: constructor\n");
  fflush(stdout);
#endif
  }

  ChTxPktDocsisTimer(MacDocsis *m) : MacDocsisTimer(m) 
  {
#ifdef TRACEME
  printf("ChTxPktDocsisTimer: constructor\n");
  fflush(stdout);
#endif
  }
    
	    
    void start(Packet *p,  double time);
    void handle(Event *e);
};

	
/*==================== End Timers common to CMTS & CM ==================*/



/*======================== CMTS timers================================= */

/*************************************************************************

*************************************************************************/
class CmtsTxPktDocsisTimer : public MacDocsisTimer 
{
  
 public:
  CmtsTxPktDocsisTimer() : MacDocsisTimer() 
  {
#ifdef TRACEME
  printf("CmtsTxPktDocsisTimer: constructor %x\n", this);
  fflush(stdout);
#endif
  }

  CmtsTxPktDocsisTimer(MacDocsis *m) : MacDocsisTimer(m) 
  {
#ifdef TRACEME
  printf("CmtsTxPktDocsisTimer: constructor\n");
  fflush(stdout);
#endif
  }

  CmtsTxPktDocsisTimer(MacDocsis *m, int channelNumber) : MacDocsisTimer(m) 
  {
    channelNumber_ = channelNumber;
#ifdef TRACEME
  printf("CmtsTxPktDocsisTimer: constructor, channelNumber: %d\n",channelNumber_);
  fflush(stdout);
#endif
  }
    
    void handle(Event *e);
//    void start(Packet *e, Packet *p,  double time);
    void start(Packet *p,  double time);
 protected:
};

class CmTxPktDocsisTimer : public MacDocsisTimer 
{
  
 public:
  CmTxPktDocsisTimer() : MacDocsisTimer() 
  {
#ifdef TRACEME
  printf("CmTxPktDocsisTimer: constructor %x\n", this);
  fflush(stdout);
#endif
  }

  CmTxPktDocsisTimer(MacDocsis *m) : MacDocsisTimer(m) 
  {
#ifdef TRACEME
  printf("CmTxPktDocsisTimer: constructor\n");
  fflush(stdout);
#endif
  }
  CmTxPktDocsisTimer(MacDocsis *m, int channelNumber) : MacDocsisTimer(m) 
  {
    channelNumber_ = channelNumber;
#ifdef TRACEME
  printf("CmTxPktDocsisTimer: constructor, channelNumber: %d\n",channelNumber_);
  fflush(stdout);
#endif
  }
    
    void handle(Event *e);
//    void start(Packet *e, Packet *p,  double time);
    void start(Packet *p,  double time);
 protected:
};

/*************************************************************************
Timers to schedule transmition of MAPS.
*************************************************************************/
class MapDocsisTimer : public MacDocsisTimer 
{
 public:
  MapDocsisTimer(MacDocsis *m) : MacDocsisTimer(m) {}
    
    void handle(Event *e);
};

/*************************************************************************
Timers to schedule transmition of Management messages
*************************************************************************/
class CmtsUcdDocsisTimer : public MacDocsisTimer 
{
 public:
  CmtsUcdDocsisTimer(MacDocsis *m) : MacDocsisTimer(m) {}
    
    void handle(Event *e);
};

/*************************************************************************
  this is the sequence hold timer (docsis 3.0) TOMMY
*************************************************************************/
class CmSeqTimer : public MacDocsisTimer 
{
 public:
  CmSeqTimer(MacDocsis *m) : MacDocsisTimer(m) {}
  
    void handle(Event *e);
};


/*************************************************************************

*************************************************************************/
class CmtsRngDocsisTimer : public MacDocsisTimer 
{
 public:
  CmtsRngDocsisTimer(MacDocsis *m) : MacDocsisTimer(m) {}
  
    void handle(Event *e);
};

/*************************************************************************

*************************************************************************/
class CmtsSyncDocsisTimer : public MacDocsisTimer 
{
 public:
  CmtsSyncDocsisTimer(MacDocsis *m) : MacDocsisTimer(m) {}
    
    void handle(Event *e);
};

//#ifdef RATE_CONTROL//---------------------------------------------------
/*************************************************************************

*************************************************************************/
class CmtsTokenDocsisTimer : public MacDocsisTimer 
{
 public:
  CmtsTokenDocsisTimer(MacDocsis *m) : MacDocsisTimer(m) {}
    
    void handle(Event *e);
};

class CmtsSFTokenDocsisTimer : public MacDocsisTimer
{
public:
    CmtsSFTokenDocsisTimer(MacDocsis *m) : MacDocsisTimer(m) {}

    void handle(Event *e);
};
//#endif//----------------------------------------------------------------

/*======================== END CMTS timers============================= */


/*======================== CM timers=================================== */

/*************************************************************************
Timers to schedule transmition of Management messages
*************************************************************************/
class CmRngDocsisTimer : public MacDocsisTimer 
{
 public:
  CmRngDocsisTimer(MacDocsis *m) : MacDocsisTimer(m) {}
    
    void handle(Event *e);
};
class CmStatusTimer : public MacDocsisTimer 
{
 public:
  CmStatusTimer(MacDocsis *m) : MacDocsisTimer(m) {}
    
    void handle(Event *e);
};


/*************************************************************************
SEND timer..(per service-flow timer) 
*************************************************************************/
class CmServiceFlowSendTimer : public MacDocsisTimer 
{
 public:
  CmServiceFlowSendTimer(MacDocsis *m) : MacDocsisTimer(m) {}
    
    void handle(Event *e);
};

/*************************************************************************
REQUEST timer..(per service-flow timer)
*************************************************************************/
class CmServiceFlowRequestTimer : public MacDocsisTimer 
{
 public:
  CmServiceFlowRequestTimer(MacDocsis *m) : MacDocsisTimer(m) {}
    
    void handle(Event *e);
};

/*======================== End CM timers=================================== */

/*************************************************************************
 Timer for resequencing 
*************************************************************************/

class reseqTimer : public MacDocsisTimer
{

 public:
  reseqTimer(reseqMgr *, reseqFlowListElement *myElement);

  ~reseqTimer();

    void start(Event *e,  double time);
    void handle(Event *e);
    
 protected:
    reseqFlowListElement *myElement;
    reseqMgr *myReseqMgr;
};

/*************************************************************************
 Timer for  optimization
*************************************************************************/
//$A805
class optimizationTimer : public MacDocsisTimer
{

 public:
  optimizationTimer(resourceOptimizer *myOptimizer);

  ~optimizationTimer();

    void start(Event *e,  double time);
    void handle(Event *e);
    
 protected:
    resourceOptimizer *myOptimizer;
};

#endif /* __mac_docsistimers_h__ */

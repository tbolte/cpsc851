/***************************************************************************
 * Module: packetMonitor
 *
 * Explanation:
 *   This file contains the base class packetMonitor.
 *   The monitor maintains two types of stats.  
 *       Long Term stats:  the counters are never cleared.
 *       Running stats:  the idea is that an external program can periodically
 *             obtain the current count and then can clear the counters.
 *
 * Revisions:
 *
 *  TODO:
************************************************************************/

#ifndef ns_packetMonitor_h
#define ns_packetMonitor_h

#include "packet.h"
#include "rtp.h"
#include "ping.h"
#include "loss_monitor.h"
#include "hdr-docsis.h"


#define  packetMonitor_NO_TRACE 0
#define  packetMonitor_TRACE_TO_STDOUT 1
#define  packetMonitor_TRACE_TO_FILE 2
#define  packetMonitor_TRACE_TO_BINARY_FILE 4


#define TRACE_STRING_LENGTH 255

struct packetStatsType
{
  double  byteCount;
  u_int32_t packetCount;

  double runningByteCount;
  u_int32_t runningPacketCount;

};


struct PREC_T
{
  double tstamp;
  unsigned int direction;

  unsigned int packet_t;    //defined in packet.h

  unsigned int saddr;
  unsigned int daddr;
  unsigned short len;

  unsigned int seq;
  unsigned int ack;
  unsigned int off_win;
  unsigned int tcp_flags;
  unsigned int seglen;
  unsigned short source;
  unsigned short dest;

  unsigned int snd_una;
  unsigned int snd_nxt;
  unsigned int rcv_nxt;
  unsigned int snd_wnd;
  unsigned int snd_cwnd;

};

//define for direction
#define PT_SEND_DIRECTION 0
#define PT_RECV_DIRECTION 1
#define PT_DUPLEX_DIRECTION 2

/*************************************************************************
 ************************************************************************/
class packetMonitor
{
public:
  packetMonitor();
  packetMonitor(int direction);
  packetMonitor(int direction, u_int32_t traceLevel,  char *TEXToutfile, char  *BINARYoutfile);

  ~packetMonitor();
  void init();
  void init(int id, int direction);
  void setTraceLevel(u_int32_t traceLevel);
  void setTextTraceFile(char *tFileName);
  void setBinaryTraceFile(char *bFileName);

  //returns the trace in a string
 int packetTrace(Packet *p, char *traceOutput, int outputSize);

 int packetTrace(Packet *p);
 int packetTrace(Packet *p,char *preText);

 void clearStats();
 void getStats(struct packetStatsType *callersStats);

 void updateStats(Packet *p);
 void clearRunningStats();
 virtual void printStatsSummary();

 void getBinaryTraceSummary();
  int numberID;
  int direction;     // either SEND, RECV, or DUPLEX
  u_int32_t traceLevel;    // bitmask sets where trace information goes:
                     //0: no tracing (noop), 1: stdout; 2:files; 4:tcpdump binary file
  int state;         //1: initialized;
  FILE *textFile;
  FILE *binaryFile;
  u_int32_t binaryByteCount;
  u_int32_t binaryCount;
  void binaryPacketTrace(Packet *p);
  struct packetStatsType myStats;

private:




};


#endif /* __ns_packetMonitor_h__ */

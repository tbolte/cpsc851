
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <tclcl.h>
#include "globalDefines.h"

#include "schedulerParams.h"

DSSchedulerParams* DSSchedulerParams::_instance = 0;

void DSSchedulerParams::init() {
    char params[1024];
    char *pToken;

    if (_instance != 0) {
        //fprintf(stderr, "Attempted to initialize DSSchedulerParams more than once!\n");
        return;
    }
    
    _instance = new DSSchedulerParams();
    
    Tcl& tcl = Tcl::instance();
    tcl.evalc("if [info exists DSSchedMode] { set DSSchedMode } else { set DSSchedMode -1 }");
    //fprintf(stderr, "this is what I get: %s\n", tcl.result());
    _instance->mode = atoi(tcl.result());
    //fprintf(stderr, "my mode is %d\n", _instance->mode);
    
    switch (_instance->mode) {
        case HOSTBASED:
            fprintf(stderr, "My mode is %d -- not implemented yet\n", _instance->mode);
            break;
            
        case FAIRSHARE:
        case FAIRSHAREAQM:
            tcl.evalc("if [info exists DSSchedModeFairshareParams] { set DSSchedModeFairshareParams }");
            strcpy(params, tcl.result());
            if (strlen(params) != 0) {
                printf("Customized fairshare params in use\n");
                pToken = strtok(params, " \t");
                if (strcasecmp(pToken, "fairshare") != 0) {
                    fprintf(stderr, "Wrong fairshare parameter specification!\n");
                    break;
                }

                while ((pToken = strtok(NULL, " \t")) != NULL) {
                    //fprintf(stderr, "%s\n", pToken);
                    if (strncasecmp(pToken, "MonitorPeriod=", strlen("MonitorPeriod=")) == 0) {
                        sscanf(pToken+strlen("MonitorPeriod="), "%d", &_instance->monitorPeriod);
                    } else if (strncasecmp(pToken, "PortCapacity=", strlen("PortCapacity=")) == 0) {
                        sscanf(pToken+strlen("PortCapacity="), "%f", &_instance->portCapacity);
                    } else if (strncasecmp(pToken, "NearCongestionStateThreshold=", strlen("NearCongestionStateThreshold=")) == 0) {
                        sscanf(pToken+strlen("NearCongestionStateThreshold="), "%f", &_instance->nearCongestionStateThreshold);
                    } else if (strncasecmp(pToken, "ExtendedHighConsumptionStateThreshold=", strlen("ExtendedHighConsumptionStateThreshold=")) == 0) {
                        sscanf(pToken+strlen("ExtendedHighConsumptionStateThreshold="), "%f", &_instance->extendedHighConsumptionStateThreshold);
                    } else if (strncasecmp(pToken, "ExtendedHighConsumptionStateExitThreshold=", strlen("ExtendedHighConsumptionStateExitThreshold=")) == 0) {
                        sscanf(pToken+strlen("ExtendedHighConsumptionStateExitThreshold="), "%f", &_instance->extendedHighConsumptionStateExitThreshold);
                    } else if (strncasecmp(pToken, "SubscriberProvisionedBW=", strlen("SubscriberProvisionedBW=")) == 0) {
                        sscanf(pToken+strlen("SubscriberProvisionedBW="), "%f", &_instance->subscriberProvisionedBW);
                    } else if (strncasecmp(pToken, "PriorityAllocation=", strlen("PriorityAllocation=")) == 0) {
                        sscanf(pToken+strlen("PriorityAllocation="), "%f", &_instance->priorityAlloc);
                    } else if (strncasecmp(pToken, "Adaptive=", strlen("Adaptive=")) == 0) {
                        sscanf(pToken+strlen("Adaptive="), "%d", &_instance->adaptiveFS);
                    } else if (strncasecmp(pToken, "TargetQL=", strlen("TargetQL=")) == 0) {
                        sscanf(pToken+strlen("TargetQL="), "%f", &_instance->targetQL);
                    } else if (strncasecmp(pToken, "TargetDL=", strlen("TargetDL=")) == 0) {
                        sscanf(pToken+strlen("TargetDL="), "%f", &_instance->targetDL);
                    } else if (strncasecmp(pToken, "Multitier=", strlen("Multitier=")) == 0) {
                        sscanf(pToken+strlen("Multitier="), "%d", &_instance->multitier);
                    } else {
                        fprintf(stderr, "Unknown Fairshare Parameter: %s\n", pToken);
                    }
                }

            } else {
                printf("Default fairshare params in use\n");
            }

            // check if those values are reasonable
            if (_instance->monitorPeriod <= 0) {
                fprintf(stderr, "Bad monitor period (<=0)!\n");
            }
            printf("MonitorPeriod = %d\n", _instance->monitorPeriod);
            printf("PortCapacity = %.1f\n", _instance->portCapacity);
            if (_instance->nearCongestionStateThreshold <= 0.5) {
                fprintf(stderr, "Bad near congestion state threshold (<=0.5)!\n");
            }
            if (_instance->nearCongestionStateThreshold > 0.98) {
                fprintf(stderr, "Near congestion state threshold too large (>0.98)!\n");
            }
            printf("NearCongestionStateThreshold = %.2f\n", _instance->nearCongestionStateThreshold);
            if (_instance->extendedHighConsumptionStateThreshold <= 0.5) {
                fprintf(stderr, "Bad extended high consumption state threshold (<=0.5)!\n");
            }
            if (_instance->extendedHighConsumptionStateThreshold > 0.98) {
                fprintf(stderr, "Extended high consumption state threshold too large (>0.98)!\n");
            }
            printf("ExtendedHighConsumptionStateThreshold = %.2f\n", _instance->extendedHighConsumptionStateThreshold);
            if (_instance->extendedHighConsumptionStateExitThreshold <= 0.2) {
                fprintf(stderr, "Bad extended high consumption state exit threshold (<=0.2)!\n");
            }
            if (_instance->extendedHighConsumptionStateExitThreshold >= _instance->extendedHighConsumptionStateThreshold-0.1) {
                fprintf(stderr, "Extended high consumption state threshold too large (>=extendedHighConsumptionStateThreshold-0.1)!\n");
            }
            printf("ExtendedHighConsumptionStateExitThreshold = %.2f\n", _instance->extendedHighConsumptionStateExitThreshold);
            if (_instance->subscriberProvisionedBW <= 2.0) {
                fprintf(stderr, "Bad subscriber provisioned BW (<=2.0Mbps)!\n");
            }
            printf("Subscriber provisioned BW = %.2f\n", _instance->subscriberProvisionedBW);
            if (_instance->priorityAlloc < 0.8) {
                fprintf(stderr, "Bad priority allocation (<0.8)!\n");
            }
            if (_instance->priorityAlloc > 0.99) {
                fprintf(stderr, "Priority allocation too large (>0.99)!\n");
            }
            printf("PriorityAllocation = %.2f\n", _instance->priorityAlloc);
            if (_instance->adaptiveFS > 1 || _instance->adaptiveFS < 0) {
                fprintf(stderr, "Adaptive should be set to 0 or 1!\n");
            }
            printf("Adaptive = %d\n", _instance->adaptiveFS);
            if (_instance->targetQL < 0.0) {
                fprintf(stderr, "TargetQL cannot be negative!\n");
            }
            printf("TargetQL = %.2f\n", _instance->targetQL);
            if (_instance->targetDL < 0.0) {
                fprintf(stderr, "TargetDL cannot be negative!\n");
            }
            printf("TargetDL = %.4f\n", _instance->targetDL);
            if (_instance->multitier > 1 || _instance->multitier < 0) {
                fprintf(stderr, "Multitier should be set to 0 or 1!\n");
            }
            printf("Multitier = %d\n", _instance->multitier);
            break;

        case BROADBANDAQM:
            tcl.evalc("if [info exists DSSchedModeBROADBANDParams] { set DSSchedModeBROADBANDParams }");
            strcpy(params, tcl.result());
            if (strlen(params) != 0) {
                printf("Customized BROADBAND AQM params in use\n");
                pToken = strtok(params, " \t");
                if (strcasecmp(pToken, "BROADBANDAQM") != 0) {
                    fprintf(stderr, "Wrong BROADBANDAQM parameter specification!\n");
                    break;
                }

                while ((pToken = strtok(NULL, " \t")) != NULL) {
                    //fprintf(stderr, "%s\n", pToken);
                    if (strncasecmp(pToken, "MonitorPeriod=", strlen("MonitorPeriod=")) == 0) {
                        sscanf(pToken+strlen("MonitorPeriod="), "%d", &_instance->monitorPeriod);
                    } else if (strncasecmp(pToken, "SubscriberProvisionedBW=", strlen("SubscriberProvisionedBW=")) == 0) {
                        sscanf(pToken+strlen("SubscriberProvisionedBW="), "%f", &_instance->subscriberProvisionedBW);
                    } else if (strncasecmp(pToken, "PriorityAllocation=", strlen("PriorityAllocation=")) == 0) {
                        sscanf(pToken+strlen("PriorityAllocation="), "%f", &_instance->priorityAlloc);
                    } else if (strncasecmp(pToken, "HiPriQueueDelay=", strlen("HiPriQueueDelay=")) == 0) {
                        sscanf(pToken+strlen("HiPriQueueDelay"), "%d", &_instance->HiPriQueueDelayTarget);
                    } else if (strncasecmp(pToken, "LowPriQueueDelay=", strlen("LowPriQueueDelay=")) == 0) {
                        sscanf(pToken+strlen("LowPriQueueDelay"), "%d", &_instance->LowPriQueueDelayTarget);
                    } else if (strncasecmp(pToken, "HiPriInterval=", strlen("HiPriInterval=")) == 0) {
                        sscanf(pToken+strlen("HiPriInterval"), "%d", &_instance->HiPriInterval);
                    } else if (strncasecmp(pToken, "LowPriInterval=", strlen("LowPriInterval=")) == 0) {
                        sscanf(pToken+strlen("LowPrinterval"), "%d", &_instance->LowPriInterval);
                    } else if (strncasecmp(pToken, "Adaptive=", strlen("Adaptive=")) == 0) {
                        sscanf(pToken+strlen("Adaptive="), "%d", &_instance->adaptiveFS);
                    } else {
                        fprintf(stderr, "Unknown BROADBANDAQM Parameter: %s\n", pToken);
                    }
              
                }
            } else {
                printf("Default BROADBANDAQM params in use\n");
            }

            // check if those values are reasonable
            if (_instance->monitorPeriod <= 0) {
                fprintf(stderr, "Bad monitor period (<=0)!\n");
            }
            printf("MonitorPeriod = %d\n", _instance->monitorPeriod);
//          if (_instance->nearCongestionStateThreshold <= 0.5) {
//              fprintf(stderr, "Bad near congestion state threshold (<=0.5)!\n");
//          }
            printf("Subscriber provisioned BW = %.2f\n", _instance->subscriberProvisionedBW);
            if (_instance->priorityAlloc < 0.8) {
                fprintf(stderr, "Bad priority allocation (<0.8)!\n");
            }
            if (_instance->priorityAlloc > 0.99) {
                fprintf(stderr, "Priority allocation too large (>0.99)!\n");
            }
            printf("PriorityAllocation = %.2f\n", _instance->priorityAlloc);
            if (_instance->adaptiveFS > 1 || _instance->adaptiveFS < 0) {
                fprintf(stderr, "Adaptive should be set to 0 or 1!\n");
            }
            printf("HiPriQueueDelayTarget = %f\n", _instance->HiPriQueueDelayTarget);
            printf("LowPriQueueDelayTarget = %f\n", _instance->LowPriQueueDelayTarget);
            printf("HiPriInterval = %f\n", _instance->HiPriInterval);
            printf("LowPriInterval = %f\n", _instance->LowPriInterval);
            break;
   
        default:
            printf("My mode is %d -- use default params\n", _instance->mode);
            break;
    }

}


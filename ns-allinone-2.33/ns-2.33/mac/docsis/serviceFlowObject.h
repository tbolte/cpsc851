/***************************************************************************
 * Module: serviceFlowObject
 *
 * Explanation:
 *   This file contains the class definition of a service flow.
 * 
 *
 * Revisions:
 *   $A806:  Added hierarchical scheduling
 *   $A900 : add time monitors for while in state0 or 1
 *   $A902 : To fix aggregate SF final service/arrival rate calculation....
 *   $A904 : monitor rate of FS state transitions 
 *
 *  TODO:
 *   -Move the rate control support into this object
 *
************************************************************************/

#ifndef ns_serviceFlowObject_h
#define ns_serviceFlowObject_h

#include "packet.h"
#include "rtp.h"
#include "hdr-docsis.h"
#include "globalDefines.h"
#include "packetListElement.h"
#include "mac-docsis.h"
#include "mac-docsistimers.h"
#include "serviceFlowMgr.h"


class ListObj;
class AQMObj;
class bondingGroupObject;


//Set this to -1 if you don't want a special printf trace
//This also determines which flow will create the traceAQM.out file
//#define TRACED_SFID 5
//This is the DS UDP flow for the case of 67 active CMs
//  For this case, tcp web flows are 61-71
#define TRACED_SFID 59
#define TRACED_SFID2 6000000
#define TRACED_AggSFID 1000
#define TRACED_AggSFID1 1000
#define TRACED_AggSFID2 1001

#define SERVICE_FLOW_OBJECT_MAX_PACKETS 20

#define FLOW_WILDCARD 255

//$A902
#define SINGLE_SF 1
#define AGGREGATED_SF 2

struct serviceFlowObjectStatsType
{
  //
  u_int32_t  numberPacketArrivals;    //Only to be set once when a new packet arrives
  double     numberByteArrivals;

//this is number of bytes counted per channel so effectively it's numberFramesServiced
  u_int32_t  numberPacketsServiced2; 
 //Next two count number of bytes and pkts that actually arrive and get forwarded
  double     numberBytesServiced;
  u_int32_t  numberPacketsServiced;
  double     TotalBytesDropped;
  u_int32_t  TotalPacketsDropped;
  u_int32_t  numberAddPackets;
  u_int32_t  numberRemovePackets;
  u_int32_t  numberMatchCalls;
  u_int32_t  numberMatchAddressCalls;
  u_int32_t  channelArray[MAX_CHANNELS];
  u_int32_t  bondingGroupArray[MAX_BONDING_GROUPS];
  double     avgQueueDelay;
  double     avgLossRate;
  double     lastUpdateTime;
  double     firstUpdateTime;
  double     largestServiceRateBurst;
//$A900
  double     timeSpentInState1;
  double     timeSpentInState2;
  double     lastStateChange;
//$A904
  u_int32_t  countToState1to2;
  u_int32_t  countToState2to1;
  unsigned int numberPriorityChanges;
  unsigned int numberAtHiPriority;  // in unit of monitor time period
  unsigned int numberAtLoPriority;  // in unit of monitor time period
};

/*************************************************************************
 ************************************************************************/
class serviceFlowObject
{
public:
  serviceFlowObject();
  serviceFlowObject(int macaddr,int32_t src_ip, int32_t dst_ip, packet_t pkt_type, u_int16_t serviceFlowID,  int myBGIDParam);

  virtual ~serviceFlowObject();

  void constructionInit();
  virtual void init(int direction, MacDocsis *myMac,serviceFlowMgr *mySFMgrParam);

  virtual void setQueueBehavior(int maxQSize, int QType, int priority, int QParam1, int QParam2, int QParam3, double QParam4, double QParam5);
  virtual int match(Packet* p);
  virtual int matchMACAddress(Packet* p, int direction);
  virtual int matchAddress(Packet* p);
  virtual int matchAddress(Packet* p, int direction);
//$A801
  virtual int matchPktType(Packet* p);

  u_int16_t getNextPSN();
  void setNextPSN(u_int16_t nextPSN);
  int getDirection();

  void setMacHandle(MacDocsis *myMacParam);
  virtual void printSFInfo();
  virtual void newArrivalUpdate(Packet *p);
  virtual void updateStats(Packet *p, int channelNumber, int bondingGroupNumber);
  virtual void getStats(struct serviceFlowObjectStatsType *callersStats);
  virtual void printStatsSummary(char *outputFile);
  virtual void printShortSummary();
  int getBondingGroup();

  virtual void channelIdleEvent();
  virtual void channelStatus(double avgChannelAccesDelay, double avgTotalSlotUtilization, double avgMySlotUtilization);

  u_int16_t nextPSN;
  int direction;   //0 for UPSTREAM, 1 for DOWNSTREAM
  int type;        //0 if this flow is user data (DATA_PKT) 
                   // else 1 if this flow represents a
                   // management flow
  struct flow_classifier classifier;
  int macaddr;     //mac address of the CM associated with the flow
  u_int16_t flowID;
//$A801
  u_int16_t dsid;

  MacDocsis *myMac;
  ListObj *myPacketList;
  
  int monitorDropCount;
  int monitorPacketArrivalCount;

  int qnp_;
  int qnb_;
  int max_qnp_;                  /* Max # of pkts observed */
  int max_qnb_;                  /* Max # of bytes observed */
  int min_qnp_;                  /* Min # of pkts  observed */
  int min_qnb_;                  /* Min # of bytes observed */
  int queue_total_bytes_in, queue_total_bytes_out;  /*used for final 
                              utilization stats*/
  int  qlim_;                    /* max queue size in packets */
  double lastDumpTime;
  double lastUtilDumpTime;


  virtual int addPacket(Packet *p);
  virtual int insertPacketInTimeOrder(Packet *p, double packetEntryTime, int flowID);
  virtual Packet *removePacket();
  virtual Packet *getPacket(int i);
  virtual int packetsQueued(void);

  // rate control
//JJM MOVED
  double tokens_;
  int tokenqlen_;
  double rate_;
  int bucket_;
  virtual int isRateControlled() { return 0; }
  virtual int initTokenBucketIfNecessary() { return 0; }
  virtual int getTokenQLen() { return 0; }
  virtual int getTokenQMaxSize() { return 0; }
  virtual void enqueTokenQ(Packet*) { }
  virtual Packet* dequeTokenQ() { return NULL; }
  virtual Packet* peekTokenQ() { return NULL; }
  virtual void failEnqueTokenQ(Packet* p) {
      struct hdr_cmn *ch = HDR_CMN(p);
      // update drop count
      myStats.TotalPacketsDropped++;
      myStats.TotalBytesDropped += ch->size();
      monitorDropCount++;
  }
  virtual double updateTokenBucket() { return 0.0; }
  virtual void consumeTokens(int pktsize) { }
  virtual double getTokens() { return 0.0; }
  virtual double getTokensRate() { return 0.0; }
  virtual void startTokenQTimer(Packet *p, double etime) {}

  int myBGID;
  serviceFlowMgr *mySFMgr;
//$A806
  void setHierarchicalMode(int modeFlag);
  void setBondingGroupObject(bondingGroupObject *myBGObjParam);
  bondingGroupObject *myBGObject;
  int hierarchicalMode;
//$A902
//AGGREGATED, SINGLEFLOW, ...
  int serviceFlowType;

  int dumpQueues(char *outputFile);
  double totalPacketQueueTime;
  double packetQueueTimeCount;

  double lastPacketTime;

  double lastDequeueTime;

  double     lastTimeServiced;
  double     interServiceTime1Total;
  int        interServiceTime1Count;
  double     largestInterServiceTime1;
  double     interServiceTime2Total;
  int        interServiceTime2Count;
  double     largestInterServiceTime2;

//TODO : Eventually get rid of.
  int cindex;
  int findex;
  struct serviceFlowObjectStatsType myStats;
  int myPacketCount;


  int flowPriority;
  int minRate;
//$A806
  double maxRate;
  double minLatency;

//$A807
  double lastSchedulingEvent;


  u_int16_t SchedQType;
  int SchedQSize;
  u_int16_t SchedServiceDiscipline;

  u_int16_t reseqFlag;
  int reseqWindow;
  double reseqTimeout;

  int deficitCounter;
  int flowQuantum;
  double flowWeight;

  double MAXp;
  int    adaptiveMode;

  double  burstTimeScale;
  int     serviceRateBurstCounter;
  double  largestServiceRateBurst;
  double  lastBurstTimeCalc;

  double  updateRates();
  double     avgArrivalRate;
  double     avgServiceRate;

private:
  double     byteArrivals;
  double     byteDepartures;
  double     lastRateSampleTime;
 double  rateWeight;

};

class DSserviceFlowObject: public serviceFlowObject
{
    friend class MacDocsisCMTS;
public:
  DSserviceFlowObject();
  DSserviceFlowObject(int macaddr,int32_t src_ip, int32_t dst_ip, packet_t pkt_type, u_int16_t serviceFlowID, int myBGIDParam);
  virtual ~DSserviceFlowObject();
  virtual void init(int direction, MacDocsis *myMac,serviceFlowMgr * mySFMgrParam);

  virtual int isRateControlled() { return (ratecontrol ? 1 : 0); }
  virtual int initTokenBucketIfNecessary() {
      if (init_) 
      {
          tokens_ =  bucket_;
          lastupdatetime_ = Scheduler::instance().clock();
          init_ = 0;
          return 1;
      }
      return 0;
  }
  virtual int getTokenQLen() { return tokenq_->length(); }
  virtual int getTokenQMaxSize() { return tokenqlen_; }
  virtual void enqueTokenQ(Packet* p) { tokenq_->enque(p); }
  virtual Packet* dequeTokenQ() { return tokenq_->deque(); }
  virtual Packet* peekTokenQ() { return tokenq_->head(); }
  virtual double updateTokenBucket()
  {
      double now = Scheduler::instance().clock();
      tokens_ += (now - lastupdatetime_) * rate_;
      if (tokens_ > bucket_) tokens_ = bucket_;
      lastupdatetime_ = now;
      return tokens_;
  }
  virtual void consumeTokens(int pktsize) { tokens_ -= pktsize; }
  virtual double getTokens() { return tokens_; }
  virtual double getTokensRate() { return rate_; }
  virtual void startTokenQTimer(Packet *p, double etime)
  {
      mhSFToken_->start(p, etime);
  }

private:


  /*************************************************************************
   This structure defines attributes of downstream service flow of a CM 
  *************************************************************************/
//  struct flow_classifier classifier;
//  u_int16_t flow_id; /* Downstream service flow id */
  PhsType PHS_profile; /* PHS to be applied */

  //#ifdef RATE_CONTROL//-------------------------------------------------
  PacketQueue *tokenq_; /* Token bucket queue */
  /* Will not be used in CM code as of now */

  CmtsSFTokenDocsisTimer *mhSFToken_; // added

//JJM MOVED
//  double tokens_;
//  double rate_;
//  int bucket_;
//  int tokenqlen_;
  double lastupdatetime_;
  int init_;
  Event	intr;
  char ratecontrol; /* To indicate whether rate-control is ON or not */
  //#endif //-------------------------------------------------------------
  
};


class USserviceFlowObject: public serviceFlowObject
{
public:
  USserviceFlowObject();
  USserviceFlowObject(int macaddr,int32_t src_ip, int32_t dst_ip, packet_t pkt_type, u_int16_t serviceFlowID,  int myBGIDParam);
  virtual ~USserviceFlowObject();
  virtual void init(int direction, MacDocsis *myMac,serviceFlowMgr * mySFMgrParam);

private:


  /*************************************************************************
  This structure defines attributes of an upstream service flow of a CM node
  *************************************************************************/
//  struct flow_classifier classifier;
  UpSchedType sched_type; /* UGS, RT-VBR, BEST-EFFORT*/
  PhsType PHS_profile;    /* PHS to be applied */
  
  double ginterval;       /* Grant interval for UGS flow */
  
//  u_int16_t flow_id;      /* Upstream service flow id */
  u_int16_t gsize;        /* Grant size(bytes) for UGS flow */
  u_char flag;

};



#endif /* __ns_serviceFlowObject_h__ */

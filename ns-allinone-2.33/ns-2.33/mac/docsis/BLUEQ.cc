/************************************************************************
* File:  BLUEQ.cc
*
* Purpose:
*  This module contains a basic list object.
*
* Revisions:
*
************************************************************************/
#include "BLUEQ.h"
#include <iostream>
#include <math.h>
#include <random.h>

#include "object.h"

#include "globalDefines.h"
#include "docsisDebug.h"
//#define TRACEME 0
//#define FILES_OK 0

BLUEQ::BLUEQ() : ListObj()
{
#ifdef TRACEME
  printf("BLUEQ: constructed list \n");
#endif
}

BLUEQ::~BLUEQ()
{
#ifdef TRACEME
  printf("BLUEQ: destructed list \n");
#endif
}

void BLUEQ::initList(int maxListSize)
{
  double curTime =  Scheduler::instance().clock();

  ListObj::initList(maxListSize);

  minth = 0;
  queueThreshold=0;
  flowPriority = 0;
  maxth = maxListSize;
  maxp = .10;
  weightQ = 1.0;
  gentleMode = 0;
  adaptive1Mode=0;
  avgQ = 0.0;
  dropP = 0.0;
  q_time = 0.0;
  avgTxTime = (double)1500 * 8 / (double) 5000000;

  freeze_time = 0.05;
//  incr1 = .02;
  incr1 = .04;
  dec1 = .002;
//This should be drop tail
//  incr1 = 0;
//  dec1 = 0;
  lastUpdateTime = curTime;

  avgDropP = 0;
  avgDropPSampleCount = 0;
  avgAvgQ = 0;
  avgAvgQPSampleCount= 0;

  numberIncrements = 0;
  numberDecrements=0;
  numberDrops =0;
  numberTotalChannelIdleEvents = 0;
  numberValidChannelIdleEvents = 0;


#ifdef TRACEME
  printf("BLUEQ:init(%lf): max list size: %d avgTxTime:%lf, freeze_time:%lf, incr1:%lf, dec1:%lf  set lastUpdateTime:%lf\n",
           curTime,maxListSize,avgTxTime,freeze_time,incr1,dec1,lastUpdateTime);
#endif


}


/*************************************************************
* routine:
*   int  BLUEQ::addElement(ListElement& element)
*
* Function: this routine inserts the ListElement to the tail of 
* the list.
*  If this operation can ont be done, the caller must take care
*  of cleaning up.
*           
* inputs: 
*    ListElement& element : the element to be inserted
*
* outputs:
*  Returns a SUCCESS or FAILURE.
*        Possible failures:
*             -malloc fails
*             -list already has  > MAXLISTSIZE elements
*
***************************************************************/
int  BLUEQ::addElement(ListElement& element)
{
  int rc = FAILURE;
  double curTime =  Scheduler::instance().clock();
  double updateTime = curTime - lastUpdateTime;

#ifdef TRACEME
   printf("BLUEQ:addElement(%lf) listsize:%d, avgQ:%3.1f, mint:%d, maxt:%d, dropP:%3.3f \n ",
      curTime,curListSize,avgQ,minth,maxth,dropP);
#endif


  avgQ = updateAvgQ();
  //Correct for the case of if the avgQ is 0
  if (avgQ < 0.1)  
      dropP = 0.0;

  if (listID == 8888) {
     printf("BLUEQ:addElement(%lf) UPDATE AvgQ listsize:%d, avgQ:%3.1f, mint:%d, maxt:%d, dropP:%3.3f \n ",
      curTime,curListSize,avgQ,minth,maxth,dropP);
  }
#ifdef TRACEME
     printf("BLUEQ:addElement(%lf) UPDATE AvgQ listsize:%d, avgQ:%3.1f, mint:%d, maxt:%d, dropP:%3.3f \n ",
      curTime,curListSize,avgQ,minth,maxth,dropP);
#endif


  if (packetDrop(dropP) == TRUE) {
    rc = FAILURE;
#ifdef TRACEME
    if (rc==FAILURE)
      printf("BLUEQ:channeladdElement(%lf): DROP EVENT from packetDrop:  avgQ:%2.2f, dropP:%lf, freeze_time:%lf,  time since last update:%lf \n",
          curTime,avgQ,dropP,freeze_time,updateTime);
#endif
  }
  else {
    rc = ListObj::addElement(element);
#ifdef TRACEME
    if (rc==FAILURE)
      printf("BLUEQ:channeladdElement(%lf): DROP EVENT from addElement:  avgQ:%2.2f, dropP:%lf, freeze_time:%lf,  time since last update:%lf \n",
          curTime,avgQ,dropP,freeze_time,updateTime);
#endif
  }

   //if the queue level gets too big...force a drop
  if (rc == SUCCESS) {
    if (avgQ > queueThreshold)  {
      if (updateTime > freeze_time) {
        dropP = dropP + incr1;
        if (dropP > 1.0)
         dropP = 1.0;

        numberIncrements++;
        avgDropP+= dropP;
        avgDropPSampleCount++;
      }
    }
  }

  if (rc == FAILURE) {
    numberDrops++;
#ifdef TRACEME
    printf("BLUEQ:channeladdElement(%lf): DROP EVENT:  avgQ:%2.2f, dropP:%lf, freeze_time:%lf,  time since last update:%lf \n",
          curTime,avgQ,dropP,freeze_time,updateTime);
#endif
    if (updateTime > freeze_time) {
      dropP = dropP + incr1;
      if (dropP > 1.0)
       dropP = 1.0;

      numberIncrements++;
      avgDropP+= dropP;
      avgDropPSampleCount++;
#ifdef  FILES_OK 
      if (traceFlag == 1) {
        FILE* fp = NULL;
        char traceString[32];
        char *tptr = traceString;
        sprintf(tptr,"traceAQM%d.out",listID);
        fp = fopen(tptr, "a+");
        fprintf(fp,"%lf %d %3.1f %d %2.3f %2.3f %f %f Inc\n",
           Scheduler::instance().clock(),listID, avgQ, curListSize, dropP, avgDropP/avgDropPSampleCount, updateTime,numberDrops);
        fclose(fp);
      }
#endif
#ifdef TRACEME
    printf("BLUEQ:channeladdElement(%lf): UPDATE dropP:  avgQ:%6.1f, dropP:%3.3lf, avgDropP:%3.3f (#samples:%f), freeze_time:%lf, lastUpdateTime:%lf, time since last update:%lf \n",
          curTime,avgQ,dropP,avgDropP/avgDropPSampleCount,avgDropPSampleCount,freeze_time,lastUpdateTime,updateTime);
#endif
      lastUpdateTime = curTime;
    }
  }
#ifdef TRACEME
   printf("BLUEQ:addElement(%lf) exit rc=%d, listsize:%d, avgQ:%3.1f, min-t:%d, max-t:%d, dropP:%3.3f \n ",
      curTime,rc,curListSize,avgQ,minth,maxth,dropP);
#endif

  return rc;
}

/*************************************************************
* routine: ListElement * ListObj::removeElement()
*
* Function:
*
* inputs: 
*
* outputs:
*   The element at the top of the list is removed and returned.
*
*   A NULL is returned if the list is empty
*
***************************************************************/
ListElement * BLUEQ::removeElement()
{
  ListElement *tmpPtr = NULL;
  double curTime =   Scheduler::instance().clock();

#ifdef TRACEME
  printf("BLUEQ::removeElement:(%lf) start size: %d\n", curTime, curListSize);
#endif

  tmpPtr = ListObj::removeElement();
  if (curListSize == 0) {
    q_time = curTime;
  }

  return tmpPtr;
}



void BLUEQ::updateStats()
{
  double curTime =   Scheduler::instance().clock();

#ifdef TRACEME
  printf("BLUEQ:updateStats:(%lf), updated avg: %3.1f\n",curTime,avgQ);
#endif
}

void BLUEQ::printStatsSummary()
{
  double curTime =   Scheduler::instance().clock();
  double tmpAvgDropP = 0;

  if (avgDropPSampleCount > 0)
    tmpAvgDropP = avgDropP / (double)avgDropPSampleCount;
  
//#ifdef TRACEME
  printf("BLUEQ:ListObj:printStatsSummary:(%lf), updated avg: %3.1f, avgDropP:%2.3f(#samples:%f), #Incs:%lf, #Decs:%lf, #numberDrops:%lf, numberTotalChannelIdleEvents:%lf, numberValidChannelIdleEvents%lf\n",
       curTime,avgQ, tmpAvgDropP,avgDropPSampleCount,numberIncrements,numberDecrements,numberDrops,numberTotalChannelIdleEvents, numberValidChannelIdleEvents);
//#endif
}



/*************************************************************
* routine: void BLUEQ::adaptAQMParams()
*
* Function: this routine adapts the AQM parameters.
*   The algorithm is TBD.
*           
* inputs: 
*
* outputs:
*
***************************************************************/
void BLUEQ::setAQMParams(int maxAQMSizeP, int priorityP, int minthP, int maxthP, int adaptiveMode, double maxpP, double weightQP)
{

  int y = (int) ((double)maxthP * .75);
  int x=minthP;

  flowPriority = priorityP;


  //make sure that the maxth is at least 4 larger than minth
  if ( y-x < 4)
    y = x + 4;

  MAXLISTSIZE = maxAQMSizeP;
  minth = minthP;
  maxth = y;
//  maxth = maxthP;
  maxp = maxpP;
  weightQ = weightQP;
  queueThreshold= (maxth - minth)/2;
  if (queueThreshold < minth)
    queueThreshold = minth+1;

//#ifdef TRACEME
  printf("BLUEQ:setAQMParams: minth:%d  maxth:%d (But set to %d) queueThreshold:%d  maxp:%2.2f weightQ:%2.4f, priority:%d\n",minth,maxth,y,queueThreshold,maxpP,weightQP,flowPriority);
//#endif
}

/*************************************************************
* routine: void BLUEQ::adaptAQMParams()
*
* Function: this routine adapts the AQM parameters.
*   The algorithm is TBD.
*           
* inputs: 
*
* outputs:
*
***************************************************************/
void BLUEQ::adaptAQMParams()
{

#ifdef TRACEME
  printf("BLUEQ:adaptAQMParams: updated minth:%d  maxth:%d  maxp:%2.2f \n",minth,maxth,maxp);
#endif
}


double BLUEQ::getAvgQ()
{
  return(avgQ);
}

double BLUEQ::getAvgAvgQ()
{

  if (avgAvgQPSampleCount > 0)
    return(avgAvgQ/avgAvgQPSampleCount);
  else
    return(0);

}

double BLUEQ::getDropP()
{
  return(dropP);
}

double BLUEQ::getAvgDropP()
{
  double tmpAvgDropP = 0.0;

  if (avgDropPSampleCount > 0)
    tmpAvgDropP = avgDropP / avgDropPSampleCount;
  
  return(tmpAvgDropP);
}


double BLUEQ::updateAvgQ()
{
  double returnAvg = 0.0;
  double curTime =   Scheduler::instance().clock();

#ifdef TRACEME
  printf("BLUEQ:updateAvgQ:(%lf)  curListSize:%d, current avgQ:%3.3f, weightQ:%2.4f, q_time:%3.6f \n",
       curTime,curListSize,avgQ,weightQ,q_time);
  fflush(stdout);
#endif


  if (curListSize == 0) {
    double quietTime = curTime - q_time;
    double power = quietTime/avgTxTime;
    double x=  pow((1-weightQ),power);
//    returnAvg = avgQ*x;
    returnAvg = 0;
#ifdef TRACEME
    printf("BLUEQ:updateAvgQ:(%lf) case of empty queue quietTime: %6.6f, power:%3.6f, x:%3.6f, returnAvg:%3.6f \n",
       curTime,quietTime,power,x,returnAvg);
#endif
  }
  else {
    returnAvg = (1-weightQ)*avgQ + weightQ*curListSize;
#ifdef TRACEME
    printf("BLUEQ:updateAvgQ:(%lf) case of queue len:%d, current avgQ:%3.3f, updated avg:%3.3f \n",
       curTime,curListSize, avgQ,returnAvg);
#endif
  }
  avgAvgQ+=returnAvg;
  avgAvgQPSampleCount++;


#ifdef TRACEME
  printf("BLUEQ:updateAvgQ:(%lf) curLen:%d, avgQ:%3.3f, returnAvg:%3.3f avgAvgQ:%3.3f (weightQ:%3.3f)\n",
       curTime,curListSize, avgQ, returnAvg,avgAvgQ/avgAvgQPSampleCount,weightQ);
#endif

  return returnAvg;

}

void BLUEQ::channelIdleEvent()
{
  double curTime =   Scheduler::instance().clock();
  double updateTime = curTime - lastUpdateTime;

  numberTotalChannelIdleEvents++;
#if 0
#ifdef  FILES_OK 
  if (traceFlag == 1) {
    FILE* fp = NULL;
    char traceString[32];
    char *tptr = traceString;
    sprintf(tptr,"traceAQM%d.out",listID);
    fp = fopen(tptr, "a+");
    fprintf(fp,"%lf %3.1f %d %2.3f 0 %f I \n",
       Scheduler::instance().clock(),avgQ, curListSize, dropP, updateTime);
    fclose(fp);
  }
#endif
#endif

#ifdef TRACEME
  printf("BLUEQ:channelIdleEvent(%lf): avgQ:%2.2f, dropP:%lf, freeze_time:%lf,  time since last update:%lf \n",
         curTime,avgQ,dropP,freeze_time,updateTime);
#endif

  if (updateTime > freeze_time) {
    numberValidChannelIdleEvents++;
//    dropP-= .5 * dropP;
//   dropP = dropP - dec1;
//   double randNumber  = Random::uniform(0,dec1);
   double randNumber = 0;
   dropP = dropP - dec1-randNumber;
    numberDecrements++;

    //make sure it does not go negative, if not reset lastUpdateTime
    if (dropP < 0)
     dropP = 0;
    else
      lastUpdateTime = curTime;

    avgDropP+= dropP;
    avgDropPSampleCount++;
#ifdef  FILES_OK 
    if (traceFlag == 1) {
      FILE* fp = NULL;
      char traceString[32];
      char *tptr = traceString;
      sprintf(tptr,"traceAQM%d.out",listID);
      fp = fopen(tptr, "a+");
      fprintf(fp,"%lf  %d  %3.1f %d %2.3f %2.3f %f %f Dec \n",
           Scheduler::instance().clock(),listID, avgQ, curListSize, dropP, avgDropP/avgDropPSampleCount, updateTime,numberDrops);
      fclose(fp);
    }
#endif
  }

}

/*************************************************************
* routine: int BLUEQ::packetDrop(double dropP)
*
* Function: this probabalistically drops the packet. 
*           
* inputs: 
*    The drop rate that should be used.
*
* outputs:
*   TRUE or FALSE:  True instructs the caller to DROP the packet.
*
***************************************************************/
int BLUEQ::packetDrop(double dropP)
{
  int dropFlag = FALSE;
  double randNumber  = Random::uniform(0,1.0);
  double curTime =  Scheduler::instance().clock();

#ifdef TRACEME
  printf("BLUEQ:packetDrop(%lf):  packetDrop:Entry: uniform:%2.2f, dropP:  %2.2f avgQ:%6.1f, minth:%d, maxth:%d  \n",
       curTime,randNumber,dropP,avgQ,minth,maxth);
#endif
  //drop the packet if current len exceeds maxth;
  if (curListSize > maxth)  {
    dropFlag = TRUE;
#ifdef TRACEME
    printf("BLUEQ:packetDrop(%lf): packetDrop:Exit: Immediate drop, curLen > maxth (%d, %d) \n",curTime,curListSize,maxth);
#endif
  }
  else {

//    if (avgQ > 0) {
  if (avgQ > minth) {
//  if (curListSize > minth) {
//  if (curListSize > 0) {
      if (randNumber <= dropP)
        dropFlag = TRUE;
    }
#ifdef TRACEME
    printf("BLUEQ:packetDrop(%lf): packetDrop:Exit: uniform:%2.2f, dropP:  %2.2f dropFlag:%d \n",curTime,randNumber,dropP,dropFlag);
#endif
  }

#ifdef TRACEME
  printf("BLUEQ:packetDrop(%lf): packetDrop:Exit: uniform:%2.2f, dropP:  %2.2f dropFlag:%d \n",curTime,randNumber,dropP,dropFlag);
#endif

  return dropFlag;
}



/************************************************************************
* File:  serviceFlowListElement.cc
*
* Purpose:
*   Class for the service Flow List Element object
*
* Revisions:
***********************************************************************/

#include "serviceFlowListElement.h"

//$A500
#include "serviceFlowObject.h"


#include "docsisDebug.h"
//#define TRACEME 0

/***********************************************************************
*function: serviceFlowListElement::serviceFlowListElement
*
*explanation:
*
*inputs:
*
*outputs:
************************************************************************/
serviceFlowListElement::serviceFlowListElement() : ListElement()
{
#ifdef TRACEME
  printf("serviceFlowListElement: constructed service Flow List Element \n");
#endif
  mySF = NULL;
}

serviceFlowListElement::serviceFlowListElement(serviceFlowObject *mySFObjParam)
{
#ifdef TRACEME
  printf("serviceFlowListElement: constructed service Flow List Element \n");
#endif
  mySF = mySFObjParam;
}

/***********************************************************************
*function: serviceFlowListElement::~serviceFlowListElement
*
*explanation:
*
*inputs:
*
*outputs:
************************************************************************/
serviceFlowListElement::~serviceFlowListElement()
{
}

/***********************************************************************
*function: int serviceFlowListElement::putServiceFlow(serviceFlowObject *SFObjParam)
*
*explanation:
*
*inputs:
*
*outputs:
************************************************************************/
int serviceFlowListElement::putServiceFlow(serviceFlowObject *SFObjParam)
{
int rc = 0;

#ifdef TRACEME
  printf("serviceFlowListElement:putServiceFlow:  (service flow id :%d)\n",SFObjParam->flowID);
#endif
  if (mySF == NULL)
  {
    mySF =  SFObjParam;
  }
  else {
    printf("serviceFlowListElement:putServiceFlow: ERROR: mySF ptr is NOT NULL  \n");
    mySF =  SFObjParam;
  }

#ifdef TRACEME
  printf("serviceFlowListElement:putServiceFlow: put handle %0x\n",mySF);
#endif
  return rc;
}

/***********************************************************************
*function: serviceFlowObject * serviceFlowListElement::getServiceFlow()
*
*explanation:
*
*inputs:
*
*outputs:
************************************************************************/
serviceFlowObject * serviceFlowListElement::getServiceFlow()
{

#ifdef TRACEME
  printf("serviceFlowListElement:getServiceFlow: return mySF of %0x \n",mySF);
  if (mySF !=NULL)
    printf("        .......service flow id :%d\n",mySF->flowID);
  else
    printf("        ..... but mySF is NULL ??  \n");
#endif
  return mySF;
}





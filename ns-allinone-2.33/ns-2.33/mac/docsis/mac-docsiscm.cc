/******************************************************************************
 * This file contains the CM object class method implementations 
 *
 * Revisions:
 *
 *  $A304: all changes required to port code to ns2.33 on vmplant
 *  $A305:  add docsis 3.0 extended header
 *  $A311:  support configMgr
 *  $A400:  v.93 changes
 *  $A510:  support US SF Mgr and RED queues
 *  $A601:    Added a new channel access delay monitor
 *
 *  $A700:  Temporarily removed sending Mgmt messages US
 *          Create CONCATREQ.out to track contention req usage
*           Fixed frag bug - when we call fill_extended_header we need
*           to use the [].frag_pkt  pointer as this is the packet that is being sent in
 *          multiple fragments
*  $701:    support multiple SF's via the mgr->anyPackets() and mgr->deque() methods  
*           Had to change case when we sent a concatenated packet, always add
*           a piggy request. This was a fix for supporting MGMT and default SF's
*           CM records the number of bytes it requested in a concatenated request
*           in numberBytesInNextConcat_.  Send_concat then sees when a partial grant
*           comes in.  And only sends 1 packet. 
*  $702:    fixes to the following:
*             -to correctly track the avg firstbackoff (in slots) 
*              and the avg backoff for the each collision incident
* 
 *****************************************************************************/

#include "globalDefines.h"
#include "mac-docsis.h"
#include "random.h"
#include <stdio.h>
#include <string.h>
#include "ping.h"
#include "loss_monitor.h"

//$A311
#include "configMgr.h"
#include "macPhyInterface.h"
#include "medium.h"
#include "serviceFlowMgr.h"
#include "reseqMgr.h"
#include "serviceFlowObject.h"
#include "schedulerObject.h"
#include "packetMonitor.h"

#include "docsisDebug.h"

/*===============================MACROS=========================*/
#define US_RATE_CONTROL 1

//# of seconds between status msgs
//#define CM_STATUS_MSG_FREQ 2
#define CM_STATUS_MSG_FREQ 200

#define WATCH_CMID 1 /*Set to the cm_id for special tracing*/

//Turns on timing logs
//#define TIMINGS 1

//Note: this will trace MAC message data, but no framing layer  overhead
//#define TRACE_CM_UP_DATA 0
//#define TRACE_CM_DOWN_DATA 0


//If 1, we dump all (or CM0,1) CM status msgs to
// the file adaptstatus.dat
#define DUMP_ADAPTATION_STATUS_MSG 1

//#define ARRIVAL_TRACE 0

//Define this to 1 so the final stats are 1 line rather than4 lines of text...
#define SHORT_STATS 1
#define EPSILON 0.0000000000005

//#define  MAX_CONCAT_BYTES 1550
//#define  MAX_CONCAT_BYTES 4550
#define  MAX_CONCAT_BYTES 45500

#define DIRECTION_UP 1
#define DIRECTION_DOWN 0
#define MACDOCSIS_COMMAND 100

/*===============================END MACROS=============================*/

/*************************************************************************
   TCL Hooks for the simulator
*************************************************************************/
static class MacDocsisCmClass : public TclClass 
{
public:
  MacDocsisCmClass() : TclClass("Mac/DocsisCM") {}
  TclObject* create(int, const char*const*) 
  {
    return (new MacDocsisCM);
  }
} class_mac_docsiscm;

/*************************************************************************
CONSTRUCTOR FUNCTION
*************************************************************************/
MacDocsisCM::MacDocsisCM()  
  : MacDocsis(), mhCmStatus_(this),  mhCmRng_(this), mhSend_(this), mhReq_(this), mhSeq_(this)
{
  /* 
     Will not be having service-flow information here...So, using 
     command function to set the service-flow parameters...
  */  
  /*collision = 0;*/ //Commenting this out, since we have one collision variable per
  //Channel in the channel_muxer. Remember to handle all instances of collision.
  SizeUpFlowTable = 0;
  SizeDownFlowTable = 0;
  default_upstream_index_ = 0;
  default_dstream_index_ = 0;
  map_acktime = -1.0;
  debug_cm = 0;
  priority = 0;
  SndList = 0;
  ReqList = 0;
  last_dmptime = 0;
  rng_ = new RNG;
  rng_->set_seed(RNG::HEURISTIC_SEED_SOURCE);
  
  UGSswitch[0] = &MacDocsisCM::ugs_idle;
  UGSswitch[1] = &MacDocsisCM::ugs_decision;
  UGSswitch[2] = &MacDocsisCM::ugs_tosend;
  UGSswitch[3] = &MacDocsisCM::ugs_waitformap;
  
  RTPOLLswitch[0] = &MacDocsisCM::rtpoll_idle;
  RTPOLLswitch[1] = &MacDocsisCM::rtpoll_decision;
  RTPOLLswitch[2] = &MacDocsisCM::rtpoll_tosend;
  RTPOLLswitch[3] = &MacDocsisCM::rtpoll_waitformap;
  RTPOLLswitch[4] = &MacDocsisCM::rtpoll_tosendreq;
  RTPOLLswitch[5] = &MacDocsisCM::rtpoll_reqsent;
  
  BEFFORTswitch[0] = &MacDocsisCM::beffort_idle;
  BEFFORTswitch[1] = &MacDocsisCM::beffort_decision;
  BEFFORTswitch[2] = &MacDocsisCM::beffort_tosend;
  BEFFORTswitch[3] = &MacDocsisCM::beffort_waitformap;
  BEFFORTswitch[4] = &MacDocsisCM::beffort_tosendreq;
  BEFFORTswitch[5] = &MacDocsisCM::beffort_reqsent;
  BEFFORTswitch[6] = &MacDocsisCM::beffort_contention;
  

  bzero((void *)&MACstats,sizeof (struct  mac_statistics));
  MACstats.last_BWCalcTime = Scheduler::instance().clock();

  bzero((void *)&CMstats,sizeof (struct cm_statistics));

  last_StatusTime  =0.0; /* Indicates the last time a CM status Msg was sent*/
  my_lan = lan_num - 1;


//$A310
 //next_USFlowID=3;
 //next_DSFlowID=3;

//$A311
  myConfigMgr = new CMconfigMgr;
  myConfigMgr->init(this);
 
//$510
  myDSSFMgr = NULL;
  myUSSFMgr = NULL;

  avgCSRatio=0;
  avgCSRatioCount=0;
  avgTotalSlotUtilization = 0;
  avgMySlotUtilization = 0;
  avgChannelAccessDelay = 0;
  lastStatusUpdateTime=0;


      
#ifdef TCP_DELAY_BIND_ALL //--------------------------------------------------
#else //----------------------------------------------------------------------
  bind("total_CM_bytes_sent", &MACstats.total_num_sent_bytes);
  //bind("total_CM_pkts_sent", &MACstats.total_num_sent_bytes);
#endif//----------------------------------------------------------------------
}

/*************************************************************************

*************************************************************************/
int MacDocsisCM::command(int argc, const char*const* argv)
{
  int rc = 0 ;
#ifdef TRACE_CM_COMMAND //------------------------------------------------
  printf("CM:command: entered with command param count:%d and command:: %s\n",argc,argv[1]);
#endif //-------------------------------------------------------------------- 
  rc = myConfigMgr->processCommandLine(argc, argv);
  if (rc == MACDOCSIS_COMMAND)
	return MacDocsis::command(argc,argv);
  return rc;
}

/*************************************************************************

*************************************************************************/
void MacDocsisCM::print_classifiers()
{
  int i;
  FILE* USfp = NULL;
  FILE* DSfp = NULL;
  

  if (debug_cm)
  {
    printf("print_classifiers for CM%d (macaddress:%d) UPSTREAM FLOWS \n",cm_id,index_);
    for (i= 0; i < SizeUpFlowTable;i++)
    {
      printf("tbindex = %d\n",i);
      printf("Flow-id = %d\n",UpFlowTable[i].upstream_record.flow_id);
      printf("Sched-type = %d\n",UpFlowTable[i].upstream_record.sched_type);
      
      printf("Src-ip = %d\n",UpFlowTable[i].upstream_record.classifier.src_ip);
      printf("dst-ip = %d\n",UpFlowTable[i].upstream_record.classifier.dst_ip);
      printf("pkt-type = %d\n",UpFlowTable[i].upstream_record.classifier.pkt_type);
      printf("Phs-profile = %d\n",UpFlowTable[i].upstream_record.PHS_profile);
      printf("Flag = %d\n",UpFlowTable[i].upstream_record.flag);
      printf("Gsize = %d\n",UpFlowTable[i].upstream_record.gsize);
      printf("Ginterval = %lf\n",UpFlowTable[i].upstream_record.ginterval);
      printf("\n");
    }  
    printf("DOWNSTREAM FLOWS \n");
  
    for (i= 0; i < SizeDownFlowTable;i++)
    {
      printf("tbindex = %d\n",i);
      printf("Flow-id = %d\n",DownFlowTable[i].downstream_record.flow_id);
      printf("Src-ip = %d\n",DownFlowTable[i].downstream_record.classifier.src_ip);
      printf("dst-ip = %d\n",DownFlowTable[i].downstream_record.classifier.dst_ip);
      printf("pkt-type = %d\n",DownFlowTable[i].downstream_record.classifier.pkt_type);
      printf("Phs-profile = %d\n",DownFlowTable[i].downstream_record.PHS_profile);
    }
  }

//$A510
//#ifdef FILES_OK
  USfp = fopen("USCMFlows.out", "a+");
  DSfp = fopen("DSCMFlows.out", "a+");
  for (i= 0; i < SizeUpFlowTable;i++)
  {
    fprintf(USfp,"%d %d %d %d %d %d %d %d %d %d %d %d %d %1.6f\n",
      cm_id, index_, i,UpFlowTable[i].upstream_record.flow_id,
      UpFlowTable[i].upstream_record.sched_type,
      UpFlowTable[i].upstream_record.flowPriority,
      UpFlowTable[i].upstream_record.SchedQType,
      UpFlowTable[i].upstream_record.SchedQSize,
      UpFlowTable[i].upstream_record.SchedServiceDiscipline,
      UpFlowTable[i].upstream_record.classifier.src_ip,
      UpFlowTable[i].upstream_record.classifier.dst_ip,
      UpFlowTable[i].upstream_record.classifier.pkt_type,
      UpFlowTable[i].upstream_record.classifier.flowID,
      UpFlowTable[i].upstream_record.PHS_profile,
      UpFlowTable[i].upstream_record.flag,
      UpFlowTable[i].upstream_record.gsize,
      UpFlowTable[i].upstream_record.ginterval);
  }  
  
  for (i= 0; i < SizeDownFlowTable;i++)
  {
    fprintf(DSfp,"%d %d %d %d %d %d %d %d %d %d %d %d %d %d \n",
      cm_id,index_, i,
      DownFlowTable[i].downstream_record.flow_id,
      DownFlowTable[i].downstream_record.dsid,
      DownFlowTable[i].downstream_record.flowPriority,
      DownFlowTable[i].downstream_record.SchedQType,
      DownFlowTable[i].downstream_record.SchedQSize,
      DownFlowTable[i].downstream_record.SchedServiceDiscipline,
      DownFlowTable[i].downstream_record.classifier.src_ip,
      DownFlowTable[i].downstream_record.classifier.dst_ip,
      DownFlowTable[i].downstream_record.classifier.pkt_type,
      DownFlowTable[i].downstream_record.classifier.flowID,
      DownFlowTable[i].downstream_record.PHS_profile);
  }
  fclose(USfp);
  fclose(DSfp);
//#endif
}

/*************************************************************************
 * Routine: void MacDocsisCM::set_default(struct upstream_sflow *flow_table)
 * 
 * Explanation:
 *   searches the upstream flow table and designated the default.  The algorithm simply
 *   selects the first entry corresponding to Sched Type = 
 *
 * Inputs:
 * 
 * Outputs:
 * 
 *
*************************************************************************/
void MacDocsisCM::set_default()
{
int i;
char p=SizeUpFlowTable;

  if (debug_cm)
    printf("MacDocsisCM:set_default: Find default upstream flow: SizeUpFlowTable:%d\n",SizeUpFlowTable);

  for (i= 0; i < SizeUpFlowTable;i++)
  {
    if (debug_cm) {
      printf("CM:set_default:tbindex = %d\n",i);
      printf("CM:set_default:Flow-id = %d\n",UpFlowTable[i].upstream_record.flow_id);
      printf("CM:set_default:Sched-type = %d\n",UpFlowTable[i].upstream_record.sched_type);
      printf("CM:set_default:Src-ip = %d\n",UpFlowTable[i].upstream_record.classifier.src_ip);
      printf("CM:set_default:dst-ip = %d\n",UpFlowTable[i].upstream_record.classifier.dst_ip);
    }
    if (UpFlowTable[i].upstream_record.sched_type == BEST_EFFORT) {
      p = i;
      if (debug_cm) 
        printf("CM:set_default: FOUND:  tbindex = %d (flow_id=%d)\n",i,UpFlowTable[i].upstream_record.flow_id);
    }
  }
  default_upstream_index_ = p;
}

/*************************************************************************
 Timer handler functions 
*************************************************************************/
void MacDocsisCM::CmSeqHandler(Event *e)
{
//!!!!!!!!!!!!!!!! FIX THIS !!!!!!!!!!!!!!!!!!!!
	return;
}

/************************************************************************
 *
 * void MacDocsisCM::CmSendHandler(
 *
 * input:   
 *   int channelNumber
 *  Packet *p
 *
 * outputs:  none
 *
 * function:
 *   This is called when an US transmission is complete
 *
 *   Overview:
 *
 ************************************************************************/
void MacDocsisCM::CmSendHandler(int channelNumber, Packet *p)
{
char tbindex = classify(p, UPSTREAM);
int len = myUSSFMgr->len_queue(p,tbindex);
serviceFlowObject *mySFObj = myUSSFMgr->getServiceFlow(p);

  Packet::free((Packet *)p);

  if (debug_cm) {
    printf("CM:%d(flow-id %d):CmSendHandler:sendHandler(%lf): set channel %d IDLE (SF q len:%d, current state:%d) \n",
	       cm_id,UpFlowTable[tbindex].upstream_record.flow_id,
           Scheduler::instance().clock(),channelNumber,len,UpFlowTable[tbindex].state);
  }

  /* unlock IFQ. */
  if(callback_) 
    {
      Handler *h = callback_;
      callback_ = 0;
      h->handle((Event*) 0);
    } 
  

}

void MacDocsisCM::CmSndTimerHandler(Event *e)
{
	sptr temp;
	char tbindex;

#ifdef TRACE_DEBUG
  tracePoint("CM:CmSndTimerHandler",cm_id,0);
#endif
  if (debug_cm)
    printf("MacDocsisCM:CmSndTimerHandler[%d](%lf) \n",
              cm_id,Scheduler::instance().clock());
	
	temp = SndList;

	if (!temp)
	  {
	    printf("CM%d :Error in CmSndTimerHandler: Exiting\n",cm_id);
	    exit(1);
	  }
	else
	  {
	    SndList = SndList->next;
	  }
	
	tbindex = temp->tindex;
	free(temp);
	
	if (UpFlowTable[tbindex].upstream_record.sched_type == UGS)
	  {
	    (this->*UGSswitch[UpFlowTable[tbindex].state])(tbindex, SEND_TIMER, UpFlowTable[tbindex].pkt);
	  }	
	else if (UpFlowTable[tbindex].upstream_record.sched_type == RT_POLL)
	  {
	    (this->*RTPOLLswitch[UpFlowTable[tbindex].state])(tbindex, SEND_TIMER, UpFlowTable[tbindex].pkt);
	  }
	else if (UpFlowTable[tbindex].upstream_record.sched_type == BEST_EFFORT)
	  {
	    (this->*BEFFORTswitch[UpFlowTable[tbindex].state])(tbindex, SEND_TIMER, UpFlowTable[tbindex].pkt);
	  }
	return;
}	

/*************************************************************************

*************************************************************************/	
void MacDocsisCM::CmRngHandler(Event * e)
{
  Packet * p = Packet::alloc();
  char tbindex;
  hdr_docsis   *dh  = hdr_docsis::access(p);
  hdr_cmn   *ch  = HDR_CMN(p);  
  double curr_time = Scheduler::instance().clock();
  
//$A700
  return;

  if (debug_cm)
    printf("CmRngHandler(%lf):CM%d :Sending a RNG-REQ message \n",curr_time,cm_id);
  
  ch->uid() = 0;
  //ch->ptype() = PT_MAC;
  ch->ptype() = PT_DOCSISMGMT;
  ch->iface() = -2;
  ch->error() = 0;
    
  ch->size() = 10 + SIZE_MGMT_HDR;
  dh->dshdr_.fc_type = MAC_SPECIFIC;
  dh->dshdr_.fc_parm = MANAGEMENT;
  dh->dshdr_.ehdr_on = 0;
  dh->dshdr_.mac_param = 0;
  
  /* Send the packet */
  tbindex = classify(p, UPSTREAM);
  
  if (debug_cm)
    printf("CM%d :Packet classified on flow-id %d\n",cm_id,UpFlowTable[tbindex].upstream_record.flow_id);
  
  HandleOutMgmt(p,tbindex); 
  
  /* add some randomness.  This will reduce the frequency but that's ok */
  double random_delay  = Random::uniform(.01,3);
  mhCmRng_.start((Packet *) (&rintr), (rng_freq+random_delay));  
}

/*************************************************************************
 * Routine: 
 *    void MacDocsisCM::CmStatusHandler(Event * e)
 * 
 * Explanation:
 *   This is called from the status msg timer to send the next status message.
 *
 * Inputs:
 * 
 * Outputs:
 * 
 *   Sends a packet with the following data format:
 *  
 *     Note: See hdr-docsis.h for the lastest defintion for this....
  *         int USByteCount;
  *         int DSByteCount;
  *         int USCollisionRate;
  *         int currentBkOffScale;
  *         int USRAD;        //US request access delay 
  *         int USNRAD;       //US normalized access delay
  *         int timeSinceLastStatus;      //number of milliseconds since last sent a status
 *
 *     
 *
*************************************************************************/	
void MacDocsisCM::CmStatusHandler(Event * e)
{
  struct status_docsismgmt statusMsg;
  int msgSize = sizeof(struct status_docsismgmt);
  int i = 0; //loop variable
  u_char *srcPtr,*dstPtr;
  FILE* Sfp = NULL;
#ifdef TRACE_DEBUG
  tracePoint("CM:CmStatusHandler",cm_id,0);
#endif

//$A700
  return;

  Packet *p = AllocPkt(sizeof(u_char) * msgSize);	
  u_char *data = (u_char *) p->accessdata();
  char tbindex;
  hdr_docsis   *dh  = hdr_docsis::access(p);
  hdr_cmn   *ch  = HDR_CMN(p);  
  double curr_time = Scheduler::instance().clock();
  double tmpTime;

  statusMsg.cm_id = cm_id; 
  tmpTime = (curr_time - last_StatusTime); 
  statusMsg.timeSinceLastStatus = (int) (1000000.0*tmpTime); 
  last_StatusTime = curr_time;
  //set status msg fields
  statusMsg.USByteCount = (int)CMstats.total_BW_bytesUP_status ;
  CMstats.total_BW_bytesUP_status = 0;
  statusMsg.DSByteCount = (int) CMstats.total_BW_bytesDOWN_status ;
  CMstats.total_BW_bytesDOWN_status = 0;
  statusMsg.currentBkOffScale = bkoffScale;

  if (CMstats.num_delay_samples_status > 0) {
    statusMsg.USRAD = (int) (1000000 * CMstats.avg_req_stime_status / CMstats.num_delay_samples_status);
    if (CMstats.avg_req_stimeBYTES_status > 0) 
      statusMsg.USNRAD = (int) (100 * statusMsg.USRAD) / CMstats.avg_req_stimeBYTES_status; 
    else
      statusMsg.USNRAD = 0;
  } 
  else {
    statusMsg.USRAD = 0;
    statusMsg.USNRAD = 0;
  }

  CMstats.avg_req_stime_status =0;
  CMstats.avg_req_stimeBYTES_status =0;
  CMstats.num_delay_samples_status  = 0;

  if (CMstats.total_pktsSentUP_status > 0)
    statusMsg.USCollisionRate = (int) ( 1000 * CMstats.total_collisions_status / CMstats.total_pktsSentUP_status);
  else
    statusMsg.USCollisionRate = 0;

  CMstats.total_collisions_status = 0;
  CMstats.total_pktsSentUP_status = 0;

  //copy struct to msg
  srcPtr = (u_char *)&statusMsg;
  dstPtr = (u_char *)data;
  if (msgSize >255) {
    printf("CmStatusHandler(%lf):CM%d :WARNING: Adjusting CM status msg to 255 bytes\n",
            curr_time,cm_id);
    msgSize = 255;
  }
  for(i=0; i<msgSize; i++) {
    *dstPtr++ = *srcPtr++;
  }
  if (debug_cm)
    printf("CmStatusHandler(%lf):CM%d :Sending a CM status message of size:%d (test:%d)\n",
            curr_time,cm_id,msgSize,statusMsg.currentBkOffScale);
  
  ch->uid() = 0;
  ch->ptype() = PT_DOCSISSTATUS;
  ch->iface() = -2;
  ch->error() = 0;
    
//What about DOCSIS_HDR_LEN
  ch->size() += ETHER_HDR_LEN;
  ch->size() += SIZE_MGMT_HDR + msgSize;
  dh->dshdr_.fc_type = MAC_SPECIFIC;
  dh->dshdr_.fc_parm = STATUS;
  dh->dshdr_.ehdr_on = 0;
  //JJM WRONG
  dh->dshdr_.mac_param = (u_char) msgSize;
//TODO : Do I need to do this ?
//  dh->dshdr_.len =  ch->size();
//  ch->size() += SIZE_MGMT_HDR;

  tbindex = classify(p, UPSTREAM);


//Note: unlike the request message, we have no flow id associated with the msg.l
//The algorithm at the cmts tracks this messages by cm_id
//  dh->dshdr_.len = UpFlowTable[tbindex].upstream_record.flow_id;
  if (debug_cm)
      printf("CmStatusHandler:(%lf): cmid:%d, tbindex:%d \n",
        Scheduler::instance().clock(), cm_id,tbindex);

  
  if (debug_cm)
      printf("CmStatusHandler:(%lf) CMUSByteCount:%d, DSByteCount:%d USCR:%d, bkoffScale:%d, USRAD(us):%d, USNRAD(us):%d\n",
        Scheduler::instance().clock(), statusMsg.USByteCount,
        statusMsg.DSByteCount, statusMsg.USCollisionRate, statusMsg.currentBkOffScale,statusMsg.USRAD,statusMsg.USNRAD);


#ifdef FILES_OK
    if (DUMP_ADAPTATION_STATUS_MSG == 1) {
      Sfp = fopen("adaptstatus.dat", "a+");
      fprintf(Sfp,"%d %f %d %d %d %d %d %d\n",
        statusMsg.cm_id,curr_time,statusMsg.USByteCount,statusMsg.DSByteCount,
        statusMsg.USRAD,statusMsg.USNRAD,statusMsg.USCollisionRate,statusMsg.currentBkOffScale);
      fclose(Sfp);
    }
#endif

  /* Send the packet */
  HandleOutMgmt(p,tbindex); 
  
  double random_delay  = Random::uniform(.01,.1);
  mhCmStatus_.start((Packet *) (&StatusIntr), (status_freq+random_delay));	
}

/*************************************************************************

*************************************************************************/
void MacDocsisCM::CmReqTimerHandler(Event *e)
{
  rptr temp;
  char tbindex;
  temp = ReqList;
  tempReqList = ReqList; 

#ifdef TRACE_DEBUG
  tracePoint("CM:CmReqTimerHandler",cm_id,0);
#endif
  if (!temp)
    {
      printf("CM%d :Error in CmReqTimerHandler: Exiting\n",cm_id);
      exit(1);
    }
  else
    {
      ReqList = ReqList->next;
    }
  tbindex = temp->rindex;
  free(temp);
  
  if (UpFlowTable[tbindex].upstream_record.sched_type == UGS)
    {
      printf("CM%d :MacDocsisCM->CmReqTimerHandler: Error, Req timer was set for UGS flow , Exiting\n",cm_id);
      exit(1);
    }
  else if (UpFlowTable[tbindex].upstream_record.sched_type == RT_POLL)
    {
      (this->*RTPOLLswitch[UpFlowTable[tbindex].state])(tbindex, REQ_TIMER, UpFlowTable[tbindex].pkt);
    }
  else if (UpFlowTable[tbindex].upstream_record.sched_type == BEST_EFFORT)
    {
      if(reqFlag == 0)
         reqFlag = 1;
      reqFlagCounter++;

      (this->*BEFFORTswitch[UpFlowTable[tbindex].state])(tbindex, REQ_TIMER, UpFlowTable[tbindex].pkt);
    }
}	

/*************************************************************************
 * Routine:  void MacDocsisCM::HandleMap(Packet * p)
 *
 * Explanation: This routine invokes every flow handler (in
 *   whatever state they are in) to process the map. 
 *   If a flow sees an opportunity to send- it updates 
 *   the CM's allocation table.
 *
 * Inputs:  
 *   Packet *p : The packet containing the MAP
 *
 * Outputs:
 *
 *
*************************************************************************/
void MacDocsisCM::HandleMap(Packet * p)
{
  /* The 'data' portion of 'p' will have all the IEs */
  int i = 0;

#ifdef TRACE_DEBUG
  tracePoint("CM:HandleMap",cm_id,0);
#endif
  
  if (debug_cm) {
    printf("CM%d:HandleMap: received MAP at %lf (sizeUpFlowTable:%d) \n",
     cm_id,Scheduler::instance().clock(),SizeUpFlowTable);
  }

  map_ = p->copy();
  
  //Make sure callers don't free the map_
  for (i = 0; i < SizeUpFlowTable; i++)
    {
      if (UpFlowTable[i].upstream_record.sched_type == UGS)
	{
           if (debug_cm)
             printf("CM%d : received MAP: handle UGS flow %lf\n",cm_id,Scheduler::instance().clock());
	  (this->*UGSswitch[UpFlowTable[i].state])(i, MAP_ARRIVAL, UpFlowTable[i].pkt);
	}
      else if (UpFlowTable[i].upstream_record.sched_type == RT_POLL)
	{
           if (debug_cm)
             printf("CM%d : received MAP: handle rtPS flow %lf\n",cm_id,Scheduler::instance().clock());
	  (this->*RTPOLLswitch[UpFlowTable[i].state])(i, MAP_ARRIVAL, UpFlowTable[i].pkt);
	}
      else if (UpFlowTable[i].upstream_record.sched_type == BEST_EFFORT)
	{
           if (debug_cm) {
             printf("CM%d:HandleMap: received MAP handle BE flow %lf (sizeUpFlowTable:%d) STATE:%d\n",
                      cm_id,Scheduler::instance().clock(),SizeUpFlowTable, UpFlowTable[i].state);
           }

	  (this->*BEFFORTswitch[UpFlowTable[i].state])(i, MAP_ARRIVAL, UpFlowTable[i].pkt);
	}
    }
  Packet::free(p);
  Packet::free(map_);
  map_ = 0;

}

/*************************************************************************

*************************************************************************/
void MacDocsisCM::Initialize_entry(char dir, char tbindex)
{
  switch(dir) 
    {
    case 0:
      UpFlowTable[tbindex].upstream_record.sched_type = BEST_EFFORT;
      UpFlowTable[tbindex].upstream_record.gsize 	= 0;
      UpFlowTable[tbindex].upstream_record.ginterval 	= 0;
      UpFlowTable[tbindex].upstream_record.flag 	= 0;
      UpFlowTable[tbindex].upstream_record.PHS_profile= (PhsType)0;
      UpFlowTable[tbindex].upstream_record.flow_id	= 0;
      UpFlowTable[tbindex].upstream_record.classifier.src_ip= 0;
      UpFlowTable[tbindex].upstream_record.classifier.dst_ip= 0;
      UpFlowTable[tbindex].upstream_record.classifier.pkt_type= (packet_t)0;
      
      UpFlowTable[tbindex].alloc_list= 0;
      UpFlowTable[tbindex].state = 0;
      UpFlowTable[tbindex].pkt= 0;
      UpFlowTable[tbindex].bk_offwin= 0;
      UpFlowTable[tbindex].bk_offcounter= 0;
      UpFlowTable[tbindex].bk_offend= 0;
      UpFlowTable[tbindex].bk_offstart= 0;
      UpFlowTable[tbindex].max_retries= MAX_NUM_RETRIES;
      UpFlowTable[tbindex].num_retries= 0;
      UpFlowTable[tbindex].contention_on= 0;
      UpFlowTable[tbindex].pending= 0;
      UpFlowTable[tbindex].map_acktime= 0;
      UpFlowTable[tbindex].req_time= 0;
      UpFlowTable[tbindex].seq_num = 0;
      UpFlowTable[tbindex].OriginalFragPkt = 0;
      UpFlowTable[tbindex].frag_pkt = 0;
      UpFlowTable[tbindex].frag_data = 0;
      UpFlowTable[tbindex].curr_gsize = 0;
      UpFlowTable[tbindex].cmts_addr = 0;
      UpFlowTable[tbindex].num_pkt_snt= 0;
      UpFlowTable[tbindex].avg_queuing_delay= 0;
      UpFlowTable[tbindex].queuing_samples= 0;
      UpFlowTable[tbindex].avg_req_stime= 0;
      UpFlowTable[tbindex].avg_req_stimeBYTES= 0;
      UpFlowTable[tbindex].enque_time= 0;
      UpFlowTable[tbindex].num_delay_samples= 0;
      UpFlowTable[tbindex].total_piggyreq= 0;
//$A601
      UpFlowTable[tbindex].channelDelayCountSamples= 0;
      UpFlowTable[tbindex].channelDelayCount= 0.0;
      UpFlowTable[tbindex].channelDelayBytes= 0.0;

      UpFlowTable[tbindex].totalACKs= 0;
      UpFlowTable[tbindex].totalACKsFiltered= 0;
      
      UpFlowTable[tbindex].total_creq= 0;
      UpFlowTable[tbindex].total_fcoll= 0;
      UpFlowTable[tbindex].avg_fcont= 0;
      UpFlowTable[tbindex].fcont_count= 0;
//$A702
      UpFlowTable[tbindex].avg_TotalBackoff= 0;
      UpFlowTable[tbindex].avg_TotalBackoffCount= 0;
      UpFlowTable[tbindex].avg_Backoff= 0;
      UpFlowTable[tbindex].avg_BackoffTmp= 0;
      UpFlowTable[tbindex].avg_BackoffTmpCount= 0;
      UpFlowTable[tbindex].avg_BackoffCount= 0;

//$A701
      UpFlowTable[tbindex].total_drops_fragmentation=0;
      UpFlowTable[tbindex].total_collision_drops= 0;
      UpFlowTable[tbindex].total_num_req_denied = 0;
      UpFlowTable[tbindex].total_queue_drops= 0;
      UpFlowTable[tbindex].avg_slotspermap= 0;
      UpFlowTable[tbindex].avg_pkts= 0;
      UpFlowTable[tbindex].avg_bytes= 0;
      UpFlowTable[tbindex].num_bytes= 0;
      UpFlowTable[tbindex].num_pkts= 0;
    
      /* STATISTICS */
      UpFlowTable[tbindex].SID_num_sent_bytes= 0;
      UpFlowTable[tbindex].SID_num_sent_pkts= 0;
      
      UpFlowTable[tbindex].last_mfrtime= 0;
      UpFlowTable[tbindex].drop_count= 0;
    
      //UpFlowTable[tbindex].max_qsize= QUEUE_THRESHOLD;
      
      UpFlowTable[tbindex].ugsjitter = 0.0;
      UpFlowTable[tbindex].jitterSamples = 0;
      UpFlowTable[tbindex].last_granttime = 0.0;
      UpFlowTable[tbindex].nominal_alloctime = 0.004;
      UpFlowTable[tbindex].last_jittercaltime = 0.0;            
      UpFlowTable[tbindex].acceptance_rate = 0.0;
      UpFlowTable[tbindex].prev_acceptance_rate = 0.0;
      UpFlowTable[tbindex].wt_factor = 0.15;
      UpFlowTable[tbindex].num_slots_req = 1;
      UpFlowTable[tbindex].tokens_ = 0.0;
      UpFlowTable[tbindex].bucket_ = 24448;
      UpFlowTable[tbindex].init_ = 0;
      UpFlowTable[tbindex].ratecontrol = 0;
      UpFlowTable[tbindex].rate_ = 128000.0;
      reqFlag = 0;
      reqFlagCounter = 0;
      UpFlowTable[tbindex].totalConcatFrames = 0;
      UpFlowTable[tbindex].totalPacketsInConcatFrames = 0;
      break;
      
    case 1:
      DownFlowTable[tbindex].downstream_record.classifier.src_ip = 0;
      DownFlowTable[tbindex].downstream_record.classifier.dst_ip = 0;
      DownFlowTable[tbindex].downstream_record.classifier.pkt_type = (packet_t)0;
      DownFlowTable[tbindex].downstream_record.flow_id = 0;
      DownFlowTable[tbindex].downstream_record.PHS_profile = (PhsType)0;
      break;
    }
  return;  
}

/*************************************************************************
So a packet has been received by the PHY, 
and we want to send it up to the DOCSIS link...
*************************************************************************/
void MacDocsisCM::sendUp(Packet* p) 
{
}

/*************************************************************************
Send packets passed by LLC to physical layer..So, NO MAC level Management
Messages will be sent via this fucntion
*************************************************************************/
void MacDocsisCM::sendDown(Packet* p) 
{
  char tbindex;
  struct hdr_mac* mh = HDR_MAC(p);
  struct hdr_cmn* ch = HDR_CMN(p);
  struct hdr_docsisextd* eh = HDR_DOCSISEXTD(p);
  
  
#ifdef TRACE_DEBUG
  tracePoint("CM:sendDown:",cm_id,ch->size());
#endif

  eh->num_hdr = 0;

  if (debug_cm){
    printf("CM%d:sendDown(%lf):  Entered with packet size:  %d from higher layers  \n",
	   cm_id,
	   Scheduler::instance().clock(), 
	   ch->size());
  }
  
  //$A510
  serviceFlowObject *mySFObj = NULL;
  double curTime =   Scheduler::instance().clock();
  mySFObj = myUSSFMgr->getServiceFlow(p);
  if (mySFObj == NULL) {
    printf("MacDocsis::CM[%d](%lf): HARD ERROR (cm_id:%d): Could not find this service flow:  packet of size:%d,   and ptype:%d\n",
     cm_id,curTime,cm_id,ch->size(),ch->ptype());
    exit(1);
  }
  //problem is we don't know the channel
//  mySFObj->myStats.numberByteArrivals += ch->size();
//  mySFObj->myStats.numberPacketArrivals++;
//  mySFObj->lastPacketTime = curTime;
    mySFObj->newArrivalUpdate(p);


  /* Unlock IFQ */
  if(callback_) 
    {
      Handler *h = callback_;
      callback_ = 0;
      h->handle((Event*) 0);
    }   
  tbindex = classify(p, UPSTREAM);
  
#ifdef FILES_OK
#ifdef ARRIVAL_TRACE
    FILE* fp = NULL;
    fp = fopen("arrivals.out", "a+");
    fprintf(fp,"%lf %d %d %d %d %d \n",
	     Scheduler::instance().clock(), ch->size(), cm_id, UpFlowTable[tbindex].upstream_record.flow_id,
             UpFlowTable[tbindex].upstream_record.sched_type,UpFlowTable[tbindex].state);
   
    fclose(fp);
#endif
#endif

  if (UpFlowTable[tbindex].debug)
    {
      printf("CM%d(flow-id %d) :sendDown(%lf): Received an upper layer pkt of size %d and sched_type:%d (cur state:%d) \n",
	     cm_id, UpFlowTable[tbindex].upstream_record.flow_id,
	     Scheduler::instance().clock(), ch->size(), UpFlowTable[tbindex].upstream_record.sched_type,UpFlowTable[tbindex].state);
      
      dump_pkt(p);
    }
  
  MACstats.total_num_appbytesUS += ch->size(); 
  MACstats.total_num_appPktsUS++;
  ApplyPhs(p, tbindex);
  HandleOutData(p,tbindex); 
}

/*************************************************************************
 Packet enters the SM for respective flow from this function 
*************************************************************************/
void MacDocsisCM::HandleOutData(Packet* p, char tbindex)
{
  struct hdr_cmn* ch = HDR_CMN(p);
  struct hdr_docsis* dh = HDR_DOCSIS(p);
  struct hdr_mac* mh = HDR_MAC(p);

#ifdef TRACE_DEBUG
  tracePoint("CM:HandleOutData:",cm_id,0);
#endif
  
  /* Make docsis  & ethernet header */
  /* 
     Ethernet header has to be properly updated as CM node is also
     simulating the end-host 
  */
  mh->set(MF_DATA,index_); /* To set the MAC src-addr and type */
  ch->size() += ETHER_HDR_LEN;
    
  dh->dshdr_.fc_type = DATA;
  dh->dshdr_.fc_parm = 0;    //just init to 0 = not used for data
  dh->dshdr_.ehdr_on = 0;
  dh->dshdr_.mac_param = 0;
  dh->dshdr_.len = ch->size();
  ch->size() += DOCSIS_HDR_LEN;
  if (UpFlowTable[tbindex].debug)
     printf("CM%d :HandleOutData(%lf): Packet arrived,  Sending frame of size %d Upstream \n", 
	       cm_id,Scheduler::instance().clock(),ch->size());
    
  /* Pass the packet to state-machine */  
  if (UpFlowTable[tbindex].upstream_record.sched_type == UGS)
    {
      (this->*UGSswitch[UpFlowTable[tbindex].state])(tbindex, PKT_ARRIVAL , p);
    }
  else if (UpFlowTable[tbindex].upstream_record.sched_type == RT_POLL)
    {
      (this->*RTPOLLswitch[UpFlowTable[tbindex].state])(tbindex, PKT_ARRIVAL, p);
    }
  else if (UpFlowTable[tbindex].upstream_record.sched_type == BEST_EFFORT)
    {
      (this->*BEFFORTswitch[UpFlowTable[tbindex].state])(tbindex, PKT_ARRIVAL, p);
    }
  return;
}

/*************************************************************************

*************************************************************************/
void MacDocsisCM::HandleOutMgmt(Packet* p, char tbindex)
{

//A700
   struct hdr_cmn* ch = HDR_CMN(p);
   printf("CM%d :HandleOutMgmt%lf): HARD ERROR HANDLEOUTMGMT ,  Packet arrived of size:%d   \n", 
	       cm_id,Scheduler::instance().clock(),ch->size());


  if (UpFlowTable[tbindex].upstream_record.sched_type == UGS)
    {
      (this->*UGSswitch[UpFlowTable[tbindex].state])(tbindex, PKT_ARRIVAL, p);
    }
  else if (UpFlowTable[tbindex].upstream_record.sched_type == RT_POLL)
    {
      (this->*RTPOLLswitch[UpFlowTable[tbindex].state])(tbindex, PKT_ARRIVAL, p);
    }
  else if (UpFlowTable[tbindex].upstream_record.sched_type == BEST_EFFORT)
    {
      (this->*BEFFORTswitch[UpFlowTable[tbindex].state])(tbindex, PKT_ARRIVAL, p);
    }
  return;
}

/*************************************************************************
  *
  * Routine:   void MacDocsisCM::SendData(Packet *p, char tbindex)
  *
  * Inputs:
  *
  * Outputs:
  *
  * Explanation:  This is called when a MAP is received and we
  *  have data ready to be sent.
  *  For UGS and RT_POLL, it sends the packet.
  *  For BE, need to check if need to use concat and/or fragmentation
  *************************************************************************/
void MacDocsisCM::SendData(Packet *p, char tbindex)
{
  int rc = 0;
  Packet* q;
  int concat_req = 0;
  q = UpFlowTable[tbindex].pkt;
  
  struct hdr_cmn* ch = HDR_CMN(q);
  docsis_chabstr *ch_abs = docsis_chabstr::access(q);    
  int idleChannel = 0;
//$A510
  serviceFlowObject *mySFObj = NULL;
  double curTime =   Scheduler::instance().clock();
  mySFObj = myUSSFMgr->getServiceFlow(p);
  if (mySFObj == NULL) {
    printf("CM:SendData(%lf): HARD ERROR (cm_id:%d): Could not find this service flow:  packet of size:%d,   and ptype:%d\n",
     curTime,cm_id,ch->size(),ch->ptype());
//    return;
    exit(1);
  }

  UpFlowTable[tbindex].avg_req_stime += (Scheduler::instance().clock() - UpFlowTable[tbindex].enque_time);
  UpFlowTable[tbindex].avg_req_stimeBYTES += ch->size();
  UpFlowTable[tbindex].num_delay_samples++;

//$A601
   UpFlowTable[tbindex].channelDelayCountSamples++;
   UpFlowTable[tbindex].channelDelayCount += (Scheduler::instance().clock() - UpFlowTable[tbindex].enque_time);
   UpFlowTable[tbindex].channelDelayBytes += ch->size();
   CMstats.channelDelayCountSamples++;
   CMstats.channelDelayCount += (Scheduler::instance().clock() - UpFlowTable[tbindex].enque_time);
   CMstats.channelDelayBytes += ch->size();

  if (UpFlowTable[tbindex].debug) {
     printf("CM%d(flow-id %d):sendData(%lf): NRAD UPDATE CASE 1  CASE 3, delay:%lf, avg req delay:%lf, Bytes:%lf, #samples:%d \n",
	       cm_id,UpFlowTable[tbindex].upstream_record.flow_id,
	       Scheduler::instance().clock(),
               Scheduler::instance().clock() - UpFlowTable[tbindex].enque_time,
               UpFlowTable[tbindex].avg_req_stime / UpFlowTable[tbindex].num_delay_samples,
               UpFlowTable[tbindex].avg_req_stimeBYTES,
               UpFlowTable[tbindex].num_delay_samples);
  }
  CMstats.avg_req_stime_status += (Scheduler::instance().clock() - UpFlowTable[tbindex].enque_time);
  CMstats.avg_req_stimeBYTES_status += ch->size();
  CMstats.num_delay_samples_status++;


  
  if (UpFlowTable[tbindex].debug) {
     printf("CM%d(flow-id %d):sendData: ENTRY sending a packet at %lf,  check req_time and cmts ack  \n",
	       cm_id,UpFlowTable[tbindex].upstream_record.flow_id,
	       Scheduler::instance().clock());
       MapSentAfterReq(tbindex);
  }
  
  
  switch(UpFlowTable[tbindex].upstream_record.sched_type) 
  {
    case UGS:
    case RT_POLL:
      if (UpFlowTable[tbindex].debug) {
	printf("CM%d(flow-id %d) :sendData:RT_POLL: sending a packet at %lf \n",
	       cm_id,
	       UpFlowTable[tbindex].upstream_record.flow_id,
	       Scheduler::instance().clock());
//        packetTrace(q, 1, "CM:sendData:RT_POLL"); 
    }

    MACstats.total_num_frames_US++;

      
	  //JJM WRONG - the MAP will indicate which channel to use.... but for now we just do this
      idleChannel = myMedium->findIdleChannel(CHANNEL_DIRECTION_UP);
      if (debug_cm)
        printf(" MacDocsisCm::SendData(%lf): Reached the US sending stage using channel %d \n ",Scheduler::instance().clock(),idleChannel);

      ch_abs->channelNum=idleChannel;
  
      /* STATISTICS*/
      UpFlowTable[tbindex].num_pkts++ ;
      UpFlowTable[tbindex].num_bytes += ch->size();
      MACstats.total_num_sent_frames++;
      MACstats.total_num_sent_bytes += ch->size(); 
      MACstats.total_num_BW_bytesUP += ch->size(); 

//$A510
      mySFObj->updateStats(p,idleChannel,mySFObj->getBondingGroup());

      CMstats.total_BW_bytesUP_status += ch->size(); 
      CMstats.total_pktsSentUP_status++;
      
      //#ifdef TRACE_CM_UP_DATA //----------------------------------------------
      if (debug_cm)
        printf("CM%dup(flow-id: %d) %lf %f %d \n",
	       cm_id,UpFlowTable[tbindex].upstream_record.flow_id,
	       Scheduler::instance().clock(), 
	       MACstats.total_num_sent_bytes,
	       ch->size());
      //#endif //---------------------------------------------------------------
      
      /* Start a timer that expires when the packet transmission is complete. */
#ifdef US_RATE_CONTROL
      if (UpFlowTable[tbindex].ratecontrol == 1)
        USRateMeasure(tbindex,q);
#endif

      if (debug_cm)
        printf(" MacDocsisCm[%d]::SendData(%lf): Reached the US sending stage using channel %d \n",cm_id,Scheduler::instance().clock(),idleChannel);

      MacSendFrame(q,idleChannel);
      break;
      
    case BEST_EFFORT:
      /* Check whether concatenation is required */
      if ((bit_on(UpFlowTable[tbindex].upstream_record.flag,CONCAT_ENABLE_BIT)) && 
	  (!bit_on(UpFlowTable[tbindex].upstream_record.flag,CONCAT_ON_BIT)) && 
	  (!bit_on(UpFlowTable[tbindex].upstream_record.flag,FRAG_ON_BIT)) && 
	  (bit_on(UpFlowTable[tbindex].upstream_record.flag,FRAG_ENABLE_BIT)))
	{
	  concat_req = check_concat_req(p,tbindex);
	}
      else if (bit_on(UpFlowTable[tbindex].upstream_record.flag,CONCAT_ON_BIT))
	{
	  concat_req = 1;
	}
      if (UpFlowTable[tbindex].debug) {
	printf("CM%d(flow-id %d):sendData:BEST_EFFORT: sending a packet at %lf,  check req_time and cmts ack  \n",
	       cm_id,UpFlowTable[tbindex].upstream_record.flow_id,
	       Scheduler::instance().clock());
       MapSentAfterReq(tbindex);
      }
      
      if (concat_req) {
	    rc = send_concat(q,tbindex);
//$A701
//        if (rc == -1)
//          return;
      }
      else {
	    rc =decide_frag(q,tbindex);
      }
      break;
  }
  
  /* Update the statistics variable..*/
  double curr_time = Scheduler::instance().clock();
  
  if (curr_time - UpFlowTable[tbindex].last_mfrtime >= 1.0)
  {
      UpFlowTable[tbindex].last_mfrtime = curr_time;

      //UpFlowTable[tbindex].avg_pkts = (UpFlowTable[tbindex].avg_pkts + UpFlowTable[tbindex].num_pkts )/ 2;
      //UpFlowTable[tbindex].avg_bytes = (UpFlowTable[tbindex].avg_bytes + UpFlowTable[tbindex].num_bytes )/ 2;
      //UpFlowTable[tbindex].num_pkts = 0;
      //UpFlowTable[tbindex].num_bytes = 0;      
  }  
  if (UpFlowTable[tbindex].debug) {
     printf("CM%d(flow-id %d):sendData: EXIT sending a packet at %lf,  check req_time and cmts ack  \n",
	       cm_id,UpFlowTable[tbindex].upstream_record.flow_id,
	       Scheduler::instance().clock());
       MapSentAfterReq(tbindex);
  }
  return;
}

/*************************************************************************
 * routine: int MacDocsisCM::SendReq(char tbindex, Packet* p)
 *
 * explanation:
 *   This is called when the CM wants to send a request to the CMTS
 *   It is invoked off the REQ_TIMER thread.
 *   The routine  check_concat_req(tbindex) is called to see if
 *   we can send a concat request.  
 *   The routine also calls beffort_ratecheck to see if it
 *   is even allowed to request bandwidth yet (subject to
 *   rate control).
 *      If it's not - just delete the request packet and return
 *
   We init len to the number of packets in the queue (if any)
 *       int len = len_queue(tbindex);
 *   But we need to make sure that len is not more than the max number
 *   allowed in a concat frame (remember the max_concat_threshold
 *   has been internally converted to represent the max number of
*    packets that can be inserted minus 1...)
 *     if ((len > 0) && (len > UpFlowTable[tbindex].max_concat_threshhold))
 *       len = UpFlowTable[tbindex].max_concat_threshhold;
 *
 *   When we get the grant,we tell send_concat how many packets to
 *   put into the frame with this state variable:
 *            UpFlowTable[tbindex].numberPacketsInNextConcat_ = 0;
 *   
 * inputs
 * char tbindex:
 * Packet *p: This is the current packet that we want to send.
 * NOte that we might add additional packets to it.
 *
 * outputs
 *   Returns a  0 if an error occurs and the request is not sent,
 *   else a 1.
 *
*************************************************************************/
int MacDocsisCM::SendReq(char tbindex, Packet* p)
{
  struct hdr_cmn* ch = HDR_CMN(p);
  Packet* n = Packet::alloc();
  hdr_cmn* chn = HDR_CMN(n);
  hdr_docsis* dh = HDR_DOCSIS(n);
  docsis_chabstr *ch_abs = docsis_chabstr::access(n);    
  int concat_req = 0;
  u_int32_t req_size;
  Packet* r;
  int num,i;
//  u_char slots;
  u_int32_t slots;
  double curTime =   Scheduler::instance().clock();
  FILE* fp = NULL;
  int len = 0;
  Packet *tmpPkt;

//$A510
  serviceFlowObject *mySFObj = NULL;
  mySFObj = myUSSFMgr->getServiceFlow(p);
  if (mySFObj == NULL) {
    printf("CM:SendReq(%lf): HARD ERROR (cm_id:%d): Could not find the service flow\n", curTime,cm_id);
    exit(1);
  }


  //$A510
//$A701 : is it possible that p is MGMT?  IF so this is wrong....
//  int len = myUSSFMgr->len_queue(p,tbindex);
//  int    len = len_queue(tbindex);

//$A701
  len = myUSSFMgr->len_queue(p,tbindex);
//  if (myUSSFMgr->anyPackets() == TRUE)
  if (0)
  {
    mySFObj = myUSSFMgr->nextSF();
    if (mySFObj == NULL) {
       printf("CM:Send_Req(%lf): (cm_id:%d): HARD ERROR, nextSF returns NULL SF Object\n",
           Scheduler::instance().clock(),cm_id);
        exit(1);
    }

    tmpPkt = mySFObj->getPacket(0);
    if (tmpPkt == NULL) {
         printf("CM:Send_Req(%lf): (cm_id:%d): HARD ERROR, nextSF is EMPTY??\n",
           Scheduler::instance().clock(),cm_id);
         exit(1);
    }
    tbindex = classify(tmpPkt, UPSTREAM);
    len = mySFObj->myPacketCount;
  }
    

  chn->uid() = 0; 
  //chn->ptype() = PT_MAC;
  chn->ptype() = PT_DOCSISREQ;  
  chn->iface() = -2;
    
  if (UpFlowTable[tbindex].debug) {
    printf("CM%d(flow-id %d):SendReq:(%lf)Entered with a packet of size: %d, and length of SF queue is %d\n",
	   cm_id,
	   UpFlowTable[tbindex].upstream_record.flow_id,curTime,
	   ch->size(),len);
//For all queue elements, display the latest pkt delay
    if ((len > 0) && (cm_id == 1)) {
      myUSSFMgr->printShortSummary();
    }
  }
  
  if (bit_on(UpFlowTable[tbindex].upstream_record.flag,FRAG_ON_BIT))
    req_size = UpFlowTable[tbindex].frag_data;
  else
    req_size = ch->size();
  
  if (bit_on(UpFlowTable[tbindex].upstream_record.flag,PIGGY_ENABLE_BIT))
    req_size += 3; /* For piggybacked request */

  
  UpFlowTable[tbindex].curr_reqsize = 0;
  UpFlowTable[tbindex].numberBytesInNextConcat_ = 0;
  UpFlowTable[tbindex].numberPacketsInNextConcat_ = 0;
  concat_req = check_concat_req(p,tbindex);
  if (UpFlowTable[tbindex].debug)
    printf("CM%d(flow-id %d):SendReq:(%lf)concat_req flag: %d\n",
	   cm_id,
	   UpFlowTable[tbindex].upstream_record.flow_id,curTime,
	   concat_req);

  
  /* What about the MAC header 6 byte overhead for the case of no concat  ???? */
  if (!(bit_on(UpFlowTable[tbindex].upstream_record.flag,FRAG_ON_BIT)) &&(concat_req))
  {
      if (UpFlowTable[tbindex].debug)
	printf("CM%d(flow-id %d):SendReq:(%lf) Decided to send a Concatenated req (req_size:%d) \n",
	       cm_id,
	       UpFlowTable[tbindex].upstream_record.flow_id,curTime,
	       req_size);
      /* Add  6 bytes for Concat frame header */
      req_size += 6;
      req_size -= 3; /* For piggybacked request since piggy req will not be sent on a concat frame */
      
  //$A510
      len = myUSSFMgr->len_queue(p,tbindex);
//      len = len_queue(tbindex);
      if (UpFlowTable[tbindex].debug)
        printf("CM%d(flow-id %d):SendReq:(%lf) Requesting to carry %d packets, cur req size:%d (limit:%d(pkts), %d(bytes))\n",
	       cm_id, UpFlowTable[tbindex].upstream_record.flow_id,curTime, 
	       len+1, req_size, UpFlowTable[tbindex].max_concat_threshhold,MAX_CONCAT_BYTES);


      if ((len > 0) && (len > UpFlowTable[tbindex].max_concat_threshhold))
        len = UpFlowTable[tbindex].max_concat_threshhold;

      //Change:  for len number of packets, we want to make sure we don't cause fragmentation.
      //So modify len so that the number of bytes in the len packets will fit in a single frame.
      //Code  a routine len  = find_len(maxdatasize,len);
      //TODO
      //By this point, len represents the number of packets to go in a concatenate frame.

    if (UpFlowTable[tbindex].debug)
	printf("CM%d(flow-id %d):SendReq:(%lf) Actual request to carry %d packets (limit:%d,qlen=%d)\n",
	       cm_id, UpFlowTable[tbindex].upstream_record.flow_id,curTime,
	       len+1, UpFlowTable[tbindex].max_concat_threshhold,myUSSFMgr->len_queue(p,tbindex));
//	       len+1, UpFlowTable[tbindex].max_concat_threshhold,len_queue(tbindex));
  //$A510

      for (i = 0; i <= (len-1); i++)
	{
//	  r = pkt_lookup(tbindex,i);
  //$A510
      if (UpFlowTable[tbindex].debug)
        printf("MacDocsisCM:SendReq:(%lf): CMID:%d: get the packet at the i'th position : %d \n", curTime,cm_id,i);
      r =  mySFObj->getPacket(i);
	  
	  if (r == NULL)
	  {
	     printf("CM%d(flow-id %d) :MacDocsisCM:SendReq error, pkt_lookup returned NULL, exiting\n",
		     cm_id,
		     UpFlowTable[tbindex].upstream_record.flow_id);
	      exit(1);
	  }


	  struct hdr_cmn* chr = HDR_CMN(r);
	  
	  /* What about the MAC header overhead for each inner frame??
	     for the case of no concat  ???? */
	  req_size += chr->size();
	  //JJM WRONG
      slots = (int) req_size / bytes_pminislot;
      if ((req_size % bytes_pminislot) != 0)
         slots += 1;
	  //255 is max but req_size is data only....
      if ((slots > 240) || (req_size > MAX_CONCAT_BYTES))
      {
	    req_size -= chr->size();
        if (req_size < 0)
          req_size=0;
	    UpFlowTable[tbindex].numberPacketsInNextConcat_ = i-1;
	    if (UpFlowTable[tbindex].numberPacketsInNextConcat_ < 0)
	      UpFlowTable[tbindex].numberPacketsInNextConcat_ = 0;

        UpFlowTable[tbindex].numberBytesInNextConcat_ = req_size;
        UpFlowTable[tbindex].curr_reqsize = req_size;


	    if (UpFlowTable[tbindex].debug)
	      printf("CM%d(Flow-id %d):SendReq:(%lf)Can't add any more packets.... size: %d, req_size:%d (slots:%d), numberPacketsInNextConcat_:%d\n",
		   cm_id, UpFlowTable[tbindex].upstream_record.flow_id,
		   curTime, chr->size(), req_size,slots,
		   UpFlowTable[tbindex].numberPacketsInNextConcat_);
	    break;
	  }
	  //This is the normal path-  make sure this is set with the correct
	  //number of packets to ADD into the concatonated frame
	  else {
	    UpFlowTable[tbindex].numberPacketsInNextConcat_ = len;
	    UpFlowTable[tbindex].numberBytesInNextConcat_ = req_size;
	  }
	  
	  if (UpFlowTable[tbindex].debug)
	    printf("CM%d(Flow-id %d):SendReq:(%lf)Add frame to concat request of size: %d, req_size:%d bytes\n",
		   cm_id, UpFlowTable[tbindex].upstream_record.flow_id,
		   curTime, chr->size(), req_size);
	}
      set_bit(&UpFlowTable[tbindex].upstream_record.flag, CONCAT_ON_BIT,ON);
  
    }	

    //Note: the request may or may not be concatonated here ...
    //The CMTS actually does not see or care if the request 
    //is concatonated.
  //JJM WRONG
    slots = (int) req_size / bytes_pminislot;

  if ((req_size % bytes_pminislot) != 0)
    slots += 1;

  if (slots > 255) {
    printf("\nSendReq:  ERROR:  requesting slots > 255 (%d)\n",slots);
    exit(0);
  }
  
  dh->dshdr_.fc_type = MAC_SPECIFIC;
  dh->dshdr_.fc_parm = REQUEST;
  dh->dshdr_.ehdr_on = 0;
  dh->dshdr_.mac_param = (u_char) slots;
  dh->dshdr_.len = UpFlowTable[tbindex].upstream_record.flow_id;
  chn->size() = DOCSIS_HDR_LEN;

//$A701
   UpFlowTable[tbindex].curr_reqsize = slots * bytes_pminislot;

#ifdef US_RATE_CONTROL
 if (UpFlowTable[tbindex].ratecontrol == 1)
 {
  int ratecheck_returnval = 0;
  ratecheck_returnval = beffort_ratecheck(tbindex, REQ_TIMER, p,slots);

  if(ratecheck_returnval != 1 && ratecheck_returnval != -1) {
    printf("Error in beffort_ratecheck_returnval contention %d\n",ratecheck_returnval);

    exit(0);
  }
//JJM BUG : this seems wrong. Why drop the packet ?
  if(ratecheck_returnval == -1) {
    if (UpFlowTable[tbindex].debug)
      printf("CM%d(flow-id %d):SendReq:(%lf) Insufficient tokens for this bw request- delete req pkt (numberPacketsInNextConcat_:%d)\n",
	   cm_id, UpFlowTable[tbindex].upstream_record.flow_id,curTime,
          UpFlowTable[tbindex].numberPacketsInNextConcat_);
    Packet::free(n);
//What about any other state ?
//	    UpFlowTable[tbindex].numberPacketsInNextConcat_ = len;

    return 0;
  }
 }
#endif

//$A701
  if (UpFlowTable[tbindex].debug)
    printf("CM%d(Flow-id %d):SendReq:(%lf) requesting this number of slots:%d, this number of bytes:%d\n",
		   cm_id, UpFlowTable[tbindex].upstream_record.flow_id,
		   curTime,slots, UpFlowTable[tbindex].curr_reqsize);

//$A300
 //JJM WRONG
  int idleChannel = myMedium->findIdleChannel(CHANNEL_DIRECTION_UP);

  if (UpFlowTable[tbindex].debug)
    printf("MacDocsisCM:: Sendreq: Reached the US sending stage using channel %d at instance %lf \n",idleChannel,curTime);

  if(idleChannel == -1){
           printf("Sending from CM %d but could not find channel: return 0\n", cm_id);
           Packet::free(n);
           return 0;
  }
 
  if (UpFlowTable[tbindex].debug)
    printf("CM%d(Flow-id %d):SendReq:(%lf)Sending req for %d bytes (%d slots)\n",
	   cm_id,
	   UpFlowTable[tbindex].upstream_record.flow_id,
       curTime,
	   req_size,dh->dshdr_.mac_param);
    	

  /* STATISTICS */
  UpFlowTable[tbindex].req_time = Scheduler::instance().clock();
  UpFlowTable[tbindex].num_pkts++ ;
  UpFlowTable[tbindex].num_bytes += chn->size();
  MACstats.total_num_sent_frames++;
  MACstats.total_num_sent_bytes += chn->size(); 
  MACstats.total_num_BW_bytesUP += chn->size(); 
  CMstats.total_BW_bytesUP_status += chn->size(); 
  CMstats.total_pktsSentUP_status++;

//$A510
   mySFObj->updateStats(p,idleChannel,mySFObj->getBondingGroup());

  UpFlowTable[tbindex].total_creq++;
  
  //#ifdef TRACE_CM_UP_DATA //---------------------------------------------------------
  if (debug_cm)
    printf("CM%dup(flow-id: %d) %lf %f %d \n",
	   cm_id,
	   UpFlowTable[tbindex].upstream_record.flow_id,
	   Scheduler::instance().clock(),
	   MACstats.total_num_sent_bytes,
	   chn->size());
  //#endif //--------------------------------------------------------------------------

    MACstats.total_num_frames_US++;
  
  ch_abs->channelNum=idleChannel;

  if (UpFlowTable[tbindex].debug)
    printf("CM%d(flow-id %d):SendReq:(%lf) Request Mesage: total frame  size(bytes): %d\n",
	   cm_id, UpFlowTable[tbindex].upstream_record.flow_id,curTime,
	   chn->size());
	
//$A700
#ifdef FILES_OK
    fp = fopen("CONCATREQ.out", "a+");
    fprintf(fp,"%f\t%3d\t%4d\t%4d\t%9d\t%9d\t%9d\t%3d\t%3d\t%3d\t%9d\t%9d\n", 
	   curTime, cm_id, UpFlowTable[tbindex].upstream_record.flow_id,
	   req_size,(int)dh->dshdr_.mac_param,
	   chn->size(),UpFlowTable[tbindex].total_creq,
      UpFlowTable[tbindex].total_collision_drops,
      UpFlowTable[tbindex].total_num_req_denied,
      UpFlowTable[tbindex].total_drops_fragmentation,
      CMstats.total_num_cslots,CMstats.total_num_dslots);
    fclose(fp);
#endif

  MacSendFrame(n,idleChannel);
  //$A510
//  len = myUSSFMgr->len_queue(p,tbindex);
//  len =  len_queue(tbindex);


#ifdef TIMINGS
//	printf("3 %lf %d %d 1\n",Scheduler::instance().clock(),
//			UpFlowTable[tbindex].upstream_record.flow_id,cm_id); 
        timingsTrace(p,3);
#endif

  return 1;
}

/*************************************************************************
* function: int MacDocsisCM::check_concat_req(char tbindex)
*
* inputs:  
*  char tbindex : 
*
* outputs:
*  int : returns a 1 if there is at least one packet waiting in the queue
*        AND concat is enabled.
*
*************************************************************************/
int MacDocsisCM::check_concat_req(Packet *p, char tbindex)
{
  //$A510
  int len = myUSSFMgr->len_queue(p,tbindex);
//int len = len_queue(tbindex);

  if (UpFlowTable[tbindex].debug)
    printf("CM%d(flow-id %d):check_concat_req:(%lf) len_queue: %d, max_thresh: %d, concat enable bit: %d \n",
	   cm_id, UpFlowTable[tbindex].upstream_record.flow_id,Scheduler::instance().clock(),
	   len,UpFlowTable[tbindex].max_concat_threshhold,
	   bit_on(UpFlowTable[tbindex].upstream_record.flag,CONCAT_ENABLE_BIT));  

//  if (((len_queue(tbindex)) >= UpFlowTable[tbindex].max_concat_threshhold) && 
//      (bit_on(UpFlowTable[tbindex].upstream_record.flag,CONCAT_ENABLE_BIT)))  
  if ((len > 0) && 
      (bit_on(UpFlowTable[tbindex].upstream_record.flag,CONCAT_ENABLE_BIT)))  
    {
      return 1;
    }
  else 
  {
    return 0;
  }
}

/*************************************************************************

*************************************************************************/
int MacDocsisCM::check_frag_req(u_int32_t size,char tbindex)
{
	//JJM WRONG DO I need this 
  if ( size + defaultUSOverhead_bytes > UpFlowTable[tbindex].curr_gsize)
    return 1;
  else 
    return 0;
}

/*************************************************************************
  * function: int MacDocsisCM::decide_frag(Packet* p,char tbindex)
  *
  * explanation:  This routine is called at the slot time that has been
  *   allocated as the beginning transmission time for this packet.
  *   It is called only for BE traffic.
  * LOGIC:
  *    -First see if this packet is a portion of a fragmented frame
  *     that is being sent.
  *    -Else, see if this packet needs to be sent as a fragment
  *    -Else, see this packet needs a piggyback request 
  *
  * inputs
  *   Packet *p:  frame (not concatenated)
  *   char tbindex:  index to US flow table
  *
  * outputs:
  *   rc = 1 if the packet is sent fragmented, a 0 if NOT.
  *        rc = -1 if we can not send due to no tokens
*************************************************************************/
int MacDocsisCM::decide_frag(Packet* p,char tbindex)
{
  int rc = 0;
  struct hdr_cmn* ch = HDR_CMN(p);
  docsis_chabstr *ch_abs = docsis_chabstr::access(p);    
  int frag_req = 0;
//$A510
  double curTime =   Scheduler::instance().clock();
  serviceFlowObject *mySFObj = NULL;
  mySFObj = myUSSFMgr->getServiceFlow(p);
  if (mySFObj == NULL) {
    printf("CM:SendData(%lf): HARD ERROR (cm_id:%d): Could not find this service flow:  packet of size:%d,   and ptype:%d\n",
     curTime,cm_id,ch->size(),ch->ptype());
//    return;
    exit(1);
  }
  
  if ((bit_on(UpFlowTable[tbindex].upstream_record.flag,FRAG_ENABLE_BIT)) && 
      (bit_on(UpFlowTable[tbindex].upstream_record.flag,FRAG_ON_BIT)))
    {
      /* TODO:  increment statistics for this case */
      if (UpFlowTable[tbindex].debug)
	    printf("CM%d(flow-id %d):decide_frag: (FRAG ON )Send fragment  of size %d (%d slots) \n",
	       cm_id, UpFlowTable[tbindex].upstream_record.flow_id, ch->size(),ch->size()/bytes_pminislot);
      rc = 1;   
      send_frag_data(p,tbindex);	
    }
  else if ((bit_on(UpFlowTable[tbindex].upstream_record.flag,FRAG_ENABLE_BIT)) && 
	   (!bit_on(UpFlowTable[tbindex].upstream_record.flag,FRAG_ON_BIT)))
    {
      if (UpFlowTable[tbindex].debug)
	    printf("CM%d(flow-id %d):decide_frag: (NOT FRAG ON) Check if need to frag pkt of size %d (%d slots) \n",
	       cm_id, UpFlowTable[tbindex].upstream_record.flow_id, ch->size(),ch->size()/bytes_pminislot);
      /* TODO:  increment statistics for this case */
      frag_req = check_frag_req(ch->size(),tbindex);
      
      if (frag_req)
	{
	  if (UpFlowTable[tbindex].debug)
	    printf("CM%d(flow-id %d):decide_frag:Decided to fragment pkt (currently not ON) of size %d (%d slots) \n",
		   cm_id, UpFlowTable[tbindex].upstream_record.flow_id, ch->size(),ch->size()/bytes_pminislot);

	  /* TODO:  increment statistics for this case */
      rc = 1;   
	  send_frag_data(p,tbindex);
	}
      else
	{
	  if (UpFlowTable[tbindex].debug)
	    printf("CM%d(flow-id %d):decide_frag:Frag is enabled but don't have to - just send piggy req\n",
		   cm_id, UpFlowTable[tbindex].upstream_record.flow_id);
	  /*  Frag is eanbled but not on, Can send piggyback req */
	  if ((bit_on(UpFlowTable[tbindex].upstream_record.flag,PIGGY_ENABLE_BIT)) && 
	      (!bit_on(UpFlowTable[tbindex].upstream_record.flag,NO_PIGGY_BIT)))
	    {
	      FillPiggyReq(tbindex,p);
	    }
	  else if ((bit_on(UpFlowTable[tbindex].upstream_record.flag,NO_PIGGY_BIT)) && 
		   (bit_on(UpFlowTable[tbindex].upstream_record.flag,PIGGY_ENABLE_BIT)))
	   {
//$701


	      if (UpFlowTable[tbindex].debug)
             printf("CM%d(flow-id %d): 1 No Piggyback req is sent on a Concatenated frame\n",
		       cm_id,
		       UpFlowTable[tbindex].upstream_record.flow_id);
	      /* Set the bit to indicate that no piggyback req was sent  */

	      
	      set_bit(&UpFlowTable[tbindex].upstream_record.flag, PIGGY_NOT_SEND,ON);
	      set_bit(&UpFlowTable[tbindex].upstream_record.flag, NO_PIGGY_BIT,OFF);	      

//TRY THIS?
//$A701
//	      FillPiggyReq(tbindex,p);
//	      set_bit(&UpFlowTable[tbindex].upstream_record.flag, PIGGY_NOT_SEND,OFF);
	  }
	  else if (bit_on(UpFlowTable[tbindex].upstream_record.flag,NO_PIGGY_BIT))
	  {
	    if (UpFlowTable[tbindex].debug)
		{
		  if  (bit_on(UpFlowTable[tbindex].upstream_record.flag,PIGGY_ENABLE_BIT))
		    printf("CM%d(flow-id %d): 2 No Piggyback req is sent on a Concatenated frame\n",
			   cm_id, UpFlowTable[tbindex].upstream_record.flow_id);
		}
	      set_bit(&UpFlowTable[tbindex].upstream_record.flag, NO_PIGGY_BIT,OFF);	      
//TRY THIS?
//$A701
//	      FillPiggyReq(tbindex,p);
//	      set_bit(&UpFlowTable[tbindex].upstream_record.flag, PIGGY_NOT_SEND,OFF);
	  }	  


      //JJM WRONG : the map will indicate the channel....but for now this is OK
      int idleChannel = myMedium->findIdleChannel(CHANNEL_DIRECTION_UP);
          if (debug_cm)
            printf("MacDocsisCm:: decide_frag: Reached the US sending stage using channel %d at instance %lf\n",idleChannel, Scheduler::instance().clock());
	  ch_abs->channelNum=idleChannel;
	  
	  /* STATISTICS */
	  UpFlowTable[tbindex].num_pkts++ ;
	  UpFlowTable[tbindex].num_bytes += ch->size();
	  MACstats.total_num_sent_frames++;
	  MACstats.total_num_sent_bytes += ch->size(); 
	  MACstats.total_num_BW_bytesUP += ch->size();

      CMstats.total_BW_bytesUP_status += ch->size(); 
      CMstats.total_pktsSentUP_status++;
//$A510
      mySFObj->updateStats(p,idleChannel,mySFObj->getBondingGroup());
 
	  //#ifdef TRACE_CM_UP_DATA  //--------------------------------------------------
	  if (debug_cm)
	    printf("CM%dup(flow-id: %d) %lf %f %d\n",
		   cm_id,
		   UpFlowTable[tbindex].upstream_record.flow_id,Scheduler::instance().clock(),
		   MACstats.total_num_sent_bytes, ch->size());
	  //#endif //--------------------------------------------------------------------

	  if (UpFlowTable[tbindex].debug)
	    printf("CM%d(flow-id %d):decide_frag: send frame (size:%d) WITHOUT fragmentation (but frag allowed) at %lf \n",
		   cm_id, UpFlowTable[tbindex].upstream_record.flow_id, 
		   ch->size(), Scheduler::instance().clock());
	  
    MACstats.total_num_frames_US++;
	  /* Start a timer that expires when the packet transmission is complete. */
#ifdef US_RATE_CONTROL
          if (UpFlowTable[tbindex].ratecontrol == 1)
            USRateMeasure(tbindex,p);
#endif

      MacSendFrame(p,idleChannel);

	}
    }
  else if ((!bit_on(UpFlowTable[tbindex].upstream_record.flag,FRAG_ENABLE_BIT)) && 
	   (!bit_on(UpFlowTable[tbindex].upstream_record.flag,FRAG_ON_BIT)))
    {
      /*  
	  Ok No fragmentation needs to be done
	  Check whether piggybacking is enabled 
	  and can it be done
      */
    if ((bit_on(UpFlowTable[tbindex].upstream_record.flag,PIGGY_ENABLE_BIT)) && 
	  (!bit_on(UpFlowTable[tbindex].upstream_record.flag,NO_PIGGY_BIT)))
	{
	  FillPiggyReq(tbindex,p);
	}
    else if ((bit_on(UpFlowTable[tbindex].upstream_record.flag,NO_PIGGY_BIT)) && 
	       (bit_on(UpFlowTable[tbindex].upstream_record.flag,PIGGY_ENABLE_BIT)))
	{
	  if (UpFlowTable[tbindex].debug)
	    printf("CM%d(flow-id %d): 3 No Piggyback req is sent on a Concatenated frame\n",
		   cm_id, UpFlowTable[tbindex].upstream_record.flow_id);
	  
	  /* Set the bit to indicate that no piggyback req was sent */
	  set_bit(&UpFlowTable[tbindex].upstream_record.flag, PIGGY_NOT_SEND,ON);
	  set_bit(&UpFlowTable[tbindex].upstream_record.flag, NO_PIGGY_BIT,OFF);	  
//TRY THIS?
//$A701
//	      FillPiggyReq(tbindex,p);
//	      set_bit(&UpFlowTable[tbindex].upstream_record.flag, PIGGY_NOT_SEND,OFF);
	}
    else if (bit_on(UpFlowTable[tbindex].upstream_record.flag,NO_PIGGY_BIT))
	{
	  if (UpFlowTable[tbindex].debug)
	    {
	      if  (bit_on(UpFlowTable[tbindex].upstream_record.flag,PIGGY_ENABLE_BIT))
		printf("CM%d(flow-id %d): 4 No Piggyback req is sent on a Concatenated frame\n",
		       cm_id, UpFlowTable[tbindex].upstream_record.flow_id);
	    }
	  set_bit(&UpFlowTable[tbindex].upstream_record.flag, NO_PIGGY_BIT,OFF);
//TRY THIS?
//$A701
//	      FillPiggyReq(tbindex,p);
//	      set_bit(&UpFlowTable[tbindex].upstream_record.flag, PIGGY_NOT_SEND,OFF);
	}
      

      //JJM WRONG : the map will indicate the channel....but for now this is OK
      int idleChannel = myMedium->findIdleChannel(CHANNEL_DIRECTION_UP);
        if (UpFlowTable[tbindex].debug)
             printf("MacDocsisCm:: decide_frag2: Reached the US sending stage using channel %d at instance %lf\n",idleChannel, Scheduler::instance().clock());
      ch_abs->channelNum=idleChannel;
      
      /* STATISTICS */
      UpFlowTable[tbindex].num_pkts++ ;
      UpFlowTable[tbindex].num_bytes += ch->size();
      MACstats.total_num_sent_frames++;
      MACstats.total_num_sent_bytes += ch->size(); 
      MACstats.total_num_BW_bytesUP += ch->size(); 

      CMstats.total_BW_bytesUP_status += ch->size(); 
      CMstats.total_pktsSentUP_status++;
//$A510
      mySFObj->updateStats(p,idleChannel,mySFObj->getBondingGroup());

      //#ifdef TRACE_CM_UP_DATA  //-----------------------------------------------      
      if (debug_cm)
        printf("CM%dup(flow-id: %d) %lf %f %d\n",
	       cm_id,
	       UpFlowTable[tbindex].upstream_record.flow_id,
	       Scheduler::instance().clock(),
	       MACstats.total_num_sent_bytes,
	       ch->size());
      //#endif //-----------------------------------------------------------------

      if (UpFlowTable[tbindex].debug)
	printf("CM%d(flow-id %d):decide_frag: send frame (size:%d) WITHOUT fragmentation at %lf \n",
	       cm_id,
	       UpFlowTable[tbindex].upstream_record.flow_id, 
	       ch->size(),
	       Scheduler::instance().clock());
      
    MACstats.total_num_frames_US++;
#ifdef US_RATE_CONTROL
      if (UpFlowTable[tbindex].ratecontrol == 1)
        USRateMeasure(tbindex,p);
#endif

      MacSendFrame(p,idleChannel);

    }  
  return rc;
}

/*************************************************************************
 * routine:  void MacDocsisCM::send_frag_data(Packet* p, char tbindex)
 *
 * explanation: This routine is called when the packet p is a part of a frag
 *  or must be sent as a frag. 
 *  The 2 key state params:
 *	       UpFlowTable[tbindex].curr_gsize //the #bytes in current grant
 *	       UpFlowTable[tbindex].frag_data) //the # bytes left in the frag
 *  Each call to this routine will send curr_gsize of the total data left (frag_data)
 *
 * inputs:   
 *   Packet *p:  frame (not concatonated)
 *   char tbindex:  index to US flow table
 *
 * outputs:
 * 
 * 
*************************************************************************/
void MacDocsisCM::send_frag_data(Packet* p, char tbindex)
{
//$A510
  double curTime =   Scheduler::instance().clock();
  serviceFlowObject *mySFObj = NULL;
  mySFObj = myUSSFMgr->getServiceFlow(p);
  struct hdr_cmn* ch = HDR_CMN(p);
  if (mySFObj == NULL) {
    printf("CM:SendData(%lf): HARD ERROR (cm_id:%d): Could not find this service flow:  packet of size:%d,   and ptype:%d\n",
     curTime,cm_id,ch->size(),ch->ptype());
//    return;
    exit(1);
  }
  
  if (UpFlowTable[tbindex].debug) {
    printf("CM%d(flow-id %d):send_frag_data:(%lf): Entered ...current grant size:%d, current frag_data:%d, Original Pkt ptr:%x\n",
	   cm_id, UpFlowTable[tbindex].upstream_record.flow_id,curTime,
           UpFlowTable[tbindex].curr_gsize, UpFlowTable[tbindex].frag_data, UpFlowTable[tbindex].OriginalFragPkt);
    fflush(stdout);
  }
  
  /* Sending a packet. Turn-off NO_PIGGY_BIT in case it was turned on */
  if (bit_on(UpFlowTable[tbindex].upstream_record.flag,NO_PIGGY_BIT))
    {
      set_bit(&UpFlowTable[tbindex].upstream_record.flag, NO_PIGGY_BIT,OFF);      
    }
  
  //This is set if we are in the middle of sending a fragment
  if (bit_on(UpFlowTable[tbindex].upstream_record.flag,FRAG_ON_BIT))
  {
      //this packet is only a portion of frag_pkt ???
      // we adjust the q->size() below to be equal to the grant amount
      Packet* q= UpFlowTable[tbindex].frag_pkt->copy();
      
      struct hdr_docsisextd* eh = HDR_DOCSISEXTD(q);
      eh->num_hdr = 0;
      
      if (UpFlowTable[tbindex].debug)
	printf("CM%d(flow-id %d):send_frag_data:(%lf): Sending an interior fragment: Curr grant size = %d, frag_data = %d\n",
	       cm_id,
	       UpFlowTable[tbindex].upstream_record.flow_id,curTime,
	       UpFlowTable[tbindex].curr_gsize,
	       UpFlowTable[tbindex].frag_data);
      
      //16 is the frag overhead
	  //JJM WRONG
      if (UpFlowTable[tbindex].curr_gsize < (UpFlowTable[tbindex].frag_data + defaultUSOverhead_bytes + DOCSIS_FRAG_OVERHEAD))
	{
	  struct hdr_cmn* ch = HDR_CMN(q);
	  
	  if (UpFlowTable[tbindex].debug)
	    printf("CM%d(flow-id %d):send_frag_data(:%lf): Sending middle fragment\n",
		   cm_id,
		   UpFlowTable[tbindex].upstream_record.flow_id, curTime);

	  /* Since 16 is the overhead for sending fragment header + docsis hdr*/
	  //JJM WRONG
	  UpFlowTable[tbindex].frag_data -= (UpFlowTable[tbindex].curr_gsize - DOCSIS_FRAG_OVERHEAD - defaultUSOverhead_bytes); 
	  
	  if (UpFlowTable[tbindex].debug)
	    printf("CM%d(flow-id %d)frag_data = %d\n",
		   cm_id,
		   UpFlowTable[tbindex].upstream_record.flow_id,
		   UpFlowTable[tbindex].frag_data);
	  
	  /* 
	     No need of adding PHY overhead bytes, docsis hdr bytes as 
	     they have already been taken care of by making size = curr_gsize 
	  */
	  ch->size() = UpFlowTable[tbindex].curr_gsize;
	  	  

//$A702
	  fill_extended_header(2,q,tbindex);	  

	  docsis_chabstr *ch_abs = docsis_chabstr::access(q);
 //JJM WRONG
      int idleChannel = myMedium->findIdleChannel(CHANNEL_DIRECTION_UP);
	  if (UpFlowTable[tbindex].debug)
          printf("MacDocsisCM::send_frag_data(%lf): Reached the US sending stage using channel %d \n",curTime,idleChannel);

          ch_abs->channelNum=idleChannel;

	  /* STATISTICS */
	  UpFlowTable[tbindex].num_pkts++ ;
	  UpFlowTable[tbindex].num_bytes += ch->size();
	  MACstats.total_num_sent_frames++;
	  MACstats.total_num_sent_bytes += ch->size(); 
	  MACstats.total_num_BW_bytesUP += ch->size(); 	  

	  CMstats.total_num_frag++;
      CMstats.total_BW_bytesUP_status += ch->size(); 
      CMstats.total_pktsSentUP_status++;

//$A510
      mySFObj->updateStats(p,idleChannel,mySFObj->getBondingGroup());
	  
	  /* Start a timer that expires when the packet transmission is complete. */
#ifdef US_RATE_CONTROL
          if (UpFlowTable[tbindex].ratecontrol == 1)
            USRateMeasure(tbindex,q);
#endif

      MacSendFrame(q,idleChannel);

	}
      else /* Finishing-off fragmentation */
	{
	  struct hdr_cmn* ch = HDR_CMN(q);
	  struct hdr_docsisextd* eh = HDR_DOCSISEXTD(q);
	  eh->num_hdr = 0;
	  
	  if (UpFlowTable[tbindex].debug)
	    printf("CM%d(flow-id %d):send_frag_data(%lf):Sending last fragment\n",
		   cm_id,
		   UpFlowTable[tbindex].upstream_record.flow_id,curTime);
	  
	  set_bit(&UpFlowTable[tbindex].upstream_record.flag, FRAG_ON_BIT,OFF);
	  
	  /* Indicate the last fragment number */
	  /* change the states accordingly n state machine, not here*/
	  /* Since 3 bytes will be incremneted by piggybacking routine */
	  ch->size() = UpFlowTable[tbindex].frag_data + DOCSIS_FRAG_OVERHEAD - 3; 

	  UpFlowTable[tbindex].frag_data = 0;
//$A702
	  fill_extended_header(1,q,tbindex);
	  UpFlowTable[tbindex].seq_num = 0;

	  docsis_chabstr *ch_abs = docsis_chabstr::access(q);
 //JJM WRONG
      int idleChannel = myMedium->findIdleChannel(CHANNEL_DIRECTION_UP);
      ch_abs->channelNum=idleChannel;
	  
	  //STATISTICS
	  UpFlowTable[tbindex].num_pkts++ ;
	  UpFlowTable[tbindex].num_bytes += ch->size();
	  MACstats.total_num_sent_frames++;
	  MACstats.total_num_sent_bytes += ch->size(); 
	  MACstats.total_num_BW_bytesUP += ch->size(); 

	  CMstats.total_num_frag++;
      CMstats.total_BW_bytesUP_status += ch->size(); 
      CMstats.total_pktsSentUP_status++;

//$A510
      mySFObj->updateStats(p,idleChannel,mySFObj->getBondingGroup());
	  
	  /* Start a timer that expires when the packet transmission is complete. */
#ifdef US_RATE_CONTROL
          if (UpFlowTable[tbindex].ratecontrol == 1)
            USRateMeasure(tbindex,q);
#endif

      MacSendFrame(q,idleChannel);

	  Packet::free(UpFlowTable[tbindex].frag_pkt);	  
//$A700
      UpFlowTable[tbindex].OriginalFragPkt = NULL;
	}
  }
  else
  //else frag bit is off which means we will begin a new frag
  {

//$A702
      if (mySFObj->type != MGMT_PKT) {
	    UpFlowTable[tbindex].OriginalFragPkt = p;
      }

      struct hdr_cmn* ch = HDR_CMN(p);
      Packet *n = AllocPkt(sizeof(int));	
      int *data = (int*) n->accessdata();
      struct hdr_cmn* chn = HDR_CMN(n);
      struct hdr_docsis* dh = HDR_DOCSIS(n);
      struct hdr_docsisextd* eh = HDR_DOCSISEXTD(n);
      eh->num_hdr = 0;
      set_bit(&UpFlowTable[tbindex].upstream_record.flag, FRAG_ON_BIT,ON);
      UpFlowTable[tbindex].seq_num = 0;
       
      /* 3 for piggybacked req */
	  //JJM WRONG
      UpFlowTable[tbindex].frag_data =  ch->size() - (UpFlowTable[tbindex].curr_gsize - DOCSIS_FRAG_OVERHEAD - defaultUSOverhead_bytes) + 3;
      chn->size() = UpFlowTable[tbindex].curr_gsize;
      chn->ptype() = PT_DOCSISFRAG;
      
      dh->dshdr_.fc_type = MAC_SPECIFIC;
      dh->dshdr_.fc_parm = FRAGMENT;
      dh->dshdr_.ehdr_on = 1;
      dh->dshdr_.mac_param = 6;    //length of EHDR is 6 octets
        //See Table 6-14 of DOCSIS 3.0 MULPI spec     

      dh->dshdr_.len = chn->size() - DOCSIS_FRAG_OVERHEAD;
      
	  //JJM WRONG
      if (UpFlowTable[tbindex].debug)
	       printf("CM%d(flow-id %d)Sending first fragment of size %d (grants:%d)\n",
	       cm_id,
	       UpFlowTable[tbindex].upstream_record.flow_id,
	       dh->dshdr_.len, dh->dshdr_.len/bytes_pminislot);
      
//$A702
      fill_extended_header(0,n,tbindex);
      
      /* Sending the unfragmented pkt pointer in data-portion */
      *data++ = (int)p;
      
      /* 
	 Increment the size of packet to be fragment by overhead-bytes so 
	 that no spl handling is required as in case of concat 
      */

      UpFlowTable[tbindex].frag_pkt = n->copy();

      docsis_chabstr *ch_abs = docsis_chabstr::access(n);
 //JJM WRONG
      int idleChannel = myMedium->findIdleChannel(CHANNEL_DIRECTION_UP);
      if (UpFlowTable[tbindex].debug)
            printf("MacDocsisCM::send_frag_data3: Reached the US sending stage using channel %d at instance %lf\n",idleChannel, Scheduler::instance().clock());
      ch_abs->channelNum=idleChannel;
      
      /* STATISTICS */
      UpFlowTable[tbindex].num_pkts++ ;
      UpFlowTable[tbindex].num_bytes += chn->size();
      MACstats.total_num_sent_frames++;
      MACstats.total_num_sent_bytes += chn->size(); 
      MACstats.total_num_BW_bytesUP += chn->size(); 

//$A510
      mySFObj->updateStats(p,idleChannel,mySFObj->getBondingGroup());

      CMstats.total_num_frag++;
      CMstats.total_BW_bytesUP_status += chn->size(); 
      CMstats.total_pktsSentUP_status++;

      
      /* Start a timer that expires when the packet transmission is complete. */
#ifdef US_RATE_CONTROL
      if (UpFlowTable[tbindex].ratecontrol == 1)
        USRateMeasure(tbindex,n);
#endif

      MacSendFrame(n,idleChannel);

      UpFlowTable[tbindex].pkt = UpFlowTable[tbindex].frag_pkt;      
    }
}

/*************************************************************************
 * Routine: void MacDocsisCM::fill_extended_header(int i, Packet* p,char tbindex)
 *
 * Explanation:
 *  This is called when we are sending a fragment, we need to  setup
 *  the piggyback request in the frag extended header.
 *  If we send the frag and don't have a piggy req then its the last frag.
 *  In this case, we see if we can attach a separate piggy request.  We
 *  do this only if there is another packet in the queue and if
 *  beffort_ratecheck says its ok.
 *    If we do, the piggy req header will be the first, the frag request header will be #2
 *    If we do have pkts queued but CAN NOT send it, do the following: 
 *     UpFlowTable[tbindex].state = BEFFORT_DECISION;
 *     UpFlowTable[tbindex].pkt = deque_pkt(tbindex);
 *      beffort_decision(tbindex, PKT_ARRIVAL, UpFlowTable[tbindex].pkt);
 *
 * inputs:
 *   i:   0: first frag, 1:last frag, 2:interior frag
 *   p:  reference to the frag that is being sent
 *   tbindex:
 *
*************************************************************************/
void MacDocsisCM::fill_extended_header(int i, Packet* p,char tbindex)
{
//  struct hdr_cmn* tmpch = HDR_CMN(p);
//  struct hdr_docsisextd* ch = HDR_DOCSISEXTD(p);
  int j;
  int ratecheck_returnval = 1;
  Packet *q = NULL;
  int slots_requested;
  Packet *fragPkt = NULL;

//$A510
//$A700
  serviceFlowObject *mySFObj = NULL;
//  mySFObj = myUSSFMgr->getServiceFlow(p);

//$A701  first frag use this packet, but other frags we need to reference the original
//GET RID OF ??
  if (i == 0) 
    fragPkt = p;
  else
    fragPkt = p;

  struct hdr_cmn* tmpch = HDR_CMN(fragPkt);
  struct hdr_docsisextd* ch = HDR_DOCSISEXTD(fragPkt);
  struct hdr_cmn* fragPktCh = HDR_CMN(fragPkt);

//  mySFObj = myUSSFMgr->getServiceFlow(fragPkt);
  mySFObj = myUSSFMgr->getServiceFlow(UpFlowTable[tbindex].OriginalFragPkt);

  if (mySFObj == NULL) {
    printf("CM:fill_extended_header(%lf): HARD ERROR (cm_id:%d): Could not find this service flow:  packet of size:%d,   and ptype:%d\n",
     Scheduler::instance().clock(),cm_id,fragPktCh->size(),fragPktCh->ptype());
//    return;
    exit(1);
  }

  
//  This is called to setup the piggy request that is on a frag
//  about to be transMITTED.   
//   >0 means piggy req all set and moved to REQSENT state
//  0  means there is no more data so NO piggy
  j = fill_piggyback_req(tbindex);

  if (UpFlowTable[tbindex].debug)
  {
      printf("CM%d(flow-id %d):fill_extended_header(%lf):  ENTRY : i param:%d, j:%d, (SFid:%d,myPacketCount:%d)  q len:%d  pkt size:%d, current state: %d (curr_gsize:%d, frag_data:%d) \n",
	   cm_id,
	   UpFlowTable[tbindex].upstream_record.flow_id,
	   Scheduler::instance().clock(),
       i,j, mySFObj->flowID,mySFObj->myPacketCount, mySFObj->myPacketCount, fragPktCh->size(),
	   UpFlowTable[tbindex].state,
       UpFlowTable[tbindex].curr_gsize, UpFlowTable[tbindex].frag_data);
  }
  
  //this means no piggy was set, 
 // so its last frag, check if can add a separate piggy request
  if (j == 0)
  {

    //$A510
    if ((mySFObj->myPacketCount) > 0) 
//    if ((myUSSFMgr->len_queue(p,tbindex)) > 0) 
//    if (len_queue(tbindex) > 0) 
    {

     //return a reference to next pkt in queue
//$A510
      q = mySFObj->getPacket(0);   
      if (q == NULL) {
        printf("CM:fill_extended_header(%lf): HARD ERROR 2 (cm_id:%d): bad pkt ptr\n",
                Scheduler::instance().clock(),cm_id);
        exit(1);
      }

//      q = UpFlowTable[tbindex].packet_list->pkt;
      struct hdr_cmn* chn = HDR_CMN(q);

	  //JJM WRONG
      slots_requested = (int) chn->size() / bytes_pminislot;
    if (UpFlowTable[tbindex].debug)
    {
      printf("CM%d(flow-id %d):fill_extended_header(%lf):  pkt size:%d,  slots_requested: %d slots, current state: %d, q len:%d \n",
	   cm_id,
	   UpFlowTable[tbindex].upstream_record.flow_id,
	   Scheduler::instance().clock(),chn->size(),
	   slots_requested, UpFlowTable[tbindex].state,
       mySFObj->myPacketCount);
    }
  

      //We call ratecheck as if this was a new request...rather than a piggy request
      ratecheck_returnval = MacDocsisCM::beffort_ratecheck(tbindex, REQ_TIMER, q,slots_requested);
      if (UpFlowTable[tbindex].debug)
        printf("CM%d(flow-id %d):fill_extended_header(%lf): sending last frag, state:%d, pkt q len:%d ,ratecheck:%d\n",
	   cm_id,
	   UpFlowTable[tbindex].upstream_record.flow_id,
	   Scheduler::instance().clock(),
//	   UpFlowTable[tbindex].state,len_queue(tbindex),ratecheck_returnval);
	   UpFlowTable[tbindex].state, mySFObj->myPacketCount,ratecheck_returnval);
//$A510

     if (ratecheck_returnval == -1) {
      UpFlowTable[tbindex].state = BEFFORT_DECISION;
//Need the first packet in the queue
      //$A510
      UpFlowTable[tbindex].pkt = myUSSFMgr->deque_pkt(mySFObj);
//      UpFlowTable[tbindex].pkt = deque_pkt(tbindex);

        UpFlowTable[tbindex].enque_time = Scheduler::instance().clock();
      if (UpFlowTable[tbindex].debug) {
        printf("CM%d(flow-id %d):fillextendedheader(%lf): NRAD CASE 6, reset enque_time \n",
	       cm_id,UpFlowTable[tbindex].upstream_record.flow_id,
	       Scheduler::instance().clock());
      }

       beffort_decision(tbindex, PKT_ARRIVAL, UpFlowTable[tbindex].pkt);
     }   
     else {
      /* Try to send piggyback req for other frames 
	 in queue since this is the last fragment */

//$A700
      FillPiggyReq(tbindex, UpFlowTable[tbindex].OriginalFragPkt);
//      FillPiggyReq(tbindex,p);
      
      /* Set this bit to prevent entering 'if' 
	 in beffort_tosend for SEND_TIMER event */
      set_bit(&UpFlowTable[tbindex].upstream_record.flag, PIGGY_NOT_SEND,OFF);
     }
    }
    else {
//no need to piggyback....move to init
      UpFlowTable[tbindex].state = BEFFORT_IDLE;
       /* Turn-off this bit, as the info has been used */
      set_bit(&UpFlowTable[tbindex].upstream_record.flag, PIGGY_NOT_SEND,OFF);
    }
  }
  if (UpFlowTable[tbindex].debug)
    printf("CM%d(flow-id %d):fill_extended_header(%lf):  sending piggyback req for %d slots, current state: %d \n",
	   cm_id,
	   UpFlowTable[tbindex].upstream_record.flow_id,
	   Scheduler::instance().clock(),
	   j, UpFlowTable[tbindex].state);
  
  ch->exthdr_[ch->num_hdr].eh_type = 3;
  ch->exthdr_[ch->num_hdr].eh_len = 5;
  ch->exthdr_[ch->num_hdr].eh_data[0] = 0; 
  ch->exthdr_[ch->num_hdr].eh_data[1] = j; /* Num of mini-slots requested */
  
 //   i:   0: first frag, 1:last frag, 2:interior frag
  if (i == 0)
    ch->exthdr_[ch->num_hdr].eh_data[2] = 1;  //indicate this is first frag
  else 
    ch->exthdr_[ch->num_hdr].eh_data[2] = 0; /* NOT first fragment */
  
  if (i == 1)
    ch->exthdr_[ch->num_hdr].eh_data[3] = 1; /* indicate the Last fragment */
  else 
    ch->exthdr_[ch->num_hdr].eh_data[3] = 0;
  
  ch->exthdr_[ch->num_hdr].eh_data[4] = UpFlowTable[tbindex].seq_num++;
  ch->exthdr_[ch->num_hdr].eh_data[5] = UpFlowTable[tbindex].upstream_record.flow_id;
  ch->num_hdr++;  
}

/*************************************************************************
 * routine: 
 *       int MacDocsisCM::FillPiggyReq(char tbindex, Packet * p)
 *
 * explanation: This is called to see if a request can be piggybacked
 *       to this packet, p.
 *   Nothing happens if  (len_queue(tbindex) = 0)
 *   IF there is at least one packet in the queue,
 *    deque it and save it in the flow table:
*        UpFlowTable[tbindex].pkt = deque_pkt(tbindex);
*    This routine pulls this next packet, sees how many
*    slots are required and then calls
*       FillPiggyExtHdr(tbindex,p,slots); 
*     This sets the current packet with a piggyback request
*     for bandwidth for the next packet that was just dequeued
*
* inputs:
*
* outputs:
*
*    Rc:   -1 on error , else 1
*************************************************************************/
int MacDocsisCM::FillPiggyReq(char tbindex, Packet * p)
{
  int req_size,concat_req,slots;
  Packet* r;
  int ratecheck_returnval = 1;
  struct hdr_cmn* ch = HDR_CMN(p);
  double curTime =   Scheduler::instance().clock();
  Packet *tmpPkt = NULL;

//$A510
  serviceFlowObject *mySFObj = NULL;
  mySFObj = myUSSFMgr->getServiceFlow(p);
  if (mySFObj == NULL) {
    printf("CM:FillPiggyReq(%lf): HARD ERROR (cm_id:%d): Could not find this service flow:  packet of size:%d,   and ptype:%d\n",
     Scheduler::instance().clock(),cm_id,ch->size(),ch->ptype());
//    return;
    exit(1);
  }

  if (UpFlowTable[tbindex].debug)
    printf("CM:FillPiggyReq(%lf): (cm_id:%d):  BEFORE SWITCH: SFObject flowID:%d,  queue len:%d\n",
     Scheduler::instance().clock(),cm_id,mySFObj->flowID,mySFObj->myPacketList->curListSize);

  
   //$A510
//$A701
  if (myUSSFMgr->anyPackets() == TRUE)
  {
//  if (mySFObj->myPacketCount > 0)
//  if ((myUSSFMgr->len_queue(p,tbindex)) > 0)
//  if (len_queue(tbindex) > 0)

//A701
//At this poiont, we might change SF's set get a new tbindex...
      mySFObj = myUSSFMgr->nextSF();
      if (mySFObj == NULL) {
        printf("CM:FillPiggyReq(%lf): (cm_id:%d): HARD ERROR, nextSF returns NULL SF Object\n",
           Scheduler::instance().clock(),cm_id);
        exit(1);
      }

      tmpPkt = myUSSFMgr->deque_pkt(mySFObj);
      tbindex = classify(tmpPkt, UPSTREAM);
      UpFlowTable[tbindex].pkt = tmpPkt;

      if (UpFlowTable[tbindex].debug)
        printf("CM:FillPiggyReq(%lf): (cm_id:%d):  AFTER SWITCH: SFObject flowID:%d,  queue len:%d\n",
          Scheduler::instance().clock(),cm_id,mySFObj->flowID,mySFObj->myPacketList->curListSize);

      if (UpFlowTable[tbindex].pkt == NULL) {
        printf("CM:FillPiggyReq(%lf): (cm_id:%d): HARD ERROR, SFMgr says there are packets but returns NULL SF Object\n",
           Scheduler::instance().clock(),cm_id);
        exit(1);
      }

      //UpFlowTable[tbindex].pkt = myUSSFMgr->deque_pkt(mySFObj);
//      UpFlowTable[tbindex].pkt = deque_pkt(tbindex);
      Packet * q;
      q = UpFlowTable[tbindex].pkt;
#ifdef TIMINGS
//	printf("2 %lf %d %d 3\n",Scheduler::instance().clock(),
//			UpFlowTable[tbindex].upstream_record.flow_id,cm_id); 
        timingsTrace(q,2);
#endif
      
      struct hdr_cmn* ch = HDR_CMN(q);
      
      /* Only see if there is any data grants or grant pending, 
	 else send a request */   
      if (CanBeSent(tbindex,q))
	{
	  if (UpFlowTable[tbindex].debug)
	    printf("CM%d(flow-id %d):FillPiggyReq(%lf): DataGrant is allocated... goto REQSENT state\n",
		   cm_id, UpFlowTable[tbindex].upstream_record.flow_id,
		   Scheduler::instance().clock());
	  
	  UpFlowTable[tbindex].state = BEFFORT_TOSEND;
	  beffort_tosend(tbindex, SEND_PKT, q);
          return ratecheck_returnval;
	}
      else if (DataGrantPending(tbindex))
	{
	  if (UpFlowTable[tbindex].debug)
	    printf("CM%d(flow-id %d):FillPiggyReq(%lf): DataGrant is pending... goto REQSENT state\n",
		   cm_id, UpFlowTable[tbindex].upstream_record.flow_id,
		   Scheduler::instance().clock());
	  
	  UpFlowTable[tbindex].state = BEFFORT_REQSENT;
          return ratecheck_returnval;
	}      


      req_size = ch->size();
//      req_size += upchannel.overhead_bytes;
	  //JJM WRONG DO i need the following ?
//      req_size += defaultUSOverhead_bytes;
      req_size += 3; /* For sending piggy req */

// sending this next pkt using a piggyrequest on current transmission
//Reset enqueu time to now?
      UpFlowTable[tbindex].enque_time = Scheduler::instance().clock();
      if (UpFlowTable[tbindex].debug) {
        printf("CM%d(flow-id %d):FillPiggyReq(%lf): NRAD CASE 2, reset enque_time \n",
	       cm_id,UpFlowTable[tbindex].upstream_record.flow_id,
	       Scheduler::instance().clock());
      }


      concat_req = check_concat_req(p,tbindex);
      
      if (!(bit_on(UpFlowTable[tbindex].upstream_record.flag,FRAG_ON_BIT)) &&(concat_req))
	{
           //if not a frag AND concat on...
      UpFlowTable[tbindex].numberBytesInNextConcat_ = 0;
	  UpFlowTable[tbindex].numberPacketsInNextConcat_ = 0;
	  if (UpFlowTable[tbindex].debug)
	    printf("CM%d(flow-id %d):FillPiggyReq:(%lf) Decided to send a Concatenated req \n",
		   cm_id, UpFlowTable[tbindex].upstream_record.flow_id,
		   Scheduler::instance().clock());
	  
	  /* Add  6 bytes for Concat frame header */
	  req_size += 6;
	  req_size -= 3; /* For piggy req, since concat frames will not carry piggy req */
	  
//$A701
//WRONG:  We need to switch to the new tbindex....
          int len = mySFObj->myPacketCount;
//           int len = myUSSFMgr->len_queue(p,tbindex);
//          int len = len_queue(tbindex);
          if ((len > 0) && (len > UpFlowTable[tbindex].max_concat_threshhold))
            len = UpFlowTable[tbindex].max_concat_threshhold;

          if (UpFlowTable[tbindex].debug)
	     printf("CM%d(flow-id %d):FillPiggyReq:(%lf) Requesting to carry %d packets (max:%d) \n",
	       cm_id, UpFlowTable[tbindex].upstream_record.flow_id,Scheduler::instance().clock(),
	       len+1, UpFlowTable[tbindex].max_concat_threshhold);

          for (int i = 0; i <= (len-1); i++)
	    {
//	      r = pkt_lookup(tbindex,i);
  //$A510
          if (UpFlowTable[tbindex].debug)
            printf("MacDocsisCM:FillPiggyReq:(%lf): CMID:%d: get the packet at the i'th position : %d \n", curTime,cm_id,i);
          r =  mySFObj->getPacket(i);
	      
          if (r == 0)
		  {
		    printf("CM%d(flow-id %d): MacDocsisCM:FillPiggyReq error, pkt_lookup returned NULL, exiting\n",
			 cm_id, UpFlowTable[tbindex].upstream_record.flow_id);
		    exit(1);
		  }
	      struct hdr_cmn* chr = HDR_CMN(r);
	      req_size += chr->size();

		  //JJM WRONG
          slots = (int) req_size / bytes_pminislot;
          if ((req_size % bytes_pminislot) != 0)
            slots += 1;

	      //255 is max but req_size is data only....
	      if (slots > 240)
          {
	        req_size -= chr->size();
            if (req_size < 0)
              req_size = 0;       
            UpFlowTable[tbindex].numberBytesInNextConcat_ = req_size;
	        UpFlowTable[tbindex].numberPacketsInNextConcat_ = i-1;
	        if (UpFlowTable[tbindex].numberPacketsInNextConcat_ < 0)
	          UpFlowTable[tbindex].numberPacketsInNextConcat_ = 0;

	        if (UpFlowTable[tbindex].debug)
	          printf("CM%d(Flow-id %d):FillPiggyReq:(%lf)Can't add any more packets.... size: %d, req_size:%d bytes,numberPacketsInNextConcat_:%d\n",
		   cm_id, UpFlowTable[tbindex].upstream_record.flow_id,
		   Scheduler::instance().clock(), chr->size(), req_size,
		   UpFlowTable[tbindex].numberPacketsInNextConcat_);
	        break;
	      }
	      //This is the normal path-  make sure this is set with the correct
	      //number of packets to ADD into the concatonated frame
	      else {
	        UpFlowTable[tbindex].numberPacketsInNextConcat_ = len;
            UpFlowTable[tbindex].numberBytesInNextConcat_ = req_size;
	      }
	    }
	  set_bit(&UpFlowTable[tbindex].upstream_record.flag, CONCAT_ON_BIT,ON);
	}	
      
      if (UpFlowTable[tbindex].debug)
	printf("CM%d(flow-id %d):FillPiggyReq:(%lf)Sending Piggy req for %d bytes\n",
	       cm_id, UpFlowTable[tbindex].upstream_record.flow_id,
	       Scheduler::instance().clock(), req_size);
      
	  //JJM WRONG
      slots = (int) req_size / bytes_pminislot;
      
      if ((req_size % bytes_pminislot) != 0)
	slots += 1;
      
//      FillPiggyExtHdr(tbindex,p,slots);

//Just set this to something .... 
//    UpFlowTable[tbindex].num_slots_req = 10;
    //We call ratecheck as if this was a new request...rather than a piggy request
    ratecheck_returnval = MacDocsisCM::beffort_ratecheck(tbindex, REQ_TIMER, q,slots);
    if (UpFlowTable[tbindex].debug)
      printf("CM%d(flow-id %d):FillPiggyReq(%lf): #in queue:%d, ratecheck:%d\n",
	   cm_id,
	   UpFlowTable[tbindex].upstream_record.flow_id,
	   Scheduler::instance().clock(),myUSSFMgr->len_queue(p,tbindex),ratecheck_returnval);
//	   Scheduler::instance().clock(),len_queue(tbindex),ratecheck_returnval);

     if (ratecheck_returnval == -1) {
      UpFlowTable[tbindex].state = BEFFORT_DECISION;
        UpFlowTable[tbindex].enque_time = Scheduler::instance().clock();
      if (UpFlowTable[tbindex].debug) {
        printf("CM%d(flow-id %d):FillPiggyReq(%lf): NRAD CASE 6, reset enque_time \n",
	       cm_id,UpFlowTable[tbindex].upstream_record.flow_id,
	       Scheduler::instance().clock());
      }

      beffort_decision(tbindex, PKT_ARRIVAL,q);
     }   
     else {
      //note: p is original packet that contains the piggy requ,  q is next to send
      FillPiggyExtHdr(tbindex,p,slots); 
      UpFlowTable[tbindex].req_time = Scheduler::instance().clock();
      if (UpFlowTable[tbindex].debug)
	printf("CM%d(flow-id %d):FillPiggyReq:(%lf)Sent piggy request for %d bytes, checkreq time and cmts ack \n",
	       cm_id, UpFlowTable[tbindex].upstream_record.flow_id,
	       Scheduler::instance().clock(), req_size);
	MapSentAfterReq(tbindex);
      
        UpFlowTable[tbindex].enque_time = Scheduler::instance().clock();
      if (UpFlowTable[tbindex].debug) {
        printf("CM%d(flow-id %d):FillPiggyReq(%lf): NRAD CASE 2, reset enque_time \n",
	       cm_id,UpFlowTable[tbindex].upstream_record.flow_id,
	       Scheduler::instance().clock());
      }

      /* Change the state to request sent */
      UpFlowTable[tbindex].state = BEFFORT_REQSENT;
      UpFlowTable[tbindex].total_piggyreq++;      
     }
  }
  else
  {
    /* Change the state to idle */
    if (UpFlowTable[tbindex].debug)
	    printf("CM%d(flow-id %d):FillPiggyReq:(%lf)No piggy req being sent, ALL SF Queues are empty\n",
	       cm_id, UpFlowTable[tbindex].upstream_record.flow_id,
	       Scheduler::instance().clock());
      
    UpFlowTable[tbindex].pkt = 0;
    UpFlowTable[tbindex].state = BEFFORT_IDLE;      
  }
  return ratecheck_returnval;
}

/*************************************************************************
 *   MacDocsisCM::FillPiggyExtHdr(char tbindex,Packet * p, int slots)
 *
 * Explanation: This is called when we are about to send a piggy request
 *  This routine will fill in the exh hdr fields. 
 *  It also calls beffort_ratecheck and returns this rc
 *   It ratecheck says do not send, we do not turn on ehdr_on !!!
 *
* inputs:
*
* outputs:
*
*************************************************************************/
void MacDocsisCM::FillPiggyExtHdr(char tbindex,Packet * p, int slots)
{
  struct hdr_docsisextd* ch = HDR_DOCSISEXTD(p);
  struct hdr_docsis* dh = HDR_DOCSIS(p);
  struct hdr_cmn* chn = HDR_CMN(p);
  
  ch->exthdr_[ch->num_hdr].eh_type = 1;
  ch->exthdr_[ch->num_hdr].eh_len = 3;
  ch->exthdr_[ch->num_hdr].eh_data[0] = slots; 
  ch->exthdr_[ch->num_hdr].eh_data[1] = UpFlowTable[tbindex].upstream_record.flow_id;
  ch->num_hdr++;
  chn->size() += 3;
  

  if (UpFlowTable[tbindex].debug) {
     printf("CM%d(flow-id %d):FillPiggyExtHdr(%lf): PIGGY:(#hdrs:%d) set num_slots_req:%d, eh_data[]:%d %d %d %d\n",
     cm_id, UpFlowTable[tbindex].upstream_record.flow_id,
     Scheduler::instance().clock(),
     ch->num_hdr,
     UpFlowTable[tbindex].num_slots_req,
     ch->exthdr_[ch->num_hdr-1].eh_data[0],
     ch->exthdr_[ch->num_hdr-1].eh_data[1],
     ch->exthdr_[ch->num_hdr-1].eh_data[2],
     ch->exthdr_[ch->num_hdr-1].eh_data[3]);
  }

  /* Turn-on the extended header */
  dh->dshdr_.ehdr_on = 1;  
}

/*************************************************************************
 * routine: int MacDocsisCM::fill_piggyback_req(char tbindex)
*
* explanation: 
*  This is called to setup the piggy request that is on a frag
*  about to be transMITTED.   
*
* inputs:
*  char tbindex :
*
* outputs:
*   >0 means piggy req all set and moved to REQSENT state
*  0  means there is no more data so NO piggy
*
*************************************************************************/
int MacDocsisCM::fill_piggyback_req(char tbindex)

{
  int req_size;
  double current_time;

  current_time = Scheduler::instance().clock();

  if (UpFlowTable[tbindex].debug)
       printf("CM%d(%lf)(flow-id %d)fill_piggyback_request: current UpFlowTable.frag_data:%d\n",
               cm_id,current_time, UpFlowTable[tbindex].upstream_record.flow_id,UpFlowTable[tbindex].frag_data);
  
  if (UpFlowTable[tbindex].frag_data > 0)
  {
//CanBeSent returns 0 if it can send... 
      if (!CanBeSent(tbindex,UpFlowTable[tbindex].frag_pkt))
	{
//so it can send....we add a piggy req
	  UpFlowTable[tbindex].state = BEFFORT_REQSENT;
	  UpFlowTable[tbindex].req_time = Scheduler::instance().clock();
	  //JJM WRONG DO I need the following
//	  req_size = UpFlowTable[tbindex].frag_data + DOCSIS_FRAG_OVERHEAD + defaultUSOverhead_bytes;;
	  req_size = UpFlowTable[tbindex].frag_data + DOCSIS_FRAG_OVERHEAD;
//	  req_size = UpFlowTable[tbindex].frag_data + 16 + upchannel.overhead_bytes;
	  req_size = (int) (req_size / bytes_pminislot);
	  
	  if (req_size % bytes_pminislot != 0)
	    req_size++;
	  
          if (UpFlowTable[tbindex].debug)
            printf("CM%d(%lf)(flow-id %d)fill_piggyback_request: move to BEFFORT_REQSENT, req_size:%d\n",
               cm_id,current_time, UpFlowTable[tbindex].upstream_record.flow_id,req_size);

	  return req_size;
	}
      else
	{
//if we can not send
          if (UpFlowTable[tbindex].debug)
            printf("CM%d(%lf)(flow-id %d)fill_piggyback_requst: call beffort_tosend,return 0\n",
               cm_id,current_time, UpFlowTable[tbindex].upstream_record.flow_id);
	  beffort_tosend(tbindex, SEND_PKT,UpFlowTable[tbindex].frag_pkt);
	  return 0;
	}
  }
  else{
    if (UpFlowTable[tbindex].debug)
       printf("CM%d(%lf)(flow-id %d)fill_piggyback_requst: Final frag, no  more frag data\n",
       cm_id,current_time, UpFlowTable[tbindex].upstream_record.flow_id);

    return 0;
  }
}

/*************************************************************************
* function:  int MacDocsisCM::send_concat(Packet* p, char tbindex)
* 
* Explanation:  
*   This is called by SendData when a concatonated frame is ready to be sent.
*   The number to send is
*     int len =  UpFlowTable[tbindex].numberPacketsInNextConcat_; 
*     PLUS the packet that is passed in to the routine.
*
*   Note: This might be called when there are no packets in the queue-
*   in which case it is sent as a single packet frame.
*
* input: 
*  Packet *p : packet to send
*  char tbindex : flow ptr
*
* outputs: none
*       rc = 1 if the packet is sent fragmented, a 0 if sent in 
*       the concat frame, a -1 on error (we called drop)
*
*************************************************************************/
int MacDocsisCM::send_concat(Packet* p, char tbindex)
{
int rc = 0;
//$A510
  struct hdr_cmn* tmpch = HDR_CMN(p);
  serviceFlowObject *mySFObj = NULL;
  mySFObj = myUSSFMgr->getServiceFlow(p);
  if (mySFObj == NULL) {
    printf("CM%d:send_concat(%lf): HARD ERROR  Could not find this service flow:  packet of size:%d,  and ptype:%d\n",
     cm_id,Scheduler::instance().clock(),tmpch->size(),tmpch->ptype());
//    return;
    exit(1);
  }


//$A701 - case when we send MGMT packets concat, but there are other SF packets ...
  if (mySFObj->type== MGMT_PKT) {
    printf("CM%d:send_concat(%lf): MGMT CASE: numberPacketsInNextConcat:%d (bytes:%d), #pkts in queue:%d \n",
     cm_id,Scheduler::instance().clock(), UpFlowTable[tbindex].numberPacketsInNextConcat_,
            UpFlowTable[tbindex].numberBytesInNextConcat_, mySFObj->myPacketCount);
  }

//Remember this is the number of packets to add minus 1!!!
    int len =  UpFlowTable[tbindex].numberPacketsInNextConcat_; 
    int reqLen =  UpFlowTable[tbindex].numberBytesInNextConcat_; 
     if (UpFlowTable[tbindex].debug)
         printf("CM%d(flow-id %d):send_concat:(%lf) On entry: len:%d (in bytes:%d),  (grantSize:%d,#InNExt:%d, qlen:%d) \n",
                 cm_id, UpFlowTable[tbindex].upstream_record.flow_id,Scheduler::instance().clock(),
                 len, UpFlowTable[tbindex].curr_gsize,UpFlowTable[tbindex].numberPacketsInNextConcat_,
                  UpFlowTable[tbindex].numberBytesInNextConcat_, myUSSFMgr->len_queue(p,tbindex));
//                 len, UpFlowTable[tbindex].curr_gsize,UpFlowTable[tbindex].numberPacketsInNextConcat_,len_queue(tbindex));


  if (UpFlowTable[tbindex].debug)
     printf("CM%d(flow-id %d):send_concat:(%lf) Sending concat frame containing %d packets (%d slots) (grantSize:%d,#InNExt:%d) \n",
       cm_id, UpFlowTable[tbindex].upstream_record.flow_id,Scheduler::instance().clock(),
       len+1, UpFlowTable[tbindex].curr_gsize/bytes_pminislot, UpFlowTable[tbindex].curr_gsize,UpFlowTable[tbindex].numberPacketsInNextConcat_);
  
  UpFlowTable[tbindex].numberPacketsInNextConcat_  = 0;
  UpFlowTable[tbindex].numberBytesInNextConcat_ = 0;
    
//$A701
// Check if frag is not enabled and the CMTS allocated a partial grant
  if (!(bit_on(UpFlowTable[tbindex].upstream_record.flag,FRAG_ENABLE_BIT))) {
    if (UpFlowTable[tbindex].curr_gsize <  reqLen) {
      printf("CM%d(flow-id %d):send_concat:(%lf)  HARD ERROR PARTIAL GRANT BUT FRAG NOT ON: curr_gsize:%d,  reqLen:%d  (grantSize in slots:%d,#InNExt:%d but setting to 0) \n",
       cm_id, UpFlowTable[tbindex].upstream_record.flow_id,Scheduler::instance().clock(),
       UpFlowTable[tbindex].curr_gsize,reqLen, UpFlowTable[tbindex].curr_gsize/bytes_pminislot,UpFlowTable[tbindex].numberPacketsInNextConcat_);

      reqLen = 0;
      len = 0;
    }
  }



  if (UpFlowTable[tbindex].debug) {
     printf("CM%d(flow-id %d):send_concat:(%lf) Actual concat frame contains %d packets (grantSize:%d,#InNext:%d)  \n",
       cm_id, UpFlowTable[tbindex].upstream_record.flow_id,Scheduler::instance().clock(),
       len+1, UpFlowTable[tbindex].curr_gsize, UpFlowTable[tbindex].numberPacketsInNextConcat_);
     printf(" ... totalConcatFrames:%d,   totalPacketInConcatFrames:%d\n",
		     UpFlowTable[tbindex].totalConcatFrames, UpFlowTable[tbindex].totalPacketsInConcatFrames);
  }


//If we are not going to send a concat frame- we will try to send a piggyback req
//  if (len < UpFlowTable[tbindex].max_concat_threshhold)
    if (len == 0)
    {
//$A701
      // Check if frag is not enabled and the CMTS allocated a partial grant
      if (!(bit_on(UpFlowTable[tbindex].upstream_record.flag,FRAG_ENABLE_BIT))) {
        if (UpFlowTable[tbindex].curr_gsize < tmpch->size()) {
         printf("CM%d(flow-id %d):send_concat:(%lf)  HARD ERROR DROP frame of size:%d because gsize only %d  containing %d packets (%d total slots) (grantSize:%d,#InNExt:%d) \n",
           cm_id, UpFlowTable[tbindex].upstream_record.flow_id,Scheduler::instance().clock(),
           tmpch->size(),UpFlowTable[tbindex].curr_gsize,len+1,tmpch->size()/bytes_pminislot, UpFlowTable[tbindex].curr_gsize/bytes_pminislot,UpFlowTable[tbindex].numberPacketsInNextConcat_);
//Does this packet really get dropped ?
//         MACstats.total_packets_dropped++;
         UpFlowTable[tbindex].total_drops_fragmentation++;
//         UpFlowTable[tbindex].total_collision_drops++;
//          drop(p);
          return 0;
        }
      }

      set_bit(&UpFlowTable[tbindex].upstream_record.flag, CONCAT_ON_BIT,OFF);
      
      if (UpFlowTable[tbindex].debug)
        printf("CM%d(flow-id %d):send_concat:(%lf) Turning off CONCAT \n",
         cm_id, UpFlowTable[tbindex].upstream_record.flow_id,Scheduler::instance().clock());

      /* 
	 This is the case where sufficient packets are not available in the 
	 queue to make concat frame Since originally size of concat frame 
	 was requested and it is not being sent, we can still try to send 
	 piggyback request..(provided it is enabled)but, if sufficient space 
	 is not available, then shud not send it. So, check whether there is 
	 space for piggy req in current grant, If no then set the NO_PIGGY_BIT 
      */
      rc =  decide_frag(p,tbindex);
      return rc;
    } 
//
  UpFlowTable[tbindex].totalConcatFrames++;
  UpFlowTable[tbindex].totalPacketsInConcatFrames = UpFlowTable[tbindex].totalPacketsInConcatFrames + len + 1;

     if (UpFlowTable[tbindex].debug)
         printf("CM%d(flow-id %d):send_concat:(%lf), len:%d, will allocate a pkt with data len:%d\n",
                 cm_id, UpFlowTable[tbindex].upstream_record.flow_id,Scheduler::instance().clock(),
                 len,(len+1)*sizeof(int));

  int i = 0;
  Packet* r;
  
  /* Allocate memory for the new concatenated frame */
//  Packet *c = AllocPkt((UpFlowTable[tbindex].max_concat_threshhold + 1)*sizeof(int));	
  //  ??  But what if we don't send len packets ?
  Packet *c = AllocPkt((len+1)*sizeof(int));	
  int *data = (int*) c->accessdata();
  struct hdr_cmn* ch = HDR_CMN(c);
  struct hdr_cmn* chp = HDR_CMN(p);
  struct hdr_docsis* dh = HDR_DOCSIS(c);
  struct hdr_docsisextd* eh = HDR_DOCSISEXTD(c);
  
  eh->num_hdr = 0;
//Insert the first packet
  *data++ = (int)p;
  ch->size() = chp->size();
  


  r = p;

  if (UpFlowTable[tbindex].debug)
       printf("CM%d(flow-id %d):send_concat:(%lf), concat pkt size:%d\n",
              cm_id, UpFlowTable[tbindex].upstream_record.flow_id,Scheduler::instance().clock(),
             ch->size());
  
//Next, insert the remaining len packets 
  for (i = 0; i <= (len-1) ; i++)
  {
     //error check- should not be 0
     //if it is, then UpFlowTable[tbindex].numberPacketsInNextConcat_; 
     //was 1 off (too high)
//     if (len_queue(tbindex) > 0) 
      //$A510
      if ((myUSSFMgr->len_queue(p,tbindex)) > 0) {

      //$A510
      r = myUSSFMgr->deque_pkt(mySFObj);
//      r = deque_pkt(tbindex);
      struct hdr_cmn* chr = HDR_CMN(r);

// don't do this.... but increment BYTES and samples.  So we want a sample of 0 for the concatenated packet ???
//      UpFlowTable[tbindex].avg_req_stime += (Scheduler::instance().clock() - UpFlowTable[tbindex].enque_time);
      UpFlowTable[tbindex].avg_req_stimeBYTES += chr->size();
      UpFlowTable[tbindex].num_delay_samples++;

//$A601
  UpFlowTable[tbindex].channelDelayBytes += chr->size();
  UpFlowTable[tbindex].channelDelayCountSamples++;

   CMstats.channelDelayCountSamples++;
   CMstats.channelDelayBytes += ch->size();

  CMstats.avg_req_stime_status += (Scheduler::instance().clock() - UpFlowTable[tbindex].enque_time);
  CMstats.avg_req_stimeBYTES_status += chr->size();
  CMstats.num_delay_samples_status++;
  if (UpFlowTable[tbindex].debug) {
     printf("CM%d(flow-id %d):send_concat(%lf): NRAD CASE 3 UPDATE, delay:%lf, avg req delay:%lf, Bytes:%lf, #samples:%d \n",
	       cm_id,UpFlowTable[tbindex].upstream_record.flow_id,
	       Scheduler::instance().clock(),
               Scheduler::instance().clock() - UpFlowTable[tbindex].enque_time,
               UpFlowTable[tbindex].avg_req_stime / UpFlowTable[tbindex].num_delay_samples,
               UpFlowTable[tbindex].avg_req_stimeBYTES,
               UpFlowTable[tbindex].num_delay_samples);
  }

#ifdef TIMINGS
//	printf("2 %lf %d %d 4\n",Scheduler::instance().clock(),
//			UpFlowTable[tbindex].upstream_record.flow_id,cm_id); 
        timingsTrace(r,2);
#endif

      
      
      ch->size() += chr->size();

      /* Increse the pkt size bu upstream overhead bytes, to prevent
	 spl handling for concat frames. */
      *data++ = (int)r;

     if (UpFlowTable[tbindex].debug)
         printf("CM%d(flow-id %d):send_concat:(%lf), Added pkt: updated concat pkt size:%d\n",
                 cm_id, UpFlowTable[tbindex].upstream_record.flow_id,Scheduler::instance().clock(),
                 ch->size());
     }
     else {
       printf("CM%d(flow-id %d):send_concat:(%lf) ERROR: len 0, i:%d, #InNExt:%d, qlen:%d) \n",
         cm_id, UpFlowTable[tbindex].upstream_record.flow_id,Scheduler::instance().clock(),
       i,UpFlowTable[tbindex].numberPacketsInNextConcat_,myUSSFMgr->len_queue(p, tbindex));
       break;
     }
  }	
  dh->dshdr_.fc_type = MAC_SPECIFIC;
  dh->dshdr_.fc_parm = CONCATENATION;
  dh->dshdr_.ehdr_on = 0;
//  dh->dshdr_.mac_param = UpFlowTable[tbindex].max_concat_threshhold + 1;
// dh->dshdr_.mac_param = len + 1;
// Had a bug where very rarely max_concat_threshold was 1 larger than len_queue.
  //JJM WRONG
 dh->dshdr_.mac_param = i + 1;

  dh->dshdr_.len = ch->size();
  ch->size() += DOCSIS_HDR_LEN;
  ch->ptype() = PT_DOCSISCONCAT;
  
  set_bit(&UpFlowTable[tbindex].upstream_record.flag, CONCAT_ON_BIT,OFF);
 
  /* Turn on a bit to indicate that piggy-backibg shud not be done.*/
  set_bit(&UpFlowTable[tbindex].upstream_record.flag, NO_PIGGY_BIT,ON);  

//$A701
// Check if frag is not enabled and the CMTS allocated a partial grant
  if (!(bit_on(UpFlowTable[tbindex].upstream_record.flag,FRAG_ENABLE_BIT))) {
    if (UpFlowTable[tbindex].curr_gsize <  ch->size()) {
     printf("CM%d(flow-id %d):send_concat:(%lf)  HARD ERROR SENDING concat frame of size:%d  containing %d packets (%d total slots) (grantSize:%d,#InNExt:%d) \n",
       cm_id, UpFlowTable[tbindex].upstream_record.flow_id,Scheduler::instance().clock(),
       ch->size(),len+1,ch->size()/bytes_pminislot, UpFlowTable[tbindex].curr_gsize/bytes_pminislot,UpFlowTable[tbindex].numberPacketsInNextConcat_);
    }
  }

//A702  set OriginalFrag if frag is  going to happen
//  if ((bit_on(UpFlowTable[tbindex].upstream_record.flag,FRAG_ENABLE_BIT))) {
     UpFlowTable[tbindex].OriginalFragPkt = p;
//  }

  rc = decide_frag(c, tbindex);

  if (rc) {
    if (UpFlowTable[tbindex].debug)
      printf("CM%d(flow-id %d):send_concat: sending a FRAGMENTED concat frame at %lf of size %d (%d slots) with %d packets)\n",
	   cm_id,
	   UpFlowTable[tbindex].upstream_record.flow_id,
	   Scheduler::instance().clock(),
	   ch->size(),ch->size()/bytes_pminislot,dh->dshdr_.mac_param);
  }
  else {
    if (UpFlowTable[tbindex].debug)
//JJM BUG this always shows 0 size 
      printf("CM%d(flow-id %d):send_concat: sending a NONFRAGMENTED concat frame at %lf of size %d (%d slots) with %d packets)\n",
	   cm_id,
	   UpFlowTable[tbindex].upstream_record.flow_id,
	   Scheduler::instance().clock(),
	   ch->size(),ch->size()/bytes_pminislot,dh->dshdr_.mac_param);
  }
  
  return rc;
}

/*************************************************************************

*************************************************************************/
void MacDocsisCM::RecvFrame(Packet* p,int dummy)
{
  char tbindex,t;
  struct hdr_mac* mh = HDR_MAC(p);
  struct hdr_docsis* dh = HDR_DOCSIS(p);
  struct hdr_cmn* ch = HDR_CMN(p);
  double curr_time = Scheduler::instance().clock();
 
#ifdef TRACE_DEBUG
  tracePoint("CM:RecvFrame:",cm_id,0);
#endif

// TODO:  add summary packet trace
  if (debug_cm)
    printf("MacDocsisCM:RecvFrame[%s]:(%lf): pkt of size %d (type:%d) arrived on cm_id: %d\n",
	   docsis_id,Scheduler::instance().clock(),ch->size(),ch->ptype_,cm_id);
  
  /* Update the statistics variable.. */
  if (curr_time - MACstats.last_rtime >= 1.0)
    {
      MACstats.last_rtime = curr_time;


#ifdef TRACE_CM  //---------------------------------------------------------
      //printf("Avg pkts = %d Avg bytes = %d\n",avg_pkts,avg_bytes);
#endif //-------------------------------------------------------------------      
    }       

  MACstats.total_num_BW_bytesDOWN += ch->size();
  CMstats.total_BW_bytesDOWN_status += ch->size(); 

  MACstats.total_num_rx_frames++;
  MACstats.total_num_rx_bytes += ch->size(); 

  //#ifdef TRACE_CM_DOWN_DATA //-----------------------------------------------
  if (debug_cm)
    printf("CM%ddown %lf %f %d\n",
	   cm_id,Scheduler::instance().clock(),MACstats.total_num_rx_bytes,ch->size());
  //#endif //------------------------------------------------------------------
  
  //dump_stats();

  tbindex = classify(p,DOWNSTREAM);
  t = ClassifyDataMgmt(p);
  
  if (debug_cm)
    printf("MacDocsisCM:CM%d :RecvFrame(%lf): CM receives a docsis frame: type: %d and size:%d\n",
	   cm_id, Scheduler::instance().clock(), t, ch->size());

  if (t == 0)
  {
    handle_indata(p,tbindex);
  }
  else if (t == 1)
  {
    handle_inmgmt(p,tbindex);
  }
}

/*************************************************************************

*************************************************************************/
void MacDocsisCM::dump_stats()
{
  double curr_time = Scheduler::instance().clock();
  
  if (curr_time - last_dmptime >= 1.0)
    {
      int i;
      last_dmptime = curr_time;
      printf("Dumping stats for CM %d(mac-addr) at %lf\n",index_,curr_time);

      //printf("Avg Num of pkts received ps = %d\n",num_pkts);
      //printf("Avg Num of bytes received ps = %d\n",num_bytes);
      //printf("Avg Num of mgmt pkts received ps = %d\n",MACstats.num_mgmtpkts);
      //printf("Avg Num of mgmt bytes received ps = %d\n",MACstats.num_mgmtbytes);
      //printf("\n");	
      
      for (i = 0; i < SizeUpFlowTable; i++)
	{
	  printf("Dumping stats for flow-id %d\n",UpFlowTable[i].upstream_record.flow_id);

	  //printf("Avg Num of pkts sent ps %d\n",UpFlowTable[i].num_pkts);
	  //printf("Avg Num of bytes sent ps %d\n",UpFlowTable[i].num_bytes);

	  printf("Avg queuing delay  %lf\n",UpFlowTable[i].avg_queuing_delay);
	  printf("Packets dropped  %d\n",UpFlowTable[i].drop_count);
	  UpFlowTable[i].drop_count = 0;

	  //printf("Avg number of slots pm  %d\n",UpFlowTable[i].avg_slotspermap);

	  printf("\n");	
	}      
    }
}

/*************************************************************************

*************************************************************************/
void MacDocsisCM::handle_indata(Packet* p, char tbindex)
{
  struct hdr_cmn *ch = HDR_CMN(p);
 // $A305: 
   docsis_dsehdr *chx = docsis_dsehdr::access(p);     


  if (debug_cm) {
       printf("MacDocsisCM::handle_indata[%s](%lf): packet arrives, size: %d, dsid:%d,  PSN:%d\n", docsis_id,
          Scheduler::instance().clock(),hdr_cmn::access(p)->size(),chx->dsid,chx->packet_sequence_number);
      fflush(stdout);
  }


  if (debug_cm)
    {
//      printf("CM%d receives data packet\n",cm_id);
//      dump_pkt(p);
//      printf("CM%d :Packet classified on flow-id = %d\n",
//	     cm_id,DownFlowTable[tbindex].downstream_record.flow_id);
    }

  PhUnsupress(p, tbindex);
  ch->size() -= ETHER_HDR_LEN;
  ch->size() -= DOCSIS_HDR_LEN;
  ch->num_forwards() += 1;
  
  MACstats.total_num_appbytesDS += ch->size();
  MACstats.total_num_appPktsDS++;
  

  if (debug_cm)
  {
    printf("mac-docsiscm.cc:: calling uptarget_ ->recv in handle_indata\n");
    fflush(stdout);
  }

//$A307  - highest point in the MAC for inbound IP packets
  if (myDSpacketMonitor != NULL)
  {
    //$A400
    myDSpacketMonitor->updateStats(p);
    myDSpacketMonitor->packetTrace(p);
  }

  uptarget_->recv(p, (Handler*) 0);
  return;
}

/*************************************************************************

*************************************************************************/
void MacDocsisCM::handle_inmgmt(Packet* p, char tbindex)
{
  hdr_docsis   *dh  = hdr_docsis::access(p);
  hdr_cmn   *ch  = HDR_CMN(p);
  hdr_mac   *mp  = HDR_MAC(p);
  struct hdr_docsismgmt *mgmt = HDR_DOCSISMGMT(p);  
  double curr_time = Scheduler::instance().clock();
  
  if (curr_time - MACstats.last_mmgmttime >= 1.0)
    {
      MACstats.last_mmgmttime = curr_time;
      MACstats.avg_mgmtpkts = (MACstats.avg_mgmtpkts + MACstats.num_mgmtpkts )/ 2;
      MACstats.avg_mgmtbytes = (MACstats.avg_mgmtbytes + MACstats.num_mgmtbytes )/ 2;
      MACstats.num_mgmtpkts = 0;
      MACstats.num_mgmtbytes = 0;
      
      if (debug_cm)
	printf("CM:handle_inmgmt: Avg Management pkts received %d Avg Management bytes %d \n",
	       MACstats.avg_mgmtpkts,
	       MACstats.avg_mgmtbytes);      
    }   
  MACstats.num_mgmtpkts++;
  MACstats.num_mgmtbytes += ch->size();

  //$A405
  ch->size() -= DOCSIS_HDR_LEN;
  ch->size() -= SIZE_MGMT_HDR;

  if (debug_cm)
  {
    printf("handle_inmgmt:(%lf):CM%d receives mgt packet of size:%d\n",curr_time,cm_id,ch->size());
  }
  if (mgmt->type == 1)
    HandleMap(p);  
  else
    HandleOtherMgmt(p);

  return;
}

/*************************************************************************

*************************************************************************/
void MacDocsisCM::HandleOtherMgmt(Packet* p)
{  
  struct hdr_cmn *ch = HDR_CMN(p);
  struct hdr_docsismgmt *mgmt = HDR_DOCSISMGMT(p);  
  hdr_docsis   *dh  = hdr_docsis::access(p);
  double curr_time = Scheduler::instance().clock();
  u_int32_t * data = (u_int32_t *)p->accessdata();
  struct CMUpdate_docsismgmt CMUpdateMsg;
  int msgSize = sizeof (struct CMUpdate_docsismgmt);
  u_char *srcPtr,*dstPtr;
  int i;
  int nextbkoffScale = 1;

  
  /* Do nothing */
  if (debug_cm)
    printf("CM:HandleOtherMgmt(%f): CM%d receives a management message type:%d\n",
	   curr_time,cm_id,mgmt->type_());
  
  if (mgmt->type_() == CMUPDATEMESSAGE) {
    //Now copy the message data 
    srcPtr = (u_char *)data;
    dstPtr = (u_char *)&CMUpdateMsg;

    for (i=0;i<msgSize;i++) {
       *dstPtr++ = *srcPtr++;
    }

    if ((cm_id <= msgSize) && (cm_id > 0)){
      nextbkoffScale = CMUpdateMsg.newScaleParam[(cm_id-1)];
    }
    else
      nextbkoffScale = 1;

    
    if (debug_cm)
      printf("CM:HandleOtherMgmt(%f): CM%d receives a CMUPDATEMESSAGE:msgSize:%d, prev bkoffScale:%d, new bkoffScale:%d\n",
        curr_time,cm_id,msgSize,bkoffScale,nextbkoffScale);
    
    bkoffScale = nextbkoffScale;
    avg_bkoffScale += bkoffScale; 
    avg_bkoffScale_count++;

  }

  Packet::free(p);
}

/*************************************************************************

*************************************************************************/
char MacDocsisCM::classify(Packet* p,char dir)
{
  char i;
  
  if (dir == DOWNSTREAM)
    {
      for (i = 0; i < SizeDownFlowTable; i++)
	{
	  if (match(p, DownFlowTable[i].downstream_record.classifier))
	    return i;
	}
      return default_dstream_index_; /* Default service-flow for all the traffic */
    }
  else
    if (dir == UPSTREAM)
      {
	for (i = 0; i < SizeUpFlowTable; i++)
	  {
            if (debug_cm)
               printf("CM:classify(%lf):upstream flow:  try flow table entry %d\n",Scheduler::instance().clock(),i);

	    if (match(p, UpFlowTable[i].upstream_record.classifier)) {
               if (debug_cm)
                 printf("CM:classify(%lf): upstream: matched flow  %d\n",Scheduler::instance().clock(),i);
	      return i;
	    }
	  }
        if (debug_cm)
          printf("CM:classify(%lf): returning default upstream index: %d \n",Scheduler::instance().clock(),default_upstream_index_);

	return default_upstream_index_; /* Default service-flow for all the traffic */
      }
    else
      {
	printf("CM%d :MacDocsisCM->ClassifyServiceFlow: Error, Unknow direction, Exiting\n",cm_id);
	exit(1);
      }
}

/*************************************************************************

*************************************************************************/
void MacDocsisCM::PhUnsupress(Packet* p, char tbindex)
{
  struct hdr_cmn *ch = HDR_CMN(p);
  switch(DownFlowTable[tbindex].downstream_record.PHS_profile) 
    {
    case SUPRESS_ALL:
      ch->size() += SUPRESS_ALL_SIZE - 2 ;
      break;
    
    case SUPRESS_TCPIP:
      ch->size() += SUPRESS_TCPIP_SIZE - 2;
      break;
    
    case SUPRESS_UDPIP:
      ch->size() += SUPRESS_UDPIP_SIZE - 2;
      break;
    
    case SUPRESS_MAC:
      ch->size() += SUPRESS_MAC_SIZE - 2;
      break;
    
    case NO_SUPRESSION:
      break;
    
    default:
      printf("CM%d :MacDocsisCM->PhUnsupress:Unknown PHS type..\n",cm_id);
      break;
    }
  return;  
}

/*************************************************************************
verified
*************************************************************************/
void MacDocsisCM::ApplyPhs(Packet* p, char tbindex)
{
  struct hdr_cmn *ch = HDR_CMN(p);
  
  switch(UpFlowTable[tbindex].upstream_record.PHS_profile) 
    {
    case SUPRESS_ALL:
      ch->size() -= (SUPRESS_ALL_SIZE - 2) ;
      break;
    
    case SUPRESS_TCPIP:
      ch->size() -= (SUPRESS_TCPIP_SIZE - 2);
      break;
    
    case SUPRESS_UDPIP:
      ch->size() -= (SUPRESS_UDPIP_SIZE - 2);
      break;
    
    case SUPRESS_MAC:
      ch->size() -= (SUPRESS_MAC_SIZE - 2);
      break;
    
    case NO_SUPRESSION:
      break;
    
    default:
      printf("CM%d :MacDocsisCM->ApplyPhs:Unknown PHS type..\n",cm_id);
      break;
    }
  return;
}

/*************************************************************************

*************************************************************************/
void MacDocsisCM::dumpFinalCMStats(char *outputFile)
{
  double curr_time = Scheduler::instance().clock();
  FILE* fp = NULL;
  u_int32_t total_piggyreq = 0, total_creq = 0,total_fcoll = 0;
  u_int32_t num_delay_samples = 0, num_NORMALIZED_delay_samples=0;
  u_int32_t queuing_samples = 0, total_cdrops = 0, total_qdrops = 0;
  //$A701
  u_int32_t total_fdrops=0;
  u_int32_t total_dreq = 0;

//$A702
  u_int32_t fcont_count = 0;
  u_int32_t avg_TotalBackoffCount = 0;
  u_int32_t avg_BackoffCount = 0;
//  double w,x,y,z;
 
  double avg_TotalBackoff = 0.0;
  double avg_Backoff = 0.0;
  double avgAdaptiveBKOFF  = 0;
  double avg_fcont = 0.0;
  
  u_int32_t totalACKs = 0, totalACKsFiltered = 0;
  
  double app_DSbw =0.0;
  double app_USbw =0.0;
  double avg_access_delay = 0.0,avg_queuing_delay = 0.0;
  double avg_NORMALIZED_access_delayBYTES = 0;
  double avg_NORMALIZED_access_delay = 0;


  int AggregatetotalConcatFrames = 0;
  int AggregatetotalPacketsInConcatFrames = 0;
  double avgNumberPacketsPerConcat = 0;
  struct packetStatsType USstats;
  struct packetStatsType DSstats;
  struct macPhyInterfaceStatsType macPhyInterfaceStats;
  struct channelStatsType channelStats;
  struct serviceFlowMgrStatsType SFMgrStats;
  int i;
  


  bzero((void *)&USstats,sizeof (struct packetStatsType));
  bzero((void *)&DSstats,sizeof (struct packetStatsType));
  bzero((void *)&macPhyInterfaceStats,sizeof (struct macPhyInterfaceStatsType));
  bzero((void *)&channelStats,sizeof (struct channelStatsType));
  bzero((void *)&SFMgrStats,sizeof (struct serviceFlowMgrStatsType));


  //Get the raw stats data for the concise data output file 
  myDSpacketMonitor->getStats(&DSstats);
  myUSpacketMonitor->getStats(&USstats);


  if (MACstats.total_num_sent_frames == 0)
    MACstats.total_num_sent_frames = 1;


//  if (debug_cm) {
  printf("dumpFinalCMStats(CMID:%d): numChannels:%d  size of Flow Table:  %d \n",cm_id,SizeUpFlowTable);
  printf("  total_num_sent_bytes(frames): %f (%d),  total_num_rx_bytes(frames):%f (%d) \n",
            MACstats.total_num_sent_bytes,MACstats.total_num_sent_frames,
	        MACstats.total_num_rx_bytes,MACstats.total_num_rx_frames);

  printf("dumpFinalCMStats: DS packet monitor:  \n");
  myDSpacketMonitor->printStatsSummary();
  printf("dumpFinalCMStats: US packet monitor:  \n");
  myUSpacketMonitor->printStatsSummary();

  //get summary of binary packet trace files
  printf("dumpFinalCMStats: DS BINARY packet monitor:  \n");
  myDSpacketMonitor->getBinaryTraceSummary();
  printf("dumpFinalCMStats: US BINARY packet monitor:  \n");
  myUSpacketMonitor->getBinaryTraceSummary();

  printf("Total application bytes(pkts) DS: %f (%d)  Total application bytes(pkts) US: %f (%d) \n",
       MACstats.total_num_appbytesDS,MACstats.total_num_appPktsDS,MACstats.total_num_appbytesUS, MACstats.total_num_appPktsUS);


   myPhyInterface->getStats(&macPhyInterfaceStats);
   myPhyInterface->printStatsSummary();
   printf("dumpFinalCMstats:  reseqMgr stats ... \n");
// myPhyInterface calls this...
//   myPhyInterface->myReseqMgr->printStatsSummary("DSCMSIDstats.out");

  printf("dumpFinalCMstats(cm_id:%d):  US SF Mgr stats ... \n",cm_id);
  myUSSFMgr->printStatsSummary("CMUSSIDstats.out");

  myUSSFMgr->getStats(&SFMgrStats);
  printf("mac-docsiscm::dumpFinalCMstats: CM US SFs (%d) \n",SFMgrStats.numberServiceFlows);
  printf("CMstats: CM US SFs (%d) \n",SFMgrStats.numberServiceFlows);
  printf("CMstats(%d) numberSFs:%d, avgArrivalRate:%lf bps, avgServiceRate:%lf bps,   avgLR:%3.3f   avgPacketDelay:%3.6f seconds      \n",
      cm_id, SFMgrStats.numberServiceFlows, SFMgrStats.avgArrivalRate, SFMgrStats.avgServiceRate,SFMgrStats.avgLossRate,SFMgrStats.avgPacketDelay);

   //dump per channel stats
   int numDSChannels = myPhyInterface->getNumberDSChannels();
   int numUSChannels = myPhyInterface->getNumberUSChannels();
   printf("mac-docsiscm::dumpFinalCMstats: numDSChannels: %d,  numUSChannels: %d \n",numDSChannels,numUSChannels);

  /* Calculate per cm stats based on per flow stats */
  for (int i= 0; i < SizeUpFlowTable; i++)
  {
      total_piggyreq += UpFlowTable[i].total_piggyreq;
      
      totalACKs += UpFlowTable[i].totalACKs;
      totalACKsFiltered += UpFlowTable[i].totalACKsFiltered;
      

      total_dreq += UpFlowTable[i].total_num_req_denied;
      total_fdrops += UpFlowTable[i].total_drops_fragmentation; 
      total_cdrops += UpFlowTable[i].total_collision_drops;
      total_qdrops += UpFlowTable[i].total_queue_drops;
      total_creq += UpFlowTable[i].total_creq;
      total_fcoll += UpFlowTable[i].total_fcoll;
      avg_fcont += (double) UpFlowTable[i].avg_fcont;
      fcont_count += UpFlowTable[i].fcont_count;
//$A702
      avg_Backoff +=  (double)UpFlowTable[i].avg_Backoff;
      avg_BackoffCount +=  UpFlowTable[i].avg_BackoffCount;
      avg_TotalBackoff +=  (double)UpFlowTable[i].avg_TotalBackoff;
      avg_TotalBackoffCount += UpFlowTable[i].avg_TotalBackoffCount;

      printf("dumpFinalCMStats:(cm_id:%d)(flow # %d, out of %d flows): Flows:avg_fcont:%d, fcont_count:%d   avg_TotalBackoffCont:%d, avg_TotalBackofCount:%d, avg_Backoff:%d, avg_BackoffCount:%d \n", cm_id,i,SizeUpFlowTable,
         UpFlowTable[i].avg_fcont,
         UpFlowTable[i].fcont_count,
         UpFlowTable[i].avg_TotalBackoff,
         UpFlowTable[i].avg_TotalBackoffCount,
         UpFlowTable[i].avg_Backoff,
         UpFlowTable[i].avg_BackoffCount);

      queuing_samples += UpFlowTable[i].queuing_samples;
      avg_access_delay += UpFlowTable[i].avg_req_stime;
      num_delay_samples += UpFlowTable[i].num_delay_samples;
      if (UpFlowTable[i].avg_req_stimeBYTES > 0) {
        num_NORMALIZED_delay_samples += UpFlowTable[i].num_delay_samples;
        avg_NORMALIZED_access_delay += (UpFlowTable[i].avg_req_stime/UpFlowTable[i].avg_req_stimeBYTES);
      }

      avg_NORMALIZED_access_delayBYTES += UpFlowTable[i].avg_req_stimeBYTES;

      avg_queuing_delay += UpFlowTable[i].avg_queuing_delay;      
      AggregatetotalConcatFrames += UpFlowTable[i].totalConcatFrames;
      AggregatetotalPacketsInConcatFrames += UpFlowTable[i].totalPacketsInConcatFrames;
  
      if (MACstats.total_num_sent_frames == 0)
        MACstats.total_num_sent_frames = 1;
  
      if (debug_cm) {
         printf("dumpFinalCMStats (flow # %d): %f %f %f %d %d %f %f %f\n", i, avg_access_delay, 
                 avg_NORMALIZED_access_delay, avg_NORMALIZED_access_delayBYTES, 
                 num_delay_samples, num_NORMALIZED_delay_samples, avg_queuing_delay,
                 UpFlowTable[i].avg_req_stime, UpFlowTable[i].avg_req_stimeBYTES);
      }
  }
  

//START

//$A702

  if (fcont_count > 0)
     avg_fcont =  avg_fcont / fcont_count;
  else
     avg_fcont = 0;

  if (avg_TotalBackoffCount > 0)
     avg_TotalBackoff = avg_TotalBackoff / avg_TotalBackoffCount;
  else
     avg_TotalBackoff = 0;

  if (avg_BackoffCount > 0)
     avg_Backoff = avg_Backoff / avg_BackoffCount;
  else
     avg_Backoff = 0;

  if (avg_bkoffScale_count > 1)
    avgAdaptiveBKOFF  = avg_bkoffScale/ avg_bkoffScale_count; 
  else
    avgAdaptiveBKOFF = 0;

  printf("dumpFinalCMStats:(cm_id:%d)  Total num of first collisions: %d, Avg First Contention backoff(slots): %lf(%d)  avgTotalBackoff:%f(%d),  avg Incident Backoff:%f(%d), avg Adaptive Backoff:%f(%d)\n",
	 cm_id,total_fcoll, avg_fcont,fcont_count, avg_TotalBackoff, avg_TotalBackoffCount,avg_Backoff,avg_BackoffCount, avgAdaptiveBKOFF,avg_bkoffScale_count);


//DONE


  if (queuing_samples > 0)
    avg_queuing_delay = avg_queuing_delay / queuing_samples;
  else
    avg_queuing_delay = 0;


  if (num_delay_samples >0){
    avg_access_delay = avg_access_delay / num_delay_samples;
    if (num_NORMALIZED_delay_samples >0){
//Normalize to delay per megabyte
      avg_NORMALIZED_access_delay = (avg_NORMALIZED_access_delay / num_NORMALIZED_delay_samples) * 1000000;
    }
    else
      avg_NORMALIZED_access_delay = 0;
  }
  else
    avg_access_delay = 0;

  if (debug_cm) {
    printf("dumpFinalCMStats: %d %f %f %f %d %f \n", cm_id,avg_access_delay, avg_NORMALIZED_access_delay, avg_NORMALIZED_access_delayBYTES, num_delay_samples, avg_queuing_delay);
  }
  if (AggregatetotalConcatFrames > 0)
    avgNumberPacketsPerConcat = (double)  ((double)AggregatetotalPacketsInConcatFrames/ (double)AggregatetotalConcatFrames);
  //$A400
  else
     avgNumberPacketsPerConcat = 0;


   //$A400
   //make sure there are no 0's 
   if (MACstats.total_num_sent_frames == 0)
       MACstats.total_num_sent_frames=1;


  
#ifdef SHORT_STATS //--------------------------------------------------------------------
  if (outputFile != NULL) {
    fp = fopen(outputFile, "a+");


//fields 1 - 6
    fprintf(fp,"%d %16.6f %16.0f %d %16.0f %d  ", cm_id, curr_time,
       MACstats.total_num_sent_bytes, MACstats.total_num_sent_frames,
       MACstats.total_num_rx_bytes, MACstats.total_num_rx_frames);

//fields 7-10
    fprintf(fp,"%16.0f %d %16.0f %d ",
	       USstats.byteCount,USstats.packetCount, DSstats.byteCount,DSstats.packetCount);


//fields 11-12
//    fprintf(fp,"%d %3.3f ",
//            total_qdrops, ((double)total_qdrops/(double)MACstats.total_num_sent_frames));
    if (MACstats.total_num_frames_US > 0)
      fprintf(fp,"%d %3.3f ",
       MACstats.total_packets_dropped, 
      (double)MACstats.total_packets_dropped/(double)MACstats.total_num_sent_frames);
    else
      fprintf(fp,"%d 0.0 ", MACstats.total_packets_dropped );


//fields 13-14
    if (total_creq > 0)
      fprintf(fp,"%d %3.3f ", CMstats.total_num_collisions, 
          (double)100*(double)CMstats.total_num_collisions/total_creq);
    else
      fprintf(fp,"%d 0.0", CMstats.total_num_collisions);
    
    app_DSbw = ((DSstats.byteCount)*(8))/((Scheduler::instance().clock()));
    app_USbw = ((USstats.byteCount)*(8))/((Scheduler::instance().clock()));
    

//fields 15 - 28
    if (fcont_count ==0)
       fcont_count=1;
	//total_fcoll is the total added over all CMs of the number of first collisions
    // total_fcoll is the average ??
	//avg_fcont is the average backoff values based on all cms 
    fprintf(fp,"  %d %d %16.0f %16.0f %d %d %d %3.1f %3.1f %3.1f %3.6f %3.6f %3.6f %3.6f ",
	    total_cdrops, total_fdrops, 
	    app_USbw, app_DSbw,  total_piggyreq, total_creq, total_dreq,
	    (double)total_fcoll/(double)fcont_count, avg_fcont, avg_Backoff,  avg_queuing_delay+ avg_access_delay, 
	    avg_queuing_delay, avg_access_delay, avg_NORMALIZED_access_delay);


//Fields 29 - 38
    if (totalACKs > 0)
      fprintf(fp,"  %d %d %3.3f %3.3f %d %d %d %d  \n",
      	totalACKs, totalACKsFiltered, 
        ((double)totalACKsFiltered/(double)totalACKs),avgNumberPacketsPerConcat,
		MACstats.total_num_frames_US,
        MACstats.total_num_mgt_pkts_US,MACstats.total_num_status_pkts_US,CMstats.total_num_frag);
    else
      fprintf(fp,"  %d %d 0.0 %3.3f %d %d %d %d %d %d \n",
      	totalACKs, totalACKsFiltered, 
        avgNumberPacketsPerConcat,MACstats.total_num_frames_US,
        MACstats.total_num_mgt_pkts_US,MACstats.total_num_status_pkts_US,CMstats.total_num_frag,
        AggregatetotalConcatFrames, AggregatetotalPacketsInConcatFrames);

    
    fclose(fp);
  }
  if (debug_cm) 
  {
      printf("%16.0f %d %16.0f %d  ", 
		MACstats.total_num_sent_bytes, MACstats.total_num_sent_frames,
       MACstats.total_num_rx_bytes, MACstats.total_num_rx_frames);

      printf("%16.0f %d %16.0f %d ",
	     DSstats.byteCount,DSstats.packetCount, USstats.byteCount,USstats.packetCount);

    if (MACstats.total_num_frames_US > 0)
         printf("%d %3.3f  ",
            total_qdrops, ((double)total_qdrops/(double)MACstats.total_num_sent_frames)*100);
    else
         printf("%d 0 ", total_qdrops);


    if (MACstats.total_num_frames_US > 0)
      printf("%d %.3f ",CMstats.total_num_collisions, (double)100*(double)CMstats.total_num_collisions/MACstats.total_num_frames_US);
    else
      printf("%d 0.0",CMstats.total_num_collisions);
    
      
   if (totalACKs > 0)
      printf("totalACKs:%d, totalACKsFiltered:%d percentageFiltered:%f \n",
	       totalACKs, totalACKsFiltered, 
	       ((double)totalACKsFiltered/(double)totalACKs));
    else
       printf("totalACKs:%d, totalACKsFiltered:%d percentageFiltered:0 \n",
	       totalACKs, totalACKsFiltered);

       printf("avg # packets Per concat frame:%f,TotalConcatFrames%d, TotalIPPacketsInConcat:%d, totalFrames:%d\n", 
		      avgNumberPacketsPerConcat, AggregatetotalConcatFrames, AggregatetotalPacketsInConcatFrames,MACstats.total_num_frames_US); 

  }
#endif//----- SHORT_STATS -------------------------------------------------------------------
  
  printf("\nCM%d dumpFinalCMStats(%f) Total drops: %d;  loss rate: %f percent ",
	 cm_id, curr_time, MACstats.total_packets_dropped, 
	 ((double)MACstats.total_packets_dropped/(double)MACstats.total_num_sent_frames)*100);
  
  printf("\n                     Total Packets received downstream: %d, Total Packets sent upstream: %d ",
	 MACstats.total_num_rx_frames, MACstats.total_num_sent_frames);

  printf("\n                     Total Bytes received downstream: %f, Total Bytes sent upstream: %f ",
	 MACstats.total_num_rx_bytes, MACstats.total_num_sent_bytes);
  
  if (total_creq > 0)
    printf("\n                     Total Collisions: %d,(rate:%2.4f)   Total Fragments: %d\n",
	 CMstats.total_num_collisions, (double)100*(double)CMstats.total_num_collisions/total_creq,CMstats.total_num_frag);
   else
    printf("\n                     Total Collisions: %d,(rate: 0.0)   Total Fragments: %d\n",
	 CMstats.total_num_collisions, CMstats.total_num_frag);

  printf("Total drops due to max retries: %d, Total drops due to fragmentation: %d\n",
	 total_cdrops,total_fdrops);
  
  app_DSbw = ((DSstats.byteCount)*(8))/((Scheduler::instance().clock()));
  app_USbw = ((USstats.byteCount)*(8))/((Scheduler::instance().clock()));
  
  printf("Total num of App bytes on US: %d, Total num of App bytes on DS: %d Application bw in US(kbps) %lf, DSBW:%f\n",
	 MACstats.total_num_appbytesUS, MACstats.total_num_appbytesDS, app_USbw,app_DSbw);

  printf("Total Piggy req sent: %d, Total Contention req sent: %d\n",
	 total_piggyreq, total_creq);


  printf("Avg access delay: %lf Avg queuing delay: %lf\n",
	 avg_access_delay+avg_queuing_delay, avg_queuing_delay);

  printf("Avg request delay: %lf\n", avg_access_delay);
  printf("total Acks:%d, Acks filtered:%d 0 avg # packets Per concat frame:%f\n",totalACKs, totalACKsFiltered, avgNumberPacketsPerConcat);
  printf("Total number frames sent: %d\n", MACstats.total_num_frames_US);
  printf("Total number mgt frames sent: %d, rng sent:%d, status sent:%d\n", 
          MACstats.total_num_mgt_pkts_US,MACstats.total_num_rng_pkts_US,MACstats.total_num_status_pkts_US);
}

/*************************************************************************

*************************************************************************/
void MacDocsisCM::dumpBWCM(char *outputFile)
{
  double curr_time = Scheduler::instance().clock();
  double BWup = 0;
  double BWdown = 0;
  FILE* fp;    
  
  /*
    printf("CM:dumpBWCM: %f  Entered, current bytesUP:%f;  bytesDOWN:%f, MACstats.last_BWCalcTime: %6.2f, outputFile:%s \n",
    curr_time, MACstats.total_num_BW_bytesUP, MACstats.total_num_BW_bytesDOWN,
    MACstats.last_BWCalcTime, outputFile);
  */  
  fp = fopen(outputFile, "a+");
  
  if (curr_time - MACstats.last_BWCalcTime > 0)
    {      
      BWup = (MACstats.total_num_BW_bytesUP*8)/(curr_time - MACstats.last_BWCalcTime);
      BWdown = (MACstats.total_num_BW_bytesDOWN*8)/(curr_time - MACstats.last_BWCalcTime);
      MACstats.last_BWCalcTime = curr_time;
      MACstats.total_num_BW_bytesUP = 0;
      MACstats.total_num_BW_bytesDOWN = 0;
      
      fprintf(fp,"%f  %6.2f  %6.2f\n",curr_time, BWup, BWdown);
      
      if (debug_cm)
	    printf("CM%d: %f  %6.2f  %6.2f\n",cm_id,curr_time, BWup, BWdown);
    }
  else 
    {
      fprintf(fp,"%f  %6.2f  %6.2f\n",curr_time, BWup, BWdown);
      
      if (debug_cm)
	printf("CM%d: %f  %6.2f  %6.2f\n",cm_id,curr_time, BWup, BWdown);      
    }
  fclose(fp);
   
  //Don't we just want to call this for the UGS sched type ?
  for (int i = 0; i < SizeUpFlowTable; i++)
    {
      if (UpFlowTable[i].upstream_record.sched_type == UGS)
        dumpUGSJITTER(i,"UGSJitterCM.out");
    }  
}

/*****************************************************************************
 * Routine void MacDocsisCM::dumpUGSJITTER(char tbindex,char *outputFile)
 *
 * Explanation
 *    This is called periodically (as of 3/26/05 it is piggybacked off
 *    the dumpBWCM mechanism).  It computes a jitter statistic based
 *    on stats gathered since the last time this method was called.
 *    Each time a UGS allocation is made, the jitter is updated.  This running
 *    total of jitter is simply the difference between the scheduled
 *    allocation time (based on the UGS grant interval) and the actual.
 *
 * Inputs
 *
 * Outputs
 *
*************************************************************************/
void MacDocsisCM::dumpUGSJITTER(char tbindex,char *outputFile)
{
  double curr_time = Scheduler::instance().clock();
  double avgjitter = 0.0;
  FILE* fp;
  
  fp = fopen(outputFile, "a+");
  
      
      if (UpFlowTable[tbindex].jitterSamples > 0)
        avgjitter = UpFlowTable[tbindex].ugsjitter / UpFlowTable[tbindex].jitterSamples;
      else
        avgjitter = 0;

      
      //Can record a particular flow or all flows....
      if (UpFlowTable[tbindex].upstream_record.flow_id == 4) {
        if (debug_cm) {
          printf("CMdumpUGSJitter(%lf): flow_id = %d  avgjitter = %f (jitter:%lf, #samples:%d\n", 
                        curr_time,
                        UpFlowTable[tbindex].upstream_record.flow_id, avgjitter,
                        UpFlowTable[tbindex].ugsjitter, UpFlowTable[tbindex].jitterSamples);
	}
      
        fprintf(fp,"%lf %d %lf\n",curr_time, UpFlowTable[tbindex].upstream_record.flow_id,
		      avgjitter);
      }

      
      UpFlowTable[tbindex].ugsjitter = 0.0;
      UpFlowTable[tbindex].jitterSamples = 0;
      UpFlowTable[tbindex].last_jittercaltime = curr_time;
  fclose(fp);
}


/*****************************************************************************
Called periodically by a TCL script to monitor the queue in the DS direction....
This method also computes the utilization of the DS channel for each iteration....
*****************************************************************************/
void MacDocsisCM::dumpDOCSISQueueStats(char *outputFile)
{
 
 
  if (debug_cm)
    printf("CMTS:dumpDOCSISQueueStats: %f    total_num_sent_bytes:%f   \n",
       Scheduler::instance().clock(), MACstats.total_num_sent_bytes);

 myUSSFMgr->dumpSFQueueLevels(outputFile);
  
}


/*************************************************************************
* Function:  void MacDocsisCM::UpdateJitter(char tbindex)
*
* Explanation:
*  This is called whenever a MAP is received by a UGS flow.
*  The statistic adds all error (scheduled allocation time
*   from actual).  Then the dumpUGSJitter method periodically
*   obtains the mean jitter =  runningJitter / #samples
*
*
* Inputs:
*
* Outputs:
*
*
*************************************************************************/
void MacDocsisCM::UpdateJitter(char tbindex)
{
  aptr tmp;
  tmp = UpFlowTable[tbindex].alloc_list;
  double tmpJitter = 0;

  if (UpFlowTable[tbindex].upstream_record.sched_type != UGS)
    return;
  
  while (tmp)
    {
      if (tmp->type == 0) /* Data Grant */
	{
	  if ((tmp->start_time < UpFlowTable[tbindex].last_granttime) || 
	      (fabs(tmp->start_time-UpFlowTable[tbindex].last_granttime) <= EPSILON*fabs(tmp->start_time)))
	    tmp = tmp->next;
	  else
	    {
	      UpFlowTable[tbindex].last_granttime = tmp->start_time;
//              tmpJitter = UpFlowTable[tbindex].nominal_alloctime - tmp->start_time;
              tmpJitter = tmp->start_time - UpFlowTable[tbindex].nominal_alloctime;
	      //So don't attempt to guess if a packet was dropped....
              if (tmpJitter < 100 * UpFlowTable[tbindex].upstream_record.ginterval) {
                UpFlowTable[tbindex].ugsjitter += tmpJitter;
	        UpFlowTable[tbindex].jitterSamples++;
	      
              if (debug_cm)
                printf("CM:UpdateJitter(%lf): updated ugsjitter:%lf (Jitter:%lf,start_time:%lf, Nominal alloc Time:%lf)\n",
	          Scheduler::instance().clock(),
                  UpFlowTable[tbindex].ugsjitter,
	          tmpJitter,tmp->start_time,
	          UpFlowTable[tbindex].nominal_alloctime);

	        UpFlowTable[tbindex].nominal_alloctime += UpFlowTable[tbindex].upstream_record.ginterval;	      
              }
              else {
              if (debug_cm)
                printf("CM:UpdateJitter(%lf): Detected a dropped UGS packet, alloc Time: %lf\n",
	          Scheduler::instance().clock(),UpFlowTable[tbindex].nominal_alloctime);

	        UpFlowTable[tbindex].nominal_alloctime = tmp->start_time + UpFlowTable[tbindex].upstream_record.ginterval;	      
              }
	      break;
	    }
	}
      else
	tmp = tmp->next;
    }
  return;
}

/*************************************************************************
 * Routine: void MacDocsisCM::USRateMeasure(char tbindex, Packet* p)
 *
 * Explanation: This routine updates the available tokens subject 
 *         to the BW rate taking into account the transmission 
 *         of this packet.
 *
 * inputs:
 *
 * outputs:
 *
**************************************************************************/
#ifdef US_RATE_CONTROL
void MacDocsisCM::USRateMeasure(char tbindex, Packet* p)
{

       us_getupdatedtokens(tbindex);
       struct hdr_cmn *ch = HDR_CMN(p);
       int pktsize_us = ch->size()<<3;
       UpFlowTable[tbindex].tokens_ = UpFlowTable[tbindex].tokens_ - pktsize_us;
       if (debug_cm)
          printf("CM(%d):USRateMeasure(%lf): Current tokens for  %lf\n",
            cm_id,Scheduler::instance().clock(),UpFlowTable[tbindex].tokens_);

       return;
}

/*************************************************************************
 * Routine: void MacDocsisCM::us_getupdatedtokens(char tbindex) {
 *
 * Explanation: This routine computes the available tokens subject
 *              to BW rate limits.
 *
 * inputs:
 *
 * outputs:
 *
***********************************************************************/
void MacDocsisCM::us_getupdatedtokens(char tbindex) {

	//Just do once- at init time
       if(UpFlowTable[tbindex].init_){

	       UpFlowTable[tbindex].tokens_ = (double) UpFlowTable[tbindex].bucket_;
	       UpFlowTable[tbindex].lastupdatetime_ = Scheduler::instance().clock();
	       UpFlowTable[tbindex].init_ = 0;
       }
   
	double now = Scheduler::instance().clock();

        UpFlowTable[tbindex].tokens_ += (now - UpFlowTable[tbindex].lastupdatetime_) * UpFlowTable[tbindex].rate_;

        if(UpFlowTable[tbindex].tokens_ > UpFlowTable[tbindex].bucket_) {

		UpFlowTable[tbindex].tokens_ = UpFlowTable[tbindex].bucket_;
	}

	UpFlowTable[tbindex].lastupdatetime_ = Scheduler::instance().clock();

	return;
}
#endif


/*****************************************************************************
 * Routine: int MacDocsis::classify(Packet* p,char dir,int* find)
 *
 * Explanation:
 *   Examines the packet and matches it to a flow in the UpflowTable
 *  or downFlowTable.
 *   The dir param determines if the upstream or downstream flow
 *   are looked at.
 *
 * Input:
 *    Packet *p : the packet
 *    char dir:   DOWNSTREAM or UPSTREAM
 *
 * Output: 
 *   returns the index of the matching cm's UpFlowTabble entry.
 *  
 * Explanation: this is different from the CMTS.  The index is returned.  The findex is
 *  not used.
 *
*****************************************************************************/
int MacDocsisCM::findFlow(int macaddr, int32_t src_ip, int32_t dst_ip, packet_t pkt_type, char dir, int*findex)
{
  char i;
  int returnIndex = 0;
  
  
  *findex = 0;

  if (debug_cm)
    printf("CM:findFlow(%lf): Entered, macaddr:%d, src_ip:%d, dst_ip:%d, pkt_type:%d, dir:%d\n",
     Scheduler::instance().clock(),macaddr,src_ip,dst_ip,pkt_type,dir);

  if (dir == DOWNSTREAM)
    {
      for (i = 0; i < SizeDownFlowTable; i++)
	{
//	  if (match(p, DownFlowTable[i].downstream_record.classifier))
      if (match(src_ip,dst_ip,pkt_type,DownFlowTable[i].downstream_record.classifier))

      {
        returnIndex = i;
        break;
      }
//	    return i;
	}
//      return default_dstream_index_; /* Default service-flow for all the traffic */
        returnIndex =  default_dstream_index_; /* Default service-flow for all the traffic */
    }
  else
    if (dir == UPSTREAM)
    {
	  for (i = 0; i < SizeUpFlowTable; i++)
	  {
        if (debug_cm)
           printf("CM:findFlow(%lf):upstream flow:  try flow table entry %d\n",Scheduler::instance().clock(),i);

        if (match(src_ip,dst_ip,pkt_type,UpFlowTable[i].upstream_record.classifier))
        {
               if (debug_cm)
                 printf("CM:findFlow(%lf): upstream: matched flow  %d\n",Scheduler::instance().clock(),i);
//	      return i;
          returnIndex = i;
          break;
	    }
        if (debug_cm)
          printf("CM:findFlow(%lf): returning default upstream index: %d \n",Scheduler::instance().clock(),default_upstream_index_);

        returnIndex =  default_upstream_index_; /* Default service-flow for all the traffic */
//	    return default_upstream_index_; /* Default service-flow for all the traffic */
      }
    }
    else
    {
      printf("CM%d :MacDocsisCM->findFlow: Error, Unknow direction, Exiting\n",cm_id);
      exit(1);
    }

  return returnIndex;
//  return 0;
}


//$A307
/*************************************************************************
*function:int MacDocsisCMTS::setupChannels(int numDChs, int numUChs)
*
*explanation: 
*  This method is called when the CM object is being configured.The method
*  setups the necessary object to support the configured number 
*  of upstream and downstream channels.
*
*inputs:
*
*outputs: Returns a 0 on success, else an error code:
*    1: general error
*
*
*************************************************************************/
int MacDocsisCM::setupChannels(void)
{
  int rc = 0;
  int i;
  char traceString[32];
  char *tptr = traceString;

    myDSpacketMonitor = new packetMonitor();
    myUSpacketMonitor = new packetMonitor();
    myDSpacketMonitor->init(cm_id,PT_SEND_DIRECTION);
    myUSpacketMonitor->init(cm_id,PT_RECV_DIRECTION);

#ifdef FILES_OK


	//TODO:  We can create separate trace files per CM....
	// Doing it this way....the entries are not in the right order
//    myDSpacketMonitor->setTextTraceFile("CMDSPACKET.txt");
//    myUSpacketMonitor->setTextTraceFile("CMUSPACKET.txt");

//    myDSpacketMonitor->setBinaryTraceFile("CMDSPACKET.bin");
//    myUSpacketMonitor->setBinaryTraceFile("CMUSPACKET.bin");


	  //Uncomment to create just the binary trace files
    myDSpacketMonitor->setTraceLevel( 0x00 | (packetMonitor_TRACE_TO_FILE) | (packetMonitor_TRACE_TO_BINARY_FILE));
    myUSpacketMonitor->setTraceLevel( 0x00 | (packetMonitor_TRACE_TO_FILE) | (packetMonitor_TRACE_TO_BINARY_FILE));
    sprintf(tptr,"CM%dDSPACKET.txt",cm_id);
    myDSpacketMonitor->setTextTraceFile(tptr);
    sprintf(tptr,"CM%dUSPACKET.txt",cm_id);
    myUSpacketMonitor->setTextTraceFile(tptr);
    sprintf(tptr,"CM%dDSPACKET.bin",cm_id);
    myDSpacketMonitor->setBinaryTraceFile(tptr);
    sprintf(tptr,"CM%dUSPACKET.bin",cm_id);
    myUSpacketMonitor->setBinaryTraceFile(tptr);
#endif




    myPhyInterface = new CMmacPhyInterface;
    myPhyInterface->setMyMac(this);
 //JJM WRONG
    myPhyInterface->init(cm_id,myMedium);
   
	numDChan = myPhyInterface->getNumberDSChannels();
	numUChan = myPhyInterface->getNumberUSChannels();
	numChannels = numUChan + numDChan;
	if (debug_cm) {
		printf("MacDocsisCM:setupChannels(%s):Setting up %d Downstream Channels, and %d Upstream Channels\n",docsis_id,numDChan,numUChan);
    }

//$A510
    if (debug_cm)
      printf("MacDocsisCM::setupChannels:  create the US SFMgr  \n");

    myUSSFMgr = new CMserviceFlowMgr();
    myUSSFMgr->setMacHandle(this);
    myUSSFMgr->init(UPSTREAM, ACK_SUPPRESSION_OFF, cm_id);
    myUSSFMgr->setMyPhyInterface(myPhyInterface);


}

 //$A306
/****************************************************************************
 * Method:  void MacDocsisCM::MacSendFrame(Packet * p, int channelNumber)
 *
 * Function:   All frames that are to be sent over an US channel will be 
 *             sent down through this method.
 *
 * Parameters:
 *    Packet *p:  A packet that already has been adjusted for all headers
 *    int channelNumber :
 *
 * *************************************************************************/
int MacDocsisCM::MacSendFrame(Packet * p, int channelNumber) 
{
  int rc = 0;
//$A510
  struct hdr_cmn* ch = HDR_CMN(p);
  serviceFlowObject *mySFObj = NULL;

#ifdef TRACE_DEBUG
  tracePoint("CM:MacSendFrame:",cm_id,0);
#endif

  mySFObj = myUSSFMgr->getServiceFlow(p);
  if (mySFObj == NULL) {
    printf("CM:MacSendFrame(%lf): HARD ERROR (cm_id:%d): Could not find this service flow:  packet of size:%d,  and ptype:%d\n",
     Scheduler::instance().clock(),cm_id,ch->size(),ch->ptype());
//    return;
    exit(1);
  }
  
  if (debug_cm) {
    printf("MacDocsisCM:MacSendFrame: Call myPhyInterface->SendFrame(packet %x, flowID:%d, channelNumber %d)\n",
            p,mySFObj->flowID,channelNumber);
  }
//$A510
//If we do this here..most frames are concatenated and so we won't track correctly
//  mySFObj->updateStats(p,channelNumber,mySFObj->getBondingGroup());


  rc = myPhyInterface->SendFrame(p,channelNumber,this);


  return rc;
}



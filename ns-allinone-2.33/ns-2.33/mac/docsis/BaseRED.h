/************************************************************************
* File:  BaseRED.h
*
* Purpose:
*   Inlude files for a base list object. A BaseRED is a container
*   of AQM Elements.
*
*
* Revisions:
*
***********************************************************************/
#ifndef BaseRED_h
#define BaseRED_h

#include "ListObj.h"

class Packet;

class BaseRED : public ListObj {

public:
  virtual ~BaseRED();
  BaseRED();
  virtual void initList(int maxAQMSize);
  virtual void setAQMParams(int maxAQMSize, int priority, int minth, int maxth, int adaptiveMode, double maxp, double weightQ);
  virtual int addElement(ListElement& element);
  virtual ListElement *removeElement();    //dequeue and return
  virtual void adaptAQMParams();
  virtual void newArrivalUpdate(Packet *p);
  virtual void updateStats();
  virtual void printStatsSummary();
  virtual double getAvgQ();
  virtual double getAvgAvgQ();
  virtual double getAvgAvgQLatency();
  virtual double getAvgAvgQLoss();
  virtual double getDropP();
  virtual double getAvgDropP();
  virtual double getAvgMaxP();

  virtual void channelIdleEvent();
  virtual void channelStatus(double accessDelayMeasure, double channelUtilizationMeasure, double consumptionMeasure);

  int maxth;
  int minth;
  double maxp;
  double weightQ;
  //define the base time constant to use for averaging
  double filterTimeConstant;
  int flowPriority;


  double freeze_time;
  double incr1;
  double dec1;
  double lastUpdateTime;

  double numberIncrements;
  double numberDecrements;
  double numberDrops;
  double numberTotalChannelIdleEvents;
  double numberValidChannelIdleEvents;

  double queueLatencyTotalDelay;
  double queueLatencySamplesCount;
  double queueLatencyMonitorTotalArrivalCount;
  double avgPacketLatency;
  double avgAvgQLatency;
  double avgAvgQLatencySampleCount;
  double lastQLatencySampleTime;

  double avgQLoss;
  double avgQLossCounter;
  double avgQLossSampleCounter;
  double lastQLossSampleTime;

  double avgAvgQLoss;
  double avgAvgQLossSampleCounter;

  double avgQ;
  double dropP;
  double q_time;
  double avgDropP;
  double avgDropPSampleCount;
  double avgAvgQ;
  double avgAvgQPSampleCount;
  double avgMaxP;
  double avgMaxPSampleCount;
protected:


private:

  int targetQueueLevel;
  double targetDelayLevel;
  double upperLatencyLimit;
  double upperLatencyThresh;
  double lowerLatencyThresh;
  double alpha;
  double beta;
  
  int count;
  int gentleMode;
  //0 is no adaptation, 1 is Floyd's Adaptive RED, 2 is BaseRED
  int adaptiveMode;
  double avgTxTime;
  double accessDelayMeasure;
  double channelUtilizationMeasure;
   double consumptionMeasure;


  //states:  0 : NOT initialized
  //states:  1 : REGION 1
  //states:  2 : REGION 2
  //states:  3 : REGION 3
  int stateFlag;
  double updateFrequency;
  int bytesQueued;
  double avgServiceRate;
  double byteArrivals;
  double byteDepartures;
  double lastRateSampleTime;
  double lastMonitorSampleTime;
  double rateWeight;
  double avgArrivalRate;

  double BaseREDupdateFrequency;
  int    BaseREDUpdateCountThreshold;
  int    BaseREDUpdateCount;
  double BaseREDLastSampleTime;
  double BaseREDByteArrivals;
  double BaseREDByteDepartures;
  double avgBaseREDArrivalRate;
  double avgBaseREDServiceRate;
  double BaseREDRateWeight;

  double BaseREDQueueDelaySamples;
  double BaseREDQueueLevelSamples;
  double BaseREDAccessDelaySamples;
  double BaseREDConsumptionSamples;
  double BaseREDChannelUtilizationSamples;


  double updateAvgQLoss();
  double updateAvgQ();
  void updateRates();
  double updateAvgQLatency();
  void  updateMonitors();
  double computeDropP();
  double computeDropPDelayBased();
  int packetDrop(double dropP);
 

};


#endif

/************************************************************************
* File:  BLUEQ.h
*
* Purpose:
*   Inlude files for a base list object. A BLUEQ is a container
*   of AQM Elements.
*
*
* Revisions:
*
***********************************************************************/
#ifndef BLUEQ_h
#define BLUEQ_h

#include "ListObj.h"

class BLUEQ : public ListObj {

public:
  virtual ~BLUEQ();
  BLUEQ();
  virtual void initList(int maxAQMSize);
  virtual void setAQMParams(int maxAQMSize, int priority, int minth, int maxth, int adaptiveMode, double maxp, double weightQ);
  virtual int addElement(ListElement& element);
  virtual ListElement *removeElement();    //dequeue and return
  virtual void adaptAQMParams();
  void updateStats();
  void printStatsSummary();
  double getAvgQ();
  double getAvgAvgQ();
  double getDropP();
  double getAvgDropP();

  void channelIdleEvent();

  int flowPriority;
  int maxth;
  int minth;
  int queueThreshold;
  double maxp;
  double weightQ;

  double avgQ;
  double dropP;
  double q_time;

  double freeze_time;
  double incr1;
  double dec1;
  double lastUpdateTime;

  double avgDropP;
  double avgDropPSampleCount;
  double avgAvgQ;
  double avgAvgQPSampleCount;

  double numberIncrements;
  double numberDecrements;
  double numberDrops;
  double numberTotalChannelIdleEvents;
  double numberValidChannelIdleEvents;

protected:


private:

  int gentleMode;
  int adaptive1Mode;
  double avgTxTime;

  double updateAvgQ();
  int packetDrop(double dropP);

};


#endif

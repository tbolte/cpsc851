/***************************************************************************
 *	This file contains methods for the base MacDocsis class
 *
 * Revisions:
 *  $A302:  changed the receive process so rxed packets are stored
 *          in the RxPktTimer object and NOT in the channel Pkt Queue.
 *
 *  $A307 :  changes to support MacPhyInterface at the CM
 *  $A400:  v.93 changes
 *
 **************************************************************************/

#include "globalDefines.h"
#include "mac-docsis.h"
#include <stdio.h>
#include "ping.h"
#include "loss_monitor.h"
#include "arp.h"
#include "rtp.h"

#include "macPhyInterface.h"
#include "packetMonitor.h"
#include "channelProperties.h"

#include "docsisDebug.h"
//#define TRACE 1


int docsis_dsehdr::offset_;
int docsis_chabstr::offset_;
/*=============================Timer handlers=======================*/
/*****************************************************************************
*****************************************************************************/
static class MacDocsisDSEHDR : public PacketHeaderClass 
{
public:
  MacDocsisDSEHDR() : PacketHeaderClass("PacketHeader/DocsisCMTSESH", sizeof(docsis_dsehdr)) 
  {
    bind_offset(&docsis_dsehdr::offset_);
#ifdef TRACE
    printf("Constructor:MacDocsisDSEHDR: size allocated:%d\n",  sizeof(docsis_dsehdr));
#endif 
  }
} class_hdr_docsis_dsehdr;


static class MacDocsisCHABSTR : public PacketHeaderClass 
{
public:
  MacDocsisCHABSTR() : PacketHeaderClass("PacketHeader/DocsisCHABSTR", sizeof(docsis_chabstr)) 
  {
    bind_offset(&docsis_chabstr::offset_);
#ifdef TRACE
     printf("Constructor:MacDocsisCHABSTR: size allocated:%d\n", sizeof(docsis_chabstr));
#endif
  }
} class_hdr_docsis_chabstr;

/*************************************************************************
Constructor function
*************************************************************************/
//MacDocsis::MacDocsis() : Mac(), mhTxPkt_(this), mhRxPkt_(this) 
MacDocsis::MacDocsis() : Mac()
{
  MACstats.last_rtime = 0.0;
  
  MACstats.avg_mgmtpkts = 0;
  MACstats.num_mgmtpkts = 0;
  MACstats.last_mmgmttime = 0;
  
  myDSpacketMonitor = NULL;
  myUSpacketMonitor = NULL;

  defaultDSChannel = 0;
  defaultUSChannel = 0;
  defaultUSOverhead_bytes = 0;

  for (int i = 0; i < 10; i++)
    docsis_id[i]=0;


#ifdef TRACE
  printf("MacDocsis: constructor mac addr :%d \n", index_);
#endif

//$A302
//   myRxPktQ.init();
}


/*************************************************************************

*************************************************************************/
int MacDocsis::command(int argc, const char*const* argv)
{
  if (argc == 3) 
  {
		Tcl& tcl = Tcl::instance();
	    TclObject *obj;

	    if( (obj = TclObject::lookup(argv[2])) == 0) {
	        fprintf(stderr, "%s lookup failed\n", argv[1]);
	        return TCL_ERROR;
	    }
	    if (strcmp(argv[1], "medium") == 0) {
	        myMedium = (medium*) obj;
	        if (myMedium == 0) {
			    printf("setting medium lookup failed\n");
				tcl.resultf("no such object %s", argv[2]);
				return (TCL_ERROR);
			}
		return (TCL_OK);
		}
  }
  return Mac::command(argc,argv);
}


/*****************************************************************************
 * Routine: void MacDocsis::configure_upstream()
 *
 * Explanation:
 *   This is called to setup the upstream channel.  
 *
 * Input:
 *
 * Output: 
 *
*****************************************************************************/
void MacDocsis::configure_upstream(int defaultDSChannelP, int defaultUSChannelP)
{
  double n;
  int frsize;
  struct channelPropertiesType  channelProperties;

  defaultDSChannel = defaultDSChannelP;
  defaultUSChannel = defaultUSChannelP;
#ifdef TRACE //---------------------------------------------------------
  printf("mac-docsisbase::configure_upstream(id:%d):  default DS: %d,  default US: %d \n",
        docsis_id,defaultDSChannel, defaultUSChannel);
  printf("mac-docsisbase::configure_upstream:  myPhyInterface: %x  \n",myPhyInterface);
#endif

  myPhyInterface->getChannelProperties(&channelProperties,defaultUSChannel);

  defaultUSOverhead_bytes =  channelProperties.overheadBytes;
  
  n = channelProperties.ticks_p_minislot * 6.25; /*Number of micro-sec per mini-slot*/
  if (n==0) {
    printf("mac-docsisbase::configure_upstream: CONFIG HARD ERROR ticks_p_minislot= %d  \n", 
       channelProperties.ticks_p_minislot);
    exit(1);
  }
  size_mslots = n / ((double)(10*10*10*10*10*10));
  minislots_psec = (u_int32_t)((10*10*10*10*10*10)/n);
  

  bytes_pminislot = (u_int16_t)((channelProperties.channelCapacity / 8)  * size_mslots);
  //JJM WRONG
  frsize = 6 + channelProperties.overheadBytes;
  size_ureqgrant = (frsize) / bytes_pminislot;
  
  if (( frsize % bytes_pminislot) != 0)
    size_ureqgrant++;

#ifdef TRACE //---------------------------------------------------------
  printf("mac-docsisbase::configure_upstream:  default DS: %d,  default US: %d \n",defaultDSChannel, defaultUSChannel);
  printf("mac-docsisbase::configure_upstream:  Slot details for All Channels: Using ideal Channel \n");
  printf("size_mslots = %lf\n",size_mslots);
  printf("minislots_psec = %d\n",minislots_psec);
  printf("bytes_pminislot = %d (upchannel data rate:%d, size_mslots:%f\n\n",
	 bytes_pminislot,channelProperties.channelCapacity,size_mslots);
  printf("size_reqgrant %d\n",size_ureqgrant);
#endif //---------------------------------------------------------------

}

/****************************************************************************
 * Function: void MacDocsis::recv(Packet* p, Handler* h) 
 *
 * Function:   
 *   This method is called by the phy object as it passes up a frame
 *   It is invoked after the prop delay and prepares the node for the
 *   receive process.
 *
 * Parameters:
 *    Packet *p:  
 *
 * Design Notes:
 *
 *
************************************************************************************/
void MacDocsis::recv(Packet* p, Handler* h) 
{
  struct hdr_cmn *ch = HDR_CMN(p);
  hdr_tcp *tcph = hdr_tcp::access(p);
  hdr_rtp* rh = hdr_rtp::access(p);

  /* 
     Incoming packets from phy layer, send UP to ll layer. 
     Now, it is in receiving mode. 
  */  
  if (ch->direction() == hdr_cmn::UP) 
    {
    
#ifdef TRACE //---------------------------------------------------------
      printf("MacDocsis:recv[%s](%lf) incoming packet\n",docsis_id, Scheduler::instance().clock());
      if(ch->ptype_ == PT_TCP)
        printf(" ....TCP Sequence Num %d Ack No   %d\n",tcph->seqno(), tcph->ackno());
      if(ch->ptype_ == PT_UDP)
        printf(".....UDP Sequence Num %d \n", rh->seqno());
#endif //---------------------------------------------------------------
      
      sendUp(p);

      return;
    }
  
  /* 
     Packets coming down from ll layer (from ifq actually),
     send them to phy layer. 
     Now, it is in transmitting mode. 
  */
  
#ifdef TRACE //---------------------------------------------------------
  printf("MacDocsis::recv[:%s](%lf):  sending packet down to PHY : %d \n",docsis_id, 
	 Scheduler::instance().clock());
#endif //---------------------------------------------------------------
  

 //$A400
 if(strcmp(docsis_id,"CMTS") == 0)
 {
   if (myDSpacketMonitor != NULL)
   {
      myDSpacketMonitor->updateStats(p);
      myDSpacketMonitor->packetTrace(p);
   }
 }
 else
 {
   if (myUSpacketMonitor != NULL)
   {
     myUSpacketMonitor->updateStats(p);
     myUSpacketMonitor->packetTrace(p);
   }
 }


  callback_ = h;
  sendDown(p);
  
  return;
}

/****************************************************************************
 * Function: void MacDocsis::recvHandler(Event *e) 
 *
 * Function:   
 *   This method is called when the nodes receive process completes
 *   and the last bit of the frame has been received.  The frame
 *   is ready to be sent up to the higher layers of the  MAC.
 *
 *   The node reaches down to its PHY and pulls the pkt that arrived.
 *
 * Parameters:
 *
 * Design Notes:
 *
 *  $A307:  eventually this method will be removed.  For now it just calls
 *    the CMs machPhyInterface to pass the frame up.
 *
************************************************************************************/
//$A302
void MacDocsis::recvHandler(Event *e, Packet *p) 
{
//NOOP Now
}


/****************************************************************************
 * Method:  void MacDocsis::sendHandler(Event *e) 
 *
 * Function:   
 *
 * Parameters:
 *    Event *e
 *
 * Design Notes:
 *
 *
*************************************************************************/
void MacDocsis::sendHandler(Event *e) 
{
  
#ifdef TRACE //---------------------------------------------------------
  printf("%s: leaving sendHandler\n", docsis_id);
#endif //---------------------------------------------------------------
}
	


/*************************************************************************
This returns a 0 if this frame contains data

else it returns a 1 in which case it contains 
a MAC specific header (i.e., request)
*************************************************************************/
char MacDocsis::ClassifyDataMgmt(Packet* p)
{
  struct hdr_docsis *ds = HDR_DOCSIS(p);
  
  if (ds->dshdr_.fc_type == DATA)
    {
      return 0;
    }
  else if (ds->dshdr_.fc_type == MAC_SPECIFIC)
    {
      return 1;
    }
#ifdef TRACE //---------------------------------------------------------
  printf("ClassifyDataMgt(%lf): ERROR: its neither DATA nor MAC_SPECIFIC\n", Scheduler::instance().clock());
#endif //---------------------------------------------------------------
  return 1;
}

/*************************************************************************

*************************************************************************/
int MacDocsis::match(Packet* p, struct flow_classifier cl)
{
  struct hdr_cmn *ch = HDR_CMN(p);
  struct hdr_ip *chip = HDR_IP(p);

#ifdef TRACE 
  printf("%s:match:(%lf): Packet type = %d src-ip = %d dst-ip = %d(PT_UDP:%d, PT_CBR:%d)\n",docsis_id,
	  Scheduler::instance().clock(),
         ch->ptype(),chip->saddr(),chip->daddr(),PT_UDP,PT_CBR);
#endif 
  
  //First make sure it's one of these packet types...
  if ((ch->ptype() == PT_UDP) || 
      (ch->ptype() == PT_TCP) || 
      (ch->ptype() == PT_ACK) || 
      (ch->ptype() == PT_HTTP) || 
      (ch->ptype() == PT_PING) || 
      (ch->ptype() == PT_RTP) || 
      (ch->ptype() == PT_CBR)  ||
      (ch->ptype() == PT_LOSS_MON)) 
    {
#ifdef TRACE 
      printf("%s:match: Try to match pkt: (src,dst,type):%d,%d,%d) with flow %d,%d,%d\n",docsis_id,
              chip->saddr(), chip->daddr(), ch->ptype(), cl.src_ip,cl.dst_ip,cl.pkt_type);
#endif     
      if ((ch->ptype() == cl.pkt_type) && 
	  (chip->saddr() == cl.src_ip) && 
      (chip->daddr() == cl.dst_ip))
      {
	    return 1;
      }
      else { 
        return 0;      
      }
    }


  return 0;
}

//$A306
/*************************************************************************

*************************************************************************/
int MacDocsis::match(int32_t src_ip, int32_t dst_ip, packet_t ptype, struct flow_classifier cl)
{

#ifdef TRACE 
  printf("%s:match:(%lf): Packet type = %d src-ip = %d dst-ip = %d\n",docsis_id,
	  Scheduler::instance().clock(), ptype,src_ip,dst_ip);
#endif 
  
  //First make sure it's one of these packet types...
  if ((ptype == PT_UDP) || 
      (ptype == PT_TCP) || 
      (ptype == PT_ACK) || 
      (ptype == PT_HTTP) || 
      (ptype == PT_PING) || 
      (ptype == PT_RTP) || 
      (ptype == PT_CBR)  ||
      (ptype == PT_LOSS_MON)) 
    {
#ifdef TRACE 
      printf("%s:match: Try to match pkt: (src,dst,type):%d,%d,%d) with flow %d,%d,%d\n",docsis_id,
              src_ip,dst_ip,ptype, cl.src_ip,cl.dst_ip,cl.pkt_type);
#endif     
//JJM WRONG 7-14-2009
//      if ((ptype == cl.pkt_type) && 
//	  (src_ip == cl.src_ip) && 
//	  (dst_ip == cl.dst_ip))
      if ((ptype == cl.pkt_type) && 
	  (src_ip == cl.src_ip))
	  {
	    return 1;
	  }
        else 
          return 0;      
    }
  return 0;
}

/*************************************************************************

*************************************************************************/
int MacDocsis::bit_on(u_char flag, int pos)
{  
  switch(pos) 
    {
    case FRAG_ENABLE_BIT:
      if (flag & 128)
	return 1;
      else 
	return 0;
      break;
	
    case CONCAT_ENABLE_BIT:
      if (flag & 64)
	return 1;
      else 
	return 0;
      break;
    
    case FRAG_ON_BIT:
      if (flag & 32)
	return 1;
      else 
	return 0;
      break;
    
    case CONCAT_ON_BIT:
      if (flag & 16)
	return 1;
      else 
	return 0;
      break;
    
    case PIGGY_ENABLE_BIT:
      if (flag & 8)
	return 1;
      else 
	return 0;
      break;
    
    case NO_PIGGY_BIT:
      if (flag & 4)
	return 1;
      else 
	return 0;
      break;

    case PIGGY_NOT_SEND:
      if (flag & 2)
	return 1;
      else 
	return 0;
      break;
    }
}

/*************************************************************************

*************************************************************************/
void MacDocsis::set_bit(u_char * flag, int pos, int op)
{
  switch (pos) 
    {
    case FRAG_ENABLE_BIT:
      if (op == ON)	
	*flag |= 128;
      else	
	*flag &= 127;      
      break;
      
    case CONCAT_ENABLE_BIT:
      if (op == ON)	
	*flag |= 64;
      else	
	*flag &= 191;    
      break;
      
    case FRAG_ON_BIT:
      if (op == ON)	
	*flag |= 32;  
      else	
	*flag &= 223;    
      break;
      
    case CONCAT_ON_BIT:
      if (op == ON)	
	*flag |= 16;   
      else	
	*flag &= 239;    
      break;
      
    case PIGGY_ENABLE_BIT:
      if (op == ON)	
	*flag |= 8;
      else	
	*flag &= 247;    
      break;
      
    case NO_PIGGY_BIT:
      if (op == ON)	
	*flag |= 4;
      else	
	*flag &= 251;    
      break;
      
    case PIGGY_NOT_SEND:
      if (op == ON)	
	*flag |= 2;    
      else	
	*flag &= 253;    
      break;
    }
}

/*************************************************************************

*************************************************************************/
Packet* MacDocsis::AllocPkt(int n)
{
  Packet* q = Packet::alloc();
    
  if (n > 0)
    q->allocdata(n);
  
  return q;
}


/************************************************************************

*************************************************************************/
void MacDocsis::dump_pkt(Packet* p)
{
  struct hdr_cmn *ch = HDR_CMN(p);
  struct hdr_ip *chip = HDR_IP(p);
  
  printf("%s:Packet header:\n", docsis_id);
  printf("Src-ip = %d Dst-ip = %d Pkt-type = %d Pkt-size = %d\n", 
	 chip->saddr(),chip->daddr(),ch->ptype(),ch->size());
}

      
/*************************************************************************
Calculate bs raised to in
*************************************************************************/
u_int32_t MacDocsis::power(u_int32_t bs, u_int32_t in)
{
  u_int32_t product = 1;
  
  for (int i = 0; i < in; i++)
    product *= bs;
  
  return product;
}


/*************************************************************************
Show channel Status for Debugging
*************************************************************************/
void MacDocsis::dumpChannelStatus()
{
	int i;
	
	for(i=0; i<numChannels; i++)
//		printf("%s:channel(%d -- %d): rxstate_=%d txstate_=%d\n",docsis_id, i, cmts_arr[0]->channels[i].ch_direction,cmts_arr[0]->channels[i].rx_state_,cmts_arr[0]->channels[i].tx_state_);
//		printf("%s:channel(%d -- %d): rxstate_=%d txstate_=%d\n",docsis_id, i, cmts_arr[0]->channels[i].ch_direction,cmts_arr[0]->channels[i].getRxState(),cmts_arr[0]->channels[i].getTxState());

	return;
}


/*****************************************************************************
 * Routine: int MacDocsis::classify(Packet* p,char dir,int* find)
 *
 * Explanation:
 *   Examines the packet and matches it to a flow in the CmRecord.
 *   The dir param determines if the upstream or downstream flow
 *   are looked at.
 *
 * Input:
 *    Packet *p : the packet
 *    char dir:   DOWNSTREAM or UPSTREAM
 *    int *find:  This ptr variable is set with the index of the flow
 *                 table entry.
 *
 * Output: 
 *   returns the index of the matching cm's CmRecord table entry.
 *
*****************************************************************************/
int MacDocsis::findFlow(int macaddr, int32_t src_ip, int32_t dst_ip, packet_t pkt_type, char dir, int*find)
{

  return 0;
}

void MacDocsis::setBGMgr(bondingGroupMgr * BGMgrPARAM)
{
  myBGMgr = BGMgrPARAM;
}


//$A510
void MacDocsis::tracePoint(char *s1, int param1, int param2)
{
  FILE* fp = NULL;
  double curTime= Scheduler::instance().clock();
  char traceString[256];
  char *tptr = traceString;

  sprintf(tptr,"%lf %s %d %d ",curTime,s1,param1,param2);
//  printf("MacDocsis:tracePoint:(%lf)  %s ",curTime,s1);
  fp = fopen("tracePoint.out", "a+");
  fprintf(fp,"%s\n",tptr);
  fflush(stdout);
  fclose(fp);

}


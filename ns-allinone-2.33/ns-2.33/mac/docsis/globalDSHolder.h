/***************************************************************************
 * Module: medium
 *
 * Explanation:
 *   This file contains the abstraction for the physical medium. 
 *
 * Revisions:
 *
 *  TODO:
************************************************************************/

#include "docsisDebug.h"
#include "medium.h"
#include "serviceFlowMgr.h"
#include "configMgr.h"
#include "bondingGroupMgr.h"

#ifndef ns_global_ds_h
#define ns_global_ds_h

/*************************************************************************
 ************************************************************************/
//class medium : NsObject
class globalDSHolder {

	public:
	void setmyMedium(medium *);
    void setmyMac(MacDocsisCMTS *);
	void setmyBGMgr(bondingGroupMgr *);
	void setmyDSSFMgr(serviceFlowMgr *);
	void setmyUSSFMgr(serviceFlowMgr *);
	void setmyCMconfigMgr(CMconfigMgr *);
	void setmyCMTSconfigMgr(CMTSconfigMgr *);
	void setmyCMTSmacPhyInterface(CMTSmacPhyInterface *);
	void setmyCMmacPhyInterface(CMmacPhyInterface *);
	void setmyCMTSreseqMgr(reseqMgr *);
	void setmyCMreseqMgr(reseqMgr *);

	medium *getmyMedium(void);
    MacDocsisCMTS *getmyMac(void);
	bondingGroupMgr *getmyBGMgr(void);
	serviceFlowMgr *getmyDSSFMgr(void);
	serviceFlowMgr *getmyUSSFMgr(void);
	CMconfigMgr *getmyCMconfigMgr(void);
	CMTSconfigMgr *getmyCMTSconfigMgr(void);
	CMTSmacPhyInterface *getmyCMTSmacPhyInterface(void);
	CMmacPhyInterface *getmyCMmacPhyInterface(void);
	reseqMgr *getmyCMTSreseqMgr(void);
	reseqMgr *getmyCMreseqMgr(void);

	private:
	medium *myMedium;
    MacDocsisCMTS *myMac;
	bondingGroupMgr *myBGMgr;
	serviceFlowMgr *myDSSFMgr;
	serviceFlowMgr *myUSSFMgr;
	CMconfigMgr *myCMconfigMgr;
	CMTSconfigMgr *myCMTSconfigMgr;
	CMTSmacPhyInterface *myCMTSmacPhyInterface;
	CMmacPhyInterface *myCMmacPhyInterface;
	reseqMgr *myCMTSreseqMgr;
	reseqMgr *myCMreseqMgr;
};

#endif /* __ns_global_ds_h__ */

/***************************************************************************
* Module: bondingGroupMgr
*
* Explanation:
*   This file contains the class definition for the bonding Group Manager Object.
*
* Revisions:
*
*  TODO:
*
************************************************************************/
#include "bondingGroupMgr.h"

#include "bondingGroupObject.h"
#include "aggregateSFObject.h"

#include "docsisDebug.h"
//#define TRACEME 0
//#define TRACEME1 0

static class bondingGroupClass: public TclClass {
	public:
		bondingGroupClass() : TclClass("BondingGroup") {}
		TclObject* create(int, const char*const*) {
			return (new bondingGroupMgr);
		}
} class_bondingGroup;

bondingGroupMgr::bondingGroupMgr() : BiConnector()
{
#ifdef TRACEME //---------------------------------------------------------
	printf("Constructed BGMGR\n");
#endif //---------------------------------------------------------------

  hierarchicalMode = FALSE;
}

bondingGroupMgr::~bondingGroupMgr() 
{
#ifdef TRACEME //---------------------------------------------------------
	printf("Destructed BGMGR\n");
#endif //---------------------------------------------------------------

}

void bondingGroupMgr::init(int numBGs)
{
	int i;

	numberBGs = numBGs;
	myBG = new bondingGroupObject[numberBGs];

//#ifdef TRACEME1 //---------------------------------------------------------
    printf("bondingGroupMgr::init(%lf): small init: Num BGs to create:  %d \n", Scheduler::instance().clock(),numBGs);
//#endif //---------------------------------------------------------------

	for(i=0;i<numberBGs;i++)
	{
	  myBG[i].init(i,myMedium,this);
    }
}

void bondingGroupMgr::init(int serviceDiscipline,int QSize, int QType)

{
	int i;

    if ((serviceDiscipline == HDRR) || (serviceDiscipline == HOSTBASED) || (serviceDiscipline == TSNTS) || (serviceDiscipline == FAIRSHARE ))
        hierarchicalMode = TRUE;
    else
        hierarchicalMode = FALSE;
   
//#ifdef TRACEME1 //---------------------------------------------------------
    printf("bondingGroupMgr::init(%lf): Big init:  Num BGs to create:  %d \n", Scheduler::instance().clock(),numberBGs);
//#endif //---------------------------------------------------------------

    int SchedQSize = QSize;
    int SchedQType = QType;
    int queueServiceDiscipline = serviceDiscipline;

    double maxRate =  BG_DEFAULT_RATE;
    int numberAggregateQueues = 1;

    switch (queueServiceDiscipline)
    {

      case HDRR:
         numberAggregateQueues = 1;
         break;

      case HOSTBASED:
         numberAggregateQueues = 8;
         break;

      case TSNTS:
         numberAggregateQueues = 2;
         break;

      case FAIRSHARE:
         numberAggregateQueues = 2;
         break;

      default:
         numberAggregateQueues = 1;
         break;
   }

	for(i=0;i<numberBGs;i++)
	{
	  myBG[i].configureBG(hierarchicalMode, queueServiceDiscipline, SchedQSize, SchedQType, numberAggregateQueues, maxRate);
	}
}


/***********************************************************************
*function: int bondingGroupMgr::optimize(int opCode)
*
*explanation:
*  This is called periodically to optimize the system.  
*
*inputs:
*  int opCode : indicates a specific type of optimization that is desired
*      Refer to globalDefines.h, but some possible values are:
*         #define OPTIMiZER_HEARTBEAT  0
*         #define ADJUST_PACKET_SCHEDULER 1
*         #define ADJUST_BANDWIDTH_MANAGEMENT 2
*         #define LOAD_BALANCE 3
*
*outputs:
*  returns a SUCCESS or FAILURE
*
************************************************************************/
int bondingGroupMgr::optimize(int opCode)
{
  int rc = SUCCESS;
	int i;

#ifdef TRACEME1 //---------------------------------------------------------
    printf("bondingGroupMgr::optimize(%lf):  %d, optimize %d BG Objects \n", Scheduler::instance().clock(),opCode,numberBGs);
#endif //---------------------------------------------------------------

	for(i=0;i<numberBGs;i++)
	{
	  myBG[i].optimize(opCode);
    }

  return rc;
}


/*****************************************************************************
* routine: void bondingGroupMgr::configureBGs(int serviceDiscipline,int QSize, int QType, int numberQueues)
*
* function: 
*   This method is called to dynamically modifiy the BG config. 
* 
*   THIS IS TODO....CURRENTLY NOT USED
*
*Inputs:
*
*Outputs:
*
*****************************************************************************/
void bondingGroupMgr::configureBGs(int serviceDiscipline,int QSize, int QType, int numberQueues)
{
	int i;

#ifdef TRACEME1 //---------------------------------------------------------
    printf("bondingGroupMgr::configureBGs(%lf): THIS METHOD IS NOT USED  \n", 
          Scheduler::instance().clock());
#endif //---------------------------------------------------------------

//TODO:  We need to invoke a BGobject method called updateOperation  or something
// This would put the updated config into effect - could be done dynamically.
// We can access a file here that maps SFs to Aggregate Flows. 
// We would have to update the SFObject - put it into hierarchicalMode
// and we would have to have the BGObject classifier set as a result of this
#if 0
	for(i=0;i<numberBGs;i++)
	{
      myBG[i].SchedQSize = QSize;;
      myBG[i].SchedQType = QType;
      myBG[i].queueServiceDiscipline = serviceDiscipline;
      myBG[i].numberAggregateQueues =  numberQueues;
	}
#endif

}


void bondingGroupMgr::setMyPhyInterface(CMTSmacPhyInterface *myPhyInterfaceParam)
{
  int i;

  myPhyInterface = myPhyInterfaceParam;
#ifdef TRACEME //---------------------------------------------------------
  printf("bondingGroupMgr::setMyPhyInterface(%lf) for %d BG objects, set this ptr: %x \n", 
		   Scheduler::instance().clock(),numberBGs, myPhyInterfaceParam);
#endif //---------------------------------------------------------------
  for(i=0;i<numberBGs;i++)
  {
    myBG[i].setMyPhyInterface(myPhyInterface);
  }

}

int bondingGroupMgr::addChannelToBG(int BGIDX, int ChIDX)
{
  int rc;
  rc = myBG[BGIDX].addChannelToBG(ChIDX);
  return rc;
}

int bondingGroupMgr::removeChannelFromBG(int BGIDX, int ChIDX)
{
  int rc;

  rc =myBG[BGIDX].removeChannelFromBG(ChIDX);
  return rc;
}

int bondingGroupMgr::checkChannelInBG(int BGIDX, int ChIDX)
{
	return myBG[BGIDX].checkChannelInBG(ChIDX);
}

/*****************************************************************************
* routine: int bondingGroupMgr::findSetofFlows(int channelNumber,struct serviceFlowSet *mySet,int maxSetSize)
*
* function: 
*   This method is called to find all service flows that
*   are mapped to the channelNumber.  The SFs might not have packets waiting however.
*   The method appends to the mySet structure the set of SF Object pointers.
*   The number of members in the set is returned.  
*
*Inputs:
*  int channelNumber: The channel of interest.
*  struct serviceFlowSet *mySet : the structure that will hold the set of SF pointers
*     This routine does not assume that the list is empty.  
*
*Outputs:
* Returns the updated number of members in the set.  
*
*****************************************************************************/
int bondingGroupMgr::findSetofFlows(int channelNumber,struct serviceFlowSet *mySet)
{
  int BGIndex = 0;
  int rc;
  int numberInSet = mySet->numberMembers;
  int maxSetSize = mySet->maxSetSize;
  int i;


#ifdef TRACEME //---------------------------------------------------------
    printf("bondingGroupMgr::findSetofFlows(%lf): #BGs:%d, channelNumber:%d:  (current set size:%d)\n", Scheduler::instance().clock(),numberBGs, channelNumber, mySet->numberMembers);
#endif //---------------------------------------------------------------

  //find all SFs in all BGs with this channel, 
  for (BGIndex=0;BGIndex<numberBGs;BGIndex++) {
    rc = myBG[BGIndex].checkChannelInBG(channelNumber);
	if (rc == TRUE) {  
      //Step into each object 
//      numberInSet = myBG[BGIndex].getSFSet(mySet);
      numberInSet = myBG[BGIndex].getCompleteSFSet(mySet);

#ifdef TRACEME //---------------------------------------------------------
      printf("bondingGroupMgr::findSetofFlows(%lf): BG index: %d:  For channel %d we found  numberInSet:%d, Set size:%d \n", 
			  Scheduler::instance().clock(),BGIndex,channelNumber, numberInSet,mySet->numberMembers);
#endif //---------------------------------------------------------------

	}
  }

  return numberInSet;
}

/*****************************************************************************
* routine: int bondingGroupMgr::findSetofChannels(int BGID,struct channelSet *myChannelSet)
*
* function: 
*   This method is called to find all service flows that
*   1)are mapped to the channelNumber; and 2)have a packet ready to transmit
*   The method appends to the mySet structure the set of SF Object pointers.
*   The number of members in the set is returned.  
*
*Inputs:
*  int channelNumber: The channel of interest.
*  struct serviceFlowSet *mySet : the structure that will hold the set of SF pointers
*     This routine does not assume that the list is empty.  
*
*Outputs:
* Returns the updated number of members in the set.  
*
*****************************************************************************/
int bondingGroupMgr::findSetofChannels(int BGIndex,struct channelSet *myChannelSet)
{
  int rc;
  int numberInSet = myChannelSet->numberMembers;
  int maxSetSize = myChannelSet->maxSetSize;


#ifdef TRACEME 
    printf("bondingGroupMgr::findSetofChannels(%lf): #BGs:%d, BGID:%d:  (current set size:%d, max:%d)\n", 
			Scheduler::instance().clock(),numberBGs, BGIndex, myChannelSet->numberMembers,maxSetSize);
#endif 
	//Step into each object 
    numberInSet = myBG[BGIndex].getChannelSet(myChannelSet);

#ifdef TRACEME 
    printf("bondingGroupMgr::findSetofChannels(%lf): numberInSet:%d, Set size:%d \n", 
			  Scheduler::instance().clock(),numberInSet,myChannelSet->numberMembers);
#endif 

  return numberInSet;
}

/*****************************************************************************
* routine: int bondingGroupMgr::findSetofBGs(int channelNumber, struct bondingGroupSet *)
*
* function: 
*   This method is called to find the set of BGs that are allowed to use
*   this channel.  Note that there might not be a SF that is eligible to send...
*
*Inputs:
*  int channelNumber: The channel of interest.
*  struct bondingGrouSet *mySet : the structure that will hold the set of SF pointers
*     This routine does not assume that the list is empty.  
*
*Outputs:
* Returns the updated number of members in the set.  
*
*****************************************************************************/
int bondingGroupMgr::findSetofBGs(int channelNumber, struct bondingGroupSet * mySet)
{
  int rc = SUCCESS;
  int numberInSet = mySet->numberMembers;
  int maxSetSize = mySet->maxSetSize;
  int SetIndex=0;
  int i;


#ifdef TRACEME 
    printf("bondingGroupMgr::findSetofBGs(%lf): #BGs:%d,  (current set size:%d, max:%d)\n", 
			Scheduler::instance().clock(),numberBGs, mySet->numberMembers,maxSetSize);
#endif 

  //find all SFs in all BGs with this channel, 
  for (i=0;i<numberBGs;i++) {
    rc = myBG[i].checkChannelInBG(channelNumber);
	if (rc == TRUE) {  
      mySet->numberMembers++;
      mySet->BGObjectPtrs[SetIndex++] = &myBG[i];
//      mySet->BGObjectPtrs[BGIndex] = (bondingGroupObject *)&myBG[BGIndex];

#ifdef TRACEME //---------------------------------------------------------
      printf("bondingGroupMgr::findSetofBGs(%lf): BG index: %d (numberAggQueues:%d)  For channel %d we found  numberInSet:%d, Updated Set size:%d, Ptr Entry:%d \n", 
			  Scheduler::instance().clock(),myBG[i].myBGID,myBG[i].numberAggregateQueues,channelNumber, numberInSet,mySet->numberMembers,mySet->BGObjectPtrs[SetIndex]);
#endif //---------------------------------------------------------------

	}
  }

  numberInSet =  mySet->numberMembers;
  return numberInSet;
}

/*************************************************************
* function:
*  void bondingGroupMgr::addSFToBG(int BGIndex, serviceFlowObject *mySFObjParam)                
*
* explanation:  This method adds the SF to the
*    BG object's  SF set.
*
* inputs: 
*    serviceFlowObject *mySFObjParam: ptr to the SF object to be added
*
* outputs: 
*   Returns a SUCCESS or FAILURE
*
***************************************************************/
int bondingGroupMgr::addSFToBG(int BGIndex, serviceFlowObject *mySFObjParam)                
{   
  int rc;

#ifdef TRACEME
  printf("bondingGroupMgr::addSFToBG(%lf): addSF flowID %d to BGIndex: %d (BG Obj BGID:%d, hierarchicalMode:%d) \n",
          Scheduler::instance().clock(), mySFObjParam->flowID,BGIndex,myBG[BGIndex].myBGID,hierarchicalMode);
#endif

  rc = myBG[BGIndex].addSFToBG(mySFObjParam);


#ifdef TRACEME
  printf("bondingGroupMgr::addSFToBG: Updated BG %d with the SF Object, rc=%d \n",BGIndex,rc);
#endif

  return rc;
}                                                                             

/*************************************************************
* function: int bondingGroupMgr::pickChannel(serviceFlowObject *mySF);
*
* explanation:  This method selects a channel on behalf of the SF 
*
* inputs: 
*    serviceFlowObject *mySFObjParam: ptr to the SF object to be added
*
* outputs: 
*   Returns a SUCCESS or FAILURE
*
***************************************************************/
int bondingGroupMgr::pickChannel(serviceFlowObject *mySF)
{   
  int rc;
  int BGIndex = mySF->myBGID;

  rc = myBG[BGIndex].findFirstAvailableChannel();

#ifdef TRACEME
  printf("bondingGroupMgr::pickChannel: for SF flowID:%d, from BG:%d,  selected channel:%d \n",
          Scheduler::instance().clock(), mySF->flowID,BGIndex,rc);
#endif

  return rc;
}                                                                             

/*************************************************************
* function: int bondingGroupMgr::removeSFFromBG(int BGIndex, serviceFlowObject *mySFObjParam)                
*
* explanation:  This method removes the SF from the
*    BG object's  SF set.
*
* inputs: 
*    serviceFlowObject *mySFObjParam: ptr to the SF object to be removed
*
* outputs: 
*   Returns a SUCCESS or FAILURE
*
***************************************************************/
int bondingGroupMgr::removeSFFromBG(int BGIndex, serviceFlowObject *mySFObjParam)                
{   
  int rc;

#ifdef TRACEME
  printf("bondingGroupMgr::removeSFFromBG(%lf): remove flowID %d to BGIndex: %d (BG Obj BGID:%D) \n",
          Scheduler::instance().clock(), mySFObjParam->flowID,BGIndex,myBG[BGIndex].myBGID);
#endif

  rc = myBG[BGIndex].removeSFFromBG(mySFObjParam);


#ifdef TRACEME
  printf("bondingGroupMgr::removeSFFromBG: Updated BG %d by removing a SF Object \n",BGIndex);
#endif

  return rc;
}                                                                             

void bondingGroupMgr::printBondingGroupInfo()
{
  int i;

  printf("bondingGroupMgr::printBondingGroupInfo(%lf): Info for %d Bonding Groups : \n",
          Scheduler::instance().clock(),numberBGs);
  for (i=0;i<numberBGs;i++) {
    myBG[i].printBondingGroupObject();
  }
}


int bondingGroupMgr::command(int argc, const char*const* argv) {
	if (argc == 3) {
		if (strcmp(argv[1], "set-num-bonding-group") == 0) {
			init(atoi(argv[2]));
			return TCL_OK;
		}
		TclObject *obj;
		if( (obj = TclObject::lookup(argv[2])) == 0) {
			fprintf(stderr, "%s lookup failed\n", argv[1]);
			return TCL_ERROR;
		}
		if (strcmp(argv[1], "medium") == 0) {
			myMedium = (medium *)obj;
			return TCL_OK;
		}
	}
	else if (argc == 4) {
		if(strcmp(argv[1], "add-channel-bonding-group") == 0) {
			addChannelToBG(atoi(argv[2]),atoi(argv[3]));
			return TCL_OK;
		}
		else if(strcmp(argv[1], "del-channel-bonding-group") == 0) {
			removeChannelFromBG(atoi(argv[2]),atoi(argv[3]));
			return TCL_OK;
		}
	}
	return BiConnector::command(argc, argv);
}

/***********************************************************************
*function: void bondingGroupMgr::printStatsSummary(char *outputFile)
*
*explanation:
*
*inputs:
*
*outputs:
*
************************************************************************/
void bondingGroupMgr::printStatsSummary(char *outputFile)
{
  int i;

  printf("bondingGroupMgr::printStatsSummary(%lf): Number of BGs:%d \n",
		    Scheduler::instance().clock(), numberBGs);

  for(i=0;i<numberBGs;i++)
  {
    myBG[i].printStatsSummary(outputFile);
  }
}

int bondingGroupMgr::getHierarchicalMode() 
{
  return hierarchicalMode;
}


void bondingGroupMgr::dumpSFQueueLevels(char *outputFile) 
{
  int i,j;
  FILE *fp;
  double curTime = Scheduler::instance().clock();
  fp = fopen(outputFile, "a+");

#ifdef TRACEME
  printf("bondingGroupMgr::dumpSFQueueLevels:(%lf): Number of BGs:%d, #AggQueues in BG 0: %d\n",
		    Scheduler::instance().clock(), numberBGs, myBG[0].numberAggregateQueues);
#endif

  for(i=0;i<numberBGs;i++)
  {
    //for each aggregate queue, get current number in queue
    fprintf(fp,"%d\t%lf",i,curTime);
    for(j=0;j<myBG[i].numberAggregateQueues;j++)
    {
      fprintf(fp,"\t%d ",myBG[i].myAggSFObjects[j].myPacketList->getListSize());
    }
    fprintf(fp,"\n");
  }
  fclose(fp);
}

/***********************************************************************
*function: int bondingGroupMgr::transferSF(serviceFlowObject *fromSFObj, serviceFlowObject *toSFObj, flowID)
*
*explanation:
*  This method moves packets belonging the the flowID from the first SFObject list to the second SF Object list
*
*inputs:
* serviceFlowObject *fromSFObj
* serviceFlowObject *toSFObj
* int flowID:  this is the flow ID that will be used to find packets in the from SFObj list.
*
*outputs:
*  returns the total number of packets transferred.  A -1 indicates an error happened.
*  A 0 is a valid return - just means no packets from flowID were in the fromSFOBj.
*
************************************************************************/
int bondingGroupMgr::transferSF(serviceFlowObject *fromSFObj, serviceFlowObject *toSFObj, int flowID)
{
  int transferCount = 0;
  int rc = SUCCESS;
  serviceFlowListElement *tmpPtr = NULL;
  serviceFlowObject *tmpSFObject = NULL;
  Packet *tmpPkt =  NULL;
  packetListElement *myLE = NULL;
// this pointer is used to track through the list
  ListElement *nextLE = NULL;
  int i;
  double curTime = Scheduler::instance().clock();
  struct hdr_cmn *hdr = NULL;
  int fromListSize = fromSFObj->myPacketList->getListSize();
  double packetEntryTime=0;
  char queueFileName;



#ifdef TRACEME
  printf("bondingGroupMgr::transferSF(%lf): There are %d packets in the from List  Begin looking for this flow:%d\n ",
	  Scheduler::instance().clock(),fromListSize,flowID);
  printf("bondingGroupMgr::transferSF:(%lf): fromAggSF flowID:%d (numberPkts:%d), toAggSF flowID:%d (numberPkts:%d), pkt flowID:%d\n",
		    Scheduler::instance().clock(), fromSFObj->flowID, fromListSize, toSFObj->flowID, toSFObj->myPacketList->getListSize(), flowID);

#endif


  nextLE = fromSFObj->myPacketList->head;
  //For each packet in the from list 
  for (i=0; i<fromListSize;i++) {
    if (nextLE != NULL) {

      myLE = (packetListElement *)nextLE;
      nextLE = myLE; 
//      if (i < 10)
//         printf(" Look at pkt in flowID %d, packetEntryTime: %f \n ",myLE->flowID,myLE->packetEntryTime);

      //if the pkt is for this flow
      if (myLE->flowID == flowID) {
         //since we are removing myLE, let's correct the pointer we use to track through the list
         nextLE = myLE->prev; 
         tmpPkt = myLE->getPacket();
         packetEntryTime= myLE->packetEntryTime;
         //remove the LE (the  element object gets deleted in this routine)
         rc = fromSFObj->myPacketList->removeThisElement(myLE);
         if (rc == SUCCESS)
           fromSFObj->myPacketCount =  fromSFObj->myPacketList->getListSize();

#ifdef TRACEME
          printf("bondingGroupMgr::transferSF(%lf): rc from removeThisElement:%d the flowID:%d  listSize:%d \n ",
	             Scheduler::instance().clock(),rc, flowID,fromSFObj->myPacketList->getListSize());
#endif


         // now insert the packet in the correct location of the toSFObj list
          rc = toSFObj->insertPacketInTimeOrder(tmpPkt,packetEntryTime,flowID);
         if (rc == SUCCESS) {
           toSFObj->myPacketCount=  toSFObj->myPacketList->getListSize();
           transferCount++;
#ifdef TRACEME
            printf("bondingGroupMgr::transferSF(%lf):succeeded to insert the packet to the new queue (rc:%d), update to Q size:%d \n ",
	             Scheduler::instance().clock(),rc,toSFObj->myPacketList->getListSize());
#endif
         }
         else {
//#ifdef TRACEME
            printf("bondingGroupMgr::transferSF(%lf): HARD ERROR  Failed to insert, rc:%d \n ",
	             Scheduler::instance().clock(),rc);
//#endif
            break;
         }
      }
      if (nextLE != NULL)
        nextLE = nextLE->next;
    }
    else {
//#ifdef TRACEME
         printf("bondingGroupMgr::transferSF(%lf): HARD ERROR index %d, Ignore fromListSize:%d, toListSize:%d  \n ",
	             Scheduler::instance().clock(),i, fromSFObj->myPacketList->getListSize(), toSFObj->myPacketList->getListSize());
//#endif
      break;
    }
  }
#ifdef TRACEME
  printf("bondingGroupMgr::transferSF(%lf): Found a total of %d pkts that matched flow %d (looked at a total of %d packets in the from Queue with flowID:%d \n ",
	             Scheduler::instance().clock(),transferCount, flowID, i,fromSFObj->flowID);
#endif

  return transferCount;
}



/***************************************************************************
 * Module: resourceOptimizer
 *
 * Explanation:
 *   This file contains the class definition of the object that
 *   is responsible for system optimization.
 *
 * Revisions:
 *
************************************************************************/

#ifndef ns_resourceOptimizer_h
#define ns_resourceOptimizer_h

#include "globalDefines.h"
#include "mac-docsis.h"
//#include "hdr-docsis.h"
//#include "mac-docsistimers.h"
//#include "serviceFlowMgr.h"
//#include "bondingGroupMgr.h"
//#include "docsisDebug.h"

//#include "Minmax.h"

class MacDocsisCMTS;
class CMTSserviceFlowMgr;
class  bondingGroupMgr;
class schedulerObject;

struct resourceOptimizerStatsType
{
  int numberOptimizations;
};



class resourceOptimizer
{
public:
  resourceOptimizer();
  virtual ~resourceOptimizer();

  void init(int direction, MacDocsisCMTS *myMAC, CMTSserviceFlowMgr *mySFMgr, bondingGroupMgr *myBGMgr, schedulerObject *myScheduler);

  int optimizeSystem( );

  void getStats(struct resourceOptimizerStatsType *callersStats);
  void printStatsSummary(char *outputFile);
  void printShortSummary();
  int optimizerTimerHandler(Event *e);

  int direction;  //0 for UPSTREAM,  1 for DOWNSTREAM

private:
  schedulerObject *myScheduler;
  bondingGroupMgr *myBGMgr;
  MacDocsisCMTS *myMac;
  serviceFlowMgr *mySFMgr;
  double optimizationPeriod;  //in seconds to microsecond precision
  Event myEvent;
  optimizationTimer *myTimer;
  struct resourceOptimizerStatsType myStats;

//MinMax *myMinMax;

};

#endif


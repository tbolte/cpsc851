/***************************************************************************
 * Module: bondingGroupMgr
 *
 * Explanation:
 *   This file contains the class definition of the bonding group manager.
 *   This class contains the list of bonding groups - with
 *   each bonding group having  a set of channels (each with a 
 *   set of nodes).
 *   
 *
 * Revisions:
 *   $A806:  support for hierarchical scheduling
 *
 *  TODO:
 *
************************************************************************/

#ifndef ns_bondingGroupMgr_h
#define ns_bondingGroupMgr_h

#include "serviceFlowMgr.h"

//#include "config.h"
class bondingGroupObject;
class CMTSmacPhyInterface;
class serviceFlowMgr;
class MacDocsis;
class bondingGroupObject;

struct bondingGroupSet
{
  int  numberMembers;
  int  maxSetSize;
  bondingGroupObject *BGObjectPtrs[MAX_BONDING_GROUP_SET_SIZE];
};

/*************************************************************************
 ************************************************************************/
class bondingGroupMgr : public BiConnector
{
public:
  bondingGroupMgr();
  ~bondingGroupMgr();
  void init(int numberBGs);
  void init(int serviceDiscipline,int QSize, int QType);
  void setMyPhyInterface(CMTSmacPhyInterface *myPhyInterfaceParam);
  int addChannelToBG(int BGIndex,int ChIndex);
  int removeChannelFromBG(int BGIndex,int ChIndex);
  int checkChannelInBG(int BGIndex,int ChIndex);
  int findSetofFlows(int channelNumber, struct serviceFlowSet *);
  int findSetofChannels(int BGIndex, struct channelSet *);
//$A806
  int findSetofBGs(int channelNumber, struct bondingGroupSet *);
  int pickChannel(serviceFlowObject *mySF);

//$A807
  int transferSF(serviceFlowObject *fromSFObj, serviceFlowObject *toSFObj, int flowID);

  int addSFToBG(int BGIndex,serviceFlowObject *mySFObjParam);
  int removeSFFromBG(int BGIndex,serviceFlowObject *mySFObjParam);
  void printBondingGroupInfo();

  void configureBGs(int serviceDiscipline,int QSize, int QType, int numberQueues);
  void printStatsSummary(char *outputFile);
  int getHierarchicalMode();
  void dumpSFQueueLevels(char *outputFile);
  int optimize(int opCode);

  //This is the head of the array of BG objects
  bondingGroupObject *myBG;
  MacDocsis *myMac;
  serviceFlowMgr *mySFMgr;
  int numberBGs;

private:
  medium *myMedium;
  CMTSmacPhyInterface *myPhyInterface;

  int hierarchicalMode;

protected:
  int command(int argc, const char*const* argv);

};


#endif /* __ns_bondingGroupMgr_h__ */

/***************************************************************************
* Module: bondingGroupObject
*
* Explanation:
*   This file contains the class definition for the bonding group object.
*
* Revisions:
*   $A806:  Modifications to support hierarchical scheduling
*
*  TODO:
*
************************************************************************/
#include "bondingGroupObject.h"

#include "globalDefines.h"
#include "ListObj.h"
#include "serviceFlowObject.h"
#include "serviceFlowListElement.h"

#include "BGClassifierObject.h"
#include "aggregateSFObject.h"

#include "DRRpacketScheduler.h"


#include "docsisDebug.h"
//#define TRACEME 0
//#define TRACEME1 0
//#define FILES_OK 0
//#define SHOW_QUEUES 0
#define QUEUE_MONITOR_START 18 
#define  QUEUE_MONITOR_STOP 21

bondingGroupObject::bondingGroupObject() 
{

  hierarchicalMode = FALSE;
  mySFSet=NULL;
  myChannelSet=NULL;
  myAggSFObjects=NULL;
  myClassifier=NULL;
  numberAggregateQueues  = 0;
  nextAggSF_FlowID = 0;
  numberSFs=0;
  myPktScheduler = NULL;

  BGMAXp = 0.50;
  adaptiveMode = 0;
  BGmaxRate = BG_DEFAULT_RATE;
  queueServiceDiscipline = DRR;

//These are default values for aggregate SFs
  SchedQSize = 256;
  SchedQType = FIFOQ;
  BGWeight = 1.0;
  BGPriority = 0;
  BGDefaultQuantum = DEFAULT_FLOW_QUANTUM;


//Params for TSNTS
  Vcap = 38000000.0;
  Rchan = .65 * Vcap;
  Rts = 0.0;
  Rnts = 0.0;
  Rsum = 0.0;
  Rtsthresh = .75 * Rchan;
  Rntsthresh =  Rchan;
  TSNTS_state = TSNTS_UNCONGESTED;
  lastRateSampleTime = 0.0;
  controlFrequency  = 5;
 
  arrivalCount = 0;
  lossCount = 0;

  lastRateSampleTime = 0.0;
  byteArrivals = 0;
  byteDepartures = 0;
  avgArrivalRate  = 0.0;
  avgServiceRate  = 0.0;
  rateWeight = 0.50;

//Params for Fairshare
  FairShareCapacity = 38000000.0;
  FairShareCongestionThresh = 0.65 * FairShareCapacity;
  GoodToBadRateThreshold =  0.10 * FairShareCapacity;
  BadToGoodRateThreshold =GoodToBadRateThreshold;

#ifdef TRACEME
  printf("bondingGroupObject Constructed\n");
#endif
}

bondingGroupObject::~bondingGroupObject() 
{
#ifdef TRACEME
	printf("bondingGroupObject Destructor\n");
#endif
  delete myChannelSet;
  delete mySFSet;
  delete myChannelSet;
  delete myClassifier; 
  delete myAggSFObjects;
}

/***********************************************************************
*function: void bondingGroupObject::init(int BGIDparam, medium *myMediumParam, bondingGroupMgr *myBGMgrParam)
*
*explanation:  This method inits this bonding group object.
*
*  NOTE:  Currently, all configuration for the BG is hard coded here.
*         Eventually need a tcl interface for this.
*
*inputs:
*   int BGIDparam
*    medium *myMediumParam
*    bondingGroupMgr *myBGMgrParam)
*
*outputs:
*
************************************************************************/
void bondingGroupObject::init(int BGIDparam, medium *myMediumParam, bondingGroupMgr *myBGMgrParam)
{
  int i;

	myBGMgr = myBGMgrParam;
	myBGID = BGIDparam;
	myMedium = myMediumParam;

	myChannelSet = new IntegerListObj();
	myChannelSet->initList(MAX_CHANNELS_PER_BG);

	mySFSet = new ListObj();
	mySFSet->initList(MAX_SFS_PER_BG);

    //set base flow ID for this BG
    nextAggSF_FlowID = myBGID * 1000;

//#ifdef TRACEME
	printf("bondingGroupObject:init: myBGID:%d \n", myBGID);
//#endif


    bzero((void *)&myStats,sizeof(struct bondingGroupObjectStatsType));

}

/***********************************************************************
*function: int bondingGroupObject::optimize(int opCode)
*
*explanation:  This method is called periodically to perform
*     any long term bandwidth mgt or optimization functions
*     that might be required.
*
*
*inputs:
*   int opCode
*
*outputs:
*
************************************************************************/
int bondingGroupObject::optimize(int opCode)
{
  int i;
  int rc = SUCCESS;
  double curTime =   Scheduler::instance().clock();


  if (opCode == OPTIMIZER_HEARTBEAT) {
    updateRates();
  }
  else {
 

  //First, update the BG Flow rate monitors
  updateRates();

#ifdef TRACEME
    printf("bondingGroupObject::optimize(%lf): BGID:%d  total avgServiceRate: %9f \n",
          Scheduler::instance().clock(),myBGID, avgServiceRate);
#endif

#ifdef FILES_OK
  FILE *fp;
  fp = fopen("BGOPT.out", "a+");
  fprintf(fp,"%lf %d %9.0f %9.0f ",curTime,myBGID,avgArrivalRate,avgServiceRate);
  for(i=0;i<numberAggregateQueues;i++)
  {
    fprintf(fp," (%d: %9.0f %9.0f), ", i,myAggSFObjects[i].avgArrivalRate, myAggSFObjects[i].avgServiceRate);
  }
#endif

   //Second, invoke the schedulers optimizer....this might adjust the AQM operations
//   myPktScheduler->optimize(ADJUST_PACKET_SCHEDULER);

  //Third, invoke the discipline specific adaptation
  switch (queueServiceDiscipline)
    {

//FCFS
      case HDRR:

         break;

//HOSTBASED
      case HOSTBASED:
//Only do this now for BG1
         if (myBGID == 1)
           myClassifier->adjustFlowMap(SHUFFLE_FLOW_TO_BUCKET_MAPPING);
         break;

//Fairshare
      case FAIRSHARE:
           rc = adjustFairShareState();
         break;

//TSNTS
      case TSNTS:
           rc = adjustTSNTSState();
         break;

        //otherwise it is NOT hierarchical 
      default:
         break;
    }

#ifdef FILES_OK
  fprintf(fp,"\n");
  fclose(fp);
#endif
  }   
  return rc;
}

/***********************************************************************
*function: void bondingGroupObject::configureBG(int QSize, int QType, int QDiscipline, int numberQueues, double serviceRate, int weight, int priority)
*
*explanation:  This method configures the scheduling related params of this BG object.
*   It is called when the BG Mgr initializes.  
*   For the most part, this method is only applicable when some form of hierarchical scheduling is used.
*
*
*inputs:
*   int hierarchicalModeParam
*   int queueServiceDisciplineParam
*   int QSizeParam
*   int QTypeParam
*   int numberAggregateQueuesParam
*   double maxRateParam
*
*outputs:
*
* TODO:  For adaptiation...will this be used ?
*
************************************************************************/
void bondingGroupObject::configureBG(int hierarchicalModeParam, int queueServiceDisciplineParam, int QSizeParam, int QTypeParam, int numberAggregateQueuesParam, double maxRateParam)
{
  int i;
  double flowWeight = 1.0;
  int flowPriority = PRIORITY_LOW;

//#ifdef TRACEME
    printf("bondingGroupObject::configureBG(%lf): BGID:%d  updated QSize:%d, QType:%d, QDisc:%d, numberQueues:%d, serviceRate:%f \n",
          Scheduler::instance().clock(),myBGID, QSizeParam, QTypeParam, queueServiceDisciplineParam, numberAggregateQueuesParam, maxRateParam);
//#endif

    hierarchicalMode =  hierarchicalModeParam;
    queueServiceDiscipline = queueServiceDisciplineParam;
    SchedQSize = QSizeParam;
    SchedQType = QTypeParam;
    numberAggregateQueues = numberAggregateQueuesParam;
    BGmaxRate =  maxRateParam;

    BGWeight = 1.0;
    BGPriority = 0;
    BGDefaultQuantum = DEFAULT_FLOW_QUANTUM;
    adaptiveMode = 0;
    BGMAXp = 0.10;
    int flowQuantum = DEFAULT_FLOW_QUANTUM;

#ifdef TRACEME
	printf("bondingGroupObject:configure: myBGID:%d, hierarchicalMode:%d, QDiscipline:%d, QSize:%d, QType:%d, numberAggregateQs:%d, BGmaxRate:%f\n",
             myBGID,hierarchicalMode,queueServiceDiscipline,SchedQSize,SchedQType,numberAggregateQueues,BGmaxRate);
#endif


    bzero((void *)&myStats,sizeof(struct bondingGroupObjectStatsType));

    switch (queueServiceDiscipline)
    {

//FCFS
      case HDRR:
         myClassifier = new BGClassifierObject();
         myClassifier->init(myBGID,MAX_SFS_PER_BG, queueServiceDiscipline, this);
         myClassifier->initFlowMap((char *)NULL);
         myPktScheduler = new pDRRpacketSchedulerObject();
         myPktScheduler->init(bondingGroupDRR,myBGMgr->mySFMgr,myBGMgr);

         break;

//HOSTBASED
      case HOSTBASED:
         myClassifier = new BGClassifierObjectHash();
         myClassifier->init(myBGID,MAX_SFS_PER_BG, queueServiceDiscipline, this);
//JJM:  have it read a file : "HOSTBASEDMAP.dat"
         myClassifier->initFlowMap((char *)NULL);
         myPktScheduler = new pDRRpacketSchedulerObject();
         myPktScheduler->init(bondingGroupDRR,myBGMgr->mySFMgr,myBGMgr);
         break;

//Fairshare
      case FAIRSHARE:
         myClassifier = new BGClassifierObjectPriority();
         myClassifier->init(myBGID,MAX_SFS_PER_BG, queueServiceDiscipline, this);
//JJM:  have it read a file : "FAIRSHAREMAP.dat"
         myClassifier->initFlowMap((char *)NULL);
//JJM This will be a nw PrioritypacketSchedulerObject
         myPktScheduler = new pDRRpacketSchedulerObject();
         myPktScheduler->init(bondingGroupDRR,myBGMgr->mySFMgr,myBGMgr);
//Next, we need to init all SF's flowPriority with HIGH PRIORITY (puts them in priority BE class)
         myBGMgr->mySFMgr->resetSFPriority();
//Params for Fairshare
         FairShareCapacity =  BGmaxRate;
         FairShareCongestionThresh = 0.65 * FairShareCapacity;
         GoodToBadRateThreshold =  0.10 * FairShareCapacity;
         BadToGoodRateThreshold =GoodToBadRateThreshold;
         break;

//TSNTS
      case TSNTS:
         myClassifier = new BGClassifierObjectPriority();
         myClassifier->init(myBGID,MAX_SFS_PER_BG, queueServiceDiscipline, this);
//JJM:  have it read a file : "TSNTSMAP.dat"
         myClassifier->initFlowMap((char *)NULL);
//JJM This will be a nw PrioritypacketSchedulerObject
         myPktScheduler = new pDRRpacketSchedulerObject();
         myPktScheduler->init(bondingGroupDRR,myBGMgr->mySFMgr,myBGMgr);
         break;

        //otherwise it is NOT hierarchical 
      default:
         hierarchicalMode = FALSE; 
         numberAggregateQueues = 0;
         myClassifier = NULL;
         printf("bondingGroupObject::configure(%lf): For BGID %d, We are assuming scheduling discipline that is NOT HIERARCHICAL!! (queueServiceDiscipline:%d) \n", Scheduler::instance().clock(),myBGID,queueServiceDiscipline);
         break;
    }

    if (hierarchicalMode == TRUE) 
    {

      myAggSFObjects = new aggregateSFObject[numberAggregateQueues];


      double y = SchedQSize / 10;
      double x = SchedQSize / 2;
//#ifdef TRACEME //---------------------------------------------------------
//      myClassifier->printFlowMap();
      printf("bondingGroupObject::configure(%lf): Num aggregate queues to create:  %d \n", Scheduler::instance().clock(),numberAggregateQueues);
//#endif //---------------------------------------------------------------
      for(i=0;i<numberAggregateQueues;i++)
      {
        myAggSFObjects[i].init(DOWNSTREAM, nextAggSF_FlowID,myBGMgr->myMac, myBGMgr->mySFMgr, this);
        nextAggSF_FlowID++;
        if (queueServiceDiscipline == TSNTS) {
          //set 2nd queue to low priority (TS)
          if (i==1) {
            flowWeight = 1.0;
            flowPriority = PRIORITY_HIGH;
          }
          else {
           //set to higher priority
            flowWeight = 2.0;
            flowPriority = PRIORITY_LOW;
          }
        }
        if (queueServiceDiscipline == FAIRSHARE) {
          //set 2nd queue to 'timeout' queue
          if (i==1) {
            flowWeight = 0.25;
            flowPriority =  PRIORITY_LOW;
          }
          else {
           //set to higher priority
            flowWeight = 0.75;
            flowPriority = PRIORITY_HIGH;
          }
        }

        //update the flowQuantum to be used
        flowQuantum = int ( flowWeight * 2 * (double) DEFAULT_FLOW_QUANTUM);
        if (flowQuantum < MIN_FLOW_QUANTUM) {
          flowQuantum = MIN_FLOW_QUANTUM;
        }

        myAggSFObjects[i].flowQuantum = flowQuantum;

//#ifdef TRACEME //---------------------------------------------------------
        printf("bondingGroupObject::configure(%lf): For aggregate queue #%d, flowQuantum is: %d , SchedQType is %d\n", Scheduler::instance().clock(),i, myAggSFObjects[i].flowQuantum,SchedQType);
//#endif //---------------------------------------------------------------
        switch (SchedQType)
        {

        case RedQ:
          myAggSFObjects[i].setQueueBehavior(SchedQSize, SchedQType, flowPriority, (int)y,(int)x,0,BGMAXp,DEFAULT_FILTER_TIME_CONSTANT);
          break;

        case AdaptiveRedQ:
          myAggSFObjects[i].setQueueBehavior(SchedQSize, SchedQType, flowPriority, (int)y,(int)x,1,BGMAXp,DEFAULT_FILTER_TIME_CONSTANT);
          break;

        case DelayBasedRedQ:
          myAggSFObjects[i].setQueueBehavior(SchedQSize, SchedQType, flowPriority, (int)y,(int)x,2,BGMAXp,DEFAULT_FILTER_TIME_CONSTANT);
          break;

        case BAQMQ:
          myAggSFObjects[i].setQueueBehavior(SchedQSize, SchedQType, flowPriority, (int)y,(int)x,0,BGMAXp,DEFAULT_FILTER_TIME_CONSTANT);
          break;

        //otherwise it is a FIFOQ
        default:
          myAggSFObjects[i].setQueueBehavior(SchedQSize, SchedQType, flowPriority, (int)y,(int)x,0,BGMAXp, DEFAULT_FILTER_TIME_CONSTANT);
          break;
        }
      }
    }
}

void bondingGroupObject::setMyPhyInterface(CMTSmacPhyInterface *myPhyInterfaceParam)
{

#ifdef TRACEME
    printf("bondingGroupObject::setMyPhyInterface (ptr:%x)\n",myPhyInterfaceParam);
#endif
    myPhyInterface = myPhyInterfaceParam;

}

/*************************************************************
* function:
*     void bondingGroupObject::addChannelToBG(int ChIDX)                
*
* explanation:  This method adds the channel to the
*    BG's channel set.
*
* inputs: 
*  int ChIDX:  The channel number to be added
*
* outputs: returns SUCCESS or FAILURE
*
***************************************************************/
int bondingGroupObject::addChannelToBG(int ChIDX)                
{   
	IntegerListElement *myListElement = new IntegerListElement();
	int rc;

#ifdef TRACEME
	printf("bondingGroupObject::addChannelToBG:(%lf):  addchannel %d to BG %d \n",
		   Scheduler::instance().clock(), ChIDX,myBGID);
#endif

    myListElement->putData(ChIDX);
    rc = myChannelSet->addElement(*myListElement);

#ifdef TRACEME
	printf("bondingGroupObject::addChannelToBG:  IntegerList size : %d \n",myChannelSet->getListSize());
#endif

	return rc;
}                                                                             

int bondingGroupObject::removeChannelFromBG(int ChIDX)
{   
	int rc;

    rc = myChannelSet->removeElement(ChIDX);
#ifdef TRACEME
    printf("bondingGroupObject::removeChannelFromBG:(%lf): rc from removeElement:%d, number in Set:%d \n",
				            Scheduler::instance().clock(),rc,myChannelSet->getListSize());
#endif

	return rc;
}                                                                             

/*************************************************************
* function: int bondingGroupObject::checkChannelInBG(int ChIDX)
*
* explanation:  
*   Return the 1 (TRUE)  if the channel is in this BG
*   else return  0
*
* inputs: 
*     int ChIDX: The channel to check
*
*  Outputs:
*    Returns a TRUE or FALSE
*
***************************************************************/
int bondingGroupObject::checkChannelInBG(int ChIDX)
{   
  int rc;

  rc = myChannelSet->isElement(ChIDX);
#ifdef TRACEME
    printf("bondingGroupObject::checkChannel:(%lf): For this BG:%d, Channel %d is in myChannelSet ?%d (channelSet size:%d) (numberAggQueues:%d)\n",
           Scheduler::instance().clock(),myBGID, ChIDX, rc,myChannelSet->getListSize(),numberAggregateQueues);
#endif

  return rc;    
}

/*****************************************************************************
  * routine: int bondingGroupObject::isAnyChannelFree(void)
  *
  * function: 
  *   This method returns a TRUE if there is ANY free channel
  *   in the BG, else a FALSE
  *
  *Inputs:
  *
  *Outputs:
  *  Returns a TRUE or FALSE
  *
  *****************************************************************************/
int bondingGroupObject::isAnyChannelFree(void)
{
  int channelSetSize = myChannelSet->getListSize();
  int channelID;
  int channelStatus;
  int i;
  IntegerListElement *tmpPtr = NULL;


#ifdef TRACEME
   printf("bondingGroupObject::isAnyChannelFree(%lf):  current channel list size: :%d \n",
          Scheduler::instance().clock(),channelSetSize);
#endif
  if (channelSetSize > 0) {
    tmpPtr = myChannelSet->head;
    for (i=0; i<channelSetSize;i++) {
      channelID = tmpPtr->getData();
	  channelStatus = myPhyInterface->getChannelStatus(channelID);
#ifdef TRACEME
      printf("bondingGroupObject::isAnyChannelFree(%lf):  channel:%d status:%d\n",
          Scheduler::instance().clock(),channelID,channelStatus);
#endif
	  if (channelStatus == CHANNEL_IDLE) {
#ifdef TRACEME
         printf("bondingGroupObject::isAnyChannelFree(%lf):  Found free channel:%d\n",
          Scheduler::instance().clock(),channelID);
#endif
        return TRUE;
	  }
      tmpPtr = tmpPtr->next;
    }
#ifdef TRACEME
    printf("bondingGroupObject::isAnyChannelFree:  ALL CHANNELS ARE BUSY! \n");
#endif
  }
#ifdef TRACEME
  else
    printf("bondingGroupObject:: the channel set List is empty \n");
#endif

  return FALSE;


}

/*****************************************************************************
  * routine: int bondingGroupObject::isChannelFree(int ChIDX)
  *
  * function: 
  *   This method returns a TRUE if the ChIDX channel is available 
  *      (i.e., not busy)
  *
  *Inputs:
  *  int channelNumber:
  *
  *Outputs:
  *  Returns a TRUE or FALSE.  If there is an error, it returns FALSE
  *
*****************************************************************************/
int bondingGroupObject::isChannelFree(int ChIDX)
{
  int rc = FALSE;
  int channelStatus;

  channelStatus = myPhyInterface->getChannelStatus(ChIDX);
  if (channelStatus == CHANNEL_IDLE) {
    rc = TRUE;
  }

#ifdef TRACEME
    printf("bondingGroupObject::isChannelFree:(%lf): channel %d status is %d \n",
				            Scheduler::instance().clock(),ChIDX,channelStatus);
#endif

  return rc;    
}

/*****************************************************************************
  * routine: int bondingGroupObject::findFirstAvailableChannel()
  *
  * function: 
  *   This method selects the first free (i.e., TX idle) channel
  *   in the BG channel set.
  *
  *Inputs:
  *
  *Outputs:
  *  Returns a valid channel number else a -1
  *
*****************************************************************************/
int bondingGroupObject::findFirstAvailableChannel()
{
  int channelSetSize = myChannelSet->getListSize();
  int channelID;
  int channelStatus;
  int i;
  IntegerListElement *tmpPtr = NULL;


#ifdef TRACEME
   printf("bondingGroupObject::findFirstAvailableChannel(%lf):  current channel list size: :%d \n",
          Scheduler::instance().clock(),channelSetSize);
#endif
  if (channelSetSize > 0) {
    tmpPtr = myChannelSet->head;
    for (i=0; i<channelSetSize;i++) {
      channelID = tmpPtr->getData();
	  channelStatus = myPhyInterface->getChannelStatus(channelID);
#ifdef TRACEME
      printf("bondingGroupObject::findFirstAvailableChannel(%lf):  channel:%d status:%d\n",
          Scheduler::instance().clock(),channelID,channelStatus);
#endif
	  if (channelStatus == CHANNEL_IDLE) {
#ifdef TRACEME
         printf("bondingGroupObject::findFirstAvailableChannel(%lf):  Found free channel:%d\n",
          Scheduler::instance().clock(),channelID);
#endif
        return channelID;
	  }
      tmpPtr = tmpPtr->next;
    }
#ifdef TRACEME
    printf("bondingGroupObject::findFirstAvailableChannel:  ALL CHANNELS ARE BUSY! \n");
#endif
  }
#ifdef TRACEME
  else
    printf("bondingGroupObject::findFirstAvailableChannel: the channel set List is empty \n");
#endif


	return -1;    
}


/*****************************************************************************
  * routine: int bondingGroupObject::getSFSet(SetObject *mySet)
  *
  * function: 
  *   This method fills in the caller's SetObject with the SFs in this Bonding Group
  *   It only returns the BGs that have packets waiting in the queue
  *
  *   Algorithm:
  *     for each channel in the channel set
  *       
  *
  *Inputs:
  *   struct  serviceFlowSet *mySet
  *     struct serviceFlowSet
  *     {
  *       int  numberMembers;
  *       int  maxSetSize;
  *       serviceFlowObject (*SFArray)[MAX_SERVICE_FLOW_SET_SIZE];
  *     };
  *
  *Outputs:
  *  Returns the final number in the mySet.  
  *  Updates the callers serviceFlowSet.  
  *
  *****************************************************************************/
int bondingGroupObject::getSFSet(struct serviceFlowSet *callersSet)
{
  int numberCallerSFs = callersSet->numberMembers;
  int numberMySFs = mySFSet->getListSize();
  int returnNumber = numberCallerSFs;
  int i,j;
  serviceFlowListElement *tmpPtr = NULL;
  serviceFlowObject *tmpSFObject = NULL;
  
#ifdef TRACEME
  printf("bondingGroupObject::getSFSet(%lf):(BGID:%d) callers set size: %d, callers  maxSetSize :%d,  my SF list size:%d \n",
          Scheduler::instance().clock(),myBGID,numberCallerSFs,callersSet->maxSetSize,numberMySFs);
#endif
  tmpPtr = (serviceFlowListElement *) mySFSet->head;
  if (tmpPtr != NULL) {

    j = numberCallerSFs;
    //add  numberMySFs to the callersSet
    for (i=numberCallerSFs;i<(numberCallerSFs + numberMySFs);i++) {

      tmpSFObject =  tmpPtr->getServiceFlow();
      if ((tmpSFObject != NULL)  && (callersSet->numberMembers < callersSet->maxSetSize))
      {
        if (tmpSFObject->packetsQueued() > 0) {
#ifdef TRACEME 
          printf("bondingGroupObject::getServiceFlowSet(%lf): Index:%d  add this SF element flowid:%d (pktsQueued:%d)\n ",
		     Scheduler::instance().clock(),i, tmpSFObject->flowID, tmpSFObject->packetsQueued());
//          tmpSFObject->printSFInfo();
#endif
          callersSet->SFObjPtrs[j++] = tmpSFObject;
		  callersSet->numberMembers++;
          returnNumber++;
        }
#ifdef TRACEME 
        else
          printf("bondingGroupObject::getServiceFlowSet(%lf): Index:%d  DO NOT add this SF element flowid:%d (pktsQueued:%d)\n ",
		     Scheduler::instance().clock(),i, tmpSFObject->flowID, tmpSFObject->packetsQueued());
//          tmpSFObject->printSFInfo();
#endif
      }
	  else
        break;
      tmpPtr = (serviceFlowListElement *)tmpPtr->next;
    }
  }
  return returnNumber;
}

/*****************************************************************************
  * routine: int bondingGroupObject::getCompleteSFSet(SetObject *mySet)
  *
  * function: 
  *   This method fills in the caller's SetObject with the SFs in this Bonding Group
  *   It only returns the BGs that have packets waiting in the queue
  *
  *   Algorithm:
  *     for each channel in the channel set
  *       
  *
  *Inputs:
  *   struct  serviceFlowSet *mySet
  *     struct serviceFlowSet
  *     {
  *       int  numberMembers;
  *       int  maxSetSize;
  *       serviceFlowObject (*SFArray)[MAX_SERVICE_FLOW_SET_SIZE];
  *     };
  *
  *Outputs:
  *  Returns the final number in the mySet.  
  *  Updates the callers serviceFlowSet.  
  *
  *****************************************************************************/
int bondingGroupObject::getCompleteSFSet(struct serviceFlowSet *callersSet)
{
  int numberCallerSFs = callersSet->numberMembers;
  int numberMySFs = mySFSet->getListSize();
  int returnNumber = numberCallerSFs;
  int i,j;
  serviceFlowListElement *tmpPtr = NULL;
  serviceFlowObject *tmpSFObject = NULL;
  
#ifdef TRACEME
  printf("bondingGroupObject::getCompleteSFSet(%lf):(BGID:%d) callers set size: %d, callers  maxSetSize :%d,  my SF list size:%d \n",
          Scheduler::instance().clock(),myBGID,numberCallerSFs,callersSet->maxSetSize,numberMySFs);
#endif
  tmpPtr = (serviceFlowListElement *) mySFSet->head;
  if (tmpPtr != NULL) {

    j = numberCallerSFs;
    //add  numberMySFs to the callersSet
    for (i=numberCallerSFs;i<(numberCallerSFs + numberMySFs);i++) {

      tmpSFObject =  tmpPtr->getServiceFlow();
      if ((tmpSFObject != NULL)  && (callersSet->numberMembers < callersSet->maxSetSize))
      {
        if (tmpSFObject->packetsQueued() >= 0) {
//        if (tmpSFObject->packetsQueued() > 0) {
#ifdef TRACEME 
          printf("bondingGroupObject::getCompleteServiceFlowSet(%lf): Index:%d  add this SF element flowid:%d (pktsQueued:%d)\n ",
		     Scheduler::instance().clock(),i, tmpSFObject->flowID, tmpSFObject->packetsQueued());
//          tmpSFObject->printSFInfo();
#endif
          callersSet->SFObjPtrs[j++] = tmpSFObject;
		  callersSet->numberMembers++;
          returnNumber++;
        }
#ifdef TRACEME 
        else
          printf("bondingGroupObject::getCompleteServiceFlowSet(%lf): Index:%d  DO NOT add this SF element flowid:%d (pktsQueued:%d)\n ",
		     Scheduler::instance().clock(),i, tmpSFObject->flowID, tmpSFObject->packetsQueued());
//          tmpSFObject->printSFInfo();
#endif
      }
	  else
        break;
      tmpPtr = (serviceFlowListElement *)tmpPtr->next;
    }
  }
  return returnNumber;
}

/*****************************************************************************
  * routine: int bondingGroupObject::getCompleteAggSFSet(SetObject *mySet)
  *
  * function: 
  *   This method fills in the caller's SetObject with the SFs in this Bonding Group
  *   It only returns the BGs that have packets waiting in the queue
  *
  *   Algorithm:
  *     for each channel in the channel set
  *       
  *
  *Inputs:
  *   struct  serviceFlowSet *mySet
  *     struct serviceFlowSet
  *     {
  *       int  numberMembers;
  *       int  maxSetSize;
  *       serviceFlowObject (*SFArray)[MAX_SERVICE_FLOW_SET_SIZE];
  *     };
  *
  *Outputs:
  *  Returns the final number in the mySet.  
  *  Updates the callers serviceFlowSet.  
  *
  *****************************************************************************/
int bondingGroupObject::getCompleteAggSFSet(struct serviceFlowSet *callersSet)
{
  int numberCallerSFs = callersSet->numberMembers;
  int numberMySFs = mySFSet->getListSize();
  int returnNumber = numberCallerSFs;
  int i,j;
  aggregateSFObject *tmpSFObject = NULL;
  
#ifdef TRACEME
  printf("bondingGroupObject::getCompleteAggSFSet(%lf):(BGID:%d) callers set size: %d, callers  maxSetSize :%d,  my SF list size:%d \n",
          Scheduler::instance().clock(),myBGID,numberCallerSFs,callersSet->maxSetSize,numberMySFs);
#endif
  j = numberCallerSFs;
  for(i=0;i<numberAggregateQueues;i++)
  {
    tmpSFObject = &myAggSFObjects[i];
    if (tmpSFObject != NULL) {

      if (callersSet->numberMembers < callersSet->maxSetSize)
      {
        if (tmpSFObject->packetsQueued() >= 0) {
#ifdef TRACEME 
          printf("bondingGroupObject::getCompleteServiceAggFlowSet(%lf): Index:%d  add this SF element flowid:%d (pktsQueued:%d)\n ",
		     Scheduler::instance().clock(),i, tmpSFObject->flowID, tmpSFObject->packetsQueued());
//          tmpSFObject->printSFInfo();
#endif
          callersSet->SFObjPtrs[j++] = tmpSFObject;
		  callersSet->numberMembers++;
          returnNumber++;
        }
#ifdef TRACEME 
        else
          printf("bondingGroupObject::getCompleteServiceAggFlowSet(%lf): Index:%d  DO NOT add this SF element flowid:%d (pktsQueued:%d)\n ",
		     Scheduler::instance().clock(),i, tmpSFObject->flowID, tmpSFObject->packetsQueued());
//          tmpSFObject->printSFInfo();
#endif
      }
	  else
        break;
    }
  }
#ifdef TRACEME 
  printf("bondingGroupObject::getCompleteServiceAggFlowSet(%lf): returning %d (i:%d, j:%d) \n ",
    Scheduler::instance().clock(),returnNumber,i,j);
#endif
  return returnNumber;
}

/*****************************************************************************
  * routine: int bondingGroupObject::getChannelSet(struct channelSet *callersSet)
  *
  * function: 
  *   This method fills in the caller's SetObject with the channel set
  *   associated with this  Bonding Group
  *
  *   Algorithm:
  *     for each channel in the channel set
  *       
  *
  *Inputs:
  *     struct channelSet *callersSet : the callers channel set structure
  *
  *Outputs:
  *  Returns the final number in the channelSet.  
  *  Updates the callers channelSet  structure. 
  *
  *****************************************************************************/
int bondingGroupObject::getChannelSet(struct channelSet *callersSet)
{
  int numberCallerChannels = callersSet->numberMembers;
  int numberMyChannels = myChannelSet->getListSize();
  int returnNumber = numberCallerChannels;
  int i,j;
  IntegerListElement *tmpPtr = NULL;
  int channelMember;
  
#ifdef TRACEME
  printf("bondingGroupObject::getChannelSet(%lf)(BGID:%d): callers set size: %d, callers  maxSetSize :%d,  my channellist size:%d \n",
          Scheduler::instance().clock(),myBGID,numberCallerChannels,callersSet->maxSetSize,numberMyChannels);
#endif
  tmpPtr = myChannelSet->head;
  if (tmpPtr != NULL) {

    //add  numberMyChannels to the callersSet
    for (i=numberCallerChannels;i<(numberCallerChannels + numberMyChannels);i++) {

      channelMember =  tmpPtr->getData();
      if (callersSet->numberMembers < callersSet->maxSetSize)
      {
#ifdef TRACEME 
        printf("bondingGroupObject::getChannelSet(%lf): Index:%d  add this member: :%d\n ",
		    Scheduler::instance().clock(),i, channelMember);
#endif
        callersSet->channels[i] = channelMember;
		callersSet->numberMembers++;
        returnNumber++;
      }
	  else
        break;
      tmpPtr = tmpPtr->next;
    }
  }
  return returnNumber;
}

/*************************************************************
* function:
*  void bondingGroupObject::addSFToBG(serviceFlowObject *mySFObjParam)                
*
* explanation:  This method adds the SF to the
*    BG object's  SF set.
*
* inputs: 
*    serviceFlowObject *mySFObjParam: ptr to the SF object to be added
*
* outputs: 
*   Returns a SUCCESS or FAILURE
*
***************************************************************/
int bondingGroupObject::addSFToBG(serviceFlowObject *mySFObjParam)                
{   
	serviceFlowListElement *mySFListElement = new serviceFlowListElement();
	int rc;

#ifdef TRACEME
	printf("bondingGroupObject::addSFToBG(%lf): addSF flowID %d to BG %d (hierarchicalMode:%d)\n",
          Scheduler::instance().clock(), mySFObjParam->flowID,myBGID, hierarchicalMode);
#endif

    mySFObjParam->myBGObject = this;

    mySFListElement->putServiceFlow(mySFObjParam);
    rc = mySFSet->addElement(*mySFListElement);

    if (rc == SUCCESS)
    {
      if (hierarchicalMode == TRUE) 
        mySFObjParam->setHierarchicalMode(TRUE);
      numberSFs++;

#ifdef TRACEME
      printf("bondingGroupObject::addSFToBG: Updated list size : %d, numberSFs:%d \n",mySFSet->getListSize(),numberSFs);
#endif

    }
    else
	  printf("bondingGroupObject::addSFToBG(%lf): HARD ERROR:  could not add flowID%d to BG:%d (listSize:%d) \n",
          Scheduler::instance().clock(), mySFObjParam->flowID,myBGID,mySFSet->getListSize());


	return rc;
}                                                                             

int bondingGroupObject::removeSFFromBG(serviceFlowObject *mySFObjParam)
{   
	int rc;

#ifdef TRACEME
    printf("bondingGroupObject::removeSFFromBG:(%lf): rc from removeElement:%d, number in Set:%d \n",
		          Scheduler::instance().clock(),mySFObjParam->flowID,myChannelSet->getListSize());
#endif
//    rc = mySFSet->removeElement();
//    numberSFs--;`


	return rc;
}                                                                             

/*************************************************************
* function: int bondingGroupObject::checkChannelInBG(int ChIDX)
*
* explanation:  
*   Return the 1 (TRUE)  if the channel is in this BG
*   else return  0
*
* inputs: 
*     int ChIDX: The channel to check
*
*  Outputs:
*    Returns a TRUE or FALSE
*
***************************************************************/
int bondingGroupObject::isSFInBG(serviceFlowObject *mySFObjParam)
{   
  int rc;

//  rc = mySFSet->isElement(mySFObjParam);

  return rc;    
}

/*************************************************************
*
*  function: int bondingGroupObject::addPacket(Packet *p)
*
* explanation:  
*   This method is called when hierarchical scheduling
*   is being used. The method needs to insert
*   the packet in the correct leaf queue.
*
* inputs: 
*    Packet *p:  Packet to be queueud
*
*  Outputs:
*    Returns a SUCCESS or FAILURE
*
***************************************************************/
int bondingGroupObject::addPacket(Packet *p, serviceFlowObject *mySFObject)
{   

  aggregateSFObject *myAggSFObj = NULL;
  
  int rc = FAILURE;
  double curTime =   Scheduler::instance().clock();
  struct hdr_cmn *ch = HDR_CMN(p);
  struct hdr_ip *chip = HDR_IP(p);


#ifdef TRACEME 
    printf("bondingGroupObject::addPacket(%lf): Attempt to Insert packet in BG:%d,  SF flowid:%d, original Pkt priority:%d \n",
	  Scheduler::instance().clock(),myBGID, mySFObject->flowID, chip->prio_);
#endif 


  myAggSFObj = myClassifier->findAggregateQueue(p, mySFObject);

  if (myAggSFObj != NULL) {
#ifdef TRACEME 
    printf("bondingGroupObject::addPacket(%lf): Insert packet in BG:%d,  SF flowid:%d, AggSF flowid:%d, sizes:SF:%d,  AggSF:%d \n",
	  Scheduler::instance().clock(),myBGID, mySFObject->flowID, myAggSFObj->flowID, mySFObject->myPacketCount,myAggSFObj->myPacketCount);
#endif 

   rc = myAggSFObj->addPacket(p);
   if (rc == SUCCESS) {
#ifdef TRACEME 
    printf("bondingGroupObject::addPacket(%lf): success!   ListSize:%d \n",
	  Scheduler::instance().clock(), myAggSFObj->packetsQueued());
#endif 
    byteArrivals += ch->size();
    arrivalCount++;
   }
   else {
    lossCount++;
#ifdef TRACEME 
    printf("bondingGroupObject::addPacket(%lf): FAILED!   ListSize:%d, total drops:%d \n",
	  Scheduler::instance().clock(), myAggSFObj->packetsQueued(), lossCount);
#endif 
   }

  }

  return rc;
}


/*****************************************************************************
  * routine: int bondingGroupObject::findFirstAvailableChannel()
  *
  * function: 
  *   This method selects the first free (i.e., TX idle) channel
  *   in the BG channel set.
  *
  *Inputs:
  *
  *Outputs:
  *  Returns a valid channel number else a -1
  *
*****************************************************************************/
void bondingGroupObject::printBondingGroupObject()
{
  int channelSetSize = myChannelSet->getListSize();
  int SFSetSize = mySFSet->getListSize();
  int channelID;
  int serviceFlowID;
  int i;
  IntegerListElement *tmpPtr = NULL;
  serviceFlowListElement *SFElementPtr = NULL;
  serviceFlowObject *SFObj;


   printf("bondingGroupObject::printBondingGroupObject(%lf):(BGID:%d)  channel set size:%d,  SF set size:%d, hierarchicalMode:%d, numberAggQueues:%d \n",
          Scheduler::instance().clock(),myBGID,channelSetSize,SFSetSize,hierarchicalMode,numberAggregateQueues);
  printf("         channel set: ");
  if (channelSetSize > 0) {
    tmpPtr = myChannelSet->head;
    for (i=0; i<channelSetSize;i++) {
      channelID = tmpPtr->getData();
      printf("   %d ",channelID);
      tmpPtr = tmpPtr->next;
	}
    printf("  \n");
  } else
    printf("    EMPTY \n");

  printf("         SF set: ");
  if (SFSetSize > 0) {
    SFElementPtr = (serviceFlowListElement *)mySFSet->head;
    for (i=0; i<SFSetSize;i++) {
      SFObj = SFElementPtr->getServiceFlow();
      serviceFlowID = SFObj->flowID;
      printf("   %d ",serviceFlowID);
      SFElementPtr = (serviceFlowListElement *)SFElementPtr->next;
	}
    printf("  \n");
  } else
    printf("    EMPTY \n");
}

/*************************************************************
*
*  function: int bondingGroupObject::packetsQueued()
*
* explanation:  
*   This method returns total number of packets in all aggregate queues
*
* inputs: 
*
*  Outputs:
*    Returns total packet count
*
***************************************************************/
int bondingGroupObject::packetsQueued()
{   
  int i;
  int packetCount = 0;

  for(i=0;i<numberAggregateQueues;i++)
  {
#ifdef TRACEME 
    printf("bondingGroupObject::packetsQueued(%lf): BG:%d,AggregateQueueID:%d, have %d packets \n",
        Scheduler::instance().clock(),myBGID, i, myAggSFObjects[i].packetsQueued());
#endif 
    packetCount += myAggSFObjects[i].packetsQueued();
  }

#ifdef TRACEME 
  printf("bondingGroupObject::packetsQueued(%lf): BG:%d has %d AggregateQueues queued with a total of %d packets \n",
   Scheduler::instance().clock(),myBGID, numberAggregateQueues,packetCount);
#endif 

  return packetCount;
}

/*************************************************************
*
*  function: int bondingGroupObject::anyPacketQueued()
*
* explanation:  
*   This method returns TRUE if >0 packets in any of the queues 
*    else returns a FALSE
*
* inputs: 
*
*  Outputs:
*    Returns TRUE or FALSE 
*
***************************************************************/
int bondingGroupObject::anyPacketsQueued()
{   
  int i=0;
  int rc = FALSE;

  for(i=0;i<numberAggregateQueues;i++)
  {
   if (myAggSFObjects[i].packetsQueued() > 0)
     rc = TRUE;
     break;
  }

#ifdef TRACEME 
  printf("bondingGroupObject::anyPacketsQueued(%lf): BG:%d has %d AggregateQueues with at least 1 packet queued \n",
          Scheduler::instance().clock(),myBGID, numberAggregateQueues);
#endif 
  return rc;
}

/***********************************************************************
*function: void bondingGroupMgr::printStatsSummary(char *outputFile)
*
*explanation:
*
*inputs:
*
*outputs:
*
************************************************************************/
void bondingGroupObject::printStatsSummary(char *outputFile)
{
  int i;

  printf("bondingGroupObject::printStatsSummary(%lf): Number of aggregateQueues:%d \n",
		    Scheduler::instance().clock(), numberAggregateQueues);

  for(i=0;i<numberAggregateQueues;i++)
  {
   myAggSFObjects[i].printStatsSummary(outputFile) ;
  }
}



/***********************************************************************
*function: int bondingGroupObject::newArrivalUpdate(Packet *p, serviceFlowObject *mySFObject)
*
*explanation:
*
*inputs:
*
*outputs:
*
************************************************************************/
void bondingGroupObject::newArrivalUpdate(Packet *p, serviceFlowObject *mySFObject)
{
  aggregateSFObject *myAggSFObj = NULL;

  myAggSFObj = myClassifier->findAggregateQueue(p, mySFObject);

#ifdef TRACEME 
  printf("bondingGroupObject::newArrivalUpdate(%lf): BGID:%d, Number of aggregateQueues:%d,  pkt SF flowID:%d \n",
		    Scheduler::instance().clock(), myBGID, numberAggregateQueues, mySFObject->flowID);
#endif 

  if (myAggSFObj != NULL) {
#ifdef TRACEME 
    printf("bondingGroupObject::newArrivalUpdate(%lf): Deflect this call from SF flowID:%d to  Aggreg flowID:%d` \n",
	  Scheduler::instance().clock(),mySFObject->flowID, myAggSFObj->flowID);
#endif 

   myAggSFObj->newArrivalUpdate(p);
  }
  else {
#ifdef TRACEME 
    printf("bondingGroupObject::newArrivalUpdate(%lf): ERROR? Cound NOT  Deflect this call from SF flowID:%d to an  Aggreg flowID` \n",
	  Scheduler::instance().clock(),myBGID, mySFObject->flowID);
#endif 
  }
}


/***********************************************************************
*function: int bondingGroupObject::updateStats(Packet *p, int channelNumber, int bondingGroupNumber, serviceFlowObject *mySFObject)
*
*explanation:
*
*inputs:
*
*outputs:
*
************************************************************************/
void bondingGroupObject::updateStats(Packet *p, int channelNumber, int bondingGroupNumber, serviceFlowObject *mySFObject)
{
  aggregateSFObject *myAggSFObj = NULL;

  myAggSFObj = myClassifier->findAggregateQueue(p, mySFObject);

#ifdef TRACEME 
  printf("bondingGroupObject::updateStats(%lf): Number of aggregateQueues:%d \n",
		    Scheduler::instance().clock(), numberAggregateQueues);
#endif 

  if (myAggSFObj != NULL) {
#ifdef TRACEME 
    printf("bondingGroupObject::updateStats(%lf): Deflect this call from SF flowID:%d to  Aggreg flowID:%d` \n",
	  Scheduler::instance().clock(),myBGID, mySFObject->flowID, myAggSFObj->flowID);
#endif 

    myAggSFObj->updateStats(p,channelNumber,bondingGroupNumber);
  }
  else {
#ifdef TRACEME 
    printf("bondingGroupObject::updateStats(%lf): ERROR? Cound NOT  Deflect this call from SF flowID:%d to an  Aggreg flowID` \n",
	  Scheduler::instance().clock(),myBGID, mySFObject->flowID);
#endif 
  }

}


/***********************************************************************
*function: int bondingGroupObject::adjustTSNTSState()
*
*explanation:  
*   This implements the TS/NTS adjustment.
*   The system transitions to either uncongested state or to
*    one of two congestion states, referred to as TS-CONGESTION
*    and NTS-CONGESTION
*   
*
*inputs:
*    There are no direct inputs, but these are the variables that 
*    are maintained:
*       double Rchan;
*       double Rts;
*       double Rnts;
*       double Rsum;
*       double Rtsthresh;
*       double Rntsthresh;
*       int   TSNTS_state;
*
*outputs:
*  returns a SUCCESS or FAILURE
*
************************************************************************/
int bondingGroupObject::adjustTSNTSState()
{
  int i;
  int rc = SUCCESS;
  int defaultQuantum = myPktScheduler->defaultQuantum;


#ifdef TRACEME 
  printf("bondingGroupObject::adjustTSNTSState(%lf):BGID:%d, currentState:%d, numberAggQueues:%d, Rtsthreshold:%9f, Rntsthreshold:%9f, defaultQuantum:%d  \n",
	  Scheduler::instance().clock(),myBGID,TSNTS_state,numberAggregateQueues,Rtsthresh,Rntsthresh,defaultQuantum);
#endif 

    Rts =  myAggSFObjects[0].avgServiceRate;
    Rnts =  myAggSFObjects[1].avgServiceRate;
    Rsum = Rts + Rnts;

#ifdef TRACEME 
  printf("bondingGroupObject::adjustTSNTSState(%lf):BGID:%d, State:%d, Rts:%9f, Rnts:%9f, Rsum:%9f",
	  Scheduler::instance().clock(),myBGID,TSNTS_state,Rts,Rnts,Rsum);
#endif 
     switch (TSNTS_state)
     {

       case TSNTS_UNCONGESTED:
          if (Rts > Rtsthresh) {
            TSNTS_state = TS_CONGESTED;
            myAggSFObjects[0].flowQuantum = 0.75 * (double)2 * defaultQuantum;
            myAggSFObjects[1].flowQuantum = 0.25 * (double)2 * defaultQuantum;
          }
          else if (Rnts > Rntsthresh) {
            TSNTS_state = NTS_CONGESTED;
            myAggSFObjects[0].flowQuantum = 0.5 * (double)2 * defaultQuantum;
            myAggSFObjects[1].flowQuantum = 0.5 * (double)2 * defaultQuantum;
          }
          break;

       case TS_CONGESTED:
          if (Rts <= Rtsthresh) {
            if (Rnts > Rntsthresh) {
              TSNTS_state = NTS_CONGESTED;
              myAggSFObjects[0].flowQuantum = 0.5 * (double)2 * defaultQuantum;
              myAggSFObjects[1].flowQuantum = 0.5 * (double)2 * defaultQuantum;
            }
            else {
              TSNTS_state = TSNTS_UNCONGESTED;
              myAggSFObjects[0].flowQuantum = 0.5 * (double)2 * defaultQuantum;
              myAggSFObjects[1].flowQuantum = 0.5 * (double)2 * defaultQuantum;
            }
          }
          break;

       case NTS_CONGESTED:
          if (Rts > Rtsthresh) {
            TSNTS_state = TS_CONGESTED;
            myAggSFObjects[0].flowQuantum = 0.75 * (double)2 * defaultQuantum;
            myAggSFObjects[1].flowQuantum = 0.25 * (double)2 * defaultQuantum;
          }
          else  if (Rnts < Rntsthresh) {
            TSNTS_state = TSNTS_UNCONGESTED;
            myAggSFObjects[0].flowQuantum = 0.5 * (double)2 * defaultQuantum;
            myAggSFObjects[1].flowQuantum = 0.5 * (double)2 * defaultQuantum;
          }
          break;

        //otherwise it is an error
       default:
          printf("bondingGroupObject::adjustTSNTSState(%lf): BGID:%d, HARD ERROR TSNTS_state::%d  \n",
	              Scheduler::instance().clock(),myBGID,TSNTS_state);
          exit(1);
     }

#ifdef TRACEME 
  printf("  NewState:%d, flowQuantum0:%d, flowQuantum1:%d \n", TSNTS_state,myAggSFObjects[0].flowQuantum, myAggSFObjects[1].flowQuantum);
#endif 
  return rc;
}


/***********************************************************************
*function: int bondingGroupObject::adjustFairshareState()
*
*explanation: This implements the Fairshare adjustment algorithm.
*   Alll flow services rates should have been updated before calling this.
*
*inputs:
*  Indirectly uses the SF's priority and lastSchedulingEvent fields
*  
*
*outputs:
*  returns a SUCCESS or FAILURE

*  TODO:  This should operate only on the SFs in this BG.
*        
*
************************************************************************/
int bondingGroupObject::adjustFairShareState()
{
  int i;
  int rc = SUCCESS;
  double curTime =   Scheduler::instance().clock();
  int congestionFlag = FALSE;
 serviceFlowObject *SFObject = NULL;
  serviceFlowObject *tmpSFObject = NULL;
  serviceFlowListElement *SFListElement = NULL;
  serviceFlowListElement *tmpPtr = NULL;
  double byteCount = 0;
  aggregateSFObject *myAggSFObjFrom = NULL;
  aggregateSFObject *myAggSFObjTo = NULL;
  int transferCount = 0;
  int numberFlowChanges = 0;
#ifdef SHOW_QUEUES
  char traceString1[32];
  char *tptr1 = traceString1;
  char traceString2[32];
  char *tptr2 = traceString2;
#endif
  aggregateSFObject *SF1 = (aggregateSFObject *)&myAggSFObjects[PRIORITY_HIGH];
  aggregateSFObject *SF2 = (aggregateSFObject *)&myAggSFObjects[PRIORITY_LOW];


#ifdef SHOW_QUEUES
   //only for debug....do this just once
  if (myBGID == 1) {
  if ((curTime > QUEUE_MONITOR_START) && (curTime < QUEUE_MONITOR_STOP)) {
    sprintf(tptr1,"FLOWQUEUEBEFORE%d%d.out",myBGID,PRIORITY_HIGH);
    sprintf(tptr2,"FLOWQUEUEBEFORE%d%d.out",myBGID,PRIORITY_LOW);
    SF1->dumpQueues(tptr1);
    SF2->dumpQueues(tptr2);
  }
  }
#endif

//TODO:  Need to change this so it only operates on the SFs in this BG.
// For now, just operate on BG 1
  if (myBGID == 1) {

#ifdef TRACEME
  printf("bondingGroupObject:adjustFairShareState:(%lf):BGID:%d %12f\n", curTime,myBGID,avgServiceRate);
#endif

//Step 1:  Determine if the Fairshare pipe being managed is congested.  
//         If it is not congested, just check to see if any flows
//         in BE state can be moved to priority BE state. 

  if (avgServiceRate > FairShareCongestionThresh) {
    congestionFlag = TRUE;
#ifdef TRACEME
    printf("bondingGroupObject:adjustFairShareState:(%lf):BGID:%d detected congestion, avgServiceRate:%9f, FairShareCongestionThresh:%9f\n", curTime,myBGID,avgServiceRate,FairShareCongestionThresh);
#endif
  }


//Step 2: update the thresholds
  GoodToBadRateThreshold =  0.10 * FairShareCapacity;
  BadToGoodRateThreshold =GoodToBadRateThreshold;

#ifdef TRACEME
    printf("bondingGroupObject:adjustFairShareState:(%lf):BGID:%d congestionFlag:%d, FairShareCongestionThresh: %9f,  GoodToBadRateThreshold:%9f \n", curTime,myBGID,congestionFlag, FairShareCongestionThresh, GoodToBadRateThreshold);
#endif

//Step 3:  Put a SF into BE state if necessary 
//Step 4: Look at all flows currently in BE state, move any that are eligible for Priority BE
    //for all SFs, if curTime-lastSchedulingEvent > FAIRSHARE_  
    tmpPtr = (serviceFlowListElement *) myBGMgr->mySFMgr->mySFList->head;
    while (tmpPtr != NULL)
    {
      tmpSFObject =  tmpPtr->getServiceFlow();
      //Service only the SFs that are assigned to this BG
      if (tmpSFObject->myBGID != myBGID) {
        tmpPtr = (serviceFlowListElement *)tmpPtr->next;
        continue;
      }
      if (tmpSFObject != NULL) 
      {
#ifdef TRACEME
        printf("bondingGroupObject::adjustFairShare(%lf):SF: flowID:%d, currentavgServiceRate:%12.0f \n ",
            Scheduler::instance().clock(), tmpSFObject->flowID, tmpSFObject->avgServiceRate);
#endif
        if (congestionFlag == TRUE) {
          if (tmpSFObject->avgServiceRate > GoodToBadRateThreshold) {
            tmpSFObject->flowPriority = PRIORITY_LOW;
            tmpSFObject->lastSchedulingEvent = curTime;
            myAggSFObjFrom = (aggregateSFObject *)&myAggSFObjects[PRIORITY_HIGH];
            myAggSFObjTo = (aggregateSFObject *)&myAggSFObjects[PRIORITY_LOW];
#ifdef TRACEME
           printf("bondingGroupObject::adjustFairShare(%lf): Putting SFflowID %d into TIMEOUT! (avgServiceRate: %12.0f) \n ",
            Scheduler::instance().clock(), tmpSFObject->flowID, tmpSFObject->avgServiceRate);
           printf("bondingGroupObject::adjustFairShare(%lf): aggSFObjFrom flowID:%d, addSFObjto flowID:%d (currentSize:%d),  packet's flowID:%d\n",
	        Scheduler::instance().clock(), myAggSFObjFrom->flowID, myAggSFObjTo->flowID, myAggSFObjTo->packetsQueued(),tmpSFObject->flowID);
#endif
            numberFlowChanges++;
            transferCount = myBGMgr->transferSF(myAggSFObjFrom,myAggSFObjTo,tmpSFObject->flowID);

#ifdef TRACEME 
            printf("bondingGroupObject::adjustFairShare(%lf): transferCount%d, update addSFObjto currentSize:%d \n",
	           Scheduler::instance().clock(),transferCount, myAggSFObjTo->packetsQueued());
#endif 
          }
        }
        // look for flows ready to switch
        if ( (tmpSFObject->flowPriority == PRIORITY_LOW) && ((curTime - tmpSFObject->lastSchedulingEvent ) > FAIRSHARE_PENALTY_TIME)) {
            tmpSFObject->flowPriority = PRIORITY_HIGH;
            myAggSFObjTo = (aggregateSFObject *)&myAggSFObjects[PRIORITY_HIGH];
            myAggSFObjFrom = (aggregateSFObject *)&myAggSFObjects[PRIORITY_LOW];
#ifdef TRACEME
           printf("bondingGroupObject::adjustFairShare(%lf): Moving SFflowID %d out of TIMEOUT! (avgServiceRate: %12.0f) \n ",
            Scheduler::instance().clock(), tmpSFObject->flowID, tmpSFObject->avgServiceRate);
           printf("bondingGroupObject::adjustFairShare(%lf): aggSFObjFrom flowID:%d, addSFObjto flowID:%d (currentSize:%d),  packet's flowID:%d\n",
	        Scheduler::instance().clock(), myAggSFObjFrom->flowID, myAggSFObjTo->flowID, myAggSFObjTo->packetsQueued(),tmpSFObject->flowID);
#endif
            numberFlowChanges++;
            transferCount = myBGMgr->transferSF(myAggSFObjFrom,myAggSFObjTo,tmpSFObject->flowID);

#ifdef TRACEME 
            printf("bondingGroupObject::adjustFairShare(%lf): transferCount%d, update addSFObjto currentSize:%d \n",
	           Scheduler::instance().clock(),transferCount, myAggSFObjTo->packetsQueued());
#endif 
        }
        tmpPtr = (serviceFlowListElement *)tmpPtr->next;
    }
  }

//#ifdef TRACEME
  printf("bondingGroupObject::adjustFairShare(%lf):BGID:%d, made %d SF assignment changes.  New list sizes: Priority:%d, BE:%d  \n ",
            Scheduler::instance().clock(), myBGID, numberFlowChanges,SF1->myPacketList->getListSize(),  SF2->myPacketList->getListSize());
//#endif

//         myPktScheduler->optimize(ADJUST_PACKET_SCHEDULER);

  }

#ifdef SHOW_QUEUES
   //only for debug....do this just once
  if (myBGID == 1) {
  if ((curTime > QUEUE_MONITOR_START) && (curTime < QUEUE_MONITOR_STOP)) {
    sprintf(tptr1,"FLOWQUEUEAFTER%d%d.out",myBGID,PRIORITY_HIGH);
    sprintf(tptr2,"FLOWQUEUEAFTER%d%d.out",myBGID,PRIORITY_LOW);
    SF1->dumpQueues(tptr1);
    SF2->dumpQueues(tptr2);
  }
  }
#endif
  return rc;
}

/***********************************************************************
*function: void bondingGroupObject::updateRates()
*
*explanation: This updates the BG Object aggregate rates 
*   and it invokes the updateRates of all SFs.
*
*inputs:
*
*outputs:
*
************************************************************************/
void bondingGroupObject::updateRates()
{
  double curTime =   Scheduler::instance().clock();
  double sampleTime = curTime - lastRateSampleTime;
  double ArrivalRateSample=0;
  double DepartureRateSample=0;
  double totalBytesServiced = 0;
  int i;

#ifdef TRACEME
  printf("bondingGroupObject:updateRates:(%lf):BGID:%d  byteArrivals:%f, byteDepartures:%f, time since last rate sample:%f  \n", curTime,myBGID,byteArrivals,byteDepartures,sampleTime);
#endif

  for(i=0;i<numberAggregateQueues;i++)
  {
    totalBytesServiced += myAggSFObjects[i].updateRates();
#ifdef TRACEME
    if (myBGID == 1)
      printf("bondingGroupObject::updateRates:(%lf) AggSF index:%d : totalBytes: %9.0f, avgServiceRate:%9f\n", curTime,i,totalBytesServiced,myAggSFObjects[i].avgServiceRate);
#endif
  }

  if (sampleTime > 0 ) {
    ArrivalRateSample = byteArrivals*8/sampleTime;
    avgArrivalRate = (1-rateWeight)*avgArrivalRate + rateWeight*ArrivalRateSample;
    DepartureRateSample = totalBytesServiced*8/sampleTime;
    avgServiceRate = (1-rateWeight)*avgServiceRate + rateWeight*DepartureRateSample;
  }

#ifdef TRACEME
  if (myBGID == 1)
    printf("bondingGroupObject::updateRates:(%lf) WATCH  avgArrivalRate:%f (sample:%f),  avgServiceRate:%f(sample:%f), totalBytesServiced:%9.0f\n",
       curTime,avgArrivalRate,ArrivalRateSample,avgServiceRate,DepartureRateSample,totalBytesServiced);
#endif

  byteArrivals =0;
  byteDepartures =0;
  lastRateSampleTime = curTime;
}



/***************************************************************************
 * Module: hierarchicalPacketSchedulerObject
 *
 * Explanation:
 *   This file contains the class definition of the hierarchical packet scheduler. 
 *
 * Revisions:
 *
************************************************************************/


#include "hierarchicalPacketScheduler.h"
#include "serviceFlowObject.h"
#include "channelAbstraction.h"
#include "bondingGroupObject.h"
#include "aggregateSFObject.h"

#include "docsisDebug.h"
//#define TRACEME 0
#define TRACEME1 0
//#define FILES_OK 0




/*************************************************************************
 ************************************************************************/
hierarchicalPacketSchedulerObject::hierarchicalPacketSchedulerObject()
{
  lastBGIndexServiced=0;
  lastAggQueueIndexServiced=-1;
}

/*************************************************************************
 ************************************************************************/
hierarchicalPacketSchedulerObject::~hierarchicalPacketSchedulerObject()
{
}



void hierarchicalPacketSchedulerObject::init(int serviceDisciplineParam, int QSizeParam, int QTypeParam,  serviceFlowMgr * SFMgrParam, bondingGroupMgr *myBGMgrParam)
{
  double curTime = Scheduler::instance().clock();
  int i;

  lastBGIndexServiced=0;
  lastAggQueueIndexServiced=-1;
  QSize = QSizeParam;
  QType = QTypeParam;
  serviceDiscipline = serviceDisciplineParam;

  packetSchedulerObject::init(serviceDiscipline, SFMgrParam, myBGMgrParam);



//#ifdef TRACEME1 //---------------------------------------------------------
    printf("hierarchicalPacketSchedulerObject::init(%lf): Discipline:%d, qSize:%d, qType:%d  \n", curTime,serviceDisciplineParam,QSize,QType);
//#endif

}


/***********************************************************************
*function: bondingGroupObject * hierarchicalPacketSchedulerObject::selectBondingGroup()
*
*explanation: This method selects the next BG that will be served
*
* 
*inputs:
*  int channelNumber:  currently this is not used
*  struct bondingGroupSet *myBGSet:  this is the set of BGs that can use the channel and that
*  have at least one SF with a packet waiting for a transmission opportunity.
*
*outputs:
*  Returns the BG that is selected,  else a NULL;
*
*
************************************************************************/
bondingGroupObject * hierarchicalPacketSchedulerObject::selectBondingGroup(int channelNumber, struct bondingGroupSet *myBGSet)
{
  bondingGroupObject *selectedBG = NULL;

  return selectedBG;
}

/***********************************************************************
*function: Packet *hierarchicalPacketSchedulerObject::selectPacket(int channelNumber, struct serviceFlowSet *mySFSet)
*
*explanation: This method selects the next packet to be sent.
*    Pseudo code
* 
* 1.  myBGMgr -> get the BG object set (the set of BGs that are allowed to use this channel
* 2.  myBGObj =  selectBG
* 3.   p = selectPacketFromBG
*  
*
* 
*inputs:
*  int channelNumber:  currently this is not used
*  struct serviceFlowSet *mySFSet:  the callers SET of SFs to chose from.
*      The method assumes that the SFs have a packet
*
*outputs:
*  Returns a packet to be sent next, else a NULL;
*
*
************************************************************************/
Packet *hierarchicalPacketSchedulerObject::selectPacket(int channelNumber, struct serviceFlowSet *mySFSet)
{
  int mySFSetCount = mySFSet->numberMembers;
  int mySFCount = mySFMgr->numberSFs;
  int numberBGs = 0;
  Packet *returnPkt = NULL;
  double curTime = Scheduler::instance().clock();
  struct bondingGroupSet myBGSet;
  bondingGroupObject *selectedBG = NULL;
  bondingGroupObject *BGObj = NULL;
  int i;


#ifdef FILES_OK
  FILE *fp;
#endif



#ifdef TRACEME 
  printf("hierarchicalPacketSchedulerObject::selectPacket(%lf):(channelNumber:%d) SF set count:%d, totalSFs:%d \n", 
   curTime,channelNumber,mySFSetCount,mySFCount);
#endif 

//First, get the set of BGs that are eligible
  myBGSet.numberMembers = 0;
  myBGSet.maxSetSize = MAX_BONDING_GROUP_SET_SIZE;
  numberBGs = myBGMgr->findSetofBGs(channelNumber, &myBGSet);

#ifdef TRACEME 
  printf("hierarchicalPacketSchedulerObject::selectPacket(%lf): found this many BGs:%d (out of a total:%d), BGs are: \n", curTime,numberBGs,myBGMgr->numberBGs);
  BGObj = NULL;
    if (numberBGs > 0) {
	  for (i=0; i<numberBGs;i++) {
	    BGObj = myBGSet.BGObjectPtrs[i];
	    printf("   %d(%d) ", BGObj->myBGID,myBGSet.BGObjectPtrs[i]);
      }
      printf("  \n");
    } else
      printf("    EMPTY \n");
    fflush(stdout);
#endif 
#ifdef FILES_OK
  fp = fopen("BGPS.out", "a+");
  BGObj = NULL;
    if (numberBGs > 0) {
      fprintf(fp,"%lf %d %d ",curTime,channelNumber,numberBGs);
	  for (i=0; i<numberBGs;i++) {
	    BGObj = myBGSet.BGObjectPtrs[i];
        if (BGObj != NULL) {
          fprintf(fp," (%d,%d);  ",BGObj->myBGID,BGObj->packetsQueued());
        }
        else
          fprintf(fp," (ERROR); ");
      }
    } else
      fprintf(fp,"    EMPTY ");
#endif

//Second, select the BG to use
// For now just use RR
#ifdef TRACEME 
  printf("hierarchicalPacketSchedulerObject::selectPacket(%lf): begin RR to select BG: numberBGs in set:%d, lastBGIndexServiced:%d \n", 
      curTime,numberBGs,lastBGIndexServiced);
#endif 
  BGObj = NULL;
  selectedBG = NULL;
  lastBGIndexServiced++;
  if (lastBGIndexServiced >= numberBGs)
    lastBGIndexServiced =0;
  if (numberBGs > 0) {
	  for (i=lastBGIndexServiced; i<numberBGs;i++) {
	    BGObj = myBGSet.BGObjectPtrs[lastBGIndexServiced];
        if (BGObj != NULL) {
	      if (BGObj->anyPacketsQueued()) {
            selectedBG = BGObj;
            break;
          }
        lastBGIndexServiced++;
        if (lastBGIndexServiced >= numberBGs)
          lastBGIndexServiced =0;
        }
#ifdef TRACEME 
        else
           printf("hierarchicalPacketSchedulerObject::selectPacket(%lf): See a NULL BGObj at i:%d \n",curTime,i);
#endif 
      }
  }
  selectedBG = BGObj;

#ifdef FILES_OK
  fprintf(fp," (%d,%d);  ",selectedBG->myBGID,selectedBG->packetsQueued());
  fprintf(fp,"\n");
  fclose(fp);
#endif

//Third, select a packet from that BG
  if (selectedBG != NULL) {
#ifdef TRACEME 
  printf("hierarchicalPacketSchedulerObject::selectPacket(%lf): Selected BG Index:%d, this BGObject has %d queues and a total of %d packets waiting\n", 
      curTime,selectedBG->myBGID,selectedBG->numberAggregateQueues,selectedBG->packetsQueued());
#endif 
    returnPkt = selectPacketfromBG(channelNumber, selectedBG);
  }
  else {
#ifdef TRACEME 
  printf("hierarchicalPacketSchedulerObject::selectPacket(%lf): DID NOT SELECT A BG \n",curTime);
#endif 
  }

  return returnPkt;
}


/***********************************************************************
*function: Packet *hierarchicalPacketSchedulerObject::selectPacketfromBG(int channelNumber, bondingGroupObject *myBGObj)
*
*explanation: This method selects the next packet to be sent from the specified BG
*
*
* 
*inputs:
*  int channelNumber:  currently this is not used
*  bondingGroupObject *myBGObj:  BG object that has been selected  to send next
*
*outputs:
*  Returns a packet to be sent next, else a NULL;
*
*
************************************************************************/
Packet *hierarchicalPacketSchedulerObject::selectPacketfromBG(int channelNumber,  bondingGroupObject *myBGObj)
{
  struct serviceFlowSet mySFSet;
  mySFSet.numberMembers = 0;
  mySFSet.maxSetSize = MAX_SERVICE_FLOW_SET_SIZE;
  int mySFSetCount = 0;
  int numberActiveSFs = 0;
  Packet *returnPkt = NULL;
  serviceFlowObject *selectedSFObj = NULL;
  double curTime = Scheduler::instance().clock();
  int rc;
  int i;


    rc = myBGObj->getCompleteAggSFSet(&mySFSet);
    mySFSetCount = mySFSet.numberMembers;
    if (mySFSetCount == 0) {
#ifdef TRACEME
      printf("hierarchicalPacketSchedulerObject::selectPacketfromBG:(%lf): BGid:%d,  rc from findSetofFlow:%d, number in Set:%d \n",
            Scheduler::instance().clock(),myBGObj->myBGID,rc,mySFSet.numberMembers);
#endif
      return returnPkt;
    }

#ifdef TRACEME 
  printf("hierarchicalPacketSchedulerObject::selectPacketfromBG(%lf):(channelNumber:%d), BGIndex:%d, numberAggregateQueues:%d, we found a SFSet %d large \n", 
   curTime,channelNumber,myBGObj->myBGID, myBGObj->numberAggregateQueues,mySFSetCount);
    printf("hierarchicalPacketSchedulerObject::selectPacketfromBG:(%lf): rc from findSetofFlow:%d, number in Set:%d \n",
            Scheduler::instance().clock(),rc,mySFSet.numberMembers);
    printf(" Flowids in Set:   " );
    serviceFlowObject *SFObj;
    u_int16_t serviceFlowID;
    if (mySFSetCount > 0) {
      for (i=0; i<mySFSetCount;i++) {
        SFObj = mySFSet.SFObjPtrs[i];
        serviceFlowID = SFObj->flowID;
        printf("   %d ",serviceFlowID);
      }
      printf("  \n");
    } else
      printf("    EMPTY \n");
    fflush(stdout);
#endif


  returnPkt = myBGObj->myPktScheduler->selectPacket(channelNumber, &mySFSet);

#ifdef TRACEME 
  printf("hierarchicalPacketSchedulerObject::selectPacketfromBG(%lf):(channelNumber:%d), BGIndex:%d, numberAggregateQueues:%d, length of all queues:%d  \n", 
   curTime,channelNumber,myBGObj->myBGID, myBGObj->numberAggregateQueues,myBGObj->packetsQueued());
#endif 

  return returnPkt;


}


/***********************************************************************
*function: int hierarchicalPacketSchedulerObject::selectChannel(Packet *pkt, serviceFlowObject *mySFObj, struct channelSet *myChannelSet, int *channelSelection)
*
*explanation: This method selects the channel that will transmit the pkt.
* 
*inputs:
*
*outputs:
*  Returns a SUCCESS or FAILURE
*
*******************************************************************/
int hierarchicalPacketSchedulerObject::selectChannel(Packet *pkt, serviceFlowObject *mySFObj, struct channelSet *myChannelSet, int *channelSelection)
{
  int channelNumber = 0;
  int rc = FAILURE;
  int myChannelSetCount = myChannelSet->numberMembers;
  double curTime = Scheduler::instance().clock();
  int i;


#ifdef TRACEME 
   printf("hierarchicalPacketSchedulerObject::selectChannel(%lf): Need to select channel,  channel set count:%d \n", curTime,myChannelSetCount);
  if (myChannelSetCount >0) {
    for (i=0;i<myChannelSetCount;i++) {
      printf("   %x ",myChannelSet->channels[i]);
    }
    printf("\n");
  }
#endif 

  if (myChannelSetCount >0) {
    for (i=0;i<myChannelSetCount;i++) {
      channelNumber = myChannelSet->channels[i];
      if (mySFMgr->myPhyInterface->getChannelStatus(channelNumber) == CHANNEL_IDLE) {
        rc = SUCCESS;
        break;
#ifdef TRACEME 
   printf("hierarchicalPacketSchedulerObject::selectChannel(%lf): First IDLE channel: %d\n", curTime,channelNumber);
#endif 
      }
    }
  }

  if ((channelSelection != NULL) && (rc == SUCCESS))
  {
    *channelSelection = channelNumber;
  }
  else
    *channelSelection = -1;

#ifdef TRACEME 
   if (rc == SUCCESS)
     printf("hierarchicalPacketSchedulerObject::selectChannel(%lf): Return SUCCESS channelSelection:%d \n", curTime,*channelSelection);
   else
     printf("hierarchicalPacketSchedulerObject::selectChannel(%lf): Return FAILURE channelSelection:%d \n", curTime,*channelSelection);
#endif 
  return rc;
}


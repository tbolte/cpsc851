/********************************************************************************
 *
 *    This file contains c++ methods for implementing the channel and mediums timers
 *
 *  Revisions:
 *  $A307:  Added support for resequencing timer
 *          
 ******************************************************************************/

#include "channelTimers.h"

//$A500
#include "channelMgr.h"
#include "reseqFlowListElement.h"
#include "reseqMgr.h"

//#define TRACEME 0

baseTimer::baseTimer()
{
  busy_ =  paused_ = 0; stime = rtime = 0.0; 
}


void baseTimer :: start(Event * e, double time)
{
  Scheduler &s = Scheduler::instance();


  if(!e)
  {
	printf("MacDocsisTimer::start: packet/event is NULL!! (time:%f)\n",time);
    exit(1);
  }
  
 if (busy_ == 1)
  {

    printf("baseTimer:start(%lf) ERROR:  Timer already busy, scheduled for %lf\n",
     Scheduler::instance().clock(),(stime+rtime));
    exit(1);
  }

  busy_ = 1;
  paused_ = 0;
  stime = s.clock();
  rtime = time;
  assert(rtime >= 0.0);
  
#ifdef TRACEME
  printf("baseTimerr:start(%lf): schedule event at %lf \n",
     Scheduler::instance().clock(),rtime);
#endif

  s.schedule(this, e , rtime);
}

/*************************************************************************

*************************************************************************/
void baseTimer::stop(Event * e) 
{
  Scheduler &s = Scheduler::instance();
  
  assert(busy_);
  
  if(paused_ == 0)
    s.cancel((Event *)e);
  
 
  busy_ = 0;
  paused_ = 0;
  stime = 0.0;
  rtime = 0.0;
#ifdef TRACEME
  printf("baseTimer:stop:(%lf) stop event\n", Scheduler::instance().clock());
#endif
}


/***********************************************************************
*function: TxChannelTimer::TxChannelTimer(channelMgr *m, int channelNumberParam) : baseTimer() 
*
*explanation:  This is the constructor for a Tx Channel Timer.
*    A timer is associated with a particular channel.  The timer holds a pointer to its
*    channelMgr.  
*    When the timer is active, it holds a reference to the packet being transmit
*
*inputs:
* channelMgr *m:  ptr to the channel mgr object. 
* int channelNumberParam:  specifices the channel number associated with this timer object
*
*
*outputs:
*
*
************************************************************************/
TxChannelTimer::TxChannelTimer(channelMgr *m, int channelNumberParam) : baseTimer() 
{
    myChMgr = m;
	channelNumber = channelNumberParam;
#ifdef TRACEME
    printf("TxChannelTimer: constructor\n");
    fflush(stdout);
#endif
}

TxChannelTimer::~TxChannelTimer()
{
}

void TxChannelTimer::start(Packet *myp,  double time)
{
struct hdr_cmn *ch = HDR_CMN(myp);

 if (busy_ == 1)
  {

    printf("TxChannelTimer:start(%lf)[channel:%d]: ERROR:  Timer already busy , current pkt size: %d, scheduled for %lf \n",
     Scheduler::instance().clock(),channelNumber,ch->size(),(stime+rtime));
  }

 p = myp;
#ifdef TRACEME
  printf("TxChannelTimer:(%lf): starting the timer  (channel:%d, myp:%x,to fire: %lf) \n", 
		   Scheduler::instance().clock(), channelNumber,p, (double)(Scheduler::instance().clock()+time));
  fflush(stdout);
#endif
 baseTimer::start(&TimerEvent_,time);
}


void TxChannelTimer::handle(Event *e) 
{       
  busy_ = 0;
  paused_ = 0;
  stime = 0.0;
  rtime = 0.0;
  
#ifdef TRACEME
  printf("TxChannelTimer:(%lf): caling sendHandler (channel:%d, myp:%x, e->uid:%x) \n", Scheduler::instance().clock(), channelNumber,p,TimerEvent_.uid_);
  fflush(stdout);
#endif
  myChMgr->TxCompletionHandler(p,channelNumber);
}





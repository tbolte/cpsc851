/************************************************************************
* File:  packetListElement.cc
*
* Purpose:
*   Class for the packet List Element object
*
* Revisions:
***********************************************************************/

#include "packetListElement.h"


#include "docsisDebug.h"
//#define TRACEME 0

/***********************************************************************
*function: packetListElement::packetListElement
*
*explanation:
*
*inputs:
*
*outputs:
************************************************************************/
packetListElement::packetListElement() : ListElement()
{
#ifdef TRACEME
  printf("packetListElement: constructed packet List Element \n");
#endif
  myPkt = NULL;
  flowID= 0;
}


/***********************************************************************
*function: packetListElement::packetListElement
*
*explanation:
*
*inputs:
*
*outputs:
************************************************************************/
packetListElement::packetListElement(Packet *p) : ListElement()
{
  myPkt =  p;
  flowID = 0;
#ifdef TRACEME
  printf("packetListElement: constructed packet List Element \n");
#endif
}

/***********************************************************************
*function: packetListElement::packetListElement
*
*explanation:
*
*inputs:
*
*outputs:
************************************************************************/
packetListElement::packetListElement(Packet *p, int flowIDP) : ListElement()
{
  myPkt =  p;
  flowID = flowIDP;
#ifdef TRACEME
  printf("packetListElement: constructed packet List Element for SF flowID%d \n", flowID);
#endif
}

/***********************************************************************
*function: packetListElement::~packetListElement
*
*explanation:
*
*inputs:
*
*outputs:
************************************************************************/
packetListElement::~packetListElement()
{
}

/***********************************************************************
*function: int packetListElement::putPkt(Packet *pkt)
*
*explanation:
*
*inputs:
*
*outputs:
************************************************************************/
int packetListElement::putPkt(Packet *pkt)
{
int rc = 0;

  myPkt =  pkt;
  return rc;
}



/***********************************************************************
*function: Packet * packetListElement::getPacket()
*
*explanation:
*
*inputs:
*
*outputs:
************************************************************************/
Packet * packetListElement::getPacket()
{
//Packet *myPkt = NULL;
  return myPkt;
}







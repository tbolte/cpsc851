
/***************************************************************************
 * Module: channelAbstraction
 * 
 * Explanation:
 *   This file contains the channel abstraction object.  This object
 *   abstracts a unidirectional (i.e., half duplex) channel.
 *
 * Objects:
 *   ChannelAbstraction
 *
 *
 * Revisions:
 *
 *
 * TODO:
 *    -add routine headers to each method
 *
************************************************************************/

#include <float.h>
#include "docsisDebug.h"
#include "channelAbstraction.h"
#include "globalDefines.h"


channelAbstraction::channelAbstraction() //Constructor
{
}

channelAbstraction::~channelAbstraction() //Destructor
{
}

void channelAbstraction::sendUp(Packet* p)
{
	Scheduler &s = Scheduler::instance();
	Packet *newp;
	double propdelay = 0.0;
	struct hdr_cmn *hdr = HDR_CMN(p);
	int count=0;
	NodeListData *element;
	hdr->direction() = hdr_cmn::UP;

    //$A401
    myStats.total_num_sent_frames++;
    myStats.total_num_sent_bytes += hdr->size();


	mySendUpNodeList.setCurHeadElement();
	element = mySendUpNodeList.accessCurrentElement();
#ifdef TRACEME
	printf("ChannelAbstraction:SendUp(%lf): mySendUpNodeList Size : %d\n",
			   Scheduler::instance().clock(), mySendUpNodeList.getListSize());
#endif

///$A801
//  docsis_dsehdr *chx = docsis_dsehdr::access(p);
//      printf("channelAbstraction:SendUp:(%lf): pkt PSN:%d, pkt dsid:%d \n",
//               Scheduler::instance().clock(),chx->packet_sequence_number,chx->dsid);

	for(count = mySendUpNodeList.getListSize(); count > 0;count--)
	{
		element = mySendUpNodeList.nextElement();
		newp = p->copy();
//		propdelay = 0.000010;
		propdelay = myProperties.propDelay;
		element->startChTxDocsistimer(newp,propdelay);
///$A801
//  docsis_dsehdr *chx2 = docsis_dsehdr::access(newp);
//      printf("channelAbstraction:SendUp:(%lf): NEW pkt PSN:%d, pkt dsid:%d \n",
//               Scheduler::instance().clock(),chx2->packet_sequence_number,chx2->dsid);
	}
	Packet::free(p);
}


/*************************************************************
* routine: channelAbstraction::channelAbstraction(int channelNumber)
*
* Function: this is the channelAbstraction constructor
*           
* inputs: 
*   int channelNumber: the channel number assigned to this channel object  
*
* outputs:
*   none
*
***************************************************************/
channelAbstraction::channelAbstraction(int channelNumber)
{
  channelNumber_ = channelNumber;
}

void channelAbstraction::init(int channelNumber) // Setting and filling all values in the setnumchan() function in medium
{
  channelNumber_ = channelNumber;
//JJM WRONG
//This needs to be initialized to the configured number of stations.
  mySendDownNodeList.initList(500);
  mySendUpNodeList.initList(500);
  bzero((void *)&myStats,sizeof(struct channelStatsType));
  myStats.channelNumber = channelNumber;
  bzero((void *)&myProperties,sizeof(struct channelPropertiesType));

  collision_ = NO_COLLISION_DETECTED;
  rx_state_ = CHANNEL_IDLE;
  tx_state_ = CHANNEL_IDLE;

  //JJM WRONG
  rx_state_counterBUSY = 0;
  rx_state_counterIDLE = 0;
  tx_state_counterBUSY = 0;
  tx_state_counterIDLE = 0;

  utilizationMonitorBytes = 0;
  utilizationMonitorLastSampleTime = 0;
}

double channelAbstraction::TX_Time(Packet* p)
{
	struct hdr_cmn *ch = HDR_CMN(p);
	double t;
#ifdef TRACEME
	printf("packet size: %d, channel capacity: %d\n",ch->size(),myProperties.channelCapacity); 
#endif
	t = (double)((double)ch->size()/(myProperties.channelCapacity/8));
	
    t = t - .00000091;
    if (t <=0)
		t = .0000001;
	return t;
}

double channelAbstraction::getUtilizationMonitorSample()
{
  double curr_time = Scheduler::instance().clock();
  double channelUtilization = 0.0;
  double timeInterval  = curr_time - utilizationMonitorLastSampleTime;
  double BWConsumed  = 0.0;

  if (timeInterval > 0)
    BWConsumed  = (utilizationMonitorBytes * 8) / timeInterval;


  if ( myStats.channelCapacity > 0)
   channelUtilization = 100 * BWConsumed / myStats.channelCapacity;
  
#ifdef TRACEME
	printf("ChannelAbstraction:getUtilizationMonitorSample(%lf): utilizationMonitorBytes:%lf, BWConsumed:%lf, channelCapacity:%d, Util:%lf \n",
			   Scheduler::instance().clock(), utilizationMonitorBytes,BWConsumed,myStats.channelCapacity,channelUtilization);
#endif


  //update the state of the monitor
  utilizationMonitorBytes = 0;
  utilizationMonitorLastSampleTime = curr_time;

  return channelUtilization;
}

/*************************************************************
* routine: double channelAbstraction::getChannelUtilization()
*
* Function: this computes and returns the channel utilization
*    based on the total number of bytes sent/received and
*    the current lifetime of the simulation.
*           
* inputs: 
*
* outputs:
*   returns the channel utilization defined as the percentage
*   of the channel capacity used by the MAC.
*
***************************************************************/
double channelAbstraction::getChannelUtilization()
{
  double curr_time = Scheduler::instance().clock();

//  double totalBytes = myStats.total_num_sent_bytes; + myStats.total_num_rx_bytes; 
  double totalBytes = myStats.total_num_sent_bytes;

  double channelUtilization = 0.0;

  double BWConsumed  = (totalBytes * 8) / curr_time;

  if ( myStats.channelCapacity > 0)
   channelUtilization = 100 * BWConsumed / myStats.channelCapacity;
  
#ifdef TRACEME
	printf("ChannelAbstraction:getChannelUtilization(%lf): totalBytes:%lf, BWConsumed:%lf, channelCapacity:%d, Util:%lf \n",
			   Scheduler::instance().clock(), totalBytes,BWConsumed,myStats.channelCapacity,channelUtilization);
#endif


  return channelUtilization;
}

void channelAbstraction::getChannelProperties(channelPropertiesType *channelProperties)
{
	 *channelProperties = myProperties;
}

//#JIT002
void channelAbstraction::setChannelProperties(int chID, int direction,u_int32_t  dataRate, double propDelay, 
		u_int32_t OHBytes,int FECOverhead, u_int32_t ticksPerMinislot, u_int32_t maxBurst, double lossRate)
{

  myStats.channelCapacity = dataRate;

  myProperties.channelID = chID;
  myProperties.channelMode = direction;
  myProperties.channelCapacity = dataRate;
  myProperties.propDelay = propDelay;
//#JIT002
  myProperties.FECPercentage = FECOverhead;   //set to 8% for now
  myProperties.maxBurstSize = maxBurst;
  myProperties.overheadBytes = OHBytes;
  myProperties.ticks_p_minislot = ticksPerMinislot;
  myProperties.errorModelLossRate1 = lossRate;
  myProperties.errorModelLossRate2 = 0.0;


  double n;
  double size_mslots;
  double x;

  n = ((double)myProperties.ticks_p_minislot) * 6.25; /*Number of micro-sec per mini-slot*/
  size_mslots = n / ((double)(10*10*10*10*10*10));

  x = ((double) myProperties.channelCapacity) * size_mslots;
  myProperties.bytes_pminislot = (u_int16_t)x;


#ifdef TRACEME
	printf("ChannelAbstraction:setChannelProperties(%lf): channelCapacity:%ld, propDelay:%lf, OH Bytes:%d, x:%f, bytes_pminislot:%d\n",
			   Scheduler::instance().clock(), dataRate,propDelay,OHBytes,x,(int) myProperties.bytes_pminislot);
#endif

  if (channelNumber_ != chID) {
    printf("ChannelAbstraction:setChannelProperties(%lf):  ERROR:  chID (%d) does not match channelNumber_ (%d)f\n",
			   Scheduler::instance().clock(), chID,channelNumber_);
  }
}

int channelAbstraction::getCollisionState()
{
  return collision_;
}

void channelAbstraction::setCollisionState()
{
  collision_ = COLLISION_DETECTED;
}

void channelAbstraction::clearCollisionState()
{
  collision_ = NO_COLLISION_DETECTED;
}

int channelAbstraction::getTxState()
{
#ifdef TRACEME
  printf("channelAbstraction(%lf)[%d]: set tx_state_ :%d (busy:%lf, idle%lf)\n",
			   Scheduler::instance().clock(), channelNumber_,tx_state_,tx_state_counterBUSY,tx_state_counterIDLE);
#endif
  return tx_state_;
}

  
void channelAbstraction::setTxStateSEND()
{
  tx_state_ = CHANNEL_SEND;
  tx_state_counterBUSY++;
#ifdef TRACEME
  printf("channelAbstraction(%lf)[%d]: set tx_state_ to CHANNEL_SEND  (busy:%lf, idle%lf)\n",
			   Scheduler::instance().clock(), channelNumber_,tx_state_counterBUSY,tx_state_counterIDLE);
#endif
}

void channelAbstraction::setTxStateIDLE()
{
  tx_state_ = CHANNEL_IDLE;
  tx_state_counterIDLE++;
#ifdef TRACEME
  printf("channelAbstraction(%lf)[%d]: set tx_state_ to CHANNEL_IDLE  (busy:%lf, idle%lf)\n",
			   Scheduler::instance().clock(), channelNumber_,tx_state_counterBUSY,tx_state_counterIDLE);
#endif
}
  
int channelAbstraction::getRxState()
{
#ifdef TRACEME
  printf("channelAbstraction(%lf)[%d]: set rx_state_ :%d (busy:%lf, idle%lf)\n",
			   Scheduler::instance().clock(), channelNumber_,rx_state_,rx_state_counterBUSY,rx_state_counterIDLE);
#endif
  return rx_state_;
}

void channelAbstraction::setRxStateRECV()
{
  rx_state_ = CHANNEL_RECV;
  rx_state_counterBUSY++;
#ifdef TRACEME
  printf("channelAbstraction(%lf)[%d]: set rx_state_ to CHANNEL_RECV  (busy:%lf, idle%lf)\n",
			   Scheduler::instance().clock(), channelNumber_,rx_state_counterBUSY,rx_state_counterIDLE);
#endif
}

void channelAbstraction::setRxStateIDLE()
{
  rx_state_ = CHANNEL_IDLE;
  rx_state_counterIDLE++;
#ifdef TRACEME
  printf("channelAbstraction(%lf)[%d]: set rx_state_ to CHANNEL_IDLE  (busy:%lf, idle%lf)\n",
			   Scheduler::instance().clock(), channelNumber_,rx_state_counterBUSY,rx_state_counterIDLE);
#endif

}

int channelAbstraction::getDirection()
{
  return(direction_);
}

void channelAbstraction::setDirection(int direction)
{
  direction_ = direction;
}

void channelAbstraction::setDirectionDS()
{
  direction_ = CHANNEL_DIRECTION_DOWN;
}

void channelAbstraction::setDirectionUS()
{
  direction_ = CHANNEL_DIRECTION_UP;
}

void channelAbstraction::setDirectionDUPLEX()
{
  direction_ = CHANNEL_DIRECTION_DUPLEX;
}
  
void channelAbstraction::setNodeCount(int nodeCount)
{
  // Do something here
}




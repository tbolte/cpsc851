/********************************************************************************
 *
 *    This file contains c++ methods for implementing various timer classes
 *
 ******************************************************************************/

#include "mac-docsis.h"
#include "mac-docsistimers.h"

#include "docsisDebug.h"
//#define TRACEME 0

/*************************************************************************
Timers
*************************************************************************/
void MacDocsisTimer :: start(Packet * e, double time)
{
  Scheduler &s = Scheduler::instance();
  
  assert(busy_ == 0);
  
  busy_ = 1;
  paused_ = 0;
  stime = s.clock();
  rtime = time;
  assert(rtime >= 0.0);
  
#ifdef TRACEME
  printf("DocsisTimer:start(%lf): shedule event at %lf \n",
     Scheduler::instance().clock(),rtime);
#endif

  s.schedule(this, e , rtime);
}

/*************************************************************************

*************************************************************************/
void MacDocsisTimer::stop(Packet * e) 
{
  Scheduler &s = Scheduler::instance();
  
  assert(busy_);
  
  if(paused_ == 0)
    s.cancel((Event *)e);
  
  busy_ = 0;
  paused_ = 0;
  stime = 0.0;
  rtime = 0.0;
#ifdef TRACEME
  printf("DocsisTimer:stop:(%lf) stop event\n",
     Scheduler::instance().clock());
#endif
}

/*************************************************************************
Receive Timer
*************************************************************************/
void RxPktDocsisTimer::handle(Event *e) 
{       
  busy_ = 0;
  paused_ = 0;
  stime = 0.0;
  rtime = 0.0;
  
  mac->recvHandler(e);
}

/*************************************************************************
Send Timer
*************************************************************************/
void TxPktDocsisTimer::handle(Event *e) 
{       
  busy_ = 0;
  paused_ = 0;
  stime = 0.0;
  rtime = 0.0;

  mac->sendHandler(e);
}

/*************************************************************************
Send Timer
*************************************************************************/
void CmtsTxPktDocsisTimer::handle(Event *e) 
{       
  busy_ = 0;
  paused_ = 0;
  stime = 0.0;
  rtime = 0.0;
  
  //TODO $A401  this should call the macPhyInterface send handler
  mac->CmtsSendHandler(e);
}

/*************************************************************************

*************************************************************************/
void MapDocsisTimer::handle(Event *e)
{
  busy_ = 0;
  paused_ = 0;
  stime = 0.0;
  rtime = 0.0;
  
  mac->CmtsMapHandler(e);
}

/*************************************************************************

*************************************************************************/
void CmtsUcdDocsisTimer::handle(Event *e)
{
  busy_ = 0;
  paused_ = 0;
  stime = 0.0;
  rtime = 0.0;
  
  mac->CmtsUcdHandler(e);
}

/*************************************************************************

*************************************************************************/
void CmtsRngDocsisTimer::handle(Event *e)
{
  busy_ = 0;
  paused_ = 0;
  stime = 0.0;
  rtime = 0.0;

  
  mac->CmtsRngHandler(e);
}

/*************************************************************************

*************************************************************************/
void CmtsSyncDocsisTimer::handle(Event *e)
{
  busy_ = 0;
  paused_ = 0;
  stime = 0.0;
  rtime = 0.0;
  
  mac->CmtsSyncHandler(e);
}

//#ifdef RATE_CONTROL//-----------------------------------------------------
/*************************************************************************

*************************************************************************/
void CmtsTokenDocsisTimer::handle(Event *e)
{
  busy_ = 0;
  paused_ = 0;
  stime = 0.0;
  rtime = 0.0;
  
  mac->CmtsTokenHandler(e);
}
//#endif//------------------------------------------------------------------

/*************************************************************************

*************************************************************************/
void CmRngDocsisTimer::handle(Event *e)
{
  busy_ = 0;
  paused_ = 0;
  stime = 0.0;
  rtime = 0.0;
  
  mac->total_num_mgt_pkts_US ++;
  mac->total_num_rng_pkts_US ++;

#ifdef TRACEME
  printf("CmRngDocsisTimer(%lf): timer expired \n",
     Scheduler::instance().clock());
#endif
  mac->CmRngHandler(e);
}
/*************************************************************************

*************************************************************************/
void CmStatusTimer::handle(Event *e)
{
  busy_ = 0;
  paused_ = 0;
  stime = 0.0;
  rtime = 0.0;

  mac->total_num_mgt_pkts_US ++;
  mac->total_num_status_pkts_US ++;
  
#ifdef TRACEME
  printf("CmStatusTimer(%lf): status timer expired \n",
     Scheduler::instance().clock());
#endif
  mac->CmStatusHandler(e);
}

/*************************************************************************
 This is the handler hooks for the sequence hold timer (docsis 3.0) TOMMY
*************************************************************************/
void CmSeqTimer::handle(Event *e)
{
  busy_ = 0;
  paused_ = 0;
  stime = 0.0;
  rtime = 0.0;
  
  printf("TOMMY: sequence hold timer expired!!\n");
  mac->CmSeqHandler(e);
}


/*************************************************************************

*************************************************************************/
void CmServiceFlowSendTimer::handle(Event *e)
{
  busy_ = 0;
  paused_ = 0;
  stime = 0.0;
  rtime = 0.0;
//  printf("Snd timer expired..\n");
  
  mac->CmSndTimerHandler(e);
}

/*************************************************************************

*************************************************************************/
void CmServiceFlowRequestTimer::handle(Event *e)
{
  busy_ = 0;
  paused_ = 0;
  stime = 0.0;
  rtime = 0.0;
  
  mac->CmReqTimerHandler(e);
}

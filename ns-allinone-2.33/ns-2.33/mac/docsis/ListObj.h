/************************************************************************
* File:  ListObj.h
*
* Purpose:
*   Inlude files for a base list object. A ListObj is a container
*   of List Elements.
*
*
* Revisions:
*
*  $A700: 7-30-2010 added removeElement()
*
***********************************************************************/
#ifndef ListObj_h
#define ListObj_h
#include "ListElement.h"

class ListObj {

private:



protected:

  ListElement *curElement;
  double integralTotal;
  double lastIntegralTime;
  double initTime;

  double monitorPktCount;
  double lastSample;
  double queueRunningAverage;
  double numberTotalSamples;
  double numberPeriodSamples;
  double monitorPeriod;
  double minQueueLen;
  double maxQueueLen;


public:

  virtual ~ListObj();
  ListObj();
  virtual void initList(int maxListSize);
  virtual void setListSize(int maxListSize);
  virtual void updateStats();
  virtual void printStatsSummary();
  virtual int addElement(ListElement& element);
  virtual int addElementatHead(ListElement& element);
  virtual int insertElementInFrontThisElement(ListElement& newElement, ListElement& existingElement);

  virtual ListElement *removeElement();    //dequeue and return
  virtual int removeElement(ListElement *elementPtr);    //find this element in the list, dequeue and return
  virtual int removeThisElement(ListElement *elementPtr);    //dequeue and return this element that IS in the list
  virtual ListElement *getElement(int elementNumber);  //returns ptr to specific element

//JJM WRONG - remove these - if need this function then create a FIFOList (or a stack)
  ListElement *nextElement();      //jus returns ptr
  void setCurHeadElement();
  ListElement * accessCurrentElement();
// END JJM WRONG 

  int getListSize();
  unsigned int getNumberOverFlowEvents();
  virtual int displayListElements();
  int  MAXLISTSIZE;
  int getMaxListSize() { return MAXLISTSIZE; }
 
  int traceFlag;
  int listID; 

  double getAverageQueueLevel();

  //Put public only for performance reasons
  unsigned int totalNumberOverflowEvents;
  int curListSize;
  ListElement *head;
  ListElement *tail;
};



//JJM WRONG : We do not need htis.  To do a List with a Integer data type, 
//  just insert into the list IntegerListElement  which derives from ListElement
class IntegerListObj {

private:

  double integralTotal;
  double lastIntegralTime;
  double initTime;

  double monitorPktCount;
  double lastSample;
  double queueRunningAverage;
  double numberTotalSamples;
  double numberPeriodSamples;
  double monitorPeriod;
  double minQueueLen;
  double maxQueueLen;


protected:

  IntegerListElement *curElement;
  int  MAXLISTSIZE;

public:

  virtual ~IntegerListObj();
  IntegerListObj();
  void initList(int maxListSize);
  void setListSize(int maxListSize);
  int addElement(IntegerListElement& element);
  int addElementatHead(IntegerListElement& element);

  IntegerListElement *removeElement();    //dequeue and return
  int removeElement(int data);    //removes the element from the list
  int isElement(int data);    //indicats SUCCESS if the param is in the list

  int getListSize();
  unsigned int getNumberOverFlowEvents();
  int displayListElements();
  double getAverageQueueLevel();

  //Put public only for performance reasons
  unsigned int totalNumberOverflowEvents;
  int curListSize;
  IntegerListElement *head;
  IntegerListElement *tail;
};


class OrderedListObj : public ListObj 
{

private:

public:
  OrderedListObj();
  ~OrderedListObj();
  int addElement(OrderedListElement& element);
  int removeElement(OrderedListElement& element);    //dequeue and return
  int isElement(int key);    //indicats SUCCESS if the element with the key is in the list
  int displayListElements();
};

#endif

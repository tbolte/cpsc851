/************************************************************************
* File:  PIEAQM.cc
*
* Purpose:
*  This module implements PIE AQM. 
*  
* Revisions:
*
*  3/31/2014: This code was portedfrom the ns2 pie 
*  contribution from Cisco P. Natarajan, R. Pan, C. Piglione)
*
************************************************************************/
#include "PIE-AQM.h"
#include <iostream>
#include <math.h>
#include <random.h>

#include "object.h"

#include "globalDefines.h"
#include "docsisDebug.h"

//#define TRACEME 0
//#define FILES4_OK 0
//#define FILES_OK 0


PIEAQM::PIEAQM() : ListObj()
{
  stateFlag = 0;
  accessDelayMeasure =0;
  channelUtilizationMeasure=0;
  consumptionMeasure=0;

//PIE
  edv_.v_prob=0.0;      //drop p before "count"
  edv_.count=0;         // #pkts since last drop
  edv_.count_bytes=0;   // #bytes since last drop
  edv_.qdelay_old=0.0;  //previous delay

  edp_.mean_pktsize=0;    //tracks avg pkt size
  edp_.setbit=0;
  edp_.a=0.0;            //alpha weight
  edp_.b=0.0;            //beta weight
  edp_.tUpdate=0.0;      //freq of control
  edp_.qdelay_ref=0.0;   //target delay
  edp_.mark_p=0.0;       //
  edp_.use_mark_p= 0;
  edp_.bytes=0;          //true (1) if managing queue in bytes (rather than pkts)

  dq_count = -1;         
  dq_start = 0.0;
  burst_allowance=0.0;
  dq_threshold = 0;
  max_burst = 0.0;
//in bps
  avg_dq_rate = 0.0;
  tmp_rate = 0.0;

#ifdef TRACEME
  printf("PIEAQM: constructed list \n");
  fflush(stdout);
#endif
  reset();

}

PIEAQM::~PIEAQM()
{
#ifdef TRACEME
  printf("PIEAQM: destructed list \n");
#endif
}

void PIEAQM::initList(int maxListSize)
{

  ListObj::initList(maxListSize);

#ifdef TRACEME
  printf("PIEAQM:initList: size %d \n",maxListSize);
#endif
  flowPriority = 0;
  minth = 0;
  maxth = maxListSize;
  maxp = 0.10;
  weightQ = 1.0;
  filterTimeConstant = 0.002;
  adaptiveMode=0; // 0:FCFS; 1: PIE

  
  avgPacketLatency = 0.0;
  queueLatencyTotalDelay = 0.0;
  queueLatencySamplesCount = 0;
  queueLatencyMonitorTotalArrivalCount = 0;
  avgAvgQLatency= 0;
  avgAvgQLatencySampleCount= 0;
  lastQLatencySampleTime = 0;


  avgQLoss = 0;
  avgQLossCounter=0;
  avgQLossSampleCounter=0;
  lastQLossSampleTime = 0;

  avgAvgQLoss = 0;
  avgAvgQLossSampleCounter=0;

  lastRateSampleTime = 0;
  lastMonitorSampleTime = 0;


  avgQ = 0.0;
  dropP = 0.0;
  avgMaxP=0;
  avgMaxPSampleCount=0;
  avgDropP=0;
  avgDropPSampleCount=0;
  avgAvgQ = 0;
  avgAvgQPSampleCount= 0;
  q_time = 0.0;
  avgTxTime = (double)1500 * 8 / (double) 5000000;


 lastUpdateTime =0.0;
 stateFlag = 1;
 updateFrequency = .050;
 bytesQueued = 0;
 avgServiceRate=0;
 avgArrivalRate=0;
 byteArrivals = 0;
 byteDepartures = 0;
 rateWeight = .02;
 lastQLatencySampleTime =  0;

 PIEAQMupdateFrequency = AQM_ADAPTATION_TIME;
 PIEAQMUpdateCountThreshold = 0;
 PIEAQMUpdateCount = 0;
 PIEAQMLastSampleTime = 0;
 PIEAQMByteArrivals =0;
 PIEAQMByteDepartures = 0;
 avgPIEAQMArrivalRate = 0;
 avgPIEAQMServiceRate = 0;
 PIEAQMRateWeight = .02;

 PIEAQMQueueDelaySamples = 0;
 PIEAQMQueueLevelSamples = 0;
 PIEAQMAccessDelaySamples =0;
 PIEAQMConsumptionSamples = 0;
 PIEAQMChannelUtilizationSamples = 0;

  traceFlag = 1;


  qib_ = 1;  //for true
  edp_.bytes = 0;  //for false
  edp_.a= 0.125;
  edp_.b = 1.25;
  edp_.tUpdate=  PIE_UPDATE;
  dq_threshold =10;  //min number of packets in queue in order to take a departure rate sample
  edp_.mean_pktsize = 1024;

  edp_.mark_p =0.1;    //Used only if doing ECN
  edp_.use_mark_p = 0;  //True (1) to do ECN
  edp_.setbit = 1;  //set to true (1) for ECN

  edv_.v_prob = 0.0;   //drop p before "count"
  edp_.qdelay_ref =PIE_TARGET_LATENCY;  //This is QDELAY_REF

  max_burst = PIE_MAX_BURST;

  burst_allowance =max_burst;

  lastProbUpdateTime = 0.0;


//#ifdef TRACEME
//  if (traceFlag == 1) {
      printf("PIEAQM::init: ListID:%d,  tUpdate:%3.6f, qdelay_ref:%3.3f\n",listID,edp_.tUpdate,edp_.qdelay_ref);
//  }
//#endif


}

void PIEAQM::reset()
{
#ifdef TRACEME
  printf("PIEAQM:reset  \n");
#endif
    d_exp_ = 0.;
    dropping_ = 0;
    first_above_time_ = -1;
    drop_next_ = 0.0;
    maxpacket_ = 256;
    count_ = 0;

    numberDrops=0;
    numberPackets=0;

    interval_ = 0.100;
    target_ = 0.020;

//PIE
    curq_ = 0;
    edv_.count = 0;
    edv_.count_bytes = 0;
    edv_.v_prob = 0;

    calculate_p();
}

/*************************************************************
* routine:
*   int  PIEAQM::addElement(ListElement& element)
*
* Function: this routine inserts the ListElement to the tail of 
* the list.
*           
* inputs: 
*    ListElement& element : the element to be inserted
*
* outputs:
*  Returns a SUCCESS or FAILURE.
*  The caller must handle a FAILURE- this routine does not
*  delete the list element on a failure.
*
***************************************************************/
int  PIEAQM::addElement(ListElement& element)
{
  int rc = SUCCESS;
  double curTime =  Scheduler::instance().clock();


#ifdef TRACEME
  if (traceFlag == 1) {
   printf("PIEAQM:addElement(%lf)(listID:%d) (adaptiveMode:%d) listsize:%d, avgQ:%3.1f, minth:%d, maxth:%d,dropP:%3.3f, avgPacketLatency:%3.6f \n ",
      curTime,listID,adaptiveMode,curListSize,avgQ,minth,maxth,dropP,avgPacketLatency);
   }
#endif

  Packet *p = ((packetListElement &)element).getPacket();
  struct hdr_cmn *ch = HDR_CMN(p);
  byteArrivals+=ch->size();
  numberPackets++;
  PIEAQMByteArrivals += ch->size();
  ch->ts_ = curTime;

  if (maxpacket_ < ch->size_)
    // keep track of the max packet size.
    maxpacket_ = ch->size_;

//This needs to be every packet
  updateAvgQ();
  updateMonitors();

#ifdef TRACEME
  if (traceFlag == 1) {
   printf("PIEAQM:addElement(%lf) UPDATE AvgQ listsize:%d, avgQ:%3.1f, minth:%d, maxth:%d (dropP:%3.4f, count_:%d, avgArrivalRate:%f, avgServiceRate:%f, avgPacketLatency:%f \n",
      curTime,curListSize,avgQ,minth,maxth,dropP,count_,avgArrivalRate,avgServiceRate, avgPacketLatency);
   }
#endif


//If 1, we are doing PIE else just insert
  if (adaptiveMode == 1) {

    rc = doDropIn(p);

#ifdef TRACEME
    if (traceFlag == 1) {
      printf("PIEAQM:addElement(%lf) (ListID:%d) after doDropIn, rc:%d  (dropping:%d, drop_next_:%lf) \n",
         curTime,listID,rc,dropping_,drop_next_);
    }
#endif

  }
  //rc is SUCCESS  indicting the pkt can be queued, else the caller should drop it
  if (rc == SUCCESS) {
    rc = ListObj::addElement(element);
    if (rc == SUCCESS) {
      Packet *p = ((packetListElement &)element).getPacket();
      struct hdr_cmn *ch = HDR_CMN(p);
      bytesQueued+=ch->size();
    }
    else {
//     SHOULD HAPPEN on if we are doing drop tail, for now consider a HARD ERROR
//       printf("PIEAQM:addElement(%lf)(listID:%d) HARD ERROR CoDeL accepted packet but queue full ????  listsize:%d \n ", curTime,listID,curListSize);
//      exit(1);
    }
  }

#ifdef TRACEME
  if (traceFlag == 1) {
    printf("PIEAQM:addElement(%lf)(listID:%d) exit rc=%d, listsize:%d, avgQ:%3.1f, min-t:%d, max-t:%d, dropP:%3.3f, bytesQueued:%d \n ",
      curTime,listID,rc,curListSize,avgQ,minth,maxth,dropP, bytesQueued);
  }
#endif


  avgQLossSampleCounter++;
  if (rc == FAILURE) {
    avgQLossCounter++;
    numberDrops++;
#ifdef TRACEME
    if (traceFlag == 1) {
      printf("PIEAQM:addElement(%lf)(listID:%d) DROP listsize:%d, numberDropped:%f, count_:%d\n ", curTime,listID,curListSize,numberDrops,count_);
    }
#endif
  }
  return rc;
}

/*************************************************************
* routine: ListElement * ListObj::removeElement()
*
* Function:
*
* inputs: 
*
* outputs:
*   The element at the top of the list is removed and returned.
*   A NULL is returned if the list is empty
*
***************************************************************/
ListElement * PIEAQM::removeElement()
{
  packetListElement *tmpPtr = NULL;
  double curTime =   Scheduler::instance().clock();
  double x = 0;
  int pkt_size=0;


#ifdef TRACEME
  if (traceFlag == 1) {
    printf("PIEAQM::removeElement:(%lf) (listID:%d) start size: %d (bytesQueued:%d)\n", curTime, listID, curListSize,bytesQueued);
  }
#endif

  tmpPtr = (packetListElement *)ListObj::removeElement();
  if (curListSize == 0) {
    q_time = curTime;
  }

  if (tmpPtr != NULL) {

    Packet *p = ((packetListElement *)tmpPtr)->getPacket();
    struct hdr_cmn *ch = HDR_CMN(p);
    x = curTime - ch->ts_;
    pkt_size= ch->size();
    byteDepartures+=pkt_size;
    PIEAQMByteDepartures += pkt_size;
    bytesQueued-=pkt_size;
    if (bytesQueued < 0) {
      printf("PIEAQM::removeElement:(%lf)(listID:%d)  TROUBLE : bytesQueued went negative with this pkt size: %d\n", 
        curTime,listID, pkt_size);
      bytesQueued  = 0;
    }

    int rc = doDropOut(p);

    if (x > 0) {
      queueLatencyTotalDelay += x;
      queueLatencySamplesCount++;
//      updateAvgQ();
    }
  }

  double latestDepartureRate = update_dq_rate(pkt_size);

#ifdef TRACEME 
  if (traceFlag == 1) {
      printf("PIEAQM::removePacket(%lf): ListID:%d CurListSize:%d Packet delay sample: %3.6f, queueLatencyAvg:%3.6f(#samples:%6.0f), avgPacketLatency:%3.6f \n",
            curTime,listID, curListSize, x, queueLatencyTotalDelay,queueLatencySamplesCount,avgPacketLatency);
  }
#endif


  return tmpPtr;
}

//Called only when a new packet arrives  (which might be dropped....)
void PIEAQM::newArrivalUpdate(Packet *p)
{

 queueLatencyMonitorTotalArrivalCount++;

// We want to monitor the true arrival rate to the queue...
// so ignore when a new packet arrives ....we only
// caree when it gets queued.
  return;

  double curTime =  Scheduler::instance().clock();
  struct hdr_cmn *ch = HDR_CMN(p);
  byteArrivals+=ch->size();
  PIEAQMByteArrivals += ch->size();
}


void PIEAQM::updateStats()
{
  double curTime =   Scheduler::instance().clock();

#ifdef TRACEME
//  printf("PIEAQM:updateStats:(%lf), updated avg: %3.1f\n",curTime,avgQ);
#endif
}

void PIEAQM::printStatsSummary()
{
  double curTime =   Scheduler::instance().clock();

//#ifdef TRACEME
  printf("PIEAQM:ListObj:printStatsSummary:(%lf), avgAvgQ: %3.1f, avgDropP: %3.3f, avgQLatency:%3.6f\n",curTime,getAvgAvgQ(), getAvgDropP(),getAvgAvgQLatency());
//#endif
}


/*************************************************************
* routine: void PIEAQM::setAQMParams(int maxAQMSizeP, int priorityP, int minthP, int maxthP, int adaptiveModeP, double maxpP, double filterTimeConstantP)
*
* Function: this routine adapts the AQM parameters.
*   The algorithm sets CoDel specific parameters.
*           
* inputs: 
*   int maxAQMSizeP
*   int priorityP 
*   int minthP 
*   int maxthP 
*   int adaptiveModeP
*         0: FIFO
*         1: PIE
*   double maxpP
*   double filterTimeConstantP)
* outputs:
*
***************************************************************/
void PIEAQM::setAQMParams(int maxAQMSizeP, int priorityP, int minthP, int maxthP, int adaptiveModeP, double maxpP, double filterTimeConstantP)
{

  flowPriority = priorityP;
  MAXLISTSIZE = maxAQMSizeP;
  minth = minthP;
  maxth = maxthP;
  maxp = maxpP;
  filterTimeConstant = filterTimeConstantP;
  double curTime =   Scheduler::instance().clock();

  adaptiveMode= adaptiveModeP;

  if (!((adaptiveMode == 0) || (adaptiveMode ==1))) {
    printf("PIEAQM:setAQMParams: (listID:%d) WARNING BAD adaptiveMode:%d, set to FCFS \n", listID, adaptiveMode);
    adaptiveMode = 0;
  }

  printf("PIEAQM:setAQMParams(%lf) (listID:%d) adaptiveMode:%d,  minth:%d  maxth:%d (MAXLISTSIZE:%d)  maxp:%2.2f filterTimeConstant:%2.4f,  priority:%d (configured priorityP:%d),  \n",
      curTime,listID,adaptiveMode,minth,maxth,MAXLISTSIZE,maxp, filterTimeConstant,flowPriority, priorityP);

  traceFlag = 1;
}

/*************************************************************
* routine: void PIEAQM::adaptAQMParams()
*
* Function: this routine adapts the AQM parameters.
*   The algorithm is TBD.
*           
* inputs: 
*
* outputs:
*
***************************************************************/
void PIEAQM::adaptAQMParams()
{

#ifdef TRACEME
  printf("PIEAQM:adaptAQMParams: updated minth:%d  maxth:%d  maxp:%2.2f \n",minth,maxth,maxp);
#endif
}


double PIEAQM::getAvgQ()
{
  return(avgQ);
}

double PIEAQM::getAvgAvgQ()
{
  if (avgAvgQPSampleCount > 0)
    return(avgAvgQ/(double)avgAvgQPSampleCount);
  else
    return(0);
}

double PIEAQM::getAvgAvgQLatency()
{
  if (avgAvgQLatencySampleCount > 0)
    return(avgAvgQLatency/(double)avgAvgQLatencySampleCount);
  else
    return(0);
}

double PIEAQM::getAvgAvgQLoss()
{
  if (avgAvgQLossSampleCounter > 0)
    return(avgAvgQLoss/(double)avgAvgQLossSampleCounter);
  else
    return(0);
}


double PIEAQM::getDropP()
{
  return(dropP);
}

double PIEAQM::getAvgDropP()
{
  double tmpAvgDropP = 0.0;

  if (avgDropPSampleCount > 0)
    tmpAvgDropP = avgDropP / avgDropPSampleCount;
  
  return(tmpAvgDropP);
}

double PIEAQM::getAvgMaxP()
{
  double tmpAvgMaxP = 0.0;

  if (avgMaxPSampleCount > 0)
    tmpAvgMaxP = avgMaxP / avgMaxPSampleCount;
  
  return(tmpAvgMaxP);
}



/*************************************************************
* routine: double PIEAQM::updateAvgQLoss()
*
* Function: this routine computes and updates the loss monitor
*           
* inputs: 
*
* outputs:
*    returns the updated value
*
***************************************************************/
double PIEAQM::updateAvgQLoss()
{
  double curTime =   Scheduler::instance().clock();
  double sampleAvg1= 0;
  double returnAvg = 0.0;
  double sampleTime = curTime - lastQLatencySampleTime;


//  if (sampleTime < updateFrequency)
//    return avgPacketLatency;
 
 if (avgQLossSampleCounter == 0)
   return avgQLoss;

 sampleAvg1 = avgQLossCounter / avgQLossSampleCounter;

//   avgPacketLatency  = (1-weightQ) * avgPacketLatency + weightQ * sampleAvg1;
  returnAvg  = (1-filterTimeConstant) * avgQLoss + filterTimeConstant * sampleAvg1;

#ifdef TRACEME
  if (traceFlag == 1) {
  printf("PIEAQM:updateAvgQLoss:(%lf) listID:%d  curLen:%d, avgQ:%3.3f, lossSample%f, numberSamples:%f, MovingAvg:%f\n",
       curTime,listID,curListSize, avgQ,sampleAvg1,avgQLossSampleCounter,returnAvg);
   }
#endif


  avgAvgQLoss += returnAvg;
  avgAvgQLossSampleCounter++;

  lastQLossSampleTime = curTime;
  avgQLoss = returnAvg;
  avgQLossCounter=0;
  avgQLossSampleCounter=0;
  lastQLossSampleTime = curTime;

  return returnAvg;

}


/*************************************************************
* routine: double PIEAQM::updateAvgQLatency()
*
* Function: this routine computes and updates the global
*      AvgQ variable.
*           
* inputs: 
*
* outputs:
*    returns the updated value
*
***************************************************************/
double PIEAQM::updateAvgQLatency()
{
  double curTime =   Scheduler::instance().clock();
  double sampleAvg1= 0;
  double returnAvg = 0.0;
  double sampleTime = curTime - lastQLatencySampleTime;


//  if (sampleTime < updateFrequency)
//    return avgPacketLatency;
 
 if (queueLatencySamplesCount == 0)
   return avgPacketLatency;

 sampleAvg1 = queueLatencyTotalDelay / queueLatencySamplesCount;

//   avgPacketLatency  = (1-weightQ) * avgPacketLatency + weightQ * sampleAvg1;
  returnAvg  = (1-filterTimeConstant) * avgPacketLatency + filterTimeConstant * sampleAvg1;


  avgAvgQLatency+=returnAvg;
  avgAvgQLatencySampleCount++;
  lastQLatencySampleTime = curTime;

  avgPacketLatency = returnAvg;

#ifdef TRACEME
  if (traceFlag == 1) {
  printf("PIEAQM:updateAvgQLatency:(%lf) listID:%d  curLen:%d, avgQ:%3.3f, sampleAvg:%3.6f, avgPacketLatency:%3.6f based on %f samples\n",
       curTime,listID,curListSize, avgQ,sampleAvg1,avgPacketLatency,queueLatencySamplesCount);
   }
#endif

  queueLatencyTotalDelay = 0.0;
  queueLatencySamplesCount = 0;
  queueLatencyMonitorTotalArrivalCount = 0;

  return returnAvg;

}



/*************************************************************
* routine: double PIEAQM::updateAvgQ
*
* Function: this routine computes the global avgQ variable
*           
* inputs: 
*
* outputs:
*    returns the updated value.
*
***************************************************************/
double PIEAQM::updateAvgQ()
{
  double curTime =   Scheduler::instance().clock();
  double returnAvg = 0.0;


#ifdef TRACEME
  if (traceFlag == 1) {
  printf("PIEAQM:updateAvgQ:(%lf)  curListSize:%d, current avgQ:%3.3f, filterTimeConstant:%2.4f, q_time:%3.6f \n",
       curTime,curListSize,avgQ,filterTimeConstant,q_time);
  fflush(stdout);
   }
#endif


  if (curListSize == 0) {
///$A808
    double quietTime = 0;
    double power = 0;
    double x=0;
//    double quietTime = curTime - q_time;
//    double power = quietTime/avgTxTime;
//    double x=  pow((1-filterTimeConstant),power);
//    returnAvg = avgQ*x;
    returnAvg = 0;
#ifdef TRACEME
  if (traceFlag == 1) {
    printf("PIEAQM:updateAvgQ:(%lf) case of empty queue quietTime: %6.6f, power:%3.6f, x:%3.6f, returnAvg:%3.6f \n",
       curTime,quietTime,power,x,returnAvg);
   }
#endif
  }
  else {
    returnAvg = (1-filterTimeConstant)*avgQ + filterTimeConstant*curListSize;
#ifdef TRACEME
  if (traceFlag == 1) {
    printf("PIEAQM:updateAvgQ:(%lf) case of queue len:%d, current avgQ:%3.3f, updated avg:%3.3f \n",
       curTime,curListSize, avgQ,returnAvg);
   }
#endif
  }
  avgAvgQ+=returnAvg;
  avgAvgQPSampleCount++;

#ifdef TRACEME
  if (traceFlag == 1) {
  printf("PIEAQM:updateAvgQ:(%lf) curLen:%d, avgQ:%3.3f, returnAvg:%3.3f avgAvgQ:%3.3f (filterTimeConstant:%3.3f)\n",
       curTime,curListSize, avgQ, returnAvg,avgAvgQ/avgAvgQPSampleCount,filterTimeConstant);
   }
#endif
  avgQ = returnAvg;
  return returnAvg;

}

void PIEAQM::updateRates()
{
  double curTime =   Scheduler::instance().clock();
  double sampleTime = curTime - lastRateSampleTime;
  double ArrivalRateSample=0;
  double DepartureRateSample=0;

#ifdef TRACEME
  if (traceFlag == 1) {
  printf("PIEAQM:updateRates:(%lf)  byteArrivals:%f, byteDepartures:%f, time since last rate sample:%f  \n", curTime,byteArrivals,byteDepartures,sampleTime);
   }
#endif

//  if (sampleTime < updateFrequency)
//    return;

  if (sampleTime > 0 ) {
    ArrivalRateSample = byteArrivals*8/sampleTime;
    DepartureRateSample = byteDepartures*8/sampleTime;
  }

  avgArrivalRate = (1-rateWeight)*avgArrivalRate + rateWeight*ArrivalRateSample;


  avgServiceRate = (1-rateWeight)*avgServiceRate + rateWeight*DepartureRateSample;

#ifdef TRACEME
  if (traceFlag == 1) {
    printf("PIEAQM:updateRates:(%lf)  avgArrivalRate:%f (sample:%f),  avgServiceRate:%f(sample:%f) \n",
       curTime,avgArrivalRate,ArrivalRateSample,avgServiceRate,DepartureRateSample);
   }
#endif


  byteArrivals =0;
  byteDepartures =0;
  lastRateSampleTime = curTime;
}

/*************************************************************
* routine:
*  void PIEAQM::channelStatus(double accessDelayMeasure, double channelUtilizationMeasure, double consumptionMeasure)
*
* Function: this method is perioodicaly called to inform the AQM of current channel conditions. 
*        Based on this information, the routine might adapt its AQM operating settings.  It
*        can only do this if a  PIEAQMupdateFrequency amount of time has passed since the last
*        update.
*
* Inputs: 
*  double accessDelayMeasure
*  double channelUtilizationMeasure
*  double consumptionMeasure
*
* outputs:
*
*  This method will possible update the following:
*      -The minth or maxth range
*      - maxp
*      -
*****************************************************************/
void PIEAQM::channelStatus(double accessDelayMeasure, double channelUtilizationMeasure, double consumptionMeasure)
{
  double curTime =   Scheduler::instance().clock();
  double sampleTime = curTime - PIEAQMLastSampleTime;
  double ArrivalRateSample=0;
  double DepartureRateSample=0;


//  printf("PIEAQM:channelStatus(%lf) WARNING: Why am I getting this ???\n", curTime);
  return;

}


void PIEAQM::channelIdleEvent()
{
  double curTime =   Scheduler::instance().clock();
  double updateTime = curTime - lastUpdateTime;


//  printf("PIEAQM:channelIdleEvent(%lf) WARNING: Why am I getting this ???\n", curTime);
  return;

}

/*************************************************************
* routine: void PIEAQM::updateMonitors()
*
* Function: this routine is called periodically (set 
*   by updateFrequency) to update the following stats:
*        -rates :
*        -Avg Q Loss
*        -Avg Q Level
*        -Avg Q Latency
*
*  After the update if then makes an entry in the traceAQM trace file.
*           
* inputs: 
*
* outputs:
*
***************************************************************/
void PIEAQM::updateMonitors()
{
  double curTime =   Scheduler::instance().clock();
  double sampleTime = curTime - lastMonitorSampleTime;
  double sampleAvgQ = 0.0;
  double sampleAvgLatency = 0.0;


  if (sampleTime < updateFrequency)
    return;

#ifdef TRACEME
  if (traceFlag == 1) {
    printf("PIEAQM:updateMonitors:(%lf) listID:%d  byteArrivals:%f, byteDepartures:%f, time since last rate sample:%f  \n", curTime,listID,byteArrivals,byteDepartures,sampleTime);
  }
#endif

// this is done with each insert and remove
  sampleAvgQ = updateAvgQ();
  sampleAvgLatency =  updateAvgQLatency();
  updateRates();
//  if (adaptiveMode > 0) {
//    channelStatus(accessDelayMeasure, channelUtilizationMeasure,  consumptionMeasure);
//  }
  lastMonitorSampleTime = curTime;

#ifdef  FILES_OK
  if (traceFlag == 1) {
    FILE* fp = NULL;
    char traceString[32];
    char *tptr = traceString;
    sprintf(tptr,"traceAQM%d.out",listID);
    fp = fopen(tptr, "a+");
    fprintf(fp,"1 %6.6f\t%d\t%d\t%6.6f\t%6.6f\t%d\t%3.3f\t%d\t%d\t%6.6f\t%3.3f\t%12.0f\t\t%12.0f\t%3.6f\t%3.6f\n",
     Scheduler::instance().clock(),listID,dropping_,drop_next_,first_above_time_,count_, numberDrops/numberPackets,curListSize, bytesQueued,d_exp_,sampleAvgQ, avgArrivalRate, avgServiceRate, sampleAvgLatency,getAvgAvgQLatency());
    fclose(fp);
  }
#endif
}

/*************************************************************
* Routine: int PIEAQM:doDropIn(Packet *pktPtr)
*
* Function:
*   Internal routine that determines if the packet that has arrived should be dropped.
*
* Inputs: 
*    pktPtr: reference to a pkt that just arrived
*
* Outputs:
*     rc :  SUCCESS (and packet should NOT be dropped)
*           FAILURE (and packet should be dropped)
*
*  Pseudo code:
*
*    PIE:
*      simply determine if should drop pkt or NOT
*
***************************************************************/
int  PIEAQM::doDropIn(Packet *pktPtr)
{
  int rc = SUCCESS;
  double curTime =   Scheduler::instance().clock();
  double sampleAvgQ = 0.0;
  double sampleAvgLatency = 0.0;
  struct hdr_cmn *ch = HDR_CMN(pktPtr);


  ++edv_.count;
  edv_.count_bytes += ch->size();

  double localDropP= calculate_p();

#ifdef TRACEME
  if (traceFlag == 1) {
    printf("PIEAQM::doDropIn:(%lf) (listID:%d), curListSize:%d, bytesQueued:%d  edv_.count:%d, edv_.count_bytes:%d \n", curTime, listID,curListSize,bytesQueued,edv_.count,edv_.count_bytes);
  }
#endif

  if (curListSize + 1 >= MAXLISTSIZE) {
    rc = FAILURE;
  } else
    rc = drop_early(pktPtr);

  if (rc == FAILURE) {
    count_++;
  }

  sampleAvgQ = updateAvgQ();
  sampleAvgLatency =  updateAvgQLatency();

#ifdef  FILES_OK
  if (traceFlag == 1) {
    FILE* fp = NULL;
    char traceString[32];
    char *tptr = traceString;
    sprintf(tptr,"traceAQM%d.out",listID);
    fp = fopen(tptr, "a+");
    fprintf(fp,"2 %6.6f\t%d\t%d\t%6.6f\t%6.6f\t%d\t%3.3f\t%d\t%d\t%6.6f\t%3.3f\t%12.0f\t\t%12.0f\t%3.6f\t%3.6f\n",
     Scheduler::instance().clock(),listID,dropping_,drop_next_,first_above_time_,count_, numberDrops/numberPackets,curListSize, bytesQueued,d_exp_,sampleAvgQ, avgArrivalRate, avgServiceRate, sampleAvgLatency,getAvgAvgQLatency());
    fclose(fp);
  }
#endif
  return rc;
}

/*************************************************************
* Routine: int PIEAQM:doDropOut(Packet *pktPtr)
*
* Function:
*   Internal routine called when the channel(s) ready for next packet in the queue
*
* Inputs: 
*
* Outputs:
*   returns rc of SUCCESS or FAILURE. 
*
* Pseudo code:
*
*   Variables/state:
*    ------------------------
*   Set on entry
*     curTime : set to current simulation time
*
*   State:
*
*   Learned dynamically
*
*   Globally defined:
*
*   Logic
*    ------------------------
*
*   
***************************************************************/
int  PIEAQM::doDropOut(Packet *pktPtr)
{
  int rc = SUCCESS;
  double curTime = Scheduler::instance().clock();
  struct hdr_cmn *ch = HDR_CMN(pktPtr);
  double sampleAvgQ = 0.0;
  double sampleAvgLatency = 0.0;


  double localDropP = calculate_p();

  //Update sojourn time 
  //more genericallly this can be 'update the estimated average packet queue delay'
  d_exp_ = curTime - ch->ts_;

#ifdef TRACEME
  if (traceFlag == 1) {
    printf("PIEAQM::doDropOut:(%lf):ENTRY (listID:%d) dropping_:%d d_exp_:%6.6f, target_:%6.6f, first_above_time_:%6.6f, drop_now_:%d\n", curTime, listID,dropping_,d_exp_,target_,first_above_time_,drop_now_);
  }
#endif

#ifdef TRACEME
  if (traceFlag == 1) {
    printf("PIEAQM::doDropOut:(%lf):EXIT (listID:%d) dropping_:%d d_exp_:%6.6f, target_:%6.6f, first_above_time_:%6.6f, drop_now_:%d\n", curTime, listID,dropping_,d_exp_,target_,first_above_time_,drop_now_);
  }
#endif

  sampleAvgQ = updateAvgQ();
  sampleAvgLatency =  updateAvgQLatency();
#ifdef  FILES_OK
  if (traceFlag == 1) {
    FILE* fp = NULL;
    char traceString[32];
    char *tptr = traceString;
    sprintf(tptr,"traceAQM%d.out",listID);
    fp = fopen(tptr, "a+");
    fprintf(fp,"3 %6.6f\t%d\t%d\t%6.6f\t%6.6f\t%d\t%3.3f\t%d\t%d\t%6.6f\t%3.3f\t%12.0f\t\t%12.0f\t%3.6f\t%3.6f\n",
     Scheduler::instance().clock(),listID,dropping_,drop_next_,first_above_time_,count_, numberDrops/numberPackets,curListSize, bytesQueued,d_exp_,sampleAvgQ, avgArrivalRate, avgServiceRate, sampleAvgLatency,getAvgAvgQLatency());
    fclose(fp);
  }
#endif

  return rc;
}


/*************************************************************
* routine: double PIEAQM::calculate_p()
*
* Function:
*    update the drop probability based on current conditions
*    Maintains the global dropP variable.
*
* inputs: 
*
* outputs:
*    returns the current drop probability.
*    
*
* Explanation:
*    This updates p at most every tUpdate seconds
*
***************************************************************/
double PIEAQM::calculate_p()
{
  double now = Scheduler::instance().clock();
  double sampleTime = now - lastProbUpdateTime;
  double p = -1.0;
  double qdelay;

  if (sampleTime < edp_.tUpdate)
    return dropP;

  int qlen = qib_ ? bytesQueued : curListSize;

  qdelay = (avg_dq_rate > 0) ? qlen/avg_dq_rate : 0.0;
//  qdelay = (avg_dq_rate > 0) ? (qlen*8)/avg_dq_rate : 0.0;

  // in light dropping mode, take gentle steps; in medium dropping mode, take
  // medium steps; in high dropping mode, take big steps.	
  if (edv_.v_prob < 0.01) {
    p=edp_.a*(qdelay-edp_.qdelay_ref)/8+edp_.b/8*(qdelay-edv_.qdelay_old)+edv_.v_prob;
  } else if (edv_.v_prob < 0.1) {
    p=edp_.a*(qdelay-edp_.qdelay_ref)/2+edp_.b/2*(qdelay-edv_.qdelay_old)+edv_.v_prob;
  } else {

    p=edp_.a*(qdelay-edp_.qdelay_ref)+edp_.b*(qdelay-edv_.qdelay_old)+edv_.v_prob;

//    double tmp = edp_.a*(qdelay-edp_.qdelay_ref)+edp_.b*(qdelay-edv_.qdelay_old);
//    if (tmp < 0.02)
//      p=tmp+edv_.v_prob;
//    else {
//      p=0.02+edv_.v_prob;
//    }
  }

  /* for non-linear drop in prob */
  if ((qdelay == 0) && (edv_.qdelay_old == 0)) {
    p = p *0.98;
  } else if (qdelay > 0.25) {
    p = p + 0.02;
  }

  if (p < 0) p = 0;
  if (p > 1) p = 1;

  //printf("%f Q: %p Qlen: %d deq_count: %d Avg_deq_Count: %lf arv_count: %d Avg_arv_count: %lf Qdelay: %lf Drop_prob: %f avg_dq_time %f\n", now, this, qlen, edv_.deque_count, edv_.avg_deque_count, edv_.arv_count, edv_.avg_arv_count, qdelay,  p, avg_dq_time); 

  edv_.v_prob = p;
  // if the system is out of congestion when the queuing delay is well below
  // the target delay that the drop probability reaches zero. In that case, reset
  // parameters.
  if( (qdelay < (0.5*edp_.qdelay_ref)) && (edv_.qdelay_old < (0.5*edp_.qdelay_ref) ) && (edv_.v_prob == 0) ) {
    dq_count = -1;
    avg_dq_rate = 0.0;
    burst_allowance = max_burst;
  } 
		
  edv_.qdelay_old = qdelay;

  lastProbUpdateTime  = now;

  dropP = p;
#ifdef TRACEME
  if (traceFlag == 1) {
    printf("PIEAQM::calculate_p(%lf):EXIT (listID:%d) dropP:%3.3f, burstAllowance:%3.6f, avg_dq_rate:%9.0f\n", now, listID,dropP,burst_allowance,avg_dq_rate);
  }
#endif

#ifdef  FILES4_OK
  if (traceFlag == 1) {
    FILE* fp = NULL;
    char traceString[32];
    char *tptr = traceString;
    sprintf(tptr,"traceAQM%d.out",listID);
    fp = fopen(tptr, "a+");
    fprintf(fp,"4 %6.6f\t%d\t%3.3f\t%d\t%3.6f\t%3.6f\t%3.6f\t%3.3f\t%d\t%12.0f\t%3.6f\n",
     Scheduler::instance().clock(),listID, dropP, qlen, qdelay, edp_.qdelay_ref, edv_.qdelay_old, edv_.v_prob, dq_count, avg_dq_rate, burst_allowance);
    fclose(fp);
  }
#endif

  return dropP;
}


/*************************************************************
* routine: int PIEQueue::drop_early(Packet* pkt)
*
* Function:
*   this routine determines if the pkt is to be dropped.
*
* inputs: 
*
* outputs:
*   returns SUCCESSS (1)  if the pkt is NOT to be dropped    
*   returns a FAILURE if the pkt is to be dropped
*
* Explanation:
*
***************************************************************/
int PIEAQM::drop_early(Packet* pkt)
{
  double now = Scheduler::instance().clock();
  hdr_cmn* ch = hdr_cmn::access(pkt);
  double p = edv_.v_prob;
  int rc = SUCCESS;
  int qlen = qib_ ? bytesQueued : curListSize;

  //if managing queue  in bytes
  if (edp_.bytes) {
    p = p*ch->size()/edp_.mean_pktsize;
    if (p > 1) 
      p = 1; 
  }
#ifdef TRACEME
  if (traceFlag == 1) {
    printf("PIEAQM::drop_early(%lf):ENTRY (listID:%d), p:%3.3f, burstAllowance:%3.6f,edv_.count:%d,edv_.count_bytes:%d, edv_.v_prob:%3.3f\n", now, listID,p,burst_allowance,edv_.count,edv_.count_bytes,edv_.v_prob);
  }
#endif
	
  /* if there is still burst_allowance left, skip random early drop.
   * In addition, do not drop if queue delay is less than 50% of ref queue delay and
   * calculated drop prob is < 0.2. Or if queue Length is less than 2 packets. This is to ensure good link utilization. 
   * incremental, not essential optimization */
  if ( (burst_allowance > 0) || 
    ((edv_.qdelay_old < (0.5*edp_.qdelay_ref) && (edv_.v_prob < 0.2))) || (( qlen <= 2*edp_.mean_pktsize) && qib_ ) || ( qlen <= 2 && !qib_ ) ) {	     
    return SUCCESS;
  } 
  double u = Random::uniform();
  if (u <= p) {
    edv_.count = 0;
    edv_.count_bytes = 0;
    rc = FAILURE;
  }
#ifdef TRACEME
  if (traceFlag == 1) {
    if (rc == FAILURE) {
      printf("PIEAQM::drop_early(%lf): DROP (listID:%d), p:%3.3f, u:%3.3f,  burstAllowance:%3.6f,\n", now, listID,p,u,burst_allowance);
    }
    printf("PIEAQM::drop_early(%lf):EXIT (listID:%d), p:%3.3f, u:%3.3f,  burstAllowance:%3.6f\n", now, listID,p,u,burst_allowance);
  }
#endif
  return rc;			
}


/*************************************************************
* routine: double PIEAQM::update_dq_rate(int pkt_size)
*
* Function:
*    updates and returns latest dq rate
*    The function assumes a pkt (of size pkt_size) has been dequeued
*
* inputs: 
*   int pkt_size :  
*
* outputs:
*    updates the  avg_dq_rate which is in Bytes/sec
*    returns the rate in bps
*    
*
* Explanation:
*    This updates the dq estimage  at most every tUpdate seconds
*
***************************************************************/
double PIEAQM::update_dq_rate(int pkt_size)
{
  double now = Scheduler::instance().clock();


#ifdef TRACEME
  if (traceFlag == 1) {
    printf("PIEAQM::update_dq_rate(%lf):ENTRY: (listID:%d) avg_dq_rate:%12.0f, bytesQueued:%d, curListSize:%d (pkt_size:%d)\n", now, listID,avg_dq_rate,bytesQueued,curListSize,pkt_size);
  }
#endif


  //if measureing in bytes
  if ( qib_ ){

    // if not in a measurement cycle and the queue has built up to dq_threshold,
    // start the measurement cycle
    if ( (bytesQueued >= (dq_threshold*edp_.mean_pktsize)) && (dq_count == -1) ) {
      dq_start = now;
      dq_count = 0;
    }

    //If in cycle....
    if (dq_count != -1) {
      dq_count += pkt_size;
      // if we can obtain a sample.....
      if (dq_count >= (dq_threshold*edp_.mean_pktsize)) {
        double tmp = now - dq_start;
        int old_dq_count = dq_count;
        // time averaging deque rate, start off with the
        // current value, otherwise, average it.
        if (tmp > 0) {
          if (avg_dq_rate == 0) 
            avg_dq_rate = dq_count/tmp;
          else 
            avg_dq_rate =  0.9*avg_dq_rate+0.1*dq_count/tmp;

//          avg_dq_rate = 2.0;
          tmp_rate = 8.0 * avg_dq_rate;
        }

        // restart a measurement cycle if there is enough data
        if (bytesQueued > (dq_threshold*edp_.mean_pktsize)) {
          dq_start = now;
          dq_count = 0;
        } else {
           dq_count = -1;
        }

        // update burst allowance
        if (burst_allowance > 0) 
          burst_allowance -= tmp;

#ifdef TRACEME
        if (traceFlag == 1) {
          printf("PIEAQM::update_dq_rate(%lf):UPDATE: (listID:%d) avg_dq_rate:%12.0f, curListSize:%d, old_dq_count:%d,  dq_count:%d, tmp:%3.6f,burst_allowance:%6.0f\n", now, listID,avg_dq_rate,curListSize, old_dq_count,dq_count,tmp,burst_allowance);
        }
#endif

      }
    }
    
    //measuring in packets
  } else {
    // if not in a measurement cycle and the queue has built up to dq_threshold,
    // start the measurement cycle	
    if ( (curListSize >= dq_threshold) && (dq_count == -1) ) {
      dq_start = now;
      dq_count = 0;
    }

    //in a measurement cycle
    if (dq_count != -1) {
      dq_count ++;
      // done with a measurement cycle
      if (dq_count >= dq_threshold) {
        double tmp = now - dq_start;
        // time averaging deque rate, start off with the
        // current value, otherwise, average it.
        if (avg_dq_rate == 0) 
          avg_dq_rate = dq_count/tmp;
        else 
          avg_dq_rate = 0.9*avg_dq_rate+0.1*dq_count/tmp;

        tmp_rate = 8.0 * avg_dq_rate;

        // restart a measurement cycle if there is enough data
        if (curListSize > dq_threshold) {
          dq_start = now;
          dq_count = 0;
        } else {
          dq_count = -1;
        }

        // update burst allowance
        if (burst_allowance > 0) 
          burst_allowance -= tmp;
      }

    }
  }

#ifdef TRACEME
  if (traceFlag == 1) {
    printf("PIEAQM::update_dq_rate(%lf):EXIT (listID:%d) avg_dq_rate:%12.0f,tmp_rate:%12.0f, curListSize:%d, dq_count:%d, burst_allowance:%6.0f\n", now, listID,avg_dq_rate,tmp_rate,curListSize, dq_count,burst_allowance);
  }
#endif
//  return avg_dq_rate;
  return tmp_rate;
}


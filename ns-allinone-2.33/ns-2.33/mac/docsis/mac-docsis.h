/****************************************************************************
 * This file contains CM and CMTS class definitions. 
 *
 * Revisions:
 *   $A306:  10/4/2008:  add macPhyInterface object
 *   $A307:  11/11/2008: changes to add objects to CM
 *   $A805:  9/29/2010 : added support for the resourceOptimizer
 *   $A910:  4/1/2014:  add simple data packet trace
 *
 ***************************************************************************/

#include "docsisDebug.h"
#ifndef ns_mac_docsis_h
#define ns_mac_docsis_h

#include <vector>
#include "hdr-docsis.h"
#include "macPhyInterface.h"
#include "resourceOptimizer.h"

//$A805
class resourceOptimizer;
class CMTSconfigMgr;
class CMconfigMgr;
class medium;
class serviceFlowMgr;
class CMTSserviceFlowMgr;
class CMserviceFlowMgr;
class serviceFlowObject;
class bondingGroupMgr;
class schedulerObject;
class DSschedulerObject;
class USschedulerObject;
class packetMonitor;


#include "docsisVersion.h"





/*=======================CLASS DEFINITIONS============================*/

/*=======================Abstract Base class declaration===============*/

class MacDocsisCMTS;
class MacDocsisCM;

/*************************************************************************

*************************************************************************/

/*************************************************************************

POINT A
*************************************************************************/
class MacDocsis : public Mac 
{
 public:

  u_int32_t cm_id; /* node id  -for cmts this is 0 */
  macPhyInterface *myPhyInterface;
  medium *myMedium;
  DSschedulerObject *myDSScheduler;
  USschedulerObject *myUSScheduler;
  packetMonitor *myUSpacketMonitor;
  packetMonitor *myDSpacketMonitor;
  bondingGroupMgr *myBGMgr;

//$A510
  void tracePoint(char *s1, int param1, int param2);

  int myBGID; //Bonding Group ID
  int numDChan;
  int numUChan;
  int numChannels; 
  
  int defaultUSChannel;
  int defaultDSChannel;
  int defaultUSOverhead_bytes;

  //for debugging -- so you know which mac-docsis object you are reading about
  char docsis_id[10]; 

  //Used by both CMTS and CM
  u_int16_t bytes_pminislot; /* Number of bytes per mini-slot */
  u_int32_t minislots_psec;  /* Number of min-slots per second */
  double    size_mslots;     /* In micro-seconds..*/	
  u_int16_t size_ureqgrant;

   /* 
     Support for multiple docsis lans. The array stores address of each
     cmts, so that cm's can use it during the registration 
  */  

  static int num_cms;
  static int lan_num;
  static MacDocsisCMTS* cmts_arr[NUM_DOCSIS_LANS];

  
  //$A401
  struct mac_statistics MACstats;
  
  
  /* Methods */
  MacDocsis();
  virtual ~MacDocsis(){};

  virtual void CmtsUcdHandler(Event *e) {};
  virtual void CmtsSyncHandler(Event *e) {};
  virtual void CmtsRngHandler(Event *e) {};
  virtual void CmtsMapHandler(Event *e) {};
  virtual void CmStatusHandler(Event *e) {};
  virtual void CmRngHandler(Event *e) {};
  virtual void CmSndTimerHandler(Event *e) {};
  virtual void CmReqTimerHandler(Event *e) {};
  virtual void CmSeqHandler(Event *e) {};
  virtual void RecvFrame(Packet* p,int i) {};
  virtual void CmtsSendHandler(int channelNumber,Packet *p) {};
  virtual void CmSendHandler(int channelNumber,Packet *p) {};
  
  //#ifdef RATE_CONTROL//-------------------------------------------------
  virtual void CmtsTokenHandler(Event *e) {};
  virtual void CmtsSFTokenHandler(Event *e) { /* empty */ };
  //#endif//--------------------------------------------------------------
  
  int command(int argc, const char*const* argv);
 
  void dumpChannelStatus();
  
  virtual int findFlow(int macaddr, int32_t src_ip, int32_t dst_ip, packet_t pkt_type, char dir, int *find);

  void recvHandler(Event *e, Packet *p);

  void sendHandler(Event *e);
  void recv(Packet* p, Handler *h);
  void configure_upstream(int defaultDSChannel, int defaultUSChannel);
  void set_bit(u_char*,int,int);
  void dump_pkt(Packet*);
  char ClassifyDataMgmt(Packet*); 
  int match(Packet*, struct flow_classifier);
  int match(int32_t src_ip, int32_t dst_ip, packet_t ptype, struct flow_classifier cl);
  void setBGMgr(bondingGroupMgr *);

  int bit_on(u_char, int);
  u_int32_t calculate_slots(double,double);
  u_int32_t power(u_int32_t,u_int32_t);
  Packet* AllocPkt(int);  

};
 
/*================= End Abstract Base class declaration===============*/
 

/*=======================CMTS class declaration=======================*/

/*************************************************************************

POINT B
*************************************************************************/
class MacDocsisCMTS : public MacDocsis 
{
  friend class CMTSconfigMgr;
  friend class MapDocsisTimer;
  friend class CmtsUcdDocsisTimer;
  friend class CmtsRngDocsisTimer;
  friend class CmtsSyncDocsisTimer;
  
  //#ifdef RATE_CONTROL //-------------------------------------------------
  friend class CmtsTokenDocsisTimer;
  friend class CmtsSFTokenDocsisTimer;
  //#endif //--------------------------------------------------------------
  
 public:
  CMTSserviceFlowMgr *myDSSFMgr;
  CMTSserviceFlowMgr *myUSSFMgr;
  CMTSconfigMgr *myConfigMgr;
  struct cm_record *CmRecord; /* All the CMs information will be stored*/

//$A805
  resourceOptimizer *myOptimizer;
  
  u_int16_t CurrIndexCmTable;
  
  /* Registration function called by CMs before they start*/
  
  int register_to_cmts(int macaddr, 
		       u_int16_t priority,
		       u_char def_up,
		       u_char def_dn,
		       struct upstream_sflow *UpEntry,
		       u_char UpSize, 
		       struct downstream_sflow *DownEntry, 
		       u_char DownSize);
  
  MacDocsisCMTS();
  ~MacDocsisCMTS();

  int setupChannels(void);
  void CmtsSendHandler(int channelNumber,Packet *p); 
  
 protected:
  int  command(int argc, const char*const* argv);
  
 private:
  /* Timer variables */
  MapDocsisTimer mhMap_;
  CmtsRngDocsisTimer mhRng_;
  CmtsUcdDocsisTimer mhUcd_;
  CmtsSyncDocsisTimer mhSync_;
  
  //#ifdef RATE_CONTROL//------------------------------------
  CmtsTokenDocsisTimer mhToken_;
  tkptr	TokenList;
  //CmtsSFTokenDocsisTimer mhSFToken_; // added
  //#endif//-------------------------------------------------

  struct cm_status_data *CmStatusData; 
  struct cm_status_update_data *CmStatusUpdateData; 
  int numberCMsToUpdate;
  double lastCMUpdateTime;
  int readyToSendUpdate;
  
  /* Configuration parameters */
  struct cmts_conf_param Conf_Table_;
  u_int16_t SizeCmTable;
  
  //$A401
  struct cmts_statistics CMTSstats;
  
//Define the equate for the job arrays
  #define UGS_JOBS 0
  #define RTPS_JOBS 1
  #define POLL_JOBS 2
  #define BE_JOBS 3

  /* Allocation algorithm variables */

  struct job* job_list[4]; /* job_list[0] -> UGS periodic, 
			      job_list[1] -> RT-VBR periodic, 
			      job_list[2] -> RT-VBR non periodic, 
			      job_list[3]-> best effort */ 
  
  /* MAP variables */
  mapptr mptr;         /* Pointer to the head of MAP list */

  double map_stime;
  double omap_stime;
  double map_etime;
  double omap_etime;

  double map_acktime;
  double AckTime; 
  double max_slots_pmap;
  double next_map;

  int numIE;
  int map_lookahead;
  int MAP_LOOKAHEAD; // Configurable from tcl script
  int max_burst_slots;	
  int rem_overhead;
  int MapPropDelay;
  
  /* Simulation output parameters */
  int size_rtqueue; /*Avg size of rtqueue per sec */
  int avg_szrtqueue;
  double last_mrqtime;

  int size_bfqueue;
  int avg_szbfqueue;
  double last_mbfqtime;
  
  int num_bfreq;
  int avg_bfreq;
  double last_mbfreq;
  
  int num_rtreq;
  int avg_rtreq;
  double last_mrtreq;
  
  double last_dmptime;
  
  u_int32_t num_dgrant;     /* Avg num of data grants per MAP */
  u_int32_t num_contention; /* Avg num of contention grants per MAP */
  u_int32_t num_req;        /* Avg num of Unicast req per MAP */
  u_int32_t num_gpend;      /* Avg num of grant pending per MAP */
  
  u_int32_t avg_dgrant;     /* Avg num of data grants per MAP */
  u_int32_t avg_contention; /* Avg num of contention grants per MAP */
  u_int32_t avg_req;        /* Avg num of Unicast req per MAP */
  u_int32_t avg_gpend;      /* Avg num of grant pending per MAP */
  
  /* Tuning parameters */
  double proportion;        /* To control the proportion of allocation
			       between RT-POLL and Best-effort */

  u_char contention_thrhold;/* Indicates the tolerable threshhold
			       for percentage of collisions in 
			       contention slot.If this threshhold
			       is crossed , then number of 
			       contention-slots will be 
			       increased */
  
  u_char network_status;    /* 0 - underutilized, 1 - overloaded */
  
  u_int32_t rtpoll_ddlinemiss;  /* Number of ddline misses per 
				   second of RT_POLL data grants */
  
  u_int32_t beffort_ddlinemiss; /* Number of ddline misses per 
				   second of BEST_EFFORT data grants */
  u_int32_t num_rtslots;
  u_int32_t num_befslots;
  u_int32_t num_adjust_slots;
  
  Event intr;

  /* Events to be consumed on expiration of management messages*/
  Event uintr; 
  Event sintr;
  Event rintr;

  
  static int next_flowid;
  
  
  /* Methods */  
  inline void CmStatusHandler(Event * e) {}
  inline void CmRngHandler(Event * e) {}
  inline void CmSndTimerHandler(Event * e) {}
  inline void CmReqTimerHandler(Event * e) {}
  inline void UnlockQueue() {}

  void CmtsMapHandler(Event *e);
  void CmtsSyncHandler(Event * e);
  void CmtsUcdHandler(Event * e);
  void CmtsRngHandler(Event * e);
  
  //#ifdef RATE_CONTROL//-------------------------------------
  void RateControl(Packet*, int,int,int);
  void RateControl(Packet*, serviceFlowObject*, int,int,int);
  double getupdatedtokens(int,int);
  void CmtsTokenHandler(Event*);
  void CmtsSFTokenHandler(Event*);
  void insert_tokenlist(double,int,int);
  //#endif//-------------------------------------------------
		
  void sendUp(Packet* p);
  void sendDown(Packet* p);
  void RecvFrame(Packet*,int);
  void PassDownstream(Packet*);
  void HandleInData(Packet*,int,int);
  void HandleInMgmt(Packet*);
//$A16
  void HandleStatusMsg(struct status_docsismgmt *);

  void HandleFrag(Packet*);
  void HandleConcat(Packet*,int,int);
  void ReleaseJobs();
  void fill_job(struct job*,char,char,double,u_int32_t,double,u_int16_t);
  void InsertJob(struct job*,int);
  int NumberJobsQueued(struct job*,int);
  void HandleReq(u_char,int,int);
  void tune_parameters();
  void alloc_bw();
  void delete_joblist(int);
  void PhUnsupress(Packet*,int,int);
  void ApplyPhs(Packet*,int,int );
  void MakeAperiodicAlloc(struct job*,mapptr,int);
  void AllocMemCmrecord();

  void AllocCmStatusData();
  void AdjustCMBackOff1(struct cm_status_update_data *latestCMResults,double movingAvgWeight);
  void SendCMUpdateMsg();

  void MakeAllocation(mapptr,double);
  void ReOrder(mapptr);
  void MarkOtherSlots(mapptr,u_int32_t,int);
  void jobdrop(mapptr);
  void MarkGrantPending(u_int32_t*,double);
  void SendMap();
  u_int32_t  MarkUnusedSlots(u_int32_t);
  void find_flowindex(u_int16_t,int*,int*);

  void MacSendFrame(Packet*,int);
  void MacSendFrameOnCompletion(int channelNumber);

  void dump_stats();
  void ParseExtHdr(Packet *);
  void dumpUGSJITTER(char *);
  void UpdateJitter(u_int16_t, double);
  
  //u_char find_size_map();
  
  int find_size_map();
  int NumContSlots(mapptr *);
  int ChkQoSJobs(double,double);
  int  classify(Packet*,char,int*);

  //$A306
//  int findCMTSFlow(int macaddr, int32_t src_ip, int32_t dst_ip, packet_t pkt_type, char dir, int *find);
  //$A306
  virtual int findFlow(int macaddr, int32_t src_ip, int32_t dst_ip, packet_t pkt_type, char dir, int *find);


  int insert_mapjob(int,struct job*,u_int32_t*,int);
  int find_cm(int);
  u_int32_t MarkOtherAlloc(u_int32_t);
  u_int32_t FillMap(int);
  mapptr find_best_hole(u_int32_t);
  mapptr find_next(mapptr);
  mapptr find_prv(mapptr);
  mapptr TryAlloc(double,u_int32_t);
  double determine_deadline(char, int,int);
  double FitMap(mapptr,double);
  double CalculateAckTime();
  
  /* De-bugging functions */
  void print_job_list(int);
  void print_map_list();

  
  void examineJOBS(int debugLevel);
  void examineMAP(int debugLevel,Packet* Pkt);
  
  void print_short_map_list(Packet* Pkt);
  void delete_maplist();
  void dumpBWCMTS(char *fileName);
  void dumpFinalCMTSStats(char *fileName);
  void dumpDOCSISUtilStats(char *fileName, int channelDSBW, int channelUSBW);
  void dumpDOCSISQueueStats(char *fileName);
//   $A910
   int simplePacketTrace(Packet*, serviceFlowObject*, int);
  
};

/*========================End CMTS class declaration====================*/

/*========================CM class declaration==========================*/

/*************************************************************************

POINT C
*************************************************************************/
class MacDocsisCM : public MacDocsis 
{ 
  friend class CMconfigMgr;
  friend class CmRngDocsisTimer;
  friend class CmServiceFlowSendTimer;
  friend class CmServiceFlowRequestTimer;
  friend class CmSeqTimer;
  friend class CmStatusTimer;
  
 public:
  MacDocsisCM();
  ~MacDocsisCM(){};

  CMserviceFlowMgr *myDSSFMgr;
  CMserviceFlowMgr *myUSSFMgr;
  CMconfigMgr *myConfigMgr;
  //$A306
  int setupChannels(void);

  void CmSendHandler(int channelNumber,Packet *p);
  u_char SizeUpFlowTable;
  struct upstream_sflow UpFlowTable[MAX_NUM_UPFLOWS_PERCM];
      
 protected:
  int  command(int argc, const char*const* argv);
 
 private:
  
  RNG* rng_; /* Object for generating random numbers */
    
  /* Timer variables */
  CmStatusTimer mhCmStatus_;
  CmRngDocsisTimer mhCmRng_;
  CmServiceFlowSendTimer mhSend_;
  CmServiceFlowRequestTimer mhReq_; 
  CmSeqTimer mhSeq_; 
  

  Packet* map_;            /* The latest MAP received */
		          
  /* Debug variables */
  char debug_cm;

  u_int32_t bkoffScale; /* for adaptive algorithm */
  u_int32_t avg_bkoffScale;    /* avg scale param for this CM */
  u_int32_t avg_bkoffScale_count;  
  
  u_char my_lan;   /* my_lan stores the lan number which 
		      will be used as index on array cmts_arr 
		      to obtain pointer to cmts object */
  
//$A510
//  u_char SizeUpFlowTable;
//  struct upstream_sflow UpFlowTable[MAX_NUM_UPFLOWS_PERCM];
  double avgCSRatio;
  int avgCSRatioCount;
  double avgTotalSlotUtilization;
  double avgMySlotUtilization;
  double avgChannelAccessDelay;
  double lastStatusUpdateTime;
  
  u_char SizeDownFlowTable;
  struct downstream_sflow DownFlowTable[MAX_NUM_DOWNFLOWS_PERCM];
  
  u_char default_upstream_index_;
  u_char default_dstream_index_;

  u_int16_t priority; /* Used for best-effort flows only*/
  double rng_freq;    /* frequency of RNG-REQ msgs*/
  double status_freq;    /* frequency of status  msgs*/
  //$A401
  struct cm_statistics CMstats;

  double last_StatusTime;            /* Indicates the last time a CM status Msg was sent*/

  int cmts_addr;
  double map_acktime; /*The ack-time in last MAP*/

  sptr SndList; /* Snd timer list header */
  rptr ReqList; /* Req timer list header */
  rptr tempReqList; 
    
  /* 
     Array of function pointers to 
     implement the state machines 
  */
  int(MacDocsisCM::*UGSswitch[UGSSTATES])(char, EventType, Packet*) ;    
  int(MacDocsisCM::*RTPOLLswitch[RTPOLLSTATES])(char, EventType, Packet*);   
  int(MacDocsisCM::*BEFFORTswitch[BEFFORTSTATES])(char, EventType,Packet*);
  
  Event rintr; /* Event to be consumed when CmMgmt timer expires..*/
  Event StatusIntr; /* Event to be consumed when Cm status timer expires..*/
  
  double last_dmptime;
  
  /* Methods */  
  inline void CmtsMapHandler(Event * e) {}
  inline void CmtsSyncHandler(Event * e) {}
  inline void CmtsUcdHandler(Event * e) {}
  inline void CmtsRngHandler(Event * e) {}

  //#ifdef RATE_CONTROL //-----------------------------------
  inline void CmtsTokenHandler(Event *e) {};
  //#endif //------------------------------------------------

  //$A306
  virtual int findFlow(int macaddr, int32_t src_ip, int32_t dst_ip, packet_t pkt_type, char dir, int *find);
  
  void CmStatusHandler(Event * e);
  void CmSeqHandler(Event *e); 
  void CmRngHandler(Event * e);

  void CmSndTimerHandler(Event * e);
  void CmReqTimerHandler(Event * e);

  void sendUp(Packet* p);
  void sendDown(Packet* p);
  void Initialize_entry(char direction, char index);
  void HandleMap(Packet*);
  void HandleOutData(Packet*, char);
  void SendData(Packet*, char);
  int  SendReq(char, Packet*);
  int decide_frag(Packet*,char);
  void send_frag_data(Packet*,char);
  void fill_extended_header(int, Packet*, char);
  int  send_concat(Packet*, char);
  void RecvFrame(Packet*,int);
  void handle_indata(Packet*,char);
  void handle_inmgmt(Packet*,char);
  void SetDefaultFlow();
  void PhUnsupress(Packet*, char);
  void ApplyPhs(Packet*, char);

  void set_default();

  void print_alloclist(char);
  int  grantsInAllocList(char);
  
  int getNumberDataSlots(Packet* Pkt);

  void print_short_map_list(Packet* Pkt);
  void print_classifiers();
  void dump_stats();
  char classify(Packet*,char);
  int check_concat_req(Packet *p,char);
  int check_frag_req(u_int32_t,char);
  int fill_piggyback_req(char);
  void MarkUsedContentionSlots(int,char);
  void turn_off_contention(char);
  void back_off(char,Packet*);
  void insert_sndlist(double,char);
  void insert_reqlist(double,char);

  int size_sndlist(char);
  int size_reqlist(char);

  void reinsert_reqlist(char); 

  void UpdateAllocationTable(char);
  void HandleOtherMgmt(Packet*);
  void insert_alloclist(char,u_int16_t,double,double,u_int16_t,u_int32_t);
  void print_cmalloclist();
  void HandleOutMgmt(Packet*,char);
  int FillPiggyReq(char, Packet*);
  void FillPiggyExtHdr(char,Packet*, int);
  void UpdateJitter(char);
  int CanBeSent(char,Packet*);
  int CanUnicastReqBeSent(char);
  int CanContentionReqBeSent(char);
  int DataGrantPending(char);	
  int MapSentAfterReq(char);
  int NumContentionSlots(char);
  int ugs_idle(char, EventType, Packet*);
  int ugs_decision(char, EventType, Packet*); 
  int ugs_tosend(char, EventType, Packet*); 
  int ugs_waitformap(char, EventType, Packet*);
  int rtpoll_idle(char, EventType, Packet*);
  int rtpoll_decision(char, EventType, Packet*);
  int rtpoll_tosend(char, EventType, Packet*);
  int rtpoll_waitformap(char, EventType, Packet*);
  int rtpoll_tosendreq(char, EventType, Packet*);
  int rtpoll_reqsent(char, EventType, Packet*);
  int beffort_idle(char, EventType, Packet*);
  int beffort_decision(char, EventType, Packet*);
  int beffort_tosend(char, EventType, Packet*);
  int beffort_waitformap(char, EventType, Packet*);
  int beffort_tosendreq(char, EventType, Packet*);
  int beffort_reqsent(char, EventType, Packet*);
  int beffort_contention(char, EventType, Packet*);
  int beffort_ratecheck(char, EventType, Packet*, int slots);
  void USRateMeasure(char, Packet*);
  void us_getupdatedtokens(char);

  
  
  double find_contention_slot(char,u_int32_t);
  double timer_expiration(char,Packet*,int);
  
  //Statistics
  void dumpBWCM(char *fileName);
  void dumpUGSJITTER(char,char *filename);
  void dumpFinalCMStats(char *fileName);
  void dumpDOCSISQueueStats(char *fileName);
  int reqFlag, reqFlagCounter;
  
  int MacSendFrame(Packet*,int channelNumber);
};

/*======================== End CM class declaration========================*/

#endif /* __mac_docsis_h__ */

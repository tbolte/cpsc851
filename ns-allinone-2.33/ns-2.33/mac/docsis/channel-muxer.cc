/***************************************************************************
 * Module: channel-muxer
 * 
 * Explanation:
 *   This file contains the channel muxer object.  This object
 *   abstracts a unidirectional (i.e., half duplex) channel.
 *
 * Methods:
 *
 *
 * Revisions:
 *
************************************************************************/
#include "channel-muxer.h"

//uncomment for printf's
#include "docsisDebug.h"
//#define TRACEME 0


channel_muxer::channel_muxer()
{
}

channel_muxer::channel_muxer(int channelNumber)
{
  channelNumber_ = channelNumber;
}

//$A304
void channel_muxer::setChannelNumber(int channelNumber)
{
  channelNumber_ = channelNumber;
}

void channel_muxer::init()
{
  collision=0;
  ch_direction=CHANNEL_DIRECTION_UP;
		
  rx_state_ = MAC_IDLE;
  tx_state_ = MAC_IDLE;
  tx_active_ = 0;  
  packet_state = 0;  
//  downtarget_=NULL;
  /*
    Statistics
    These next two are used for periodic statistics...
    So beware, they may be reset during a run
  */
  total_num_sent_bytes =0; /* total Num of bytes received  */
  
  /* 
     These two are used at the end of the run
     These should not be reset during a run
  */
util_total_bytes_US = 0; /* Used for end of run util stat*/     
util_total_pkts_US = 0;  /* Used for end of run util stat*/     
util_total_bytes_DS = 0; /* used for end of run util stats */
util_total_pkts_DS = 0;  /* used for end of run util stats */
util_bytes_US=0;         /* used by dumpDOCSISUtilStats*/
util_bytes_DS =0;        /* used by dumpDOCSISUtilStats*/
  /*
    These next two are used for periodic statistics...
    So beware, they may be reset during a run
  */
total_num_rx_pkts =0; /* total Num of packets received  */
total_num_rx_bytes =0; /* total Num of bytes received */
total_num_mgt_pkts_US =0; /* total Num of packets received  */
total_num_status_pkts_US =0; 
total_num_rng_pkts_US =0; 
total_num_concat_pkts_US =0; 
total_num_frag_pkts_US =0; 
total_num_req_pkts_US =0; 
total_num_plaindata_pkts_US = 0;
total_num_concatdata_pkts_US = 0;
total_num_frames_US = 0;
total_num_BE_pkts_US = 0; 
total_num_RTVBR_pkts_US = 0; 
total_num_UGS_pkts_US = 0; 
total_num_OTHER_pkts_US = 0; 
total_num_BW_bytesDOWN =0;
total_num_BW_bytesUP =0;
total_num_appbytesUS = 0;
total_num_appbytesDS = 0;
total_packets_dropped =0; /* total Num of packets dropped */
num_pkts =0;
dropped_dsq=0;    /* total packets dropped at the DS queue*/	
dropped_tokenq =0;

}
int channel_muxer::getDirection()
{
  return(ch_direction);
}
void channel_muxer::setDirectionDS()
{
  ch_direction = CHANNEL_DIRECTION_DOWN;
}
void channel_muxer::setDirectionUS()
{
  ch_direction = CHANNEL_DIRECTION_UP;
}

void channel_muxer::setDirectionDUPLEX()
{
  ch_direction = CHANNEL_DIRECTION_DUPLEX;
}

int channel_muxer::getCollisionState()
{
#ifdef TRACEME
  printf("channel_muxer:getCollisionState:[channel number:%d]:(%lf): current value: %d\n",channelNumber_,Scheduler::instance().clock(),collision);
#endif 
  return(collision);
}

void channel_muxer::setCollisionState() 
{
  collision  = 1;
  numberCollisions++;
#ifdef TRACEME
  printf("channel_muxer:setCollisionState:[channel number:%d]:(%lf): set collision state (count:%d)\n",channelNumber_,Scheduler::instance().clock(),numberCollisions);
#endif 
}

void channel_muxer::clearCollisionState()
{
  collision  = 0;
#ifdef TRACEME
  printf("channel_muxer:clearCollisionState:[channel number:%d]:(%lf): clear collision state\n",channelNumber_,Scheduler::instance().clock());
#endif 
}

int channel_muxer::getTxState()
{
  return tx_state_;
}

void channel_muxer::setTxStateSEND()
{
  tx_state_ = MAC_SEND;
#ifdef TRACEME
  printf("channel_muxer[%d]:(%lf): set to MAC_SEND \n",channelNumber_,Scheduler::instance().clock());
#endif 
}

void channel_muxer::setTxStateIDLE()
{
  tx_state_ = MAC_IDLE;

#ifdef TRACEME
  printf("channel_muxer[%d]:(%lf): set tx_state_ to MAC_IDLE \n",channelNumber_,Scheduler::instance().clock());
#endif 

}
  
int channel_muxer::getRxState()
{
  return rx_state_;
}

void channel_muxer::setRxStateRECV()
{
  rx_state_ = MAC_RECV;
#ifdef TRACEME
  printf("channel_muxer[%d]:(%lf): set rx_state_ to MAC_RECV \n",channelNumber_,Scheduler::instance().clock());
#endif 
}

void channel_muxer::setRxStateIDLE()
{
  rx_state_ = MAC_IDLE;

#ifdef TRACEME
  printf("channel_muxer[%d]:(%lf): set rx_state_ to MAC_IDLE \n",channelNumber_,Scheduler::instance().clock());
#endif 

}
  
int channel_muxer::getNumberCollisions()
{
  return(numberCollisions);
}





/***************************************************************************
 * Module: configMgr
 *
 * Explanation:
 *   This file contains the class definition of the object that
 *   packages config information and config interfaces.
 * 
 * Revisions:
 *  $A801 :  fixes to separate the service flow id from dsid
 *  $A802:   changed tcl interface to configure US/DS SF: srcID dstID PktType flowID
 *
 *
************************************************************************/

#include "globalDefines.h"
#include "configMgr.h"
#include "macPhyInterface.h"
#include "reseqMgr.h"
#include "schedulerObject.h"
#include "channelProperties.h"

#include "docsisDebug.h"
//#define TRACEME 0
#define MACDOCSIS_COMMAND 100

class MacDocsis;
// $A806 Why do we start with 3?
static u_int16_t next_USFlowID = 3;
static u_int16_t next_DSFlowID = 3;
//static u_int16_t next_USFlowID = 0;
//static u_int16_t next_DSFlowID = 0;
//$A801
//start with 2.....dsid 1 is reserved for mgt messages
static u_int16_t next_dsid = 2;


/*************************************************************************
 ************************************************************************/
configMgr::configMgr()
{
#ifdef TRACEME
  printf("configMgr::constructor: \n");
#endif

}

  

void configMgr::init()
{

#ifdef TRACEME 
  printf("configMgr::init: \n");
#endif

}

int configMgr::processCommandLine(int argc, const char *const*argv)
{
int rc = SUCCESS;

#ifdef TRACEME 
  printf("configMgr::processCommandLine \n");
#endif
  return rc;
}

/*************************************************************************
 ************************************************************************/
CMTSconfigMgr::CMTSconfigMgr()
{
#ifdef TRACEME
  printf("CMTSconfigMgr::constructor: \n");
#endif

}

  

void CMTSconfigMgr::init(MacDocsisCMTS *myMacParam) 
{
  myMac = myMacParam;
  debug_config = 0;

#ifdef TRACEME 
  printf("CMTSconfigMgr::init: \n");
#endif

}

int CMTSconfigMgr::processCommandLine(int argc, const char *const*argv)
{
int rc = SUCCESS;

#ifdef TRACEME 
  printf("CMTSconfigMgr::processCommandLine argc %d argv %s\n",argc,argv[1]);
  fflush(stdout);
#endif
  if (argc == 3)
  {
    if (strcmp(argv[1], "set-numcms") == 0)
	{
	    myMac->SizeCmTable = atoi(argv[2]) - 1;
#ifdef TRACEME 
        printf("CMTSconfigMgr::processCommandLine: do set-numcms, SizeCmTable = %d\n",myMac->SizeCmTable);
        fflush(stdout);
#endif
	    myMac->AllocMemCmrecord();
        //allocates and inits the CmStatusData and CmStatusUpdateData
	    myMac->AllocCmStatusData();

	    return TCL_OK;
	}
    else if (strcmp(argv[1],"dump-BW-cmts") == 0) 
	{
	    (void)myMac->dumpBWCMTS((char *)argv[2]);
	    return TCL_OK;
	} 
    else if (strcmp(argv[1],"dump-docsis-queue-stats") == 0) 
	{
	    (void)myMac->dumpDOCSISQueueStats((char *)argv[2]);
	    return TCL_OK;
	}
    else if (strcmp(argv[1],"dump-final-cmts-stats") == 0) 
	{
	    (void)myMac->dumpFinalCMTSStats((char *)argv[2]);
	    return TCL_OK;
	}
  }
  else if(argc == 4)
  {

    if (strcmp(argv[1], "configure-channels")==0)
	{
	  int defaultDSChannel = atoi(argv[2]);
	  int defaultUSChannel = atoi(argv[3]);


#ifdef TRACE //---------------------------------------------------------
	  printf("CMTSconfigMgr::command:  'configure-channels' : call configure_upstream, default US channel:%d \n",
          defaultUSChannel);
#endif //---------------------------------------------------------------
	
      rc = myMac->setupChannels();
      myMac->configure_upstream(defaultDSChannel,defaultUSChannel);
	  return TCL_OK;
    }
  }
  else if (argc == 5)
  {
    if (strcmp(argv[1],"dump-docsis-util-stats") == 0) 
	{
	  (void)myMac->dumpDOCSISUtilStats((char *)argv[2], atoi(argv[3]), atoi(argv[4]));
	  return TCL_OK;
	}
    else if (strcmp(argv[1],"configure-mgmtparams") == 0)
    {
	  myMac->Conf_Table_.mgtparam.sync_msg_interval = atof(argv[2]);
	  myMac->Conf_Table_.mgtparam.rng_msg_interval = atof(argv[3]);
	  myMac->Conf_Table_.mgtparam.ucd_msg_interval = atof(argv[4]);
	  return TCL_OK;
	}
    else if (strcmp(argv[1],"configure-DSScheduler") == 0)
    {
	  myMac->myDSScheduler->configure(atoi(argv[2]), atoi(argv[3]), atoi(argv[4]));
	  return TCL_OK;
	}
    else if (strcmp(argv[1],"configure-USScheduler") == 0)
    {
	  myMac->myUSScheduler->configure(atoi(argv[2]), atoi(argv[3]), atoi(argv[4]));
	  return TCL_OK;
	}
  }
  else if (argc == 12)
  {
    if (strcmp(argv[1],"configure-mapparam") == 0)
	{
	  myMac->Conf_Table_.mapparam.time_covered = atof(argv[2]);
	  myMac->Conf_Table_.mapparam.map_interval = atof(argv[3]);
	  myMac->Conf_Table_.mapparam.num_contention_slots = atoi(argv[4]);
	  myMac->Conf_Table_.mapparam.num_sm_slots = atoi(argv[5]);
	  myMac->Conf_Table_.mapparam.sgrant_limit = atoi(argv[6]);
	  myMac->Conf_Table_.mapparam.lgrant_limit = atoi(argv[7]);
	  myMac->Conf_Table_.mapparam.bkoff_start = atoi(argv[8]);
	  myMac->Conf_Table_.mapparam.bkoff_end = atoi(argv[9]);
	  myMac->proportion = atof(argv[10]);
	  myMac->MAP_LOOKAHEAD = atoi(argv[11]);
	  myMac->max_burst_slots = (int)(myMac->Conf_Table_.mapparam.time_covered / myMac->size_mslots);
	  myMac->max_slots_pmap = myMac->Conf_Table_.mapparam.time_covered / myMac->size_mslots;

#ifdef TRACE_CMTS //--------------------------------------------------------- 
	  printf("Map interval = %lf\n",myMac->Conf_Table_.mapparam.map_interval);
	  printf("Max burst slots = %d\n",myMac->max_burst_slots);
	  printf("max_slots = %lf\n",myMac->max_slots_pmap);
#endif //--------------------------------------------------------------------

	  myMac->next_map = myMac->max_slots_pmap;	  
	  return TCL_OK;
	}
  }
  else if (argc == 2)
  {
    if (strcmp(argv[1],"start") == 0)
	{
      //This command is called for each node created

       struct channelPropertiesType  channelProperties;
       double MAPMsgTxTime =  0;
       double tmp = 0;
       myMac->myPhyInterface->getChannelProperties(&channelProperties,myMac->defaultUSChannel);

//The MAP time must be at least 1 MAP time in the future. It will be
//  1 + ceil((propDelay+MAPtx time)/MAPTime)
//map_etime = Scheduler::instance().clock() + 2*Conf_Table_.mapparam.map_interval;
       myMac->MapPropDelay = 1;
       if (channelProperties.channelCapacity > 0)
         MAPMsgTxTime =  ((SIZE_MGMT_HDR) + (SIZE_MAP_HDR) + 10*4)/channelProperties.channelCapacity;

       tmp = channelProperties.propDelay + MAPMsgTxTime;

#ifdef TRACE_CMTS //--------------------------------------------------------- 
	  printf("configMgr::command(%lf):  'start' : \n", Scheduler::instance().clock());
	  printf("MAPMsgTxTime:%f,channel prop delay::%f,capacity:%lf, map_interval: %f\n",
			   MAPMsgTxTime,channelProperties.propDelay,channelProperties.channelCapacity,myMac->Conf_Table_.mapparam.map_interval);
#endif //--------------------------------------------------------------------
      myMac->MapPropDelay =  (int) (myMac->MapPropDelay +  ceil(tmp/myMac->Conf_Table_.mapparam.map_interval));
      myMac->map_etime = Scheduler::instance().clock() + myMac->MapPropDelay*myMac->Conf_Table_.mapparam.map_interval;
#ifdef TRACE_CMTS //---------------------------------------------------------
	  printf("Starting CMTS.., MapPropDelay: %d, map_etime:%f\n",myMac->MapPropDelay,myMac->map_etime);
#endif //--------------------------------------------------------------------

	  myMac->ReleaseJobs();
	  //myMac->myDSSFMgr->printStats();
	  
	  myMac->mhMap_.start((Packet*) (&myMac->intr),myMac->Conf_Table_.mapparam.map_interval); 
	  double random_delay  = Random::uniform(.01,3);
	  myMac->mhUcd_.start((Packet*) (&myMac->uintr),myMac->Conf_Table_.mgtparam.ucd_msg_interval+random_delay); 
	  
	  /* add some randomness.  This will reduce 
	     the frequency but that's ok*/
	  random_delay  = Random::uniform(.01,3);
	  myMac->mhRng_.start((Packet*) (&myMac->rintr),myMac->Conf_Table_.mgtparam.rng_msg_interval+random_delay); 
	  random_delay  = Random::uniform(.01,3);
	  myMac->mhSync_.start((Packet*) (&myMac->sintr),myMac->Conf_Table_.mgtparam.sync_msg_interval+random_delay); 
	  return TCL_OK;
	}
  }  
  return MACDOCSIS_COMMAND;
}

/*************************************************************************
 ************************************************************************/
CMconfigMgr::CMconfigMgr()
{
#ifdef TRACEME
  printf("CMconfigMgr::constructor: \n");
#endif

}

  

void CMconfigMgr::init(MacDocsisCM *myMacParam) 
{
  myMac = myMacParam;
  debug_config = 1;
//  next_DSFlowID=3;
//  next_USFlowID=3;
#ifdef TRACEME 
  printf("CMconfigMgr::init: \n");
#endif

}

int CMconfigMgr::processCommandLine(int argc, const char *const*argv)
{
int rc = SUCCESS;

#ifdef TRACEME 
  printf("CMconfigMgr::processCommand: entered argc this many: %d,  last arg is :%s\n",argc,argv[1]);
#endif
  char f = 0;
  char k;
  int newFlowID;
  
  if (argc == 30) 
    {
       //SizeUpFlowTable on entry is 0 and on exit will be the number of flows for this CM
      if (strcmp(argv[1], "insert-upflow") == 0) 
	  {

	  myMac->Initialize_entry(0, myMac->SizeUpFlowTable);
	  myMac->UpFlowTable[myMac->SizeUpFlowTable].upstream_record.sched_type = (UpSchedType)atoi(argv[2]);

      newFlowID = next_USFlowID++;
	  myMac->UpFlowTable[myMac->SizeUpFlowTable].upstream_record.flow_id = newFlowID;
	  
	  myMac->UpFlowTable[myMac->SizeUpFlowTable].upstream_record.classifier.src_ip = atoi(argv[3]); 
	  myMac->UpFlowTable[myMac->SizeUpFlowTable].upstream_record.classifier.dst_ip = atoi( argv[4]); 
	  myMac->UpFlowTable[myMac->SizeUpFlowTable].upstream_record.classifier.pkt_type = (packet_t) atoi(argv[5]); 
//$A802
	  myMac->UpFlowTable[myMac->SizeUpFlowTable].upstream_record.classifier.flowID = (packet_t) atoi(argv[6]); 

	  myMac->UpFlowTable[myMac->SizeUpFlowTable].upstream_record.PHS_profile = (PhsType) atoi(argv[7]); 
	  myMac->UpFlowTable[myMac->SizeUpFlowTable].upstream_record.BondingGroup= atoi(argv[8]);

#ifdef TRACEME
        printf("\nCMConfigMgr:command:insert-upflow: flow sched_type:%d, src_ip:%d, dst_ip:%d, pkt_type:%d, flowID:%d, BondingGroup:%d, minLatency:%3.3f \n",
	    myMac->UpFlowTable[myMac->SizeUpFlowTable].upstream_record.sched_type,
        myMac->UpFlowTable[myMac->SizeUpFlowTable].upstream_record.classifier.src_ip,
	    myMac->UpFlowTable[myMac->SizeUpFlowTable].upstream_record.classifier.dst_ip,
        myMac->UpFlowTable[myMac->SizeUpFlowTable].upstream_record.classifier.pkt_type,
        myMac->UpFlowTable[myMac->SizeUpFlowTable].upstream_record.classifier.flowID,
        myMac->UpFlowTable[myMac->SizeUpFlowTable].upstream_record.BondingGroup,
	    atof(argv[27])); 
#endif



	  f = atoi(argv[9]);
	  
	  if (f)
	    myMac->set_bit(&myMac->UpFlowTable[myMac->SizeUpFlowTable].upstream_record.flag, FRAG_ENABLE_BIT,ON);
	  
	  f = atoi(argv[10]);
	  
	  if (f)
	    myMac->set_bit(&myMac->UpFlowTable[myMac->SizeUpFlowTable].upstream_record.flag, CONCAT_ENABLE_BIT,ON);
	  
	  myMac->UpFlowTable[myMac->SizeUpFlowTable].max_concat_threshhold = ((u_int16_t) atoi(argv[11])-1);
	  //If CONCAT_THRESHOLD-1 is <= 0, just turn off CONCATENATION.
	  if (myMac->bit_on(myMac->UpFlowTable[myMac->SizeUpFlowTable].upstream_record.flag,CONCAT_ENABLE_BIT) && 
	      (myMac->UpFlowTable[myMac->SizeUpFlowTable].max_concat_threshhold <= 0))
	    {
	      myMac->set_bit(&myMac->UpFlowTable[myMac->SizeUpFlowTable].upstream_record.flag, CONCAT_ENABLE_BIT,OFF);
//	      printf(" WARNING: TURNING OFF CONCATENATION since  max_concat_threshhold =%d\n",
//	             UpFlowTable[SizeUpFlowTable].max_concat_threshhold);
	      
//	      printf("Incorrect value for max_concat_threshhold specified, exiting\n");
//	      exit(1);
	    }	  
	  f = atoi(argv[12]);
	  
	  if (f)
	    myMac->set_bit(&myMac->UpFlowTable[myMac->SizeUpFlowTable].upstream_record.flag, PIGGY_ENABLE_BIT,ON);
	  
	  myMac->UpFlowTable[myMac->SizeUpFlowTable].upstream_record.gsize = (u_int16_t) atoi(argv[13]); 
	  myMac->UpFlowTable[myMac->SizeUpFlowTable].upstream_record.ginterval = (double) atof(argv[14]); 

      myMac->set_default();

	  myMac->UpFlowTable[myMac->SizeUpFlowTable].packet_list = 0;
	  myMac->UpFlowTable[myMac->SizeUpFlowTable].max_qsize = atoi(argv[16]);
	  myMac->UpFlowTable[myMac->SizeUpFlowTable].debug = atoi(argv[17]);	  	  

	  myMac->UpFlowTable[myMac->SizeUpFlowTable].ratecontrol = (char)(atoi(argv[18]));
	  myMac->UpFlowTable[myMac->SizeUpFlowTable].rate_ = (double) atof(argv[19]);


	  myMac->UpFlowTable[myMac->SizeUpFlowTable].upstream_record.SchedQType = (u_int16_t) atoi(argv[20]);
	  myMac->UpFlowTable[myMac->SizeUpFlowTable].upstream_record.SchedQSize = atoi(argv[21]);
	  myMac->UpFlowTable[myMac->SizeUpFlowTable].upstream_record.SchedServiceDiscipline = atoi(argv[22]);

	  myMac->UpFlowTable[myMac->SizeUpFlowTable].upstream_record.flowPriority = atoi(argv[23]); 

	  myMac->UpFlowTable[myMac->SizeUpFlowTable].upstream_record.flowQuantum = (u_int16_t) atoi(argv[24]); 
	  myMac->UpFlowTable[myMac->SizeUpFlowTable].upstream_record.flowWeight =  atof(argv[25]); 

	  myMac->UpFlowTable[myMac->SizeUpFlowTable].upstream_record.MAXp =  atof(argv[26]); 
	  myMac->UpFlowTable[myMac->SizeUpFlowTable].upstream_record.adaptiveMode =  atoi(argv[27]); 

	  myMac->UpFlowTable[myMac->SizeUpFlowTable].upstream_record.minRate =  atoi(argv[28]); 
	  myMac->UpFlowTable[myMac->SizeUpFlowTable].upstream_record.minLatency =  atof(argv[29]); 
	  


//$A510
      if (myMac->myUSSFMgr != NULL) {

	    if (debug_config)
          printf("CMConfigMgr:command:insert-upflow: About to create a new flow of ID %d, srcIP:%d, quantum:%d, flowWeight:%2.2f SchedQType:%d, SchedQSize:%d\n",
		    newFlowID,
	        myMac->UpFlowTable[myMac->SizeUpFlowTable].upstream_record.classifier.src_ip,
            myMac->UpFlowTable[myMac->SizeUpFlowTable].upstream_record.flowQuantum,
            myMac->UpFlowTable[myMac->SizeUpFlowTable].upstream_record.flowWeight,
            myMac->UpFlowTable[myMac->SizeUpFlowTable].upstream_record.SchedQType,
            myMac->UpFlowTable[myMac->SizeUpFlowTable].upstream_record.SchedQSize);

	    USserviceFlowObject *mySFObj = (USserviceFlowObject *) myMac->myUSSFMgr->addServiceFlow(newFlowID);

	    mySFObj->macaddr = myMac->index_;
	    mySFObj->classifier.src_ip = myMac->UpFlowTable[myMac->SizeUpFlowTable].upstream_record.classifier.src_ip; 
	    mySFObj->classifier.dst_ip = myMac->UpFlowTable[myMac->SizeUpFlowTable].upstream_record.classifier.dst_ip; 
	    mySFObj->classifier.pkt_type = myMac->UpFlowTable[myMac->SizeUpFlowTable].upstream_record.classifier.pkt_type;  
        mySFObj->myBGID = myMac->UpFlowTable[myMac->SizeUpFlowTable].upstream_record.BondingGroup;

        mySFObj->SchedQType = myMac->UpFlowTable[myMac->SizeUpFlowTable].upstream_record.SchedQType;
        mySFObj->SchedQSize = myMac->UpFlowTable[myMac->SizeUpFlowTable].upstream_record.SchedQSize;
        mySFObj->SchedServiceDiscipline = myMac->UpFlowTable[myMac->SizeUpFlowTable].upstream_record.SchedServiceDiscipline;
        mySFObj->flowPriority = myMac->UpFlowTable[myMac->SizeUpFlowTable].upstream_record.flowPriority; 
        mySFObj->minRate = myMac->UpFlowTable[myMac->SizeUpFlowTable].upstream_record.minRate; 
        mySFObj->minLatency = myMac->UpFlowTable[myMac->SizeUpFlowTable].upstream_record.minLatency; 
        mySFObj->reseqFlag = 0; 
        mySFObj->reseqWindow =  0;
        mySFObj->reseqTimeout = 0;

        mySFObj->flowQuantum =  myMac->UpFlowTable[myMac->SizeUpFlowTable].upstream_record.flowQuantum; 
        mySFObj->flowWeight =  myMac->UpFlowTable[myMac->SizeUpFlowTable].upstream_record.flowWeight; 

        mySFObj->MAXp =  myMac->UpFlowTable[myMac->SizeUpFlowTable].upstream_record.MAXp;
        mySFObj->adaptiveMode =  myMac->UpFlowTable[myMac->SizeUpFlowTable].upstream_record.adaptiveMode;

        //Note: RED, ARED, and BAQM will likely overwrite the initial settings
//JJM  1-17-2009  special ...
//        double y = 1;
//        double x = mySFObj->SchedQSize / 4;
        double y = mySFObj->SchedQSize / 10;
        double x = mySFObj->SchedQSize / 2;

        //avoid any tcl proboelms....the adaptiveMode must be 0.
       if (mySFObj->SchedQType == RedQ) 
         mySFObj->setQueueBehavior(mySFObj->SchedQSize, mySFObj->SchedQType, mySFObj->flowPriority, (int)y,(int)x,0,mySFObj->MAXp, DEFAULT_FILTER_TIME_CONSTANT);
       else  if (mySFObj->SchedQType == AdaptiveRedQ) 
         mySFObj->setQueueBehavior(mySFObj->SchedQSize, mySFObj->SchedQType, mySFObj->flowPriority, (int)y,(int)x,1,mySFObj->MAXp, DEFAULT_FILTER_TIME_CONSTANT);
       else
         mySFObj->setQueueBehavior(mySFObj->SchedQSize, mySFObj->SchedQType, mySFObj->flowPriority, (int)y,(int)x,mySFObj->adaptiveMode,mySFObj->MAXp, DEFAULT_FILTER_TIME_CONSTANT);

//	    if (debug_config)
          printf("CMConfigMgr:command:insert-upflow:  Created new US SFObject, newFlowID:%d (total#:%d),  QType/size:%d/%d initial flowQuantum:%d, flowWeight:%2.4f, priority:%d, adaptiveMode:%d \n",
		    newFlowID,myMac->myUSSFMgr->getNumberSFs(), mySFObj->SchedQType, mySFObj->SchedQSize, mySFObj->flowQuantum, mySFObj->flowWeight,mySFObj->flowPriority,mySFObj->adaptiveMode);

//Make sure this is at the end
        myMac->SizeUpFlowTable++;
      }
      else {
           printf("\nCMConfigMgr:command:insert-upflow:  HARD ERROR:   USSFMgr not created !!!!\n");
      }
	  return TCL_OK;
	}
    }
  else if (argc == 2)
    {
      if (strcmp(argv[1],"start") == 0) 
	{

	  if (debug_config)
		printf("CM%d :Starting (debug_config:%d, myMac->debug_cm:%d) \n",myMac->cm_id,debug_config,myMac->debug_cm);
	  
	  myMac->cmts_addr = myMac->cmts_arr[myMac->my_lan]->register_to_cmts(myMac->index_, 
							 myMac->priority,
							 myMac->default_upstream_index_,
							 myMac->default_dstream_index_,
							 myMac->UpFlowTable, 
							 myMac->SizeUpFlowTable, 
							 myMac->DownFlowTable, 
							 myMac->SizeDownFlowTable);
	  myMac->print_classifiers();
	  
	  /* Start the timer to send Ranging requests...*/
	  /* add some randomness.  This will reduce the frequency but that's ok */
	  double random_delay  = Random::uniform(.01,3);
	  myMac->mhCmRng_.start((Packet *) (&myMac->rintr), (myMac->rng_freq+random_delay));	
	  if (debug_config) {
	   printf("CM%d :started CM Rng timer to go off in :%f\n",myMac->cm_id,(random_delay+myMac->rng_freq));
	  }

	  /* Start the timer to send periodic status msgs */
	  random_delay  = Random::uniform(.01,3);
	  myMac->mhCmStatus_.start((Packet *) (&myMac->StatusIntr), (myMac->status_freq+random_delay));	
	  if (debug_config) {
	   printf("CM%d :started CmStatus timer to go off in :%f\n",myMac->cm_id,(random_delay+myMac->status_freq));
	  }
	  
	  return TCL_OK;
	}
    }
  else if (argc == 3) 
    {
      if (strcmp(argv[1],"CMTS") == 0) 
	{
	  return TCL_OK;
	}
      else if (strcmp(argv[1],"dump-BW-cm") == 0) 
	{
	  (void)myMac->dumpBWCM((char *)argv[2]);
	  return TCL_OK;
	}
      else if (strcmp(argv[1],"dump-final-cm-stats") == 0) 
	{
	  (void)myMac->dumpFinalCMStats((char *)argv[2]);
	  return TCL_OK;
	}
      else if (strcmp(argv[1],"dump-jitter-cm") == 0) 
	{
	  return TCL_OK;
	}
      else if (strcmp(argv[1],"dump-docsis-queue-stats") == 0) 
	{
	  if (debug_config)
	    printf("CM%d:dumpDOCSISQueueStats: debug_config flag:%d \n",myMac->cm_id,debug_config);
	  (void)myMac->dumpDOCSISQueueStats((char *)argv[2]);
	  return TCL_OK;
	}
    }
  else if(argc == 4)
  {
    if (strcmp(argv[1], "configure-channels")==0)
	{
	  myMac->defaultDSChannel = atoi(argv[2]);
	  myMac->defaultUSChannel = atoi(argv[3]);
      MacDocsis::num_cms++;
      myMac->cm_id= MacDocsis::num_cms;	  
      sprintf(myMac->docsis_id, "CM%d",myMac->cm_id);
#ifdef TRACE //---------------------------------------------------------
	  printf("CMconfigMgr::command:  'configure-channels(cm_id:%d)' : call configure_upstream, default US channel:%d \n",
          myMac->cm_id,myMac->defaultUSChannel);
#endif //---------------------------------------------------------------
      myMac->setupChannels();
      myMac->configure_upstream(myMac->defaultDSChannel,myMac->defaultUSChannel);
	  return TCL_OK;
    }
  }
  else  if (argc == 5) 
  {
    if (strcmp(argv[1],"dump-docsis-util-stats") == 0) 
	{
//No longer support this
//	  (void)myMac->dumpDOCSISUtilStats((char *)argv[2], atoi(argv[3]), atoi(argv[4]));
	  return TCL_OK;
	}
  }

  else  if(argc == 7) 
  {
    if (strcmp(argv[1],"configure-cm") == 0) 
	{
	  myMac->priority = (u_int16_t) atoi(argv[2]);
	  myMac->rng_freq = atof(argv[3]);
	  myMac->status_freq = atof(argv[4]);
	  debug_config = atoi(argv[5]);
	  myMac->debug_cm = atoi(argv[5]);
	  myMac->bkoffScale = atoi(argv[6]);
	  
	  myMac->avg_bkoffScale = myMac->bkoffScale; 
      myMac->avg_bkoffScale_count = 1;


	  if (debug_config)
	    printf("CM%d:configure-cm: rng_freq:%f,status_freq:%f, bkoffScale:%d \n",myMac->cm_id,myMac->rng_freq,myMac->status_freq,myMac->bkoffScale);
	  return TCL_OK;
	}

  }
  else if (argc == 26 ) 
  {
    if (strcmp(argv[1],"insert-downflow") == 0) 
	{
	  myMac->Initialize_entry(1, myMac->SizeDownFlowTable);

//    For testing....
//	  myMac->DownFlowTable[myMac->SizeDownFlowTable].downstream_record.flow_id = next_DSFlowID++;
//	   next_DSFlowID = 1 + (int)Random::uniform(1,50);

	  myMac->DownFlowTable[myMac->SizeDownFlowTable].downstream_record.flow_id = next_DSFlowID++;
//$A801
	  myMac->DownFlowTable[myMac->SizeDownFlowTable].downstream_record.dsid = next_dsid++;

	  myMac->DownFlowTable[myMac->SizeDownFlowTable].downstream_record.classifier.src_ip =  atoi(argv[2]); 

	  myMac->DownFlowTable[myMac->SizeDownFlowTable].downstream_record.classifier.dst_ip =  atoi(argv[3]); 
	  myMac->DownFlowTable[myMac->SizeDownFlowTable].downstream_record.classifier.pkt_type = (packet_t) atoi(argv[4]); 
//$A802
	  myMac->DownFlowTable[myMac->SizeDownFlowTable].downstream_record.classifier.flowID =  atoi(argv[5]); 

	  myMac->DownFlowTable[myMac->SizeDownFlowTable].downstream_record.PHS_profile = (PhsType) atoi(argv[6]); 
	  myMac->DownFlowTable[myMac->SizeDownFlowTable].downstream_record.BondingGroup = atoi(argv[7]);
	  if (debug_config)
//$A801
        printf("CMconfigMgr::command:insert-downflow: Add this DS flow to DownFlowTable and reseq Mgr: id:%d (total#:%d) (src:%d, dst:%d, type:%d, flowID:%d, BG:%d),  flow id:%d,  dsid:%d \n",
                myMac->index_,myMac->myPhyInterface->myReseqMgr->getNumberSFs(),
	            myMac->DownFlowTable[myMac->SizeDownFlowTable].downstream_record.classifier.src_ip,
                myMac->DownFlowTable[myMac->SizeDownFlowTable].downstream_record.classifier.dst_ip,
	            myMac->DownFlowTable[myMac->SizeDownFlowTable].downstream_record.classifier.pkt_type,
	            myMac->DownFlowTable[myMac->SizeDownFlowTable].downstream_record.classifier.flowID,
	            myMac->DownFlowTable[myMac->SizeDownFlowTable].downstream_record.BondingGroup,
	            myMac->DownFlowTable[myMac->SizeDownFlowTable].downstream_record.flow_id,
	            myMac->DownFlowTable[myMac->SizeDownFlowTable].downstream_record.dsid);

#if 0
      newFlowID = myMac->myPhyInterface->myReseqMgr->addServiceFlow(myMac->index_,
	  myMac->DownFlowTable[myMac->SizeDownFlowTable].downstream_record.classifier.src_ip,
      myMac->DownFlowTable[myMac->SizeDownFlowTable].downstream_record.classifier.dst_ip,
	  myMac->DownFlowTable[myMac->SizeDownFlowTable].downstream_record.classifier.pkt_type,
	  myMac->DownFlowTable[myMac->SizeDownFlowTable].downstream_record.flow_id,
	  myMac->DownFlowTable[myMac->SizeDownFlowTable].downstream_record.BondingGroup);
#endif



	  f = atoi(argv[8]);
	  
	  if (f)
	    myMac->default_dstream_index_ = myMac->SizeDownFlowTable;
	  
	  myMac->DownFlowTable[myMac->SizeDownFlowTable].downstream_record.ratecontrol = (char) atoi(argv[9]); 
	  myMac->DownFlowTable[myMac->SizeDownFlowTable].downstream_record.rate_ = (double) atof(argv[10]); 
	  myMac->DownFlowTable[myMac->SizeDownFlowTable].downstream_record.tokenqlen_ = (int) atoi(argv[11]); 
	  myMac->DownFlowTable[myMac->SizeDownFlowTable].downstream_record.bucket_ = (int) atoi(argv[12]); 

	  myMac->DownFlowTable[myMac->SizeDownFlowTable].downstream_record.SchedQType = (u_int16_t) atoi(argv[13]);
	  myMac->DownFlowTable[myMac->SizeDownFlowTable].downstream_record.SchedQSize = atoi(argv[14]);
	  myMac->DownFlowTable[myMac->SizeDownFlowTable].downstream_record.SchedServiceDiscipline = atoi(argv[15]);

	  myMac->DownFlowTable[myMac->SizeDownFlowTable].downstream_record.flowPriority = atoi(argv[16]); 

	  myMac->DownFlowTable[myMac->SizeDownFlowTable].downstream_record.flowQuantum = (u_int16_t) atoi(argv[17]); 
	  myMac->DownFlowTable[myMac->SizeDownFlowTable].downstream_record.flowWeight = (u_int16_t) atoi(argv[18]); 

	  myMac->DownFlowTable[myMac->SizeDownFlowTable].downstream_record.MAXp =  atof(argv[19]); 
	  myMac->DownFlowTable[myMac->SizeDownFlowTable].downstream_record.adaptiveMode =  atoi(argv[20]); 

	  myMac->DownFlowTable[myMac->SizeDownFlowTable].downstream_record.minRate =  atoi(argv[21]); 
	  myMac->DownFlowTable[myMac->SizeDownFlowTable].downstream_record.minLatency =  atof(argv[22]); 

	  myMac->DownFlowTable[myMac->SizeDownFlowTable].downstream_record.reseqFlag = (u_int16_t) atoi(argv[23]); 
	  myMac->DownFlowTable[myMac->SizeDownFlowTable].downstream_record.reseqWindow = (int) atoi(argv[24]); 
	  myMac->DownFlowTable[myMac->SizeDownFlowTable].downstream_record.reseqTimeout =  (double) atof(argv[25]); 
	  

     int numberDSChannels = myMac->myPhyInterface->getNumberDSChannels();
//     printf("CMconfigMgr: Create ReseqMgr Service Flow (flowID:%d) with numDSChannels: %d\n",
//          myMac->DownFlowTable[myMac->SizeDownFlowTable].downstream_record.flow_id, numberDSChannels);

	  DSserviceFlowObject *mySFObj = (DSserviceFlowObject *) myMac->myPhyInterface->myReseqMgr->addServiceFlow(
          myMac->DownFlowTable[myMac->SizeDownFlowTable].downstream_record.flow_id,numberDSChannels);

//$A801
	  mySFObj->dsid = myMac->DownFlowTable[myMac->SizeDownFlowTable].downstream_record.dsid;

	  mySFObj->macaddr = myMac->index_;
	  mySFObj->classifier.src_ip = myMac->DownFlowTable[myMac->SizeDownFlowTable].downstream_record.classifier.src_ip;
	  mySFObj->classifier.dst_ip = myMac->DownFlowTable[myMac->SizeDownFlowTable].downstream_record.classifier.dst_ip;
	  mySFObj->classifier.pkt_type = myMac->DownFlowTable[myMac->SizeDownFlowTable].downstream_record.classifier.pkt_type;
      mySFObj->myBGID = myMac->DownFlowTable[myMac->SizeDownFlowTable].downstream_record.BondingGroup;
      mySFObj->SchedQType = myMac->DownFlowTable[myMac->SizeDownFlowTable].downstream_record.SchedQType;
      mySFObj->SchedQSize = myMac->DownFlowTable[myMac->SizeDownFlowTable].downstream_record.SchedQSize;
      mySFObj->SchedServiceDiscipline = myMac->DownFlowTable[myMac->SizeDownFlowTable].downstream_record.SchedServiceDiscipline;
      mySFObj->flowPriority = myMac->DownFlowTable[myMac->SizeDownFlowTable].downstream_record.flowPriority;

      mySFObj->flowQuantum = myMac->DownFlowTable[myMac->SizeDownFlowTable].downstream_record.flowQuantum;
      mySFObj->flowWeight = myMac->DownFlowTable[myMac->SizeDownFlowTable].downstream_record.flowWeight;


      mySFObj->minRate = myMac->DownFlowTable[myMac->SizeDownFlowTable].downstream_record.minRate;
      mySFObj->minLatency = myMac->DownFlowTable[myMac->SizeDownFlowTable].downstream_record.minLatency;
      mySFObj->reseqFlag = myMac->DownFlowTable[myMac->SizeDownFlowTable].downstream_record.reseqFlag;
      mySFObj->reseqWindow = myMac->DownFlowTable[myMac->SizeDownFlowTable].downstream_record.reseqWindow;
      mySFObj->reseqTimeout = myMac->DownFlowTable[myMac->SizeDownFlowTable].downstream_record.reseqTimeout;

      mySFObj->setQueueBehavior( 2 * mySFObj->reseqWindow, OrderedQ, 0,0,0,0,0, DEFAULT_FILTER_TIME_CONSTANT);
//           myMac->DownFlowTable[myMac->SizeDownFlowTable].downstream_record.SchedQType, 0,0);

//$A801
//	  if (debug_config)
        printf("mac-docsiscm:command: Created new DS reseq SFObject, newFlowID:%d, dsid:%d, (total#:%d) and queue size:%d\n",
                myMac->DownFlowTable[myMac->SizeDownFlowTable].downstream_record.flow_id, mySFObj->dsid,
                myMac->myPhyInterface->myReseqMgr->getNumberSFs(),2 * mySFObj->reseqWindow);
	  
	  myMac->SizeDownFlowTable++;
	  return TCL_OK;
	}
    }  
  return MACDOCSIS_COMMAND;
}




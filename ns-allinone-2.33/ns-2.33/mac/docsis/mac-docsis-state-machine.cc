/***************************************************************************
 *   This file contains implementation of state machines for the three 
 *   types of service-flows
 *
 *
 * Revisions:
 * $A510 : 
 *     Switched to SFMgr
 *     8/27/2009 : to support AQM that requires channel Idle events
 * $A601 :  modified update void MacDocsisCM::UpdateAllocationTable(char tbindex)
 *         to monitor average access delay, avg channel utilization, and avg
 *        CM consumption and to invoke the service flow channel status method.
 * $A700: 12/27/2009: create CONCATREQ.out to track contention requst Tx's
 * $A701: 1/4/2010: fixing error if the CMTS issues a partial grant and the CM does 
 *         NOT support Fragment. 
 *         Created int MacDocsisCM::grantsInAllocList(char tbindex)
 * $A702
 *
 **********************************************************************/

/*===============================INCLUDE HEADER FILES====================*/

#include "mac-docsis.h"
#include "serviceFlowMgr.h"
#include "docsisDebug.h"

#define WATCH_CMID 1 /*If not 0, we will do special tracing of events/data 
			 for this part. CM*/

#define ACK_SUPPRESSION 0 /* If not 0, we will do ack suppression 
			     for multiple ACKs for a TCP cx that 
			     are waiting for an upstream transmission 
			     opportunity */

#define DUMPCMMAP 1 /* If set to > 0, then we will 
		       dump a MAP to the CMMAP.out file */
#define DUMPCMID 3

#define US_RATE_CONTROL 1

//If defined, adds printf's
//#include "docsisDebug.h"
//#define TRACE_ME 1

//#define FILES_OK 1

//This will dump timing info
//#define TIMINGS 1


/*===========================END INCLUDE HEADER FILES====================*/


/*========================UGS STATE MACHINE FUNCTIONS===================*/

/*************************************************************************
 tbindex denotes the service-flow entry on which packet has been mapped
*************************************************************************/
int MacDocsisCM::ugs_idle(char tbindex, EventType e, Packet* p)
{
  if (UpFlowTable[tbindex].debug)
    printf("CM%d(flow-id %d) UGS_IDLE state \n",
	   cm_id,UpFlowTable[tbindex].upstream_record.flow_id);
  
  switch(e) 
    {
    case PKT_ARRIVAL:
      UpFlowTable[tbindex].state = UGS_DECISION;
      UpFlowTable[tbindex].pkt = p;
      ugs_decision(tbindex, PKT_ARRIVAL, p);
      break;
            
    case MAP_ARRIVAL:
      UpdateAllocationTable(tbindex);
      UpdateJitter(tbindex);
      if (UpFlowTable[tbindex].debug)
	{
	  printf("CM%d(flow-id %d):ugs_idle(%lf): MAP arrived, new state: %d \n", 
	       cm_id,UpFlowTable[tbindex].upstream_record.flow_id, 
	       Scheduler::instance().clock(), UpFlowTable[tbindex].state);
	  printf("CM%d(flow-id %d) Allocation table \n",
		 cm_id,UpFlowTable[tbindex].upstream_record.flow_id);
	  print_alloclist(tbindex);
	}

      //printf("\nAllocation table for flow %d\n",
      //UpFlowTable[tbindex].upstream_record.flow_id);
      //print_alloclist(tbindex);

      break;
      
    default:
      printf("CM%d(flow-id %d)UGS-IDLE state error: Unknown event received\n",
	     cm_id,UpFlowTable[tbindex].upstream_record.flow_id);  
      break;
    }
  return 1;
}

/*************************************************************************

*************************************************************************/	
int MacDocsisCM::ugs_decision(char tbindex, EventType e, Packet* p)
{
  if (UpFlowTable[tbindex].debug)
    printf("CM%d(flow-id %d) UGS_DECISION state \n",
	   cm_id,UpFlowTable[tbindex].upstream_record.flow_id);
  
  switch(e) 
    {
    case PKT_ARRIVAL:
      if (! CanBeSent(tbindex,p))
	UpFlowTable[tbindex].state = UGS_WAITFORMAP;
      else 
	{
	  UpFlowTable[tbindex].state = UGS_TOSEND;
	  ugs_tosend(tbindex, SEND_PKT, p);
	}
      break;
      
    default:
      printf("CM%d(flow-id %d)UGS-DECISION state error: Unknown event received\n",
	     cm_id,UpFlowTable[tbindex].upstream_record.flow_id);
      break;     
    }
  return 1;
}

/*************************************************************************
*
* Function: int MacDocsisCM::ugs_waitformap(char tbindex, EventType e, Packet* p)
*
* Explanation:
*    This is called when certain events occur while a UGS flow is waiting
*    for a MAP.
*
* Inputs:
*
* Outputs:
*
*
*************************************************************************/
int MacDocsisCM::ugs_waitformap(char tbindex, EventType e, Packet* p)
{
  if (UpFlowTable[tbindex].debug)
    printf("CM%d(flow-id %d):ugs_waitformap(%lf) UGS_WAITFORMAP state (tbindex=%d)\n",
	   cm_id,UpFlowTable[tbindex].upstream_record.flow_id,
	   Scheduler::instance().clock(),(int)tbindex);
  
  switch(e) 
    {
    case PKT_ARRIVAL:
      //$A510
      myUSSFMgr->insert_pkt(p,tbindex);
//      insert_pkt(p,tbindex);
      break;
      
    case MAP_ARRIVAL:

      UpdateAllocationTable(tbindex);
      UpdateJitter(tbindex);

      if (UpFlowTable[tbindex].debug) {
        printf("CM%d(flow-id %d):ugs_waitformap(%lf): MAP ARRIVAL \n",
	   cm_id,UpFlowTable[tbindex].upstream_record.flow_id,
	   Scheduler::instance().clock());
        print_alloclist(tbindex);
      }

      if ( CanBeSent(tbindex, p))
	{
	  UpFlowTable[tbindex].state = UGS_TOSEND;
	  ugs_tosend(tbindex, SEND_PKT, p);
	}
      break;
      
    default:
      printf("CM%d(flow-id %d)UGS-WAITFORMAP state error: Unknown event received\n",
	     cm_id,UpFlowTable[tbindex].upstream_record.flow_id);
      break;      
    }
  return 1;
}

/*************************************************************************

*************************************************************************/
int MacDocsisCM::ugs_tosend(char tbindex, EventType e, Packet* p)
{
  double etime, current_time;
  struct hdr_cmn *ch = HDR_CMN(p);
  struct hdr_ip *chip = HDR_IP(p);
  Packet* t;
//$A510
  serviceFlowObject *mySFObj = NULL;
  mySFObj = myUSSFMgr->getServiceFlow(p);
  if (mySFObj == NULL) {
    printf("CM:ugs_tosend(%lf): HARD ERROR (cm_id:%d): Could not find this service flow:  packet of size:%d,  and ptype:%d\n",
     Scheduler::instance().clock(),cm_id,ch->size(),ch->ptype());
//    return;
    exit(1);
  }

  
  current_time = Scheduler::instance().clock();
  if (UpFlowTable[tbindex].debug)
    printf("CM%d(%lf)(flow-id %d) UGS_TOSEND state \n",
	   cm_id,current_time,UpFlowTable[tbindex].upstream_record.flow_id);
  
  switch(e) 
    {
    case SEND_PKT:
      etime = timer_expiration(tbindex, p,DATA_GRANT);
      
      if (etime == -1.0)
	{
	  printf("CM%d(flow-id %d) Timer expiration error: exiting..\n",
		 cm_id,UpFlowTable[tbindex].upstream_record.flow_id);
	  exit(1);
	}
      
      current_time = Scheduler::instance().clock();
      //printf("Starting snd timer..\n");
      mhSend_.start((Packet *) (&UpFlowTable[tbindex].intr), etime );
      insert_sndlist(current_time + etime, tbindex);
      break;
      
    case PKT_ARRIVAL:      
      //$A510
      myUSSFMgr->insert_pkt(p,tbindex);
//      insert_pkt(p,tbindex);
      break;
      
    case MAP_ARRIVAL:
      UpdateAllocationTable(tbindex);
      UpdateJitter(tbindex);

      if (UpFlowTable[tbindex].debug)
         printf("CM%d(%lf)(flow-id %d) UGS_TOSEND MAP \n",
	   cm_id,current_time,UpFlowTable[tbindex].upstream_record.flow_id);
      //printf("\nAllocation table for flow %d\n",
      //UpFlowTable[tbindex].upstream_record.flow_id);
      //print_alloclist(tbindex);

      break;
      
    case SEND_TIMER:
      SendData(p,tbindex);
      UpFlowTable[tbindex].pkt = 0;
      
      //$A510
      if ((myUSSFMgr->len_queue(p,tbindex)) > 0)
//      if ((len_queue(tbindex)) > 0)
	{
	  UpFlowTable[tbindex].state = UGS_DECISION;
      //$A510
      t = myUSSFMgr->deque_pkt(mySFObj);
//	  t = deque_pkt(tbindex);

	  struct hdr_cmn *ch = HDR_CMN(t);

	  UpFlowTable[tbindex].pkt = t;
	  ugs_decision(tbindex, PKT_ARRIVAL, t);
	}	
      else
	UpFlowTable[tbindex].state = UGS_IDLE; 
      break;
            
    default:
      printf("CM%d(flow-id %d) UGS-TOSEND state error: Unknown event received\n",
	     cm_id,UpFlowTable[tbindex].upstream_record.flow_id);
      break;
    }
  return 1;
}

/*======================END UGS STATE MACHINE FUNCTIONS===================*/
				

/*======================RT-POLL STATE MACHINE FUNCTIONS===================*/

/*************************************************************************
tbindex denotes the service-flow entry on which packet has been mapped
*************************************************************************/
int MacDocsisCM::rtpoll_idle(char tbindex, EventType e, Packet* p)
{
  if (UpFlowTable[tbindex].debug)
    printf("CM%d(flow-id %d) RTPOLL_IDLE state, event is %d \n",
	   cm_id,UpFlowTable[tbindex].upstream_record.flow_id,e);
  
  switch(e) 
    {
    case PKT_ARRIVAL:
      UpFlowTable[tbindex].state = RTPOLL_DECISION;
      UpFlowTable[tbindex].pkt = p;
      rtpoll_decision(tbindex, PKT_ARRIVAL, p);
      break;
            
    case MAP_ARRIVAL:
      UpdateAllocationTable(tbindex);

      //printf("\nAllocation table for flow %d\n",
      //UpFlowTable[tbindex].upstream_record.flow_id);
      //print_alloclist(tbindex);

      break;
      
    default:
      printf("CM%d(flow-id %d)RTPOLL-IDLE state error: Unknown event received\n",
	     cm_id,UpFlowTable[tbindex].upstream_record.flow_id);
      break;
    }
  return 1;
}

/*************************************************************************

*************************************************************************/	
int MacDocsisCM::rtpoll_decision(char tbindex, EventType e, Packet* p)
{
  if (UpFlowTable[tbindex].debug)
    printf("CM%d(flow-id %d) RTPOLL_DECISION state, event is %d\n",
	   cm_id,UpFlowTable[tbindex].upstream_record.flow_id,e);
  
  switch(e) 
    {
    case PKT_ARRIVAL:
      if (! CanBeSent(tbindex,p))
	{
	  if (!CanUnicastReqBeSent(tbindex))
	    UpFlowTable[tbindex].state = RTPOLL_WAITFORMAP;
	  else 
	    {
	      UpFlowTable[tbindex].state = RTPOLL_TOSENDREQ;
	      rtpoll_tosendreq(tbindex,SEND_UREQ, p);
	    }
	}
      else 
	{
	  UpFlowTable[tbindex].state = RTPOLL_TOSEND;
	  rtpoll_tosend(tbindex, SEND_PKT, p);
	}
      break;
      
    default:
      printf("CM%d(flow-id %d)RTPOLL-DECISION state error: Unknown event received\n",
	     cm_id,UpFlowTable[tbindex].upstream_record.flow_id);
      break;      
    }
  return 1;
}

/*************************************************************************

*************************************************************************/
int MacDocsisCM::rtpoll_waitformap(char tbindex, EventType e, Packet* p)
{
  if (UpFlowTable[tbindex].debug)
    printf("CM%d(flow-id %d) RTPOLL_WAITFORMAP state, event is %d \n",
	   cm_id,UpFlowTable[tbindex].upstream_record.flow_id,e);
	
  switch(e) 
    {
    case PKT_ARRIVAL:
      //$A510
      myUSSFMgr->insert_pkt(p,tbindex);
//      insert_pkt(p,tbindex);
      break;
      
    case MAP_ARRIVAL:
      UpdateAllocationTable(tbindex);

      //printf("\nAllocation table for flow %d\n",
      //UpFlowTable[tbindex].upstream_record.flow_id);
      //print_alloclist(tbindex);
      
      if ( CanBeSent(tbindex, p))
	{
	  UpFlowTable[tbindex].state = RTPOLL_TOSEND;
	  rtpoll_tosend(tbindex, SEND_PKT, p);
	}
      else if ( CanUnicastReqBeSent(tbindex))
	{
	  UpFlowTable[tbindex].state = RTPOLL_TOSENDREQ;
	  rtpoll_tosendreq(tbindex, SEND_UREQ, p);
	}
      break;
      
    default:
      printf("CM%d(flow-id %d)RTPOLL-WAITFORMAP state error: Unknown event received\n",
	     cm_id,UpFlowTable[tbindex].upstream_record.flow_id);
      break;      
    }
  return 1;
}

/*************************************************************************

*************************************************************************/
int MacDocsisCM::rtpoll_tosend(char tbindex, EventType e, Packet* p)
{
  double etime, current_time;
  Packet* t;
//$A510
  struct hdr_cmn* ch = HDR_CMN(p);
  serviceFlowObject *mySFObj = NULL;
  mySFObj = myUSSFMgr->getServiceFlow(p);
  if (mySFObj == NULL) {
    printf("CM:rtpoll_tosend(%lf): HARD ERROR (cm_id:%d): Could not find this service flow:  packet of size:%d, and ptype:%d\n",
     Scheduler::instance().clock(),cm_id,ch->size(),ch->ptype());
//    return;
    exit(1);
  }

  
  if (UpFlowTable[tbindex].debug)
    printf("CM%d(flow-id %d) RTPOLL_TOSEND state, event is %d \n",
	   cm_id,UpFlowTable[tbindex].upstream_record.flow_id,e);
  
  switch(e) 
    {
    case SEND_PKT:
      /* Function to find out the first data grant big enough 
	 for this packet (indicated by 0) */
      
      etime = timer_expiration(tbindex, p, DATA_GRANT);
      
      if (etime == -1.0)
	{
	  printf("CM%d(flow-id %d) Timer expiration error: exiting..\n",
		 cm_id,UpFlowTable[tbindex].upstream_record.flow_id);
	  exit(1);
	}    
      //printf("Starting snd timer..\n");

      mhSend_.start((Packet *)(&UpFlowTable[tbindex].intr), etime);
      current_time = Scheduler::instance().clock();
      insert_sndlist(current_time + etime, tbindex);
      break;
      
    case PKT_ARRIVAL:
      //$A510
      myUSSFMgr->insert_pkt(p,tbindex);
//      insert_pkt(p,tbindex);
      break;
      
    case MAP_ARRIVAL:
      UpdateAllocationTable(tbindex);

      //printf("\nAllocation table for flow %d\n",
      //UpFlowTable[tbindex].upstream_record.flow_id);
      //print_alloclist(tbindex);

      break;
      
    case SEND_TIMER:
      /* Senddown shud set pkt to NULL  & free the pkt */
      
      //printf("Allocation-table for flow-id %d\n",
      //UpFlowTable[tbindex].upstream_record.flow_id);
      //print_alloclist(tbindex);
      
      SendData(p,tbindex);    
      UpFlowTable[tbindex].pkt = 0;
      
      //$A510
      if ((myUSSFMgr->len_queue(p,tbindex)) > 0)
//      if ((len_queue(tbindex)) > 0)
	{
	  UpFlowTable[tbindex].state = RTPOLL_DECISION;
      //$A510
      t = myUSSFMgr->deque_pkt(mySFObj);
//	  t = deque_pkt(tbindex);
	  UpFlowTable[tbindex].pkt = t;
	  rtpoll_decision(tbindex, PKT_ARRIVAL, t);
	}	
      else
	UpFlowTable[tbindex].state = RTPOLL_IDLE;
      break;
    
    default:
      printf("CM%d(flow-id %d) RTPOLL-TOSEND state error: Unknown event received\n",
	     cm_id,UpFlowTable[tbindex].upstream_record.flow_id);
      break;
    }
  return 1;
}

/*************************************************************************

*************************************************************************/
int MacDocsisCM::rtpoll_tosendreq(char tbindex, EventType e, Packet* p)
{
  double etime,current_time;
  
  if (UpFlowTable[tbindex].debug)
    printf("CM%d(flow-id %d) RTPOLL_TOSENDREQ state, event is:%d \n",
	   cm_id,UpFlowTable[tbindex].upstream_record.flow_id,e);
  
  switch(e) 
    {
    case SEND_UREQ:
      /* Function to find out the first data grant big enough for
	 this packet (indicated by 1) */
      
      etime = timer_expiration(tbindex, p, UREQ_GRANT);
      
      if (etime == -1.0)
	{
	  printf("CM%d(flow-id %d) Timer expiration error: exiting..\n",
		 cm_id,UpFlowTable[tbindex].upstream_record.flow_id);
	  exit(1);
	}
      mhReq_.start((Packet*)(&UpFlowTable[tbindex].intr), etime);
      current_time = Scheduler::instance().clock();
      insert_reqlist(current_time + etime, tbindex);
      break;
      
    case PKT_ARRIVAL:
      //$A510
      myUSSFMgr->insert_pkt(p,tbindex);
//      insert_pkt(p,tbindex);
      break;
      
    case MAP_ARRIVAL:
      UpdateAllocationTable(tbindex);

      //printf("\nAllocation table for flow %d\n",
      //UpFlowTable[tbindex].upstream_record.flow_id);
      //print_alloclist(tbindex);
      
      if (CanBeSent(tbindex, p))
	{
	  printf("CM%d(flow-id %d) Stopping..\n",
		 cm_id,UpFlowTable[tbindex].upstream_record.flow_id);
	  
	  mhReq_.stop((Packet*)(&UpFlowTable[tbindex].intr));
	  UpFlowTable[tbindex].req_time = 999999.0;
	  UpFlowTable[tbindex].state = RTPOLL_TOSEND;
	  rtpoll_tosend(tbindex, SEND_PKT, p);
	}
      break;
      
    case REQ_TIMER:
      SendReq(tbindex, p);
      UpFlowTable[tbindex].state = RTPOLL_REQSENT;
      break;
      
    default:
      printf("CM%d(flow-id %d) RTPOLL-TOSENDREQ state error: Unknown event received\n",
	     cm_id,UpFlowTable[tbindex].upstream_record.flow_id);
      break;
    } 
  return 1;
}

/*************************************************************************

*************************************************************************/
int MacDocsisCM::rtpoll_reqsent(char tbindex, EventType e, Packet* p)
{
  if (UpFlowTable[tbindex].debug)
    printf("CM%d(flow-id %d) RTPOLL_REQSENT state \n",
	   cm_id,UpFlowTable[tbindex].upstream_record.flow_id);
  
  switch(e) 
    {
    case PKT_ARRIVAL:
      //$A510
      myUSSFMgr->insert_pkt(p,tbindex);
//      insert_pkt(p,tbindex);
      break;
      
    case MAP_ARRIVAL:
      UpdateAllocationTable(tbindex);

      //printf("\nAllocation table for flow %d\n",
      //UpFlowTable[tbindex].upstream_record.flow_id);
      //print_alloclist(tbindex);
      
      if ( CanBeSent(tbindex, p))
	{
	  UpFlowTable[tbindex].state = RTPOLL_TOSEND;
	  rtpoll_tosend(tbindex, SEND_PKT, p);
	}
      else if ( !DataGrantPending(tbindex) && (MapSentAfterReq(tbindex)))
	{
	  UpFlowTable[tbindex].state = RTPOLL_DECISION;
	  rtpoll_decision(tbindex, PKT_ARRIVAL, p);
	}
      break;
      
    default:
      printf("CM%d(flow-id %d) RTPOLL-REQSENT state error: Unknown event received\n",
	     cm_id,UpFlowTable[tbindex].upstream_record.flow_id);
      break;
			
    }
  return 1;
}

/*===========================END RT-RTPOLL STATE MACHINE FUNCTIONS===================*/


/*===========================BEST-EFFORT STATE MACHINE FUNCTIONS===================*/

/*************************************************************************
 tbindex denotes the service-flow entry on which packet has been mapped 
*************************************************************************/
int MacDocsisCM::beffort_idle(char tbindex, EventType e, Packet* p)
{

  struct hdr_cmn* ch = NULL;
  serviceFlowObject *mySFObj = NULL;
  if (UpFlowTable[tbindex].debug)
    printf("CM%d(flow-id %d):beffort_idle(%lf): BEFFORT_IDLE state , event:%d \n",
	   cm_id,UpFlowTable[tbindex].upstream_record.flow_id,
	   Scheduler::instance().clock(),e);
  
  switch(e) 
  {

    case PKT_ARRIVAL:

      ch = HDR_CMN(p);
      mySFObj = myUSSFMgr->getServiceFlow(p);

      if (mySFObj == NULL) {
        printf("CM:befort_idle(%lf): HARD ERROR (cm_id:%d): Could not find this service flow:  packet of size:%d,  and ptype:%d\n",
          Scheduler::instance().clock(),cm_id,ch->size(),ch->ptype());
//    return;
        exit(1);
      }

      if (myUSSFMgr->len_queue(p,tbindex) > 0) {
	     printf("CM%d(flow-id %d):beffort_idle(%lf): HARD ERROR PKT_ARRIVAL (size:%d, ptype:%d), we are idle, pkt arrived to NON EMPTY queue (size:%d) \n",
	       cm_id,UpFlowTable[tbindex].upstream_record.flow_id, Scheduler::instance().clock(),ch->size(),ch->ptype(),myUSSFMgr->len_queue(p,tbindex));
//         myUSSFMgr->printShortSummary();
      }

#ifdef TIMINGS
//	printf("1 %lf %d %d 1\n",Scheduler::instance().clock(),
//		UpFlowTable[tbindex].upstream_record.flow_id,cm_id); 
	//This is for the arrival from the outside
        timingsTrace(p,1);
#endif
      if (UpFlowTable[tbindex].debug)
	    printf("CM%d(flow-id %d):beffort_idle(%lf):Packet arrived (size:%d), go to decision state\n",
	       cm_id,UpFlowTable[tbindex].upstream_record.flow_id, 
	       Scheduler::instance().clock(),ch->size());
      
      UpFlowTable[tbindex].state = BEFFORT_DECISION;
      UpFlowTable[tbindex].pkt = p;
      
      if (UpFlowTable[tbindex].debug)
	     printf("CM%d(flow-id %d):beffort_idle(%lf): NRAD 20 RESET enque_time : Packet arrived, go to decision state, SF queue len:%d\n",
	       cm_id,UpFlowTable[tbindex].upstream_record.flow_id, Scheduler::instance().clock(),myUSSFMgr->len_queue(p,tbindex));

//JJM BUG
      /* Since this pkt is not being queued, so update the enque_time variable */
      UpFlowTable[tbindex].enque_time = Scheduler::instance().clock();

//      UpFlowTable[tbindex].queuing_samples++;
      beffort_decision(tbindex, PKT_ARRIVAL, p);
      break;
            
    case MAP_ARRIVAL:

      UpdateAllocationTable(tbindex);
      if (UpFlowTable[tbindex].debug)
	{
	  printf("CM%d(flow-id %d):beffort_idle(%lf): MAP arrived, new state: %d \n", 
	       cm_id,UpFlowTable[tbindex].upstream_record.flow_id, 
	       Scheduler::instance().clock(), UpFlowTable[tbindex].state);
	  printf("CM%d(flow-id %d) Allocation table \n",
		 cm_id,UpFlowTable[tbindex].upstream_record.flow_id);
	  print_alloclist(tbindex);
	}
      break;
      
    default:
      printf("CM%d(flow-id %d)(time:%lf) BEFFORT-IDLE state error:%d Unknown event :%d\n",
	     cm_id,UpFlowTable[tbindex].upstream_record.flow_id,
	       Scheduler::instance().clock(), UpFlowTable[tbindex].state,e);
      break;      
    }
  return 1;
}

/*************************************************************************
MARK1
*************************************************************************/	
int MacDocsisCM::beffort_decision(char tbindex, EventType e, Packet* p)
{
  if (UpFlowTable[tbindex].debug)
    printf("CM%d(flow-id %d):beffort_decision(%lf): entered with event %d \n",
	   cm_id,UpFlowTable[tbindex].upstream_record.flow_id, 
	   Scheduler::instance().clock(),e);
	
  switch(e) 
    {
    case PKT_ARRIVAL:
#ifdef TIMINGS
	    //We only get here from another state...
	    //We enter a new request process
//      printf("2 %lf %d %d 10\n",Scheduler::instance().clock(),
//			UpFlowTable[tbindex].upstream_record.flow_id,cm_id); 
        timingsTrace(p,2);
#endif
      if (! CanBeSent(tbindex,p))
	//If not true then there's no data grant available
	{
	  if (UpFlowTable[tbindex].debug)
	    printf("CM%d (flow-id %d):beffort_decision(%lf): Can NOT BeSent! \n", 
		   cm_id,UpFlowTable[tbindex].upstream_record.flow_id, 
		   Scheduler::instance().clock());
	  
	  if (!CanUnicastReqBeSent(tbindex))
	    {
	      if (UpFlowTable[tbindex].debug)
		printf("CM%d(flow-id %d):beffort_decision(%lf):Unicast Req Can NOT BeSent!\n", 
		       cm_id,UpFlowTable[tbindex].upstream_record.flow_id, 
		       Scheduler::instance().clock());
	      
	      if (DataGrantPending(tbindex))
		{
		  if (UpFlowTable[tbindex].debug)
		    printf("CM%d(flow-id %d):beffort_decision(%lf):DataGrant is pending... goto REQSENT state\n", 
			   cm_id,UpFlowTable[tbindex].upstream_record.flow_id, 
			   Scheduler::instance().clock());
		  
		  turn_off_contention(tbindex);
		  UpFlowTable[tbindex].state = BEFFORT_REQSENT;
		}
	      else if (CanContentionReqBeSent(tbindex))
		{
		  UpFlowTable[tbindex].state = BEFFORT_CONTENTION;
		  
		  if (!UpFlowTable[tbindex].contention_on)
		    beffort_contention(tbindex, CONTENTION_ON, p);
		  else
		    /* 
		       Contention is on and reached decision state, 
		       that implies that back-off has occured 
		    */
		    beffort_contention(tbindex, CONTENTION_BKOFF, p);		  
		  
		  if (UpFlowTable[tbindex].debug)
		    printf("CM%d(flow-id %d):beffort_decision(%lf):ContentionRequ COULD be sent:updated state:%d\n", 
			   cm_id,UpFlowTable[tbindex].upstream_record.flow_id, 
			   Scheduler::instance().clock(), UpFlowTable[tbindex].state);		  
		}
	      else 
		{
		  UpFlowTable[tbindex].state = BEFFORT_WAITFORMAP;
		}
	      
	      if (UpFlowTable[tbindex].debug)
		printf("CM%d(flow-id %d):beffort_decision(%lf):Unicast req COULD NOT be sent:updated state:%d\n", 
		       cm_id,UpFlowTable[tbindex].upstream_record.flow_id, 
		       Scheduler::instance().clock(), UpFlowTable[tbindex].state);
	    }
	  else 
	    {
	      UpFlowTable[tbindex].state = BEFFORT_TOSENDREQ;
	      turn_off_contention(tbindex);
	      beffort_tosendreq(tbindex,SEND_UREQ, p);
	      
	      if (UpFlowTable[tbindex].debug)
		printf("CM%d(flow-id %d):beffort_decision(%lf):Unicast req CAN be sent:updated state:%d \n", 
		       cm_id,UpFlowTable[tbindex].upstream_record.flow_id, 
		       Scheduler::instance().clock(), UpFlowTable[tbindex].state);
	    }
	}
      else 
	{
	  turn_off_contention(tbindex);
	  UpFlowTable[tbindex].state = BEFFORT_TOSEND;
	  beffort_tosend(tbindex, SEND_PKT, p);
	}
      break;
      
    default:
      printf("CM%d(flow-id %d) BEFFORT-DECISION state error: Unknown event: %d\n",
	     cm_id,UpFlowTable[tbindex].upstream_record.flow_id,e);
      break;      
    }
  return 1;
}

/*************************************************************************

*************************************************************************/
int MacDocsisCM::beffort_waitformap(char tbindex, EventType e, Packet* p)
{
  if (UpFlowTable[tbindex].debug)
    printf("CM%d(flow-id %d):beffort_waitformap(%lf): entered with event %d \n", 
	   cm_id,UpFlowTable[tbindex].upstream_record.flow_id, 
	   Scheduler::instance().clock(),e);

  switch(e) 
    {
    case PKT_ARRIVAL:
#ifdef TIMINGS
//      printf("1 %lf %d %d 3\n",Scheduler::instance().clock(),
//			UpFlowTable[tbindex].upstream_record.flow_id,cm_id); 
      timingsTrace(p,1);
#endif
      //$A510
      myUSSFMgr->insert_pkt(p,tbindex);
//      insert_pkt(p,tbindex);
      break;
      
    case MAP_ARRIVAL:
      UpdateAllocationTable(tbindex);
      
      if (UpFlowTable[tbindex].debug)
	{
	  printf("CM%d(flow-id %d) Allocation table \n",
		 cm_id,UpFlowTable[tbindex].upstream_record.flow_id);
	  print_alloclist(tbindex);
	}
      
      if ( CanBeSent(tbindex, p))
	{
	  turn_off_contention(tbindex);
	  UpFlowTable[tbindex].state = BEFFORT_TOSEND;
	  beffort_tosend(tbindex, SEND_PKT, p);
	}
      else if ( CanUnicastReqBeSent(tbindex))
	{
	  turn_off_contention(tbindex);
	  UpFlowTable[tbindex].state = BEFFORT_TOSENDREQ;
	  beffort_tosendreq(tbindex, SEND_UREQ, p);
	}
    else if ( CanContentionReqBeSent(tbindex))
	{
	  UpFlowTable[tbindex].state = BEFFORT_CONTENTION;

	  if (UpFlowTable[tbindex].contention_on)
	    beffort_contention(tbindex, CONTENTION_SLOTS, p);
	  else
	    beffort_contention(tbindex, CONTENTION_ON, p);	  	  
	}
    else if (DataGrantPending(tbindex))
	{
	  turn_off_contention(tbindex);
	  UpFlowTable[tbindex].state = BEFFORT_REQSENT;
	}
//JJM BUG what if we get a MAP but no grant or DG pending ?
//In this case ?
    else {
      printf("CM%d(flow-id %d):beffort_waitformap(%lf): HARD ERROR WAITFORMAP:  MAP arrived but no grant  \n", 
	   cm_id,UpFlowTable[tbindex].upstream_record.flow_id, 
	   Scheduler::instance().clock());
    }
    break;
      
    default:
      printf("CM%d(flow-id %d) BEFFORT-WAITFORMAP state error: Unknown event: %d\n",
	     cm_id,UpFlowTable[tbindex].upstream_record.flow_id,e);
      break;
      
    }
  return 1;
}

/*************************************************************************

*************************************************************************/
int MacDocsisCM::beffort_tosend(char tbindex, EventType e, Packet* p)
{
  double etime, current_time;
  Packet* t;


  if (UpFlowTable[tbindex].debug)
    printf("CM%d(flow-id %d):beffort_tosend(%lf): event :%d \n", 
	   cm_id,UpFlowTable[tbindex].upstream_record.flow_id, 
	   Scheduler::instance().clock(),e);
  
//$A510
  struct hdr_cmn* ch = HDR_CMN(p);
  serviceFlowObject *mySFObj = NULL;
  mySFObj = myUSSFMgr->getServiceFlow(p);
  if (mySFObj == NULL) {
    printf("CM:befort_tosend(%lf): HARD ERROR (cm_id:%d): Could not find this service flow:  packet of size:%d,  and ptype:%d\n",
     Scheduler::instance().clock(),cm_id,ch->size(),ch->ptype());
//    return;
    exit(1);
  }

  
  switch(e) 
    {
    case SEND_PKT:
      /* Function to find out the first data grant big enough 
	 for this packet (indicated by 0) */
      
      etime = timer_expiration(tbindex, p, DATA_GRANT);
      
      if (etime == -1.0)
	{
	  printf("CM%d(flow-id %d) Timer expiration error: exiting..\n",
		 cm_id,UpFlowTable[tbindex].upstream_record.flow_id);
	  exit(1);
	}
      
      if (UpFlowTable[tbindex].debug)
	printf("CM%d(flow-id %d)(time:%lf)Starting snd timer, size sndlist: %d, curr_gsize=%d, etime:%lf\n", 
	       cm_id,UpFlowTable[tbindex].upstream_record.flow_id, Scheduler::instance().clock(),
                          size_sndlist(tbindex), UpFlowTable[tbindex].curr_gsize,etime);
      
      //printf("etime = %lf\n",etime);

      mhSend_.start((Packet *)(&UpFlowTable[tbindex].intr), etime );
      
      current_time = Scheduler::instance().clock();
      insert_sndlist(current_time + etime, tbindex);
      break;
      
    case PKT_ARRIVAL:
#ifdef TIMINGS
//      printf("1 %lf %d %d 4\n",Scheduler::instance().clock(),
//			UpFlowTable[tbindex].upstream_record.flow_id,cm_id); 
      timingsTrace(p,1);
#endif
      //$A510
      myUSSFMgr->insert_pkt(p,tbindex);
//      insert_pkt(p,tbindex);
      break;
      
    case MAP_ARRIVAL:
      UpdateAllocationTable(tbindex);
      
      if (UpFlowTable[tbindex].debug)
	{
	  printf("CM%d(flow-id %d):beffort_tosend(%lf): MAP arrival\n", 
		 cm_id,UpFlowTable[tbindex].upstream_record.flow_id, 
		 Scheduler::instance().clock());
	  print_alloclist(tbindex);
	}
      break;
      
    case SEND_TIMER:
      /* Senddown shud set pkt to NULL  & free the pkt */

#ifdef TIMINGS
//	printf("4 %lf %d %d 1\n",Scheduler::instance().clock(),
//			UpFlowTable[tbindex].upstream_record.flow_id,cm_id); 
        timingsTrace(p,4);
#endif
      SendData(p,tbindex);
      
      if (!bit_on(UpFlowTable[tbindex].upstream_record.flag,FRAG_ON_BIT) && 
	  ((!bit_on(UpFlowTable[tbindex].upstream_record.flag,PIGGY_ENABLE_BIT)) || 
	   ((bit_on(UpFlowTable[tbindex].upstream_record.flag,PIGGY_ENABLE_BIT)) && 
	    (bit_on(UpFlowTable[tbindex].upstream_record.flag,PIGGY_NOT_SEND)))))
	
	{
	  /* This 'if' will be entered only if frag is off 
	     and piggyback request has not been sent */
	  
	  UpFlowTable[tbindex].pkt = 0;
	  
      //$A510
      if ((myUSSFMgr->len_queue(p,tbindex)) > 0)
//	  if ((len_queue(tbindex)) > 0) 
	    {
	      UpFlowTable[tbindex].state = BEFFORT_DECISION;
          //$A510
          t =  myUSSFMgr->deque_pkt(mySFObj);
//	      t = deque_pkt(tbindex);
	      UpFlowTable[tbindex].pkt = t;
#ifdef TIMINGS
//We will log this next in BEFFORT_DECI
//	printf("2 %lf %d %d 1\n",Scheduler::instance().clock(),
//			UpFlowTable[tbindex].upstream_record.flow_id,cm_id); 
//        timingsTrace(t,2);
#endif
              if (UpFlowTable[tbindex].debug) {
                 printf("CM%d(flow-id %d):BEFORT-TOSEND(%lf): NRAD CASE 2 and 6, reset enque_time \n",
	               cm_id,UpFlowTable[tbindex].upstream_record.flow_id,
	               Scheduler::instance().clock());
              }
              UpFlowTable[tbindex].enque_time = Scheduler::instance().clock();

	      beffort_decision(tbindex, PKT_ARRIVAL, t);
	    }	
	  else
	    UpFlowTable[tbindex].state = BEFFORT_IDLE;
	  
	  /* Turn-off this bit, as the info has been used */
	  set_bit(&UpFlowTable[tbindex].upstream_record.flag, PIGGY_NOT_SEND,OFF);	  
	}
      break;
      
    default:
      printf("CM%d(flow-id %d) BEFFORT-TOSEND state error: Unknown event: %d\n",
	     cm_id,UpFlowTable[tbindex].upstream_record.flow_id,e);
      break;
    }
  return 1;
}

/***************************************************************************
 * method:  int MacDocsisCM::beffort_tosendreq(char tbindex, EventType e, Packet* p)
 * params:
 *  char tbindex:
 *  EventType e:
 *  Packet *p :
 *
 * returns:  returns a 1 for success
 *
 * description: The CM is waiting for a REQ Timer so it can send the request.
 ***************************************************************************/
int MacDocsisCM::beffort_tosendreq(char tbindex, EventType e, Packet* p)
{
  double etime,current_time;
  
  if (UpFlowTable[tbindex].debug)
    printf("CM%d(flow-id %d) :beffort_tosendreq(%lf):  Entered with event : %d \n",
	   cm_id,UpFlowTable[tbindex].upstream_record.flow_id, 
	   Scheduler::instance().clock(),e);

  switch(e) 
    {
    case SEND_UREQ:
      /* Function to find out the first Unicast req grant for
	 this flow (indicated by 1) */
      
      etime = timer_expiration(tbindex, p, UREQ_GRANT);
      
      if (etime == -1.0)
	{
	  printf("CM%d(flow-id %d) Timer expiration error: exiting..\n",
		 cm_id,UpFlowTable[tbindex].upstream_record.flow_id);
	  exit(1);
	}      

      if (UpFlowTable[tbindex].debug)
	printf("CM%d(flow-id %d)(time:%lf)Starting req timer, size reqlist: %d,etime:%lf\n", 
	       cm_id,UpFlowTable[tbindex].upstream_record.flow_id, Scheduler::instance().clock(),
                          size_reqlist(tbindex), etime);
      
      mhReq_.start((Packet*)(&UpFlowTable[tbindex].intr), etime);
      current_time = Scheduler::instance().clock();
      insert_reqlist(current_time + etime, tbindex);


      break;
      
    case PKT_ARRIVAL:
#ifdef TIMINGS
//      printf("1 %lf %d %d 5\n",Scheduler::instance().clock(),
//			UpFlowTable[tbindex].upstream_record.flow_id,cm_id); 
      timingsTrace(p,1);
#endif
      //$A510
      myUSSFMgr->insert_pkt(p,tbindex);
//      insert_pkt(p,tbindex);
      break;
      
    case MAP_ARRIVAL:
      UpdateAllocationTable(tbindex);
      
      if (UpFlowTable[tbindex].debug)
	{
	  printf("CM%d(flow-id %d):beffort_tosendreq(%lf): MAP ARRIVAL: allocation table:\n", 
		 cm_id,UpFlowTable[tbindex].upstream_record.flow_id, 
		 Scheduler::instance().clock());
	  print_alloclist(tbindex);
	}
      
      if (CanBeSent(tbindex, p))
	{
	  mhReq_.stop((Packet*)(&UpFlowTable[tbindex].intr));
	  UpFlowTable[tbindex].req_time = 999999.0;
	  UpFlowTable[tbindex].state = BEFFORT_TOSEND;
	  beffort_tosend(tbindex, SEND_PKT, p);
	}
      break;
      
    case REQ_TIMER:
                     
      //On error, reinsert request and assume a collision
      if(!SendReq(tbindex, p)){
          reinsert_reqlist(tbindex);

         if (UpFlowTable[tbindex].debug)
           printf("CM%d(flow-id %d) :beffort_tosendreq(%lf): HARD ERROR  SendReq returned immediate failure, num_retries:%d,  bk_offwin:%d \n",
             cm_id,UpFlowTable[tbindex].upstream_record.flow_id,Scheduler::instance().clock(),UpFlowTable[tbindex].num_retries, UpFlowTable[tbindex].bk_offwin);
                           
         if(UpFlowTable[tbindex].num_retries == 1){
            turn_off_contention(tbindex);
            UpFlowTable[tbindex].bk_offwin = UpFlowTable[tbindex].bk_offstart;

         if (UpFlowTable[tbindex].debug)
           printf("CM%d(flow-id %d) :beffort_tosendreq(%lf): SendReq returned immediate failure,  bk_offwin:%d \n",
             cm_id,UpFlowTable[tbindex].upstream_record.flow_id,Scheduler::instance().clock(), UpFlowTable[tbindex].bk_offwin);
         }
         else {
           CMstats.total_num_collisions--;
           CMstats.total_collisions_status--;

           UpFlowTable[tbindex].bk_offwin = UpFlowTable[tbindex].bk_offwin / 2;
           UpFlowTable[tbindex].num_retries--;


         if (UpFlowTable[tbindex].debug)
           printf("CM%d(flow-id %d) :beffort_tosendreq(%lf), reduce count,  total_num_collisions: %d, num_retries:%d, bk_offwin:%d\n",
	   cm_id,UpFlowTable[tbindex].upstream_record.flow_id, 
	   Scheduler::instance().clock(),CMstats.total_num_collisions,
	   UpFlowTable[tbindex].num_retries, UpFlowTable[tbindex].bk_offwin);
         } 
      } 
      
      //      SendReq(tbindex, p);
      UpFlowTable[tbindex].req_time = Scheduler::instance().clock();
      UpFlowTable[tbindex].state = BEFFORT_REQSENT;
      break;
      
    default:
      //A7
      printf("CM%d(flow-id %d) BEFFORT-TOSENDREQ state error: Unknown event: %d\n",
	     cm_id,UpFlowTable[tbindex].upstream_record.flow_id,e);
      break;
    }
  return 1;
}

			
/*************************************************************************

*************************************************************************/			
int MacDocsisCM::beffort_reqsent(char tbindex, EventType e, Packet* p)
{
  if (UpFlowTable[tbindex].debug)
    printf("CM%d(flow-id %d) BEFFORT_REQSENT state , event: %d\n",
	   cm_id,UpFlowTable[tbindex].upstream_record.flow_id,e);
	
  switch(e) 
    {
    case PKT_ARRIVAL:
#ifdef TIMINGS
//      printf("1 %lf %d %d 6\n",Scheduler::instance().clock(),
//			UpFlowTable[tbindex].upstream_record.flow_id,cm_id); 
      timingsTrace(p,1);
#endif
      //$A510
      myUSSFMgr->insert_pkt(p,tbindex);
//      insert_pkt(p,tbindex);
      break;
      
    case MAP_ARRIVAL:
      UpdateAllocationTable(tbindex);
      
      if(UpFlowTable[tbindex].debug)
	  {
	    printf("CM%d(flow-id %d)Allocation table \n",
		 cm_id,UpFlowTable[tbindex].upstream_record.flow_id);
	    print_alloclist(tbindex);
	  }
      
      if(CanBeSent(tbindex, p))
	  {
      //If frag is OFF and grant is less than what we asked for, just exit
      //$A701
      // Check if frag is not enabled and the CMTS allocated a partial grant
      // if reqLen is large it might represent a concat request. Send_concat
      // will reduce the number of packets to be sent to just 1.  
      if (!(bit_on(UpFlowTable[tbindex].upstream_record.flag,FRAG_ENABLE_BIT))) {
        struct hdr_cmn* ch = HDR_CMN(p);
        int dataGrants = grantsInAllocList(tbindex);
        int grantBytes = dataGrants  * bytes_pminislot;
        if (grantBytes <  ch->size()) {
//        if (UpFlowTable[tbindex].curr_gsize <  UpFlowTable[tbindex].curr_reqsize) 
          printf("CM%d(flow-id %d):beffort_reqsent:(%lf)  HARD ERROR PARTIAL GRANT BUT FRAG NOT ON: data grants in bytes:%d (in slots:%d),  reqLen:%d, pkt size:%d \n",
           cm_id, UpFlowTable[tbindex].upstream_record.flow_id,Scheduler::instance().clock(),
           grantBytes,dataGrants, UpFlowTable[tbindex].curr_reqsize,ch->size());
          //We don't want to drop the pkt.....
       
//	      UpFlowTable[tbindex].state = BEFFORT_DECISION;
//          UpFlowTable[tbindex].enque_time = Scheduler::instance().clock();
//	      beffort_decision(tbindex, PKT_ARRIVAL, p);
//          UpFlowTable[tbindex].total_drops_fragmentation++;
//          return 1;
        }
      }
	  UpFlowTable[tbindex].state = BEFFORT_TOSEND;
	  turn_off_contention(tbindex);
	  beffort_tosend(tbindex, SEND_PKT, p);
	}
    else if ((UpFlowTable[tbindex].contention_on) &&
	       ( !DataGrantPending(tbindex)) && 
	       (MapSentAfterReq(tbindex)))
	{
	  /* Request lost in collision */
	  back_off(tbindex,p);
	}
    else if ((UpFlowTable[tbindex].contention_on) &&
	       ( DataGrantPending(tbindex)) && 
	       (MapSentAfterReq(tbindex)))
	{
	  /* Contention request succesfully sent */
      // what state to go to ????
	  turn_off_contention(tbindex);
	}
    else if ((!UpFlowTable[tbindex].contention_on) &&
	       ( !DataGrantPending(tbindex)) && 
	       (MapSentAfterReq(tbindex)))
	{
	  if (UpFlowTable[tbindex].debug) {
	    printf("CM%d(flow-id %d) Request denied at %lf\n", 
		   cm_id,UpFlowTable[tbindex].upstream_record.flow_id,
		   Scheduler::instance().clock());
          }
	  
          UpFlowTable[tbindex].total_num_req_denied++;
	  /* Data grant denied by CMTS */
	  UpFlowTable[tbindex].state = BEFFORT_DECISION;
          if (UpFlowTable[tbindex].debug) {
                printf("CM%d(flow-id %d):BEFORT-REQSEND(%lf): NRAD CASE 6-1, reset enque_time \n",
	               cm_id,UpFlowTable[tbindex].upstream_record.flow_id,
	               Scheduler::instance().clock());
          }
          UpFlowTable[tbindex].enque_time = Scheduler::instance().clock();
	  beffort_decision(tbindex, PKT_ARRIVAL, p);
	}	      
      break;
      
    default:
      printf("CM%d(flow-id %d)(time:%lf) BEFFORT-REQSENT state error:%d Unknown event :%d\n",
	     cm_id,UpFlowTable[tbindex].upstream_record.flow_id,
	       Scheduler::instance().clock(), UpFlowTable[tbindex].state,e);
      break;      
    }
  return 1;
}


/****************************************************************************
 * Method: int MacDocsisCM::beffort_contention(char tbindex, EventType e, Packet* p)
 *
 * Function:   
 *
 * Parameters:
 *    char tbindex:
 *    EventType e:  Indicates the event that caused this routine to be invoked:
 *      beffort_contention(tbindex, CONTENTION_ON, p);
 *                 contention just detected 
 *      beffort_contention(tbindex, CONTENTION_BKOFF, p);		  
 *                 already in contention so this is a backoff
 *      beffort_contention(tbindex, CONTENTION_SLOTS, p);
 *                 We've started contention, we are in wait for MAP state and then
 *                 we get a MAP.  
 *    Packet *p:  The frame that already has been adjusted for all headers
 *
 * Design Notes:
 *
 * **********************************************************************/
int MacDocsisCM::beffort_contention(char tbindex, EventType e, Packet* p)
{
  int ran, n;
  double etime, current_time;
  double x=0;
  
//JJM BUG COLLISIONS
  if (UpFlowTable[tbindex].debug)
    printf("CM%d(flow-id %d):beffort_contention(%lf):  Entered with event %d, total number of collisions:%d \n",
	   cm_id,UpFlowTable[tbindex].upstream_record.flow_id,
	   Scheduler::instance().clock(),e, CMstats.total_num_collisions);
  
  switch(e) 
    {
    case CONTENTION_ON:
//        ran = bkoffScale * rng_->uniform((int)UpFlowTable[tbindex].bk_offstart); 
      ran = rng_->uniform((int)(bkoffScale * (int)UpFlowTable[tbindex].bk_offstart));
      if (UpFlowTable[tbindex].debug)
           printf("CM%d(flow-id %d):beffort_contention1(%lf): BKOFF: bk_offstart:%d,  ran=%d (bkoffScale=%d))\n",
	     cm_id,UpFlowTable[tbindex].upstream_record.flow_id, Scheduler::instance().clock(),
	     (int) UpFlowTable[tbindex].bk_offstart, ran,bkoffScale);
      
      if (UpFlowTable[tbindex].debug)
	    printf("CM%d(flow-id %d):beffort_contention:Entering contention phase,will skip %d slots(bk_offstart:%d)\n",
	       cm_id,UpFlowTable[tbindex].upstream_record.flow_id,
	       ran,UpFlowTable[tbindex].bk_offstart);

      /* Set the internal bkoff-window to bkoff-start*/
      UpFlowTable[tbindex].bk_offwin = UpFlowTable[tbindex].bk_offstart;
      
      //UpFlowTable[tbindex].bk_offwin = ran + size_ureqgrant;
      /* Skip ran*size_ureqgrant opportunity */
      UpFlowTable[tbindex].bk_offcounter = ran * size_ureqgrant;
      
      /* Look for slots to skip + sufficient slots to send a req */
      UpFlowTable[tbindex].bk_offcounter += size_ureqgrant;
            
      UpFlowTable[tbindex].contention_on = 1;
      n = NumContentionSlots(tbindex);
      
      if (UpFlowTable[tbindex].debug)
        printf("CM%d(flow-id %d):beffort_contention(%lf): (1) found %d contention slots, bk_offcounter is  %d\n", 
	       cm_id,UpFlowTable[tbindex].upstream_record.flow_id, 
	       Scheduler::instance().clock(),n, UpFlowTable[tbindex].bk_offcounter);
      
      /*If this is true, then the random number of slots we are to 
	skip exceeds the number of slots in this map.*/
      if (UpFlowTable[tbindex].bk_offcounter > n) 
	  {
	    MarkUsedContentionSlots(n,tbindex);
	  UpFlowTable[tbindex].bk_offcounter -= n;
	  
	  /* Its important to make sure that after skipping the slots, 
	     enough number of slots are left capable of sending req frame.. */
	  
	  if (UpFlowTable[tbindex].bk_offcounter < size_ureqgrant)
	    UpFlowTable[tbindex].bk_offcounter = size_ureqgrant;
	  
	  UpFlowTable[tbindex].state = BEFFORT_WAITFORMAP;
	  
	  if (UpFlowTable[tbindex].debug)
	    printf("CM%d(flow-id %d):beffort_contention(%lf):Decided to wait for another MAP\n", 
		   cm_id,UpFlowTable[tbindex].upstream_record.flow_id,
		   Scheduler::instance().clock());
	  }
      else
	  {
	    u_int32_t n_slots;
	  
	  /* 
	     We have already skipped more than neccessary slots , 
	     but there were not sufficient slots to send req last time, 
	     so find the first contention tx opportunity you get 
	  */
	  
	  if (UpFlowTable[tbindex].bk_offcounter == size_ureqgrant)
	    n_slots = 1; 
	  else
	    /* 
	       e.g skip 13 slots, size_ureqgrant is 2..
	       Now when we reach here we have already skipped 11..
	       So, bkoff_couner is 15-11=4...
	       So, we have to find the begining of  4 - 2 +1  i.e starting of 14nt slot..
	    */
	    n_slots = UpFlowTable[tbindex].bk_offcounter - size_ureqgrant + 1;

	  etime = find_contention_slot(tbindex,n_slots);
	  
	  if (etime < 0.0)
	    {
	      printf("CM%d(flow-id %d) Timer expiration error: exiting..\n",
		     cm_id,UpFlowTable[tbindex].upstream_record.flow_id);
	      exit(1);
	    }
//$A702 ?
	  mhReq_.start((Packet*)(&UpFlowTable[tbindex].intr), etime);
	  current_time = Scheduler::instance().clock();
	  insert_reqlist(current_time + etime, tbindex);
	  UpFlowTable[tbindex].num_retries++;
	  UpFlowTable[tbindex].state = BEFFORT_TOSENDREQ;
	
          UpFlowTable[tbindex].req_time = Scheduler::instance().clock();


	  if (UpFlowTable[tbindex].debug)
	    printf("CM%d(flow-id %d):beffort_contention(%lf):Found etime of %lf,move to TOSENDREQ\n", 
		   cm_id,UpFlowTable[tbindex].upstream_record.flow_id, Scheduler::instance().clock(),etime);
	}
      break;
						
    case CONTENTION_SLOTS:
      /* 
	 NumContentionSlots will return number of 'unused' 
	 contention slots available in the allocation table 
      */
      
      n = NumContentionSlots(tbindex);
      if (UpFlowTable[tbindex].debug)
	printf("CM%d(flow-id %d):beffort_contention:  Found %d contention slots. \n",
	       cm_id,UpFlowTable[tbindex].upstream_record.flow_id,n);
      
      if ( UpFlowTable[tbindex].bk_offcounter > n)  
	{
	  MarkUsedContentionSlots(n,tbindex);
	  UpFlowTable[tbindex].bk_offcounter -= n;
	  
	  if (UpFlowTable[tbindex].bk_offcounter < size_ureqgrant)
	    UpFlowTable[tbindex].bk_offcounter = size_ureqgrant;
	
	  UpFlowTable[tbindex].state = BEFFORT_WAITFORMAP;
	}
      else
	{
	  u_int32_t n_slots;
	  /* 
	     We have already skipped more than neccessary slots, 
	     but there were not sufficient slots to send req last time, 
	     so find the first contention tx opportunity you get 
	  */
	  if (UpFlowTable[tbindex].bk_offcounter == size_ureqgrant)
	    n_slots = 1; 
	  else
	    /* 
	       e.g skip 13 slots, size_ureqgrant is 2..
	       Now when we reach here we have already skipped 11..
	       So, bkoff_couner is 15-11=4...
	       So, we have to find the begining of  4 - 2 +1  
	       i.e starting of 14nt slot..
	    */
	    n_slots = UpFlowTable[tbindex].bk_offcounter - size_ureqgrant + 1;
	  
	  etime = find_contention_slot(tbindex,n_slots);
	  
	  if (etime == -1.0)
	    {
	      printf("CM%d(flow-id %d) Timer expiration error: exiting..\n",
		     cm_id,UpFlowTable[tbindex].upstream_record.flow_id);
	      exit(1);
	    }
	  mhReq_.start((Packet*)(&UpFlowTable[tbindex].intr), etime);
	  current_time = Scheduler::instance().clock();
	  insert_reqlist(current_time + etime, tbindex);
	  UpFlowTable[tbindex].num_retries++;
	  UpFlowTable[tbindex].state = BEFFORT_TOSENDREQ;
	}      
      break;
      
    case CONTENTION_BKOFF:
//      ran = bkoffScale * rng_->uniform((int)UpFlowTable[tbindex].bk_offwin);
      ran = rng_->uniform((int) (bkoffScale * (int)UpFlowTable[tbindex].bk_offwin));
      if (UpFlowTable[tbindex].debug)
         printf("CM%d(flow-id %d):beffort_contention3(%lf): BKOFF: bk_offstart:%d,  ran=%d (bkoffScale=%d))\n",
	     cm_id,UpFlowTable[tbindex].upstream_record.flow_id, Scheduler::instance().clock(),
	     (int) UpFlowTable[tbindex].bk_offstart, ran,bkoffScale);

//$A702
     if (UpFlowTable[tbindex].num_retries == 1) /* First back-off */
	 {
	   UpFlowTable[tbindex].avg_fcont += ran;
	   UpFlowTable[tbindex].fcont_count++;
       //let's get the sample from the last incident


	   if (UpFlowTable[tbindex].avg_BackoffTmpCount >0) {
         x =  UpFlowTable[tbindex].avg_BackoffTmp / UpFlowTable[tbindex].avg_BackoffTmpCount;
	     UpFlowTable[tbindex].avg_Backoff += (u_int32_t)x;
	     UpFlowTable[tbindex].avg_BackoffCount++;
       }
	   UpFlowTable[tbindex].avg_BackoffTmp =0;
	   UpFlowTable[tbindex].avg_BackoffTmpCount =0;

       if (UpFlowTable[tbindex].debug)
         printf("CM%d(flow-id %d):beffort_contention: First backed-off,will skip %d  (avg:%d)\n",
	       cm_id,UpFlowTable[tbindex].upstream_record.flow_id,ran, UpFlowTable[tbindex].avg_fcont / UpFlowTable[tbindex].fcont_count);
	 }
	 UpFlowTable[tbindex].avg_TotalBackoff += ran;
	 UpFlowTable[tbindex].avg_TotalBackoffCount++;
	 UpFlowTable[tbindex].avg_BackoffTmp += ran;
	 UpFlowTable[tbindex].avg_BackoffTmpCount++;
      
    if (UpFlowTable[tbindex].debug)
	  printf("CM%d(flow-id %d):beffort_contention: Backed-off,will skip %d slots (total avg_TotalBackoff:%d, avg:%d, x:%f, avg fcont:%d)\n",
	       cm_id,UpFlowTable[tbindex].upstream_record.flow_id,ran, UpFlowTable[tbindex].avg_TotalBackoff,
	       UpFlowTable[tbindex].avg_TotalBackoff / UpFlowTable[tbindex].avg_TotalBackoffCount,x,
	       UpFlowTable[tbindex].avg_fcont / UpFlowTable[tbindex].fcont_count);
      
      
      //UpFlowTable[tbindex].bk_offwin = ran + size_ureqgrant;
      /* Skip ran*size_ureqgrant opportunity */
      UpFlowTable[tbindex].bk_offcounter = ran * size_ureqgrant;
      
      /*  Look for slots to skip + sufficient slots to send a req */
      UpFlowTable[tbindex].bk_offcounter += size_ureqgrant;
      

      n = NumContentionSlots(tbindex);
      
      if (UpFlowTable[tbindex].debug)
        printf("CM%d(flow-id %d):beffort_contention(%lf): (2) found %d contention slots, bk_offcounter is  %d\n", 
	       cm_id,UpFlowTable[tbindex].upstream_record.flow_id,
	       Scheduler::instance().clock(),n, UpFlowTable[tbindex].bk_offcounter);
      
    if (UpFlowTable[tbindex].bk_offcounter > n) 
	{
	  MarkUsedContentionSlots(n,tbindex);
	  UpFlowTable[tbindex].bk_offcounter -= n;
	  
	  /*
	    Its important to make sure that after skipping the slots, 
	    enough number of slots are left capable of sending req frame..
	  */
	  
	  if (UpFlowTable[tbindex].bk_offcounter < size_ureqgrant)
	    UpFlowTable[tbindex].bk_offcounter = size_ureqgrant;
	  
	  UpFlowTable[tbindex].state = BEFFORT_WAITFORMAP;
	  
	  if (UpFlowTable[tbindex].debug)
	    printf("CM%d(flow-id %d):beffort_contention(%lf):Decided to wait for another MAP\n", 
		   cm_id,UpFlowTable[tbindex].upstream_record.flow_id, Scheduler::instance().clock());
	}
    else
	{
	  u_int32_t n_slots;
	  /* 
	     We have already skipped more than neccessary slots, 
	     but there were not sufficient slots to send req last time, 
	     so find the first contention tx opportunity you get 
	  */
	  if (UpFlowTable[tbindex].bk_offcounter == size_ureqgrant)
	    n_slots = 1; 
	  else
	    /* e.g skip 13 slots, size_ureqgrant is 2..
	       Now when we reach here we have already skipped 11..
	       So, bkoff_couner is 15-11=4...
	       So, we have to find the begining of  4 - 2 +1  
	       i.e starting of 14nt slot..
	    */
	    n_slots = UpFlowTable[tbindex].bk_offcounter - size_ureqgrant + 1;
	  
	  etime = find_contention_slot(tbindex,n_slots);
	  
	  if (etime < 0.0)
	    {
	      printf("CM%d(flow-id %d) Timer expiration error: exiting..\n",
		     cm_id,UpFlowTable[tbindex].upstream_record.flow_id);
	      exit(1);
	    }
	  
	  mhReq_.start((Packet*)(&UpFlowTable[tbindex].intr), etime);
	  current_time = Scheduler::instance().clock();
	  insert_reqlist(current_time + etime, tbindex);
	  UpFlowTable[tbindex].num_retries++;
	  UpFlowTable[tbindex].state = BEFFORT_TOSENDREQ;
	  
	  if (UpFlowTable[tbindex].debug)
	    printf("CM%d(flow-id %d):beffort_contention(%lf):Found etime of %lf, move to TOSENDREQ \n",
		   cm_id, UpFlowTable[tbindex].upstream_record.flow_id, 
		   Scheduler::instance().clock(),etime);
	}
    break;
      
    default:
      printf("CM%d(flow-id %d) BEFFORT-REQSENT state error: Unknown event:%d\n",
	     cm_id,UpFlowTable[tbindex].upstream_record.flow_id,e);
      break;
    }
  return 1;
}

/*************************************************************************
 * Routine: int MacDocsisCM::beffort_ratecheck(char tbindex, EventType e, Packet* p)
 *
 * Explanation: This is called before sending a request for bandwidth 
 *   (including a piggy req) to see if the rate control has provided enough
 *   tokens.  The dshdr_.mac_param contains the number of slots in the request
 *   for the case of a REQ_TIMER. Normally this is 1 or 6 for a piggy request.
 *   For the case of a PIGGY_REQ, the exthrd_[].eh_data[1] is set to # slots requested.
 *
 * Inputs:
 *   tbindex: 
 *   EventType e : Specifies the asynch event that led to this invocation: 
 *                REQ_TIMER, PIGGYBACK_REQ 
 *   Packet* p
 *   slots : number of slots rquested
 *
 * Outputs:
 *   Returns int returnval_a 
 *     If 1, then can send
 *     If -1, then can NOT send
 *
*************************************************************************/
#ifdef US_RATE_CONTROL
int MacDocsisCM::beffort_ratecheck(char tbindex, EventType e, Packet* p,int slots)
{
struct hdr_cmn *ch = HDR_CMN(p);
struct hdr_docsisextd* chext = HDR_DOCSISEXTD(p);
struct hdr_docsis *dh = HDR_DOCSIS(p);
double tokens_available = 0.0;
int returnval_a = 0;
int num_slots_req = slots;

if (UpFlowTable[tbindex].debug) {
   printf("CM%d(flow-id %d):beffort_ratecheck(%lf): ENTRY UpFlowTable: numberPacketsInNextConcat_:%d, num_slots_req:%d, dshdr mac_param:%d, UpFlowTable num_slots_req:%d\n",
   cm_id, UpFlowTable[tbindex].upstream_record.flow_id,Scheduler::instance().clock(),
   UpFlowTable[tbindex].numberPacketsInNextConcat_,num_slots_req, dh->dshdr_.mac_param,UpFlowTable[tbindex].num_slots_req);
}

switch(e) {
	case REQ_TIMER:
	       if (UpFlowTable[tbindex].debug) {
	         
           printf("CM%d(flow-id %d):beffort_ratecheck(%lf):REQTIMER: alloc_list:num_slots%d,Table:num_slots_req:%d, Table:prev_acceptance_rate:%f, wt: %f \n",
		   cm_id, UpFlowTable[tbindex].upstream_record.flow_id, 
		   Scheduler::instance().clock(),
		   UpFlowTable[tbindex].alloc_list->num_slots, num_slots_req,UpFlowTable[tbindex].prev_acceptance_rate,UpFlowTable[tbindex].wt_factor);
           }

               if(num_slots_req == 0){
                   printf("beffort_ratecheck: ERROR:REQ_TIMER: num_slots_req : %d\n",UpFlowTable[tbindex].num_slots_req);
                   returnval_a = 1;
                   goto error_return;
               }
	       if (bit_on(UpFlowTable[tbindex].upstream_record.flag,FRAG_ON_BIT)) {
	       if (UpFlowTable[tbindex].debug) {
	         printf("CM%d(flow-id %d):beffort_ratecheck(%lf):REQ_TIMER: FRAG IS ON!!\n",
		   cm_id, UpFlowTable[tbindex].upstream_record.flow_id, 
		   Scheduler::instance().clock());
               }
	       }
		UpFlowTable[tbindex].acceptance_rate = (UpFlowTable[tbindex].alloc_list->num_slots / num_slots_req) * (1 - UpFlowTable[tbindex].wt_factor) + UpFlowTable[tbindex].prev_acceptance_rate * UpFlowTable[tbindex].wt_factor;
                UpFlowTable[tbindex].prev_acceptance_rate = UpFlowTable[tbindex].acceptance_rate;

                us_getupdatedtokens(tbindex);
                tokens_available = UpFlowTable[tbindex].tokens_ + (0.002 * UpFlowTable[tbindex].rate_);


	       if (UpFlowTable[tbindex].debug) {
	         printf("CM%d(flow-id %d):beffort_ratecheck(%lf):REQTIMER: updated tokens_available:%f, updated acceptance rate::%f\n",
		   cm_id, UpFlowTable[tbindex].upstream_record.flow_id, 
		   Scheduler::instance().clock(),tokens_available,UpFlowTable[tbindex].prev_acceptance_rate);
               }



                if(tokens_available >= UpFlowTable[tbindex].acceptance_rate * num_slots_req * bytes_pminislot * 8) 
			returnval_a = 1;
                else 
                        returnval_a = -1; 

//	        UpFlowTable[tbindex].num_slots_req = dh->dshdr_.mac_param; 	


	        if (UpFlowTable[tbindex].debug) {
	         printf("CM%d(flow-id %d):beffort_ratecheck(%lf):REQTIMER: tokens_available:%f, newRate:%f, num_slots_req:%d\n",
		   cm_id, UpFlowTable[tbindex].upstream_record.flow_id, 
		   Scheduler::instance().clock(),tokens_available,UpFlowTable[tbindex].acceptance_rate,
		   num_slots_req);
               }

                break;


	default:
         returnval_a =  -1;
         printf("CM%d(flow-id %d) beffort_ratecheck(%lf):  state error: Unknown event:%d\n",
	     cm_id,UpFlowTable[tbindex].upstream_record.flow_id, Scheduler::instance().clock(), e);
         break;
       }

error_return: 

if (returnval_a == 1) {
  UpFlowTable[tbindex].num_slots_req = num_slots_req;
}

if (UpFlowTable[tbindex].debug) {
  printf("CM%d(flow-id %d) beffort_ratecheck(%lf): EXIT returning : %d, adjusted UpFlowTable.num_slots_req:%d\n",
    cm_id,UpFlowTable[tbindex].upstream_record.flow_id, Scheduler::instance().clock(), returnval_a,UpFlowTable[tbindex].num_slots_req);
}
return returnval_a;


}
#endif


/*===========================END BEST-EFFORT STATE MACHINE FUNCTIONS==============*/

/*************************************************************************
* Routine:
*
* Explanation:
*   This function updates the allocation table for a service-flow when a MAP 
*    is received 
*
* Inputs:
*  The MAP is pointed to by the global ptr map_
*
* Outputs:
*
*  Called when a MAP is received from any of the states.  
*      The map_ points to  the packet
*************************************************************************/			
void MacDocsisCM::UpdateAllocationTable(char tbindex)
{
double current_time = Scheduler::instance().clock();
FILE* Sfp = NULL;
size_t len = 0;
char *linePtr = NULL;
ssize_t readCount;
int rc = 0;
int Filecm_id;
double timestamp;
int scaleParam;

int numberSlotsForThisFlow = 0;
int numberTotalDataSlots = 0;

  /* Update the data grant pending flag..*/
  

  if (map_ == NULL) {
    printf("CM%d :updateAllocationTable(%lf): Entered but MAP is null ??\n",
           cm_id,current_time);
    return;
  }
  struct hdr_docsismap  *cm  = HDR_DOCSISMAP(map_);
  u_int32_t * data = (u_int32_t *)map_->accessdata();
  
  if (debug_cm)
    printf("CM%d :updateAllocationTable(%lf): Entered !! tbindex:%d, acktime:%f, bkoff start:%d \n",
           cm_id,current_time,tbindex,cm->acktime_(),cm->bkoff_start_());

  u_int16_t flow_id, offset,iuc,offsetn;
  u_int32_t map_len,tmp,num_slots;
  int i = 1,count = 0;
  double alloc_stime,alloc_etime;
    
  UpFlowTable[tbindex].pending = 0;
  map_acktime = cm->acktime_();

  /* Update Data bkoff-start and bkoff-end window */
  if (cm->bkoff_start_() == 0x0ff) {
//JJM HI  fix hi priority
    if (debug_cm)
      printf("CM%d :updateAllocationTable(%lf): RECEIVED the update scale command in the MAP\n",
              cm_id,current_time);
    UpFlowTable[tbindex].bk_offstart = power(2,3);
    UpFlowTable[tbindex].bk_offend = power(2,cm->bkoff_end_());

    //Read the file SCALEPARAM.dat

    Sfp = fopen("SCALEPARAM.dat", "r+");
    //read each line, 
    while ( (readCount = getline(&linePtr, &len, Sfp) != -1)){
      rc = scanf(linePtr,"%d %f %d",&Filecm_id,&timestamp,&scaleParam);
      if (debug_cm)
        printf("CM%d :updateAllocationTable(%lf): SCALEPARAM:Filecm_id:%d %s\n",
              cm_id,current_time,Filecm_id,linePtr);
      if (Filecm_id == cm_id  ) {
//JJM HI  fix hi priority
        if (debug_cm)
          printf("CM%d :updateAllocationTable(%lf): FOUND OUR LINE, scaleParam:%d\n",
              cm_id,current_time,scaleParam);
      }
    }

    if (linePtr)
     free(linePtr);

    fclose(Sfp);

  }
  else { 
    UpFlowTable[tbindex].bk_offstart = power(2,cm->bkoff_start_());
    UpFlowTable[tbindex].bk_offend = power(2,cm->bkoff_end_());
//JJM HI  fix hi priority
//Uncomment the following to force CM 3 to use a small backoff rang
    if (cm_id == 3) {
//      UpFlowTable[tbindex].bk_offstart = power(2,3);
//      UpFlowTable[tbindex].bk_offend = power(2,4);
    }
  }
  
  /* Process all IEs..*/
  flow_id = 0;
  
//JJM HI  fix hi priority
  if (debug_cm)
    printf("CM%d :updateAllocationTable(%lf): bk_offstart:%d, bk_offend:%d \n",
           cm_id,Scheduler::instance().clock(),
           UpFlowTable[tbindex].bk_offstart, UpFlowTable[tbindex].bk_offend);

  map_len = calculate_slots(cm->allocstarttime_(),cm->allocendtime_());
  tmp = 0;
  
  if ( DUMPCMMAP == 1) 
    {
      if (DUMPCMID  == cm_id)
	print_short_map_list(map_);
    }

//$A510
//  if (debug_cm)
//    printf("CM%d:updateAllocationTable(%lf): Entered !! tbindex:%d, acktime:%f, bkoff start:%xd, #slots:%d \n",
//           cm_id,current_time,tbindex,cm->acktime_(),cm->bkoff_start_(),map_len);
    int numCSs = 0;

  numberTotalDataSlots = getNumberDataSlots(map_);
  
  while (i < cm->numIE_())
  {
      flow_id = *data | flow_id;
      
      if ((flow_id == 0) || (flow_id == 0xfff0)) 
	{
	  /* flow-id = 0 -> NULL IE, flow-id = 0xfff0->Unused slot */
	  i++;
	  data++;
	  flow_id = 0;
	  continue; // NULL IE
	}
      flow_id = flow_id << 2;
      flow_id = flow_id >> 2;
      
      if ((flow_id == UpFlowTable[tbindex].upstream_record.flow_id) || (flow_id == BROADCAST_SID))
	{
	  /* calculate the offset */
	  
	  offset = (*data) >> 18;
	  
	  if (offset == map_len) /* Grant pending IE */
	    UpFlowTable[tbindex].pending = 1;
	  else
	    {
	      offsetn = (*(data+1))>>18;
	      
	      /* Calculate the start-time..*/
	      num_slots = offsetn - offset;


	      //alloc_stime = cm->allocstarttime_() + (offset * size_mslots)/(10*10*10*10*10*10);
	      alloc_stime = cm->allocstarttime_() + (offset * size_mslots);



	      //alloc_etime = alloc_stime + (num_slots * size_mslots)/(10*10*10*10*10*10);
	      alloc_etime = alloc_stime + (num_slots * size_mslots);
	      
	      tmp = (*data) & 49152;
	      iuc = tmp >> 14;
          

          if (iuc == 0) { 
            numberSlotsForThisFlow+=num_slots;
          }
//$A510
//If the assignment is for contention
          if (iuc == 2) { 
            numCSs+= num_slots;
          }
          if (current_time > alloc_stime) {
            printf("CM%d :updateAllocationTable(%lf): TROUBLE: MAP GOT HERE LATE: stime:%lf, etime:%lf \n",
               cm_id,Scheduler::instance().clock(), alloc_stime, alloc_etime);
          }
//Should we notinsert it in the list?
	      
	      insert_alloclist(tbindex,flow_id,alloc_stime,alloc_etime,iuc,num_slots);
	      count++;
          if (debug_cm)
            printf("CM%d :insert_alloclist(%lf): flow id:%d , grant in slots:%d, iuc:%d\n",
                   cm_id,Scheduler::instance().clock(),flow_id,num_slots, iuc); 
	    }
	}
      i++;
      data++;
      flow_id = 0;
      tmp = 0;
  }  
  UpFlowTable[tbindex].avg_slotspermap += count;
  UpFlowTable[tbindex].avg_slotspermap /= 2;

  if (debug_cm) 
        printf("CM%d:updateAllocationTable(%lf):(tbindex:%d) map_len:%d, numberSlotsForThisFlow:%d, numberTotalDataSlots:%d \n",
           cm_id,current_time,tbindex,map_len, numberSlotsForThisFlow,numberTotalDataSlots);

  //$A601
  //averages with the most recent samples
  double weightQ = .5;
  double ratioMySlots = (double)numberSlotsForThisFlow / (double)map_len;
  double ratioTotalSlots = (double)numberTotalDataSlots / (double)map_len;
  double curTime =   Scheduler::instance().clock();

//$A601
//  if ( UpFlowTable[tbindex].num_delay_samples > 0)
//    channelAccessDelaySample = UpFlowTable[tbindex].avg_req_stime / (double)UpFlowTable[tbindex].num_delay_samples;
//  if ( UpFlowTable[tbindex].channelDelayCountSamples > 0)
//   channelAccessDelaySample = UpFlowTable[tbindex].channelDelayCount / (double)UpFlowTable[tbindex].channelDelayCountSamples;


  avgTotalSlotUtilization = (1-weightQ)*avgTotalSlotUtilization + weightQ*ratioTotalSlots;
  avgMySlotUtilization = (1-weightQ)*avgMySlotUtilization + weightQ*ratioMySlots;
  if (curTime - lastStatusUpdateTime > 0.10) {
    if (CMstats.channelDelayCountSamples > 0) {
      double channelAccessDelaySample = 0;
      channelAccessDelaySample = CMstats.channelDelayCount / (double)CMstats.channelDelayCountSamples;
      avgChannelAccessDelay = (1-weightQ)*avgChannelAccessDelay+ weightQ*channelAccessDelaySample;
      lastStatusUpdateTime = curTime;
      myUSSFMgr->channelStatus(avgChannelAccessDelay,avgTotalSlotUtilization,avgMySlotUtilization);
  if (debug_cm) {
        printf("CM%d:updateAllocationTable(%lf):(tbindex:%d) map_len:%d, numberSlotsForThisFlow:%d, numberTotalDataSlots:%d \n",
           cm_id,current_time,tbindex,map_len, numberSlotsForThisFlow,numberTotalDataSlots);
        printf("CM%d:updateAllocationTable(%lf): channelAccessDelaySample:%3.6f (avg_req_stime:%3.6f, numberSamples:%d) \n",
//           cm_id,current_time,channelAccessDelaySample, UpFlowTable[tbindex].avg_req_stime,UpFlowTable[tbindex].num_delay_samples);
//           cm_id,current_time,channelAccessDelaySample, UpFlowTable[tbindex].channelDelayCount,UpFlowTable[tbindex].channelDelayCountSamples);
             cm_id,current_time,channelAccessDelaySample, CMstats.channelDelayCount,CMstats.channelDelayCountSamples);
  }
      CMstats.channelDelayCount=0;
      CMstats.channelDelayCountSamples = 0;
    }
  }
 
//$A601
//   UpFlowTable[tbindex].avg_req_stime=0;
//   UpFlowTable[tbindex].num_delay_samples= 0;

//   UpFlowTable[tbindex].channelDelayCount=0;
//   UpFlowTable[tbindex].channelDelayCountSamples= 0;

//  CMstats.channelDelayCount=0;
//  CMstats.channelDelayCountSamples = 0;

//$A510
  double ratioCSMAPLen = (double)numCSs/(double)map_len;
//  avgCSRatio +=ratioCSMAPLen;
  avgCSRatioCount++;
  avgCSRatio = (1-weightQ)*avgCSRatio + weightQ*ratioCSMAPLen;

  if (debug_cm)
    printf("CM%d:updateAllocationTable(%lf): map_len:%d, NumCS:%d, Ratio:%2.2f  avgCSRatio:%3.3f\n",
           cm_id,current_time,map_len, numCSs,ratioCSMAPLen,avgCSRatio);

  if (avgCSRatio > .20) {
    myUSSFMgr->channelIdleEvent();
  }

// $A700: 12/27/2009: 
  CMstats.total_num_cslots+=numCSs;
  CMstats.total_num_dslots+=numberSlotsForThisFlow;

}

/*************************************************************************

*************************************************************************/
int MacDocsisCM::DataGrantPending(char tbindex)
{
  return UpFlowTable[tbindex].pending;
}

/*************************************************************************

* Routine: int MacDocsisCM::MapSentAfterReq(char tbindex)
*
* Explanation:
*   This function looks to see if the request has timed out
*
* Inputs:
*  The MAP is pointed to by the global ptr map_
*
* Outputs:
*    A 1 means the cmts ack time is now ahead of t3he request time.
*
*************************************************************************/
int MacDocsisCM::MapSentAfterReq(char tbindex)
{
  if (UpFlowTable[tbindex].debug)
    printf("CM%d(flow-id %d)At %lf, map_acktime = %lf req_time = %f\n", 
	   cm_id,UpFlowTable[tbindex].upstream_record.flow_id, Scheduler::instance().clock(),
	   map_acktime,UpFlowTable[tbindex].req_time);

  
  if (map_acktime > UpFlowTable[tbindex].req_time)
    return 1;
  else 
    return 0;
}

/*************************************************************************

*************************************************************************/
int MacDocsisCM::NumContentionSlots(char tbindex)
{
  aptr temp;
  double current_time;
  int n = 0;
  
  current_time = Scheduler::instance().clock();
  temp = UpFlowTable[tbindex].alloc_list;
	
  if (!temp)
    return 0;
  
  while (temp)
    {
      if ((!temp->used) && (temp->start_time > current_time)  && (temp->type == CONTENTION_GRANT))
	if ((!temp->used) && (temp->start_time > current_time)  && (temp->type == CONTENTION_GRANT))
	  {
	    n +=  temp->num_slots;
	  }
	else if (!temp->used && (temp->end_time < current_time) && (temp->type == CONTENTION_GRANT))
	  {
	    temp->used = 1;
	  }
      temp = temp->next;
    }
  return n;
}

/*************************************************************************

*************************************************************************/
void MacDocsisCM::MarkUsedContentionSlots(int n,char tbindex)
{
  aptr temp;
  double current_time;
  
  if (UpFlowTable[tbindex].debug)
    printf("CM%d(flow-id %d):MarkUsedContentionSlots(%lf):Going to mark %d slots as busy\n", 
	   cm_id,UpFlowTable[tbindex].upstream_record.flow_id, 
	   Scheduler::instance().clock(),n);
  
  current_time = Scheduler::instance().clock();
  temp = UpFlowTable[tbindex].alloc_list;
  
  if (!temp)
    return ;
  
  while (temp && n > 0)
    {
      if (UpFlowTable[tbindex].debug)
	printf("CM%d(flow-id %d):MarkUsedContentionSlots(%lf):Look at this entry:%ld\n", 
	       cm_id,UpFlowTable[tbindex].upstream_record.flow_id, 
	       Scheduler::instance().clock(),temp->start_time);
      
      if ((!temp->used) && (temp->start_time > current_time)&& (temp->type == CONTENTION_GRANT))
	{
	  temp->used = 1;
	  n -=  temp->num_slots;
	}
      else if (!temp->used && (temp->end_time < current_time) && (temp->type == CONTENTION_GRANT))
	{
	  temp->used = 1;
	}
      temp = temp->next;
    }
  return ;
}

/*************************************************************************
Find the starting time of the 'num' contention slot
*************************************************************************/
double MacDocsisCM::find_contention_slot(char tbindex, u_int32_t num)
{
  aptr temp;
  double current_time,t;
  
  if (UpFlowTable[tbindex].debug)
    printf("CM%d(flow-id %d):find_contention_slot(%lf):Entered for slot num of %d\n", 
	   cm_id,UpFlowTable[tbindex].upstream_record.flow_id,
	   Scheduler::instance().clock(),num);
  
  current_time = Scheduler::instance().clock();
  temp = UpFlowTable[tbindex].alloc_list;
  
  if (!temp)
    return -1.0;
  
  while (temp && num > 0)
    {
      //printf("4...\n");
      if ((!temp->used) && (temp->start_time > current_time) && (temp->type == CONTENTION_GRANT))
	{
	  if ( num > temp->num_slots)
	    num -= temp->num_slots;
	  else
	    {
	      //temp->num_slots -= (num - size_ureqgrant + 1);
	      temp->num_slots -= num ;
	      
	      if (temp->num_slots > 0)
		{
		  /* Mark all the slots before num as used */
		  //temp->start_time = temp->start_time + ((num - size_ureqgrant) *(size_mslots))/(10*10*10*10*10*10);
		  temp->start_time = temp->start_time + ((num - 1) *(size_mslots));
		}
	      else
		temp->used = 1;
	      
	      t = (temp->start_time) - current_time;
	      return t;
	    }	
	}
      else if (!temp->used && (temp->end_time < current_time) && (temp->type == CONTENTION_GRANT))
	{
	  temp->used = 1;
	}
      temp = temp->next;
    }
  return -1.0;
}

/*************************************************************************

*************************************************************************/
void MacDocsisCM::turn_off_contention(char tbindex)
{
  UpFlowTable[tbindex].bk_offwin = 0;
  UpFlowTable[tbindex].bk_offcounter = 0;
  UpFlowTable[tbindex].contention_on = 0;
  UpFlowTable[tbindex].num_retries = 0;
  return;
}

/*************************************************************************

*************************************************************************/
void MacDocsisCM::back_off(char tbindex, Packet* p)
{
  Packet* t;
//$A510
  struct hdr_cmn* ch = HDR_CMN(p);
  serviceFlowObject *mySFObj = NULL;
  mySFObj = myUSSFMgr->getServiceFlow(p);
  if (mySFObj == NULL) {
    printf("CM:back_off(%lf): HARD ERROR (cm_id:%d): Could not find this service flow:  packet of size:%d,  and ptype:%d\n",
     Scheduler::instance().clock(),cm_id,ch->size(),ch->ptype());
//    return;
    exit(1);
  }

  if (UpFlowTable[tbindex].num_retries == 1) /* First collision */
    {
      UpFlowTable[tbindex].total_fcoll++;
    }  
  CMstats.total_num_collisions++;
  CMstats.total_collisions_status++;
    
  UpFlowTable[tbindex].bk_offwin = 2*UpFlowTable[tbindex].bk_offwin;
  
  if (UpFlowTable[tbindex].debug)
    printf("CM%d(flow-id %d) :back_off(%lf) total_num_collisions: %d, num_retries:%d, bk_offwin:%d, bk_offcounter:%d\n",
	   cm_id,UpFlowTable[tbindex].upstream_record.flow_id, 
	   Scheduler::instance().clock(),CMstats.total_num_collisions,
	   UpFlowTable[tbindex].num_retries, UpFlowTable[tbindex].bk_offwin, UpFlowTable[tbindex].bk_offcounter);
  
  if (UpFlowTable[tbindex].bk_offwin > UpFlowTable[tbindex].bk_offend)
    UpFlowTable[tbindex].bk_offwin = UpFlowTable[tbindex].bk_offend;
  //UpFlowTable[tbindex].bk_offcounter = UpFlowTable[tbindex].bk_offwin;
  
  if (UpFlowTable[tbindex].num_retries >  UpFlowTable[tbindex].max_retries)
    {
      
      MACstats.total_packets_dropped++; 
      UpFlowTable[tbindex].total_collision_drops++;

     if (UpFlowTable[tbindex].debug)
       printf("CM%d(flow-id %d):back_off(%lf): Dropping a packet as the CM exceeds max retry limit:  total_num_collisions: %d, num_retries:%d, bk_offwin:%d\n",
	      cm_id,UpFlowTable[tbindex].upstream_record.flow_id, 
	      Scheduler::instance().clock(),CMstats.total_num_collisions,
	      UpFlowTable[tbindex].num_retries, UpFlowTable[tbindex].bk_offwin);

      drop(p);
            
      turn_off_contention(tbindex);
      
      if (bit_on(UpFlowTable[tbindex].upstream_record.flag,FRAG_ON_BIT))
	{
	  set_bit(&UpFlowTable[tbindex].upstream_record.flag, FRAG_ON_BIT,OFF);
	  UpFlowTable[tbindex].frag_data = 0;
	  UpFlowTable[tbindex].frag_pkt = 0;
	}
      
      UpFlowTable[tbindex].pkt = 0;
      
      //$A510
//$A701 : what if p is MGT ?
      if ((myUSSFMgr->len_queue(p,tbindex)) > 0)
//      if ((len_queue(tbindex)) > 0)
	{
	  UpFlowTable[tbindex].state = BEFFORT_DECISION;
      //$A510
      t = myUSSFMgr->deque_pkt(mySFObj);
//	  t = deque_pkt(tbindex);
	  UpFlowTable[tbindex].pkt = t;
#ifdef TIMINGS
//We will log this next in BEFFORT_DECI
//	printf("2 %lf %d %d 2\n",Scheduler::instance().clock(),
//			UpFlowTable[tbindex].upstream_record.flow_id,cm_id); 
//        timingsTrace(t,2);
#endif
          if (UpFlowTable[tbindex].debug) {
                 printf("CM%d(flow-id %d):back_off(%lf): NRAD CASE 6-2, reset enque_time \n",
	               cm_id,UpFlowTable[tbindex].upstream_record.flow_id,
	               Scheduler::instance().clock());
          }
          UpFlowTable[tbindex].enque_time = Scheduler::instance().clock();
	  beffort_decision(tbindex, PKT_ARRIVAL, t);
	}	
      else
	UpFlowTable[tbindex].state = BEFFORT_IDLE;      
    }
  else
    {
      if (UpFlowTable[tbindex].debug) {
          printf("CM%d(flow-id %d):back_off(%lf): NRAD CASE 6-3, reset enque_time \n",
             cm_id,UpFlowTable[tbindex].upstream_record.flow_id,
              Scheduler::instance().clock());
      }
      UpFlowTable[tbindex].enque_time = Scheduler::instance().clock();
      beffort_decision(tbindex, PKT_ARRIVAL, p);
    }
  
  if (UpFlowTable[tbindex].debug)
    printf("CM%d(flow-id %d)back_off::Num-retries = %d\n",
	   cm_id, UpFlowTable[tbindex].upstream_record.flow_id,
	   UpFlowTable[tbindex].num_retries);
  return;
}

/****************************************************************************
 * Method:  int MacDocsisCM::CanBeSent(char tbindex, Packet* p)
 *
 * Function:   
 *    Checks to see if the latest MAP has allocated a data grant.
 *    If so, we return a 1, else a 0.
 *    IF we are sending a frag this gets called to make sure there are
 *    no more grants.  If the CMTS ever issues 2 grants for
 *    the same flow in the same map, this might return a 1.
 *    
 *
 * Parameters:
 *    char tbindex:
 *    Packet* p: The frame that already has been adjusted for all headers
 *
 * Return Data:
 *           1: If this frame can be sent
 *               It sets the alloc table grant
 *               entries to 'used'.
 *           0: If this frame can not be sent
 *           
 * Design Notes:
 *
 * *************************************************************************/
int MacDocsisCM::CanBeSent(char tbindex, Packet* p)
{
  /* whenever you use,'p' here, remember that from 
     fill_piggyback_req, we are sending a null pointer */
  aptr temp;
  double current_time;
  
  current_time = Scheduler::instance().clock();
  temp = UpFlowTable[tbindex].alloc_list;
  
  if (!temp)
    return 0;

  while (temp)
    {
      if (UpFlowTable[tbindex].debug)
        printf("CM%d(%lf)(flow-id %d)CanBeSent: used:%d, start_time:%lf, type:%d\n",
	   cm_id,current_time, UpFlowTable[tbindex].upstream_record.flow_id,
           temp->used, temp->start_time, temp->type);

      if ((!temp->used) && (temp->start_time > current_time) && (temp->type == DATA_GRANT))
	{
//          printf("CM%d(%lf)(flow-id %d)CanBeSent: used: 1, return 1\n",
//             cm_id,current_time, UpFlowTable[tbindex].upstream_record.flow_id);
	  return 1;
	}
      else if (!temp->used && (temp->end_time < current_time) && (temp->type == DATA_GRANT))
	{
//          printf("CM%d(%lf)(flow-id %d)CanBeSent: used: 2\n",
//             cm_id,current_time, UpFlowTable[tbindex].upstream_record.flow_id);
	  temp->used = 1;
	}
      temp = temp->next;
    }
//  printf("CM%d(%lf)(flow-id %d)CanBeSent: used: 3, can't send\n",
//     cm_id,current_time, UpFlowTable[tbindex].upstream_record.flow_id);
  return 0;
}

/****************************************************************************
 * Method:  int MacDocsisCM::CanUnicastReqBeSent(char tbindex)
 *
 * Function:   
 *    Checks to see if the latest MAP has allocated a grant for a unicast request.
 *    This is the rtPS periodic grant allowing this CM to request Bw	.
 *    If so, we return a 1, else a 0.
 *    
 *
 * Parameters:
 *    char tbindex:
 *
 * Return Data:
 *           1: If this frame can be sent
 *               It sets the alloc table grant
 *               entries to 'used'.
 *           0: If this frame can not be sent
 *           
 * Design Notes:
 *
*************************************************************************/
int MacDocsisCM::CanUnicastReqBeSent(char tbindex)
{
  aptr temp;
  double current_time;
  
  if (UpFlowTable[tbindex].debug)
    printf("CM%d(flow-id %d)Checking Unicast req grants at %lf\n",
	   cm_id, UpFlowTable[tbindex].upstream_record.flow_id,
	   Scheduler::instance().clock());

  //print_alloclist(tbindex);
  current_time = Scheduler::instance().clock();
  temp = UpFlowTable[tbindex].alloc_list;
  
  if (!temp)
    return 0;
  
  while (temp)
    {
      if ((!temp->used) && (temp->start_time > current_time) && (temp->type == UREQ_GRANT))
	{
	  return 1;
	}
      else if (!temp->used && (temp->end_time < current_time) && (temp->type == UREQ_GRANT))
	{
	  temp->used = 1;
	}
      temp = temp->next;
    }
  return 0;
}
	
/*************************************************************************

*************************************************************************/
int MacDocsisCM::CanContentionReqBeSent(char tbindex)
{
  aptr temp;
  double current_time;
  
  current_time = Scheduler::instance().clock();
  temp = UpFlowTable[tbindex].alloc_list;
  
  if (!temp)
    return 0;
  
  while (temp)
    {
      if ((!temp->used) && (temp->start_time > current_time) && (temp->type == CONTENTION_GRANT))
	{
	  return 1;
	}
      else if (!temp->used && (temp->end_time < current_time) && (temp->type == CONTENTION_GRANT))
	{
	  temp->used = 1;
	}
      temp = temp->next;
    }
  return 0;
}

/*************************************************************************

*************************************************************************/
double MacDocsisCM::timer_expiration(char tbindex, Packet* p, int flag)
{
  aptr temp;
  double current_time;
  
  current_time = Scheduler::instance().clock();  
  temp = UpFlowTable[tbindex].alloc_list;
  if (UpFlowTable[tbindex].debug)
  {
    printf("CM%d(flow-id %d):timer_expiration(%lf): flag: %d\n", 
      cm_id,UpFlowTable[tbindex].upstream_record.flow_id,Scheduler::instance().clock(),flag);
   print_alloclist(tbindex);
  }
  
  if (!temp)
    return -1.0;
  
  switch(flag) 
    {
    case DATA_GRANT:
      while (temp)
	{
	  if ((!temp->used) && (temp->start_time > current_time) && (temp->type == DATA_GRANT))
	    {
	      UpFlowTable[tbindex].curr_gsize = temp->num_slots * bytes_pminislot;
	      current_time = Scheduler::instance().clock();
	      temp->used = 1;
	      return (temp->start_time - current_time);	      
	    }
	  else if (!temp->used && (temp->end_time < current_time) && (temp->type == DATA_GRANT))
	    {
	      temp->used = 1;
	    }
	  temp = temp->next;
	}
      return -1.0;
      break;
      
    case UREQ_GRANT:
      while (temp)
	{
          if (UpFlowTable[tbindex].debug)
          {
            printf("CM%d(flow-id %d):timer_expiration(%lf): used:%d,start_time:%f, type:%d\n", 
               cm_id,UpFlowTable[tbindex].upstream_record.flow_id,Scheduler::instance().clock(),
                 temp->used,temp->start_time,temp->type);
          }
	  if ((!temp->used) && (temp->start_time > current_time) && (temp->type == UREQ_GRANT))
	    {
	      temp->used = 1;
	      current_time = Scheduler::instance().clock();
              if (UpFlowTable[tbindex].debug)
              {
                 printf("CM%d(flow-id %d):timer_expiration(%lf):returning now with %f\n", 
                    cm_id,UpFlowTable[tbindex].upstream_record.flow_id,Scheduler::instance().clock(),
	                  (temp->start_time - current_time));	      
              }
	      return (temp->start_time - current_time);	      
	    }
	  else if (!temp->used && (temp->end_time < current_time) && (temp->type == UREQ_GRANT))
	    {
	      temp->used = 1;
	    }
	  temp = temp->next;
	}
        if (UpFlowTable[tbindex].debug)
        {
            printf("CM%d(flow-id %d):timer_expiration(%lf):returning now in ERROR\n", 
              cm_id,UpFlowTable[tbindex].upstream_record.flow_id,Scheduler::instance().clock());
        }
      return -1.0;
      break;
      
    case CONTENTION_GRANT:	
      while (temp)
	{
	  if ((!temp->used) && (temp->start_time > current_time) && (temp->type == CONTENTION_GRANT))
	    {
	      temp->num_slots -= 1;
	      if (temp->num_slots > 0)
		{
		  /* Mark all the slots before num as used */
		  temp->start_time = temp->start_time + (1) *size_mslots;
		}
	      else
		temp->used = 1;
	      
	      current_time = Scheduler::instance().clock();	      
	      return (temp->start_time - current_time);
	      
	    }
	  else if (!temp->used && (temp->end_time < current_time) && (temp->type == CONTENTION_GRANT))
	    {
	      temp->used = 1;
	    }
	  temp = temp->next;
	}
      return -1.0;
      break;      
    }
}

/*************************************************************************

*************************************************************************/
void MacDocsisCM::insert_sndlist( double time, char tbindex)
{
  sptr	temp, prev,node;
  
  temp = SndList;
  prev = temp;
  node = (sptr) malloc(sizeof (struct snd_timer_list));

  if (UpFlowTable[tbindex].debug)
  {
    printf("CM%d(flow-id %d):insert_sndlist(%lf): Time is %lf, cur size:%d\n", 
      cm_id,UpFlowTable[tbindex].upstream_record.flow_id,Scheduler::instance().clock(),time,
                  size_sndlist(tbindex));
  }
  
  if (node == 0)
    {
      printf("CM%d(flow-id %d) MacDocsisCM->insert_sndlist: Malloc failed, quiting\n",
	     cm_id,UpFlowTable[tbindex].upstream_record.flow_id);
      exit(1);
    }
  
  node->expiration_time = time;
  node->tindex = tbindex;
  
  if (!temp)
    {
      node->next = 0;
      SndList = node;
      return;
    }
  else
    {
      while (temp)
	{	
	  if (temp->expiration_time > time)
	    {	
	      break;
	    }
	  else
	    {
	      prev = temp;
	      temp = temp->next;
	    }
	}
      
      if (prev != temp)
	{
	  prev->next = node;
	  node->next = temp;
	  return;
	}
      else
	{
	  node->next = temp;
	  SndList = node;
	  return;
	}      
    }  
  return;
}

/*************************************************************************
 * method: int MacDocsisCM::size_sndlist(char tbindex)
 *
 * Explanation:  This returns the size of the send list
 *               A '-1' by convention is an error.
 *
*************************************************************************/
int MacDocsisCM::size_sndlist(char tbindex)
{
  sptr	temp;
  int count = 0;
  temp = SndList;

  while (temp)
  {
    count++;
    temp = temp->next;
  }
  
  if (UpFlowTable[tbindex].debug)
  {
    printf("CM%d(flow-id %d):size_sndlist(%lf): %d\n", 
      cm_id,UpFlowTable[tbindex].upstream_record.flow_id,Scheduler::instance().clock(),count);
  }
  
  return count;
}



/*************************************************************************

*************************************************************************/
void MacDocsisCM::insert_reqlist( double time, char tbindex)
{
  rptr	temp, prev,node;
  
  temp = ReqList;
  prev = temp;  
  node = (rptr) malloc(sizeof (struct req_timer_list));

  if (UpFlowTable[tbindex].debug)
  {
    printf("CM%d(flow-id %d):insert_reqlist(%lf): Time is %lf\n", 
      cm_id,UpFlowTable[tbindex].upstream_record.flow_id,Scheduler::instance().clock(),time);
  }
  
  if (node == 0)
    {
      printf("CM%d(flow-id %d) MacDocsisCM->insert_reqlist: Malloc failed, quiting\n",
	     cm_id,UpFlowTable[tbindex].upstream_record.flow_id);
      exit(1);
    }
  
  node->expiration_time = time;
  node->rindex = tbindex;
  
  if (!temp)
    {
      node->next = 0;
      ReqList = node;
      return;
    }
  else
    {
      while (temp)
	{	
	  if (temp->expiration_time > time)
	    {	
	      break;
	    }
	  else
	    {
	      prev = temp;
	      temp = temp->next;
	    }
	}
      
      if (prev != temp)
	{
	  prev->next = node;
	  node->next = temp;
	  return;
	}
      else
	{
	  node->next = temp;
	  ReqList = node;
	  return;
	}      
    }  
  return;
}

/*************************************************************************
 * method: int MacDocsisCM::size_reqlist(char tbindex)
 *
 * Explanation:  This returns the size of the send list
 *               A '-1' by convention is an error.
 *
*************************************************************************/
int MacDocsisCM::size_reqlist(char tbindex)
{
  sptr	temp;
  int count = 0;
  temp = SndList;

  while (temp)
  {
    count++;
    temp = temp->next;
  }
  
  if (UpFlowTable[tbindex].debug)
  {
    printf("CM%d(flow-id %d):size_reqlist(%lf): %d\n", 
      cm_id,UpFlowTable[tbindex].upstream_record.flow_id,Scheduler::instance().clock(),count);
  }
  
  return count;
}


//Reinsert the removed entry into the ReqList
void MacDocsisCM::reinsert_reqlist(char tbindex)
{
	rptr	temp, prev,node;
if(reqFlag ==1)
   reqFlag = 0;
//printf("\n..%d..",reqFlagCounter);
reqFlagCounter = 0;
/*	temp = ReqList;
	prev = temp;

	node = (rptr) malloc(sizeof (struct req_timer_list));
	if (node == 0)
	{
		printf("CM%d(flow-id %d) MacDocsisCM->insert_reqlist: Malloc failed, quiting\n",cm_id,UpFlowTable[tbindex].upstream_record.flow_id);
		exit(1);
	}*/
//	node->expiration_time = time;
//	node->rindex = tbindex;
 /*       node = tempReqList;
	if (!temp)
	{
		node->next = 0;
		ReqList = node;
		return;
	}
	else
	{
		while (temp)
		{	
			if (temp->expiration_time > node->expiration_time)
			{	
				break;
			}
			else
			{
				prev = temp;
				temp = temp->next;
			}
		}
		if (prev != temp)
		{
			prev->next = node;
			node->next = temp;
			return;
		}
		else
		{
			node->next = temp;
			ReqList = node;
			return;
		}
		
	}*/
ReqList = tempReqList->next;	
	return;
}


/*************************************************************************

*************************************************************************/
void MacDocsisCM::insert_alloclist( char tbindex, u_int16_t fid, double stime, 
				    double etime, u_int16_t iuc, u_int32_t n)
{  
  aptr node,tmp,itr;
  double current_time;
  
  node = (aptr) malloc(sizeof (struct allocation_time));
  
  node->start_time = stime;
  node->end_time = etime;
  node->num_slots = n;
  node->next = 0;  
  node->type = iuc;
  node->used = 0;
  
  if (!UpFlowTable[tbindex].alloc_list)
    {
      UpFlowTable[tbindex].alloc_list = node;
      return;
    }
  current_time = Scheduler::instance().clock();
  
  if (UpFlowTable[tbindex].alloc_list->next)
    {
      itr = UpFlowTable[tbindex].alloc_list->next;
      tmp = UpFlowTable[tbindex].alloc_list;
    }
  else
    {
      if ((UpFlowTable[tbindex].alloc_list->used) || 
	  (UpFlowTable[tbindex].alloc_list->end_time < current_time))
	{
	  itr = UpFlowTable[tbindex].alloc_list;
	  UpFlowTable[tbindex].alloc_list = node;
	  free(itr);
	  return;
	}
      else
	{
	  UpFlowTable[tbindex].alloc_list->next = node;
	  return;
	}
    } 
  current_time = Scheduler::instance().clock();
  
  while (itr)
    {
      if ((itr->used) || (itr->end_time < current_time))
	{
	  tmp->next = itr->next;
	  free(itr);
	  itr = tmp;
	}
      tmp = itr;
      itr = itr->next;
    }
  
  tmp->next = node;  
  itr = UpFlowTable[tbindex].alloc_list;
  
  if ((itr->used) || (itr->end_time < current_time))
    {
      UpFlowTable[tbindex].alloc_list = itr->next;
      free(itr);
    }  
}

/*************************************************************************
*
* Function: int MacDocsisCM::GrantsInalloclist(char tbindex)
*
* Explanation:
*    This is called to return the number of data grants in slots
*    that are in the latest CM.  
*
* Inputs:
*
* Outputs:
*
*
*************************************************************************/
int MacDocsisCM::grantsInAllocList(char tbindex)
{
  int numberGrants=0;
  aptr tmp;
  tmp = UpFlowTable[tbindex].alloc_list;
  
  while (tmp)
  {
      if (tmp->type == 0) {
        if (numberGrants > 0) {
          printf("CM%d(flow-id %d):grantsInAlloc:(%lf) HARD ERROR:  > 1 grant ?  Data grant: %d slots, start time:%lf, end time:%lf\n",
	        cm_id,UpFlowTable[tbindex].upstream_record.flow_id, Scheduler::instance().clock(), tmp->num_slots,tmp->start_time,tmp->end_time);
         }
        numberGrants=tmp->num_slots;
        if (UpFlowTable[tbindex].debug)
          printf("CM%d(flow-id %d):grantsInAlloc:(%lf) Data grant: %d slots, start time:%lf, end time:%lf\n",
	        cm_id,UpFlowTable[tbindex].upstream_record.flow_id, Scheduler::instance().clock(), tmp->num_slots,tmp->start_time,tmp->end_time);
      }
      tmp = tmp->next;
  }

  return numberGrants;
}

/*************************************************************************
*
* Function: void MacDocsisCM::print_alloclist(char tbindex)
*
* Explanation:
*    This is called to display the allocations for this CM in the 
 *   latest MAP.
*
* Inputs:
*
* Outputs:
*
*
*************************************************************************/
void MacDocsisCM::print_alloclist(char tbindex)
{
  aptr tmp;
  tmp = UpFlowTable[tbindex].alloc_list;
  
  if (tmp==NULL)
    printf("print_alloclist:  list is empty \n");
  else
    printf("CMprint_alloclist: .... \n");

  while (tmp)
    {
      printf("Start time = %lf ", tmp->start_time);
      printf("End time = %lf ", tmp->end_time);
      printf("Num slots = %d ", tmp->num_slots);
      
      if (tmp->type == 0)
	printf("Type = DATA GRANT ");
      else if (tmp->type == 1)
	printf("Type = UNICAST REQ GRANT ");
      else if (tmp->type == 2)
	printf("Type = CONTENTION SLOTS ");
      else if (tmp->type == 3)
	printf("Type =  SM SLOTS ");
      
      printf("Used = %d\n", tmp->used);
      tmp = tmp->next;
    }
  printf("\n");
}


/***********************************************************************
 *
* int  MacDocsisCM::getNumberDataSlots(Packet* p)
*
* input:   Packet *p : the packet that contains the map message
*
* outputs:  returns the number of data slots
*
* function:
*
***********************************************************************/
int  MacDocsisCM::getNumberDataSlots(Packet* p)
{
  int numberDataSlots = 0;
  hdr_docsismap  *cm  = hdr_docsismap::access(p);
  docsis_chabstr *ch_abs = docsis_chabstr::access(p);
  u_int32_t * data = (u_int32_t *)p->accessdata();
  u_int16_t flow_id, offset,iuc,offsetn;
  u_int32_t map_len,tmp,num_slots;
  int i = 1;
  double alloc_stime,alloc_etime;

  /* Process all IEs..*/
  flow_id = 0;
  
  //Computes the number of slots
  map_len = calculate_slots(cm->allocstarttime_(),cm->allocendtime_());
  tmp = 0;
  
  while (i < cm->numIE_())
  {
      flow_id = *data | flow_id;
      
      if ((flow_id == 0) || (flow_id == 0xfff0)) 
	{
	  /* flow-id = 0 -> NULL IE, flow-id = 0xfff0->Unused slot */
	  i++;
	  data++;
	  flow_id = 0;
	  continue; // NULL IE
	}
      flow_id = flow_id << 2;
      flow_id = flow_id >> 2;
      
      /* calculate the offset */      
      offset = (*data) >> 18;
      
    if (offset == map_len) /* Grant pending IE */
	{
       ;
	}
    else
	{
	  offsetn = (*(data+1))>>18;

	  /* Calculate the start-time..*/
	  num_slots = offsetn - offset;
	  
	  //alloc_stime = cm->allocstarttime_() + (offset * size_mslots)/(10*10*10*10*10*10);
	  alloc_stime = cm->allocstarttime_() + (offset * size_mslots);
	  
	  //alloc_etime = alloc_stime + (num_slots * size_mslots)/(10*10*10*10*10*10);
	  alloc_etime = alloc_stime + (num_slots * size_mslots);
	  
	  tmp = (*data) & 49152;
	  iuc = tmp >> 14;

      if (iuc == 0)
        numberDataSlots+=num_slots;
	  
	}
      i++;
      data++;
      flow_id = 0;
      tmp = 0;
  }
  return numberDataSlots;
}

/***********************************************************************
 *
* void MacDocsisCM::print_short_map_list(Packet* p)
*
* input:   Packet *p : the packet that contains the map message
*
* outputs:  none
*
* function:  This method dumps all IE's associated with the map.
*  The format:  each MAP will begin with a code of 0 and have zero
*   or more codes of 1,2,3.
*  0 timestamp CMID  mapstart mapstop numberIE's  maplength in slots  
*  1 timestamp flowid   (null IE)
*  2 timestamp flowid   (indicates a grant is pending to the CM) 
*  3 timestamp flowid starttime endtime iuccode #slots 
*
***********************************************************************/
void MacDocsisCM::print_short_map_list(Packet* p)
{
  //struct hdr_docsismap  *cm  = HDR_DOCSISMAP(map_);
  hdr_docsismap  *cm  = hdr_docsismap::access(p);
  docsis_chabstr *ch_abs = docsis_chabstr::access(p);
  //u_int32_t * data = (u_int32_t *)map_->accessdata();
  u_int32_t * data = (u_int32_t *)p->accessdata();
  u_int16_t flow_id, offset,iuc,offsetn;
  u_int32_t map_len,tmp,num_slots;
  int i = 1,count = 0;
  double alloc_stime,alloc_etime;
  FILE* fp;
  FILE* fpch;

#ifndef FILES_OK
   return;
#endif
  
  /* Process all IEs..*/
  flow_id = 0;
  
  //Computes the number of slots
  map_len = calculate_slots(cm->allocstarttime_(),cm->allocendtime_());
  tmp = 0;
  
  fp = fopen("CMMAP.out", "a+");
  fpch = fopen("CMMAPperchannel.out", "a+");
  fprintf(fp,"0 %lf %d  %lf %lf %d %d %lf\n",Scheduler::instance().clock(),cm_id,
	  cm->allocstarttime_(),cm->allocendtime_(),cm->numIE_(),map_len,cm->acktime_());
  fprintf(fpch,"Channel number  %d :: 0 %lf %d  %lf %lf %d %d %lf\n",ch_abs->channelNum,Scheduler::instance().clock(),cm_id,
	  cm->allocstarttime_(),cm->allocendtime_(),cm->numIE_(),map_len,cm->acktime_());

  while (i < cm->numIE_())
    {
      flow_id = *data | flow_id;
      
      if ((flow_id == 0) || (flow_id == 0xfff0)) 
	{
	  /* flow-id = 0 -> NULL IE, flow-id = 0xfff0->Unused slot */
	  i++;
	  data++;
	  flow_id = 0;
	  fprintf(fp,"1 %lf %d \n",Scheduler::instance().clock(),flow_id);
	  fprintf(fpch,"Channel number  %d :: 1 %lf %d \n",ch_abs->channelNum,Scheduler::instance().clock(),flow_id);
	  continue; // NULL IE
	}
      flow_id = flow_id << 2;
      flow_id = flow_id >> 2;
      
      /* calculate the offset */      
      offset = (*data) >> 18;
      
      if (offset == map_len) /* Grant pending IE */
	{
	  fprintf(fp,"2 %lf %d \n",Scheduler::instance().clock(),flow_id);
	  fprintf(fpch,"Channel number  %d :: 2 %lf %d \n",ch_abs->channelNum,Scheduler::instance().clock(),flow_id);
	}
      else
	{
	  offsetn = (*(data+1))>>18;

	  /* Calculate the start-time..*/
	  num_slots = offsetn - offset;
	  
	  //alloc_stime = cm->allocstarttime_() + (offset * size_mslots)/(10*10*10*10*10*10);
	  alloc_stime = cm->allocstarttime_() + (offset * size_mslots);
	  
	  //alloc_etime = alloc_stime + (num_slots * size_mslots)/(10*10*10*10*10*10);
	  alloc_etime = alloc_stime + (num_slots * size_mslots);
	  
	  tmp = (*data) & 49152;
	  iuc = tmp >> 14;
	  fprintf(fp,"3 %lf %d %lf %lf %d %d\n",
		  Scheduler::instance().clock(),flow_id,
		  alloc_stime,alloc_etime,iuc,num_slots);
	  fprintf(fpch,"Channel number  %d :: 3 %lf %d %lf %lf %d %d\n",
		  ch_abs->channelNum,Scheduler::instance().clock(),flow_id,
		  alloc_stime,alloc_etime,iuc,num_slots);
	  
	  count++;
	}
      i++;
      data++;
      flow_id = 0;
      tmp = 0;
    }
  fclose(fp);
  fclose(fpch);
  return;
}

/*************************************************************************

*************************************************************************/
void MacDocsisCM::print_cmalloclist()
{
  aptr tmp;
  int i = 0;
  
  for (i = 0; i <SizeUpFlowTable; i++)
    {
      printf("CM%d(flow-id %d)\n",cm_id,UpFlowTable[i].upstream_record.flow_id);
      
      tmp = UpFlowTable[i].alloc_list;
      
      while (tmp)
	{
	  printf("Start time = %lf ", tmp->start_time);
	  printf("End time = %lf ", tmp->end_time);
	  printf("Num slots = %d ", tmp->num_slots);
	  printf("Type = %d ", tmp->type);
	  printf("Used = %d\n", tmp->used);
	  tmp = tmp->next;
	}
    }
  printf("\n");
}




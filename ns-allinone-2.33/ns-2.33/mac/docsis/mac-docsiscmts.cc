/*****************************************************************************
 * This file contains the CMTS object class method implementations
 *
 * Revisions:
 *  $A301:  free the packet once the CMTS tx process completes
 *  $A305:  add docsis 3.0 extended header
 *  $A306:  10/5/2008: added the macPhyInterface
 *  $A307:  move actual transmission and reception into the medium
 *  $A311:  support configMgr
 *  $A400:  v.93 changes
 *  $600:   5-28-2009  Minor cleanup.
 *  $A802:  added cleaner tcl classifier : src, dst, type, flowID
 *  $804:   8-11-2010 Support AQM at DS SF Objects
 *  $A805:  9-29-2010:  support resourceOptimizer
 *  $A910:  4/1/2014:  add simple data packet trace
 *
****************************************************************************/

#include "globalDefines.h"
#include "mac-docsis.h"
#include "random.h"
#include "math.h"
#include "ping.h"
#include "loss_monitor.h"

//$A311
#include "configMgr.h"
#include "macPhyInterface.h"
#include "medium.h"
#include "serviceFlowMgr.h"
#include "reseqMgr.h"
#include "serviceFlowObject.h"
#include "schedulerParams.h"
#include "schedulerObject.h"
#include "packetMonitor.h"

//Used by PACKETTRACE to trace certain flows
#define FLOW_ID 0 
#define CM_ID 0
//#define ERROR_TRACE 1

//Uncomment to trace arrivals and departures in  DSArrivals.out and DSDepartures.out
//#define  ARRIVAL_TRACE 1

//#define FILES_OK 1

//This must be defined...
//if 0, no trace
//if 3, max trace
#define TRACE_SCHEDULER 2

//Number of seconds between computing new stats for each CM
//#define ADAPTATION_UPDATE  1
#define ADAPTATION_UPDATE  100
//Number of seconds between sending an update to the CMS
//#define ADAPTATION_FREQUENCY  5
#define ADAPTATION_FREQUENCY  200
//If this is 1, dump results to files adaptation.dat and adaptResults.dat
#define DUMP_ADAPTATION_RESULTS 1



//Specify which algorithm to use
// #0 : do not adapt
// #1 : new scale param is max(1,100 *  percentage of US BW consumed )
// #2 : new scale param is max(1,x)
//       x = 
#define ADAPTATION_ALGORITHM  0

#include "docsisDebug.h"
//#define TRACE_CMTS 0

//#define TRACE_CMTS_UP_DATA 0
//#define TRACE_CMTS_DOWN_DATA 0

#define DUMPCMTSMAP 1 /* If >0 then we dump each sent map to the file CMTSMAP.out */

                          //If defined, then MarkUnusedSlot() fills 
			  //unused slots with contention opportunities
#define FILLWITHCONTOPS 0 


/*
  This was used to get around problems we had with ns rounding precision-
  which lead to events being performed what appeared to be out of order.
*/
#define EPSILON 0.000000005
#define MACDOCSIS_COMMAND 100


int MacDocsisCMTS::next_flowid = 3;
int hdr_docsisextd::offset_;
int hdr_docsis::offset_;
int hdr_docsismgmt::offset_;
int hdr_docsismap::offset_;
int MacDocsis::lan_num = 0;
int MacDocsis::num_cms = 0;
MacDocsisCMTS* MacDocsis::cmts_arr[NUM_DOCSIS_LANS];

/*****************************************************************************

*****************************************************************************/
static class MacDocsisEHeaderClass : public PacketHeaderClass 
{
public:
  MacDocsisEHeaderClass() : PacketHeaderClass("PacketHeader/DocsisCMTSExt", sizeof(hdr_docsisextd)) 
  {
    bind_offset(&hdr_docsisextd::offset_);
#ifdef TRACE_CMTS //---------------------------------------------------------
    printf("Constructor:MacDocsisEHeaderClass: size allocated:%d\n", sizeof(hdr_docsisextd));
#endif
  }
} class_hdr_macdocsisextd;

/*****************************************************************************

*****************************************************************************/
static class MacDocsisHeaderClass : public PacketHeaderClass 
{
public:
  MacDocsisHeaderClass() : PacketHeaderClass("PacketHeader/DocsisCMTS", sizeof(hdr_docsis)) 
  {
    bind_offset(&hdr_docsis::offset_);
#ifdef TRACE_CMTS //---------------------------------------------------------
    printf("Constructor:MacDocsisHeaderClass: size allocated:%d\n", sizeof(hdr_docsis));
#endif
  }
} class_hdr_macdocsis;

/*****************************************************************************

*****************************************************************************/
static class MacDocsisMHeaderClass : public PacketHeaderClass 
{
public:
  MacDocsisMHeaderClass() : PacketHeaderClass("PacketHeader/DocsisCMTSMgmt", sizeof(hdr_docsismgmt)) 
  {
    bind_offset(&hdr_docsismgmt::offset_);
#ifdef TRACE_CMTS //---------------------------------------------------------
    printf("Constructor:MacDocsisMHeaderClass: size allocated:%d\n", sizeof(hdr_docsismgmt));
#endif
  }
} class_hdr_macdocsismgmt;

/*****************************************************************************

*****************************************************************************/
static class MacDocsisMapHeaderClass : public PacketHeaderClass 
{
public:
  MacDocsisMapHeaderClass() : PacketHeaderClass("PacketHeader/DocsisCMTSMap", sizeof(hdr_docsismap)) 
  {
    bind_offset(&hdr_docsismap::offset_);
#ifdef TRACE_CMTS //---------------------------------------------------------
    printf("Constructor:MacDocsisMapHeaderClass: size allocated:%d\n", sizeof(hdr_docsismap));
#endif
  }
} class_hdr_macdocsismap;

/*****************************************************************************
 TCL Hooks for the simulator
*****************************************************************************/
static class MacDocsisCmtsClass : public TclClass 
{
public:
  MacDocsisCmtsClass() : TclClass("Mac/DocsisCMTS") {}
  
  TclObject* create(int, const char*const*) 
  {
    DSSchedulerParams::init();
    return (new MacDocsisCMTS);
  }
} class_mac_docsiscmts;

/*****************************************************************************

*****************************************************************************/
MacDocsisCMTS::MacDocsisCMTS() : MacDocsis(), 
				 mhMap_(this), 
				 mhRng_(this), 
				 mhUcd_(this),
				 mhSync_(this), 

  //#ifdef RATE_CONTROL //--------------------------------------------------
                 mhToken_(this)//,
                 //mhSFToken_(this)
  //#endif //---------------------------------------------------------------

{
  //#ifdef RATE_CONTROL //--------------------------------------------------
  TokenList = 0;	
  //#endif //---------------------------------------------------------------

  
  strcat(docsis_id,"CMTS");
#ifdef TCP_DELAY_BIND_ALL //--------------------------------------------------
  printf("MacDocsisCMTS:  CMTS object constructed,  docsisModelVersion:  %1.2f\n",docsisModelVersion);
#endif//----------------------------------------------------------------------
  
  SizeCmTable = 0;
  CurrIndexCmTable = 0;
  CmRecord = 0;

  CmStatusData = 0;
  CmStatusUpdateData = 0;
  
 //Init to 0
  numberCMsToUpdate = 0;
  lastCMUpdateTime = 0;
  readyToSendUpdate = 0;

  rem_overhead = 0;
  map_stime = 0;
  map_etime = 0;
  map_acktime = 0;
  mptr = 0;
  num_rtslots = 0;
  num_befslots = 0;
  num_adjust_slots = 0;
  AckTime = 0;
  omap_stime = 0;
  omap_etime = 0;  
  max_slots_pmap = 0;
  next_map = 0;  

  MapPropDelay = 1;

  last_mrqtime = 0.0;
  last_mbfqtime = 0.0;
  size_rtqueue = 0;
  size_bfqueue = 0;
  avg_szrtqueue = 0;
  avg_szbfqueue = 0;  
  num_bfreq = avg_bfreq = 0;
  last_mbfreq = 0;  
  num_rtreq = avg_rtreq = 0;
  last_mrtreq = 0;  
  num_dgrant = num_contention = 0;
  num_req = num_gpend = 0;  
  last_dmptime = 0;
  
  bzero((void *)&CMTSstats,sizeof (struct cmts_statistics));

  
  job_list[0] = job_list[1] = job_list[2] = job_list[3] = 0;
  cmts_arr[lan_num] = this;
  lan_num++; 
  
  /*
    Statistics
    These next two are used for periodic statistics...
    So beware, they may be reset during a run
  */

  //Begin $A401
  bzero((void *)&MACstats,sizeof (struct mac_statistics));
  MACstats.last_BWCalcTime = Scheduler::instance().clock();
  //End $A401
  
  //$A306
  cm_id = 0xFFFF; /* node id  -for cmts this is 64k */
    
//$A311
  myConfigMgr = new CMTSconfigMgr;
  myConfigMgr->init(this);

#ifdef TRACE_CMTS //--------------------------------------------------------
  printf("CMTS(id:%d) created successfully\n",cm_id);
#endif //-------------------------------------------------------------------
  
  return;
}

MacDocsisCMTS::~MacDocsisCMTS()
{
}


/*****************************************************************************

*****************************************************************************/
int MacDocsisCMTS::command(int argc, const char*const* argv)
{
  int rc = 0 ;
#ifdef TRACE_CMTS //------------------------------------------------
  printf("CMTS:command: entered with command param count:%d and command:: %s\n",argc, argv[1]);
#endif //-------------------------------------------------------------------- 
  rc = myConfigMgr->processCommandLine(argc, argv);
#ifdef TRACE_CMTS //------------------------------------------------
  printf("CMTS:command: returned from myConfigMgr with rc : %d\n",rc);
#endif //-------------------------------------------------------------------- 
  if (rc == MACDOCSIS_COMMAND)
	return MacDocsis::command(argc,argv);
  return rc;
}

/*****************************************************************************

*****************************************************************************/
void MacDocsisCMTS::AllocMemCmrecord()
{

  CmRecord = (struct cm_record *) malloc(SizeCmTable*sizeof (struct cm_record));
  
  if (CmRecord == 0)
    {
      printf("MacDocsisCMTS->AllocMemCmrecord: Failed to assign memory, Quiting\n");
      exit(1);
    } 
  bzero(CmRecord,(SizeCmTable*sizeof (struct cm_record)));
  return ;
}

/*****************************************************************************

*****************************************************************************/
void MacDocsisCMTS::AllocCmStatusData()
{
  CmStatusData = (struct cm_status_data *) malloc(SizeCmTable*sizeof (struct cm_status_data));
  
  if (CmStatusData == 0)
    {
      printf("MacDocsisCMTS->AllocCmStatusData: Failed to malloc CmStatusData  memory, Quiting\n");
      exit(1);
    } 
  bzero(CmStatusData,(SizeCmTable*sizeof (struct cm_status_data)));

  CmStatusUpdateData = (struct cm_status_update_data *) malloc(SizeCmTable*sizeof (struct cm_status_update_data));
  
  if (CmStatusUpdateData == 0)
    {
      printf("MacDocsisCMTS->AllocCmStatusData: Failed to malloc CmStatusUpdateData memory, Quiting\n");
      exit(1);
    } 
  bzero(CmStatusUpdateData,(SizeCmTable*sizeof (struct cm_status_update_data)));
  numberCMsToUpdate = SizeCmTable;

#ifdef TRACE_CMTS 
  printf("AllocCmStatus: size of CmStatusData buffer: %d size of CmStatusUpdateData: %d \n",
  SizeCmTable*sizeof (struct cm_status_data), SizeCmTable*sizeof (struct cm_status_update_data));
#endif

  return ;
}


/***************************************************************************
*
* Routine: 
* int MacDocsisCMTS::register_to_cmts(int macaddr, u_int16_t priority, u_char def_up, 
*				    u_char def_dn, struct upstream_sflow *UpEntry,
*				    u_char UpSize, struct downstream_sflow * DownEntry, 
*				    u_char DownSize)
*
* Explanation:
*   This is invoked by each CM as they 'boot'.  The CM passes a pointer to it giant US and DS sflow tables.
*   It also specifies which sflow should be considered the 'default'.
*
*
*  inputs: 
*     int macaddr
*     u_int16_t priority
*     u_char def_up 
*     u_char def_dn
*     struct upstream_sflow *UpEntry,
*     u_char UpSize
*     struct downstream_sflow * DownEntry, 
*     u_char DownSize)
*
*  outputs : 
*     returns the CMTS's mac address (the mac variable  index_  )
*
*  Notes:
*
*****************************************************************************/
int MacDocsisCMTS::register_to_cmts(int macaddr, u_int16_t priority, u_char def_up, 
				    u_char def_dn, struct upstream_sflow *UpEntry,
				    u_char UpSize, struct downstream_sflow * DownEntry, 
				    u_char DownSize)
{
  int i = 0;
  char f = 0;
  int rc;
//$A310
  int newFlowID;
  
  //$A306
  //make sure the DS and US Service Flow Mgrs have been given the CmRecord pointer
  myUSSFMgr->setCMRecord(CmRecord);
  myDSSFMgr->setCMRecord(CmRecord);

#ifdef TRACE_CMTS 
  printf("mac-docsiscmts:register_to_cmts: ENTERED:registered default flows for CM (macaddr:%d) default US index:%d,  DS index: %d (SizeUpFlowTable:%d, SizeDownFlowTable:%d, CurrIndexCmTable:%d)\n",macaddr,def_up,def_dn,UpSize,DownSize,CurrIndexCmTable);
#endif 

  CmRecord[CurrIndexCmTable].cm_macaddr = macaddr;
  CmRecord[CurrIndexCmTable].priority = priority;
  CmRecord[CurrIndexCmTable].SizeDnFlTable = DownSize;
  CmRecord[CurrIndexCmTable].SizeUpFlTable = UpSize;
  CmRecord[CurrIndexCmTable].default_upstream_index_ = def_up;
  CmRecord[CurrIndexCmTable].default_downstream_index_ = def_dn;
  
  UpSchedType sclass;
  for (i = 0; i < UpSize; i++)
    {
      sclass = UpEntry[i].upstream_record.sched_type;
      CmRecord[CurrIndexCmTable].u_rec[i].sched_type = UpEntry[i].upstream_record.sched_type;
      CmRecord[CurrIndexCmTable].u_rec[i].gsize = UpEntry[i].upstream_record.gsize;
      CmRecord[CurrIndexCmTable].u_rec[i].ginterval = UpEntry[i].upstream_record.ginterval;
#ifdef TRACE_CMTS 
      if (sclass == BEST_EFFORT) 
        printf("mac-docsiscmts:register_to_cmts: registered flow: sched_type:BEST_EFFORT,gsize:%d, ginterval:%f\n",
                          CmRecord[CurrIndexCmTable].u_rec[i].gsize,
                          CmRecord[CurrIndexCmTable].u_rec[i].ginterval);
      else if (sclass == RT_POLL) 
        printf("mac-docsiscmts:register_to_cmts: registered flow: sched_type:RT_POLL,gsize:%d, ginterval:%f\n",
                          CmRecord[CurrIndexCmTable].u_rec[i].gsize,
                          CmRecord[CurrIndexCmTable].u_rec[i].ginterval);
      else if (sclass == UGS) 
        printf("mac-docsiscmts:register_to_cmts: registered flow: sched_type:UGS,gsize:%d, ginterval:%f\n",
                          CmRecord[CurrIndexCmTable].u_rec[i].gsize,
                          CmRecord[CurrIndexCmTable].u_rec[i].ginterval);
      else  
        printf("mac-docsiscmts:register_to_cmts: registered flow: sched_type:????,gsize:%d, ginterval:%f\n",
                          CmRecord[CurrIndexCmTable].u_rec[i].gsize,
                          CmRecord[CurrIndexCmTable].u_rec[i].ginterval);
#endif 
      
      if (bit_on(UpEntry[i].upstream_record.flag, FRAG_ENABLE_BIT))
	set_bit(&CmRecord[CurrIndexCmTable].u_rec[i].flag, FRAG_ENABLE_BIT,ON);
      
      if (bit_on(UpEntry[i].upstream_record.flag, CONCAT_ENABLE_BIT))
	set_bit(&CmRecord[CurrIndexCmTable].u_rec[i].flag, CONCAT_ENABLE_BIT,ON);
      
      CmRecord[CurrIndexCmTable].u_rec[i].PHS_profile = UpEntry[i].upstream_record.PHS_profile;

//$A310
//      CmRecord[CurrIndexCmTable].u_rec[i].flow_id = next_flowid;
//      UpEntry[i].upstream_record.flow_id = next_flowid;
      CmRecord[CurrIndexCmTable].u_rec[i].flow_id = UpEntry[i].upstream_record.flow_id;

      /*  Send these values in every Map */
      /*	UpEntry[i].bk_offstart = Conf_Table_.mapparam.bkoff_start;
		UpEntry[i].bk_offend = Conf_Table_.mapparam.bkoff_end;*/
      
      CmRecord[CurrIndexCmTable].u_rec[i].classifier.src_ip = UpEntry[i].upstream_record.classifier.src_ip;
      CmRecord[CurrIndexCmTable].u_rec[i].classifier.dst_ip = UpEntry[i].upstream_record.classifier.dst_ip;
      CmRecord[CurrIndexCmTable].u_rec[i].classifier.pkt_type = UpEntry[i].upstream_record.classifier.pkt_type;
//$A802
      CmRecord[CurrIndexCmTable].u_rec[i].classifier.flowID = UpEntry[i].upstream_record.classifier.flowID;

      CmRecord[CurrIndexCmTable].u_rec[i].SchedQType = UpEntry[i].upstream_record.SchedQType;
      CmRecord[CurrIndexCmTable].u_rec[i].SchedQSize = UpEntry[i].upstream_record.SchedQSize;
      CmRecord[CurrIndexCmTable].u_rec[i].SchedServiceDiscipline = UpEntry[i].upstream_record.SchedServiceDiscipline;
      CmRecord[CurrIndexCmTable].u_rec[i].flowPriority =  UpEntry[i].upstream_record.flowPriority;

      CmRecord[CurrIndexCmTable].u_rec[i].flowQuantum =  UpEntry[i].upstream_record.flowQuantum;
      CmRecord[CurrIndexCmTable].u_rec[i].flowWeight =  UpEntry[i].upstream_record.flowWeight;

      CmRecord[CurrIndexCmTable].u_rec[i].minRate =  UpEntry[i].upstream_record.minRate;
      CmRecord[CurrIndexCmTable].u_rec[i].minLatency =  UpEntry[i].upstream_record.minLatency;


//      next_flowid++;
#ifdef TRACE_CMTS 
      printf("mac-docsiscmts:register_to_cmts: registered additional US (index:%d,flow_id:%d), sched_type:%d, flowQuantum:%d, flowWeight:%2.2f,  gsize:%d, ginterval%f\n",
      i,
      CmRecord[CurrIndexCmTable].u_rec[i].flow_id,
      CmRecord[CurrIndexCmTable].u_rec[i].sched_type,
      CmRecord[CurrIndexCmTable].u_rec[i].flowQuantum,
      CmRecord[CurrIndexCmTable].u_rec[i].flowWeight,
      CmRecord[CurrIndexCmTable].u_rec[i].gsize,
      CmRecord[CurrIndexCmTable].u_rec[i].ginterval);
#endif 
	  //$A306 
	  //Add the SF
//	  myUSSFMgr->addServicFlow(macaddr,
//	              UpEntry[i].upstream_record.classifier.src_ip,
//			      UpEntry[i].upstream_record.classifier.dst_ip,
//				  UpEntry[i].upstream_record.classifier.pkt_type,
//                UpEntry[i].upstream_record.flow_id);
//$A310
      int numUSChannels = myPhyInterface->getNumberUSChannels();
      newFlowID = myPhyInterface->myReseqMgr->addServiceFlow(index_,
	              UpEntry[i].upstream_record.classifier.src_ip,
 		          UpEntry[i].upstream_record.classifier.dst_ip,
				  UpEntry[i].upstream_record.classifier.pkt_type,
                  UpEntry[i].upstream_record.flow_id,
                  UpEntry[i].upstream_record.BondingGroup, numUSChannels);
//$A802: need to set flowID

//#ifdef TRACE_CMTS 
      printf("mac-docsiscmts:register-to-cmts: Created new US reseq SFObject, newFlowID:%d (total#:%d),srcIP:%d, dstIP:%d, pktType:%d, flowID:%d,  bondingGroup:%d numberUSCHannels:%d\n",
                newFlowID,myPhyInterface->myReseqMgr->getNumberSFs(), 
	              UpEntry[i].upstream_record.classifier.src_ip,
 		          UpEntry[i].upstream_record.classifier.dst_ip,
				  UpEntry[i].upstream_record.classifier.pkt_type,
				  UpEntry[i].upstream_record.classifier.flowID,
                  UpEntry[i].upstream_record.BondingGroup,numUSChannels);
//#endif


    }
  
  for (i = 0; i < DownSize; i++)
    {

//$A310
//      CmRecord[CurrIndexCmTable].u_rec[i].flow_id = next_flowid;
      CmRecord[CurrIndexCmTable].d_rec[i].flow_id = DownEntry[i].downstream_record.flow_id;

//$A801
      CmRecord[CurrIndexCmTable].d_rec[i].dsid = DownEntry[i].downstream_record.dsid;

//      CmRecord[CurrIndexCmTable].d_rec[i].flow_id = next_flowid;
//      DownEntry[i].downstream_record.flow_id = next_flowid;

      CmRecord[CurrIndexCmTable].d_rec[i].PHS_profile = DownEntry[i].downstream_record.PHS_profile;

      CmRecord[CurrIndexCmTable].d_rec[i].classifier.src_ip = DownEntry[i].downstream_record.classifier.src_ip;
      CmRecord[CurrIndexCmTable].d_rec[i].classifier.dst_ip = DownEntry[i].downstream_record.classifier.dst_ip;
      CmRecord[CurrIndexCmTable].d_rec[i].classifier.pkt_type = DownEntry[i].downstream_record.classifier.pkt_type;
//$A802
      CmRecord[CurrIndexCmTable].d_rec[i].classifier.flowID = DownEntry[i].downstream_record.classifier.flowID;
      
      /* Token bucket support */
      //#ifdef RATE_CONTROL //---------------------------------------------------------------------------
      CmRecord[CurrIndexCmTable].d_rec[i].ratecontrol = DownEntry[i].downstream_record.ratecontrol;
      CmRecord[CurrIndexCmTable].d_rec[i].tokenq_ = new PacketQueue;
      CmRecord[CurrIndexCmTable].d_rec[i].rate_ = DownEntry[i].downstream_record.rate_;

      /*
	printf("cindex = %d findex = %d rate = %lf\n", 
	CurrIndexCmTable,
	i,
	CmRecord[CurrIndexCmTable].d_rec[i].rate_);
      */
      CmRecord[CurrIndexCmTable].d_rec[i].tokenqlen_ = DownEntry[i].downstream_record.tokenqlen_;;
      /*
	printf("cindex = %d findex = %d qlen = %d\n", 
	CurrIndexCmTable,
	i,
	CmRecord[CurrIndexCmTable].d_rec[i].tokenqlen_);
      */

      CmRecord[CurrIndexCmTable].d_rec[i].bucket_ = DownEntry[i].downstream_record.bucket_;;
      /*
	printf("cindex = %d findex = %d bucket = %d\n", 
	CurrIndexCmTable,
	i,
	CmRecord[CurrIndexCmTable].d_rec[i].bucket_);
      */
      CmRecord[CurrIndexCmTable].d_rec[i].tokens_ = 0;
      CmRecord[CurrIndexCmTable].d_rec[i].init_ = 1;
      //#endif //----------------------------------------------------------------------------------------
      CmRecord[CurrIndexCmTable].d_rec[i].util_total_bytes_DS = 0;
      CmRecord[CurrIndexCmTable].d_rec[i].util_total_pkts_DS = 0;
      CmRecord[CurrIndexCmTable].d_rec[i].total_pkts_dropped = 0;
      CmRecord[CurrIndexCmTable].d_rec[i].dropped_tokenq = 0;     
      CmRecord[CurrIndexCmTable].d_rec[i].dsq_delay = 0;
      CmRecord[CurrIndexCmTable].d_rec[i].dropped_dsq = 0;
      CmRecord[CurrIndexCmTable].d_rec[i].num_qsamples = 0;
         
//      next_flowid++;
//$A801
#ifdef TRACE_CMTS 
      printf("mac-docsiscmts:register_to_cmts: registered additional DS (index:%d,flow_id:%d,dsid:%d), sched_type:%d, schedServiceDiscipline:%d, flowQuantum:%d, flowWeight:%2.2f", 
      i,
      CmRecord[CurrIndexCmTable].d_rec[i].flow_id,
      CmRecord[CurrIndexCmTable].d_rec[i].dsid,
      CmRecord[CurrIndexCmTable].d_rec[i].SchedQType,
      CmRecord[CurrIndexCmTable].d_rec[i].SchedServiceDiscipline,
      CmRecord[CurrIndexCmTable].d_rec[i].flowPriority,
      CmRecord[CurrIndexCmTable].d_rec[i].flowQuantum,
      CmRecord[CurrIndexCmTable].d_rec[i].flowWeight);
#endif 

      CmRecord[CurrIndexCmTable].d_rec[i].SchedQType = DownEntry[i].downstream_record.SchedQType;
      CmRecord[CurrIndexCmTable].d_rec[i].SchedQSize = DownEntry[i].downstream_record.SchedQSize;
      CmRecord[CurrIndexCmTable].d_rec[i].SchedServiceDiscipline = DownEntry[i].downstream_record.SchedServiceDiscipline;
      CmRecord[CurrIndexCmTable].d_rec[i].flowPriority =  DownEntry[i].downstream_record.flowPriority;
      CmRecord[CurrIndexCmTable].d_rec[i].minRate =  DownEntry[i].downstream_record.minRate;
      CmRecord[CurrIndexCmTable].d_rec[i].minLatency =  DownEntry[i].downstream_record.minLatency;
      CmRecord[CurrIndexCmTable].d_rec[i].reseqFlag  =  DownEntry[i].downstream_record.reseqFlag;
      CmRecord[CurrIndexCmTable].d_rec[i].reseqWindow  = DownEntry[i].downstream_record.reseqWindow;
      CmRecord[CurrIndexCmTable].d_rec[i].reseqTimeout  = DownEntry[i].downstream_record.reseqTimeout;

      CmRecord[CurrIndexCmTable].d_rec[i].flowQuantum =  DownEntry[i].downstream_record.flowQuantum;
      CmRecord[CurrIndexCmTable].d_rec[i].flowWeight =  DownEntry[i].downstream_record.flowWeight;

	  //$A306 
	  //Add the SF
#if 0
	  newFlowID = myDSSFMgr->addServiceFlow( macaddr,
                    DownEntry[i].downstream_record.classifier.src_ip,
					DownEntry[i].downstream_record.classifier.dst_ip,
                    DownEntry[i].downstream_record.classifier.pkt_type,
                    DownEntry[i].downstream_record.flow_id,
                    DownEntry[i].downstream_record.BondingGroup);
     serviceFlowObject *mySFObj = myDSSFMgr->returnServiceFlowObject(DownEntry[i].downstream_record.flow_id);
#endif

	  DSserviceFlowObject *mySFObj = (DSserviceFlowObject *) myDSSFMgr->addServiceFlow(DownEntry[i].downstream_record.flow_id);
	  mySFObj->macaddr = macaddr;
//$A801
	  mySFObj->dsid = DownEntry[i].downstream_record.dsid;

	  mySFObj->classifier.src_ip = DownEntry[i].downstream_record.classifier.src_ip;
	  mySFObj->classifier.dst_ip = DownEntry[i].downstream_record.classifier.dst_ip;
	  mySFObj->classifier.pkt_type = DownEntry[i].downstream_record.classifier.pkt_type;
//$A802
	  mySFObj->classifier.flowID = DownEntry[i].downstream_record.classifier.flowID;
      mySFObj->myBGID = DownEntry[i].downstream_record.BondingGroup;
      // this is the real stuff for rate control settings
      mySFObj->ratecontrol = DownEntry[i].downstream_record.ratecontrol; 
      mySFObj->rate_ = DownEntry[i].downstream_record.rate_; 
      mySFObj->tokenqlen_ = DownEntry[i].downstream_record.tokenqlen_; 
      mySFObj->bucket_ = DownEntry[i].downstream_record.bucket_; 
      mySFObj->init_ = 1; // require init
      mySFObj->SchedQType = DownEntry[i].downstream_record.SchedQType;
      mySFObj->SchedQSize = DownEntry[i].downstream_record.SchedQSize;
      mySFObj->SchedServiceDiscipline = DownEntry[i].downstream_record.SchedServiceDiscipline;
      mySFObj->flowPriority = DownEntry[i].downstream_record.flowPriority;
      mySFObj->minRate = DownEntry[i].downstream_record.minRate;
      mySFObj->minLatency = DownEntry[i].downstream_record.minLatency;
      mySFObj->reseqFlag = DownEntry[i].downstream_record.reseqFlag;
      mySFObj->reseqWindow = DownEntry[i].downstream_record.reseqWindow;
      mySFObj->reseqTimeout = DownEntry[i].downstream_record.reseqTimeout;

      mySFObj->flowQuantum = DownEntry[i].downstream_record.flowQuantum;
      mySFObj->flowWeight = DownEntry[i].downstream_record.flowWeight;

//$A804
      mySFObj->MAXp =  DownEntry[i].downstream_record.MAXp;
      mySFObj->adaptiveMode =  DownEntry[i].downstream_record.adaptiveMode;


//  $A804  -  before we always assumed FCFS
//      mySFObj->setQueueBehavior( DownEntry[i].downstream_record.SchedQSize, DownEntry[i].downstream_record.SchedQType,0,0,0,0,0,0);
       //Note: RED, ARED, and BAQM will likely overwrite the initial settings
       //Set minth and maxth -  this is close to Floyd's recommendations : 5 and 3*
        double y = mySFObj->SchedQSize / 10;
        double x = mySFObj->SchedQSize / 2;

       if (mySFObj->SchedQType == RedQ)
         mySFObj->setQueueBehavior(mySFObj->SchedQSize, mySFObj->SchedQType, mySFObj->flowPriority, (int)y,(int)x,0,mySFObj->MAXp, DEFAULT_FILTER_TIME_CONSTANT);
       else  if (mySFObj->SchedQType == AdaptiveRedQ)
         mySFObj->setQueueBehavior(mySFObj->SchedQSize, mySFObj->SchedQType, mySFObj->flowPriority, (int)y,(int)x,1,mySFObj->MAXp, DEFAULT_FILTER_TIME_CONSTANT);
       else  if (mySFObj->SchedQType == DelayBasedRedQ)
         mySFObj->setQueueBehavior(mySFObj->SchedQSize, mySFObj->SchedQType, mySFObj->flowPriority, (int)y,(int)x,2,mySFObj->MAXp, DEFAULT_FILTER_TIME_CONSTANT);
       else  if (mySFObj->SchedQType == BAQMQ)
         mySFObj->setQueueBehavior(mySFObj->SchedQSize, mySFObj->SchedQType, mySFObj->flowPriority, (int)y,(int)x,0,mySFObj->MAXp, DEFAULT_FILTER_TIME_CONSTANT);
       else
         mySFObj->setQueueBehavior(mySFObj->SchedQSize, mySFObj->SchedQType, mySFObj->flowPriority, (int)y,(int)x,mySFObj->adaptiveMode,mySFObj->MAXp, DEFAULT_FILTER_TIME_CONSTANT);


#ifdef TRACE_CMTS 
//For test...	  rc = myDSSFMgr->removeServiceFlow(DownEntry[i].downstream_record.flow_id);
//     printf("mac-docsiscmts:register_to_cmts: removed the SF, rc=%d\n",rc);
#endif

	 rc = myBGMgr->addSFToBG(DownEntry[i].downstream_record.BondingGroup,mySFObj);

//#ifdef TRACE_CMTS 
     printf("mac-docsiscmts:register_to_cmts: Created new DS SFObject, newFlowID:%d (total#:%d), bondingGroup:%d, add flow:%d (dssid:%d) to addSFToBG (initial flowQuantum:%d, flowWeight:%2.2f) (rc:%d)\n",
			    newFlowID,myDSSFMgr->getNumberSFs(), DownEntry[i].downstream_record.BondingGroup,mySFObj->flowID, mySFObj->dsid,mySFObj->flowQuantum,mySFObj->flowWeight,rc);
//#endif

    }
  CurrIndexCmTable++;
  return index_;  
} 

/*****************************************************************************
Packet coming up from PHY layer...
//TODO: Remove this method
*****************************************************************************/
void MacDocsisCMTS::sendUp(Packet* p) 
{
	;
}

/****************************************************************************
 *  void MacDocsisCMTS::RecvFrame(Packet* p, int concat_flag)
 *
 * Function:   
 *      Called from the recvHandler when a frame has arrived on the upstream 
 *      channel.            
 *             Needed to make sure we increment byte stats only once as
 *             this routine is reentered if the frame is concatonated
 *             static int RecvFrameEntered = 0;
 T      This routine will invoke HandleInData or HandleInMgmt ....
 *
 * Parameters:
 *    Packet *p:  A packet that already has been adjusted for all headers
 *    int concat_flag:  true if we are being reentered due to a concatonated
 *    frame.  In this case, we've already counted the packet, but we need to count the size.
 *    If 2, then this is called once we have reassembled a frag.  We should not count the bytes 
 *    in this case.
 *    It is 0 if this is a new frame (and we are not being reentered)
 *    TODO:  Maybe change this so it's not called when a frag is reassembed ?
 *
 * Design Notes:
 *
 *
 * *************************************************************************/
void MacDocsisCMTS::RecvFrame(Packet* p, int concat_flag)
{
  struct hdr_cmn* ch = HDR_CMN(p);
  struct hdr_docsis* dh = HDR_DOCSIS(p);
  struct hdr_mac *mh = HDR_MAC(p);
  int cindex, findex;
  int t;
  u_char req;
  u_int16_t flow_id;
  
#ifdef TRACE_DEBUG
  tracePoint("CMTS:RecvFrame:",concat_flag,0);
#endif

  if (concat_flag == 0) {
    MACstats.total_num_frames_US++;
    MACstats.total_num_BW_bytesUP += ch->size();
    MACstats.total_num_rx_bytes += ch->size();
    MACstats.total_num_rx_frames++;
  }
//  else
  MACstats.total_num_rx_PDUs++;
  
  
  t = ClassifyDataMgmt(p);

#ifdef TRACE_CMTS //---------------------------------------------------------
	  
  printf("CMTS:RecvFrame(%lf): CMTS received a docsis frame (size:%d, total_num_frames_US:%d), mgmt flag:%d, concat_flag:%d (mac SA:%d, DA:%d\n", 
	 Scheduler::instance().clock(),ch->size(),MACstats.total_num_frames_US,t,concat_flag, mh->macSA(),mh->macDA());
#endif //--------------------------------------------------------------------

  if (t == 0) /* if 0,  the frame contains data */
  {
      
    cindex = classify(p, UPSTREAM, &findex);

#ifdef TRACE_CMTS //--------------------------------------------------------------------
      printf("CMTS:recvFrame: Packet classified on flow-id = %d\n",
	     CmRecord[cindex].u_rec[findex].flow_id);
#endif //-------------------------------------------------------------------------------

    //dump_pkt(p);	
    PhUnsupress(p,cindex,findex);
      
      
	//Remember, 0,1, or 2
    if (concat_flag == 1) {
	  //$A401
	  MACstats.total_num_concatdata_pkts_US++;
	}
    if (concat_flag == 0) {
	  MACstats.total_num_plaindata_pkts_US++;
	}
      
#ifdef TRACE_CMTS_UP_DATA //--------------------------------------------------------------------
    printf("CMTSup %lf %f %d\n",
	     Scheduler::instance().clock(),MACstats.total_num_rx_bytes,ch->size());
#endif //-------------------------------------------------------------------------------
      
      
    HandleInData(p, cindex,findex);
  }
  else /*else the frame contains a mac specific header (i.e., a request) */
  {


#ifdef TRACE_CMTS //---------------------------------------------------------
      printf("CMTSrecvFrame:Mgmt Frame ....total_num_rx_frames:%d \n",
		       MACstats.total_num_rx_frames);
#endif //--------------------------------------------------------------------


    HandleInMgmt(p);
      
  }
#ifdef TRACE_CMTS //---------------------------------------------------------
  printf("CMTS:RecvFrame(%lf): Exiting. ch->size:%d,total_num_rx_frames:%d \n",
	 Scheduler::instance().clock(),ch->size(),MACstats.total_num_rx_frames);
#endif //--------------------------------------------------------------------
  return;
}

/*****************************************************************************
A simple ethernet PDU is carried in the Docsis payload..
No Fragmentation or Concatenation

We normally would not get here.....as we CM to CM is not of high interest
*****************************************************************************/
void MacDocsisCMTS::PassDownstream(Packet* p)
{
  /* Simply pass the pkt by applying new PHS ... */

  struct hdr_cmn * ch = HDR_CMN(p);
  int cindex,findex;
  double stime;


#ifdef TRACE_CMTS 
  printf("CMTS:PassDownstream(%lf): packet going down ch->size:%d\n", Scheduler::instance().clock(),ch->size());
#endif 
  
  MACstats.total_num_appbytesDS += ch->size();
  MACstats.total_num_appPktsDS++;
  ch->size() += ETHER_HDR_LEN;
  ch->size() += DOCSIS_HDR_LEN;
  
  cindex = classify(p, DOWNSTREAM,&findex);
  ApplyPhs(p,cindex,findex);
  
  /* Apply rate-control SendFrame will be called from rate-control */
  CmRecord[cindex].d_rec[findex].util_total_bytes_DS += ch->size();
  CmRecord[cindex].d_rec[findex].util_total_pkts_DS ++;
  
  //#ifdef RATE_CONTROL //-------------------------------------------------------
#if 0
  if (CmRecord[cindex].d_rec[findex].ratecontrol) /* Rate-control on*/
    {
      if (ClassifyDataMgmt(p)) 
        RateControl(p,cindex,findex,MGMT_PKT);
      else
        RateControl(p,cindex,findex,DATA_PKT);
      return;
    }
#else
  // this part of code is not normally run
  int type = ClassifyDataMgmt(p) ? MGMT_PKT : DATA_PKT;
  serviceFlowObject *mySFObj = myDSSFMgr->getServiceFlow(p,type);
   printf("CMTS:PassDownStream: HARD ERROR.....NEVER GET HERE ??? \n\n");
   exit(1);
  if (mySFObj != NULL) {
      if (mySFObj->isRateControlled()) {
          RateControl(p, mySFObj, cindex, findex, type);
          return;
      }
  }
#endif
  //#endif //--------------------------------------------------------------------
  
  /* Send the packet */

  if (ClassifyDataMgmt(p)) 
    MacSendFrame(p,MGMT_PKT);
  else
    MacSendFrame(p,DATA_PKT);

  return;  
}

/*****************************************************************************
* routine:  void MacDocsisCMTS::HandleInData(Packet *p, int cindex, int findex)
* function: 
*    Called when pkt has arrived on US channel.
*    overHeadBitsStripped is 1 if the packet already has had the OH bits taken off  
*    (This occurs when multiple frames are concatonated)
*
*
*****************************************************************************/
void MacDocsisCMTS::HandleInData(Packet *p, int cindex, int findex)
{
  int dst, src, target = 0;
  struct hdr_cmn* ch = HDR_CMN(p);
  struct hdr_docsis* dh = HDR_DOCSIS(p);
  struct hdr_mac* mh = HDR_MAC(p);
  
  UpSchedType sclass;
  dst = mh->macDA();
  src = mh->macSA();

  sclass = CmRecord[cindex].u_rec[findex].sched_type;
  
#ifdef TRACE_CMTS 
  printf("CMTS:HandleInData(%lf): this frame (size:%d)  has arrived\n", 
	 Scheduler::instance().clock(),ch->size());
      printf("CMTS:HandleInData:(%d), sched_type:%d, gsize:%d, ginterval%f\n",
      CmRecord[cindex].u_rec[findex].flow_id,
      CmRecord[cindex].u_rec[findex].sched_type,
      CmRecord[cindex].u_rec[findex].gsize,
      CmRecord[cindex].u_rec[findex].ginterval);
#endif 

 
  
  if (sclass == BEST_EFFORT) {
    MACstats.total_num_BE_pkts_US++;
  }
  
  else if (sclass == RT_POLL) {
    MACstats.total_num_RTVBR_pkts_US++;
  }
  
  else if (sclass == UGS) {
    MACstats.total_num_UGS_pkts_US++;
  }
    
  else{
    MACstats.total_num_OTHER_pkts_US++;
  }
  
  
  
  /* Whether or not the packet is destined for me, I need to check
   * whether an extended header containing piggyback req is present
   */
  ParseExtHdr(p);
  
  /* Not a packet destinated to me. */
  if ((dst != MAC_BROADCAST) && (dst != (u_int32_t)index_)) 
    {      
      /* Forward the packets between the CMs */
      
      /* 
	 This shud take care of ARP requests also..i.e ARP requests are passed
	 on downstream channel 
      */
      target = find_cm(dst);
      if (target) 
	{
#ifdef TRACE_CMTS //---------------------------------------------------------
	  printf("Forwarding the pkt to cm %d\n",target);
	  dump_pkt(p);
#endif //--------------------------------------------------------------------
	  
	  //$A405
	  ch->size() -= ETHER_HDR_LEN;
	  ch->size() -= DOCSIS_HDR_LEN;
	  //$A405
	  ch->num_forwards() += 1;
	  MACstats.total_num_appbytesUS += ch->size();
	  MACstats.total_num_appPktsUS++;

#ifdef TRACE_CMTS //---------------------------------------------------------
         printf("CMTS:HandleInData(%lf):  Forwarding the pkt (size:%d) to cm %d\n",Scheduler::instance().clock(),
			ch->size(),target);
#endif //--------------------------------------------------------------------
	  
	  PassDownstream(p);
	  return;
	}            
//#ifdef TRACE_CMTS 
        else
         printf("CMTS:HandleInData(%lf):  HARD ERROR   find_cm returned error \n",Scheduler::instance().clock());
//#endif 
    }  
  /* 
     Will have to decide whether to change the src-mac address when passing ARP 
     requests down as it is possible that CMs might not be having ip-addresses 
     of other CMs in their routing table 
  */
  
  /* Now, simply strip the ethernet , docsis header & pass the packet to upper layer */
  
  ch->size() -= ETHER_HDR_LEN;
  ch->size() -= DOCSIS_HDR_LEN;
  
  //$A405
  ch->num_forwards() += 1;  
  MACstats.total_num_appbytesUS += ch->size();
  MACstats.total_num_appPktsUS++;
  
#ifdef TRACE_CMTS //---------------------------------------------------------
  printf("CMTS:HandleInData(%lf): passing to uptarget... size is now: %d, sched_type:%d\n", 
	 Scheduler::instance().clock(),ch->size(),sclass);
#endif //--------------------------------------------------------------------
  
#ifdef TRACE_DEBUG
  tracePoint("CMTS:HandleInData", CmRecord[cindex].u_rec[findex].flow_id, ch->size());
#endif

//$A306
//This is the top LL interface for frames received on this node from the DOCSIS channel
  myUSpacketMonitor->updateStats(p);
  myUSpacketMonitor->packetTrace(p);
  uptarget_->recv(p, (Handler*) 0);
  return;  
}

/*****************************************************************************

*****************************************************************************/
void MacDocsisCMTS::ParseExtHdr(Packet * p)
{
  struct hdr_cmn* ch = HDR_CMN(p);
  struct hdr_docsis* dh = HDR_DOCSIS(p);
  struct hdr_docsisextd* df = HDR_DOCSISEXTD(p);
  int piggy_index = -1;

  /* Check if extended header is ON */  
  if (dh->dshdr_.ehdr_on == 1)
    {
      for (int i = 0; i < df->num_hdr; i++)
	{
	  if (df->exthdr_[i].eh_type == 1)
	    piggy_index = i;
	}
      if (piggy_index == -1)
	return;
      
      if (df->exthdr_[piggy_index].eh_type == 1) /* Piggyback req */
	{
	  u_int16_t flow_id;
	  u_char req;
	  int cindex, findex;
	  flow_id = df->exthdr_[piggy_index].eh_data[1];
	  req = df->exthdr_[piggy_index].eh_data[0];
	  find_flowindex(flow_id,&cindex,&findex);
	  
#ifdef TRACE_CMTS //----------------------------------------------------------------
	  printf("ParseExtHdr(%lf): Received a Piggyback request for flow-id %d \n",
		 Scheduler::instance().clock(),flow_id);
#endif //---------------------------------------------------------------------------
	  
	  // Reduce the size..
	  ch->size() -= 3;
	  CMTSstats.num_piggyreq++;
	  HandleReq(req, cindex, findex);
	}      
    }
  else
    return;
}

/*****************************************************************************
*  function:  void MacDocsisCMTS::HandleInMgmt(Packet *p)
*
*  explanation:
*   This routine receives a Mgt message of the following types:
*    case 2:  request header
*    case 3:  Fragmentation frame header 
*    case 26: Concatenation frame header
*    case 1:  RNG msg
* 
*    Note: if it's a concat frame-  this means its a concat request.
*
*  inputs: Packet *p : the packet
*
*  outputs : none
*
*
*****************************************************************************/
void MacDocsisCMTS::HandleInMgmt(Packet *p)
{
  struct hdr_cmn* ch = HDR_CMN(p);
  struct hdr_docsis* dh = HDR_DOCSIS(p);
  u_char req,concat_fr = 0;
  u_char frag_fr = 0;
  int cindex,findex;   /* Need to call classify for best-effort flow... */
  u_int16_t flow_id;
  struct status_docsismgmt statusMsg;
  u_char *srcPtr,*dstPtr;
  int msgSize = 0;
  int i;

  MACstats.num_mgmtpkts++;
  MACstats.num_mgmtbytes += ch->size();

  ParseExtHdr(p);
  
#ifdef TRACE_DEBUG
  tracePoint("CMTS:HandleInMgmt", dh->dshdr_.fc_parm, ch->size());
#endif

#ifdef TRACE_CMTS //---------------------------------------------------------
  printf("HandleInMgmt(%lf): channel:%d,  fc_parm = %d\n", Scheduler::instance().clock(), dh->dshdr_.fc_parm);
#endif //--------------------------------------------------------------------
  switch(dh->dshdr_.fc_parm) 
    {
    case REQUEST: /* Request frame header */
      req = dh->dshdr_.mac_param ;
      flow_id = dh->dshdr_.len;
      find_flowindex(flow_id,&cindex,&findex);
      CMTSstats.num_creq++;
      MACstats.total_num_req_pkts_US++; 
#ifdef TRACE_CMTS //---------------------------------------------------------
      printf("HandleInMgmt:  req=%d, flow_id=%d\n",req,flow_id);
#endif //--------------------------------------------------------------------
      HandleReq(req, cindex, findex);
      break;
      
    case FRAGMENT: /* Fragmentation frame header */
      //dump_pkt(p);
      
      frag_fr = 1;
      MACstats.total_num_frag_pkts_US++; 
      HandleFrag(p);
      break;
      
    case CONCATENATION: /* Concatenation frame header */
      //cindex = classify(p, UPSTREAM, &findex);

#ifdef TRACE_CMTS //---------------------------------------------------------
      cindex = classify(p, UPSTREAM, &findex);
      printf("Concatenation:  cindex = %d, findex = %d\n",cindex,findex);
#endif //--------------------------------------------------------------------

      concat_fr = 1;
      MACstats.total_num_concat_pkts_US++; 
      HandleConcat(p, cindex, findex);
      break;	

    case MANAGEMENT:
      /* Rng Message */
      /* Do nothing */
      MACstats.total_num_rng_pkts_US++; 
      break;       

    case STATUS:
      /* status message */
      MACstats.total_num_status_pkts_US++; 
      srcPtr = (u_char *)p->accessdata();
      dstPtr = (u_char *)&statusMsg;
      msgSize = dh->dshdr_.mac_param;
#ifdef TRACE_CMTS 
      printf("HandleInMgt:(%lf) Received a CM status message of pkt size:%d, msg Size:%d\n",
        Scheduler::instance().clock(),ch->size(), msgSize);
#endif
      //copy the data into the struct
      for (i=0;i<msgSize;i++) {
       *dstPtr++ = *srcPtr++;
      }

#ifdef TRACE_CMTS 
     printf("HandleInMgt:(%lf) status msg(cm_id:%d): CMTSUSByteCount:%d, DSByteCount:%d USCR:%d, bkoffScale:%d, USRAD(us):%d, USNRAD(us):%d\n",
        Scheduler::instance().clock(), statusMsg.cm_id, statusMsg.USByteCount,
        statusMsg.DSByteCount, statusMsg.USCollisionRate, statusMsg.currentBkOffScale,statusMsg.USRAD,statusMsg.USNRAD);
#endif

      HandleStatusMsg(&statusMsg);

      break;       

    default:  //Error
//#ifdef TRACE_CMTS 
      printf("HandleInMgt:  Error:  fc_parm: %d\n",dh->dshdr_.fc_parm);
//#endif
      break;
    }
  
  double curr_time = Scheduler::instance().clock();
  
  if (curr_time - MACstats.last_mmgmttime >= 1.0)
    {
      MACstats.last_mmgmttime = curr_time;
      MACstats.avg_mgmtpkts = (MACstats.avg_mgmtpkts + MACstats.num_mgmtpkts )/ 2;
      MACstats.avg_mgmtbytes = (MACstats.avg_mgmtbytes + MACstats.num_mgmtbytes )/ 2;
      MACstats.num_mgmtpkts = 0;
      MACstats.num_mgmtbytes = 0;

#ifdef TRACE_CMTS //-------------------------------------------------------
      printf("Avg Management pkts received %d Avg Management bytes %d \n",
	     MACstats.avg_mgmtpkts,MACstats.avg_mgmtbytes);
#endif //------------------------------------------------------------------
    }
  
  
  	
  Packet::free(p);
  p = 0;  
  return;  
}

/***************************************************************************
*
* Routine: void HandleStatusMsg(struct status_docsismgmt *statusMsg)
*
* Explanation:
*   This is invoked evertime a status msg is received.  Every 
*   ADAPTATION_UPDATE  time we recompute the CM's stats
*
*   And once every ADAPTATION_FREQUENCY we recompute the new backoff param
*   
*   The adaptation algorithm to be used is based on 
*       #define ADAPTATION_ALGORITHM  1
*        #0 : no adaptation
*        #1 : new scale param is min(1,100 *  percentage of US BW consumed )
*
*  inputs: 
*   struct status_docsismgmt *statusMsg : ptr to the status msg that arrived
*
*
*  outputs : none
*
*  Notes:
*    Refers to cm_status_data and modifies cm_status_update_data
*
**************************************************************/
void MacDocsisCMTS::HandleStatusMsg(struct status_docsismgmt *statusMsg)
{

int cm_id = statusMsg->cm_id - 1;
double x,y;
double current_time = Scheduler::instance().clock();
double timeInterval;
int algoType =  ADAPTATION_ALGORITHM; 
double movingAvgWeight = 1.0;
FILE* Sfp = NULL;



#ifdef TRACE_CMTS 
  printf("HandleStatusMsg:(%lf) (cm_id:%d): CMTSUSByteCount:%d, DSByteCount:%d USCR:%d, bkoffScale:%d, USRAD(us):%d, USNRAD(us):%d, timeInterval:%d\n",
        current_time, statusMsg->cm_id, statusMsg->USByteCount,
        statusMsg->DSByteCount, statusMsg->USCollisionRate, statusMsg->currentBkOffScale,statusMsg->USRAD,statusMsg->USNRAD,statusMsg->timeSinceLastStatus);
#endif

  timeInterval = ((double) statusMsg->timeSinceLastStatus)/1000000;

#ifdef TRACE_CMTS 
  printf("HandleStatusMsg:(%lf) (cm_id:%d): Before:  CmStatusData: numberSamples:%d, USBW:%f, timeInterval:%f, USBW:%f (Update.nextScaleParam %d)\n",
        current_time, statusMsg->cm_id, CmStatusData[cm_id].numberSamples,CmStatusData[cm_id].USBW,timeInterval,
        CmStatusData[cm_id].USBW + ((double)statusMsg->USByteCount*8 / timeInterval),
        CmStatusUpdateData[cm_id].nextScaleParam);
#endif

  CmStatusData[cm_id].USBW = CmStatusData[cm_id].USBW +  ((double)statusMsg->USByteCount*8 / timeInterval);
  CmStatusData[cm_id].DSBW = CmStatusData[cm_id].DSBW +  ((double)statusMsg->DSByteCount*8 / timeInterval);
  CmStatusData[cm_id].CR = CmStatusData[cm_id].CR + ((double)statusMsg->USCollisionRate / 1000);
  CmStatusData[cm_id].USRAD = CmStatusData[cm_id].USRAD + ((double)statusMsg->USRAD / 1000000);
  CmStatusData[cm_id].USNRAD = CmStatusData[cm_id].USNRAD + (double)statusMsg->USNRAD;
//  CmStatusData[cm_id].USNRAD = CmStatusData[cm_id].USNRAD + ((double)statusMsg->USNRAD / 1000000);
  CmStatusData[cm_id].currentScaleParam = statusMsg->currentBkOffScale;
  CmStatusData[cm_id].numberSamples++;


  //so we recompute each CM as messages arrive
  if (((current_time - CmStatusData[cm_id].lastResultsCalculation) > ADAPTATION_UPDATE) && 
       (CmStatusData[cm_id].numberSamples > 0)) {
    //Compute a new result...
    //use a moving avg .... set to 1
//    movingAvgWeight = .95;
    movingAvgWeight = 1.0;

    CmStatusUpdateData[cm_id].USBW = 
       (movingAvgWeight * (CmStatusData[cm_id].USBW / CmStatusData[cm_id].numberSamples)) + 
       ((1-movingAvgWeight) * CmStatusData[cm_id].USBW);
    CmStatusUpdateData[cm_id].DSBW = 
       (movingAvgWeight * (CmStatusData[cm_id].DSBW / CmStatusData[cm_id].numberSamples)) + 
       ((1-movingAvgWeight) * CmStatusData[cm_id].DSBW);

    CmStatusUpdateData[cm_id].CR = 
       (movingAvgWeight * (CmStatusData[cm_id].CR / CmStatusData[cm_id].numberSamples)) + 
       ((1-movingAvgWeight) * CmStatusData[cm_id].CR);

    CmStatusUpdateData[cm_id].USRAD = 
       (movingAvgWeight * (CmStatusData[cm_id].USRAD / CmStatusData[cm_id].numberSamples)) + 
       ((1-movingAvgWeight) * CmStatusData[cm_id].USRAD);

    CmStatusUpdateData[cm_id].USNRAD = 
       (movingAvgWeight * (CmStatusData[cm_id].USNRAD / CmStatusData[cm_id].numberSamples)) + 
       ((1-movingAvgWeight) * CmStatusData[cm_id].USNRAD);

    //Make sure the next scale param is not below 1
    if (CmStatusUpdateData[cm_id].nextScaleParam < 1)
     CmStatusUpdateData[cm_id].nextScaleParam = 1;


//    CmStatusUpdateData[cm_id].USBW = CmStatusData[cm_id].USBW / CmStatusData[cm_id].numberSamples;
//    CmStatusUpdateData[cm_id].DSBW = CmStatusData[cm_id].DSBW / CmStatusData[cm_id].numberSamples;
//    CmStatusUpdateData[cm_id].CR = CmStatusData[cm_id].CR / CmStatusData[cm_id].numberSamples;
//    CmStatusUpdateData[cm_id].USRAD = CmStatusData[cm_id].USRAD / CmStatusData[cm_id].numberSamples;
//    CmStatusUpdateData[cm_id].USNRAD = CmStatusData[cm_id].USNRAD / CmStatusData[cm_id].numberSamples;


#ifdef TRACE_CMTS 
    printf("HandleStatusMsg:(%lf) (cm_id:%d): Update: USBW:%f, DSBW:%f, CR:%f, USRAD:%f, USNRAD:%f, numberSamples:%d (nextScaleP:%d)\n",
        current_time, statusMsg->cm_id, CmStatusUpdateData[cm_id].USBW,
        CmStatusUpdateData[cm_id].DSBW,
        CmStatusUpdateData[cm_id].CR,
        CmStatusUpdateData[cm_id].USRAD,
        CmStatusUpdateData[cm_id].USNRAD,
        CmStatusData[cm_id].numberSamples,
        CmStatusUpdateData[cm_id].nextScaleParam);
#endif
    CmStatusUpdateData[cm_id].lastUpdateTime = current_time,
    CmStatusData[cm_id].lastResultsCalculation = current_time;
    CmStatusData[cm_id].numberSamples = 0;
    CmStatusData[cm_id].USBW = 0;
    CmStatusData[cm_id].DSBW = 0;
    CmStatusData[cm_id].CR = 0;
    CmStatusData[cm_id].USRAD = 0;
    CmStatusData[cm_id].USNRAD = 0;

//  Compute the next scale param for this CM
    if ((current_time - lastCMUpdateTime) > ADAPTATION_FREQUENCY) { 
      lastCMUpdateTime = current_time;
      switch(algoType)
      {
        case 0:
//Don't change anything....
          break;
        case 1:
          AdjustCMBackOff1(CmStatusUpdateData,movingAvgWeight);
          CmStatusUpdateData[cm_id].prevScaleParam = CmStatusUpdateData[cm_id].nextScaleParam;

          break;
        default:
          CmStatusUpdateData[cm_id].nextScaleParam = 1;
          CmStatusUpdateData[cm_id].prevScaleParam = CmStatusUpdateData[cm_id].nextScaleParam;
 #ifdef TRACE_CMTS 
          printf("HandleStatusMsg:(%lf) (cm_id:%d): Bad algo Type (%d)?? \n",
              current_time, statusMsg->cm_id, algoType);
#endif
          break;
      }
    }
#ifdef FILES_OK
    if (DUMP_ADAPTATION_RESULTS == 1) {
      Sfp = fopen("adaptation.dat", "a+");
      fprintf(Sfp,"%d %f %f %f %f %f %f %d %d \n",statusMsg->cm_id,current_time,
        CmStatusUpdateData[cm_id].USBW,
        CmStatusUpdateData[cm_id].DSBW,
        CmStatusUpdateData[cm_id].CR,
        CmStatusUpdateData[cm_id].USRAD,
        CmStatusUpdateData[cm_id].USNRAD,
        CmStatusUpdateData[cm_id].prevScaleParam,
        CmStatusUpdateData[cm_id].nextScaleParam);
      fclose(Sfp);
    }
#endif
  }
  if (readyToSendUpdate == 1) {

    SendCMUpdateMsg();
 #ifdef TRACE_CMTS 
          printf("HandleStatusMsg:(%lf):Send CM Update Msg ?? \n",current_time);
#endif

    readyToSendUpdate = 0;
  }

}

/***************************************************************************
*
* Routine: int MacDocsisCMTS::AdjustCMBackOff1(struct cm_status_update_data *latestCMResults)
*
* Explanation:
*   This is invoked when the CMTS needs to readjust the backoff params
*   The algorithm is:
*        #1 : new scale param is min(1,100 *  percentage of US BW consumed )
*
*  inputs: 
*   struct cm_status_update_data *updated_results
*
*
*  outputs : returns the new backoff param in the range  [1,100]
*            Updates the global variable 
*
*  Notes:
*
**************************************************************/
void MacDocsisCMTS::AdjustCMBackOff1(struct cm_status_update_data *latestCMResults, double movingAvgWeight)
{
int numberCMs = SizeCmTable;
int i;
double current_time = Scheduler::instance().clock();
double totalUSBW=0;
double x,y;
int nextBackOff = 1;
FILE* Sfp = NULL;

#ifdef TRACE_CMTS 
  printf("AdjustCMBackOff1:(%lf) for %d CMs \n", 
      current_time,numberCMs);
#endif

  //Do the update
  // Pass 1- totals
  for (i=0;i<numberCMs;i++) {
    totalUSBW+= (double) CmStatusUpdateData[i].USBW;
  }

  //Pass 2
#ifdef FILES_OK
  Sfp = fopen("SCALEPARAM.dat", "w+");
#endif
  for (i=0;i<numberCMs;i++) {
    x = (double)CmStatusUpdateData[i].USBW / totalUSBW;
    nextBackOff = (int) (100.0 * x);
    if (nextBackOff < 1) {
      nextBackOff = 1;
    }
    y =  (movingAvgWeight * (double)nextBackOff) + 
          ((1-movingAvgWeight) * (double)CmStatusUpdateData[i].nextScaleParam);
    if (y < 1.0) {
      y = 1.0;
    }
    CmStatusUpdateData[i].nextScaleParam = (int) y;
#ifdef TRACE_CMTS 
    printf("AdjustCMBackOff1:(%lf) CM:%d: nextBackOff:%d, y:%f,  nextScaleParam:%d  \n", 
        current_time,i+1,nextBackOff,y, CmStatusUpdateData[i].nextScaleParam);
#endif
#ifdef FILES_OK
    fprintf(Sfp,"%d %f %d \n",i+1, current_time,CmStatusUpdateData[i].nextScaleParam);
#endif
  }

#ifdef FILES_OK
  fclose(Sfp);
#endif
  readyToSendUpdate = 1;
  //In the future we might reduce the number of CMs that should look at this data 
  //But for now assume all CMs must look at the CMUpdate message
  numberCMsToUpdate = SizeCmTable;

}


/*****************************************************************************

*****************************************************************************/
void MacDocsisCMTS::find_flowindex(u_int16_t flow_id, int* cindex, int* findex)
{
  int i,j;
  
  for (i = 0; i < SizeCmTable; i++)
    {
      for (j = 0; j < CmRecord[i].SizeUpFlTable; j++) /* For each flow */
	{
	  if (CmRecord[i].u_rec[j].flow_id == flow_id)
	    {
	      *cindex = i;
	      *findex = j;
	      return;
	    }
	}
    }
  fprintf(stderr, "MacDocsisCMTS::find_flowindex error, flow-id not found, exiting\n");
  exit(1);
}

/*****************************************************************************
 * Routine: void MacDocsisCMTS::HandleFrag(Packet *p)
 *
 * inputs:  The frame containing a fragment
 *
 * output:
 *
 * Note:  the total_num_BE_pkts counter includes each fragment
 *
*****************************************************************************/
void MacDocsisCMTS::HandleFrag(Packet *p)
{
  struct hdr_cmn* ch = HDR_CMN(p);
  struct hdr_docsisextd* eh = HDR_DOCSISEXTD(p);
  struct hdr_docsis* dh = HDR_DOCSIS(p);
  int * data = (int *)p->accessdata();
  int cindex,findex;
  u_int16_t flow_id;
  int frag_index = -1;


  for (int i = 0; i < eh->num_hdr; i++)
    {
      if (eh->exthdr_[i].eh_type == 3)
	frag_index = i;
    }
#ifdef TRACE_CMTS //---------------------------------------------------------
  printf("CMTS:HandleFrag(%lf):Fragment frame received, number headers: %d\n",
	 Scheduler::instance().clock(),eh->num_hdr);
#endif //--------------------------------------------------------------------

  if (frag_index == -1)
    {
#ifdef ERROR_TRACE 
      printf("HandleFrag: HARD  ERROR: Fragmentation header not present in extended header\n");
#endif
      fprintf(stderr, "exiting\n");
      exit(1);
    }
  /* classify the fragmented pkt on the flow.. */
  flow_id = eh->exthdr_[frag_index].eh_data[5];
  find_flowindex(flow_id,&cindex,&findex);

#ifdef TRACE_CMTS //---------------------------------------------------------
  printf("CMTS:HandleFrag(%lf):Fragmented packet received on flow %d, (size:%d)\n",
	 Scheduler::instance().clock(), flow_id,ch->size());
#endif //--------------------------------------------------------------------
  
  MACstats.total_num_BE_pkts_US++;
  
  if (bit_on(CmRecord[cindex].u_rec[findex].flag, FRAG_ON_BIT))
    {
      if (!dh->dshdr_.ehdr_on)
	{
#ifdef ERROR_TRACE 
	  printf("MacDocsisCMTS->handle_frag: ERROR: EHDR not on in fragmented packet, Discarding the packet\n");
#endif
	  drop(p);
	  //Also, at other error points in this routine, we need to propagate an error to the caller...
	  //MACstats.total_packets_dropped++;    
	  CmRecord[cindex].u_rec[findex].frag_pkt = 0;
	  set_bit(&CmRecord[cindex].u_rec[findex].flag, FRAG_ON_BIT,OFF);
	  CmRecord[cindex].u_rec[findex].frag_data = 0;
	  CmRecord[cindex].u_rec[findex].seq_num = 0;
	  return;	  
	}
      
      if (eh->exthdr_[frag_index].eh_data[2] == 1) /* First Fragment flag */
	{
#ifdef ERROR_TRACE 
	  printf("Flow_id %d MacDocsisCMTS->handle_frag: ERROR: First fragment flag set in middle fragments\n",flow_id);
#endif
	  //	drop(p);
	  CmRecord[cindex].u_rec[findex].frag_pkt = 0;
	  set_bit(&CmRecord[cindex].u_rec[findex].flag, FRAG_ON_BIT,OFF);
	  CmRecord[cindex].u_rec[findex].frag_data = 0;
	  CmRecord[cindex].u_rec[findex].seq_num = 0;
	  return;	  
	}

      if (CmRecord[cindex].u_rec[findex].seq_num == eh->exthdr_[frag_index].eh_data[4])
	{
	  ch->size() -= ETHER_HDR_LEN;
	  ch->size() -= DOCSIS_HDR_LEN;
      //$A405

	  ch->size() -= 10; /* Overhead of fragmentation header */
	  ch->num_forwards() += 1;
	  CmRecord[cindex].u_rec[findex].frag_data += ch->size();
	  CmRecord[cindex].u_rec[findex].seq_num++; 
	  
	  if (eh->exthdr_[frag_index].eh_data[1] > 0)
	    {
	      CMTSstats.num_piggyreq++;
	      HandleReq(eh->exthdr_[frag_index].eh_data[1],cindex,findex);
	    }
	  
	  if (eh->exthdr_[frag_index].eh_data[3] == 1) /* Last fragment flag */
	    {
#ifdef TRACE_CMTS 
	      printf("flow %d Turning-off fragmentation and passing the pkt..\n",flow_id);
#endif
	      set_bit(&CmRecord[cindex].u_rec[findex].flag, FRAG_ON_BIT,OFF);
	      CmRecord[cindex].u_rec[findex].frag_data = 0;
	      CmRecord[cindex].u_rec[findex].seq_num = 0;

	      //ParseExtHdr(p);

	      RecvFrame( CmRecord[cindex].u_rec[findex].frag_pkt,2 );
	      CmRecord[cindex].u_rec[findex].frag_pkt = 0;

	      /* Check for piggyback request */	      
	    }	  
	}      
      else
	{
#ifdef ERROR_TRACE 
	  printf("MacDocsisCMTS->handle_frag: ERROR: Seq num mismatch 1, Discarding the packet\n");
#endif

	  //	drop(p);

	  set_bit(&CmRecord[cindex].u_rec[findex].flag, FRAG_ON_BIT,OFF);
	  CmRecord[cindex].u_rec[findex].seq_num = 0;
	  CmRecord[cindex].u_rec[findex].frag_pkt = 0;
	  CmRecord[cindex].u_rec[findex].frag_data = 0;
	  return;
	}
    }  
  else if (eh->exthdr_[frag_index].eh_data[2] == 1) /* First Fragment flag */
    {
      
#ifdef TRACE_CMTS //-----------------------------------------------------
      printf("Received first fragment at cmts..\n");
#endif //----------------------------------------------------------------
      
      ch->size() -= ETHER_HDR_LEN;
      ch->size() -= DOCSIS_HDR_LEN;
      ch->size() -= 10; /* Overhead of fragmentation header */
      ch->num_forwards() += 1;
      set_bit(&CmRecord[cindex].u_rec[findex].flag, FRAG_ON_BIT,ON);
      CmRecord[cindex].u_rec[findex].seq_num = eh->exthdr_[frag_index].eh_data[4] + 1;
      CmRecord[cindex].u_rec[findex].frag_data = ch->size();
      CmRecord[cindex].u_rec[findex].frag_pkt = (Packet*)(*data) ;
      
      struct hdr_cmn* fh = HDR_CMN( CmRecord[cindex].u_rec[findex].frag_pkt);

      fh->direction() = hdr_cmn::UP; /* change the direction of the pkt */
      
      if (eh->exthdr_[frag_index].eh_data[1] > 0)
	{
	  CMTSstats.num_piggyreq++;
	  HandleReq(eh->exthdr_[frag_index].eh_data[1],cindex,findex);
	}      
    } 
  else
    {
      /* Might be a probelm on return if a drop the pkt ? */
      //drop(p);
#ifdef ERROR_TRACE 
      printf("MacDocsisCMTS->handle_frag: ERROR: Fragment not correct ??\n");
#endif
    }
  return ;
}

/*****************************************************************************

*****************************************************************************/
void MacDocsisCMTS::HandleConcat(Packet *p, int cindex, int findex)
{
  struct hdr_docsis* dh = HDR_DOCSIS(p);
  Packet* q;
  int * data = (int *)p->accessdata();
  int i = 0;
  docsis_chabstr *MAINch_abs = docsis_chabstr::access(p);
  int inboundChannel =  MAINch_abs->channelNum;


#ifdef TRACE_CMTS //-------------------------------------------------------
  printf("CMTS:HandleConcat(%lf): Inbound channel:%d, hdr mac param: %d\n", 
	 Scheduler::instance().clock(),inboundChannel,dh->dshdr_.mac_param);
#endif //------------------------------------------------------------------
	
  for ( i = 0; i < dh->dshdr_.mac_param; i++)
    {
      q = (Packet* )*data++;
      struct hdr_cmn* ch = HDR_CMN(q);
      
      ch->direction() = hdr_cmn::UP; /* change the direction of the pkt
					else they r pushed back */
      docsis_chabstr *ch_abs = docsis_chabstr::access(q);
      ch_abs->channelNum=inboundChannel;

#ifdef TRACE_CMTS 
      printf("CMTS:HandleConcat(%lf): Passing pkt UP  \n", Scheduler::instance().clock());
#endif
      RecvFrame(q,1);
    }  
  return;
}

/*****************************************************************************
Send packet down to the physical layer...only packets passed by upper layers 
will be sent by this function..No Management messages..
*****************************************************************************/
void MacDocsisCMTS::sendDown(Packet* p) 
{
  int cindex, findex;  
  struct hdr_cmn* ch = HDR_CMN(p);
  struct hdr_docsis* dh = HDR_DOCSIS(p);
  struct hdr_mac* mh = HDR_MAC(p);

  cindex = classify(p, DOWNSTREAM,&findex);

#ifdef TRACE_CMTS //----------------------------------------------------------------
  printf("CMTS:sendDown: Received a packet  (size:%d) from upper layer at %lf, macDA:%d\n",
	 ch->size(), Scheduler::instance().clock(),mh->macDA());
#endif //---------------------------------------------------------------------------
  
  /* this is necessary to clear the event*/
  if(callback_) 
    {
      Handler *h = callback_;
      callback_ = 0;
      h->handle((Event*) 0);
    }
  
  //$A401
  MACstats.total_num_appbytesDS += ch->size();
  MACstats.total_num_appPktsDS++;
  
  /* Make docsis  & ethernet header */
  /* Ethernet header has to be properly updated as CMTS node is passing the
     data to the end-host */
  
  mh->set(MF_DATA,index_); /* To set the MAC src-addr and type */
  ch->size() += ETHER_HDR_LEN;
    
  dh->dshdr_.fc_type = DATA;
  dh->dshdr_.fc_parm = 0;
  dh->dshdr_.ehdr_on = 0;
  dh->dshdr_.mac_param = 0;
  dh->dshdr_.len = ch->size();
  ch->size() += DOCSIS_HDR_LEN;
  

#ifdef TRACE_CMTS //---------------------------------------------------------------------
  printf("CMTS:sendDown:  Received a packet  (size:%d) from upper layer at %lf (cindex:%d, findex:%d)\n",
	 ch->size(),Scheduler::instance().clock(),cindex,findex);
  dump_pkt(p);
  printf(" Packet classified on flow-id = %d\n", CmRecord[cindex].d_rec[findex].flow_id);
#endif //--------------------------------------------------------------------------------


  //If cindex is -1 then we assume it's arp and we will send it... the macDA should be '-1'
  if (cindex == -1) {
#ifdef TRACE_CMTS //---------------------------------------------------------------------
    printf("CMTS:sendDown(%lf):  WARNING:  ASSUMING this is an ARP frame (cindex:%d, findex:%d)\n",
	 Scheduler::instance().clock(),cindex,findex);
#endif

     printf("CMTS:sendDown(%lf) HARD ERROR   pkt of size%d arrived (util total bytes:%f) \n",
	 Scheduler::instance().clock(),ch->size(),CmRecord[cindex].d_rec[findex].util_total_bytes_DS);

    MacSendFrame(p,DATA_PKT);  
    return;  
  }

  ApplyPhs(p,cindex,findex);
    

  
  /* Apply rate-control */
  /* SendFrame will be called from rate-control */
  CmRecord[cindex].d_rec[findex].util_total_bytes_DS += ch->size();
  CmRecord[cindex].d_rec[findex].util_total_pkts_DS ++;
  
  //#ifdef RATE_CONTROL//-------------------------------------------------
#if 0
  if (CmRecord[cindex].d_rec[findex].ratecontrol) /* Rate-control on*/
    {
      RateControl(p,cindex,findex,DATA_PKT);
      return;
    }
#else
  // this is where CMTS sends packets downstream!
  serviceFlowObject *mySFObj = myDSSFMgr->getServiceFlow(p,DATA_PKT);
  if (mySFObj != NULL) {
#ifdef ARRIVAL_TRACE
//$A910 
      simplePacketTrace(p,mySFObj,1);
#endif
      if (mySFObj->isRateControlled()) {
          RateControl(p,mySFObj,cindex,findex,DATA_PKT);
          return;
      }
  }
#endif
  //#endif//--------------------------------------------------------------
  

  MacSendFrame(p,DATA_PKT);  
  return;  
}

//#ifdef RATE_CONTROL//------------------------------------------------------

/*****************************************************************************
 * Routine: void MacDocsisCMTS::RateControl(Packet *p, int cindex, int findex, int type)
 *   This routine is called only if DS rate control is turned on.  It calls this
 *   instead of SendFrame.  This routine determines when/if SendFrame should be called.
*
*     NO LONGER USED!!!!!!!!!!!!!!
 *
 * inputs: 
 *     Packet *p
 *     int cindex
 *     int findex
 *     int type
 *
 * output:
 *
 * Note: 
 *
*****************************************************************************/
void MacDocsisCMTS::RateControl(Packet *p, int cindex, int findex, int type)
{
  struct hdr_cmn *ch = HDR_CMN(p);
  int tokenQLen = CmRecord[cindex].d_rec[findex].tokenq_->length();

printf("CMTS:RateControl:  No longer used.....HARD ERROR:     \n");
exit(1);

#ifdef TRACE_CMTS //-------------------------------------------------------
  if (type == DATA_PKT)
    printf("CMTS:RateControl(%lf) : DATA_PKT: cindex = %d findex = %d pkt-length = %d,  current tokenQLen:%d\n",
      Scheduler::instance().clock(),cindex,findex,ch->size(),tokenQLen) ;
  else
    printf("CMTS:RateControl:(%lf) MGMT_PKT: cindex = %d findex = %d pkt-length = %d, current tokenQLen:%d \n",
     Scheduler::instance().clock(),cindex,findex,ch->size(), tokenQLen);
#endif //--------------------------------------------------------------------
  
  
  if (CmRecord[cindex].d_rec[findex].init_) 
    {
      CmRecord[cindex].d_rec[findex].tokens_ =  (double) CmRecord[cindex].d_rec[findex].bucket_;
      CmRecord[cindex].d_rec[findex].lastupdatetime_ = Scheduler::instance().clock();
      CmRecord[cindex].d_rec[findex].init_ = 0;

#ifdef TRACE_CMTS //---------------------------------------------------------------------
      printf("Initializing tokens to %lf \n",CmRecord[cindex].d_rec[findex].tokens_);
#endif //-------------------------------------------------------------------------------
    }
  
  /* Enque packets if queue already exists   */
  if (CmRecord[cindex].d_rec[findex].tokenq_->length() > 0)
  {
#ifdef TRACE_CMTS //---------------------------------------------------------
    printf("CMTS:RateControl(%lf) :  There are packets queued waiting on tokens..... current length%d,  max allowed:%d (\n",
      Scheduler::instance().clock(), CmRecord[cindex].d_rec[findex].tokenq_->length(), CmRecord[cindex].d_rec[findex].tokenqlen_);
#endif //-------------------------------------------------------------------
    if (CmRecord[cindex].d_rec[findex].tokenq_->length() < CmRecord[cindex].d_rec[findex].tokenqlen_)
	{
	  
#ifdef TRACE_CMTS //---------------------------------------------------------
    printf("CMTS:RateControl(%lf) : enqueue this packet, current length%d,  max allowed:%d (\n",
      Scheduler::instance().clock(), CmRecord[cindex].d_rec[findex].tokenq_->length(), CmRecord[cindex].d_rec[findex].tokenqlen_);
#endif //-------------------------------------------------------------------

	  CmRecord[cindex].d_rec[findex].tokenq_->enque(p);
	}    
    else
	{
	  CmRecord[cindex].d_rec[findex].total_pkts_dropped++;
	  CmRecord[cindex].d_rec[findex].dropped_tokenq++;  
	  //$A401
	  MACstats.total_packets_dropped++;    
	  MACstats.dropped_tokenq++;    
	  drop(p);

#ifdef TRACE_CMTS //-----------------------------------------------
	  printf("Packet dropped at Token bucket queue \n");
#endif //----------------------------------------------------------
	}
      
    if (type == DATA_PKT)
	{
	  if(callback_) 
	    {
	      Handler *h = callback_;
	      callback_ = 0;
	      h->handle((Event*) 0);
	    }
	}      
    return;
  }   
  else {
#ifdef TRACE_CMTS 
    printf("CMTS:RateControl(%lf) :  There are NO packets queued\n", Scheduler::instance().clock());
#endif 
  }

//Otherwise, there's nothing ueueue
  double tok;

  tok = getupdatedtokens(cindex,findex);
    
  int pktsize = ch->size()<<3;

#ifdef TRACE_CMTS //------------------------------------------------------
  printf("CMTS:RateControl(%lf): After getupdatedtokens, Tokens = %lf Pktsize(bytes) = %d, Pktsize(bits):%d \n",
         Scheduler::instance().clock(), CmRecord[cindex].d_rec[findex].tokens_,ch->size(), pktsize);
#endif //------------------------------------------------------------------
  
  if (CmRecord[cindex].d_rec[findex].tokens_ >=pktsize) 
    {

      MacSendFrame(p,type);
      CmRecord[cindex].d_rec[findex].tokens_-=pktsize;
    }
  else 
   //else enqueue
  {
#ifdef TRACE_CMTS //------------------------------------------------------
    printf("CMTS:RateControl(%lf):  Not enought tokens to  send....so queue the packet \n",
         Scheduler::instance().clock());
#endif //------------------------------------------------------------------
//    if (CmRecord[cindex].d_rec[findex].tokenqlen_!=0) 
    if (CmRecord[cindex].d_rec[findex].tokenq_->length() < CmRecord[cindex].d_rec[findex].tokenqlen_) 
	{	  

	  CmRecord[cindex].d_rec[findex].tokenq_->enque(p);
#ifdef TRACE_CMTS //----------------------------------------------------
      printf("CMTS:RateControl(%lf) : enqueued this packet (size:%d), current tokenq_ length%d,  max allowed:%d (\n",
      Scheduler::instance().clock(), ch->size(), CmRecord[cindex].d_rec[findex].tokenq_->length(), CmRecord[cindex].d_rec[findex].tokenqlen_);
#endif //---------------------------------------------------------------

	  double etime = (pktsize - CmRecord[cindex].d_rec[findex].tokens_)/CmRecord[cindex].d_rec[findex].rate_;
	  
	  if (etime < 0.0)
	    {
	      fprintf(stderr, "CMTS:RateControl: etime is negative,exiting \n");
	      exit(1);
	    }
	  
#ifdef TRACE_CMTS //----------------------------------------------------
      printf("CMTS:RateControl(%lf) : mhToken_ start with etime %lf\n",
      Scheduler::instance().clock(), etime);
#endif //---------------------------------------------------------------

	  mhToken_.start((Packet*)&CmRecord[cindex].d_rec[findex].intr, etime);
	  
	  double current_time = Scheduler::instance().clock();
	  insert_tokenlist(current_time+etime, cindex,findex);
	}
      else 
	{
	  CmRecord[cindex].d_rec[findex].total_pkts_dropped++;
	  CmRecord[cindex].d_rec[findex].dropped_tokenq++;	  
	  //$A401
	  MACstats.total_packets_dropped++;    
	  MACstats.dropped_tokenq++;    
	  drop(p);

#ifdef TRACE_CMTS //---------------------------------------------------------------------
      printf("CMTS:RateControl(%lf) : Packet dropped at Token bucket queue since Max queue len is Zero \n",
      Scheduler::instance().clock());
#endif //--------------------------------------------------------------------------------
	}
      /* Unlock the IFQ if DATA_PKT */      
      if (type == DATA_PKT)
	{
	  if(callback_) 
	    {
	      Handler *h = callback_;
	      callback_ = 0;
	      h->handle((Event*) 0);
	    }
	}
    }  
}

/*****************************************************************************
 * Routine: *void MacDocsisCMTS::RateControl(Packet *p, serviceFlowObject *mySF, int cindex, int findex, int type)
 *   This routine is called only if DS rate control is turned on.  It calls this
 *   instead of SendFrame.  This routine determines when/if SendFrame should be called.
 *
 * inputs: 
 *     Packet *p
 *     serviceFlowObject *mySF
 *     int cindex
 *     int findex
 *     int type : indicates if frame is DATA or MGMT
 *
 * output:
 *
 * Note: 
 *
*****************************************************************************/
void MacDocsisCMTS::RateControl(Packet *p, serviceFlowObject *mySF, int cindex, int findex, int type)
{
  struct hdr_cmn *ch = HDR_CMN(p);
  double now = Scheduler::instance().clock();
  int tokenQLen = mySF->getTokenQLen();
  int tokenQMaxSize = mySF->getTokenQMaxSize();

#ifdef TRACE_CMTS //-------------------------------------------------------
  if (type == DATA_PKT)
    printf("CMTS:SFRateControl(%lf)(flowID:%d) : DATA_PKT: current tokenQLen:%d\n", now,mySF->flowID,tokenQLen);
  else
    printf("CMTS:SFRateControl(%lf) (flowID:%d): MGT_PKT: current tokenQLen:%d\n", now,mySF->flowID,tokenQLen);
#endif //------------------------------------------------------------------


  if (mySF->initTokenBucketIfNecessary()) 
  {
#ifdef TRACE_CMTS //-------------------------------------------------------
    printf("CMTS:SFRateControl(%lf)(flowID:%d) : Token bucket initialized TokenQMaxSize:%d (rate_:%f, bucket_:%d) \n", 
          now, mySF->flowID,tokenQMaxSize,mySF->rate_,mySF->bucket_);
#endif //------------------------------------------------------------------
  }

  /* Enque packets if queue already exists   */
  if (tokenQLen > 0)
  {

#ifdef TRACE_CMTS //-------------------------------------------------------
    printf("CMTS:SFRateControl(%lf)(flowID:%d) :  There are packets queued waiting on tokens..... current length%d,  max allowed:%d (\n", 
            now,mySF->flowID,tokenQLen,tokenQMaxSize);
#endif //------------------------------------------------------------------
    if (tokenQLen < tokenQMaxSize)
	{
	  mySF->enqueTokenQ(p);
	}    
    else
	{
	  CmRecord[cindex].d_rec[findex].total_pkts_dropped++;
	  CmRecord[cindex].d_rec[findex].dropped_tokenq++;  
	  //$A401
	  MACstats.total_packets_dropped++;    
	  MACstats.dropped_tokenq++;    
      // per flow statistics
      mySF->failEnqueTokenQ(p);
#ifdef TRACE_CMTS //-------------------------------------------------------
      printf("CMTS:SFRateControl(%lf)(flowID:%d) : Token bucket DROP (total:%d)\n", 
            now,mySF->flowID,CmRecord[cindex].d_rec[findex].total_pkts_dropped);
#endif //------------------------------------------------------------------
	  drop(p);
	}
      
    if (type == DATA_PKT)
	{
	  if(callback_) 
	    {
          fprintf(stderr, "handle callback\n");
	      Handler *h = callback_;
	      callback_ = 0;
	      h->handle((Event*) 0);
	    }
	}      
    return;
  }   
  else {
#ifdef TRACE_CMTS 
    printf("CMTS:SFRateControl(%lf)(flowID:%d) :  There are NO packets queued\n", now,mySF->flowID);
#endif 
  }

  double tok;

  tok = mySF->updateTokenBucket();
    
  int pktsize = ch->size()<<3;  // into bits

#ifdef TRACE_CMTS 
  printf("CMTS:SFRateControl(%lf): (flowID:%d) After getupdatedtokens, Tokens = %lf Pktsize(bits) = %d  (tok:%f)\n",
         now,mySF->flowID, mySF->tokens_,pktsize,tok);
#endif 
  
  if (tok >=pktsize) 
    {
      //fprintf(stderr, "Enough token (%lf) to send current packet (p = %p, size = %d bits)\n", tok, p, pktsize);
      MacSendFrame(p,type);
      mySF->consumeTokens(pktsize);
      //fprintf(stderr, "Now tokens are reduced to %lf.\n", mySF->getTokens());
    }
  else 
   //else enqueue
  {
    //fprintf(stderr, "Not enough token (%lf) to send current packet (p = %p, size = %d bits) -- queue it now\n", tok, p, pktsize);
#ifdef TRACE_CMTS 
    printf("CMTS:SFRateControl(%lf)(flowID:%d):  Not enought tokens to  send....so queue the packet \n",
         now,mySF->flowID);
#endif //------------------------------------------------------------------
//    if (CmRecord[cindex].d_rec[findex].tokenqlen_!=0) 
//    assert(tokenQLen == 0);
//    assert(tokenQMaxSize > 0);

	mySF->enqueTokenQ(p);

	double etime = (pktsize - tok)/(mySF->getTokensRate());
//    assert(etime > 0.0);
	  
#ifdef TRACE_CMTS //-------------------------------------------------------
      printf("CMTS:SFRateControl(%lf)(flowID):%d) : enqueued this packet (size:%d),  nextTimerEvent:%f (%f)(\n",
             now, mySF->flowID,ch->size(), etime,now+etime);
#endif //------------------------------------------------------------------

      //fprintf(stderr, "Schedule token timer at %lf to handle the queued packet later!\n", now + etime);
      mySF->startTokenQTimer(p, etime);
	  //mhSFToken_.start(p, etime);

    /* Unlock the IFQ if DATA_PKT */      
      if (type == DATA_PKT)
	  {
	    if(callback_) 
	    {
          fprintf(stderr, "handle callback\n");
	      Handler *h = callback_;
	      callback_ = 0;
	      h->handle((Event*) 0);
	    }
	  }
    }  
}

/*****************************************************************************

*****************************************************************************/
double MacDocsisCMTS::getupdatedtokens(int cindex,int findex)
{
  double now = Scheduler::instance().clock();
  
  CmRecord[cindex].d_rec[findex].tokens_ += 
    (now - CmRecord[cindex].d_rec[findex].lastupdatetime_) * CmRecord[cindex].d_rec[findex].rate_;
  
  if (CmRecord[cindex].d_rec[findex].tokens_ > CmRecord[cindex].d_rec[findex].bucket_)
    CmRecord[cindex].d_rec[findex].tokens_ = CmRecord[cindex].d_rec[findex].bucket_;
  
  CmRecord[cindex].d_rec[findex].lastupdatetime_ = Scheduler::instance().clock();
  
#ifdef TRACE_CMTS //---------------------------------------------------------
      printf("CMTS:getupdatedtokens(%lf) : updating tokens to %f \n",Scheduler::instance().clock(),CmRecord[cindex].d_rec[findex].tokens_);
#endif //--------------------------------------------------------------------
  
  return CmRecord[cindex].d_rec[findex].tokens_;
}

/*****************************************************************************

*****************************************************************************/
void MacDocsisCMTS::CmtsTokenHandler(Event * e)
{
  tkptr temp;
  int cindex,findex;
  temp = TokenList;

printf("CMTS:CmtsTokenHandler:  No longer used.....HARD ERROR:     \n");
exit(1);
  
  if (!temp)
    {
      printf("CMTS :Error1 in CmtsTokenHandler: Exiting\n");
      exit(1);
    }
  else
    {
      TokenList = TokenList->next;
    }
  
  cindex = temp->cindex;
  findex = temp->findex;
  free(temp);
  
  Packet *p;
  
#ifdef TRACE_CMTS //---------------------------------------------------------
      printf("CMTS:TokenHandler(%lf) : Next flow on cindex: %d,  findex: %d   \n",Scheduler::instance().clock(),cindex,findex);
#endif //--------------------------------------------------------------------
  
  if (CmRecord[cindex].d_rec[findex].tokenq_->length() == 0)
    {
      printf("CMTS :Error2 in CmtsTokenHandler: Exiting\n");
      exit(1);
    }
  
  p = CmRecord[cindex].d_rec[findex].tokenq_->deque();
  
  struct hdr_cmn *ch = HDR_CMN(p);
  double tok;
  
  tok = getupdatedtokens(cindex,findex);
    
  int pktsize = ch->size()<<3;
  
#ifdef TRACE_CMTS //---------------------------------------------------------
  printf("CMTS:TokenHandler(%lf) : dequeue pkt of size:%d from tokenq,  updated length:%d , tokens:%f \n",Scheduler::instance().clock(),
   ch->size(), CmRecord[cindex].d_rec[findex].tokenq_->length(),tok);
#endif //--------------------------------------------------------------------

  if (ClassifyDataMgmt(p)) 
    MacSendFrame(p,MGMT_PKT);
  else
    MacSendFrame(p,DATA_PKT);

  CmRecord[cindex].d_rec[findex].tokens_-=pktsize;
  
  if (CmRecord[cindex].d_rec[findex].tokenq_->length() > 0)
    {
      p = CmRecord[cindex].d_rec[findex].tokenq_->head();
      
      struct hdr_cmn *ch = HDR_CMN(p);
      int pktsize = ch->size()<<3;
      double etime = (pktsize - CmRecord[cindex].d_rec[findex].tokens_)/CmRecord[cindex].d_rec[findex].rate_;

#ifdef TRACE_CMTS //--------------------------------------------------------------------
      printf("CMTS:CmtsTokenHandler: Starting token timer pktsize = %d etime = %lf, to complete at %f \n",ch->size(),etime,
              Scheduler::instance().clock() - etime);
#endif //-------------------------------------------------------------------------------
      
      if (etime < 0.0)
	{
	  printf("CMTS:CmtsTokenHandler: etime is negative,exiting \n");
	  exit(1);
	}    
      mhToken_.start((Packet*)&CmRecord[cindex].d_rec[findex].intr, etime);
      
      double current_time = Scheduler::instance().clock();
      insert_tokenlist(current_time+etime, cindex,findex);
    }  
}

/*****************************************************************************
 * Routine: void MacDocsisCMTS::CmtsSFTokenHandler(Event * e)
 *   This routine handles a token buffer timer tick.  
 *
 * inputs: 
 *      Event *e : a packet
 *
 * output:
 *
 * Note: 
 *
*****************************************************************************/
void MacDocsisCMTS::CmtsSFTokenHandler(Event * e)
{
    Packet *p = (Packet *)e;
    int type = ClassifyDataMgmt(p) ? MGMT_PKT : DATA_PKT;
    serviceFlowObject *mySF = myDSSFMgr->getServiceFlow(p,type);
    int tokenQLen = mySF->getTokenQLen();
    double now = Scheduler::instance().clock();

#ifdef TRACE_CMTS //---------------------------------------------------------
    printf("CMTS:CmtsSFTokenHandler(%lf):(flowID:%d)  queue level is %d  \n",now,mySF->flowID,tokenQLen);
#endif //--------------------------------------------------------------------

//    assert(tokenQLen > 0);
    if (tokenQLen == 0)
      {
        printf("CMTS:CmtsSFTokenHandler: Error2 in CmtsTokenHandler: Exiting\n");
        exit(1);
      }

    if (mySF->dequeTokenQ() != p) {
      printf("CMTS:CmtsSFTokenHandler: Error3 in CmtsSFTokenHandler: Exiting\n");
      exit(1);
    }

    struct hdr_cmn *ch = HDR_CMN(p);
    double tok;

    tok = mySF->updateTokenBucket();

    int pktsize = ch->size()<<3;

    //fprintf(stderr, "tokens updated to %lf, pktsize %d\n", tok, pktsize);
#ifdef TRACE_CMTS //---------------------------------------------------------
    printf("CMTS:CmtsSFTokenHandler(%lf):(flowID:%d) : dequeue pkt of size:%d from tokenq,  updated length:%d ,tokens available:%f (rate_:%f, bucket_:%d \n",now,mySF->flowID,
     ch->size(), mySF->getTokenQLen(),tok,mySF->rate_,mySF->bucket_);
#endif //--------------------------------------------------------------------

    MacSendFrame(p,type);
    mySF->consumeTokens(pktsize);

    if ((tokenQLen = mySF->getTokenQLen()) > 0)
    {
        p = mySF->peekTokenQ();

        struct hdr_cmn *ch = HDR_CMN(p);
        int pktsize = ch->size()<<3;

        //fprintf(stderr, "Found next packet %p size %d on token queue\n", p, pktsize);

        double etime = (pktsize - mySF->getTokens())/(mySF->getTokensRate());

#ifdef TRACE_CMTS //---------------------------------------------------------
        printf("CMTS:CmtsSFTokenHandler:(%f)(flowID:%d): Starting token timer pktsize = %d etime = %lf, to complete at %f \n",now,mySF->flowID,pktsize,etime, now - etime);
#endif //--------------------------------------------------------------------

        if (etime < 0.0)
        {
          printf("CMTS:CmtsTokenHandler: etime is negative,exiting \n");
          exit(1);
        }
        
        //fprintf(stderr, "Schedule token timer at %lf for next packet on queue\n", now+etime);
        mySF->startTokenQTimer(p, etime);
        //mhSFToken_.start(p, etime);
    }
    else {
        //fprintf(stderr, "Token q is now empty\n");
    }
}

/*****************************************************************************

*****************************************************************************/
void MacDocsisCMTS::insert_tokenlist(double time, int cindex, int findex)
{
  
  tkptr temp, prev,node;

  temp = TokenList;
  prev = temp;
  
  node = (tkptr) malloc(sizeof (struct token_timer_list));
  
  if (node == 0)
    {
      printf("CMTS:insert_tokenlist: Malloc failed, quiting\n");
      exit(1);
    }
  node->expiration_time = time;
  node->cindex = cindex;
  node->findex = findex;
  
  if (!temp)
    {
      node->next = 0;
      TokenList = node;
      return;
    }
  else
    {
      while (temp)
	{
	  if (temp->expiration_time > time)
	    {
	      break;
	    }
	  else
	    {
	      prev = temp;
	      temp = temp->next;
	    }
	}
      
      if (prev != temp)
	{
	  prev->next = node;
	  node->next = temp;
	  return;
	}
      else
	{
	  node->next = temp;
	  TokenList = node;
	  return;
	}
    }  
  return;
}

//#endif //---------------------------- Endif RATE_CONTROL -----------------------

  //$A306
/****************************************************************************
 * Method:  void MacDocsisCMTS::MacSendFrame(Packet * p, int type)
 *
 * Function:   All frames that are to be sent over the DS channel will be 
 *             sent down through this method.
 *
 * Parameters:
 *    Packet *p:  A packet that already has been adjusted for all headers
 *    int type:  Designates the frame to be either a DATA_PKT or a MGMT_PKT
 *
 * Design Notes:
 *
 *      get the service flow object
 *      get the scheduler assignment from myDSScheduler
 * (temporarily)     set the channel number in the packet
 *      update MAC stats
 *      if the scheduler assignment is to DEFER
 *         queue the packet in the SF Object queue
 *      else
 *         call myPhy SendFrame
 *
 *
 * *************************************************************************/
void MacDocsisCMTS::MacSendFrame(Packet * p, int type)
{
  docsis_dsehdr *chx = docsis_dsehdr::access(p);    
  hdr_cmn   *ch  = HDR_CMN(p);
  hdr_docsis   *chd = HDR_DOCSIS(p);
  double stime;
  serviceFlowObject *mySFObj = NULL;
  int idleChannel;
  struct schedulerAssignmentType *schedulerAssignment=NULL;
  int rc = 0;

#ifdef TRACE_DEBUG
  tracePoint("CMTS:MacSendFrame:",0,ch->size());
#endif

  //$A401
  MACstats.total_num_sent_frames++;
  MACstats.total_num_sent_bytes += ch->size();
  MACstats.total_num_BW_bytesDOWN += ch->size();

  mySFObj = myDSSFMgr->getServiceFlow(p,type);
#ifdef TRACE_CMTS //----------------------------------------------------------------
// WATCH 1
  printf("CMTS:MacSendFrame(%lf): WATCH 1 flow id:%d send a packet of size:%d,  type param:%d , and ptype:%d  (SF list size:%d\n",
	 Scheduler::instance().clock(),mySFObj->flowID, ch->size(),type,ch->ptype(),mySFObj->myPacketList->curListSize);
#endif //---------------------------------------------------------------------------

  if (mySFObj == NULL) 
  {
    //handle this error
//#ifdef TRACE_CMTS //-------------------------------------------------------------------------
    printf("CMTS:macSendFrame(%lf): HARD ERROR: Could not find this service flow:  packet of size:%d,  type param:%d , and ptype:%d\n",
	 Scheduler::instance().clock(),ch->size(),type,ch->ptype());
//#endif //------------------------------------------------------------------------------------
//	  return;
    exit(1);
  }
#ifdef TRACE_CMTS //-------------------------------------------------------------------------
      printf("CMTS:MacSendFrame(%lf): found mySFObj (flowid:%d), packet List size:%d head:%d,  tail:%d \n ",
	     Scheduler::instance().clock(),mySFObj->flowID, mySFObj->myPacketList->curListSize,mySFObj->myPacketList->head, mySFObj->myPacketList->tail);
  printf("CMTS:MacSendFrame:printSFInfo:  ");
  mySFObj->printSFInfo();
#endif //------------------------------------------------------------------------------------

#ifdef ARRIVAL_TRACE
//$A910 
  simplePacketTrace(p,mySFObj,2);
#endif

   schedulerAssignment= myDSScheduler->makeSchedDecision(p,mySFObj);


#ifdef TRACE_CMTS //----------------------------------------------------------------
// WATCH 2
  printf("CMTS:MacSendFrame(%lf): WATCH 2 flow id:%d send a packet of size:%d,  type param:%d , and ptype:%d  (SF list size:%d),  numberAssingments:%d, first channel:%d\n",
	 Scheduler::instance().clock(),mySFObj->flowID, ch->size(),type,ch->ptype(),mySFObj->myPacketList->curListSize,schedulerAssignment->numberAssignments,schedulerAssignment->channelAssignments[0]);
#endif //---------------------------------------------------------------------------

#ifdef TRACE_CMTS //-------------------------------------------------------------------------
      printf("CMTS:MacSendFrame(%lf): scheduler returns assignment number of assignments:%d (first channel:%d) \n", 
	     Scheduler::instance().clock(),schedulerAssignment->numberAssignments,schedulerAssignment->channelAssignments[0]);
#endif //------------------------------------------------------------------------------------
  if (schedulerAssignment->numberAssignments > 0) 
  { 
    //update stats
       	    
    //pass down to the macPhyInterface
//$A910 
    simplePacketTrace(schedulerAssignment->myPkt,mySFObj,3);
    rc = myPhyInterface->SendFrame(schedulerAssignment, mySFObj, this);


#ifdef TRACE_CMTS //-------------------------------------------------------------------------
      printf("CMTS:MacSendFrame(%lf): macPhyInterface returns rc:%d \n", 
	     Scheduler::instance().clock(),rc);
#endif //------------------------------------------------------------------------------------

  }
  else // channel is busy
  {
      /* Queue the packet */

#ifdef TRACE_CMTS 
      printf("CMTS:MacSendFrame(%lf):CMTS channel busy.....need to queue the packet (current SF queue:)\n", 
	     Scheduler::instance().clock());
#endif 
    rc = mySFObj->addPacket(p);
    
#ifdef TRACE_CMTS 
      printf("CMTS:MacSendFrame(%lf): return rc: %d  size of SF Objects packet List: %d\n", 
	     Scheduler::instance().clock(),rc,mySFObj->myPacketList->getListSize());
#endif 

#ifdef TRACE_CMTS //----------------------------------------------------------------
// WATCH 3
  printf("CMTS:MacSendFrame(%lf): WATCH 3 flow id:%d send a packet of size:%d,  type param:%d , and ptype:%d  (SF list size:%d), rc from addPacket:%d\n",
	 Scheduler::instance().clock(),mySFObj->flowID, ch->size(),type,ch->ptype(),mySFObj->myPacketList->curListSize,rc);
#endif //---------------------------------------------------------------------------

     if (rc == FAILURE)
       Packet::free(p);
  }
 free(schedulerAssignment);
  return;
}




//$A306
/****************************************************************************
 * Method:  void MacDocsisCMTS::MacSendFrameOnCompletion(int channelNumber)
 *
 * Function:   All frames that are to be sent over the DS channel will be 
 *             sent down through this method.
 *
 * Parameters:
 *
 * Design Notes:
 *
 * *************************************************************************/
void MacDocsisCMTS::MacSendFrameOnCompletion(int channelNumber)
{
  double stime;
  serviceFlowObject *mySFObj = NULL;
  int idleChannel;
  struct schedulerAssignmentType *schedulerAssignment=NULL;
  int rc = 0;
  Packet *p = NULL;

#ifdef TRACE_DEBUG
  tracePoint("CMTS:MacSendFrameOnCompletion:",channelNumber,0);
#endif

  schedulerAssignment= myDSScheduler->makeSchedDecision(channelNumber);

  if (schedulerAssignment->numberAssignments <= 0) 
  {
    //This is ok....just means there are no packets queued
#ifdef TRACE_CMTS //-------------------------------------------------------------------------
    printf("CMTS:MacSendFrameOnCompletion(%lf): Channel:%d, No packets waiting for transmission \n", 
	     Scheduler::instance().clock(),channelNumber);
#endif //------------------------------------------------------------------------------------
  }
  else
  {
		p = schedulerAssignment->myPkt;
		docsis_dsehdr *chx = docsis_dsehdr::access(p);    
		hdr_cmn   *ch  = HDR_CMN(p);
		hdr_docsis   *chd = HDR_DOCSIS(p);
		
//$A801
		mySFObj = myDSSFMgr->getServiceFlow(p);
#ifdef TRACE_CMTS //-------------------------------------------------------------------------
      printf("CMTS:MacSendFrameOnCompletion(%lf): found mySFObj (flowid:%d), packet List size:%d head:%d,  tail:%d \n ",
	     Scheduler::instance().clock(),mySFObj->flowID, mySFObj->myPacketList->curListSize,mySFObj->myPacketList->head, mySFObj->myPacketList->tail);
		mySFObj->printSFInfo();
#endif //------------------------------------------------------------------------------------

#ifdef TRACE_CMTS //-------------------------------------------------------------------------
      printf("CMTS:MacSendFrameOnCompletion(%lf): channel:%d,  scheduler returns assignment number of assignments:%d \n", 
	     Scheduler::instance().clock(),channelNumber,schedulerAssignment->numberAssignments);
#endif //------------------------------------------------------------------------------------
       	    
    //pass down to the macPhyInterface
//$A910 
    simplePacketTrace(schedulerAssignment->myPkt,mySFObj,3);
    rc = myPhyInterface->SendFrame(schedulerAssignment, mySFObj, this);

#ifdef TRACE_CMTS //-------------------------------------------------------------------------
      printf("CMTS:MacSendFrameOnCompletion(%lf): macPhyInterface returns rc:%d \n", 
	     Scheduler::instance().clock(),rc);
#endif //------------------------------------------------------------------------------------
  }
  free(schedulerAssignment);
  return;
}



/************************************************************************
 *
 * void MacDocsisCMTS::CmtsSendHandler(int channelNumber, Packet *p)
 *
 * input:   
 *   int channelNumber
 *  Packet *p
 *
 * outputs:  none
 *
 * function:  
 *   This is called when a DS transmission is complete
 *   
 *   Overview:
 *
 ************************************************************************/
void MacDocsisCMTS::CmtsSendHandler(int channelNumber, Packet *p)
{
  
//$A301
  Packet::free((Packet *)p);
 
  MacSendFrameOnCompletion(channelNumber);
  return;
}

/************************************************************************
 *
 * void MacDocsisCMTS::CmtsMapHandler(Event * e)
 *
 * input:   Event *e:  timer event 
 *
 * outputs:  none
 *
 * function:  
 *   This is called when a MAP is to be sent.
 *   
 *   Overview:
 *   -Figure out the start/stop times for this MAP
 *   -Call calculate_slots to make sure the MAP does not go > 4096 slots in the 
 *        future (this would violate DOCSIS).
 *   -Call alloc_bw() to assign slots
 *   -Figure out when to reset the cmts docsis timer for the next MAP 
 *
 * ***********************************************************************/
void MacDocsisCMTS::CmtsMapHandler(Event * e)
{	
  double t,s;
  double ntime ;
  u_int32_t c_slots;
  
  omap_stime = map_stime;
  omap_etime = map_etime;  
  map_stime = map_etime ;

  /* Adjust the Map starting time to account for over-allocation in 
     previous Map */

  //if (num_adjust_slots)
  //{
  //	map_stime += (num_adjust_slots) * (size_mslots);
  //	num_adjust_slots = 0;

  /* Update the actual end time of old-map..
     just in case Map was  ot sent this time */

  //	omap_etime = map_stime; 
  //}
	
  map_etime = map_stime + Conf_Table_.mapparam.time_covered;

#ifdef TRACE_CMTS //------------------------------------------------------
  printf("\nCmtsMapHandler: First  Map_etime = %lf (size_mslots=%f)\n",
	 map_etime,size_mslots);
#endif //-----------------------------------------------------------------
  
  //t = ceil((((map_etime)*(10*10*10*10*10*10)) / size_mslots));

  s = size_mslots ;
  t = map_etime / s;
  t += 0.5;
  t = (int) t;
  map_etime = (t*s);

#ifdef TRACE_CMTS //---------------------------------------------------------------------
  printf("CmtsMapHandler: Sending Map at %lf with map_stime = %lf & map_etime = %lf\n",
	 Scheduler::instance().clock(),map_stime,map_etime);
#endif //--------------------------------------------------------------------------------
  
  /* Calculate number of slots to be sent in the future */
  /* The time covered by a MAP changes if the current map_etime is
     more than 4096 slots away from current time */
  
  //printf("Calling calculate_slots..1\n");

  /* By using the start time now we simply check 
     that the map does not cover greater
     than 4096 slots in the future. */
  
  c_slots = calculate_slots(Scheduler::instance().clock(),map_etime);
  
  if (c_slots > 4096)
    //if (c_slots > 124)
    {
      u_int32_t n;

      printf("mac-docsiscmts:CmtsMapHandler: ERROR: 2  Calling calculate_slots\n");
      n = calculate_slots(Scheduler::instance().clock(),map_stime);
      
      if ((n > 4096))
	{
	  printf("mac-docsiscmts:CmtsMapHandler: ERROR: 3  Calling calculate_slots\n");
	  map_stime = omap_stime;
	  map_etime = omap_etime;
	  mhMap_.start((Packet*) (&intr), Conf_Table_.mapparam.map_interval);
	  return;
	}   
      //map_etime = map_stime + (4096 - n)*size_mslots/(10*10*10*10*10*10);
      map_etime = map_stime + (4096 - n)*size_mslots;
      t = (map_etime)/ size_mslots;
      t += 0.5;
      t = (int) t;
      map_etime = (t*size_mslots);
    }
  
  alloc_bw();
  dump_stats();   /* needed to compute a statistic average */

#ifdef TRACE_CMTS //---------------------------------------------------------------
  printf("CmtsMapHandler: Map_stime = %lf Map_etime = %lf\n",
	 map_stime,map_etime);
#endif //--------------------------------------------------------------------------
  
  ntime = next_map*size_mslots;
  
#ifdef TRACE_CMTS //------------------------------------------------------------
  printf("CmtsMapHandler: Next map to be sent after %lf\n",ntime);
#endif //-----------------------------------------------------------------------
  
  mhMap_.start((Packet*) (&intr), ntime);
  next_map = max_slots_pmap;
}

/*****************************************************************************

*****************************************************************************/
void MacDocsisCMTS::dump_stats()
{
  double curr_time = Scheduler::instance().clock();
  
  if (curr_time - last_dmptime >= 1.0)
    {
      last_dmptime = curr_time;

      //printf("CMTS statistics\n");
      //printf("Avg Num of pkts = %d Avg Num of bytes = %d (per sec)\n",avg_pkts,avg_bytes);
      //printf("Avg Num of mgmt pkts = %d Avg Num of mgmt bytes = %d (per sec)\n",CMTSstats.avg_mgmtpkts,CMTSstats.avg_mgmtbytes);
      
      //printf("Avg Size of RT-Queue =%d Avg Size of BF queue = %d\n",avg_szrtqueue,avg_szbfqueue);
      //printf("Num of RT req ps =%d Num of BF req ps = %d (per sec)\n",avg_rtreq,avg_bfreq);
      
      
      //printf("Avg num data-grants pm = %d\n",avg_dgrant);
      //printf("Avg num Unicast-req pm = %d\n",avg_req);
      //printf("Avg num contention pm = %d\n",avg_contention);
      //printf("Avg num grant pending pm = %d\n",avg_gpend);
    }
}

/*****************************************************************************

*****************************************************************************/
void MacDocsisCMTS::ReleaseJobs()
{
  /* Function called during the starting of simulation to release all
     the UGS jobs & VBR unicast req jobs */
  
  int i, j; 
  u_int32_t size;
  jptr node;
  int count  = 0;
  
  for (i = 0; i < SizeCmTable; i++) /* For each CM */
    {
      for (j = 0; j < CmRecord[i].SizeUpFlTable; j++) /* For each flow */
	{
	  if (CmRecord[i].u_rec[j].sched_type == UGS)
	    {	    
	      int tmp_size,tmp_quo;  /* To take the ceiling */
	      
          int channelNumber=defaultUSChannel;
	      tmp_size = CmRecord[i].u_rec[j].gsize + DOCSIS_HDR_LEN + ETHER_HDR_LEN;
          struct channelPropertiesType  channelProperties;
          myPhyInterface->getChannelProperties(&channelProperties,channelNumber);

          int x = tmp_size * channelProperties.bytes_pminislot;
          int overhead = myPhyInterface->getOverHead(x,channelNumber);
		  tmp_size+=overhead;

//	      tmp_size = CmRecord[i].u_rec[j].gsize + upchannel.overhead_bytes + DOCSIS_HDR_LEN + ETHER_HDR_LEN;
	      tmp_quo = (tmp_size % channelProperties.bytes_pminislot);
	      size = (tmp_size/channelProperties.bytes_pminislot);
	      
	      if (tmp_quo != 0)
		    size += 1;
	      
	      node = (struct job *) malloc(sizeof (struct job));

	      /*fill_job(node, UGS, 0, CmRecord[i].u_rec[j].ginterval, size, 
		Conf_Table_.mapparam.map_interval, CmRecord[i].u_rec[j].flow_id );*/
	      
	      //The release param in fill_job
              count++;
			  //CHANGE THIS (HARDCODED ??)
              double t = 0.004 + (count * .002);
//	      double t = 0.004;
	      
#ifdef TRACE_CMTS 
              printf("ReleaseJobs: CMTS starting a UGS service: flow id:%d, release time:%f, grant size:%f, (size:%d)  \n",
                               CmRecord[i].u_rec[j].flow_id,t,CmRecord[i].u_rec[j].ginterval,size);

//void MacDocsisCMTS::fill_job(jptr node, char sclass, char type, 
//			     double period,u_int32_t size,
//			     double release , u_int16_t flow_id)
#endif 
	      fill_job(node, UGS, 0, CmRecord[i].u_rec[j].ginterval,size, t, CmRecord[i].u_rec[j].flow_id );
	      InsertJob(node,0); /* insert job in the UGS list
				    so that list is sorted 
				    according to deadline */
	    }	
	  else if (CmRecord[i].u_rec[j].sched_type == RT_POLL )
	    {
	      size = size_ureqgrant;
	      node = (struct job *) malloc(sizeof (struct job));
	      
	      fill_job(node, RT_POLL, 1, CmRecord[i].u_rec[j].ginterval, size, 
		       Conf_Table_.mapparam.map_interval, CmRecord[i].u_rec[j].flow_id );
	      
	      InsertJob(node,1); /* insert job in the RT-POLL list*/
	    }	
	}
    }
  return;	
}

/**************************************************************************
 *
 * Routine:  void MacDocsisCMTS::print_job_list(int i)
 *
 * input:   int i:  Indicates which job list to print
 *
 * outputs:  none
 *
 * function:  
 *   Prints to stdout the contents of the desired job queue:
 *           job_list[0] which is UGS periodic
 *           job_list[1] which is rt-VBR periodic
 *           job_list[2] which is rt-VBR non periodic
 *           job_list[3] which is best effort
 *
 ************************************************************************/
void MacDocsisCMTS::print_job_list(int i)
{
  jptr jp;
  int numberJobs = 0;
  
  printf("Printing job list for queue %d\n",i);
  jp = job_list[i];
  
  if (!jp)
    {
      printf("Job list is empty \n");
      return;
    }
  else
    {
      while (jp)
	{
	  printf("Release time = %lf\n",jp->release_time);
	  printf("Period = %lf\n",jp->period);
	  printf("Deadline = %lf\n",jp->deadline);
	  printf("flow-id = %d\n",jp->flow_id);
	  printf("type = %d\n",jp->type);
	  printf("min-slots = %d\n",jp->mini_slots);
	  jp = jp->next;
          numberJobs++;
	}
    }
    printf("print_jobs: For job type:%d, found %d jobs\n",i,numberJobs);
  return;
}
/*************************************************************************
 *
 * routine: void MacDocsisCMTS::fill_job(jptr node, char sclass, 
 *              char type, double period,u_int32_t size,
 *              double release , u_int16_t flow_id)
 *
 * input:   
 * jptr node : job queue element (i.e., a job)
 * char sclass : 
 * char type
 * double period
 * u_int32_t size,
 * double release
 * u_int16_t flow_id
 *
 * outputs:  none
 *
 * function:  
 *
 * ***********************************************************************/
void MacDocsisCMTS::fill_job(jptr node, char sclass, char type, 
			     double period,u_int32_t size,
			     double release , u_int16_t flow_id)
{
  node->release_time	= release;
  node->period		= period;

  //node->deadline 	= node->release_time + node->period;

  node->deadline 	= node->release_time +0.05 ;
  node->sclass 		= (UpSchedType)sclass;
  node->flow_id		= flow_id;
  node->type		= type; /* 0 - data, 1 - unicast req, 2- contention req */
  node->mini_slots		= size;
  node->next		= 0;
  node->flag 		= 0;
  node->retry_count 	= 0;
  
  node->ugsjitter 	= 0.0;
  node->jitterSamples 	= 0;
  node->last_jittercaltime = 0.0;
#ifdef TRACE_CMTS 
  printf("fill_job: create a job entry: flow id:%d, release time:%f, period:%f,  grant size:%d  \n",
                               flow_id,release,period,size);
#endif 
}

void MacDocsisCMTS::InsertJob(struct job* node, int index)
{
  jptr jp,jn;
  jp = job_list[index];
  jn = jp;
  
  if (!jp)
    {
      job_list[index] = node;
      return;
    }
  else
    {      
      while (jp && jp->deadline < node->deadline) 
	{
	  jn = jp;	
	  jp = jp->next;	  
	}
      
      if (!jp) /* End of the list */
	jn->next = node;
      else
	{
	  if (jp == jn) /* Head of the list insertion */
	    {
	      job_list[index] = node;
	      node->next = jp;
	    }
	  else	/* Middle of the list insertion */
	    {
	      jn->next = node;
	      node->next = jp;
	    }
	}
    }
  return;
}

int MacDocsisCMTS::NumberJobsQueued(struct job* node, int index)
{
  jptr jp,jn;
  jp = job_list[index];
  jn = jp;
  int count = 0;
  
  if (!jp)
  {
      return count;
  }
  else
  {      
    while (jp) 
    {
      jp = jp->next;	  
      count++;
    }
  }
  return count;
}

/*****************************************************************************
* routine: void MacDocsisCMTS::HandleReq(u_char req, int cindex, int findex)
*
* explanation:
*   Is called when there has been a request for bw from a CM.
*   The CM can only request from a BE or RT_POLL SID.
*   The routine creates a new job entry (node), fills it in
*   and inserts the job in the job queue.
*
* inputs:
*
* outputs
*
*
*****************************************************************************/
void MacDocsisCMTS::HandleReq(u_char req, int cindex, int findex)
{
  char  type, sclass;  
  u_int16_t flow_id;
  u_int32_t size;  
  double deadline, period, release, curr_time;  
  jptr node;  
  static int gg = 0;
  

#ifdef TRACE_CMTS 
      printf("CMTS:HandleReq(%lf): flow %d, a request arrived: %d\n", 
	     Scheduler::instance().clock(), CmRecord[cindex].u_rec[findex].flow_id, (int) req);
#endif
  /* Update the Simulation output variables */
  
  curr_time = Scheduler::instance().clock();
  
  if (curr_time - last_mbfreq >= 1.0)
    {
      last_mbfreq = curr_time;
      avg_bfreq = (avg_bfreq + num_bfreq)/2;
      num_bfreq = 0;
      avg_szbfqueue = (avg_szbfqueue + size_bfqueue)/2;
      size_bfqueue = 0;

#ifdef TRACE_CMTS //------------------------------------------------------
      printf("Avg best-effort req per sec = %d \n",avg_bfreq);
#endif//------------------------------------------------------------------  
    }
  
  if (curr_time - last_mrtreq >= 1.0)
    {
      last_mrtreq = curr_time;
      avg_rtreq = (avg_rtreq + num_rtreq)/2;
      num_rtreq = 0;
      avg_szrtqueue = (avg_szrtqueue + size_rtqueue)/2;
      size_rtqueue = 0;

#ifdef TRACE_CMTS //-------------------------------------------------------
      printf("Avg rt-vbr req per sec = %d \n",avg_rtreq);
#endif //------------------------------------------------------------------
    }
    
  node = (struct job *) malloc(sizeof (struct job));
  
  /* For debugging Fragmentation.. */
  sclass = CmRecord[cindex].u_rec[findex].sched_type;
  
  
  size = req;
  deadline = determine_deadline(sclass,cindex,findex);
  period = deadline; /* So that deadline is properly updated.. */
  release = Scheduler::instance().clock();
  type = 0;


  int channelNumber=defaultUSChannel;
  struct channelPropertiesType  channelProperties;
  myPhyInterface->getChannelProperties(&channelProperties,channelNumber);

  int x = size * channelProperties.bytes_pminislot;
  int overhead = myPhyInterface->getOverHead(x,channelNumber);

#ifdef TRACE_CMTS 
      printf("CMTS:HandleReq(%lf): channel:%d, flow %d, req:%d (increase with OH:%d),  deadline:%f,  release:%f\n", 
	     Scheduler::instance().clock(), channelNumber,CmRecord[cindex].u_rec[findex].flow_id,
	     req,overhead,deadline,release);
      printf("CMTS:HandleReq(%lf): req in slots:%d, request in bytes:%d, overhead in bytes:%d \n", 
	     Scheduler::instance().clock(), req,x,overhead);
#endif 

//  size +=overhead;
  x +=overhead;
  int y = (x % channelProperties.bytes_pminislot);
  y = (x/channelProperties.bytes_pminislot);
  if (y != 0)
    y += 1;

  size=y;
  req = size;
#ifdef TRACE_CMTS 
      printf("CMTS:HandleReq(%lf):  req updated for overhead :%d \n", 
	     Scheduler::instance().clock(), size);
#endif 

  fill_job(node, sclass, type, period, size, release, 
	   CmRecord[cindex].u_rec[findex].flow_id );

  /* Assing the deadline properly...  */
  if (sclass == RT_POLL)
    {
#ifdef TRACE_CMTS 
      printf("HandleReq: Inserting RT_POLL  Job\n");
#endif 
      InsertJob(node, 2);
      num_rtreq++;
      size_rtqueue++;
    }
  else 
    {
#ifdef TRACE_CMTS 
      printf("HandleReq: Inserting best effort Job\n");
#endif 
      InsertJob(node, 3);
      num_bfreq++;
      size_bfqueue++;
      gg += 1;
    }  
}

/***********************************************************************************
 *
 * u_int32_t MacDocsis::calculate_slots(double stime, double etime)
 *
 * input:    
 *   double stime:   starting time for the map
 *   double etime: ending time
 *
 * outputs:  returns the number of slots in the map.
 *
 * function:  
 * ***********************************************************************/
u_int32_t MacDocsis::calculate_slots(double stime, double etime)
{  
  u_int32_t n;
  double d,r;
  int p,q;
  float s,t;
  
  if (etime < stime)
    {
      printf("MacDocsisCMTS->calculate_slots: Error 'etime (%lf)' < 'stime (%lf)',exiting\n",etime,stime);
      exit(1);
    }
  
  //r =  ((10*10*10*10*10*10)*(etime - stime))/ (size_mslots);  
  
  r =  ((etime - stime))/ (size_mslots);
  s = (float)r;  
  
  //r += 0.5;
  
  n = (u_int32_t)s;
  
  if (n >= 0)
    return n;
  else
    {
      printf("MacDocsisCMTS->calculate_slots: Error 'n' < 0,exiting\n");
      exit(1);
    }  
}

/**********************************************************************
 *
 * void MacDocsisCMTS::alloc_bw()
 *
 * input:  none
 *
 * outputs:  none
 *
 * function:  
 *   This is called when a MAP is to be sent. It allocates the bw.
 *   It adds all periodic jobs to the top of the mptr (sorted based 
 *   on earliest deadline).  Then it tries to fit BE jobs.
 *
 *   Overview
 *   -Call calculate_slots to find out how many slots are to be in this MAP
 *   -Add UGS and VBR poll jobs to the global map list (mptr)
 *
 **********************************************************************/
static int firstMAP = 1;

void MacDocsisCMTS::alloc_bw()
{
  int i;
  u_int32_t num_slots,c_slots;
  jptr jp;
  double map_time,ack_time = 999999.0;
  FILE* fp;
  num_slots = calculate_slots(map_stime,map_etime);
  
#ifdef TRACE_CMTS //------------------------------------------------
  printf("alloc_bw( %lf), calculate_slots finds nominal num_slots:%d \n",
	 Scheduler::instance().clock(),num_slots);
#endif //-----------------------------------------------------------
  
  map_time = (map_etime - map_stime);    
  num_dgrant = 0;
  num_contention = 0;
  num_req = 0;
  num_gpend = 0;
  
  //AckTime = 0;
  
  map_lookahead = MAP_LOOKAHEAD;
  numIE = 0;
  
  
  //print_job_list(3);
  
  /* This function will decide the new values of 
     proportion and num_contention_slots...
     Currently does nothing....*/
  tune_parameters();
  
  /* 
     insert the periodic jobs first 
     Start with job_list[0] which is UGS periodic
     job_list[1] which is rt-VBR periodic
     job_list[2] which is rt-VBR non periodic
     job_list[3] which is best effort
     Allocate slots to periodic jobs until no more slots or no more jobs
  */
  
  if (firstMAP == 1) {
#ifdef FILES_OK
    if (TRACE_SCHEDULER >0) { 
      FILE* Sfp;
      Sfp = fopen("SCHED.out", "w+");
      fprintf(Sfp,"%lf: num_slots:%d, map_lookahead:%d \n",Scheduler::instance().clock(),num_slots,map_lookahead);
      fclose(Sfp);
    }
#endif
    firstMAP = 0;
  }
  if (TRACE_SCHEDULER > 0) {
    examineJOBS(TRACE_SCHEDULER);
  }

  //Loop to handle periodic UGS and periodic RTVBR jobs
  //The RTVBR is for unicast request opportunities
  //But make sure we don't insert more than 255 IE's in a map
  for (i = 0; i < 2 && num_slots > 0 && numIE < 255; i++)
    {  
      jp = job_list[i];
      
      //For this type of job, loop through each job
      while ((num_slots > 0)  && (jp))
	{	

	  /* Double comparison */	  
	  double diff = fabs(jp->release_time - map_stime);

#ifdef TRACE_CMTS //------------------------------------------------
	  printf("alloc_bw: release time %f ddline %f (num_slots:%d)\n",jp->release_time,jp->deadline,num_slots);
	  	print_job_list(0);
	  printf("alloc_bw: map_stime %lf jp->release_time %lf (diff:%lf)\n",map_stime,jp->release_time,diff);
#endif //-----------------------------------------------------------
	  
	  
	  if(diff <= EPSILON*fabs(jp->release_time) || 
	     (jp->release_time < map_stime) || 
	     (jp->deadline < map_etime))
	    
	    // if ((jp->release_time <= map_stime) || (jp->deadline < map_etime))
	    {
	      int num = 1,k=1,status = 0;
	      double t;
	      
	      if (ack_time < jp->release_time)
		ack_time = jp->release_time;
	      
	      /* Num of jobs to be released*/
	      /* Release jobs not covered in the previous MAP due to shortage of slots */
	      
	      t = (map_etime - jp->release_time);
	      
	      /* Have to take ceiling here */
	      num = (int)((t) / (jp->period));
	      num++;
	      
	      //num will contain the number of times
	      //this job needs to be completed. Normally
	      //this will be 1.
	      while ((num_slots > 0) && (num > 0))
		{		  
	         //returns a 1 on success, 0 on failure
		  status = insert_mapjob(k,jp,&num_slots,i);
		  
#ifdef TRACE_CMTS 
		  printf("alloc_bw( %lf), Insert a periodic job in the map, updated num_slots: %d, status:%d, numIE=%d , num: %d\n",
			 Scheduler::instance().clock(),num_slots,status,numIE,num);
#endif 
		  
		  /* ! status indicates that not enough slots are available for this job*/
		  if (!status) {
#ifdef TRACE_CMTS 
		    printf("alloc_bw( %lf), Failed to insert job (num_slots: %d, status:%d  \n",
			 Scheduler::instance().clock(),num_slots,status);
#endif 
		    break;
		  }
		  
		  jp->release_time += jp->period;
		  jp->deadline = jp->release_time + jp->period;
//		  jp->deadline = jp->release_time + 0.05;
		  num--;
		  k++;
		  numIE++;
		}
	    }
	  jp = jp->next;
	}
    }
  
#ifdef TRACE_CMTS 
  printf("alloc_bw( %lf), Scheduled this many periodic jobs:  %d\n",
	 Scheduler::instance().clock(),numIE);
#endif 
  
  //print_job_list(0);
  
  /* 
     Quite possible that no UGS or RT-VBR periodic 
     jobs are being generated in the current MAP
     Now fill the map with the jobs- mptr will be 
     NULL if no statically configured jobs.
  */
  
  if (mptr)
    FillMap(0); 

#ifdef TRACE_CMTS 
  printf("alloc_bw( %lf), Back from FillMap, numIE:  %d\n",
	 Scheduler::instance().clock(),numIE);
#endif 
  
  u_int32_t saved_num_slots = num_slots;
  
  /* reserve the slots for Station maintainence, contention*/
  num_slots -= ((Conf_Table_.mapparam.num_sm_slots + Conf_Table_.mapparam.num_contention_slots) * (size_ureqgrant));
  
  /* Note:  num_slots might go negative here..... */
  
  num_rtslots = (u_int32_t)((proportion) * num_slots);
  num_befslots = num_slots - num_rtslots;
  
#ifdef TRACE_CMTS //--------------------------------------------------------------------------------------------------
  printf("alloc_bw:Before FillMap, Num of unused slots in the MAP = %d (saved#slots:%d), #rtslots:%d, #befslots:%d\n",
	 (int) num_slots,saved_num_slots,num_rtslots,num_befslots);
#endif //-------------------------------------------------------------------------------------------------------------
  
  num_slots = FillMap(1);  /* This will fit into the MAP best effort jobs */
  
  if (num_slots < 0)
    num_adjust_slots = -(num_slots);
  else
    num_adjust_slots = 0;
  
  /* THESE MANY SLOTS WILL NOT BE DESCRIBED AGAIN IN NEXT MAP..OVER-ALLOCATION HAS BEEN DONE IN THE MAP...*/
  
#ifdef TRACE_CMTS //---------------------------------------------------------------------------------
  printf("alloc_bw:Num of unused slots in the MAP after the FillMap  = %d, and num_adjust_slots:%d\n",
	 (int) num_slots,num_adjust_slots);
#endif //--------------------------------------------------------------------------------------------
  
  /* 
     We used to call this with num_slots but it didn't make sense....we want to call it
     with the full map size.  It returns the number of unused slots.
  */
  num_slots = MarkOtherAlloc(saved_num_slots);
  
#ifdef TRACE_CMTS //------------------------------------------------------------
  printf("alloc_bw:After markOtherAlloc, Num of unused slots in the MAP = %d\n",
	 (int) num_slots);
#endif //-----------------------------------------------------------------------
  
  /* 
     since all the queues are sorted in ascending order, the head job of each 
     queue will give an idea of the smallest job not served in this map..
     which will be our ack-time..
  */
  
  num_slots = MarkUnusedSlots(num_slots); /* Function to insert 
					     unused holes in the MAP..*/
  
#ifdef TRACE_CMTS //--------------------------------------------------------------
  printf("alloc_bw:After MarkUnusedSlots, Num of unused slots in the MAP = %d\n",
	 (int) num_slots);
#endif //-------------------------------------------------------------------------
  
  if (TRACE_SCHEDULER >100) { 
    if (NumberJobsQueued((jptr)job_list, BE_JOBS)>0)
      printf("alloc_bw(AFTER %lf), num_slots:%d,  num jobs (UGS,RTPS,Polls,BE):%d,%d,%d,%d \n",
	 Scheduler::instance().clock(),num_slots,
         NumberJobsQueued((jptr)job_list, UGS_JOBS),
         NumberJobsQueued((jptr)job_list, RTPS_JOBS),
         NumberJobsQueued((jptr)job_list, POLL_JOBS),
         NumberJobsQueued((jptr)job_list, BE_JOBS));
  }

  SendMap(); 
  avg_dgrant = (avg_dgrant + num_dgrant)/2;
  avg_contention = (avg_contention + num_contention)/2;
  avg_req = (avg_req + num_req)/2;
  avg_gpend = (avg_gpend + num_gpend)/2;  

}

/***************************************************************************
 *
 * u_int32_t MacDocsisCMTS::MarkUnusedSlots(u_int32_t num_slots)
 *
 * input:  u_int32_t num_slots : number of unused slots in the map
 *
 * outputs:  returns the final number of unused slots 
 *           (hopefully 0 if FILLWITHCONTOPS is on)
 *
 * function:  This method fills unused slots of the current map as unused.
 *   Or, if FILLWITHCONTOPS  is defined, it fills with CR slots.
 *   The global mptr points to the current map.
 *
 ***************************************************************************/
u_int32_t MacDocsisCMTS::MarkUnusedSlots(u_int32_t num_slots)
{
#ifndef FILLWITHCONTOPS //--------------------------------------------------
  mapptr m,prev,node;
  double t = map_stime;
  m = mptr;
  prev = m;
  
  while (m)
    {
      //printf("3..\n");
      /* Double comparison */
      //if (t != m->alloc_stime) //A hole
      
      if (fabs(t - m->alloc_stime) > EPSILON*fabs(t)) /* A hole */
	{
	  
#ifdef TRACE_CMTS //-----------------------------------------------------------------------
	  printf("MarkUnusedSlot(%lf) Found a hole (1)  (t- stime=%lf and EPSILON*t:%lf\n",
		 Scheduler::instance().clock(),
		 fabs(t-m->alloc_stime),(EPSILON*fabs(t)));
#endif //----------------------------------------------------------------------------------
	  
	  node = (mapptr) malloc(sizeof(struct map_list));
	  
	  if (!node)
	    {
	      printf("MacDocsisCMTS->MarkUnusedSlots: Malloc failed, exiting\n");
	      exit(1);
	    }
	  
	  node->alloc_stime = t;
	  node->alloc_etime = m->alloc_stime;	  
	  node->release_time = 0;
	  node->deadline = 0;
	  node->nslots = 0;
	  node->flag = 0;
	  node->flow_id = 0xfff0;
	  
	  if (prev != mptr)
	    {
	      prev->next = node;
	      node->next = m;
	    }
	  else if (prev == mptr)
	    {
	      node->next = mptr->next;
	      mptr = node;
	    }
	  
	  prev = m;
	  t =  prev->alloc_etime;
	  m = m->next;	  	  
	}
      else
	{
	  prev = m;
	  t = prev->alloc_etime;
	  m = m->next;
	}
    }
  
  /* Check whether end of the map reached or not */
  /* Double comparison */
  
  if (fabs(t - map_etime) > EPSILON*fabs(t)) 
    //if (t != map_etime)
    {
      node = (mapptr) malloc(sizeof(struct map_list));
      
      if (!node)
	{
	  printf("MacDocsisCMTS->MarkUnusedSlots: Malloc failed, exiting\n");
	  exit(1);
	}
      
      node->alloc_stime = t;
      node->alloc_etime = map_etime;      
      node->release_time = 0;
      node->flag = 0;
      node->deadline = 0;
      node->nslots = 0;
      node->flow_id = 0xfff0;
      prev->next = node;
      node->next = 0;
      
#ifdef TRACE_CMTS //--------------------------------------------------------------------------------------
      printf("MarkUnusedSlot(%lf) Found a hole at the end of the MAP  (t- etime=%lf and EPSILON*t:%lf\n",
	     Scheduler::instance().clock(), fabs(t-map_etime),(EPSILON*fabs(t)));
#endif //-------------------------------------------------------------------------------------------------
    }
  
  return num_slots;

#else //---------------------- FILLWITHCONTOPS not defined --------------------------------
  
  mapptr m;
  u_int32_t num;
  int jk = 1;   /* Init to add contention slots */
  
#ifdef TRACE_CMTS //----------------------------------------------------------------------------------------
  printf("MarkUnusedSlots(%lf) We will fill this many slots with contention ops : %d, map startime: %lf\n",
	 Scheduler::instance().clock(),num_slots,map_stime);
#endif  //--------------------------------------------------------------------------------------------------
  
  //	for (jk = 0; (jk < 2) && (numIE < 255); jk++)

  if (numIE < 255)
    {
      num = num_slots;
      num = num * size_ureqgrant; /* In case if a slot is not big enough 
				     to carry a complete Request frame */
      m = mptr;
      
      if (!mptr)
	{
	  /* So fill the entire map up ..... */
	  MarkOtherSlots(0,num,jk);
	  num_slots -= num;

#ifdef TRACE_CMTS //-------------------------------------------------------------
	  printf("MarkUnusedSlots(%lf) 0: Filled entire map, num_slots :  %d\n",
		 Scheduler::instance().clock(),num_slots);
#endif //------------------------------------------------------------------------

	  goto local_exit;
	}
      /* Double comaprison */
      //if (m->alloc_stime > map_stime)
      
      if ((fabs(m->alloc_stime - map_stime) > EPSILON*fabs(m->alloc_stime)) && 
	  (m->alloc_stime > map_stime))
	{
	  int i;
	  //printf("Calling calculate_slots..7\n");
	  i = calculate_slots(map_stime, m->alloc_stime);

#ifdef TRACE_CMTS //-------------------------------------------------------
	  printf("MarkUnusedSlots(%lf) 1: calculate slots returns:  %d\n",
		 Scheduler::instance().clock(),i);
#endif //------------------------------------------------------------------
	  
	  if (i)
	    {
	      if (num > i) 
		{
		  num = num - i;
		  num_slots -= i;
		  MarkOtherSlots(m,i,jk);
		}
	      else
		{
		  num_slots -= num;
		  MarkOtherSlots(m,num,jk);
		  num = 0;
		}
	      if (num <= 0)
		goto local_exit;
	    }
	  
	}
      
      while (m->next && num > 0)
	{
	  //printf("14..\n");
	  int i;
	  //printf("Calling calculate_slots..8\n");
	  i = calculate_slots( m->alloc_etime,m->next->alloc_stime);
 
#ifdef TRACE_CMTS //------------------------------------------------------
	  printf("MarkUnusedSlots(%lf) 2: calculate slots returns:  %d\n",
		 Scheduler::instance().clock(),i);
#endif //-----------------------------------------------------------------
	  
	  if (i)
	    {
	      if (num > i) 
		{
		  num = num - i;
		  num_slots -= i;
		  MarkOtherSlots(m,i,jk);
		}
	      else
		{
		  num_slots -= num;
		  MarkOtherSlots(m,num,jk);
		  num = 0;
		}
	      if (num <= 0)
		goto local_exit;
	    }
	  m = m->next;
	}
      
      /* Double comaprison */
      if ((fabs(m->alloc_etime - map_etime) > EPSILON*fabs(m->alloc_etime)) && 
	  (m->alloc_etime < map_etime))
	//if (m->alloc_etime < map_etime)
	{
	  int i;
	  //printf("Calling calculate_slots..9\n");
	  i = calculate_slots( m->alloc_etime,map_etime);

#ifdef TRACE_CMTS  //-----------------------------------------------------
	  printf("MarkUnusedSlots(%lf) 3: calculate slots returns:  %d\n",
		 Scheduler::instance().clock(),i);
#endif //-----------------------------------------------------------------
	  
	  if (i)
	    {
	      if (num > i)
		{
		  num = num - i;
		  num_slots -= i;
		  MarkOtherSlots(m,i,jk);
		}
	      else
		{
		  num_slots -= num;
		  MarkOtherSlots(m,num,jk);
		  num = 0;
		}
	      
	      if (num <= 0)
		goto local_exit;	      
	    }
	}
    }
 local_exit:
  return num_slots;
#endif //---------------- FILLWITHCONTOPS not defined --------------------------
}

/***************************************************************************
 *
 * void MacDocsisCMTS::SendMap()
 *   
 *   This method creates the MAP message, creates the packet
 *   that contains the message  and sends it.
 *
 * input: none
 *
 * outputs: none
 *
 * The MAP Information Elements look like:
 *   bit 0  - 13 : SID
 *   bit 14 - 17 : Interval Usage Code (IUC)
 *   bit 18 - 31 : offset 
 *   
 ***************************************************************************/
void MacDocsisCMTS::SendMap()
{
  //u_char m;
  int m;
  u_int32_t tmp,s,fl;
  u_int32_t iuc = 0; 
  mapptr k;
  double stime,t;
  double last_entry = 0.0;
  
  m = find_size_map();
#ifdef TRACE_CMTS  //--------------------------------------------------------
  printf("\nSendMap(%lf):  There are %d IE's \n", Scheduler::instance().clock(),m);
  fflush(stdout);
#endif //--------------------------------------------------------------------
  
  if (m==0) {
    printf("\nSendMap(%lf): ERROR: find_size_map is 0  ?? \n", Scheduler::instance().clock());
    fflush(stdout);
    exit(1);
  }
  
  Packet* p = AllocPkt((m)*4);
  
  if (!p) {
    printf("\nSendMap(%lf): AllocPkt returned NULL ?? \n", Scheduler::instance().clock());
    fflush(stdout);
    exit(1);
  }
  
#ifdef TRACE_CMTS  //--------------------------------------------------------
  printf("\nSendMap(%lf):  POINT A  \n", Scheduler::instance().clock());
  fflush(stdout);
#endif //--------------------------------------------------------------------

  hdr_docsismap  *map  = hdr_docsismap::access(p);
  hdr_docsismgmt  *mgmt  = hdr_docsismgmt::access(p);
  hdr_docsis   *dh  = hdr_docsis::access(p);
  hdr_cmn   *ch  = HDR_CMN(p);
  hdr_mac   *mp  = HDR_MAC(p);
  docsis_chabstr *ch_abs = docsis_chabstr::access(p);    
  u_int32_t * data = (u_int32_t *)p->accessdata();
  
#ifdef TRACE_CMTS  //--------------------------------------------------------
  printf("\nSendMap(%lf): POINT B  \n", Scheduler::instance().clock());
  fflush(stdout);
#endif //--------------------------------------------------------------------

  if (!data)
    {      
      printf("\nSendMap(%lf): accessdata returned NULL ?? \n", Scheduler::instance().clock());
      fflush(stdout);
      print_job_list(3);
      exit(1);
    }
  
  ch->uid() = 0;

  //ch->ptype() = PT_MAC;
  ch->ptype() = PT_DOCSISMAP;
  ch->iface() = -2;
  ch->error() = 0;
  
  /* Fill the map header */
  map->numIE_() = m;
  map->allocstarttime_() = map_stime;
  map->allocendtime_() = map_etime;

  map->bkoff_start_() = Conf_Table_.mapparam.bkoff_start; 
  map->bkoff_end_() = Conf_Table_.mapparam.bkoff_end; 
  map->acktime_() = CalculateAckTime();

#ifdef TRACE_CMTS  //--------------------------------------------------------
  printf("\nSendMap(%lf): POINT C  \n", Scheduler::instance().clock());
  fflush(stdout);
#endif //--------------------------------------------------------------------
  
  //if (AckTime == 0)
  //AckTime = omap_stime - 3*Conf_Table_.mapparam.map_interval;
  //map->acktime_() = AckTime;
  
  k = mptr;
  
  if (!k)
    printf("k is Null\n");
  
  while(k)
    {
      //printf("4..\n");      
      iuc = k->flag;  
            
      /* fit the IUC after 14 bits of service-id..*/
      /* So the IE is packed as offset,iuc,sid (higher bits to lower bits*/      
      iuc = iuc << 14;
      *data = 0;
      *data = *data | iuc;
      
      *data = *data | k->flow_id;

      /* Double Comaprison */
      //if (map_stime == k->alloc_stime)      
      if (fabs(map_stime - k->alloc_stime) <= EPSILON*fabs(map_stime))
	tmp = 0;
      else
	{	  	  
	  //printf("Calling calculate_slots..4\n");
	  tmp = calculate_slots(map_stime,k->alloc_stime);
	}     
 
      s = tmp;
      tmp = tmp << 18;
      *data = *data | tmp;
      data++;
      k = k->next;
      iuc = 0; 
    }   
#ifdef TRACE_CMTS  //--------------------------------------------------------
  printf("\nSendMap(%lf): POINT D  \n", Scheduler::instance().clock());
  fflush(stdout);
#endif //--------------------------------------------------------------------
  
  /* put a NULL IE */  
  *data = 0;
  //printf("Calling calculate_slots..5\n");
  tmp = calculate_slots(map_stime,map_etime);
  tmp = tmp << 18;
  *data = *data | tmp;
  //data++;
  
  double atime = map->acktime_();
  MarkGrantPending(data,atime);   

#ifdef TRACE_CMTS  //--------------------------------------------------------
  printf("\nSendMap(%lf): POINT E  \n", Scheduler::instance().clock());
  fflush(stdout);
#endif //--------------------------------------------------------------------
    
  /* add other headers */  
  mgmt->dstaddr_() = -1;
  mgmt->srcaddr_() = index_;
  mgmt->type_() = MAPMGTMESSAGE; /* Indicating MAP message */
//  mgmt->type_() = 1; /* Indicating MAP message */
  mp->macSA() = index_;
  mp->macDA() = -1;
  
  /* m denotes the number of information elements in the MAP (each IE is 4 bytes*/
  ch->size() += SIZE_MGMT_HDR + SIZE_MAP_HDR + m * 4;
  dh->dshdr_.fc_type = MAC_SPECIFIC;
  dh->dshdr_.fc_parm = MANAGEMENT;
  dh->dshdr_.ehdr_on = 0;
  dh->dshdr_.mac_param = 0;
  dh->dshdr_.len = ch->size();
  ch->size() += DOCSIS_HDR_LEN;

#ifdef TRACE_CMTS  //--------------------------------------------------------
  printf("\nSendMap(%lf): POINT F  \n", Scheduler::instance().clock());
  fflush(stdout);
#endif //--------------------------------------------------------------------
  

#ifdef TRACE_CMTS  //--------------------------------------------------------
  printf("\nSendMap(%lf): POINT G ,  channel num: %d \n", Scheduler::instance().clock(),ch_abs->channelNum);
  fflush(stdout);
#endif //--------------------------------------------------------------------
  
#ifdef TRACE_CMTS  //--------------------------------------------------------
  printf("\nSendMap(%lf):  map list ....\n", Scheduler::instance().clock());
  fflush(stdout);
  print_map_list();
#endif //--------------------------------------------------------------------
  

  if (TRACE_SCHEDULER > 0) {
    examineMAP(TRACE_SCHEDULER,p);
  }

#ifdef TRACE_CMTS  //--------------------------------------------------------
  printf("\nSendMap(%lf): POINT H  \n", Scheduler::instance().clock());
  fflush(stdout);
#endif //--------------------------------------------------------------------

#ifdef TRACE_CMTS 
  printf("SendMap( %lf), startTime:%f, stopTime:%f, num_dgrant:%d, num_gpend:%d \n",
	 Scheduler::instance().clock(),map_stime,map_etime, num_dgrant,num_gpend);
#endif 
  

  if (DUMPCMTSMAP == 1)
    print_short_map_list(p);

//$A601  -  do this after the call to print_short_map
  MacSendFrame(p,MGMT_PKT);  
  delete_maplist();
  return;  
}

/***************************************************************************
 *
 * Routing: void MacDocsisCMTS::SendCMUpdateMsg()
 *   
 * input: none
 *
 * outputs: none
 *
 ***************************************************************************/
void MacDocsisCMTS::SendCMUpdateMsg()
{
  struct CMUpdate_docsismgmt CMUpdateMsg;
  int i;
  u_char *srcPtr,*dstPtr;
  int msgSize = sizeof (struct CMUpdate_docsismgmt);
  double curr_time =  Scheduler::instance().clock();


#ifdef TRACE_CMTS  //--------------------------------------------------------
  printf("\nSendCMUpdateMsg(%lf): msgSize:%d \n", curr_time,msgSize);
#endif //--------------------------------------------------------------------
  
 //Create the msg data
 CMUpdateMsg.numberCMs = numberCMsToUpdate;
 for (i=0; i<numberCMsToUpdate; i++) {
   CMUpdateMsg.newScaleParam[i] = (u_char)CmStatusUpdateData[i].nextScaleParam;
#ifdef TRACE_CMTS  //--------------------------------------------------------
   printf("\nSendCMUpdateMsg(%lf): CM:%d, new param:%d\n", curr_time,
            i,(int)CMUpdateMsg.newScaleParam[i]);
#endif //--------------------------------------------------------------------
 }
  Packet* p = AllocPkt(sizeof(u_char) * msgSize);
  
  if (!p) {
    printf("\nSendCMUpdateMsg(%lf): AllocPkt returned NULL ?? \n", curr_time);
    exit(1);
  }
  
  hdr_docsis   *dh  = hdr_docsis::access(p);
  hdr_cmn   *ch  = HDR_CMN(p);
  hdr_mac   *mp  = HDR_MAC(p);
  hdr_docsismgmt  *mgmt  = HDR_DOCSISMGMT(p);

  u_int32_t * data = (u_int32_t *)p->accessdata();
  
  ch->ptype() = PT_DOCSISCMUPDATE;
  ch->iface() = -2;
  ch->error() = 0;
  
    
  mp->macSA() = index_;
  mp->macDA() = -1;
  
  /* m denotes the number of information elements in the MAP (each IE is 4 bytes*/

  ch->size() += SIZE_MGMT_HDR + msgSize;
  ch->size() += ETHER_HDR_LEN;

  dh->dshdr_.fc_type = MAC_SPECIFIC;

// Or MANAGEMENT ??
//  dh->dshdr_.fc_parm = MANAGEMENT;
  dh->dshdr_.fc_parm = STATUS;
  dh->dshdr_.ehdr_on = 0;
  dh->dshdr_.mac_param = 0;
//  Note:  msg might be > 255 so don't do this
//  dh->dshdr_.mac_param = (u_char)msgSize;
  dh->dshdr_.len = ch->size();
  ch->size() += DOCSIS_HDR_LEN;
  
#ifdef TRACE_CMTS  //--------------------------------------------------------
    printf("SendCMUpdateMsg(%lf):Sending a CM status message of msgSize:%d and pkt size:%d\n",
            curr_time,msgSize,ch->size());
#endif

  mgmt->dstaddr_() = -1;
  mgmt->srcaddr_() = index_;
  mgmt->type_() = CMUPDATEMESSAGE; 

 //copy struct to msg
  srcPtr = (u_char *)&CMUpdateMsg;
  dstPtr = (u_char *)data;
  for(i=0; i<msgSize; i++) {
    *dstPtr++ = *srcPtr++;
  }

  
#ifdef TRACE_CMTS  //--------------------------------------------------------
  printf("\nSendCMUpdateMsg(%lf): Send Frame\n", curr_time);
#endif //--------------------------------------------------------------------
  

  MacSendFrame(p,MGMT_PKT);  

  return;  
}


/*****************************************************************************
 Calculate the size of MAP, all the IEs plus data-grant pending sizes..
*****************************************************************************/
//u_char MacDocsisCMTS::find_size_map()
int MacDocsisCMTS::find_size_map()
{  
  jptr j;
  mapptr m;
  int i = 2;

  //u_char s = 1;
  //u_char r = 1;

  int s = 1;
  int r = 1;
  
  m = mptr;
	
  while (m)
    {
      //printf("5..\n");
      m = m->next;
      s++;
    }
  //printf("Length of map-list %d\n",s-1);
  //r = s - 1;
  
  while (i < 4)
    {
      //printf("6..\n");
      j = job_list[i];
      
      while (j)
	{
	  //printf("7..\n");
	  j = j->next;
	  s++;
	}
      i++;
    }
  //printf("Length of job-list %d\n",s-r);    
  //printf("returning %d\n",s);

  return s;
}

/*****************************************************************************

*****************************************************************************/
void MacDocsisCMTS::MarkGrantPending(u_int32_t* data, double acktime)
{
  u_int32_t tmp;
  jptr j;
  int i = 2;
  
  while (i < 4)
    {
      //printf("8..\n");
      j = job_list[i];
      
      while (j)
	{
	  //printf("9..\n");
	  /* Add grant pending only for requests which 
	     arrived before the acktime of current map */
	  //if ((fabs(j->release_time - acktime) <= EPSILON*fabs(j->release_time)) || 
	  //(j->release_time < acktime))

	  {
	    data++;
	    *data = 0;
	    *data = *data | j->flow_id;
	    //printf("Calling calculate_slots..6\n");
	    tmp = calculate_slots(map_stime,map_etime);
	    tmp = tmp << 18;
	    *data = *data | tmp;
	    num_gpend++;
	  }
	  j = j->next;
	}
      i++;
    }  
#ifdef TRACE_CMTS 
  printf("MarkGrantPending: Num of grant pending = %d\n",num_gpend);
#endif 
}

/*****************************************************************************

*****************************************************************************/
double MacDocsisCMTS::CalculateAckTime()
{

  jptr j;
  int i = 2, queue_empty = 0;  
  double ack_time = AckTime;
  
  while ( i < 4)
    {
      //printf("10..\n");
      j = job_list[i];
      
      while (j)
	{
	  //printf("11..\n");
	  if (ack_time > j->release_time)
	    ack_time = j->release_time;
	  
	  j = j->next;
	  queue_empty = 1;
	}    
      i++;
    }
  
  if (!queue_empty)
    { 
		if(myMedium->findIdleChannel(CHANNEL_DIRECTION_UP) != -1)  //TODO redo the CALCULATE ACK TIME method
			ack_time = omap_stime;
	} 
  ack_time -=  (Conf_Table_.mapparam.map_interval * MapPropDelay);
  if (ack_time  < 0)
    ack_time = 0;


  return ack_time;  
}

/*****************************************************************************

*****************************************************************************/
void MacDocsisCMTS::delete_maplist()
{
  mapptr m,tmp;
  m = mptr;
  
  if (!m)
    printf("delete_maplist: m is Null\n");
  
  tmp = m;
  m = m->next;
  free(tmp);
  mptr = 0;
  
  while (m)
    {
      //printf("12..\n");
      tmp = m;
      m = m->next;
      free(tmp);
    }
  return;  
}


/*****************************************************************************

*****************************************************************************/
void MacDocsisCMTS::print_map_list()
{
  mapptr m;
  m = mptr;
  
  while (m)
    {
      //printf("13..\n");
      printf("flow-id =%d\n",m->flow_id);
      printf("Alloc_stime =%lf\n",m->alloc_stime);
      printf("Alloc_etime =%lf\n",m->alloc_etime);
      printf("release time =%lf\n",m->release_time);
      printf("deadline =%lf\n",m->deadline);
      printf("mini-slots =%d\n\n",m->nslots);
      m = m->next;
    }
  return;
}

/*************************************************************************
 *
 * void MacDocsisCMTS::print_short_map_list(Packet* p)
 *
 * input:   Packet *p : the packet that contains the map message
 *
 * outputs:  none
 *
 * function:  This method dumps all IE's associated with the map.
 *  The format:  each MAP will begin with a code of 0 and have zero
 *   or more codes of 1,2,3.
 *  0 timestamp mapstart mapstop numberIE's  
 *  1 timestamp  flow_id start stop  release time deadline IUC numberslots
 *
 *  Note that this is more info than what is sent in the MAP....
 *  Note: Each ie consumes 4 octets. In a busy network, the size
 *  of each MAP will be about 4*numberCMs (assuming each CM is backlogged)
 *  Note: This only dumps the information associated with grant
 *  assignments for this MAP time.  The IE's for data pendings are contained
 *  in the data field.  This routine does not show these.
 *
 *
 ************************************************************************/
void MacDocsisCMTS::print_short_map_list(Packet* p)
{
  mapptr m;
  m = mptr;
  FILE* fp;
  FILE* fpch;
  hdr_docsismap  *map  = hdr_docsismap::access(p);
  hdr_docsismgmt  *mgmt  = hdr_docsismgmt::access(p);
  hdr_docsis   *dh  = hdr_docsis::access(p);
  hdr_cmn   *ch  = HDR_CMN(p);
  hdr_mac   *mp  = HDR_MAC(p);
  u_int32_t * data = (u_int32_t *)p->accessdata();
   
  /*
    Note: we can look at the data- but it's 
     different from what the mapptr contains.
     See MacDocsisCM::UpdateAllocationTable 
     for how to do this.... 
  */

  //	m = (mapptr) data;
  
#ifdef FILES_OK
  fp = fopen("CMTSMAP.out", "a+");

  fprintf(fp,"0 %lf %lf %lf %d %d %lf\n",Scheduler::instance().clock(),
	  map->allocstarttime_(),map->allocendtime_(),map->numIE_(),ch->size(),map->acktime_());
  while (m)
    {
      fprintf(fp,"1 %lf %d %lf %lf %lf %lf %d %d \n",
	      Scheduler::instance().clock(), m->flow_id,
	      m->alloc_stime,m->alloc_etime,m->release_time,
	      m->deadline,m->flag,m->nslots);
      m = m->next;
    }

  fclose(fp);
#endif
  return;
}

/**************************************************************************
 *
 * Routine : void MacDocsisCMTS::examineJOBS(int debugLevel)
 *
 * input:   int debugLevel:  Indicates how much is dumped to
 *         the output file (SCHED.out)
 *
 * outputs:  none
 *
 * function:  
 *      This prints information about the jobs waiting in
 *      the request queues.
 *
 *   J 1.997600 1,0,0,1   : time, #UGS,RT,Polls,BE requests 
 *      0 1 2.004000 0.050000 2.054000 4 0 34
 *      3 1 1.994071 0.500000 2.044071 6 0 5
 *    
 *     field 1: the job queue: 0 is ugs, 1 is rtps polls, 2:rtps data, 3:best effort
 *     field 2: the job index in the array (not all that meaningful)
             3: job release time
             4: job period
             5: job deadline
             6:  flow id
             7:  type 
             8:   number of slots
 ************************************************************************/
void MacDocsisCMTS::examineJOBS(int debugLevel)
{
  jptr jp;
  int numberJobs = 0;
  int i = 0;
  FILE* fp;
  
#ifdef FILES_OK
  fp = fopen("SCHED.out", "a+");

  fprintf(fp,"J %lf %d,%d,%d,%d \n",
	 Scheduler::instance().clock(),
         NumberJobsQueued((jptr)job_list, UGS_JOBS),
         NumberJobsQueued((jptr)job_list, RTPS_JOBS),
         NumberJobsQueued((jptr)job_list, POLL_JOBS),
         NumberJobsQueued((jptr)job_list, BE_JOBS));

  if (debugLevel > 1) {
    for (i=0; i<4; i++) {
      jp = job_list[i];
  
      if (jp)
      {
        numberJobs = 0;
        while (jp)
	{
          numberJobs++;
          if (debugLevel >2) {
	    fprintf(fp," %d %d   Release time = %lf\n",i,numberJobs,jp->release_time);
	    fprintf(fp,"    Period = %lf\n",jp->period);
	    fprintf(fp,"    Deadline = %lf\n",jp->deadline);
	    fprintf(fp,"    flow-id = %d\n",jp->flow_id);
	    fprintf(fp,"    type = %d\n",jp->type);
	    fprintf(fp,"    slots = %d\n",jp->mini_slots);
          }
          else {
	    fprintf(fp,"   %d %d %lf %lf %lf %d %d %d \n",i,numberJobs,jp->release_time,jp->period, jp->deadline, jp->flow_id, jp->type, jp->mini_slots);
          }

	  jp = jp->next;
	}
      }
    }
  }

  fclose(fp);
#endif
  return;
}
/*************************************************************************
 *
 * routine:  void MacDocsisCMTS::examineMAP(int debugLevel, Packet* p)
 *
 * input:   
 *    int debugLevel: 
 *    Packet *p : the packet that contains the map message
 *
 * outputs:  none
 *
 * function:  This method dumps to the SCHED.out file information
 *         about the MAP assignments that is being sent.
 *
 *  timestamp mapstart mapstop numberIE's packetsize  ACKtime  
 *
 *  M 1.997600 1.999600 2.001600 5 58 1.994071
 *     1.997600 6 1.999600 1.999725 1.994071 2.044071 0 5 
 *     1.997600 1 1.999725 1.999800 0.000000 0.000000 3 3
 *     1.997600 1 1.999800 2.000100 0.000000 0.000000 2 12
 *     1.997600 1 2.000100 2.001600 0.000000 0.000000 2 60
 *
 *   timestamp  flow_id start stop  release time deadline IUC numberslots
 *
 *
 *
 ************************************************************************/
void MacDocsisCMTS::examineMAP(int debugLevel, Packet* p)
{
  mapptr m;
  m = mptr;
  FILE* fp;
  hdr_docsismap  *map  = hdr_docsismap::access(p);
  hdr_docsismgmt  *mgmt  = hdr_docsismgmt::access(p);
  hdr_docsis   *dh  = hdr_docsis::access(p);
  hdr_cmn   *ch  = HDR_CMN(p);
  hdr_mac   *mp  = HDR_MAC(p);
  u_int32_t * data = (u_int32_t *)p->accessdata();
   
  /*
    Note: we can look at the data- but it's 
     different from what the mapptr contains.
     See MacDocsisCM::UpdateAllocationTable 
     for how to do this.... 
  */

  //	m = (mapptr) data;
  
#ifdef FILES_OK
  fp = fopen("SCHED.out", "a+");

  fprintf(fp,"M %lf %lf %lf %d %d %lf\n",Scheduler::instance().clock(),
	  map->allocstarttime_(),map->allocendtime_(),map->numIE_(),ch->size(),map->acktime_());
  if (debugLevel > 1) {
    while (m)
    {
      fprintf(fp,"   %lf %d %lf %lf %lf %lf %d %d \n",
	      Scheduler::instance().clock(), m->flow_id,
	      m->alloc_stime,m->alloc_etime,m->release_time,
	      m->deadline,m->flag,m->nslots);
      m = m->next;
    }
  }
  fclose(fp);
#endif

  return;
}

/***********************************************************************************
 *
 *u_int32_t MacDocsisCMTS::MarkOtherAlloc(u_int32_t num_slots)
 *
 * input:  u_int32_t num_slots : number of slots in the map
 *
 * outputs:  none
 *
 * Function:
 * This function will mark contention-slots and SM slots in map list 
 *
 *****************************************************************/
u_int32_t MacDocsisCMTS::MarkOtherAlloc(u_int32_t num_slots)
{
  mapptr m;
  u_int32_t num;
  int jk = 0;
  
#ifdef TRACE_CMTS //-------------------------------------------------
  printf("MarkOtherAlloc(%lf) Entered with num_slots:%d\n",
	 Scheduler::instance().clock(),num_slots);
#endif //------------------------------------------------------------
  
  for (jk = 0; (jk < 2) && (numIE < 255); jk++)
    {
      if (jk == 0)
	num = Conf_Table_.mapparam.num_contention_slots ;
      else
	num =  Conf_Table_.mapparam.num_sm_slots;
      
      num = num * size_ureqgrant; /* In case if a slot is not big enough 
				     to carry a complete Request frame */
      m = mptr;
      
      if (!mptr)
	{
	  MarkOtherSlots(0,num,jk);
	  num_slots -= num;
	  continue;
	}
      /* Double comaprison */
      //if (m->alloc_stime > map_stime)
      if ((fabs(m->alloc_stime - map_stime) > EPSILON*fabs(m->alloc_stime)) && 
	  (m->alloc_stime > map_stime))
	{
	  int i;
	  //printf("Calling calculate_slots..7\n");
	  i = calculate_slots(map_stime, m->alloc_stime);
	  
	  if (i)
	    {
	      if (num > i) 
		{
		  num = num - i;
		  num_slots -= i;
		  MarkOtherSlots(m,i,jk);
		}
	      else
		{
		  num_slots -= num;
		  MarkOtherSlots(m,num,jk);
		  num = 0;
		}
	      
	      if (num <= 0)
		continue;
	    }	  
	}
      
      while (m->next && num > 0)
	{
	  //printf("14..\n");

	  int i;

	  //printf("Calling calculate_slots..8\n");

	  i = calculate_slots( m->alloc_etime,m->next->alloc_stime);
	  
	  if (i)
	    {
	      if (num > i) 
		{
		  num = num - i;
		  num_slots -= i;
		  MarkOtherSlots(m,i,jk);
		}
	      else
		{
		  num_slots -= num;
		  MarkOtherSlots(m,num,jk);
		  num = 0;
		}

	      if (num <= 0)
		continue;
	    }
	  m = m->next;
	}   
   
      /* Double comaprison */
      if ((fabs(m->alloc_etime - map_etime) > EPSILON*fabs(m->alloc_etime)) && 
	  (m->alloc_etime < map_etime))

	//if (m->alloc_etime < map_etime)
	{
	  int i;

	  //printf("Calling calculate_slots..9\n");

	  i = calculate_slots( m->alloc_etime,map_etime);
	  
	  if (i)
	    {
	      if (num > i)
		{
		  num = num - i;
		  num_slots -= i;
		  MarkOtherSlots(m,i,jk);
		}
	      else
		{
		  num_slots -= num;
		  MarkOtherSlots(m,num,jk);
		  num = 0;
		}
	      
	      if (num <= 0)
		continue;	      
	    }
	}
    }
  return num_slots;  
}

/****************************************************************************
 *
 * void MacDocsisCMTS::MarkOtherSlots(mapptr t, u_int32_t num,int jk)
 *
 * inputs:
 *   mapptr t: the map
 *   u_int32_t num : number of slots.
 *   int jk:  indicates if mgt or CR slots (value 0 and 1 respectively)
 *
 * outputs:  none
 *
 * function:  This method  marks the cr and mgt slots in the map.
 *    jk will be used as sid for distinguishing between various slots.
 *      a 0 says its mgt ,  a 1 says it CR.
 *****************************************************************************/
void MacDocsisCMTS::MarkOtherSlots(mapptr t, u_int32_t num,int jk)
{
  mapptr m,node;
  m = mptr;
  
#ifdef TRACE_CMTS  //---------------------------------------------
  printf("MarkOtherSlots(%lf) Entered with num:%d and jk:%d\n",
	 Scheduler::instance().clock(),num,jk);
#endif //---------------------------------------------------------
  
  node = (mapptr) malloc(sizeof(struct map_list));
  
  if (!node)
    {
      printf("MacDocsisCMTS->MarkOtherSlots: Malloc failed, exiting\n");
      exit(1);
    }

  node->alloc_stime = map_stime;

  //node->alloc_etime = node->alloc_stime + (num*size_mslots)/(10*10*10*10*10*10);

  node->alloc_etime = node->alloc_stime + (num*size_mslots);  
  node->release_time = 0;
  node->deadline = 0;
  node->nslots = num;
  numIE++;
  
  if (jk ==0)
    {
      node->flag = SM_GRANT;
      node->flow_id = 1; 
      CMTSstats.total_num_mgmtslots += (num/size_ureqgrant);
    }
  else
    {	
      node->flag = CONTENTION_GRANT;
      node->flow_id = 1; 
      num_contention += num;
      CMTSstats.total_num_cslots += (num/size_ureqgrant);
    }
  
  node->next = 0;
  
  if (!mptr)
    {
      mptr = node;
      return;
    }
  
  while (m != t) 
    {
      //printf("16..\n");
      m = m->next;
    }
  
  if (m == mptr) /* Inserting after/before the 1st node */
    {
      /* 
	 Now the hole can be in either direction..
	 if the first allocation does not starts at map_stime, 
	 then the hole can be between map_stime and mptr..
      */
      
      /*Double Comparison */
      //if (m->alloc_stime != map_stime)
      if (fabs(m->alloc_stime - map_stime) > EPSILON*fabs(m->alloc_stime))
	{
	  node->next = mptr;
	  mptr = node;
	}
      else
	{
	  node->next = mptr->next;
	  mptr->next = node;
	  node->alloc_stime = m->alloc_etime;
	  node->alloc_etime = node->alloc_stime + ((num)*(size_mslots));
	}      
    }
  else
    {
      node->next = m->next;
      m->next = node;
      node->alloc_stime = m->alloc_etime;
      node->alloc_etime = node->alloc_stime + ((num)*(size_mslots));
    }  
}

/*************************************************************************
 *
 * u_int32_t MacDocsisCMTS::FillMap(int i)
 *
 * input:   int i:  
 *   if 0: periodic jobs
 *   if 1: best effort jobs
 *         or VBR data grants
 *
 * outputs:  
 *   This is messed up. 
 *   If int i = 0 always returns  0 
 *            else if int i = 1 it returns the number of 
 *            slots that WERE not allocated in the MAP.
 *            It might also be < 0, which implies 
 *            we borrowed slots from the future.
 *
 *
 *   Note: The caller doesn't use the return.  
 *  
 *  Note:  actually it returns  (num_rtslots + num_befslots)
 *         which is not correct.  But the caller
 *         corrects itself.
 *
 * function:  
 *   This is called when a MAP is to be sent.
 *   The mptr contains the start of the map.
 *
 *   A MAP element is:
 *   struct map_list
 *   {
 *     double alloc_stime;    when can the allocation be started in MAP
 *     double alloc_etime;    when can the allocation be ended in MAP
 *     double release_time;
 *     double deadline;       Request should be satisfied by this time 
 *     u_int32_t nslots;
 *     mapptr next;
 *     u_int16_t flow_id;
 *     u_int16_t  flag;       To indicate whether this is hole or not
 *                             This is the IUC
 *   };
 *
 * Notes:  There is no way for a RTPS job to fail.
 *         We don't look at MAPLOOKAHEAD.  We do the allocation
 *         no matter how far we stretch the MAP.  
 *
 * ***********************************************************************/
u_int32_t MacDocsisCMTS::FillMap(int i)
{
  mapptr curr,k;
  double alloc_time;
  u_int32_t num_slots = 0;
  double new_etime;
  int cont_slots,diff,flag; /* Number of contigous slots 
			   available towards the end */
  
#ifdef TRACE_CMTS 
  printf("FillMap:  called with param: %d:\n",i);
#endif 
  
  /* Fit the UGS and RT-VBR periodic jobs */  
  curr = mptr;
  
  if (i == 0)
    {
      /* Find the next job which needs to be allocated*/
      while (1)
	{
          //We try to allocate as many jobs as possible
	  curr = find_next(mptr);
	  
	  if (!curr) {
#ifdef TRACE_CMTS //---------------------------------------------------
            printf("FillMap:  got a 0 from find_next, return  %d\n",num_slots);
#endif //--------------------------------------------------------------
	    return num_slots;
	  }
	  else
	    {
	      /* 
		 check if job can be allocated starting at its release time, 
		 If not then try allocation it after the end of allocation of next job 
	      */
#ifdef TRACE_CMTS //---------------------------------------------------
            printf("FillMap:  call FitMap with release_time : %f\n",curr->release_time);
#endif //--------------------------------------------------------------
	      alloc_time = FitMap(curr,curr->release_time);

	      /* Double comparison */
	      double f = -1.0;
	      
	      if (fabs(alloc_time - f) > EPSILON*fabs(alloc_time))
		//if (alloc_time != -1.0)
		{
		  MakeAllocation(curr,alloc_time);		
		  ReOrder(curr);

#ifdef TRACE_CMTS //-------------------------------------------------------------
		  printf("FillMap:  Make a periodic allocation, num_slots:%d \n",
			 num_slots);
#endif //------------------------------------------------------------------------
		}
	    }
	}
      return num_slots;
    }
  else if (i == 1)
    {
      jptr jpr,jpb,tmp;
      mapptr t = 0;
      jpr = job_list[2];
      jpb = job_list[3];
      
      /* Check whether mptr exists or not */
      /* Now fill the map-list with RT-VBR data grant jobs*/
      //Only use a proportion of the available slots....
      //Loop through all realtime jobs
      while (jpr && (num_rtslots > 0) && (numIE < 255))
	{
	  t = 0;
	  
	  if (mptr)
	    t = find_best_hole(jpr->mini_slots);
	  
	  /* This function will decrease the num_rtslots*/
	  //if ((t || !mptr) && (num_rtslots - jpr->mini_slots >= 0))
	  if (t || !mptr) 
	    {
	      MakeAperiodicAlloc(jpr,t,2);
	      num_rtslots -= jpr->mini_slots;
	      size_rtqueue--;
              CMTSstats.num_ureqgrant++;

#ifdef TRACE_CMTS //---------------------------------------------------------
	      printf("FillMap:  Make a rtvbrc allocation, num_rtslots:%d \n",
		     num_rtslots);
#endif //--------------------------------------------------------------------	      
	    }	  
#ifdef TRACE_CMTS //---------------------------------------------------------
	    else {
	      printf("FillMap: 1  Either couldn't allocate more jobs (%x) or no more jobs (%x) \n",
		     t,mptr);
	    }
#endif //--------------------------------------------------------------------	      
	  jpr = jpr->next;			  	  
	}
      
      delete_joblist(2);
      jpr = job_list[2];
      
      if ((!jpr) && (num_rtslots > 0)) /* No more rt-vbr jobs, use their slots..*/
	{
	  num_befslots += num_rtslots;
	}
      
#ifdef TRACE_CMTS //-----------------------------------------------------------------------------
      printf("FillMap : Num_befslots available = %d,  num_rtslots = %d and map_lookahead:%d  \n",
	     num_befslots,num_rtslots,map_lookahead);
#endif //----------------------------------------------------------------------------------------
      
      /* Now fill the map-list with BE data grant jobs*/
      while (jpb && numIE < 255)
	{	  
#ifdef TRACE_CMTS //-----------------------------------------------------------------------------
	  printf("FillMAP(%lf) : for this job, #minislots: %d \n",
		 Scheduler::instance().clock(),jpb->mini_slots);
#endif //----------------------------------------------------------------------------------------
	  //printf("2..\n");
	  
	  if (jpb->mini_slots <= num_befslots)
	    {
#ifdef TRACE_CMTS //-------------------------------------------------
	      printf("FillMAP(%lf): jpb->mini_slots:%d\n",
		     Scheduler::instance().clock(),jpb->mini_slots);
#endif //------------------------------------------------------------
	      t = 0;
	      
	      if (mptr) 
		{
		  t = find_best_hole(jpb->mini_slots);

#ifdef TRACE_CMTS //--------------------------------------------
		  printf("FillMAP(%lf): Found best hole...\n",
			 Scheduler::instance().clock());
#endif //-------------------------------------------------------
		}
	      //if ((t || !mptr) && (num_befslots - jpb->mini_slots >= 0))
	      if (t || !mptr)
		{
		  MakeAperiodicAlloc(jpb,t,3);
		  num_befslots -= jpb->mini_slots;
		  size_bfqueue--;
		  CMTSstats.num_creqgrant++;
#ifdef TRACE_CMTS //------------------------------------------------------------------
		  printf("FillMap : Make a best effort  allocation, remaining num_befslots:%d\n",
			 num_befslots);
#endif //-----------------------------------------------------------------------------
		}
#ifdef TRACE_CMTS //---------------------------------------------------------
  	       else {
	        printf("FillMap: 2  Either couldn't allocate more jobs (%x) or no more jobs (%x) \n",
		     t,mptr);
	       }
#endif //--------------------------------------------------------------------	      
	      //jpb = jpb->next;			      
	  }
	  //Remember, the map_lookahead is how for beyond the current slot we are allowed to go
	  else if ((jpb->mini_slots > num_befslots) && (jpb->mini_slots <= (num_befslots + map_lookahead)))
//main path- no call to fill_best_hole since the job can be added in the MAP
	    {
	      
	      t = 0;
	      cont_slots = NumContSlots(&t); /* NumContSlots will find the number of 
						contigous slots at the end of the map */	      
	      diff = jpb->mini_slots - cont_slots;	      
	      new_etime = map_etime + diff*size_mslots;
	      
#ifdef TRACE_CMTS //-----------------------------------------------------
	      printf("FillMAP(%lf): Have to stretch MAP by %d slots, cont_slots=%d\n",
		     Scheduler::instance().clock(),diff, cont_slots);
#endif //----------------------------------------------------------------
	      
	      // We need to strech the map by diff slots 
	      // but only if it does NOT interfere with periodic
	      // jobs in the near future.
	      flag = ChkQoSJobs(map_etime,new_etime);
	      
	      if (flag) /* any Qos job in this region */
		{
		  /* Deny the request
		     Delete this request so that no data grant pending is sent */
		  if (jpb->retry_count == MAX_RETRY)
		    {
		      jpb->flag = 1;
		      size_bfqueue--;
		      CMTSstats.num_creqdenied++;
		    }
		  else
		    jpb->retry_count++;
		}
	      else 
		{
		  map_etime = new_etime;		
		  MakeAperiodicAlloc(jpb,t,3);  /* Do the allocation */
		  num_befslots -=  cont_slots;
		  map_lookahead -= diff;
		  //printf("num_befslots = %d map_lookahead = %d\n",num_befslots,map_lookahead);
		  size_bfqueue--;
		  CMTSstats.num_creqgrant++;		  
		  next_map += diff;  /* Adjust the next map send time */

#ifdef TRACE_CMTS //-------------------------------------------------------------------------
		  printf("FillMap : Make a be allocation WITH LOOKAHEAD, num_befslots:%d\n",
			 num_befslots);
#endif //------------------------------------------------------------------------------------		  
		}
	      //jpb = jpb->next;	      
	    }
	  //If we get here we know the requested number of slots will not fit in the
	  //current MAP plus the map_lookahead.  So we need to allocate a partial grant
	  else if (num_befslots + map_lookahead > 0)/* Allocate partial grants */
	    {
// this happens if we have already stretched the MAP.  So map_lookahead will be <255
// And we don't allocate a partial grant ... ??
	      t = 0;
	      cont_slots = NumContSlots(&t); /* NumContSlots will find the number of 
						contigous slots at the end of the map */	      
	      
              CMTSstats.num_preqgrant++;		  
#ifdef TRACE_CMTS //-----------------------------------------------------
	      printf("FillMAP(%lf): Have to make a partial grant since number requested(%d) > num_befslots+map_lookahead(%d)\n",
		     Scheduler::instance().clock(),jpb->mini_slots,num_befslots+map_lookahead);
	      printf("FillMAP(%lf):  .... cont_slots=%d, grant will be %d \n", 
		     Scheduler::instance().clock(),cont_slots,num_befslots+map_lookahead);
#endif //----------------------------------------------------------------
	      
	      if (mptr) {
#ifdef TRACE_CMTS //-------------------------------------------------------------
                   printf("FillMap : num_befslots+map_lookahead > 0...(num_befslots=%d, map_lookahead=%d\n",
				    num_befslots,map_lookahead);
#endif //------------------------------------------------------------------------
		t = find_best_hole(num_befslots+map_lookahead);
#ifdef TRACE_CMTS //-------------------------------------------------------------
                   printf("FillMap : return from find_best_hole with t= %d\n",t);
#endif //------------------------------------------------------------------------
		 }
	      
	      /* This function will decrease the num_rtslots*/
	      //if ((t || !mptr) && (num_befslots - jpb->mini_slots >= 0))	      
	      if (t || !mptr)
		{
		  jpb->mini_slots = num_befslots + map_lookahead;
	          diff = jpb->mini_slots - cont_slots;	      
	          if (diff < 0)
		      diff = 0;
	          new_etime = map_etime + diff*size_mslots;
		  map_etime = new_etime;		

		  MakeAperiodicAlloc(jpb,t,3);
		  num_befslots = 0;
		  map_lookahead = 0;
		  size_bfqueue--;

		  CMTSstats.num_creqgrant++;
		  CMTSstats.num_preqgrant++;
		  next_map += diff;  /* Adjust the next map send time */

#ifdef TRACE_CMTS //-------------------------------------------------------------
		  printf("FillMap : Make a best effort  partial grant, allocated slots:%d\n",
			 jpb->mini_slots);
	          printf("FillMAP(%lf): Have to stretch MAP by %d slots, cont_slots=%d\n",
		     Scheduler::instance().clock(),diff, cont_slots);
	          printf("FillMAP(%lf): next_map:%lf, map_etime:%lf \n",
		     Scheduler::instance().clock(),next_map,map_etime);
#endif //------------------------------------------------------------------------
		}
#ifdef TRACE_CMTS //---------------------------------------------------------
	    else {
	      printf("FillMap: 3  Either couldn't allocate more jobs (%x) or no more jobs (%x) \n",
		     t,mptr);
	    }
#endif //--------------------------------------------------------------------	      
	    }	
	  jpb = jpb->next;		
	}
      
      delete_joblist(3);            
      jpr = job_list[2];
      
      /* Now allocate VBR jobs with anything left from the befslots*/
      while (jpr && (num_befslots > 0) && (numIE < 255))
	{
	  t = 0;
	  
	  if (mptr) {
#ifdef TRACE_CMTS //---------------------------------------------------------
     printf("FillMap : call find_best_hole with this many slots %d\n", jpr->mini_slots);
#endif //--------------------------------------------------------------------
	    t = find_best_hole(jpr->mini_slots);
	  }
	  
	  /* This function will decrease the num_rtlsots */
	  //if ((t || !mptr) && (num_befslots - jpr->mini_slots >= 0))
	  if (t || !mptr) 
	    {
	      MakeAperiodicAlloc(jpr,t,2);
	      num_befslots -= jpr->mini_slots;
	      size_rtqueue--;
              CMTSstats.num_ureqgrant++;

#ifdef TRACE_CMTS //---------------------------------------------------------
	      printf("FillMap : Make a ?? be allocation, num_befslots:%d\n",
		     num_befslots);
#endif //--------------------------------------------------------------------
	    }	  
#ifdef TRACE_CMTS //---------------------------------------------------------
	    else {
	      printf("FillMap: 4 Either couldn't allocate more jobs (%x) or no more jobs (%x) \n",
		     t,mptr);
	    }
#endif //--------------------------------------------------------------------	      
	  jpr = jpr->next;			  
	}      
      delete_joblist(2);

#ifdef TRACE_CMTS //---------------------------------------------------------
     printf("FillMap : return this many UNUSED slots:%d\n", (num_rtslots + num_befslots));
     printf("FillMap :    creqgrants:%d, ureqgrants:%d, preqgrants:%d\n", 
              CMTSstats.num_creqgrant,
              CMTSstats.num_ureqgrant,
              CMTSstats.num_preqgrant);
#endif //--------------------------------------------------------------------
      
      return (num_rtslots + num_befslots); /* Return the unused slots. 
					      A negative return value will 
					      indicate that those many slots in 
					      future have already been promised.. */
      //return (num_befslots);
    } 
}

/*
  void MacDocsisCMTS::AllocPartialGrants()
  {
  jptr jb;
  int num;
  num = num_rtslots + num_befslots;
  jpb = job_list[3];
  while (num > 0)
  {
  }
  }
*/

/*****************************************************************************

*****************************************************************************/
int MacDocsisCMTS::NumContSlots(mapptr *t)
{
  mapptr m,prev;
  m = mptr;
  
  if (!mptr)
    {
      //printf("Returning %d\n",num_befslots);

      return num_befslots;/* All the slots are available */
    }
  
  while (m )
    {
      //printf("18..\n");
      prev = m;
      m = m->next;
    }
  
  /* Double comparison */
  if (fabs(prev->alloc_etime - map_etime) <= EPSILON*fabs(prev->alloc_etime))
    //if (prev->alloc_etime == map_etime)
    {
      *t = prev;
      //printf("Returning 0\n");
      return 0;
    }
  else
    {
      int i,nslots;
      *t = prev;

      //printf("Calling calculate_slots..10\n");
      i = calculate_slots(prev->alloc_etime,map_etime);      
      nslots = ((Conf_Table_.mapparam.num_sm_slots + Conf_Table_.mapparam.num_contention_slots) * (size_ureqgrant));     
      //if (i == nslots) 
      i -= nslots;
      //printf("Returning %d\n",i);
      return i;
    }
}

/*****************************************************************************

*****************************************************************************/
int MacDocsisCMTS::ChkQoSJobs(double st, double et)
{
  int i;
  jptr jp;
  
  i = 0;
  
  for (i = 0; i < 2; i++)
    {
      jp = job_list[i];
      
      while (jp)
	{
	  //printf("19..\n");

	  /*Double comparison*/
	  /*
	    if ((jp->release_time > st) || 
	    (fabs(jp->release_time -st) <= EPSILON*fabs(jp->release_time)) || 
	    (jp->deadline < et) || 
	    (fabs(jp->deadline - et) <= EPSILON*fabs(jp->deadline)))
	  */

	  if ((jp->deadline < et) || 
	      (fabs(jp->deadline - et) <= EPSILON*fabs(jp->deadline)))
	    //if ((jp->release_time >= st) || (jp->deadline <= et))
	    return 1;
	  
	  jp = jp->next;
	}
    }
  //printf("returning from ChkQoSJobs\n");
  return 0;
}

/*****************************************************************************

*****************************************************************************/
void MacDocsisCMTS::MakeAperiodicAlloc(jptr j, mapptr t,int qnum)
{
  mapptr m,node;
  m = mptr;
  
  node = (mapptr) malloc(sizeof(struct map_list));
  
  if (!node)
    {
      printf("MacDocsisCMTS->MakeAperiodicAlloc: Malloc failed, exiting\n");
      exit(1);
    }
  
  node->alloc_stime = map_stime; 
 
  //node->alloc_etime = node->alloc_stime + ((j->mini_slots)*(size_mslots))/(10*10*10*10*10*10);

  node->alloc_etime = node->alloc_stime + ((j->mini_slots)*(size_mslots));
  node->release_time = j->release_time;
  node->deadline = j->deadline;
  node->nslots = j->mini_slots;
  node->flow_id = j->flow_id;
  node->flag = DATA_GRANT;
  node->next = 0;
  numIE++;
  
  /* Update ACK time properly */
  if (AckTime < j->release_time)
    AckTime = j->release_time;
  
  num_dgrant += node->nslots;
      
  if (!mptr)
    {
      mptr = node;
      j->flag = 1;/* Mark the node for deletion */
      return;
    }
  
  while (m != t) 
    {
      //printf("20..\n");
      m = m->next;
    }
  
  if (m == mptr) /* Inserting after/before the 1st node */
    {
      /* 
	 Now the hole can be in either direction..if the first allocation does 
	 not starts at map_stime, then the hole can be between map_stime and mptr..
      */
      
      /* Double comparison */
      if (fabs(m->alloc_stime - map_stime) > EPSILON*fabs(m->alloc_stime))
	//if (m->alloc_stime != map_stime)
	{
	  node->next = mptr;
	  mptr = node;
	}
      else
	{
	  node->next = mptr->next;
	  mptr->next = node;
	  node->alloc_stime = m->alloc_etime;

	  //node->alloc_etime = node->alloc_stime + ((j->mini_slots)*(size_mslots))/(10*10*10*10*10*10);

	  node->alloc_etime = node->alloc_stime + ((j->mini_slots)*(size_mslots));
	}     
    }
  else
    {
      node->next = m->next;
      m->next = node;
      node->alloc_stime = m->alloc_etime;
      node->alloc_etime = node->alloc_stime + ((j->mini_slots)*(size_mslots));
    }
  j->flag = 1;  
}

/*************************************************************************
* function: mapptr MacDocsisCMTS::find_best_hole(u_int32_t n)
* 
* explanation: This function returns a pointer to the map node where the best hole begins
*              The best hole is the largest of all the holes associated with the
*              jobs currently in the map list.
*
* inputs:  
* u_int32_t n : the number of slots that we need to allocate
*
* outputs:
*
* mapptr t : returns a ptr to a MAP.
*            Returns a NULL if couldn't find  a hole.  The
*            caller will have to hang on to the job
*            Usually this occurs because we've already stretched the MAP.
*************************************************************************/
mapptr MacDocsisCMTS::find_best_hole(u_int32_t n)
{
  mapptr m,bestp = NULL;
  u_int32_t j,best = 999999;

#ifdef TRACE_CMTS //--------------------------------------------------
  printf("find_best_hole(%lf): Entered with n : %d\n",
	 Scheduler::instance().clock(),n);
#endif //-------------------------------------------------------------
  
  m = mptr;
  
  /* Double comparison */
  if (fabs(m->alloc_stime - map_stime) > EPSILON*fabs(m->alloc_stime))
    //if (m->alloc_stime != map_stime)
    {
      //printf("Calling calculate_slots..11\n");
      //Compute the number of slots from the start of the map
      //until the starting time of this job
      j = calculate_slots(map_stime,m->alloc_stime);
      
      if (j > n)
	{
	  bestp = m;
	  best = j - n;
	}
    }
  
  while (m->next)
    {
      //printf("21..\n");
      if (m->next)
	{
	  //printf("Calling calculate_slots..12\n");
	  j = calculate_slots(m->alloc_etime,m->next->alloc_stime);
	  
	  if ((j > n) && ((j-n) < best))
	    {
	      bestp = m;
	      best = j - n;
	    }
	}
      m = m->next;
    }
  /* Double comparison */
  
  if (fabs(m->alloc_etime - map_etime) > EPSILON*fabs(m->alloc_etime))
    //if (m->alloc_etime != map_etime)
    {
      //printf("Calling calculate_slots..13\n");
      //printf("m->alloc_etime = %lf map_etime = %lf\n",m->alloc_etime,map_etime);
      j = calculate_slots(m->alloc_etime,map_etime);

      //printf("find_best_hole: Return with j of  %d, n=%d;  best=%d\n",j,n,best);
      
      if ((j > n) && ((j-n) < best))
	{
	  bestp = m;
	  best = j - n;
	}
    }
#ifdef TRACE_CMTS //-----------------------------------------------
  printf("find_best_hole: Return with j of  %d, n=%d;  best=%d\n",j,n,best);
  if (bestp != NULL)
      printf("find_best_hole: Return node with start time of %lf\n",
	 bestp->alloc_stime);
  else
    printf("find_best_hole: bestp is NULL ??? \n");
#endif //----------------------------------------------------------
  
  return bestp;
}

/************************************************************************
 *
 * function:  mapptr  MacDocsisCMTS::find_next(mapptr m)
 *
 *     This method
 *
 * input:   mapptr m: 
 *
 * outputs:  
 *   returns the next job in the map list.
 *
 *************************************************************************/
mapptr  MacDocsisCMTS::find_next(mapptr m)
{
  mapptr k;
  k = m;
  
  if (!k) 
    {    
#ifdef TRACE_CMTS 
      printf("find_next: Returning a NULL  \n");
#endif
      
      return 0;
    }
  else
    {
#ifdef TRACE_CMTS 
      printf("find_next: print jobs  \n");
      print_job_list(0);
#endif
      /* Double comparison */
      double diff;
      double f = -1.0;
      
      //while (k && (k->alloc_stime != -1.0)) 
      while (k && (fabs(k->alloc_stime - f) > EPSILON*fabs(k->alloc_stime)))
	{

#ifdef TRACE_CMTS //----------------------------------------------------
	  printf("find_next: Passing over job with start time %lf \n",
		 k->alloc_stime);
#endif //---------------------------------------------------------------
	  
	  k = k->next;
	}
      
#ifdef TRACE_CMTS //---------------------------------------------
      if (!k) 
        printf("find_next: DID NOT FIND A JOB TO RETURN \n");
      else
        printf("find_next: Returning job with start time %lf \n",
	     k->alloc_stime);
#endif //--------------------------------------------------------
      
      return k;
    }
}

/************************************************************************
 *
 * function:  double MacDocsisCMTS::FitMap(mapptr j, double t)
 *
 * Explanation:
 *  Try to fit job 'j' starting at 't'.
 *  Keep on trying till you reach the end of the MAP.
 *  This method 
 *
 * input:   mapptr  j : job to be inserted in the map
 *          double t : release time 
 *
 * outputs:  
 *   returns the starting time in the map.
 *
 *************************************************************************/
double MacDocsisCMTS::FitMap(mapptr j, double t)
{
  mapptr k;
  double r,alloc_stime,alloc_etime,duration;
  
  alloc_stime = t;
  //r = t + (j->nslots)*size_mslots/(10*10*10*10*10*10);
  r = t + (j->nslots)*size_mslots;
  alloc_etime = r;
  duration = r - t;
  k = mptr;
  
  while (1)
    {
      //printf("23..\n");
      k = mptr;
      /* Double comparison */
      
      //if (t == map_etime)
      if (fabs(t - map_etime) <= EPSILON*fabs(t))
	{
	  /* let this function decide the fate of this job */
	  jobdrop(j);
	  break;
	}	
      else
	{
          //Go through each job that's in the MAP LIST
	  while (k)
	    {
	      //printf("24..\n");
	      /* Someone starting <= j and 
		 finishing < or = or > j overlaps with me */
	      /* Double comparison */
	      double diff;
	      double f = -1.0;
	      
	      if (fabs(k->alloc_stime - f) <= EPSILON*fabs(k->alloc_stime))		
		//if (k->alloc_stime == -1.0)
		{
		  k = k->next;
		  continue;
		}
	      
	      /* Double comparison */
	      if (((k->alloc_stime < alloc_stime) || 
		   (fabs(k->alloc_stime-alloc_stime) <= EPSILON*fabs(k->alloc_stime))) && 
		  (fabs(k->alloc_etime - alloc_stime) > EPSILON*fabs(k->alloc_etime)) &&
		  (k->alloc_etime > alloc_stime))
		//if ((k->alloc_stime <= alloc_stime) && (k->alloc_etime > alloc_stime))
		{	
		  alloc_stime = k->alloc_etime;
		  alloc_etime = alloc_stime + duration;
		  break;
		}
	      else
		/* Double comparison */
		if ((fabs(k->alloc_stime - alloc_stime) > EPSILON*fabs(k->alloc_stime)) && 
		    (k->alloc_stime > alloc_stime) && 
		    (fabs(k->alloc_stime - alloc_etime) > EPSILON*fabs(k->alloc_stime)) && 
		    (k->alloc_stime < alloc_etime)) 
		  //if ((k->alloc_stime > alloc_stime) && (k->alloc_stime < alloc_etime)) 
		  {
		    /* Have a hole between my release time & this guys start time.. */
		    if ((k->alloc_stime - alloc_stime) >= duration)
		      {
#ifdef TRACE_CMTS
                        printf("CMTS:fitMAP(%lf): Allocating but created a hole %d\n",
	                  Scheduler::instance().clock(),(k->alloc_stime - alloc_stime));
#endif

			return alloc_stime;
		      }
		    else
		      {
			alloc_stime = k->alloc_etime;
			alloc_etime = alloc_stime + duration;
			break;			
		      }		    
		  }
	      k = k->next;
	    }
	}

      if (!k)	
	break;
    }
  return alloc_stime;
}

/*****************************************************************************

*****************************************************************************/
void MacDocsisCMTS::MakeAllocation(mapptr j, double t)
{
  j->alloc_stime = t;
  //j->alloc_etime = t + (size_mslots * j->nslots)/(10*10*10*10*10*10);
  j->alloc_etime = t + (size_mslots * j->nslots);
  UpdateJitter(j->flow_id,j->alloc_stime);
  return ;
}

/*****************************************************************************
* Routine: void MacDocsisCMTS::UpdateJitter(u_int16_t flow_id, double t)
*
* Explanation: 
*   Called when the CMTS has allocated BW for this job in the next map.
*
* Inputs:
*   flow_id: the flow or job to look at
*   t : the allocated time (j->alloc_stime) of the job
*
* Outputs:
*
*
*****************************************************************************/
void MacDocsisCMTS::UpdateJitter(u_int16_t flow_id, double t)
{
  jptr jp;
  jp = job_list[0];
  
#ifdef TRACE_CMTS 
  printf("CMTS:UpdateJitter(%lf): Entered with flow_id : %d\n",
	 Scheduler::instance().clock(),flow_id);
#endif 
  while (jp)
    {
      if (jp->flow_id == flow_id)
	{
	  jp->ugsjitter += (t - (jp->release_time - jp->period));
          jp->jitterSamples++;
#ifdef TRACE_CMTS 
          printf("CMTS:UpdateJitter(flow:%d)(%lf):  updated ugsjitter : %f (t:%lf,release_time:%lf, error:%lf)\n",
           jp->flow_id,
	   Scheduler::instance().clock(),jp->ugsjitter,t,
	   jp->release_time - jp->period,(t - (jp->release_time - jp->period)));
#endif 
	}
      jp = jp->next;
    }
}

/*****************************************************************************

*****************************************************************************/
void MacDocsisCMTS::ReOrder(mapptr k)
{
  mapptr curr,kpr,prev;
  
  curr = mptr;
  prev = curr;  
  kpr = find_prv(k);
  
  while (curr)
    {
      //printf("25..\n");
      /* Double comparison */
      double f = -1.0;
      
      if ((curr != k) && (fabs(curr->alloc_stime-f) > EPSILON*fabs(curr->alloc_stime)) && 
	  (fabs(curr->alloc_stime - k->alloc_stime) > EPSILON*fabs(curr->alloc_stime)) && 
	  (curr->alloc_stime > k->alloc_stime))
	//if ((curr != k) && (curr->alloc_stime != -1) && (curr->alloc_stime > k->alloc_stime))
	break;
      
      prev = curr;
      curr = curr->next;
    }
  
  if (prev == curr)
    {
      if (kpr)
	kpr->next = k->next;
      
      k->next = mptr;
      mptr = k;
    }	
  else if ((prev != k) && (curr !=0)) /* Insertion between 2 nodes */
    { 
      if (kpr)
	kpr->next = k->next;
      
      prev->next = k;
      k->next = curr;      
    }  
}

/*****************************************************************************

*****************************************************************************/
mapptr MacDocsisCMTS::find_prv(mapptr k)
{
  mapptr p;
  
  p = mptr;
  
  if (k == mptr)
    return 0;
  
  while ( p  && (p->next != k)) 
    {
      //printf("26..\n");
      p=p->next;
    }
  return p;
}

/*****************************************************************************

*****************************************************************************/
void MacDocsisCMTS::delete_joblist(int i)
{
  jptr tmp,prev;
  
  tmp = job_list[i];
  prev = tmp;
  
  if (!tmp)
    {
      return;
    }
  
  tmp = tmp->next;
  
  while (tmp)
    {
      //printf("27..\n");
      if (tmp->flag == 1)
	{
	  prev->next = tmp->next;
	  free(tmp);
	  tmp = prev;
	}      
      prev = tmp;
      tmp = tmp->next;      
    }  
  tmp = job_list[i];
  
  if (tmp && (tmp->flag == 1))
    {
      job_list[i] = job_list[i]->next;
      free(tmp);
    }  	
}

/**********************************************************************
 * Routine:
 *  int MacDocsisCMTS::insert_mapjob(int num, jptr jp, u_int32_t *num_slots, int queue)
 *
 * function:  
 *   This inserts the job (jp) into the mptr (the map list)
 *
 * input:  
 *  int num: the number of jobs  (always 1 ???)
 *  jptr jp :      The job to add
 *  u_int32_t *num_slots : ptr variable that indicates the number of slots in the MAP
 *                        This routine then updates this with the number of slots left
 *                        after the job has been added to the map list.
 *                        to an output variable to contain the number of slots
 *  int queue: : the job index (0,1,2,3)
 *
 * outputs:  
 *   returns a status of 1 success and 0 if not enough slots available
 *
 **********************************************************************/
int MacDocsisCMTS::insert_mapjob(int num, jptr jp, u_int32_t *num_slots, int queue)
{
  mapptr node,prev,curr;
  curr = mptr;
  prev = curr;

#ifdef TRACE_CMTS 
  if (queue == 0)
    printf("CMTS:insert_mapjob(%lf): Entered num: %d, *num_slots:%d,  queue:%d, set uic to %d\n",
	 Scheduler::instance().clock(),num,*num_slots,queue,DATA_GRANT);
  else
    printf("CMTS:insert_mapjob(%lf): Entered num: %d, *num_slots:%d,  queue:%d, set uic to %d\n",
	 Scheduler::instance().clock(),num,*num_slots,queue,UREQ_GRANT);
#endif 
  
  if (*num_slots > jp->mini_slots)
    //add the job to the map list
    {
      node = (mapptr) malloc(sizeof(struct map_list));
      
      if (!node)
	{
	  printf("MacDocsisCMTS->insert_mapjob: Malloc failed, exiting\n");
	  exit(1);
	}      
      node->alloc_stime = -1.0;
      node->alloc_etime = -1.0;
      
      //if (jp->release_time < map_stime)
      /* Double comparison */      
      double diff = fabs(jp->release_time - map_stime);

      if ((diff <= EPSILON*fabs(jp->release_time))|| (jp->release_time < map_stime))
	node->release_time = map_stime;
      else
	node->release_time = jp->release_time; 
      
      node->deadline = node->release_time + jp->period;

      //node->deadline = jp->deadline;

      node->nslots = jp->mini_slots;
      node->flow_id = jp->flow_id;
      node->next = 0;
      
      //If this is the job_list[0], the UGS queue,  
      if (queue == 0)
	{
	  num_dgrant += node->nslots;
	  node->flag = DATA_GRANT;
	}
      //else this is the rtVBR or BE ???
      //in which case the slots are  for requests
      else
	{
	  num_req += node->nslots;
	  node->flag = UREQ_GRANT;
	}
      *num_slots = *num_slots - jp->mini_slots;      
    }
  else
    {
      //so not enough slots in the map
      printf("CMTS:insert_mapjob(%lf): Not enough slots left in the MAP: num:%d, *num_slots::%d\n",
             Scheduler::instance().clock(),num,*num_slots);
      return 0;
    }
  
  //Now add the allocation to the MAP mptr
  if (!mptr)
    mptr = node;
  else
    {
      //Loops through the list of nodes and inserts this 
      //new node sorted by deadline. So the head of the
      //list has the earliest deadline.
      while ((curr) && (curr->deadline < node->deadline))
	{
	  //printf("28..\n");
	  prev = curr;
	  curr = curr->next;
	}
      
      if (curr == prev)
	{
	  mptr = node;
	  node->next = curr;
	}
      else
	{
	  prev->next = node;
	  node->next = curr;
	}
    }  
  return 1;
}

/*****************************************************************************

*****************************************************************************/
void MacDocsisCMTS::PhUnsupress(Packet* p, int cindex,int findex)
{
  struct hdr_cmn *ch = HDR_CMN(p);
  
  switch(CmRecord[cindex].u_rec[findex].PHS_profile) 
    {
    case SUPRESS_ALL:
      ch->size() += SUPRESS_ALL_SIZE - 2 ;
      break;
    
    case SUPRESS_TCPIP:
      ch->size() += SUPRESS_TCPIP_SIZE - 2;
      break;
    
    case SUPRESS_UDPIP:
      ch->size() += SUPRESS_UDPIP_SIZE - 2;
      break;
    
    case SUPRESS_MAC:
      ch->size() += SUPRESS_MAC_SIZE - 2;
      break;
    
    case NO_SUPRESSION:
      break;
    
    default:
      printf("MacDocsisCMTS->PhUnsupress:Unknown PHS type..\n");
      break;
    }
  return;
}

/*************************************************************************
 * routine: void MacDocsisCMTS::ApplyPhs(Packet* p, int cindex,int findex)
 *
 * Function:  This method applies DS physical layer OH to the packet.
 *
 * Input
 *  Packet* p : packet to that is to be sent downstream.
 *  int cindex
 *  int findex
 *  
*************************************************************************/
void MacDocsisCMTS::ApplyPhs(Packet* p, int cindex,int findex)
{
  struct hdr_cmn *ch = HDR_CMN(p);
  int rc_p=0;

#ifdef TRACE_CMTS 
  printf("CMTS:ApplyPhs(%lf): Entered, Cindex = %d findex= %d\n",
	 Scheduler::instance().clock(),cindex,findex);
#endif
  
  switch(CmRecord[cindex].d_rec[findex].PHS_profile) 
    {
    case SUPRESS_ALL:
      ch->size() -= (SUPRESS_ALL_SIZE - 2) ;
      break;
    
    case SUPRESS_TCPIP:
      ch->size() -= (SUPRESS_TCPIP_SIZE - 2);
      break;
    
    case SUPRESS_UDPIP:
      ch->size() -= (SUPRESS_UDPIP_SIZE - 2);
      break;
    
    case SUPRESS_MAC:
      ch->size() -= (SUPRESS_MAC_SIZE - 2);
      break;
    
    case NO_SUPRESSION:
      break;
    
    default:
      printf("MacDocsisCMTS->ApplyPhs:Unknown PHS type:%d\n",CmRecord[cindex].d_rec[findex].PHS_profile);
      break;
    }
  return;
}

/*****************************************************************************
 * Routine: int MacDocsisCMTS::classify(Packet* p,char dir,int* find)
 *
 * Explanation:
 *   Examines the packet and matches it to a flow in the CmRecord.
 *   The dir param determines if the upstream or downstream flow
 *   are looked at.
 *
 * Input:
 *    Packet *p : the packet
 *    char dir:   DOWNSTREAM or UPSTREAM
 *    int *find:  This ptr variable is set with the index of the flow
 *                 table entry.
 *
 * Output: 
 *   returns the index of the matching cm's CmRecord table entry.
 *
*****************************************************************************/
int MacDocsisCMTS::classify(Packet* p,char dir,int* find)
{
  int i,cm;
  struct hdr_mac *mh = HDR_MAC(p);
  
  if (dir == DOWNSTREAM)
    {
      cm = find_cm(mh->macDA());
#ifdef TRACE_CMTS 
  printf("CMTS:classify(%lf): Entered, packet going DOWNSTREAM, macDA:%d, find cm:%d \n",
	 Scheduler::instance().clock(),mh->macDA(),cm);
#endif
      
      if (cm <0 ){
#ifdef TRACE_CMTS 
         printf("CMTS:classify(%lf): DS ERROR: find_cm returned error \n",Scheduler::instance().clock());
#endif 
      }

      for (i = 0; i < CmRecord[cm].SizeDnFlTable; i++)
	{
	  if (match(p, CmRecord[cm].d_rec[i].classifier))
	    {
	      *find = i;
	      return cm;
	    }	  
	}
      *find = CmRecord[cm].default_downstream_index_;
      return cm;
    }
  else if (dir == UPSTREAM)
    {
      cm = find_cm(mh->macSA());
#ifdef TRACE_CMTS 
  printf("CMTS:classify(%lf): Entered, packet going UPSTREAM, macSA:%d, find_cm:%d \n",
	 Scheduler::instance().clock(),mh->macSA(),cm);
#endif
      if (cm <0 ){
#ifdef TRACE_CMTS 
         printf("CMTS:classify(%lf): US ERROR: find_cm returned error  (cm:%d)\n",Scheduler::instance().clock(),cm);
#endif 
	 *find = 65;
	 return cm;
      }
      
      for (i = 0; i < CmRecord[cm].SizeUpFlTable; i++)
	{
	  if (match(p, CmRecord[cm].u_rec[i].classifier))
	    {
	      *find = i;
	      return cm;
	    }
	}      
      *find = CmRecord[cm].default_upstream_index_;
#ifdef TRACE_CMTS 
         printf("CMTS:classify(%lf):    return  cm:%d,   flow index:%d \n",Scheduler::instance().clock(),cm,*find);
#endif 
      return cm;
    }  
  else
    {
      printf("MacDocsisCM->ClassifyServiceFlow: Error, Unknown direction, Exiting\n");
      exit(1);
    }
}

/*****************************************************************************
 * Routine: int MacDocsisCMTS::classify(Packet* p,char dir,int* find)
 *
 * Explanation:
 *   Examines the packet and matches it to a flow in the CmRecord.
 *   The dir param determines if the upstream or downstream flow
 *   are looked at.
 *
 * Input:
 *    Packet *p : the packet
 *    char dir:   DOWNSTREAM or UPSTREAM
 *    int *find:  This ptr variable is set with the index of the flow
 *                 table entry.
 *
 * Output: 
 *   returns the index of the matching cm's CmRecord table entry.
 *
*****************************************************************************/
int MacDocsisCMTS::findFlow(int macaddr, int32_t src_ip, int32_t dst_ip, packet_t pkt_type, char dir, int*find)
{

  int i,cm;

#ifdef TRACE_CMTS 
  printf("CMTS:findFlow(%lf): Entered, macaddr:%d, src_ip:%d, dst_ip:%d, pkt_type:%d, dir:%d\n",
	 Scheduler::instance().clock(),macaddr,src_ip,dst_ip,pkt_type,dir);
#endif
  
  if (dir == DOWNSTREAM)
  {
      cm = find_cm(macaddr);
#ifdef TRACE_CMTS 
  printf("CMTS:findFlow(%lf): Entered, packet going DOWNSTREAM, macDA:%d, find cm:%d \n",
	 Scheduler::instance().clock(),macaddr,cm);
#endif
      
      if (cm <0 ){
//#ifdef TRACE_CMTS 
         printf("CMTS:findFlow(%lf): HARD ERROR   find_cm returned error \n",Scheduler::instance().clock());
	     *find = 65;
	     return cm;
//#endif
      }

      for (i = 0; i < CmRecord[cm].SizeDnFlTable; i++)
	  {
	    if (match(src_ip,dst_ip,pkt_type, CmRecord[cm].d_rec[i].classifier))
	    {
#ifdef TRACE_CMTS 
           printf("CMTS:findFlow(%lf):  FOund MATCH cm record index:%d and flow_id:%d\n",
	 Scheduler::instance().clock(),cm, CmRecord[cm].d_rec[i].flow_id);
#endif
	      *find = i;
	      return cm;
	    }	  
	  }
      *find = CmRecord[cm].default_downstream_index_;
#ifdef TRACE_CMTS 
      printf("CMTS:findFlow(%lf):  Did NOT Find MATCH,return cm:%d, default DS find:%d, flow_id:%d\n",
      	 Scheduler::instance().clock(),cm,*find,CmRecord[cm].d_rec[*find].flow_id);
#endif
      return cm;
  }
  else if (dir == UPSTREAM)
  {
      cm = find_cm(macaddr);
#ifdef TRACE_CMTS 
  printf("CMTS:findFlow(%lf): Entered, packet going UPSTREAM, macSA:%d, find_cm:%d \n",
	 Scheduler::instance().clock(),macaddr,cm);
#endif
      if (cm <0 ){
//#ifdef TRACE_CMTS 
         printf("CMTS:findFlow(%lf): US ERROR: find_cm returned error  (cm:%d)\n",Scheduler::instance().clock(),cm);
//#endif 
	 *find = 65;
	 return cm;
      }
      
      for (i = 0; i < CmRecord[cm].SizeUpFlTable; i++)
	  {
	    if (match(src_ip,dst_ip,pkt_type, CmRecord[cm].u_rec[i].classifier))
	    {
#ifdef TRACE_CMTS 
           printf("CMTS:findFlow(%lf):  FOund MATCH cm record index:%d and flow_id:%d\n",
	 Scheduler::instance().clock(),cm, CmRecord[cm].u_rec[i].flow_id);
#endif
	      *find = i;
	      return cm;
	    }
	  }      
      *find = CmRecord[cm].default_upstream_index_;
#ifdef TRACE_CMTS 
      printf("CMTS:findFlow(%lf):  Did NOT Find MATCH,return cm:%d, default US find:%d, flow_id:%d\n",
      	 Scheduler::instance().clock(),cm,*find,CmRecord[cm].u_rec[*find].flow_id);
#endif 
      return cm;
  }  
  else
  {
      printf("MacDocsisCMTS:findFlow: HARD Error, Unknown direction, Exiting\n");
      exit(1);
  }
}

/*****************************************************************************
 * Routine:
 * 
 * Explanation: Given the mac address parameter, this routine
 *    searches the CM table to return the CmRecord table index of
 *    the correct CM.

*****************************************************************************/
int MacDocsisCMTS::find_cm(int t)
{
  int i;
#ifdef TRACE_CMTS 
  printf("CMTS:find_cm(%lf): try to match this  mac address:  %x (sizeCmTable:%d)\n",Scheduler::instance().clock(),t,SizeCmTable);
#endif 
  
  for (i = 0 ; i < SizeCmTable; i++)
    {
#ifdef TRACE_CMTS 
  printf("      i:%d, mac address:  %x\n",i,CmRecord[i].cm_macaddr);
#endif 
      if (CmRecord[i].cm_macaddr == t)
	return i;
    }	
#ifdef TRACE_CMTS 
  printf("CMTS:find_cm(%lf): FAILED   find_cm returns error \n",Scheduler::instance().clock());
#endif 
  return -1;
}

/*****************************************************************************

*****************************************************************************/
void MacDocsisCMTS::tune_parameters()
{
}

/*****************************************************************************

*****************************************************************************/
double MacDocsisCMTS::determine_deadline(char sclass,int cindex,int findex)
{
  if (sclass == RT_POLL)
    return CmRecord[cindex].u_rec[findex].ginterval;
  else
    return 0.5; //For best-effort..
}

/*****************************************************************************

*****************************************************************************/
void MacDocsisCMTS::jobdrop(mapptr j)
{
    printf("CMTS:jobdrop(%lf): ERROR:  \n", Scheduler::instance().clock());
}

/*****************************************************************************

*****************************************************************************/
void MacDocsisCMTS::CmtsUcdHandler(Event * e)
{
  Packet * p = Packet::alloc();
  double stime;
  hdr_docsis   *dh  = hdr_docsis::access(p);
  hdr_cmn   *ch  = HDR_CMN(p);
  hdr_docsismgmt  *mgmt  = hdr_docsismgmt::access(p);
  hdr_mac   *mp  = HDR_MAC(p);
  double curr_time = Scheduler::instance().clock();
  
#ifdef TRACE_CMTS //-------------------------------------------------
  printf("CMTSUcdHandler(%lf): Sending a UCD message \n",curr_time);
#endif //------------------------------------------------------------
  
  ch->uid() = 0;

  //ch->ptype() = PT_MAC;
  ch->ptype() = PT_DOCSISMGMT;
  ch->iface() = -2;
  ch->error() = 0;
    
  ch->size() = 22 + SIZE_MGMT_HDR;
  mp->macSA() = index_;
  mp->macDA() = -1;
  dh->dshdr_.fc_type = MAC_SPECIFIC;
  dh->dshdr_.fc_parm = MANAGEMENT;
  dh->dshdr_.ehdr_on = 0;
  dh->dshdr_.mac_param = 0;
  mgmt->type_() = UCDMESSAGE; 
  

  MacSendFrame(p,MGMT_PKT);
    
  /*  add some randomness.  This will reduce the frequency but that's ok */
//  double random_delay  = Random::uniform(.01,3);
//  mhUcd_.start((Packet*) (&uintr),Conf_Table_.mgtparam.ucd_msg_interval+random_delay); 
}

/*****************************************************************************

*****************************************************************************/
void MacDocsisCMTS::CmtsSyncHandler(Event * e)
{
  Packet * p = Packet::alloc();
  double stime;
  hdr_docsis   *dh  = hdr_docsis::access(p);
  hdr_cmn   *ch  = HDR_CMN(p);
  hdr_docsismgmt  *mgmt  = hdr_docsismgmt::access(p);
  hdr_mac   *mp  = HDR_MAC(p);
  double curr_time = Scheduler::instance().clock();
  
#ifdef TRACE_CMTS //---------------------------------------------------
  printf("CMTSSyncHandler(%lf): Sending a SYNC message \n",curr_time);
#endif //--------------------------------------------------------------
  
  ch->uid() = 0;
  //ch->ptype() = PT_MAC;
  ch->ptype() = PT_DOCSISMGMT;
  ch->iface() = -2;
  ch->error() = 0;    
  ch->size() = 10 + SIZE_MGMT_HDR;
  mp->macSA() = index_;
  mp->macDA() = -1;
  dh->dshdr_.fc_type = MAC_SPECIFIC;
  dh->dshdr_.fc_parm = MANAGEMENT;
  dh->dshdr_.ehdr_on = 0;
  dh->dshdr_.mac_param = 0;
  mgmt->type_() = SYNCMESSAGE; /* Indicating NON-MAP message */  

  MacSendFrame(p,MGMT_PKT); /* Send the packet */
  
  /* add some randomness.  This will reduce the frequency but that's ok */
  double random_delay  = Random::uniform(.01,3);
  mhSync_.start((Packet*) (&sintr),Conf_Table_.mgtparam.sync_msg_interval+random_delay); 
}

/*****************************************************************************

*****************************************************************************/
void MacDocsisCMTS::CmtsRngHandler(Event * e)
{
  Packet * p = Packet::alloc();
  double stime;
  hdr_docsis   *dh  = hdr_docsis::access(p);
  hdr_cmn   *ch  = HDR_CMN(p);
  hdr_docsismgmt  *mgmt  = hdr_docsismgmt::access(p);
  hdr_mac   *mp  = HDR_MAC(p);
  double curr_time = Scheduler::instance().clock();
  
#ifdef TRACE_CMTS //-----------------------------------------------------------------
  printf("CMTSRngHandler(%lf): Sending a RNG-RSP message \n",curr_time);
#endif //----------------------------------------------------------------------------
  
  ch->uid() = 0;

  //ch->ptype() = PT_MAC;  
  ch->ptype() = PT_DOCSISMGMT;
  ch->iface() = -2;
  ch->error() = 0;    
  ch->size() = 27 + SIZE_MGMT_HDR;
  mp->macSA() = index_;
  mp->macDA() = -1;
  dh->dshdr_.fc_type = MAC_SPECIFIC;
  dh->dshdr_.fc_parm = MANAGEMENT;
  dh->dshdr_.ehdr_on = 0;
  dh->dshdr_.mac_param = 0;
  mgmt->type_() = RANGEMESSAGE; /* Indicating NON-MAP message */  



  MacSendFrame(p,MGMT_PKT);
  
  // add some randomness.  This will reduce the frequency but that's ok
  double random_delay  = Random::uniform(.01,3);
  mhRng_.start((Packet*) (&rintr),Conf_Table_.mgtparam.rng_msg_interval+random_delay); 
}


/*****************************************************************************
 * Routine: void MacDocsisCMTS::dumpFinalCMTSStats(char *outputFile)
 *
 * Explanation:
 *   This gathers the final stats from all objects.
 *
 * Input:
 *   char *outputFile:  The text name of an output file that will contain a very
 *     concise set of final stats (numbers only)
 *
 *
 * Output: 
 *
*****************************************************************************/
void MacDocsisCMTS::dumpFinalCMTSStats(char *outputFile)
{
  double curr_time = Scheduler::instance().clock();
  struct packetStatsType USstats;
  struct packetStatsType DSstats;
  struct macPhyInterfaceStatsType macPhyInterfaceStats;
  struct channelStatsType channelStats;
  int i;
  double BWConsumed = 0;


  FILE* fp;
  FILE* fpch;

  if (curr_time == 0) {
    return;
  }
  if (outputFile == NULL) 
  {
    return;
  }
  


//#ifdef TRACE_CMTS 
  printf("dumpFinalCMTSStats: numChannels: %d (DS:%d, US:%d)\n", numChannels,numDChan,numUChan);
  printf("  total_num_sent_bytes(frames): %f (%d),  total_num_rx_bytes(frames):%f (%d) (numPDUs:%d)\n", 
		  MACstats.total_num_sent_bytes,MACstats.total_num_sent_frames,
		  MACstats.total_num_rx_bytes,MACstats.total_num_rx_frames, MACstats.total_num_rx_PDUs);


  printf("Total application bytes(pkts) DS: %f (%d)  Total application bytes(pkts) US: %f (%d) \n",
	 MACstats.total_num_appbytesDS,MACstats.total_num_appPktsDS,MACstats.total_num_appbytesUS, MACstats.total_num_appPktsUS);

  printf("Total packets dropped: %d,  Packets dropped in token queue: %d\n", 
	 MACstats.total_packets_dropped, MACstats.dropped_tokenq);


  printf("Num of contention req received: %d Num of piggy req received: %d\n", 
	 CMTSstats.num_creq,CMTSstats.num_piggyreq);
  printf("Num of contention req granted: %d Num of contention req denied: %d \n", 
	 CMTSstats.num_creqgrant,CMTSstats.num_creqdenied);
  printf("Num of unicast requests granted %d , Num of contention req granted with a partial: %d \n", 
	 CMTSstats.num_ureqgrant,CMTSstats.num_preqgrant);
  printf("Total num of contention slots: %d Total num of management slots: %d \n",
	 CMTSstats.total_num_cslots,CMTSstats.total_num_mgmtslots);


  //Get the raw stats data for the concise data output file 
  myDSpacketMonitor->getStats(&DSstats);
  myUSpacketMonitor->getStats(&USstats);



  fp = fopen(outputFile, "a+");
      
  fprintf(fp,"%f\t%16.0f\t%d\t%16.0f\t%d\t%d\t",
      curr_time,MACstats.total_num_sent_bytes, MACstats.total_num_sent_frames,
      MACstats.total_num_rx_bytes, MACstats.total_num_rx_frames,MACstats.total_num_rx_PDUs);
      
  fprintf(fp,"%16.0f\t%d\t%16.0f\t%d\t",
	USstats.byteCount,USstats.packetCount,
    DSstats.byteCount,DSstats.packetCount);

  if (DSstats.packetCount > 0)
      fprintf(fp,"%d\t%d\t%3.3f\t",
			  MACstats.total_packets_dropped, MACstats.dropped_tokenq,
			  ((double)MACstats.total_packets_dropped/(double)DSstats.packetCount) * 100);
  else
      fprintf(fp,"%d\t%d\t0\t",
			  MACstats.total_packets_dropped, MACstats.dropped_tokenq);

  fprintf(fp,"%d\t%d\t%d\t%d\t",
	      CMTSstats.num_creq, CMTSstats.num_piggyreq, CMTSstats.num_creqgrant, CMTSstats.num_creqdenied);

  fprintf(fp,"%d\t%d\t%d\t%d\t%d\t%d\t%d\t", 
	      MACstats.num_mgmtpkts, MACstats.total_num_rng_pkts_US, MACstats.total_num_concat_pkts_US,
	      MACstats.total_num_frag_pkts_US, MACstats.total_num_req_pkts_US, MACstats.total_num_plaindata_pkts_US,
	      MACstats.total_num_concatdata_pkts_US);
      
  fprintf(fp,"%d\t%d\t%d\t%d\t", 
	      MACstats.total_num_BE_pkts_US, MACstats.total_num_RTVBR_pkts_US,
	      MACstats.total_num_UGS_pkts_US, MACstats.total_num_OTHER_pkts_US);

  fprintf(fp,"%d\t%d \n",
	      CMTSstats.num_ureqgrant, CMTSstats.num_preqgrant); 


  printf("dumpFinalCMTSStats: DS packet monitor:  \n");
  myDSpacketMonitor->printStatsSummary();
  printf("dumpFinalCMTSStats: US packet monitor:  \n");
  myUSpacketMonitor->printStatsSummary();
  
//get summary of binary packet trace files
  printf("dumpFinalCMTSStats: DS BINARY packet monitor:  \n");
  myDSpacketMonitor->getBinaryTraceSummary();
  printf("dumpFinalCMTSStats: US BINARY packet monitor:  \n");
  myUSpacketMonitor->getBinaryTraceSummary();



  myPhyInterface->printStatsSummary();
  //dump per channel stats
  int numDSChannels = myPhyInterface->getNumberDSChannels();
  int numUSChannels = myPhyInterface->getNumberUSChannels();
  printf("numDSChannels: %d,  numUSChannels: %d \n",numDSChannels,numUSChannels);

  fpch = fopen("CMTSstatsperchannel.out", "a+");

  for (i=0; i<numDSChannels; i++) {
    myPhyInterface->getChannelStats(&channelStats,i);
    BWConsumed = channelStats.total_num_sent_bytes*8/curr_time;
    printf("CHANNEL STATS DS(%d), util:%3.3f,  total_num_sent_bytes (frames): %12.0f (%12.0f), numberCollisions (pkts): %12.0lf TotalBW:%12.1f\n",
			channelStats.channelNumber,channelStats.channelUtilization,channelStats.total_num_sent_bytes, channelStats.total_num_sent_frames, 
			channelStats.numberCollisions,BWConsumed);
    fprintf(fpch,"1 %i %3.3f %lf %lf %lf %lf %lf %12.1f\n",
			channelStats.channelNumber,channelStats.channelUtilization,channelStats.total_num_sent_bytes, channelStats.total_num_sent_frames, 
			channelStats.total_num_rx_bytes, channelStats.total_num_rx_frames, channelStats.numberCollisions, BWConsumed);
  }
  for (i=numDSChannels; i<(numDSChannels+numUSChannels); i++) {
    myPhyInterface->getChannelStats(&channelStats,i);
    BWConsumed = channelStats.total_num_sent_bytes*8/curr_time;
    printf("CHANNEL STATS US(%d), util:%3.3f,  total_num_sent_bytes (frames): %12.0f (%12.0f), numberCollisions (pkts): %12.0f  TotalBW: %12.1f\n",
			channelStats.channelNumber,channelStats.channelUtilization,channelStats.total_num_sent_bytes, channelStats.total_num_sent_frames, 
			channelStats.numberCollisions, BWConsumed);
    fprintf(fpch,"0 %i %3.3f %lf %lf %lf %lf %lf %12.1f\n",
			channelStats.channelNumber,channelStats.channelUtilization,channelStats.total_num_sent_bytes, channelStats.total_num_sent_frames, 
			channelStats.total_num_rx_bytes, channelStats.total_num_rx_frames, channelStats.numberCollisions, BWConsumed);
  }

  fclose(fp);
  fclose(fpch);

  //Currently we don't use this
  myPhyInterface->getStats(&macPhyInterfaceStats);

  printf("dumpFinalCMTSstats:  DS BG Mgr stats ... \n");
  myBGMgr->printStatsSummary("BGSIDstats.out");
  
  printf("dumpFinalCMTSstats:  DS SF Mgr stats ... \n");
  myDSSFMgr->printStatsSummary("DSSIDstats.out");
  printf("dumpFinalCMTSstats:  US SF Mgr stats ... \n");
  myUSSFMgr->printStatsSummary("USSIDstats.out");

  printf("dumpFinalCMTSstats:  DS scheduler stats ... \n");
  myDSScheduler->printStatsSummary();
  printf("dumpFinalCMTSstats:  US scheduler stats ... \n");
  myUSScheduler->printStatsSummary();


  return;
  
}

/*****************************************************************************

*****************************************************************************/
void MacDocsisCMTS::dumpBWCMTS(char *outputFile)
{
  double curr_time = Scheduler::instance().clock();
  double TotalBWup = 0;
  double TotalBWdown = 0;
  double ChBWup = 0;
  double ChBWdown = 0;
  FILE* fp;
  
#ifdef TRACE_CMTS 
  printf("CMTS:dumpBWCMTS: %f  Entered, current bytesUP:%f;  bytesDOWN:%f, last_BWCalcTime: %6.2f, outputFile:%s \n",
	 curr_time, MACstats.total_num_BW_bytesUP, MACstats.total_num_BW_bytesDOWN,MACstats.last_BWCalcTime,outputFile);
  printf("  total bytes (frames)  sent: %f (%d),  total bytes (frames) received:%f (%d) \n", 
		  MACstats.total_num_sent_bytes,MACstats.total_num_sent_frames,
		  MACstats.total_num_rx_bytes,MACstats.total_num_rx_frames);
#endif 
    
  fp = fopen(outputFile, "a+");
  
  if (curr_time - MACstats.last_BWCalcTime > 0)
    {      

          TotalBWup = (MACstats.total_num_BW_bytesUP*8)/(curr_time - MACstats.last_BWCalcTime);
          TotalBWdown = (MACstats.total_num_BW_bytesDOWN*8)/(curr_time - MACstats.last_BWCalcTime);
          MACstats.total_num_BW_bytesUP = 0;
          MACstats.total_num_BW_bytesDOWN = 0;
          fprintf(fp,"%f  %6.2f  %6.2f\n",curr_time, TotalBWup, TotalBWdown);
#ifdef TRACE_CMTS //--------------------------------------------
          printf("CMTS: %f  %6.2f  %6.2f\n",curr_time, TotalBWup, TotalBWdown);
#endif //-------------------------------------------------------

      MACstats.last_BWCalcTime = curr_time;
    }
  else 
    {
      fprintf(fp, "%f  %6.2f  %6.2f\n",curr_time, TotalBWup, TotalBWdown);
      
#ifdef TRACE_CMTS 
      printf("CMTS: %f  %6.2f  %6.2f\n",curr_time, TotalBWup, TotalBWdown);
#endif 
    }  
  fclose(fp);
  dumpUGSJITTER("UGSJitterCMTS.out");  
}

/*****************************************************************************
 * Routine void MacDocsisCMTS::dumpUGSJITTER(char *outputFile)
 *
 * Explanation
 *    This is called periodically (as of 3/26/05 it is piggybacked off 
 *    the dumpBWCMTS mechanism).  It computes a jitter statistic based
 *    on stats gathered since the last time this method was called.
 *    Each time a UGS allocation is made, the jitter is updated.  This running
 *    total of jitter is simply the difference between the scheduled
 *    allocation time (based on the UGS grant interval) and the actual.
 *
 * Inputs
 *
 * Outputs
 *
*****************************************************************************/
void MacDocsisCMTS::dumpUGSJITTER(char *outputFile)
{
  jptr jp;
  FILE *fp;
  double curr_time = Scheduler::instance().clock();
  
  jp = job_list[0]; /* UGS jobs */
  
  if (!jp)
    return; /* No UGS jobs */
  
  fp = fopen(outputFile, "a+");
  
  while (jp)
    {
          double avgjitter = 0.0;
	  
	  if (jp->jitterSamples > 0)
	    avgjitter = jp->ugsjitter / jp->jitterSamples;
	  else
	    avgjitter = 0;


#ifdef TRACE_CMTS //---------------------------------------------
	  if (jp->flow_id == 4) {
            printf("CMTSdumpUGSJITTER(%lf): flow_id = %d avgjitter = %lf (jitter:%lf, #samples:%d\n",
                 curr_time,jp->flow_id,avgjitter,jp->ugsjitter,jp->jitterSamples);
	    fprintf(fp,"%lf %d %lf\n",curr_time,jp->flow_id, avgjitter);
	  }
#endif //--------------------------------------------------------
	  jp->ugsjitter = 0.0;
	  jp->jitterSamples = 0;
	  jp->last_jittercaltime = curr_time;
      jp = jp->next;      
    }
  fclose(fp);
}

/*****************************************************************************
Called periodically by a TCL script to monitor the queue in the DS direction....
This method also computes the utilization of the DS channel for each iteration....
*****************************************************************************/
void MacDocsisCMTS::dumpDOCSISQueueStats(char *outputFile)
{
 
 
#ifdef TRACE_CMTS //----------------------------------------------------------------------------------
 printf("CMTS:dumpDOCSISQueueStats: %f    total_num_sent_bytes:%f   \n",
       Scheduler::instance().clock(), MACstats.total_num_sent_bytes);
#endif //---------------------------------------------------------------------------------------------

 myDSSFMgr->dumpSFQueueLevels(outputFile);
  
}

/*****************************************************************************
Called periodically by a TCL script to monitor the queue in the DS direction....
*****************************************************************************/
void MacDocsisCMTS::dumpDOCSISUtilStats(char *outputFile, int DSBW, int USBW)
{
  double curr_time = Scheduler::instance().clock();
  double DSutil=0;
  double USutil=0;
  
#ifdef TRACE_CMTS //-----------------------------------------------------------------------
  printf("CMTS:dumpDOCSISUtilStats: %f     DSutil:%6.3f  USutil:%6.3f\n",
	 curr_time, DSutil, USutil);
#endif //----------------------------------------------------------------------------------

//TODO: To monitor utilization this routine needs to cycle through all DS and US
//      channels and  call the macPhyInterface to get the latest utilization (or
//      this method needs to track/calculate it itself via byte counts)
//      The output needs to change to
//	 curr_time, Ch1util, Ch2util, Ch3util, .....);
//      And the user needs to know that, for example ch1...ch8 are DS and ch9-12 are US
//  

#if 0
  fp = fopen(outputFile, "a+");
  
  /* Print :  timestamp  max pkts  min pkts  cur pkts util */
  fprintf(fp,"%f %3.3f %3.3f %16f %16f\n",curr_time,DSutil,USutil,util_bytes_DS,util_bytes_US);
  
  /* Reset  */
  util_bytes_DS=0;
  util_bytes_US=0;
  lastUtilDumpTime = curr_time;
  fclose(fp);
#endif

}

/*************************************************************************
*function:int MacDocsisCMTS::setupChannels(int numDChs, int numUChs)
*
*explanation: 
*  This method is called when the CMTS object is being configured.The method
*  setups the necessary object to support the configured number 
*  of upstream and downstream channels.
*
*inputs:
*
*outputs: Returns a 0 on success, else an error code:
*    1: general error
*
*
*************************************************************************/
int MacDocsisCMTS::setupChannels(void)
{
  int rc = 0;
  int i;

    myDSpacketMonitor = new packetMonitor();
    myUSpacketMonitor = new packetMonitor();
    myDSpacketMonitor->init(cm_id,PT_SEND_DIRECTION);
    myUSpacketMonitor->init(cm_id,PT_RECV_DIRECTION);

// To turn on CMTS packet traces...uncomment
//    myUSpacketMonitor->setTraceLevel( 0x00 | (packetMonitor_TRACE_TO_FILE) | (packetMonitor_TRACE_TO_BINARY_FILE));
//    myDSpacketMonitor->setTraceLevel( 0x00 | (packetMonitor_TRACE_TO_FILE) | (packetMonitor_TRACE_TO_BINARY_FILE));
    myDSpacketMonitor->setTraceLevel( 0x00);
    myUSpacketMonitor->setTraceLevel( 0x00);

    myDSpacketMonitor->setTextTraceFile("CMTSDSPACKET.txt");
    myUSpacketMonitor->setTextTraceFile("CMTSUSPACKET.txt");

    myDSpacketMonitor->setBinaryTraceFile("CMTSDSPACKET.bin");
    myUSpacketMonitor->setBinaryTraceFile("CMTSUSPACKET.bin");

    myMedium->init(this);

    myPhyInterface = new CMTSmacPhyInterface;
    myPhyInterface->setMyMac(this);
    myPhyInterface->init(cm_id,myMedium);
//    myPhyInterface->init(cm_id,9,1,myMedium);
	numDChan = myPhyInterface->getNumberDSChannels();
	numUChan = myPhyInterface->getNumberUSChannels();
	numChannels = numUChan + numDChan;
#ifdef TRACE_CMTS 
	printf("MacDocsisCMTS::setupChannels:  Setting up %d Downstream Channels, and %d Upstream Channels\n", numDChan, numUChan);
#endif


#ifdef TRACE_CMTS 
	printf("MacDocsisCMTS::setupChannels:  macPhyInterface says numberDSChannels: %d \n",
			 myPhyInterface->getNumberDSChannels());
	fflush(stdout);
#endif

	myDSSFMgr = new CMTSserviceFlowMgr();

	myDSSFMgr->setMacHandle(this);
	myDSSFMgr->init(DOWNSTREAM);
	myDSSFMgr->setMyPhyInterface(myPhyInterface);
	myUSSFMgr = new CMTSserviceFlowMgr();
	myUSSFMgr->setMacHandle(this);
	myUSSFMgr->init(UPSTREAM);
	myUSSFMgr->setMyPhyInterface(myPhyInterface);
	
	myPhyInterface->setMyDSSFMgr(myDSSFMgr);
	myPhyInterface->linkMediumToDSSFMgr();

	myDSScheduler = new DSschedulerObject();
	myDSScheduler->init(myDSSFMgr,myBGMgr);
	myUSScheduler = new USschedulerObject();
	myUSScheduler->init(myDSSFMgr,myBGMgr);
	myDSScheduler->myMacPhyInterface = (macPhyInterface * ) myPhyInterface;

	myBGMgr->setMyPhyInterface( (CMTSmacPhyInterface *)myPhyInterface);

//$A805
    myOptimizer = new resourceOptimizer();
    myOptimizer->init(DOWNSTREAM,this, myDSSFMgr, myBGMgr,myDSScheduler);
    myBGMgr->myMac = this;
    myBGMgr->mySFMgr = myDSSFMgr;

//#ifdef TRACE_CMTS 
	printf("MacDocsisCMTS::setupChannels:  All done, macPhyInterface says numberDSChannels: %d  (ptr:%x)\n",
			 myPhyInterface->getNumberDSChannels(),myPhyInterface);
//#endif


}


/*************************************************************************
*function:int MacDocsisCMTS::setupChannels(int numDChs, int numUChs)
*
*explanation: 
*  This method is called when the CMTS object is being configured.The method
*  setups the necessary object to support the configured number 
*  of upstream and downstream channels.
*
*inputs:
 *     Packet *p
 *     serviceFlowObject *mySF
 *     int cindex
 *     int findex
 *     int type : indicates if frame is DATA or MGMT
*      int fileID:   
*               1: From the Internet....DSArrivals1.out
*               2: After rate control...DSArrivals2.out
*               3: Sent DS to the Phy (arriving at the PHY for DS) ...DSArrivals3.out
*                        NOTE: This is all FRAMES !!!!
*
*outputs: Returns a 0 on success, else an error code:
*    1: general error
*
*
*************************************************************************/
int MacDocsisCMTS::simplePacketTrace(Packet *p, serviceFlowObject *mySF, int fileID)
{

  int rc = 0;
  double now = Scheduler::instance().clock();

#ifdef ARRIVAL_TRACE
  struct hdr_cmn *ch = HDR_CMN(p);
  int type = ClassifyDataMgmt(p) ? MGMT_PKT : DATA_PKT;

    if (fileID == 1) {
      if (type == DATA_PKT){
        FILE* fp = NULL;
        struct hdr_cmn* ch = HDR_CMN(p);
        fp = fopen("DSArrivals1.out", "a+");
        fprintf(fp,"%lf %d %d \n",
          Scheduler::instance().clock(), ch->size(), mySF->flowID);
        fclose(fp);
      }
    } else if (fileID ==2) {
      if (type == DATA_PKT){
        FILE* fp = NULL;
        struct hdr_cmn* ch = HDR_CMN(p);
        fp = fopen("DSArrivals2.out", "a+");
        fprintf(fp,"%lf %d %d \n",
          Scheduler::instance().clock(), ch->size(), mySF->flowID);
        fclose(fp);
      }
    } else if (fileID ==3) {
      FILE* fp = NULL;
      struct hdr_cmn* ch = HDR_CMN(p);
      fp = fopen("DSArrivals3.out", "a+");
      fprintf(fp,"%lf %d %d \n",
          Scheduler::instance().clock(), ch->size(), mySF->flowID);
      fclose(fp);
    }
#endif

  return rc;
}

/***************************************************************************
 * Module: hierarchical packet scheduler object.
 *
 * Explanation:
 *   This file contains the class definition of the hierarchical packet scheduler
 *
 * Revisions:
 *
 *  TODO:
 *
************************************************************************/

#ifndef ns_hierarchicalPacketSchedulerObject_h
#define ns_hierarchicalPacketSchedulerObject_h

#include "packetScheduler.h"

#include "bondingGroupMgr.h"


class hierarchicalPacketSchedulerObject: public packetSchedulerObject
{
public:
  hierarchicalPacketSchedulerObject();
  ~hierarchicalPacketSchedulerObject();

  void init(int serviceDiscipline, int qSizeParam, int QTypeParam, serviceFlowMgr*, bondingGroupMgr *);
  Packet *selectPacket(int channelNumber, struct serviceFlowSet *mySFSet); 
  Packet *selectPacketfromBG(int channelNumber, bondingGroupObject *myBGObg); 
  int  selectChannel(Packet *pkt, serviceFlowObject *mySFObj, struct channelSet *myChannelSet, int *channelSelection);


private:
  int numberSFs;
  int lastBGIndexServiced;
  int lastAggQueueIndexServiced;
  int lastSFsent;
  int QSize;
  int QType;
  int serviceDiscipline;

  bondingGroupObject * selectBondingGroup(int channelNumber, struct bondingGroupSet *myBGSet);


};



#endif /* __ns_hierarchicalPacketSchedulerObject_h__ */

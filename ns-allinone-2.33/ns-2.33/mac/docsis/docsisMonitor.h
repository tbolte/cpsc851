/***************************************************************************
 * Module: docsisMonitor
 *
 * Explanation:
 *   This file contains the class definition of the docsis Monitor.
 *
 * Revisions:
 *
 *  TODO:
************************************************************************/

#ifndef ns_docsisMonitor_h
#define ns_docsisMonitor_h

#include "packet.h"
#include "rtp.h"
//#include "mac-docsis.h"
#include "hdr-docsis.h"

#include "packetMonitor.h"


//So unlike the PREC_T this contains DOCSIS frame info
struct FREC_T
{
  double tstamp;
  unsigned int direction;

  unsigned int packet_t;    //defined in packet.h

  unsigned int saddr;
  unsigned int daddr;
  unsigned short len;

  unsigned int seq;
  unsigned int ack;
  unsigned int off_win;
  unsigned int tcp_flags;
  unsigned int seglen;
  unsigned short source;
  unsigned short dest;

  unsigned int snd_una;
  unsigned int snd_nxt;
  unsigned int rcv_nxt;
  unsigned int snd_wnd;
  unsigned int snd_cwnd;

};

struct docsisStatsType
{
  u_int32_t frameByteCount;
  u_int32_t frameCount;

  u_int32_t runningFrameByteCount;
  u_int32_t runningFrameCount;
};

/*************************************************************************
 ************************************************************************/
class docsisMonitor: public packetMonitor
{
public:


  docsisMonitor();
  docsisMonitor(int direction);

  ~docsisMonitor();

  //returns the trace in a string
  virtual int packetTrace(Packet *p, char *traceOutput, int outputSize, int channelNumber);
  virtual int packetTrace(Packet *p, int channelNumber);
  virtual void binaryPacketTrace(Packet *p);

  virtual void init();
  virtual void init(int id, int direction);
  virtual void clearStats();
  virtual void clearRunningStats();
  void updateStats(Packet *p);
  void printStatsSummary();
  void getStats(struct docsisStatsType *callersStats);

  struct docsisStatsType myDOCSISStats;

private:

};

/*************************************************************************
 ************************************************************************/
class docsisCMTSMonitor: public docsisMonitor
{
public:


  docsisCMTSMonitor();
  docsisCMTSMonitor(int direction);

  ~docsisCMTSMonitor();
  void init(int id, int direction);
  void clearStats();
  void clearRunningStats();



private:

};

/*************************************************************************
 ************************************************************************/
class docsisCMMonitor: public docsisMonitor
{
public:


  docsisCMMonitor();
  docsisCMMonitor(int direction);

  ~docsisCMMonitor();

  void init(int id, int direction);
  void clearStats();
  void clearRunningStats();


private:

};

#endif /* __ns_DOCSISMonitor_h__ */

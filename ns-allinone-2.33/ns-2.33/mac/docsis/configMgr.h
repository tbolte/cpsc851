/***************************************************************************
 * Module: configMgr
 *
 * Explanation:
 *   This file contains the class definition of the object that
 *   packages  the MAC config.  It also provides the main interface
 *   to TCL.  
 *
 * Revisions:
 *   
 *
************************************************************************/

#ifndef ns_configMgr_h
#define ns_configMgr_h

#include "hdr-docsis.h"
#include "mac-docsis.h"


/*************************************************************************
 ************************************************************************/
class configMgr
{
public:
  configMgr();

  MacDocsis *myMac;
  virtual void init();
  virtual int processCommandLine(int argc, const char *const*argv);

//struct mgmt_conf_param 
  double sync_msg_interval;       
  double rng_msg_interval;
  double ucd_msg_interval;        

//struct map_conf_param
  double time_covered;            /* Time covered by MAP(in ms), 
                     0 inidcates varying, >0 indicates 
                     the actual time to be covered by MAP */

  double map_interval;            /* Interval between two maps(in s)*/

  u_int32_t num_contention_slots; /* Number of contention slots per map */
  u_int32_t num_sm_slots;         /* Number of station maintainence 
                     slots per map */
  u_int32_t sgrant_limit;         /* Max number of bytes that can be
                     sent in short grant IE */
  u_int32_t lgrant_limit;         /* Max number of bytes that can be
                     sent in long grant IE */
  u_char bkoff_start;             /* Initial back-off window for 
                     contention data and request */
  u_char bkoff_end;               /* Final back-off window for 
                     contention data and request */

  int debug_config;

private:



};

class CMTSconfigMgr: public configMgr
{
public:
  MacDocsisCMTS *myMac;
  CMTSconfigMgr();
  void init(MacDocsisCMTS *myMac);
  int processCommandLine(int argc, const char *const*argv);

};

class CMconfigMgr: public configMgr
{
public:
  MacDocsisCM *myMac;
  CMconfigMgr();
  void init(MacDocsisCM *myMac);
  int processCommandLine(int argc, const char *const*argv);

};


#endif /* __ns_configMgr_h__ */

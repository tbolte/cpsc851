/***************************************************************************
 * Module: channelProperties
 *
 * Explanation:
 *   This file contains the class definition of the object that
 *   packages config information and config interfaces.
 * 
 * Revisions:
 *
 *
************************************************************************/

#include "globalDefines.h"
#include "channelProperties.h"


#include "docsisDebug.h"
//#define TRACEME 0

/*************************************************************************
 ************************************************************************/
channelProperties::channelProperties()
{
#ifdef TRACEME
  printf("channelProperties::constructor: \n");
#endif

  data_rate =0;     
  prop_delay=0;
  max_burst_size=0;
  overhead_bytes=0;
  ticks_p_minislot=0;

}




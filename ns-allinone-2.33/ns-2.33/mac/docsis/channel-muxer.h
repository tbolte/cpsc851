/***************************************************************************
 * Module: channel-muxer
 *
 * Explanation:
 *   This file contains the channel muxer object.  This object
 *   abstracts a unidirectional (i.e., half duplex) channel.
 *
 * Revisions:
 *
************************************************************************/
/****************************************************************************
 ***************************************************************************/

#ifndef ns_channel_muxer_h
#define ns_channel_muxer_h

#include "hdr-docsis.h"

#define CHANNEL_DIRECTION_DOWN 0
#define CHANNEL_DIRECTION_UP 1
#define CHANNEL_DIRECTION_DUPLEX 2

/*************************************************************************
 * This is the class that will keep up with the Channel/Phy abstraction  *
 * that allows for numChannels channels. The metadata for all channels   *
 * is contained here.                         Added by: WBSP, 04.11.2008 *
 ************************************************************************/
class channel_muxer
{
public:
  channel_muxer();
  channel_muxer(int channelNumber);
  
  Event rxintr_; // Event to be consumed by the receive process

  int ch_direction;  // upstream or downstream channel

  /* Internal MAC state */
  MacState rx_state_;	// incoming state (MAC_RECV or MAC_IDLE)
  MacState tx_state_;	// outgoing state
  int tx_active_;
  int packet_state;  // 2 states possible 1: Packet not Handled 0: Packet Handled

  /*STATISTICS*/

  double util_total_bytes_US;    /* used for final utilization stats*/
  double util_total_bytes_DS;    /* used for final utilization stats*/
  int util_total_pkts_US;        /* used for final utilization stats*/
  int util_total_pkts_DS;        /* used for final utilization stats*/
  double util_bytes_US;          /* used by dumpDOCSISUtilStats*/
  double util_bytes_DS;          /* used by dumpDOCSISUtilStats*/
 
  u_int32_t total_num_sent_pkts; /* total Num of packets sent */
  
  /*
    Note:  need this to be a TracedDouble to access from tcl right ????
  */
  //$A0:  Need to change to a  double to avoid int overrun
  
  double total_num_sent_bytes; /* total Num of bytes received 
				  used to for CMTSdown 
				  used to for CMup */
  u_int32_t num_pkts;
  u_int32_t dropped_dsq;    /* total packets dropped at the DS queue*/ 
  u_int32_t dropped_tokenq;
  //$A5
  u_int32_t total_num_mgt_pkts_US; /* total Num of mgt received  */
  u_int32_t total_num_rng_pkts_US; 
  u_int32_t total_num_status_pkts_US; 
  u_int32_t total_num_concat_pkts_US; 
  u_int32_t total_num_frag_pkts_US; 
  u_int32_t total_num_req_pkts_US; 
  u_int32_t total_num_plaindata_pkts_US; 
  u_int32_t total_num_concatdata_pkts_US; 
  u_int32_t total_num_frames_US; 
  u_int32_t total_num_BE_pkts_US; 
  u_int32_t total_num_RTVBR_pkts_US; 
  u_int32_t total_num_UGS_pkts_US; 
  u_int32_t total_num_OTHER_pkts_US; 
  
  u_int32_t total_num_rx_pkts; /* total Num of packets received */
  
  //$A0
  double total_num_rx_bytes; /* total Num of bytes received 
				used to for CMTSup 
				used to for CMdown */
  
  //$A1
  double total_num_BW_bytesUP;   /* total Num of bytes for BW calculation*/
  double total_num_BW_bytesDOWN; /* total Num of bytes for BW calculation*/
  
  double total_num_appbytesUS;   /*total Num app bytes received 
				   on US w/o docsis overhead*/
  
  double total_num_appbytesDS;   /*total Num app bytes forwarded 
				   on DS w/o docsis overhead*/
  
  u_int32_t total_packets_dropped;/* total packets dropped
				     for cmts, this is downstream,
				     for cm, this is upstream*/

  void init();
  
  //$A304
  void setChannelNumber(int channelNumber);

  int getCollisionState();
  void setCollisionState();
  void clearCollisionState();
  
  int getTxState();
  void setTxStateSEND();
  void setTxStateIDLE();

  int getRxState();
  void setRxStateRECV();
  void setRxStateIDLE();

  int getDirection();
  void setDirectionUS();
  void setDirectionDS();
  void setDirectionDUPLEX();

  int getNumberCollisions();

// private:
//  MacState tx_state_;	// outgoing state
   int channelNumber_;
   int  collision;       /* Indicate whether collision occured */

// Statistics
   double numberCollisions;

};

#endif /* __ns_channel_muxer_h__ */

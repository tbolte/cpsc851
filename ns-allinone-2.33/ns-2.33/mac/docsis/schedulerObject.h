/***************************************************************************
 * Module: schedulerObject
 *
 * Explanation:
 *   This file contains the class definition of the scheduler. 
 *
 * Revisions:
 *
 *  TODO:
 *
************************************************************************/

#ifndef ns_schedulerObject_h
#define ns_schedulerObject_h


class serviceFlowObject;
class serviceFlowMgr;
class bondingGroupMgr;
class macPhyInterface;
class packetSchedulerObject;

#include "packet.h"
#include "rtp.h"
#include "hdr-docsis.h"

#define SCHED_STATE_INITED 1
#define SCHED_STATE_CONFIGURED 2

#define MAX_SINGLE_BURST_CHANNELS 8

struct  schedulerAssignmentType
{
  int maxBurstSize;
  int minBurstSize;
  int burstUnits;
  int numberAssignments;
//$A801
  u_int16_t dsidAssignment; 
  int channelAssignments[MAX_SINGLE_BURST_CHANNELS];  //assume will never stripe over > 8 channels
  Packet *myPkt;
};

struct  schedulerStatsType
{
  u_int32_t numberSchedulingDecisions;
  u_int32_t topMakeSchedDecisionCount;
  u_int32_t lowerMakeSchedDecisionCount;
  u_int32_t topImmediateMgmtSchedAssignments;
  u_int32_t topDeferredMgmtSchedAssignments;
  u_int32_t topImmediateDataSchedAssignments;
  u_int32_t topDeferredDataSchedAssignments;
};

/*************************************************************************
 ************************************************************************/
class schedulerObject
{
public:
  schedulerObject();
  virtual ~schedulerObject();

  void init(serviceFlowMgr*, bondingGroupMgr *);
  virtual schedulerAssignmentType * makeSchedDecision(Packet *pkt, serviceFlowObject *mySF);
  virtual schedulerAssignmentType * makeSchedDecision(int channelNumber);
  virtual int adjustScheduler(int opCode);
  int pickChannel(Packet *pkt);
  int pickChannel(Packet *pkt, serviceFlowObject *mySF);
  serviceFlowMgr *mySFMgr;
  macPhyInterface *myMacPhyInterface;

  void printStatsSummary();
  void getSchedulerStats(struct  schedulerStatsType *schedStats);

  bondingGroupMgr *myBGMgr;
  packetSchedulerObject *myPktScheduler;

  struct schedulerStatsType myStats;

  int stateFlag;   //INITED, CONFIGURED,
  int schedulerMode;
  int defaultQSize;
  int defaultQType;


protected:
  int maxBurstSize;
  int minBurstSize;
  int burstUnits;

private:

};

/*************************************************************************
 ************************************************************************/
class DSschedulerObject: public schedulerObject
{
public:
  DSschedulerObject();
  virtual ~DSschedulerObject();
  schedulerAssignmentType * makeSchedDecision(Packet *pkt, serviceFlowObject *mySF);
  schedulerAssignmentType * makeSchedDecision(int channelNumber);
  void configure(int schedulerMode, int defaultQSize, int defaultQType);
  int adjustScheduler(int opCode);


private:

};

/*************************************************************************
 ************************************************************************/
class USschedulerObject: public schedulerObject
{
public:
  USschedulerObject();
  virtual ~USschedulerObject();
  schedulerAssignmentType * makeSchedDecision(Packet *pkt, serviceFlowObject *mySF);
  schedulerAssignmentType * makeSchedDecision(int channelNumber);
  void configure(int schedulerMode, int defaultQSize, int defaultQType);


private:

};

#endif /* __ns_schedulerObject_h__ */

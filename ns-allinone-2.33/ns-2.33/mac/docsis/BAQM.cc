/************************************************************************
* File:  BAQM.cc
*
* Purpose:
*  This module contains a basic list object.
*  
*  Important:  The rates that are monitored are the true arrival/departure
*      rates to the queue.  They are not the correct service flow rates.
* 
*   All algorithm parameters are set in the setAQMParams method. 
*
* Revisions:
*  $A602 : 10-27-2009:  monitors the avg packet queue  delay
*  $A604 : 11-19-2009 :  adaptiveMode of 0 is the default BAQM behavior.
*  $A605 : 12-17-2009 :  support delay-based CA adaptive mode 1
*
************************************************************************/
#include "BAQM.h"
#include <iostream>
#include <math.h>
#include <random.h>

#include "object.h"

#include "packetListElement.h"

#include "globalDefines.h"
#include "docsisDebug.h"
//#define TRACEME 0
#define FILES_OK 0

BAQM::BAQM() : ListObj()
{
 stateFlag = 0;
#ifdef TRACEME
  printf("BAQM: constructed list \n");
  fflush(stdout);
#endif
}

BAQM::~BAQM()
{
#ifdef TRACEME
  printf("BAQM: destructed list \n");
#endif
}

//Should set params to reasonable settings, but could be overided in setAQMParams
void BAQM::resetAQMParams()
{

  maxp = 0.5;
  minth = 4;
  maxth = MAXLISTSIZE / 2;
  optimalMinth = (double) minth;
  optimalMaxth = (double)maxth;
  targetQueueLevel = (minth + maxth) / 2;
  if (targetQueueLevel <= minth)
    targetQueueLevel = minth+1;
  alpha = maxp / 4;
  if (alpha < 0.01)
    alpha = 0.01;

  beta = 0.90;

  targetDelayLevel = .175;
//  targetDelayLevel = .05;

  highestMaxp = .9;
  lowestMaxp = 0.0;
  maxthCONFIGURED = 0;
  minthCONFIGURED = 0;


  rateWeight = .02;
  BAQMRateWeight = .02;
  BAQMWeight = .02;
  BAQMHistoryWeight = 0.02;
  maxp = 0.10;
  weightQ = 1.0;
  filterTimeConstant = 0.002;
  gentleMode = 0;
  adaptiveMode=0; 


//#ifdef TRACEME
  printf("BAQM:resetAQMParams: minth:%d  maxth:%d  maxp:%2.2f targetQueueLevel:%d (targetDelayLevel:%3.3f) weightQ:%2.4f,  priority:%d, adaptiveMode:%d,  alpha:%1.2f, beta:%1.2f \n",
             minth,maxth,maxp,targetQueueLevel,targetDelayLevel,weightQ,flowPriority, adaptiveMode, alpha, beta);
//#endif

}


void BAQM::initList(int maxListSize)
{

  ListObj::initList(maxListSize);

  flowPriority = 0;
  resetAQMParams();
  
  count = -1;
//$A605
  avgPacketLoss = 0.0;

  //$A602
  avgPacketLatency = 0.0;
  queueLatencyTotalDelay = 0.0;
  queueLatencySamplesCount = 0;
  queueLatencyMonitorTotalArrivalCount = 0;

  avgQ = 0.0;
  dropP = 0.0;
  avgMaxP=0;
  avgMaxPSampleCount=0;
  avgDropP=0;
  avgDropPSampleCount=0;
  avgAvgQ = 0;
  avgAvgQPSampleCount= 0;
  q_time = 0.0;
  avgTxTime = (double)1500 * 8 / (double) 5000000;


 lastUpdateTime =0.0;
 stateFlag = 1;
 latencyToThroughputRatio = 0.5;
 updateFrequency = .50;
 bytesQueued = 0;
 avgServiceRate=0;
 avgArrivalRate=0;
 byteArrivals = 0;

//$A605
 intervalPacketArrivals = 0;
 intervalPacketsDropped = 0;
 lastLossRateSampleTime = 0;

 byteDepartures = 0;
 rateWeight = .02;
 localVsNetworkCongestionRatio=0;
 lastRateSampleTime = 0;

 BAQMAdaptationFrequency = 5.0;
 BAQMUpdateCountThreshold = 0;
 BAQMUpdateCount = 0;
 BAQMLastAdaptationTime = 0;
 BAQMLastSampleTime = 0;
 BAQMByteArrivals =0;
 BAQMByteDepartures = 0;
 avgBAQMArrivalRate = 0;
 avgBAQMServiceRate = 0;
 BAQMRateWeight = .02;
//init to something non zero
 maxBAQMArrivalRate = 10000;
 maxBAQMServiceRate = 10000;
 avgServiceRateHistory = 0;
 avgArrivalRateHistory = 0;



 BAQMQueueDelaySamples = 0;
 BAQMQueueLevelSamples = 0;

 BAQMAccessDelaySamples =0;
 BAQMConsumptionSamples = 0;
 BAQMChannelUtilizationSamples = 0;

// BAQMWeight = .02;
 BAQMWeight = .2;
//Used for avgPacketLatency
//  avgBAQMAccessDelay = (1-BAQMWeight)*avgBAQMAccessDelay + BAQMWeight*accessDelayMeasure;
//  avgBAQMConsumption = (1-BAQMWeight)*avgBAQMConsumption + BAQMWeight*consumptionMeasure;
//  avgBAQMChannelUtilization = (1-BAQMWeight)*avgBAQMChannelUtilization + BAQMWeight*channelUtilizationMeasure;
//    optimalMinth = (1-BAQMWeight) *  optimalMinth + (BAQMWeight) * minth;
//    optimalMaxth = (1-BAQMWeight)* optimalMaxth + (BAQMWeight) * maxth;

 //sets all algorithm parameters based on this [0,1], 1 is most reactive
 BAQMsensitivity = 0.5;

//This is the channel utilization threshold that defines network congestion 
 algorithmSensitivity1 = 0.75;

//makes the algorithm more sensitive to the stations consumption/ChannelUtilization
 algorithmSensitivity2 = 2.0;

//the threshold for avgPacket delay - assume network congestion beyond this level
// algorithmSensitivity3 = 0.150;
// algorithmSensitivity3 = 0.050;
 algorithmSensitivity3 = 0.0250;

 lastavgBAQMArrivalRateHistory = 0;
 lastavgBAQMServiceRateHistory = 0;
 avgBAQMSRHistory = 0;
 avgBAQMARHistory = 0;
 BAQMHistoryWeight = 0.2;
 BAQMHistoryCount = 0;




#ifdef TRACEME
  printf("BAQM:init: max list size: %d avgTxTime:%lf  \n",maxListSize,avgTxTime);
#endif


}


/*************************************************************
* routine:
*   int  BAQM::addElement(ListElement& element)
*
* Function: this routine inserts the ListElement to the tail of 
* the list.
*           
* inputs: 
*    ListElement& element : the element to be inserted
*
* outputs:
*  Returns a SUCCESS or FAILURE.
*        Possible failures:
*             -malloc fails
*             -list already has  > MAXLISTSIZE elements
*
***************************************************************/
int  BAQM::addElement(ListElement& element)
{
  int rc = SUCCESS;
  double curTime =  Scheduler::instance().clock();
  int upperLimit  = 0;

  if (gentleMode == 1) 
    upperLimit = 2*maxth;
  else
    upperLimit = maxth;


#ifdef TRACEME
   printf("BAQM:addElement(%lf) listsize:%d, avgQ:%3.1f, mint:%d, maxt:%d, dropP:%3.3f \n ",
      curTime,curListSize,avgQ,minth,maxth,dropP);
#endif

  Packet *p = ((packetListElement &)element).getPacket();
  struct hdr_cmn *ch = HDR_CMN(p);
  byteArrivals+=ch->size();
  BAQMByteArrivals += ch->size();
//$A605
  intervalPacketArrivals++;

  (void) updateAvgQ();

// (void)  updateAvgQLatency();
//  updateRates();
//JJM
//#ifdef TRACEME
   printf("BAQM:addElement(%lf) UPDATE AvgQ listsize:%d, avgQ:%3.1f, mint:%d, maxt:%d, dropP:%3.3f (intervalPacketArrivals:%d) \n ",
      curTime,curListSize,avgQ,minth,maxth,dropP,intervalPacketArrivals);
//#endif
  if ((minth<= avgQ) && (avgQ < upperLimit)) {
    count++;
    //update dropP;
    dropP = computeDropP();
    if ( packetDrop(dropP) == TRUE) {
      rc = FAILURE;
      intervalPacketsDropped++;
#ifdef TRACEME
        printf("BAQM:addElement(%lf)  (DROPPED1): ListID:%d, listsize:%d, avgQ:%3.1f, min-t:%d, max-t:%d, dropP:%3.3f, bytesQueued:%d, #dropped:%d (totalArrivals:%d) \n ",
          curTime,listID,curListSize,avgQ,minth,maxth,dropP, bytesQueued,intervalPacketsDropped,intervalPacketArrivals);
#endif
    }
  }
  else if (avgQ >= upperLimit) {
    rc = FAILURE;
    intervalPacketsDropped++;
#ifdef TRACEME
    printf("BAQM:addElement(%lf)  (DROPPED2): ListID:%d, listsize:%d, avgQ:%3.1f, min-t:%d, max-t:%d, dropP:%3.3f, bytesQueued:%d, #dropped:%d (totalArrivals:%d)\n ",
      curTime,listID,curListSize,avgQ,minth,maxth,dropP, bytesQueued,intervalPacketsDropped,intervalPacketArrivals);
#endif
  }
  else
    count=-1;

#ifdef TRACEME
   printf("BAQM:addElement(%lf)  rc:%d listsize:%d, avgQ:%3.1f, mint:%d, maxt:%d, dropP:%3.3f \n ",
      curTime,rc,curListSize,avgQ,minth,maxth,dropP);
#endif

  //rc is SUCCESS  indicting the pkt can be queued, else the caller should drop it
  if (rc == SUCCESS) {
    rc = ListObj::addElement(element);
    if (rc == SUCCESS) {
      Packet *p = ((packetListElement &)element).getPacket();
      struct hdr_cmn *ch = HDR_CMN(p);
      bytesQueued+=ch->size();
    }
  }
  else {
    count = 0;
  }

#ifdef TRACEME
  if (rc == SUCCESS)
    printf("BAQM:addElement(%lf) (ADDED): ListID:%d, listsize:%d, avgQ:%3.1f, min-t:%d, max-t:%d, dropP:%3.3f, bytesQueued:%d \n ",
      curTime,listID,curListSize,avgQ,minth,maxth,dropP, bytesQueued);
  else
    printf("BAQM:addElement(%lf)  (DROPPED): ListID:%d, listsize:%d, avgQ:%3.1f, min-t:%d, max-t:%d, dropP:%3.3f, bytesQueued:%d \n ",
      curTime,listID,curListSize,avgQ,minth,maxth,dropP, bytesQueued);
#endif

  return rc;
}

/*************************************************************
* routine: ListElement * ListObj::removeElement()
*
* Function:
*
* inputs: 
*
* outputs:
*   The element at the top of the list is removed and returned.
*
*   A NULL is returned if the list is empty
*
***************************************************************/
ListElement * BAQM::removeElement()
{
//  ListElement *tmpPtr = NULL;
 packetListElement *tmpPtr = NULL;
  double curTime =   Scheduler::instance().clock();
  double x = 0;

#ifdef TRACEME
  printf("BAQM::removeElement:(%lf) start size: %d (bytesQueued:%d)\n", curTime, curListSize,bytesQueued);
#endif

  tmpPtr = (packetListElement *) ListObj::removeElement();
  if (curListSize == 0) {
    q_time = curTime;
  }

  if (tmpPtr != NULL) {
    //$A602
    x = curTime - tmpPtr->packetEntryTime;
    Packet *p = ((packetListElement *)tmpPtr)->getPacket();
    struct hdr_cmn *ch = HDR_CMN(p);
    byteDepartures+=ch->size();
    BAQMByteDepartures += ch->size();
    bytesQueued-=ch->size();
    if (bytesQueued < 0) {
#ifdef TRACEME
      printf("BAQM::removeElement:(%lf) TROUBLE : bytesQueued went negative with this pkt size: %d\n", curTime, ch->size());
#endif
      bytesQueued  = 0;
    }
	//$602
    if (x > 0) {
      queueLatencyTotalDelay += x;
      queueLatencySamplesCount++;
      (void)  updateAvgQLatency();

#ifdef TRACEME 
      printf("BAQM::removePacket(%lf): ListID:%d CurListSize:%d Packet delay sample: %3.3f, queueLatencyAvg:%3.3f(#samples:%6.0f), avgPacketLatency:%3.3f \n", 
            curTime,listID, curListSize, x, queueLatencyTotalDelay,queueLatencySamplesCount,avgPacketLatency);
#endif
    }
  }
  return tmpPtr;
}



//Called only when a new packet arrives  (which might be dropped....)
void BAQM::newArrivalUpdate(Packet *p)
{

//Increment monitor counts 
  queueLatencyMonitorTotalArrivalCount++;


// We want to monitor the true arrival rate to the queue...
// so ignore when a new packet arrives ....we only
// caree when it gets queued.
  return;

  double curTime =  Scheduler::instance().clock();
  struct hdr_cmn *ch = HDR_CMN(p);
  byteArrivals+=ch->size();
  intervalPacketArrivals ++;
  BAQMByteArrivals += ch->size();
}


void BAQM::updateStats()
{
  double curTime =   Scheduler::instance().clock();

#ifdef TRACEME
  printf("BAQM:updateStats:(%lf), updated avg: %3.1f\n",curTime,avgQ);
#endif
}

void BAQM::printStatsSummary()
{
  double curTime =   Scheduler::instance().clock();

#ifdef TRACEME
  printf("BAQM:ListObj:printStatsSummary:(%lf), avgAvgQ: %3.1f, avgDropP: %3.3f\n",curTime,getAvgAvgQ(), getAvgDropP());
#endif
}


/*************************************************************
* routine: void BAQM::setAQMParams(int maxAQMSizeP, int priorityP, int minthP, int maxthP, double maxpP, double filterTimeConstant)
*
* Function: this routine adapts the AQM parameters.
*   The algorithm is TBD.
*           
* inputs: 
*      ((BAQM *)myPacketList)->setAQMParams(maxQSize,flowPriority, QParam1,QParam2, QParam3, QParam4, QParam5);
*
* outputs:
*
***************************************************************/
void BAQM::setAQMParams(int maxAQMSizeP, int priorityP, int minthP, int maxthP, int adaptiveModeP, double maxpP, double filterTimeConstantP)
{


  flowPriority = priorityP;
  MAXLISTSIZE = maxAQMSizeP;
  minth = minthP;
  maxth = maxthP;

  optimalMinth = (double )minthP;
  optimalMaxth = (double) maxthP;

  maxp = maxpP;
  filterTimeConstant = filterTimeConstantP;
  configuredMaxp = maxp;
  configuredWeightQ = weightQ;
  adaptiveMode= adaptiveModeP;
  if (adaptiveMode == 0) {
    highestMaxp = 0.5;
    alpha = maxp / 4;
    if (alpha < 0.01)
      alpha = 0.01;

    beta = 0.90;
  }
  if (adaptiveMode == 1) {
    highestMaxp = 0.90;
    lowestMaxp = 0.0;
    maxthCONFIGURED = maxth;
    minthCONFIGURED = minth;
//    highestMaxp = 0.50;
    targetDelayLevel = .175;
    alpha = .1; //increase by 10%
    if (alpha < 0.01)
      alpha = 0.01;
    beta = 0.50; //decrease by 50%
  }

  targetQueueLevel = (minth + maxth) / 2;
  if (targetQueueLevel <= minth)
    targetQueueLevel = minth+1;

//#ifdef TRACEME
  printf("BAQM:setAQMParams: minth:%d  maxth:%d  maxp:%2.2f targetQueueLevel:%d targetDelayLevel:%3.3f, filterTimeConstant:%2.4f,  priority:%d, adaptiveMode:%d,  alpha:%1.2f, beta:%1.2f \n",
          minth,maxth,maxp,targetQueueLevel,targetDelayLevel,filterTimeConstant,flowPriority, adaptiveMode, alpha, beta);
//#endif

}

//$A602
double BAQM::getAvgQLatency()
{
  return(avgPacketLatency);
}


double BAQM::getAvgQ()
{
  return(avgQ);
}

double BAQM::getAvgAvgQ()
{
  if (avgAvgQPSampleCount > 0)
    return(avgAvgQ/(double)avgAvgQPSampleCount);
  else
    return(0);
}

double BAQM::getDropP()
{
  return(dropP);
}

double BAQM::getAvgDropP()
{
  double tmpAvgDropP = 0.0;

  if (avgDropPSampleCount > 0)
    tmpAvgDropP = avgDropP / avgDropPSampleCount;
  
  return(tmpAvgDropP);
}

double BAQM::getAvgMaxP()
{
  double tmpAvgMaxP = 0.0;

  if (avgMaxPSampleCount > 0)
    tmpAvgMaxP = avgMaxP / avgMaxPSampleCount;
  
  return(tmpAvgMaxP);
}




double BAQM::updateAvgQLatency()
{
  double curTime =   Scheduler::instance().clock();
  double sampleAvg1= 0;

  if (queueLatencySamplesCount == 0)
     return avgPacketLatency;

  sampleAvg1 = queueLatencyTotalDelay / queueLatencySamplesCount;
#ifdef TRACEME
  printf("BAQM:updateAvgQLatency:(%lf) listID:%d curListSize:%d, current avgQ:%3.3f queueLatencyTotalDelay:%f, samplesCount:%f, TotalArrivalCount:%f,avgPacketLatency:%3.3f \n",
       curTime,listID,curListSize,avgQ,queueLatencyTotalDelay,queueLatencySamplesCount,queueLatencyMonitorTotalArrivalCount,avgPacketLatency);
#endif


//   avgPacketLatency  = (1-weightQ) * avgPacketLatency + weightQ * sampleAvg1;
  avgPacketLatency  = (1-filterTimeConstant) * avgPacketLatency + filterTimeConstant * sampleAvg1;

#ifdef TRACEME
  printf("BAQM:updateAvgQLatency:(%lf) listID:%d  curLen:%d, avgQ:%3.3f, sampleAvg:%3.3f, avgPacketLatency:%3.3f based on %f samples\n",
       curTime,listID,curListSize, avgQ,sampleAvg1,avgPacketLatency,queueLatencySamplesCount);
#endif

  //reset monitor counts
  queueLatencyTotalDelay = 0.0;
  queueLatencySamplesCount = 0;
  queueLatencyMonitorTotalArrivalCount = 0;

  return avgPacketLatency;

}


double BAQM::updateAvgQ()
{
  double curTime =   Scheduler::instance().clock();

#ifdef TRACEME
  printf("BAQM:updateAvgQ:(%lf)  curListSize:%d, current avgQ:%3.3f, filterTimeConstant:%2.4f, q_time:%3.6f \n",
       curTime,curListSize,avgQ,filterTimeConstant,q_time);
  fflush(stdout);
#endif


  if (curListSize == 0) {
//$A808
    double quietTime = curTime - q_time;
    double power = quietTime/avgTxTime;
    double x=  pow((1-filterTimeConstant),power);
//  avgQ = avgQ*x;
    avgQ = 0;
#ifdef TRACEME
    printf("BAQM:updateAvgQ:(%lf) case of empty queue quietTime: %6.6f, power:%3.6f, x:%3.6f, avgQ:%3.6f \n",
       curTime,quietTime,power,x,avgQ);
#endif
  }
  else {
    avgQ = (1-filterTimeConstant)*avgQ + filterTimeConstant*curListSize;
#ifdef TRACEME
    printf("BAQM:updateAvgQ:(%lf) case of queue len:%d,  updated avg:%3.3f \n",
       curTime,curListSize, avgQ);
#endif
  }
  avgAvgQ+=avgQ;
  avgAvgQPSampleCount++;


#ifdef TRACEME
  printf("BAQM:updateAvgQ:(%lf): ListID:%d  curLen:%d, avgQ:%3.3f avgAvgQ:%3.3f (filterTimeConstant:%3.3f)\n",
       curTime,listID, curListSize, avgQ, avgAvgQ/avgAvgQPSampleCount,filterTimeConstant);
#endif


  return avgQ;

}

double BAQM::updateLossRateAvg()
{
  double curTime =   Scheduler::instance().clock();
  double sampleTime = curTime - lastLossRateSampleTime;
  double LossRateSample=0;

#ifdef TRACEME
  printf("BAQM:updateLossRateAvg:(%lf)  packetsDropped:%d,   packetArrivals:%d  time since last rate sample:%f  \n", curTime,intervalPacketsDropped, intervalPacketArrivals,sampleTime);
#endif

//  if (sampleTime < updateFrequency)
//    return;

  if (intervalPacketArrivals > 0 ) {
    LossRateSample = (double)intervalPacketsDropped / (double) intervalPacketArrivals;
    avgPacketLoss = (1-filterTimeConstant)*avgPacketLoss + filterTimeConstant*LossRateSample;
#ifdef TRACEME
  printf("BAQM:updateLossRateAvg:(%lf) avgPacketLoss:%2.4f,  LossRateSample:%3.3f,  #dropped:%d, #received:%d \n ",
       curTime,avgPacketLoss,LossRateSample,intervalPacketsDropped, intervalPacketArrivals);
#endif
    intervalPacketArrivals =0;
    intervalPacketsDropped =0;
    lastLossRateSampleTime = curTime;
  }



  return avgPacketLoss;
}

void BAQM::updateRates()
{
  double curTime =   Scheduler::instance().clock();
  double sampleTime = curTime - lastRateSampleTime;
  double ArrivalRateSample=0;
  double DepartureRateSample=0;

#ifdef TRACEME
  printf("BAQM:updateRates:(%lf)  byteArrivals:%f, byteDepartures:%f, time since last rate sample:%f  \n", curTime,byteArrivals,byteDepartures,sampleTime);
#endif

//  if (sampleTime < updateFrequency)
//    return;

  if (sampleTime > 0 )
    ArrivalRateSample = byteArrivals*8/sampleTime;

  avgArrivalRate = (1-rateWeight)*avgArrivalRate + rateWeight*ArrivalRateSample;

  if (sampleTime > 0)
    DepartureRateSample = byteDepartures*8/sampleTime;

  avgServiceRate = (1-rateWeight)*avgServiceRate + rateWeight*DepartureRateSample;

#ifdef TRACEME
    printf("BAQM:updateRates:(%lf)  avgArrivalRate:%f (sample:%f),  avgServiceRate:%f(sample:%f) \n",
       curTime,avgArrivalRate,ArrivalRateSample,avgServiceRate,DepartureRateSample);
#endif

  byteArrivals =0;
  byteDepartures =0;
  lastRateSampleTime = curTime;
}

/*************************************************************
* routine: double BAQM::computeDropP()
*
* Function: this routine computes the drop probability
*    using the base RED or the gentle RED algorithm
* 
*  THe caller must properly call if using gentle mode
*           
* inputs: 
*
* outputs:
*
***************************************************************/
double BAQM::computeDropP()
{
  double pb=0;
  double pa=0;
  double curTime =   Scheduler::instance().clock();
  double y=0;

#ifdef TRACEME
  printf("BAQM:computDropP(%lf): avgQ:%2.2f, minth:%d, maxth:%d, count:%d \n", curTime,avgQ,minth,maxth,count);
#endif

  if (gentleMode == 1) {
    if (avgQ > maxth) {
      pb = maxp +  (1-maxp) *  (avgQ - (double)maxth) / (double)maxth;
    }
    else {
      pb = maxp *  (avgQ - (double)minth) / (double)(maxth-minth);
    }
  } 
  else {
    pb = maxp *  (avgQ - (double)minth) / (double)(maxth-minth);
  }

  //The count inflates the drop rate as the number of consecutive calls to compute drop increases
  y =  (1-(double)count*pb);
//#ifdef TRACEME
  printf("BAQM:computDropP(%lf): (gentle:%d) avgQ:%4.2f, minth:%d, maxth:%d, count:%d, pb:%3.4f, y=%3.4f \n",
         curTime,gentleMode,avgQ,minth,maxth,count,pb,y);
//#endif
  if (y < .00001) {
//#ifdef TRACEME
    printf("BAQM:computDropP(%lf): TROUBLE: y: %2.4f  pa:%2.4f pb:%2.4f\n",curTime,y, pa,pb);
//#endif
    y = .1;
  }
  pa = pb / y;
  if (pa > 1.0) {
//#ifdef TRACEME
    printf("BAQM:computDropP(%lf): TROUBLE: pa: %2.4f  pb:%2.4f,   count:%d,  y:%2.4f\n",curTime,pa,pb, count, y);
//#endif
    pa = 1.0;
  }

  if (count > 1000) {
//#ifdef TRACEME
    printf("BAQM:computDropP(%lf): TROUBLE: count:%d pb: %2.4f  pa:%2.4f\n",curTime,count,pb,pa);
//#endif
    count = 10;
  }

  avgDropP+= pa;
  avgDropPSampleCount++;
 
//#ifdef TRACEME
  printf("BAQM:computDropP(%lf): gentle:%d, FINAL: pb: %2.4f  pa:%2.4f, count:%d, avg:%2.4f\n",curTime,gentleMode, pb,pa,count, avgDropP/avgDropPSampleCount);
//#endif

  return pa;

}

int BAQM::packetDrop(double dropP)
{
  int dropFlag = FALSE;
  double randNumber  = Random::uniform(0,1.0);
  double curTime =  Scheduler::instance().clock();

  if (randNumber <= dropP)
    dropFlag = TRUE;

#ifdef  FILES_OK 
  if (traceFlag == 1) {
    FILE* fp = NULL;
    char traceString[32];
    char *tptr = traceString;
    sprintf(tptr,"traceAQM%d.out",listID);
    fp = fopen(tptr, "a+");
    if (avgDropPSampleCount > 0)  {
      fprintf(fp,"%lf\t%d\t%3.1f\t%d\t%2.3f\t%2.3f\t%2.3f\t%d\t%d\t%f\t%f\t%f\tdrop \n",
        Scheduler::instance().clock(),listID,avgQ, curListSize, dropP, avgDropP/avgDropPSampleCount,maxp,dropFlag, bytesQueued, avgArrivalRate, avgServiceRate, avgPacketLatency);
    }
    else {
      fprintf(fp,"%lf\t%d\t%3.1f\t%d\t%2.3f\t%2.3f\t%2.3f\t%d\t%d\t%f\t%f\t%f\tdrop \n",
        Scheduler::instance().clock(),listID,avgQ, curListSize, dropP, 0,maxp,dropFlag, bytesQueued, avgArrivalRate, avgServiceRate, avgPacketLatency);
    }
    fclose(fp);
  }
#endif
  
#ifdef TRACEME
  printf("BAQM:packetDrop(%lf): uniform:%2.2f, dropP:  %2.2f dropFlag:%d \n",curTime,randNumber,dropP,dropFlag);
#endif
  return dropFlag;

}


/*************************************************************
* routine:
*  void BAQM::channelStatus(double accessDelayMeasure, double channelUtilizationMeasure, double consumptionMeasure)
*
* Function: this method is perioodicaly called to inform the AQM of current channel conditions. 
*        Based on this information, the routine might adapt its AQM operating settings.  
*        Two levels of adaptation are active:
*            -timescales of updateFrequency : update the Adaptive RED maxp variable
*            -timescales of BAQMAdaptationFrequency : update the adaptive BAQM  variables
* Inputs: 
*  double accessDelayMeasure
*  double channelUtilizationMeasure
*  double consumptionMeasure
*
* outputs:
*
*  This method will possible update the following:
*      -The minth or maxth range
*      - maxp
*      -
*****************************************************************/
void BAQM::channelStatus(double accessDelayMeasure, double channelUtilizationMeasure, double consumptionMeasure)
{
  double curTime =   Scheduler::instance().clock();
  double sampleTime = curTime - BAQMLastSampleTime;
  double adaptationSampleTime = curTime - BAQMLastAdaptationTime;
  double ArrivalRateSample=0;
  double DepartureRateSample=0;
  double rateGrowth = 0.0;
  double intensityLevel = 0.0;
  int    adjustmentCode = 0;




  //update the adaptation specific monitors
  avgBAQMAccessDelay = (1-BAQMWeight)*avgBAQMAccessDelay + BAQMWeight*accessDelayMeasure;
  avgBAQMConsumption = (1-BAQMWeight)*avgBAQMConsumption + BAQMWeight*consumptionMeasure;
  avgBAQMChannelUtilization = (1-BAQMWeight)*avgBAQMChannelUtilization + BAQMWeight*channelUtilizationMeasure;

  intensityLevel = algorithmSensitivity2 * (avgBAQMConsumption / avgBAQMChannelUtilization);

  if (intensityLevel > 1.0)
    intensityLevel = 1.0;

  //But don't do anything else unless the stabilizing time has been reached
  if (sampleTime < updateFrequency)
    return;

  //continue to use the RED Queue average...

  if (sampleTime > 0 )
    ArrivalRateSample = BAQMByteArrivals*8/sampleTime;

  avgBAQMArrivalRate = (1-BAQMRateWeight)*avgBAQMArrivalRate + BAQMRateWeight*ArrivalRateSample;
  if (avgBAQMArrivalRate > maxBAQMArrivalRate)
    maxBAQMArrivalRate = avgBAQMArrivalRate;

  if (sampleTime > 0)
    DepartureRateSample = BAQMByteDepartures*8/sampleTime;

  avgBAQMServiceRate = (1-BAQMRateWeight)*avgBAQMServiceRate + BAQMRateWeight*DepartureRateSample;
  if (avgBAQMServiceRate > maxBAQMServiceRate)
    maxBAQMServiceRate = avgBAQMServiceRate;

#ifdef TRACEME
    printf("BAQM:channelStatus:(%lf) (listID:%d)  BAQMByteArrivals:%f, BAQMByteDepartures:%f, time since last rate sample:%f  \n", 
         curTime,listID,BAQMByteArrivals,BAQMByteDepartures,sampleTime);
    printf("BAQM:channelStatus:(%lf)  avgBAQMArrivalRate:%f (sample:%f),  avgBAQMServiceRate:%f(sample:%f) \n",
       curTime,avgBAQMArrivalRate,ArrivalRateSample,avgBAQMServiceRate,DepartureRateSample);
#endif

  BAQMByteArrivals =0;
  BAQMByteDepartures =0;
  BAQMLastSampleTime = curTime;

//  avgQ = updateAvgQ();
//  (void) updateAvgQLatency();

  updateRates();
//$A605
  (void) updateLossRateAvg();


  rateGrowth = serviceRateHistory(avgBAQMServiceRate, avgBAQMArrivalRate);

  if (adaptiveMode == 0) {
#ifdef TRACEME
    printf("BAQM:channelStatus:(%lf):(listID:%d) adaptive mode: %d, avgPacketLatency:%3.3f,  avgQ:%3.3f, avgLR:%2.2f,minth:%d, maxth:%d, maxp:%2.3f , targetQueueLevel:%d, new state:%d, rateGrowth:%3.3f,intensityLevel:%3.3f \n", 
          curTime, listID, adaptiveMode, avgPacketLatency, avgQ, avgPacketLoss, minth, maxth, maxp, targetQueueLevel, stateFlag,rateGrowth,intensityLevel);
#endif

    if ((avgQ > targetQueueLevel) && (maxp <=highestMaxp)) {
      //increase maxp;
        maxp = maxp + alpha;
        if (maxp > highestMaxp)
           maxp  = highestMaxp;
    }
    else if ((avgQ < targetQueueLevel) && (maxp >= 0.01)) {
      //decrease maxp;
        maxp = maxp * beta;
    }
    else if ((avgPacketLatency > algorithmSensitivity3) && (maxp >= 0.01)) {
      //increase maxp;
        maxp = maxp + alpha;
    }
   if (maxp > highestMaxp)
     maxp = highestMaxp;

#ifdef TRACEME
    printf("BAQM:channelStatus:(%lf):(listID:%d)  Updated:  avgQ:%3.3f, minth:%d, maxth:%d, maxp:%2.3f   \n", 
          curTime, listID, avgQ, minth, maxth, maxp);
#endif


#ifdef TRACEME
 printf("BAQM:chanelStatus(%lf): accessDelayM:%3.3f, channelUtilizationM:%3.3f, consumptionMeasure:%3.3f \n",curTime,accessDelayMeasure, channelUtilizationMeasure,consumptionMeasure);
 printf("BAQM:chanelStatus(%lf): AVGaccessDelayM:%3.3f, AVGchannelUtilizationM:%3.3f, AVGconsumptionMeasure:%3.3f, avgPacketLatency:%3.3f \n",curTime,avgBAQMAccessDelay, avgBAQMChannelUtilization,avgBAQMConsumption,avgPacketLatency);
#endif

    if ((adaptiveMode ==0) && (adaptationSampleTime >= BAQMAdaptationFrequency)) {
      stateFlag = adjustState(channelUtilizationMeasure,avgPacketLatency);
#ifdef TRACEME
      printf("BAQM:channelStatus:(%lf):(listID:%d) adaptive mode: %d avgQ:%3.3f, minth:%d, maxth:%d, maxp:%2.3f , targetQueueLevel:%d , new state:%d, rateGrowth:%3.3f,intensityLevel:%3.3f \n", 
          curTime, listID, adaptiveMode, avgQ, minth, maxth, maxp, targetQueueLevel, stateFlag,rateGrowth,intensityLevel);
#endif

    //Now the BAQM extensions
      switch(stateFlag) {

        case 0:
          adjustmentCode = -1;
          printf("BAQM:channelStatus:(%lf): ERROR:  NOT INITIALIZED !! \n", curTime);
          exit(0);
          break;

        //no congestion, decrease congestion avoidance 
        //hold at stable point
        case 1:
          adjustmentCode = 1;
          adaptAQMParams(MOVE_TO_OPTIMAL, intensityLevel);
          break;


        //local congestion
        //if serviceRate is growing adapt params 
        case 2:
          adjustmentCode = 2;
          intensityLevel = algorithmSensitivity2 * (avgBAQMConsumption / avgBAQMChannelUtilization);
          //This causes oscillation about the knee....
          //So if the service rate is growing, increase queue range
          if (rateGrowth > .005)
             adaptAQMParams(INCREASE_AQM_QUEUE, intensityLevel);
          else
             adaptAQMParams(DECREASE_AQM_QUEUE, intensityLevel);
          break;


        //network wide congestion
        case 3:
          adjustmentCode = 3;
          //adjust the intensityLevel for the avgPacketLatency
          intensityLevel = intensityLevel + (avgPacketLatency);
          adaptAQMParams(INCREASE_CONGESTION_AVOIDANCE, intensityLevel );
          break;


        default:
          adjustmentCode = 4;
          printf("BAQM:channelStatus:(%lf): ERROR:  BAD STATE VALUE: %d !! \n", curTime,stateFlag);
          exit(0);
         break;
      }
#ifdef TRACEME
      printf("BAQM:channelStatus:(%lf): (listID:%d) Updated params: state:%d, BAQMRateWeight:%3.3f,  minth:%d, maxth:%d, maxp:%2.3f intensityLevel:%3.3f, avgPacketLatency:%3.3f avgQ:%3.3f \n", 
          curTime, listID, stateFlag, BAQMRateWeight, minth,maxth, maxp, intensityLevel, avgPacketLatency, avgQ);
#endif

      BAQMLastAdaptationTime = curTime;
    }

  }

//delay based CA 
  if (adaptiveMode == 1) {

    if ((avgPacketLatency > targetDelayLevel) && (maxp <=0.50)) {
      //increase maxp;
      maxp = maxp + alpha;
      if (maxp > highestMaxp)
        maxp = highestMaxp;
    }
    else if ((avgPacketLatency > targetDelayLevel) && (maxp <=highestMaxp)) {
      //lower minth/maxth
      minth = minth - (minth/2);
      if (minth < 2)
        minth = 2;
      if (minth > minthCONFIGURED)
        minth = minthCONFIGURED;

      maxth = maxth - (maxth/2);
      if ( (maxth - minth) < 4)
         maxth = minth+4;
      if (maxth > maxthCONFIGURED)
        maxth = maxthCONFIGURED;
    
      if(minth < 8) {
        //increase maxp;
        maxp = maxp + alpha;
        if (maxp > highestMaxp)
          maxp = highestMaxp;
      }
    }
    else if ((avgPacketLatency < (targetDelayLevel)) && 
             (avgPacketLatency > (targetDelayLevel/2))) {
      //decrease maxp;
      maxp = maxp * beta;
      if (maxp < lowestMaxp)
        maxp = lowestMaxp;
      if (maxp < 0.50) {
        minth = minth + (minth/8);
        if (minth > minthCONFIGURED)
          minth = minthCONFIGURED;
        maxth = maxth + (maxth/8);
        if (maxth > maxthCONFIGURED)
          maxth = maxthCONFIGURED;
      }
    }
#ifdef TRACEME
    printf("BAQM:channelStatus:(%lf):(listID:%d) adaptive mode: %d, avgPacketLatency:%3.3f (target1:%3.3f, target2:%3.3f),  avgQ:%3.3f, avgLR:%3.3f, minth:%d, maxth:%d, maxp:%2.3f , targetQueueLevel:%d, avgPDelay:%3.3f, new state:%d, rateGrowth:%3.3f,intensityLevel:%3.3f \n", 
          curTime, listID, adaptiveMode, avgPacketLatency, targetDelayLevel, (targetDelayLevel/2), avgQ, avgPacketLoss, minth, maxth, maxp, targetQueueLevel, avgPacketLatency, stateFlag,rateGrowth,intensityLevel);
#endif
  }
#ifdef  FILES_OK 
  if (traceFlag == 1) {
    FILE* fp = NULL;
    double x = 0;
    fp = fopen("traceBAQM.out", "a+");
    if (avgDropPSampleCount > 0)  
        x= avgDropP/avgDropPSampleCount;

 //   fprintf(fp,"%lf\t%d\t%3.1f\t\t%d\t%3.3f\t%2.3f\t%2.3f\t%2.2f\n", 
 //     Scheduler::instance().clock(),listID,avgQ, curListSize,avgPacketLatency, dropP,avgPacketLoss,maxp);

    fprintf(fp,"%lf\t%d\t%3.1f\t\t%d\t%3.3f\t%2.3f\t%2.3f,\t%2.3f\t%d\t%f\t%f\t%3.6f\t%3.6f\t%3.6f\t%2.3f\t%d\t%d\t%d\t%d\t%3.3f\t%3.3f\n",
      Scheduler::instance().clock(),listID,avgQ, curListSize,avgPacketLatency, dropP,avgPacketLoss, x,bytesQueued, 
      avgBAQMArrivalRate, avgBAQMServiceRate, accessDelayMeasure, channelUtilizationMeasure,consumptionMeasure,
      maxp,targetQueueLevel,adjustmentCode,minth,maxth,highestMaxp,intensityLevel);
    

    fclose(fp);
  }
#endif

#ifdef  FILES_OK 
  if (traceFlag == 1) {
    FILE* fp = NULL;
    char traceString[32];
    char *tptr = traceString;
    sprintf(tptr,"traceAQM%d.out",listID);
    fp = fopen(tptr, "a+");
    if (avgDropPSampleCount > 0)  {
      fprintf(fp,"%lf\t%d\t%3.1f\t%d\t%2.3f\t%2.3f\t%2.3f\t%d\t%d\t%f\t%f\t%f\tstatus \n",
          Scheduler::instance().clock(),listID,avgQ, curListSize, dropP, avgDropP/avgDropPSampleCount,maxp,0, bytesQueued, avgArrivalRate, avgServiceRate, avgPacketLatency);
    }
    else
    {
      fprintf(fp,"%lf\t%d\t%3.1f\t%d\t%2.3f\t%2.3f\t%2.3f\t%d\t%d\t%f\t%f\t%f\tstatus \n",
          Scheduler::instance().clock(),listID,avgQ, curListSize, dropP, 0.0 ,maxp,0, bytesQueued, avgArrivalRate, avgServiceRate, avgPacketLatency);
    }
    fclose(fp);
  }
#endif

  

}


void BAQM::channelIdleEvent()
{
  return;
}



/*************************************************************
* routine: void BAQM::adaptAQMParams()
*
* Function: this routine adapts the AQM parameters.
*           
* inputs: 
*   int reactFlag
*   double sensitivityLevel
*
* outputs:
*   Adapts the following:
*     highestMaxp : maximum drop probability
*     minth,maxth : maximum drop probability
  highestMaxp = 0.5;
  maxp = 0.5;
  weightQ = 0.01;
  minth = 4;
  maxth = maxAQMSize / 2;
  targetQueueLevel = (minth + maxth) / 2;
  if (targetQueueLevel <= minth)
    targetQueueLevel = minth+1;

#define RETURN_TO_DEFAULT 0
#define INCREASE_AQM_QUEUE 1
#define DECREASE_AQM_QUEUE 2
#define INCREASE_CONGESTION_AVOIDANCE 3
#define DECREASE_CONGESTION_AVOIDANCE 4
#define MOVE_TO_OPTIMAL 5
*
***************************************************************/
void BAQM::adaptAQMParams(int reactFlag, double intensityLevel)
{
  double curTime =   Scheduler::instance().clock();
  double x;
  int y;
  double oldHighestMaxP = highestMaxp;

#ifdef TRACEME
  printf("BAQM:adaptAQMParams(%f): (listID:%d)  reactFlag:%d, intensityLevel:%2.2f  \n",curTime,listID,reactFlag,intensityLevel);
#endif
    switch(reactFlag) {
      case(RETURN_TO_DEFAULT):
        resetAQMParams();
        break;
      //additive increase
      case(INCREASE_AQM_QUEUE):
        if (maxth < MAXLISTSIZE) {
          minth += 1;
          maxth += 1;
          targetQueueLevel = (minth + maxth) / 2;
          if (targetQueueLevel <= minth)
            targetQueueLevel = minth+1;
        }

        break;
      //multiplicative decrease
      case(DECREASE_AQM_QUEUE):
        if (minth> 1) {
          x = (double) minth * 0.5;
          minth = (int) x;
          if (minth < 1) 
            minth = 1;

          x = (double) maxth * 0.5;
          maxth = (int) x;

          if ((maxth - minth) < 3)
                maxth = minth + 3;

          targetQueueLevel = (minth + maxth) / 2;
          if (targetQueueLevel <= minth)
            targetQueueLevel = minth+1;
        }

        break;

      case(INCREASE_CONGESTION_AVOIDANCE):
        highestMaxp += (intensityLevel * highestMaxp);
        if (highestMaxp >= 1.0) {
          highestMaxp = 1.0;

          //if can't increase maxp, lower the AQM minth/maxth setting
          if (minth> 1) {
            x = (double) minth * 0.5;
            minth = (int) x;
            if (minth < 1) 
              minth = 1;

            x = (double) maxth * 0.5;
            maxth = (int) x;

          if ((maxth - minth) < 3)
                maxth = minth + 3;

            targetQueueLevel = (minth + maxth) / 2;
            if (targetQueueLevel <= minth)
              targetQueueLevel = minth+1;
          }


        }

        break;

      case(DECREASE_CONGESTION_AVOIDANCE):
        highestMaxp -= (1-intensityLevel) * highestMaxp;
        if (highestMaxp < .01)
          highestMaxp = .01;
        break;

      //
      case(MOVE_TO_OPTIMAL):
          x = (double) minth + 0.05*(double) (optimalMinth-minth);
          minth = (int) x;
          if (minth < 1) 
            minth = 1;

          x = (double) maxth + 0.05*(double)(optimalMaxth-maxth);
          maxth = (int) x;
          if (maxth > MAXLISTSIZE)
              maxth = MAXLISTSIZE;

          if ((maxth - minth) < 3)
                maxth = minth + 3;
              
          targetQueueLevel = (minth + maxth) / 2;
          if (targetQueueLevel <= minth)
            targetQueueLevel = minth+1;

          x = highestMaxp + 0.5 * (configuredMaxp-highestMaxp);
          highestMaxp = x;

#ifdef TRACEME
  printf("BAQM:adaptAQMParams(%f): (listID:%d)  MOVE_TO_OPTIMAL: minth:%d, optimalminth:%d,  maxth:%d, optimalmaxth:%d highestMaxp:%3.3f  \n",
      curTime,listID, minth,optimalMinth,maxth,optimalMaxth,highestMaxp);
#endif

        break;

      default:
       break;
    }

#ifdef TRACEME
    printf("BAQM:adaptAQM:(%lf): (listID:%d) avgQ:%3.1f, targetQueueLevel:%d, minth:%d (optimal:%4.0f), maxth:%d (optimal:%4.0f), dropP:%3.3f, maxp:%3.3f, highestMaxp:%3.3f, intensityLevel:%3.3f \n", 
          curTime, listID,avgQ, targetQueueLevel, minth,optimalMinth, maxth, optimalMaxth, dropP, maxp, highestMaxp, intensityLevel);
#endif
}

/*************************************************************
* routine: int BAQM::adjustState()
*
* Function: this method computes and returns the new BAQM state
*           
* inputs: 
*
* outputs:
*    int state:  a valid BAQM state
*
***************************************************************/
int BAQM::adjustState(double channelUtilization, double packetDelay)
{
  double curTime =   Scheduler::instance().clock();
  int newState=1;


//If the avgQ is < 1 AND channelUtilization < threshold  assume no congestion
if ((avgQ < 1) && (channelUtilization < algorithmSensitivity1)) {
  newState = 1;
  //If previous state was network congestion
  if (stateFlag == 3) {
    optimalMinth = (1-BAQMWeight) *  optimalMinth + (BAQMWeight) * minth;
    optimalMaxth = (1-BAQMWeight)* optimalMaxth + (BAQMWeight) * maxth;
  }
}

//Local congestion: 
//If the avgQ is > minth  AND channelUtilization < threshold  assume no congestion
if ((avgQ > minth) && (channelUtilization < algorithmSensitivity1)) {
  newState = 2;
}
  
//Network congestion: 
//If the avgQ is > minth  AND channelUtilization > threshold  assume no congestion
if ((packetDelay > algorithmSensitivity3) && (channelUtilization > algorithmSensitivity1)) {
//if ((avgQ > minth) && (channelUtilization > algorithmSensitivity1)) {
  newState = 3;
}

#ifdef TRACEME
  printf("BAQM:adjustState(%f): (listID:%d)  updated state: %d, channelUtilization:%3.3f,pktDelay:%3.3f  minth:%d  maxth:%d  maxp:%2.2f optimalMinth:%4.0f, optimalmaxth:%4.0f \n",
        curTime,listID,newState,channelUtilization,packetDelay,minth,maxth,maxp,optimalMinth,optimalMaxth);
#endif

  return newState;
}

/*************************************************************
* routine: double BAQM::levelOfMice()
*
* Function: this method returns a measure that estimates how many 
*          low bandwidth BE flows are active.
*           
* inputs: 
*
* outputs:
*    double levelofMice :  ranges 0 to 1.0.  0 implies there are
*          NO mice,  1: implies there are mice.
*
***************************************************************/
double BAQM::levelOfMice()
{

  double levelOfMiceFlows = 0;

#ifdef TRACEME
  printf("BAQM:levelOfMice: level of Mice Service Flows ::%2.2f \n",levelOfMiceFlows);
#endif
  
  return levelOfMiceFlows;
}

/*************************************************************
* routine: double BAQM::serviceRateHistory()
*
* Function: this method returns a measure that
*    captures the level of service rate increase or decrease
*           
* inputs: 
*
* outputs:
*    double serviceRateHistory:  this value ranges from [-1.0, 1.0]. 
*      A -1.0 indicates the service rate is rapidly decreasing
*      A +1.0 indicates the service rate is rapidly increasing
*
***************************************************************/
double BAQM::serviceRateHistory(double avgBAQMServiceRate, double  avgBAQMArrivalRate)
{
  double curTime =   Scheduler::instance().clock();
  double serviceRateHistory = 0;
  double serviceRateDifference=0;

  avgBAQMSRHistory = (1-BAQMHistoryWeight)*avgBAQMSRHistory + BAQMHistoryWeight*avgBAQMServiceRate;
  avgBAQMARHistory = (1-BAQMHistoryWeight)*avgBAQMARHistory + BAQMHistoryWeight*avgBAQMArrivalRate;
  BAQMHistoryCount++;

  serviceRateDifference= (avgBAQMSRHistory - lastavgBAQMServiceRateHistory);

  serviceRateHistory= serviceRateDifference / maxBAQMServiceRate;


  avgServiceRateHistory = (1-BAQMHistoryWeight)*avgServiceRateHistory + BAQMHistoryWeight*serviceRateHistory;

  //Every 5 samples, remember it
  if (BAQMHistoryCount > 5) {
    BAQMHistoryCount=0;
    lastavgBAQMServiceRateHistory = avgBAQMSRHistory;
    lastavgBAQMArrivalRateHistory = avgBAQMARHistory;
  }

#ifdef TRACEME
    printf("BAQM:serviceRateHistory:(%lf): (listID:%d) avgSRParam:%f, avgSRHistory:%f, lastavgBAQMSRH:%f,  avgARParam:%f, avgARHistory:%f, lastavgBAQMARH:%f \n", 
        curTime, listID,avgBAQMServiceRate,avgBAQMSRHistory,lastavgBAQMServiceRateHistory,avgBAQMArrivalRate,avgBAQMARHistory,lastavgBAQMArrivalRateHistory);
    printf("BAQM:serviceRateHistory:(%lf): (listID:%d) difference:%f, serviceRateHistory:%3.3f, avgServiceRateHistory:%3.3f  \n", 
        curTime,listID, serviceRateDifference,serviceRateHistory, avgServiceRateHistory);
#endif

  return avgServiceRateHistory;
}






#ifndef ns_mac_docsisDebug_h
#define ns_mac_docsisDebug_h


//Define this is want a trace of all data arrivals and acks that are sent
//Note: Don't have this on AND the RXSIDESNUMACKTRACE- it's one or the other
//#define SENDSIDESNUMACKTRACE 0
#define RXSIDESNUMACKTRACE 0
//$A900
//#define SNUMACKTRACED_CX 17888
#define SNUMACKTRACED2_CX  17889
#define SNUMACKTRACED_CX 1
//#define SNUMACKTRACED2_CX  601
#define TCP_SEG_SIZE 1460

//This is a short trace to do timings or
// debug nasty problems
//#define TRACE_DEBUG 0

//#define ALL_DEBUG 0
//#define  FILES_OK 1

//Turn on selective traces
#ifndef ALL_DEBUG
//#define  FILES_OK 1
//#define TRACEME 0
//#define TRACEMEFILE 0


//tcp-sink
//#define TRACESINK 1
//tcp.cc, tcp-reno.cc, tcp-sack1.cc
//#define TRACETCP 0
//#define TCPRENO 0
//#define TRACENEWRENO 0
//#define TRACED_CX 222222
#define TRACED_CX 1

#endif

//Turn on ALL traces
#ifdef ALL_DEBUG
//Undefine the following to create all the data files to be created
#define  FILES_OK 1
#define TRACEME 0
#define TRACEMEFILE 0

//udp-sink.cc
#define TRACEUDPSINK 0
//tcp-sink
#define TRACESINK 1
//tcp.cc, tcp-reno.cc, tcp-sack1.cc
#define TRACETCP 0
#define TCPRENO 0
#define TRACENEWRENO 0
#define TRACED_CX 13

//For mac-docsisbase.cc
#define TRACE 0
//For the cmts
#define TRACE_CMTS 0
//For mac-docsis-state-machine.cc



#endif

#define DEBUG 0
#ifdef DEBUG
        #define print_flag 1
#else
        #define print_flag 0
#endif

#define print_flag 1

#define dprintfm(args...) do { \
	        if (print_flag==1) printf(args); \
	        } while (0)

#define eprintfm(...)  do { \
	        printf(## __VA_ARGS__); \
	        } while (0)

#ifdef DEBUG_COUNTER
        #define print_flag_counter 1
#else
        #define print_flag_counter 0
#endif

#define dprintfc(args...) do { \
	        if (print_flag_counter==1) printf(args); \
	        } while (0)

#define eprintfc(...)  do { \
	        printf(## __VA_ARGS__); \
	        } while (0)

#endif /* __mac_docsisDebug_h__ */


/************************************************************************
* File:  PIEAQM.h
*
* Purpose:
*  This module implements  PIE AQM
*
* Revisions:
*
*  3/31/2014: This code was portedfrom the ns2 pie 
*  contribution from Cisco P. Natarajan, R. Pan, C. Piglione)
*
*
***********************************************************************/
#ifndef PIEAQM_h
#define PIEAQM_h

#include "ListObj.h"
#include "packetListElement.h"


/*
 * Early drop parameters, supplied by user
 */
struct edp_pie {
	/*
	 * User supplied.
	 */
	int mean_pktsize;	/* avg pkt size, linked into Tcl */
	int setbit;		/* true to set congestion indication bit */
	double a, b;		/* parameters to pie controller */
	double tUpdate;		/*sampling timer*/
	double qdelay_ref;	/* desired queue delay in ms */
	double mark_p;		/* when p < mark_p, mark chosen packets */
				/* when p > mark_p, drop chosen packets */
	int use_mark_p;		/* use mark_p only for deciding when to drop, */
				/* 	if queue is not full */
	int bytes; 		/* if this flag is set drop probability depends on pkt size */				
};

/*
 * Early drop variables, maintained by PIE
 */
struct edv_pie {
	double v_prob;	/* prob. of packet drop before "count". */
	int count;		/* # of packets since last drop */
	int count_bytes;	/* # of bytes since last drop */
	double qdelay_old;
};


class PIEAQM : public ListObj {

public:
  virtual ~PIEAQM();
  PIEAQM();
  virtual void initList(int maxAQMSize);
  virtual void setAQMParams(int maxAQMSize, int priority, int minth, int maxth, int adaptiveMode, double maxp, double weightQ);
  virtual int addElement(ListElement& element);
  virtual ListElement *removeElement();    //dequeue and return
  virtual void adaptAQMParams();
  virtual void newArrivalUpdate(Packet *p);
  virtual void updateStats();
  virtual void printStatsSummary();
  virtual double getAvgQ();
  virtual double getAvgAvgQ();
  virtual double getAvgAvgQLatency();
  virtual double getAvgAvgQLoss();
  virtual double getDropP();
  virtual double getAvgDropP();
  virtual double getAvgMaxP();

  virtual void channelIdleEvent();
  virtual void channelStatus(double accessDelayMeasure, double channelUtilizationMeasure, double consumptionMeasure);

  int maxth;
  int minth;
  double maxp;
  double weightQ;
  //define the base time constant to use for averaging
  double filterTimeConstant;
  int flowPriority;


  double lastUpdateTime;

  double numberIncrements;
  double numberDecrements;
  double numberTotalChannelIdleEvents;
  double numberValidChannelIdleEvents;

  double queueLatencyTotalDelay;
  double queueLatencySamplesCount;
  double queueLatencyMonitorTotalArrivalCount;
  double avgPacketLatency;
  double avgAvgQLatency;
  double avgAvgQLatencySampleCount;
  double lastQLatencySampleTime;

  double avgQLoss;
  double avgQLossCounter;
  double avgQLossSampleCounter;
  double lastQLossSampleTime;

  double avgAvgQLoss;
  double avgAvgQLossSampleCounter;

  double avgQ;
  double dropP;
  double q_time;
  double avgDropP;
  double avgDropPSampleCount;
  double avgAvgQ;
  double avgAvgQPSampleCount;
  double avgMaxP;
  double avgMaxPSampleCount;

//Real codel stuff
   // Static state (user supplied parameters)
    double target_;         // target queue size (in time, same units as clock)
    double interval_;       // width of moving time window over which to compute min

    // Dynamic state used by algorithm
    double first_above_time_; // when we went (or will go) continuously above
                              // target for interval
    double drop_next_;      // time to drop next packet (or when we dropped last)
    int count_;             // how many drops we've done since the last time
                            // we entered dropping state.
    int dropping_;          // = 1 if in dropping state.
    int maxpacket_;         // largest packet we've seen so far (this should be
                            // the link's MTU but that's not available in NS)

    int curq_;        // current qlen seen by arrivals
    double d_exp_;    // delay seen by most recently dequeued packet
    int drop_now_;    //flag indicates next packet enqueued should be dropped (unless queue is almost empty)

    double numberDrops;
    double numberPackets;


protected:


private:

  //0 is FCFS, 1 is PIEAQM 
  int adaptiveMode;
  double avgTxTime;
  double accessDelayMeasure;
  double channelUtilizationMeasure;
   double consumptionMeasure;


  //states:  0 : NOT initialized
  //states:  1 : REGION 1
  //states:  2 : REGION 2
  //states:  3 : REGION 3
  int stateFlag;
  double updateFrequency;
  int bytesQueued;
  double avgServiceRate;
  double byteArrivals;
  double byteDepartures;
  double lastRateSampleTime;
  double lastMonitorSampleTime;
  double rateWeight;
  double avgArrivalRate;

  double PIEAQMupdateFrequency;
  int    PIEAQMUpdateCountThreshold;
  int    PIEAQMUpdateCount;
  double PIEAQMLastSampleTime;
  double PIEAQMByteArrivals;
  double PIEAQMByteDepartures;
  double avgPIEAQMArrivalRate;
  double avgPIEAQMServiceRate;
  double PIEAQMRateWeight;

  double PIEAQMQueueDelaySamples;
  double PIEAQMQueueLevelSamples;
  double PIEAQMAccessDelaySamples;
  double PIEAQMConsumptionSamples;
  double PIEAQMChannelUtilizationSamples;


  double updateAvgQLoss();
  double updateAvgQ();
  void updateRates();
  double updateAvgQLatency();
  void  updateMonitors();
 
  void reset();
  int  doDropIn(Packet *pktPtr);
  int  doDropOut(Packet *pktPtr);

//PIE stuff

  double update_dq_rate(int pkt_size);   
  int drop_early(Packet* pkt);
  double calculate_p();
  edp_pie edp_;           /* Config params */
  edv_pie edv_;           /* PIE State variables */
  double lastProbUpdateTime;

  int dq_count;           /*number of bytes departed since current measuremeent cycle starts*/
  double dq_start;        /*the start timestamp of current measurement cycle*/
  double burst_allowance; /*current max burst size that is allowed before random drops kick in*/
  int dq_threshold;       /*threshold that needs to be across before a sample of the dequeue rate is measured */
  double max_burst;       /*maximum burst allowed before random earl*/
  double avg_dq_rate;     /*time averaged dequeue rate*/
  double tmp_rate;
  int qib_;               /*boolean: q measured i bytes ?*/
};


#endif

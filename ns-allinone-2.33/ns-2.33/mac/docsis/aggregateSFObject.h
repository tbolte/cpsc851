/***************************************************************************
 * Module: aggregateSFObject
 *
 * Explanation:
 *   This file contains the class definition of an aggregate service flow object.
 *   While a serviceFlowObject packaged all necessary config and state information
 *   for a single flow (including its queue), a aggregateSFObject packages state
 *   and config needed to support a set of flows that are to be treated as a single flow
 *   from a bandwidth scheduling perspective.
 * 
 *
 * Revisions:
 *
 *  TODO:
 *   -Move the rate control support into this object
 *
************************************************************************/

#ifndef ns_aggregateSFObject_h
#define ns_aggregateSFObject_h

#include "serviceFlowObject.h"



/*************************************************************************
 ************************************************************************/
class aggregateSFObject : public DSserviceFlowObject
{
public:
  aggregateSFObject();

  virtual ~aggregateSFObject();

  void constructionInit();
  void init(int directionParam, int aggregateFlowIDParam, MacDocsis *myMacParam, serviceFlowMgr *mySFMgr,  bondingGroupObject *myBGObjParam);

  void setQueueBehavior(int maxQSize, int QType, int priority, int QParam1, int QParam2, int QParam3, double QParam4, double QParam5);
  int match(Packet* p);
  int matchMACAddress(Packet* p, int direction);
  int matchAddress(Packet* p);
  int matchAddress(Packet* p, int direction);
  int matchPktType(Packet* p);

  void channelIdleEvent();
  void channelStatus(double avgChannelAccesDelay, double avgTotalSlotUtilization, double avgMySlotUtilization);

  int addPacket(Packet *p);
  Packet *removePacket();
  int packetsQueued(void);


private:
  bondingGroupObject *myBGObj;

};



#endif /* __ns_aggregateSFObject_h__ */

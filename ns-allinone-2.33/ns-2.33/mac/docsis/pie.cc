/* -*-	Mode:C++; c-basic-offset:8; tab-width:8; indent-tabs-mode:t -*- */
/*
 * Proportional Integral Controller - Enhanced (PIE) AQM Implementation
 *
 * Authors of the code:
 * Preethi Natarajan (prenatar@cisco.com)
 * Rong Pan (ropan@cisco.com)
 * Chiara Piglione (cpiglion@cisco.com)


 * Copyright (c) 2013, Cisco Systems, Inc.  
 * All rights reserved.
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following
 * conditions are met:
 *   -	Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *   -	Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer
 *      in the documentation and/or other materials provided with the distribution.
 *   -	Neither the name of Cisco Systems, Inc. nor the names of its contributors may be used to endorse or promote products derived 
 *      from this software without specific prior written permission.
 * ALL MATERIALS ARE PROVIDED BY CISCO AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, 
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, AND NONINFRINGEMENT ARE DISCLAIMED.  IN NO EVENT SHALL
 * CISCO OR ANY CONTRIBUTOR BE LIABLE FOR ANY DIRECT, INDIRECT, SPECIAL, EXEMPLARY, CONSEQUENTIAL, OR INCIDENTAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGES. 
 *
 *  
 */

#include <math.h>
#include <sys/types.h>
#include "config.h"
#include "template.h"
#include "random.h"
#include "flags.h"
#include "delay.h"
#include "pie.h"

static class PIEClass : public TclClass {
public:
	PIEClass() : TclClass("Queue/PIE") {}
	TclObject* create(int argc, const char*const* argv) {
		if (argc==5) 
			return (new PIEQueue(argv[4]));
		else
			return (new PIEQueue("Drop"));
	}
} class_pie;


PIEQueue::PIEQueue(const char * trace) : CalcTimer(this), link_(NULL), q_(NULL),
	qib_(0), de_drop_(NULL), EDTrace(NULL), tchan_(0), curq_(0),
	edp_(), edv_(), dq_count(-1), dq_threshold(0), burst_allowance(0.0), max_burst(0.0), avg_dq_rate(0.0)
{
	if (strlen(trace) >=20) {
		printf("trace type too long - allocate more space to traceType in pie.h and recompile\n");
		exit(0);
	}
	strcpy(traceType, trace);

	bind_bool("bytes_", &edp_.bytes);
	bind_bool("queue_in_bytes_", &qib_);	    // boolean: q in bytes?
	bind("a_", &edp_.a);		
	bind("b_", &edp_.b);	   
	bind_time("tUpdate_", &edp_.tUpdate);		  	  
	bind ("qdelay_ref_", &edp_.qdelay_ref);     
	bind("mean_pktsize_", &edp_.mean_pktsize);  // avg pkt size
	bind_bool("setbit_", &edp_.setbit);	    // mark instead of drop
	bind("prob_", &edv_.v_prob);		    // drop probability
	bind("curq_", &curq_);			    // current queue size
	bind("mark_p_", &edp_.mark_p);
	bind_bool("use_mark_p_", &edp_.use_mark_p);
	bind("dq_threshold_",&dq_threshold);
	
	bind("burst_allowance_",&max_burst);
	burst_allowance = max_burst;
	q_ = new PacketQueue();			    // underlying queue
	pq_ = q_;
	reset();
}

void PIEQueue::reset()
{
	//double now = Scheduler::instance().clock();
	edv_.count = 0;
	edv_.count_bytes = 0;
	edv_.v_prob = 0;
	curq_ = 0;

	calculate_p();
	Queue::reset();
}


void PIEQueue::enque(Packet* pkt)
{
	double now = Scheduler::instance().clock();
	hdr_cmn* ch = hdr_cmn::access(pkt);
	++edv_.count;
	edv_.count_bytes += ch->size();

	int droptype = DTYPE_NONE;

	int qlen = qib_ ? q_->byteLength() : q_->length();
	curq_ = qlen;	// helps to trace queue during arrival, if enabled

	int qlim = qib_ ? (qlim_ * edp_.mean_pktsize) : qlim_;

	if (qlen >= qlim) {
		droptype = DTYPE_FORCED;
	}
	else {
		if (drop_early(pkt, qlen)) {
			droptype = DTYPE_UNFORCED;
		} else {
			droptype = DTYPE_NONE;
		}
	}

	if (droptype == DTYPE_UNFORCED) {
		Packet *pkt_to_drop = pickPacketForECN(pkt);
		if (pkt_to_drop != pkt) {
			q_->enque(pkt);
			q_->remove(pkt_to_drop);
			pkt = pkt_to_drop; /* XXX okay because pkt is not needed anymore */
		}

		if (de_drop_ != NULL) {
			if (EDTrace != NULL) 
				((Trace *)EDTrace)->recvOnly(pkt);
			de_drop_->recv(pkt);
		}
		else {
			drop(pkt);
		}
	} else {
		q_->enque(pkt);
		if (droptype == DTYPE_FORCED) {
			pkt = pickPacketToDrop();
			q_->remove(pkt);
			drop(pkt);
			edv_.count = 0;
			edv_.count_bytes = 0;
		}
	}
	return;
}

double PIEQueue::calculate_p()
{
	double now = Scheduler::instance().clock();
	double p;
	double qdelay;
	int qlen = qib_ ? q_->byteLength() : q_->length();

	qdelay = (avg_dq_rate > 0) ? qlen/avg_dq_rate : 0.0;
	// in light dropping mode, take gentle steps; in medium dropping mode, take
  	// medium steps; in high dropping mode, take big steps.	
	if (edv_.v_prob < 0.01) {
		p=edp_.a*(qdelay-edp_.qdelay_ref)/8+edp_.b/8*(qdelay-edv_.qdelay_old)+edv_.v_prob;
	} else if (edv_.v_prob < 0.1) {
		p=edp_.a*(qdelay-edp_.qdelay_ref)/2+edp_.b/2*(qdelay-edv_.qdelay_old)+edv_.v_prob;
	} else {
                double tmp = edp_.a*(qdelay-edp_.qdelay_ref)+edp_.b*(qdelay-edv_.qdelay_old);
                if (tmp < 0.02)
                  p=tmp+edv_.v_prob;
                else {
                  p=0.02+edv_.v_prob;
		}
        }

        /* for non-linear drop in prob */
        if ((qdelay == 0) && (edv_.qdelay_old == 0)) {
                p = p *0.98;
        } else if (qdelay > 0.25) {
                p = p + 0.02;
	}

	if (p < 0) p = 0;
	if (p > 1) p = 1;

	//printf("%f Q: %p Qlen: %d deq_count: %d Avg_deq_Count: %lf arv_count: %d Avg_arv_count: %lf Qdelay: %lf Drop_prob: %f avg_dq_time %f\n", now, this, qlen, edv_.deque_count, edv_.avg_deque_count, edv_.arv_count, edv_.avg_arv_count, qdelay,  p, avg_dq_time); 

	edv_.v_prob = p;
	// if the system is out of congestion when the queuing delay is well below
	// the target delay that the drop probability reaches zero. In that case, reset
	// parameters.
	if( (qdelay < (0.5*edp_.qdelay_ref)) && (edv_.qdelay_old < (0.5*edp_.qdelay_ref) ) && (edv_.v_prob == 0) ) {
		dq_count = -1;
		avg_dq_rate = 0.0;
		burst_allowance = max_burst;
	} 
		
	edv_.qdelay_old = qdelay;

	CalcTimer.resched(edp_.tUpdate);	
	return p;
}

int PIEQueue::drop_early(Packet* pkt, int qlen)
{
	double now = Scheduler::instance().clock();
	hdr_cmn* ch = hdr_cmn::access(pkt);
	double p = edv_.v_prob;

	if (edp_.bytes) {
		p = p*ch->size()/edp_.mean_pktsize;
		if (p > 1) p = 1; 
	}
	
	/* if there is still burst_allowance left, skip random early drop.
	 * In addition, do not drop if queue delay is less than 50% of ref queue delay and
	 * calculated drop prob is < 0.2. Or if queue Length is less than 2 packets. This is to ensure good link utilization. 
         * incremental, not essential optimization */
	if ( (burst_allowance > 0) || 
	     ((edv_.qdelay_old < (0.5*edp_.qdelay_ref) && (edv_.v_prob < 0.2))) || (( qlen <= 2*edp_.mean_pktsize) && qib_ ) || ( qlen <= 2 && !qib_ ) ) {	     
			return(0);
	} 
	double u = Random::uniform();
	if (u <= p) {
		edv_.count = 0;
		edv_.count_bytes = 0;
		hdr_flags* hf = hdr_flags::access(pickPacketForECN(pkt));

		/* PN: Drop or mark ??
		 * If use_mark_p set to true and prob < mark_p then drop
		 * else mark
		 * This logic is similar to RED queue
		 */

		if (edp_.setbit && hf->ect() &&
			(!edp_.use_mark_p || (p < edp_.mark_p))) {
			hf->ce() = 1; 	// mark Congestion Experienced bit
			return (0);	// no drop
		} else {
			return (1);	// drop
		}
	}
	return (0);			// no DROP/mark
}

Packet* PIEQueue::pickPacketForECN(Packet* pkt)
{
	return pkt; /* pick the packet that just arrived */
}

Packet* PIEQueue::pickPacketToDrop() 
{
	int victim;
	victim = q_->length() - 1;
	return(q_->lookup(victim)); 
}

Packet* PIEQueue::deque()
{
	Packet *p;
	p = q_->deque();
	double now = Scheduler::instance().clock();
	int pkt_size = (p != NULL) ? hdr_cmn::access(p)->size() : 0;

	if ( qib_ ){
		// if not in a measurement cycle and the queue has built up to dq_threshold,
		// start the measurement cycle
		if ( (q_->byteLength() >= (dq_threshold*edp_.mean_pktsize)) && (dq_count == -1) ) {
		   dq_start = now;
		   dq_count = 0;
	   	}

	  	if (dq_count != -1) {
		   dq_count += pkt_size;
		   // done with a measurement cycle
		   if (dq_count >= (dq_threshold*edp_.mean_pktsize)) {
			   double tmp = now - dq_start;
			   // time averaging deque rate, start off with the
			   // current value, otherwise, average it.
			   if (avg_dq_rate == 0) avg_dq_rate = dq_count/tmp;
			   else avg_dq_rate = 0.9*avg_dq_rate+0.1*dq_count/tmp;

			   // restart a measurement cycle if there is enough data
			   if (q_->byteLength() > (dq_threshold*edp_.mean_pktsize)) {
				dq_start = now;
				dq_count = 0;
			   } else {
				dq_count = -1;
			   }

			   // update burst allowance
			   if (burst_allowance > 0) burst_allowance -= tmp;

		   }
	   	}
	} else {
		// if not in a measurement cycle and the queue has built up to dq_threshold,
		// start the measurement cycle	
		if ( (q_->length() >= dq_threshold) && (dq_count == -1) ) {
		   dq_start = now;
		   dq_count = 0;
	   	}

		//in a measurement cycle
	   	if (dq_count != -1) {
		   dq_count ++;
		   // done with a measurement cycle
		   if (dq_count >= dq_threshold) {
			   double tmp = now - dq_start;
			   // time averaging deque rate, start off with the
			   // current value, otherwise, average it.
			   if (avg_dq_rate == 0) avg_dq_rate = dq_count/tmp;
			   else avg_dq_rate = 0.9*avg_dq_rate+0.1*dq_count/tmp;

			   // restart a measurement cycle if there is enough data
			   if (q_->length() > dq_threshold) {
				dq_start = now;
				dq_count = 0;
			   } else {
				dq_count = -1;
			   }

			   // update burst allowance
			   if (burst_allowance > 0) burst_allowance -= tmp;

		   }

	   	}
	}

	curq_ = qib_ ? q_->byteLength() : q_->length(); // helps to trace queue during arrival, if enabled
	return (p);
}

int PIEQueue::command(int argc, const char*const* argv)
{
	Tcl& tcl = Tcl::instance();
	if (argc == 2) {
		if (strcmp(argv[1], "reset") == 0) {
			reset();
			return (TCL_OK);
		}
		if (strcmp(argv[1], "early-drop-target") == 0) {
			if (de_drop_ != NULL)
				tcl.resultf("%s", de_drop_->name());
			return (TCL_OK);
		}
		if (strcmp(argv[1], "edrop-trace") == 0) {
			if (EDTrace != NULL) {
				tcl.resultf("%s", EDTrace->name());
			}
			else {
				tcl.resultf("0");
			}
			return (TCL_OK);
		}
		if (strcmp(argv[1], "trace-type") == 0) {
			tcl.resultf("%s", traceType);
			return (TCL_OK);
		}
	} 
	else if (argc == 3) {
		// attach a file for variable tracing
		if (strcmp(argv[1], "attach") == 0) {
			int mode;
			const char* id = argv[2];
			tchan_ = Tcl_GetChannel(tcl.interp(), (char*)id, &mode);
			if (tchan_ == 0) {
				tcl.resultf("PIE: trace: can't attach %s for writing", id);
				return (TCL_ERROR);
			}
			return (TCL_OK);
		}
		// tell PIE about link stats
		if (strcmp(argv[1], "link") == 0) {
			LinkDelay* del = (LinkDelay*)TclObject::lookup(argv[2]);
			if (del == 0) {
				tcl.resultf("PIE: no LinkDelay object %s", argv[2]);
				return(TCL_ERROR);
			}
			link_ = del;
			return (TCL_OK);
		}
		if (strcmp(argv[1], "early-drop-target") == 0) {
			NsObject* p = (NsObject*)TclObject::lookup(argv[2]);
			if (p == 0) {
				tcl.resultf("no object %s", argv[2]);
				return (TCL_ERROR);
			}
			de_drop_ = p;
			return (TCL_OK);
		}
		if (strcmp(argv[1], "edrop-trace") == 0) {
			NsObject * t  = (NsObject *)TclObject::lookup(argv[2]);
			if (t == 0) {
				tcl.resultf("no object %s", argv[2]);
				return (TCL_ERROR);
			}
			EDTrace = t;
			return (TCL_OK);
		}
		if (!strcmp(argv[1], "packetqueue-attach")) {
			delete q_;
			if (!(q_ = (PacketQueue*) TclObject::lookup(argv[2])))
				return (TCL_ERROR);
			else {
				pq_ = q_;
				return (TCL_OK);
			}
		}
	}
	return (Queue::command(argc, argv));
}

void PIEQueue::trace(TracedVar* v)
{
	char wrk[500];
	const char *p;

	if (((p = strstr(v->name(), "prob")) == NULL) &&
	    ((p = strstr(v->name(), "curq")) == NULL)) {
		fprintf(stderr, "PIE:unknown trace var %s\n", v->name());
		return;
	}
	if (tchan_) {
		int n;
		double t = Scheduler::instance().clock();
		// XXX: be compatible with nsv1 PI trace entries
		if (*p == 'c') {
			sprintf(wrk, "Q %g %d", t, int(*((TracedInt*) v)));
		} else {
			sprintf(wrk, "%c %g %g", *p, t, double(*((TracedDouble*) v)));
		}
		n = strlen(wrk);
		wrk[n] = '\n'; 
		wrk[n+1] = 0;
		(void)Tcl_Write(tchan_, wrk, n+1);
	}
	return; 
}

void PIECalcTimer::expire(Event *)
{
	a_->calculate_p();
}

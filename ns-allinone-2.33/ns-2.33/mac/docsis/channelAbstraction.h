/***************************************************************************
 * Module: channelAbstraction
 *
 * Explanation:
 *   This file contains the channel abstraction object.  This object
 *   abstracts a communications channel.
 *
 * Objects:
 *
 * Revisions:
 *
************************************************************************/


#include <stdio.h>
#include "packet.h"
#include "NodeList.h"
#include "hdr-docsis.h"
#include "channelStats.h"
#include "channelProperties.h"

#ifndef ns_channel_abstraction_h
#define ns_channel_abstraction_h

#define CHANNEL_MODE_HALF_DUPLEX 1
#define CHANNEL_MODE_DUPLEX 0

#define COLLISION_DETECTED 1
#define NO_COLLISION_DETECTED 0

#define CHANNEL_IDLE 0
#define CHANNEL_RECV 1
#define CHANNEL_SEND 2

#define CHANNEL_DIRECTION_DOWN 0
#define CHANNEL_DIRECTION_UP 1
#define CHANNEL_DIRECTION_DUPLEX 2


class channelAbstraction
{
	
public:
  channelAbstraction();
  ~channelAbstraction();
  channelAbstraction(int);
  
  void init(int);
  void sendUp(Packet *); 
  void setNodeCount(int); 
  
  int getCollisionState();
  void setCollisionState();
  inline void incrCollisionVar() { collision_++; }
  void clearCollisionState();
  
  double TX_Time(Packet* pkt);	

  int getTxState();
  void setTxStateSEND();
  void setTxStateIDLE();

  int getRxState();
  void setRxStateRECV();
  void setRxStateIDLE();

  int getDirection();
  void setDirection(int);
  void setDirectionDS();
  void setDirectionUS();
  void setDirectionDUPLEX();

  double getChannelUtilization();
  double getUtilizationMonitorSample();
  
//#JIT002
  void setChannelProperties(int chID, int direction,u_int32_t  dataRate, double propDelay,
		          u_int32_t OHBytes,int FECOverhead, u_int32_t ticksPerMinislot, u_int32_t maxBurst, double lossRate);

  void getChannelProperties(channelPropertiesType *channelProperties);
  
  NodeList mySendDownNodeList; 
  NodeList mySendUpNodeList; 

  struct channelStatsType myStats; 
  struct channelPropertiesType myProperties;
  
  private:
    int rx_state_;	
    int tx_state_;	
	//JJM WRONG
    double rx_state_counterBUSY;
    double rx_state_counterIDLE;
    double tx_state_counterBUSY;
    double tx_state_counterIDLE;

    int channelNumber_;
    int collision_;       /* Indicate whether collision occured */
    int direction_;
	int delay_;
	double utilizationMonitorBytes;
	double utilizationMonitorLastSampleTime;

};

#endif /* __ns_channel_abstraction_h__ */

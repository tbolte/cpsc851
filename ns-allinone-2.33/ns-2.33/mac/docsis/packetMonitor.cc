/***************************************************************************
 * Module: packetMonitor 
 * 
 * Explanation:
 *   This file contains the base class packetMonitor.
 *   The monitor maintains two types of stats.  
 *       Long Term stats:  the counters are never cleared.
 *       Running stats:  the idea is that an external program can periodically
 *             obtain the current count and then can clear the counters.
 *
 * Methods:
 *
 *
 * Revisions:
 *
 * TODO:  change all packetTrace methods to use
 *     int packetTrace(Packet *p, int numberID, char *traceOutput, int outputSize)
 *    to obtain the higher layer details (above the DOCSIS layer information)
 *
************************************************************************/
#include "packetMonitor.h"

//Comment this  out to remove all printfs that are not covered by the traceLevel
#include "docsisDebug.h"
//#define TRACEME 0


packetMonitor::packetMonitor()
{
  state = 0;
  traceLevel = 0;
  textFile = NULL;
  binaryFile = NULL;
  direction = PT_DUPLEX_DIRECTION;
  numberID  = 0;
  init();
}

packetMonitor::packetMonitor(int directionParam)
{
  state = 0;
  traceLevel = 0;
  textFile = NULL;
  binaryFile = NULL;
  direction = directionParam;
  numberID  = 0;
  init();
}

packetMonitor::packetMonitor(int direction, u_int32_t traceLevelParam, char *tFile, char *bFile)
{
  state = 0;
  traceLevel = traceLevelParam;
  textFile = fopen(tFile, "w+");
  binaryFile = fopen(bFile, "w+");
  direction = PT_DUPLEX_DIRECTION;
  numberID  = 0;
  init();
}

packetMonitor::~packetMonitor()
{
  fclose(textFile);
  fclose(binaryFile);
}


void packetMonitor::init(int id, int directionParam)
{
  direction = directionParam;
  numberID = id;
  init();
}

/***********************************************************************
*function: 
*
*explanation:
*
*inputs:
*
*outputs:
************************************************************************/
void packetMonitor::init()
{
#ifdef TRACEME
  printf("packetMonitor::init()(%lf): init packetMonitor numberID:%d\n", Scheduler::instance().clock(),numberID);
#endif
  myStats.byteCount = 0;
  myStats.packetCount = 0;
  myStats.runningByteCount = 0;
  myStats.runningPacketCount = 0;
  binaryByteCount = 0;
  binaryCount = 0;

  state = 1;
  traceLevel = packetMonitor_NO_TRACE;
}

/***********************************************************************
*function: 
*
*explanation:
*
*inputs:
*
*outputs:
************************************************************************/
void packetMonitor::setTraceLevel(u_int32_t level)
{

  traceLevel = (traceLevel | level);

#ifdef TRACEME
  printf("packetMonitor::setTraceLevel()(%lf): update with %d, new level: %d \n", Scheduler::instance().clock(),level,traceLevel);
#endif

}


/***********************************************************************
*function: 
*
*explanation:
*
*inputs:
*
*outputs:
************************************************************************/
void packetMonitor::setTextTraceFile(char *tFileName)
{
#ifdef TRACEME
  printf("packetMonitor::setTextTraceFile()(%lf): set to %s\n", Scheduler::instance().clock(),tFileName);
#endif


  if (textFile != NULL)
    fclose(textFile);

  textFile = fopen(tFileName, "w+");
}

/***********************************************************************
*function: 
*
*explanation:
*
*inputs:
*
*outputs:
************************************************************************/
void packetMonitor::setBinaryTraceFile(char *bFileName)
{
#ifdef TRACEME
  printf("packetMonitor::setBInaryTraceFile()(%lf): set to %s\n", Scheduler::instance().clock(),bFileName);
#endif
  if (binaryFile != NULL)
    fclose(binaryFile);
  binaryFile = fopen(bFileName, "w+");

}

void packetMonitor::printStatsSummary()
{
  printf("packetMonitor:StatsTraceSummary:  byteCount:%f, packetCount:%d\n",
         myStats.byteCount, myStats.packetCount);

}


void packetMonitor::getStats(struct packetStatsType *callersStats)
{
  *callersStats = myStats;
}

void packetMonitor::clearStats()
{
  myStats.byteCount = 0;
  myStats.packetCount = 0;
  myStats.runningByteCount = 0;
  myStats.runningPacketCount = 0;
}

void packetMonitor::updateStats(Packet *p)
{
  int rc = 0;
  struct hdr_cmn* ch = HDR_CMN(p);        /* Common header part */
  int  framesize = ch->size();

  myStats.byteCount+= framesize;
  myStats.packetCount++;
  myStats.runningByteCount+= framesize;
  myStats.runningPacketCount++;
}


void packetMonitor::clearRunningStats()
{
  myStats.runningByteCount = 0;
  myStats.runningPacketCount = 0;
}


/***********************************************************************
*function: int packetTrace(Packet *p, int numberID,char *traceOutput, int outputSize);
*
*explanation:
*  This creates a text trace line and returns it in a string
*
*inputs:
*   Packet *p
*   int numberID
*   char *traceOutput
*  
*
*outputs:
*  Returns a 0 on success, a 1 on error
*
************************************************************************/
int packetMonitor::packetTrace(Packet *p,char *traceOutput, int outputSize)
{
  char traceString[TRACE_STRING_LENGTH];
  char *tptr = traceString;
  int i;

  int rc = 0;
  struct hdr_cmn* ch = HDR_CMN(p);        /* Common header part */
  hdr_tcp *tcph = hdr_tcp::access(p);     /* TCP header */
  hdr_rtp* rh = hdr_rtp::access(p);


  hdr_ping *pingh = hdr_ping::access(p);  /* Ping header */
  hdr_arp *ah = HDR_ARP(p);               /* arp hdr if it exists */
  hdr_loss_mon *lossmonh = hdr_loss_mon::access(p);  /* Ping header */
  hdr_ip *iph = hdr_ip::access(p);        /* IP header */

  u_int16_t flow_id;

int * data = (int *)p->accessdata();
int  framesize = ch->size();

int  sizeApplicationData = 0;

  if((ch->ptype_ == PT_TCP) || (ch->ptype_ == PT_ACK) || (ch->ptype_ == PT_FTP) || (ch->ptype_ == PT_HTTP))
  {
      sizeApplicationData =  framesize - IP_HDR_LEN - tcph->hlen();
  }
  else
  {
      sizeApplicationData =  framesize - IP_HDR_LEN;
  }

  if (sizeApplicationData < 0 )
    sizeApplicationData = 0;

  //make the ascii trace to either standard out or the text file
  if((ch->ptype_ == PT_TCP) || (ch->ptype_ == PT_ACK) || (ch->ptype_ == PT_FTP) || (ch->ptype_ == PT_HTTP))
  {
	    sprintf(tptr,"%d, %lf, %d, DATA, %d, TCP, %d, %d, %d, %d, %d, %d, %d, %d\n",
		    numberID,Scheduler::instance().clock(), ch->size(),  framesize,	
		    iph->saddr(), iph->sport(), iph->daddr(), iph->dport(),
		    tcph->seqno(), tcph->ackno(),  sizeApplicationData, iph->flowid());
  }
  else if(ch->ptype_ == PT_ACK)
  {
	    sprintf(tptr,"%d, %lf, %d, DATA, %d, TCP-ACK, %d, %d, %d, %d, %d, %d, %d, %d\n",
		    numberID,Scheduler::instance().clock(), ch->size(),  framesize, 
		    iph->saddr(), iph->sport(), iph->daddr(), iph->dport(),
		    tcph->seqno(), tcph->ackno(),  sizeApplicationData,iph->flowid());
  }
  else if(ch->ptype_ == PT_UDP)/* UDP */
  {
	    sprintf(tptr,"%d, %lf, %d, DATA, %d, UDP, %d, %d, %d, %d, %d, %d\n",
		    numberID,Scheduler::instance().clock(), ch->size(),  framesize,	
		    iph->saddr(), iph->sport(), iph->daddr(), iph->dport(),
		     sizeApplicationData,iph->flowid());
  }
      
  else if(ch->ptype_ == PT_PING)/* PING */
  {
	    sprintf(tptr,"%d, %lf, %d, DATA, %d, ICMP, %d, %d, %d, %d, %d, %d\n",
		    numberID, Scheduler::instance().clock(), ch->size(),  framesize,	
		    iph->saddr(), iph->sport(), iph->daddr(), iph->dport(),
		    pingh->snum,  sizeApplicationData);
  }
  else if(ch->ptype_ == PT_LOSS_MON)/* LOSS MONITOR PING */
  {
	    sprintf(tptr,"%d, %lf,  %d, DATA, %d, LOSS_MON, %d, %d, %d, %d, %d, %d, %f, %d\n",
		    numberID,Scheduler::instance().clock(), ch->size(),  framesize,	
		    iph->saddr(), iph->sport(), iph->daddr(), iph->dport(),
		    lossmonh->snum, lossmonh->ret, lossmonh->send_time,sizeApplicationData);
  }
  else if(ch->ptype_ == PT_CBR)/* CBR-UDP */
  {
	    sprintf(tptr,"%d, %lf, %d, DATA, %d, CBR, %d, %d, %d, %d, %d, %d\n",
		    numberID, Scheduler::instance().clock(),  ch->size(),  framesize,	
		    iph->saddr(), iph->sport(), iph->daddr(), iph->dport(),
		     sizeApplicationData,rh->seqno());

  }
  else if(ch->ptype_ == PT_ARP)/* if ARP */
  {
            sprintf(tptr,"%d, %lf, %d, ARP, op: %x, sha: %x, tha: %x, spa: %x, tpa: %x, %d, %d\n",
	        numberID, Scheduler::instance().clock(), ch->size(), 
		ah->arp_op, ah->arp_sha, ah->arp_tha, ah->arp_spa, ah->arp_tpa,
		 framesize, ch->ptype_); 
  }
  else if(ch->ptype_ == PT_MAC)/* CBR-UDP */
  {
	    sprintf(tptr, "%d, %lf, %d, MAC, %d, %d\n", 
		    numberID, Scheduler::instance().clock(), ch->size(),
		     framesize, ch->ptype_); 
  }
  else // OTHER
  {
	    sprintf(tptr, "%d, %lf, %d, OTHER, %d, %d\n", 
		    numberID, Scheduler::instance().clock(), ch->size(), 
		     framesize, ch->ptype_); 
  }

  int bufferSize = strlen(tptr);
  //Finally, copy our trace buffer string to the caller's buffer
  if (bufferSize > outputSize)
    bufferSize = outputSize;

  for (i=0;i<bufferSize;i++)
  {
    traceOutput[i] = tptr[i];
  }
  traceOutput[i] =  0x00;
  bufferSize = strlen(traceOutput);
#ifdef TRACEME
  printf("packetMonitor::packetTrace(%lf):(id:%d): message(size:%d):  %s  \n", 
        Scheduler::instance().clock(),numberID, bufferSize,traceOutput);
#endif

  return rc;

}



/***********************************************************************
*function: int packetMonitor::packetTrace(Packet *p)
*
*explanation:
*  This traces the packet to either standard out, an output file
*  or a binary file depending on the trace level.
*
*inputs:
*
*outputs:
************************************************************************/
int packetMonitor::packetTrace(Packet *p)
{

  int rc = 0;
  char traceString[TRACE_STRING_LENGTH];
  FILE *fDS  = NULL;

  if (traceLevel == 0)
    return 0;

  if ((traceLevel & 0x01) != 0)
    fDS = stdout;
  if ( (traceLevel & 0x02) != 0)
    fDS = textFile;

  if ( (traceLevel & 0x04) != 0)
    //make the binary trace file entry
	binaryPacketTrace(p);


  packetTrace(p,traceString,TRACE_STRING_LENGTH);
  fprintf(fDS, "%s", traceString); 

#ifdef TRACEME
  printf("packetMonitor::packetTrace(%lf): %s", Scheduler::instance().clock(),traceString);
#endif

  return rc;
}



int packetMonitor::packetTrace(Packet *p, char *preText)
{

}

void packetMonitor::binaryPacketTrace(Packet *p)
{
struct PREC_T pktRecord;
struct hdr_cmn* ch = HDR_CMN(p);        /* Common header part */
hdr_tcp *tcph = hdr_tcp::access(p);     /* TCP header */
hdr_ip *iph = hdr_ip::access(p);        /* IP header */

  pktRecord.tstamp = Scheduler::instance().clock();
  pktRecord.len = ch->size();
  pktRecord.packet_t = ch->ptype();

  pktRecord.direction = direction;

  pktRecord.saddr = iph->saddr();
  pktRecord.daddr = iph->daddr();
		
  if((ch->ptype_  == PT_ACK) || (ch->ptype_ == PT_TCP) || (ch->ptype_ == PT_FTP) || (ch->ptype_ == PT_HTTP))
  {
    pktRecord.seq = tcph->seqno();
    pktRecord.ack = tcph->ackno();
  }
  else
  {
    pktRecord.seq = 0;
    pktRecord.ack = 0;
  }

  pktRecord.off_win = 0;
  pktRecord.tcp_flags = 0;
  pktRecord.seglen = 0;
  pktRecord.snd_una = 0;
  pktRecord.snd_nxt  = 0;
  pktRecord.rcv_nxt = 0;
  pktRecord.snd_wnd = 0;
  pktRecord.snd_cwnd = 0;
 
  binaryByteCount+= pktRecord.len;
  binaryCount++;
#ifdef TRACEME
  printf("packetMonitor::binaryPacketTrace(%lf): LOG tstamp:%lf,pkt len:%u, pkt #:%u (count%d,  byteCOunt:%u \n", Scheduler::instance().clock(),pktRecord.tstamp,
     pktRecord.len,pktRecord.direction,binaryCount, binaryByteCount);
#endif

  fwrite(&pktRecord,sizeof(struct PREC_T),1,binaryFile);
}

void packetMonitor::getBinaryTraceSummary()
{
struct PREC_T pktRecord;
int num = 1;
int n_values = 0;
int count  = 0;
double numberBytes=0;
u_int32_t PT_TCPCounter=0;
u_int32_t PT_FTPCounter = 0;
u_int32_t PT_HTTPCounter = 0;
u_int32_t PT_TCPACKCounter=0;
u_int32_t PT_UDPCounter=0;
u_int32_t PT_PINGCounter = 0;
u_int32_t PT_LOSS_MONCounter = 0;
u_int32_t PT_CBRCounter= 0;
u_int32_t PT_ARPCounter= 0;
u_int32_t PT_MACCounter=0;
u_int32_t PT_DOCSISCounter=0; 
u_int32_t PT_OTHERCounter=0;

u_int32_t SEND_DIRECTION_COUNT =0;
u_int32_t RECV_DIRECTION_COUNT =0;

  if (binaryFile == NULL) {
#ifdef TRACEME
    printf("packetMonitor::getBinaryTraceSummary(%lf):NO binaryFile setup \n",Scheduler::instance().clock());
#endif
    return;
  }

fseek(binaryFile, 0L, SEEK_SET);
long tmp = ftell(binaryFile);
#ifdef TRACEME
  printf("packetMonitor::getBinaryTraceSummary(%lf):(ftell 2:%f \n", Scheduler::instance().clock(),(double) tmp);
#endif

  //algorithm
  //loop through the file and get a record at a time - first pass just count number of elements
  n_values = fread(&pktRecord,sizeof(struct PREC_T),1,binaryFile);
#ifdef TRACEME
  printf("packetMonitor::getBinaryTraceSummary(%lf): n_value: %d (size of block:%d)  \n", Scheduler::instance().clock(),n_values,sizeof(struct PREC_T));
#endif
  while (!(feof(binaryFile)) && (n_values > 0)) {
    count++;
    numberBytes = numberBytes + (double) pktRecord.len;
    n_values = fread(&pktRecord,sizeof(struct PREC_T),1,binaryFile);

    if (pktRecord.direction == PT_SEND_DIRECTION)
      SEND_DIRECTION_COUNT++;
    else
      RECV_DIRECTION_COUNT++;

	switch (pktRecord.packet_t) {
      case PT_TCP:
		  PT_TCPCounter++;
		  break;
      case PT_FTP:
		  PT_FTPCounter++;
		  break;
      case PT_HTTP:
		  PT_HTTPCounter++;
		  break;
      case PT_ACK:
		  PT_TCPACKCounter++;
		  break;
      case PT_UDP:
		  PT_UDPCounter++;
		  break;
      case PT_PING:
		  PT_PINGCounter++;
		  break;
      case PT_LOSS_MON:
		  PT_LOSS_MONCounter++;
		  break;
      case PT_CBR:
		  PT_CBRCounter++;
		  break;
      case PT_MAC:
		  PT_MACCounter++;
		  break;
      case PT_DOCSIS:
		  PT_DOCSISCounter++;
		  break;
     default:
		  PT_OTHERCounter++;
	}
#ifdef TRACEME
//  printf("packetMonitor::getBinaryTraceSummary(%lf): n_value: %d , packet number:%u, packet size:%u \n", Scheduler::instance().clock(),n_values,
//		       pktRecord.direction,pktRecord.len);
#endif
  }
//#ifdef TRACEME
  printf("packetMonitor::getBinaryTraceSummary(%lf):#pkts:%d   total Bytes:%f \n", Scheduler::instance().clock(),count, numberBytes);
  printf("packetMonitor::getBinaryTraceSummary(%lf):#pkts:%d (#SENT:%d, #RECEIVED:%d)   total Bytes:%f \n", Scheduler::instance().clock(),count, 
		       SEND_DIRECTION_COUNT, RECV_DIRECTION_COUNT, numberBytes);
  printf("PT_TCP:%u, PT_TCPACK:%u, PT_FTP:%u, PT_HTTP:%u, PT_UDP:%u, PT_PING:%u, PT_LOSS_MON:%u, PT_CBR:%u, PT_ARP:%u, PT_MAC:%u,  PT_DOCSIS:%u, PT_OTHER:%u \n",
		   PT_TCPCounter, PT_TCPACKCounter,PT_FTPCounter, PT_HTTPCounter, PT_UDPCounter, 
           PT_PINGCounter,PT_LOSS_MONCounter,PT_CBRCounter, PT_ARPCounter,PT_MACCounter,PT_DOCSISCounter,PT_OTHERCounter);
//#endif


return;

}



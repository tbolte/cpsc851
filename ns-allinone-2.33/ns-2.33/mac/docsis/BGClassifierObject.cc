/***************************************************************************
 * Module: BGClassifierObject 
 *
 * Explanation:
 *   This file contains the class definition of an object 
 *   responsible for mapping SFs to Aggregate Flows. 
 *   The base class provides a single FCFS queue.
 * 
 *  This file contains the following subclass classifiers:
 *  BGClassifierObjectHash : hashed SFs into one of a number of buckets
 *  BGClassifierObjectPriority: maps SFs to aggregate queues based on priority rules
 *
 * Methods:
 *
 *
 * Revisions:
 *   $A906 :v1 of FAIRSHARE AQM
 *   $A910 :v1 of BROADBAND AQM
 *
 *
************************************************************************/
#include "BGClassifierObject.h"

#include <stdio.h>
#include <strings.h>
#include <errno.h>
#include "globalDefines.h"
#include "ListObj.h"
#include "packet.h"
#include "bondingGroupObject.h"
#include "serviceFlowObject.h" 
#include "aggregateSFObject.h" 

//Comment this  out to remove all printfs that are not covered by the traceLevel
#include "docsisDebug.h"
//#define TRACEME 0

//#define SHOW_QUEUES 0
#define QUEUE_MONITOR_START 18 
#define  QUEUE_MONITOR_STOP 21


/***********************************************************************
*function: BGClassifierObject::BGClassifierObject(): BGClassifierObject()
*
*explanation:
*
*inputs:
*
*outputs:
*
************************************************************************/
BGClassifierObject::BGClassifierObject() 
{
  bzero((void *)&myStats,sizeof(struct BGClassifierStatsType));
  myMap = NULL;
  myBGObject = NULL;
  myBGID = -1;
  schedMode = FCFS;
  flowMAPSize = 0;
  maxSizeMAP = 256;
#ifdef TRACEME
  printf("BGClassifierObject::constructor()(%lf): constructed a new  BGClassifierObject \n", Scheduler::instance().clock());
#endif
}

BGClassifierObject::~BGClassifierObject()
{
}

void BGClassifierObject::init(int myBGIDParam, int maxSizeMAPParam, int schedModeParam, bondingGroupObject *myBGObjectParam)
{

  myBGID = myBGIDParam;
  schedMode = schedModeParam;
  myBGObject = myBGObjectParam;
  maxSizeMAP = maxSizeMAPParam;

#ifdef TRACEME
  printf("BGClassifierObject::init()(%lf): init BGClassifierObject ID:%d, sizeMAP:%d, numberAggregateQueues:%d \n", Scheduler::instance().clock(),myBGID,maxSizeMAP,myBGObject->numberAggregateQueues);
#endif
}

int BGClassifierObject::adjustFlowMap(int opCode)
{
  int rc = SUCCESS;
#ifdef TRACEME
  printf("BGClassifierObject::adjustFlowMap)(%lf): init BGClassifierObject ID:%d, sizeMAP:%d, numberAggregateQueues:%d \n", Scheduler::instance().clock(),myBGID,maxSizeMAP,myBGObject->numberAggregateQueues);
#endif
  return rc;
}


/***********************************************************************
*function: int BGClassifierObject::initFlowMap(char *fileName)
*
*explanation:
*  This method is called to create the flow map. 
*
*   This base  class assumes a single FCFS queue. So each SFid
*   will be mapped to a single aggregate flow queue.
*
*inputs:
*   char *fileName : specifies the input file that holds data.  Sometimes
*     this is not needed.  For example, the base class knows mapping
*     and so the fileName is assumed to be NULL.
*
*     If it is not NULL, we do read and use the file contents.
*
*outputs:
*
************************************************************************/
int BGClassifierObject::initFlowMap(char *fileName)
{
  int rc = SUCCESS;
  int i;

  //warning:  all the SFs might not be added at this point
  flowMAPSize = maxSizeMAP;

#ifdef TRACEME
  printf("BGClassifierObject::initFlowMap()(%lf): BGId:%d,  numberSFs:%d, numberAggregateQueues:%d, maxSizeMAP:%d, actualSize:%d \n", Scheduler::instance().clock(),
                   myBGID, myBGObject->numberSFs, myBGObject->numberAggregateQueues,maxSizeMAP,flowMAPSize);
#endif

  //If there are no SFs using this BG, there's no point creating the map
  if (flowMAPSize > 0) {

  if (fileName == NULL) {
    myMap = new struct FlowMapType[maxSizeMAP];
    for (i=0; i<maxSizeMAP; i++) {
      myMap[i].SFID = i;
      myMap[i].BGID = myBGID;
      myMap[i].AggregateQueueID = 0;
      myMap[i].accesses = 0;
    }
  } 
  else {
    //read in the flow map from the file
    ;
  }
 
  }
  return rc;
}

/***********************************************************************
*function: int BGClassifierObject::printFlowMap()
*
*explanation:
*  This method simply dumps to stdout the contents of the flow map.
*
*inputs:
*
*outputs:
*
************************************************************************/
void BGClassifierObject::printFlowMap()
{
  int i;

#ifdef TRACEME
  printf("BGClassifierObject::printFlowMap()(%lf): BG ID:%d    Flow Map size:%d  \n", Scheduler::instance().clock(), myBGID, maxSizeMAP);
#endif

  if (flowMAPSize > 0) {
    for (i=0; i<maxSizeMAP; i++) {
      printf("%d %d %d %d %d \n", i, myMap[i].SFID, myMap[i].BGID, myMap[i].AggregateQueueID,myMap[i].accesses);
    }
 
  }
  else
    printf(" Flow Map is empty \n");

}

/***********************************************************************
*function: ListObj * BGClassifierObject::findAggregateQueue(Packet *p, serviceFlowObject *SFObj)
*
*explanation:
*  This method finds the aggregate flow queue that the SFObj has been mapped to.
*
*inputs:
*  Packet *p:  The packet to be classified
*  serviceFlowObject *SFObj: the SFObj associated with the flow
*
*outputs:
*  Returns a valid List or a NULL
*
************************************************************************/
aggregateSFObject * BGClassifierObject::findAggregateQueue(Packet *p, serviceFlowObject *SFObj)
{
  aggregateSFObject *aggSFObj = NULL;
  int AggSFQIndex =  0;


//AggSFQIndex = myMap[SFObj->flowID].AggregateQueueID;
  aggSFObj = (aggregateSFObject *)&myBGObject->myAggSFObjects[AggSFQIndex];


#ifdef TRACEME
  printf("BGClassifierObject::findAggregateQueue(%lf): SFObj flowid:%d, AggSFQIndex=%d, return aggregate flowid:%d \n", Scheduler::instance().clock(),SFObj->flowID,AggSFQIndex, aggSFObj->flowID);
#endif

  return aggSFObj;
}

/***********************************************************************
*function: int BGClassifierObject::modifyFlowMap(serviceFlowObject *SFObj, int opCode, int opParam)
*
*explanation:
*  This method finds the aggregate flow queue that the SFObj has been mapped to.
*
*inputs:
*  serviceFlowObject *SFObj: the SFObj associated with the flow
*  int opCode:
*  int opParam:
*
*outputs:
*  Returns a SUCCESS or FAILURE
*
************************************************************************/
int BGClassifierObject::modifyFlowMap(serviceFlowObject *SFObj, int opCode, int opParam)
{
  int rc = SUCCESS;

  return rc;
}
 


/***********************************************************************
*function: void BGClassifierObject::getStats(struct BGClassifierStatsType *callersStats)
*
*explanation:
*
*inputs:
*
*outputs:
*
************************************************************************/
void BGClassifierObject::getStats(struct BGClassifierStatsType *callersStats)
{
  *callersStats = myStats;
}


void BGClassifierObject::printStatsSummary()
{

}

/***********************************************************************
*function: BGClassifierObjectHash::BGClassifierObjectHash(): BGClassifierObject()
*
*explanation:
*
*inputs:
*
*outputs:
*
************************************************************************/
BGClassifierObjectHash::BGClassifierObjectHash(): BGClassifierObject()
{
}

BGClassifierObjectHash::~BGClassifierObjectHash()
{
}

void BGClassifierObjectHash::init(int myBGIDParam, int maxSizeMAPParam, int schedModeParam, bondingGroupObject *myBGObjectParam)
{
#ifdef TRACEME
  printf("BGClassifierObjectHash::init()(%lf): init  \n", Scheduler::instance().clock());
#endif
  BGClassifierObject::init(myBGIDParam, maxSizeMAPParam, schedModeParam,myBGObjectParam);
}

/***********************************************************************
*function: int BGClassifierObjectHash::adjustFlowMap(int opCode)
*
*explanation:
*  This method instructs the classifier to adjust.  The meaning of
*   the opCode is specific to the type of classifier.
*   The set of opCodes is the same for the method modifyFlowMap(serviceFlowObject *SFObj, int opCode, int opParam)
* 
*   This adjustFlowMap method is meant to possibly change all flow mappings
*   based on some strategy (like random shuffling).
*   The modifiyFowMap is meant to change the mapping for a particular service flow
* 
*inputs:
*  int opCode : possible values include:
*     #define SHUFFLE_FLOW_TO_BUCKET_MAPPING 1
*     #define CHANGE_TO_NEW_AGGREGATE_QEUEUE 2
*     #define PUT_FLOW_IN_TIMEOUT 3
*     #define REMOVE_FLOW_FROM_TIMEOUT 4
*
*outputs:
*  returns a SUCCESS or FAILURE
*
***************************************************************************/
int BGClassifierObjectHash::adjustFlowMap(int opCode)
{
  int rc;

#ifdef TRACEME
  printf("BGClassifierObjectHash::adjustFlowMap)(%lf): init BGClassifierObject ID:%d, sizeMAP:%d, numberAggregateQueues:%d \n", Scheduler::instance().clock(),myBGID,maxSizeMAP,myBGObject->numberAggregateQueues);
#endif
  rc = randomMap(myBGObject->numberAggregateQueues);
//  printFlowMap();
  return rc;
}


/***********************************************************************
*function: int BGClassifierObjectHash::initFlowMap(char *fileName)
*
*explanation:
*  This method is called to initialize the SF to Aggrgate Flow Queue 
*   mapping to an initial state.  The specific mapping determines
*   the classification.  
* 
*   The Hash type maps the set of SFs to a smaller set of Aggregate queues.
*   If the number of SFs is equal to the number of Aggregate queues,
*      this is equivalent to WFQ.
*
*    If the fileName param is NULL, then this method inits the
*    flow MAP by calling the randomMap(numberBuckets) method.
*
*inputs:
*   char *fileName : specifies the input file that holds data. 
*
*outputs:
*  returns a SUCCESS or FAILURE
*
************************************************************************/
int BGClassifierObjectHash::initFlowMap(char *fileName)
{
  int rc = SUCCESS;
  int i;

#ifdef TRACEME
  printf("BGClassifierObjectHash::initFlowMap()(%lf): BGId:%d,  numberSFs:%d, numberAggregateQueues:%d, maxSizeMAP:%d, actualSize:%d \n", Scheduler::instance().clock(),
                   myBGID, myBGObject->numberSFs, myBGObject->numberAggregateQueues,maxSizeMAP,flowMAPSize);
#endif

//  BGClassifierObject::init(myBGIDParam, maxSizeMAPParam, schedModeParam,myBGObjectParam);

  //warning:  all the SFs might not be added at this point
  flowMAPSize = maxSizeMAP;


  BGClassifierObject::initFlowMap(fileName);

  //If there are no SFs using this BG, there's no point creating the map
  if (flowMAPSize > 0) {

    if (fileName == NULL) {
      rc = randomMap(myBGObject->numberAggregateQueues);
    } 
    else {
      //read in the flow map from the file
      ;
    }
 
  }
 
  return rc;
}

/***********************************************************************
*function: int BGClassifierObjectHash::randomMap(int numberBuckets)
*
*explanation:
*  This method puts the flowMAP into a random state that
*  makes sense for this particular type of classifier.
*
*inputs:
*  int numberBuckets:  the number of aggregate queues
*
*outputs:
*  returns a SUCCESS or FAILURE
*
************************************************************************/
int BGClassifierObjectHash::randomMap(int numberBuckets)
{
  int rc = SUCCESS;
  int i,j;
  double curTime =   Scheduler::instance().clock();
  int randomID=0;
  int prevAggQueueID;
  aggregateSFObject *myAggSFObjFrom = NULL;
  aggregateSFObject *myAggSFObjTo = NULL;
  aggregateSFObject *aggQueuePtr = NULL;
  int transferCount = 0;
  int numberFlowChanges = 0;
#ifdef SHOW_QUEUES
  char traceString1[32];
  char *tptr1 = traceString1;
  char traceString2[32];
  char *tptr2 = traceString2;
#endif
//Just look at the first two
  aggregateSFObject *SF1 = (aggregateSFObject *)&myBGObject->myAggSFObjects[0];
  aggregateSFObject *SF2 = (aggregateSFObject *)&myBGObject->myAggSFObjects[1];


#ifdef SHOW_QUEUES
   //only for debug....do this just once
  if ((curTime > QUEUE_MONITOR_START) && (curTime < QUEUE_MONITOR_STOP)) {
    sprintf(tptr1,"FLOWQUEUEBEFORE%d0.out",myBGID);
    sprintf(tptr2,"FLOWQUEUEBEFORE%d1.out",myBGID);
    SF1->dumpQueues(tptr1);
    SF2->dumpQueues(tptr2);
  }
#endif


#ifdef TRACEME
  printf("BGClassifierObjectHash::randomMap()(%lf): BGId:%d,  numberSFs:%d, numberBuckets:%d, maxSizeMAP:%d, actualSize:%d \n", Scheduler::instance().clock(),
                   myBGID, myBGObject->numberSFs, numberBuckets,maxSizeMAP,flowMAPSize);
#endif

  for (i=0; i<maxSizeMAP; i++) {
    myMap[i].SFID = i;
    myMap[i].BGID = myBGID;
    randomID = (int) Random::uniform(0,numberBuckets);
    prevAggQueueID = myMap[i].AggregateQueueID;

    myMap[i].AggregateQueueID = randomID;


    //only shuffle if there's been activity and if the SF has been shuffled
    if ((myMap[i].accesses > 100) && (prevAggQueueID != randomID)) {
      myAggSFObjFrom = (aggregateSFObject *)&myBGObject->myAggSFObjects[prevAggQueueID];
      myAggSFObjTo = (aggregateSFObject *)&myBGObject->myAggSFObjects[randomID];
#ifdef TRACEME
      printf("BGClassifierObjectHash::randomMap(%lf): SF:%d prevQueueID:%d  newQueueID:%d flowID:%d \n ",
         Scheduler::instance().clock(), i, prevAggQueueID, randomID,i);
#endif
      numberFlowChanges++;
      transferCount = myBGObject->myBGMgr->transferSF(myAggSFObjFrom,myAggSFObjTo,i);
        
#ifdef TRACEME 
            printf("BGClassifierObjectHash::randomMap(%lf): transferCount%d, update addSFObjto currentSize:%d \n",
               Scheduler::instance().clock(),transferCount, myAggSFObjTo->packetsQueued());
#endif
    }
    myMap[i].accesses=0;
  }
 
#ifdef SHOW_QUEUES
   //only for debug....do this just once
  if ((curTime > QUEUE_MONITOR_START) && (curTime < QUEUE_MONITOR_STOP)) {
    sprintf(tptr1,"FLOWQUEUEAFTER%d%d.out",myBGID,PRIORITY_HIGH);
    sprintf(tptr2,"FLOWQUEUEAFTER%d%d.out",myBGID,PRIORITY_LOW);
    SF1->dumpQueues(tptr1);
    SF2->dumpQueues(tptr2);
  }
#endif

#ifdef TRACEME
  printf("BGClassifierObjectHash::randomMap(%lf):BGID:%d, made %d SF assignment changes.  New list sizes:    ",
            Scheduler::instance().clock(), myBGID, numberFlowChanges);
  for (j=0; j<numberBuckets; j++) {
    aggQueuePtr = (aggregateSFObject *)&myBGObject->myAggSFObjects[j];
    if (aggQueuePtr == NULL) {
      printf(" (NULL)");
      break;
    }
    printf(" (%d,%d)  ", j,aggQueuePtr->myPacketList->getListSize());
  }
  printf("\n");
#endif

  return rc;

}

/***********************************************************************
*function: ListObj * BGClassifierObjectHash::findAggregateQueue(Packet *p, serviceFlowObject *SFObj)
*
*explanation:
*  This method finds the aggregate flow queue that the SFObj has been mapped to.
*
*inputs:
*  Packet *p:  The packet to be classified
*  serviceFlowObject *SFObj: the SFObj associated with the flow
*
*outputs:
*  Returns a valid List or a NULL
*
************************************************************************/
aggregateSFObject * BGClassifierObjectHash::findAggregateQueue(Packet *p, serviceFlowObject *SFObj)
{
  aggregateSFObject *aggSFObj = NULL;
  int AggSFQIndex =  0;


  AggSFQIndex = myMap[SFObj->flowID].AggregateQueueID;
  myMap[SFObj->flowID].accesses++;
  aggSFObj = (aggregateSFObject *)&myBGObject->myAggSFObjects[AggSFQIndex];


#ifdef TRACEME
  printf("BGClassifierObjectHash::findAggregateQueue(%lf): SFObj flowid:%d, AggSFQIndex=%d, return aggregate flowid:%d \n", Scheduler::instance().clock(),SFObj->flowID,AggSFQIndex, aggSFObj->flowID);
#endif

  return aggSFObj;
}

/***********************************************************************
*function: int BGClassifierObjectHash::modifyFlowMap(serviceFlowObject *SFObj, int opCode, int opParam)
*
*explanation:
*  This method finds the aggregate flow queue that the SFObj has been mapped to.
*
*inputs:
*  serviceFlowObject *SFObj: the SFObj associated with the flow
*  int opCode:
*  int opParam:
*
*outputs:
*  Returns a SUCCESS or FAILURE
*
************************************************************************/
int BGClassifierObjectHash::modifyFlowMap(serviceFlowObject *SFObj, int opCode, int opParam)
{
  int rc = SUCCESS;

  if (opCode == SHUFFLE_FLOW_TO_BUCKET_MAPPING)
  {
    rc = randomMap(myBGObject->numberAggregateQueues);
//    printFlowMap();
  }
  else
  {
#ifdef TRACEME
    printf("BGClassifierObjectHash::modifyFlowMap(%lf):  flowid:%d, Bad opCode(%d) \n", Scheduler::instance().clock(),SFObj->flowID,opCode);
#endif
    rc = FAILURE;
  }
  return rc;
}
 




/***********************************************************************
*function: BGClassifierObjectPriority::BGClassifierObjectPriority(): BGClassifierObject()
*
*explanation:
*
*inputs:
*
*outputs:
*
************************************************************************/
BGClassifierObjectPriority::BGClassifierObjectPriority(): BGClassifierObject()
{
}

BGClassifierObjectPriority::~BGClassifierObjectPriority()
{
}

void BGClassifierObjectPriority::init(int myBGIDParam, int maxSizeMAPParam, int schedModeParam, bondingGroupObject *myBGObjectParam)
{
#ifdef TRACEME
  printf("BGClassifierObjectPriority::init()(%lf): init  \n", Scheduler::instance().clock());
#endif
  BGClassifierObject::init(myBGIDParam, maxSizeMAPParam, schedModeParam,myBGObjectParam);
}

/***********************************************************************
*function: int BGClassifierObjectPriority::adjustFlowMap(int opCode)
*
*explanation:
*  This method instructs the classifier to adjust.  The meaning of
*   the opCode is specific to the type of classifier.
*   The set of opCodes is the same for the method modifyFlowMap(serviceFlowObject *SFObj, int opCode, int opParam)
* 
*   This adjustFlowMap method is meant to possibly change all flow mappings
*   based on some strategy (like random shuffling).
*   The modifiyFowMap is meant to change the mapping for a particular service flow
* 
*inputs:
*  int opCode : possible values include:
*     #define SHUFFLE_FLOW_TO_BUCKET_MAPPING 1
*     #define CHANGE_TO_NEW_AGGREGATE_QEUEUE 2
*     #define PUT_FLOW_IN_TIMEOUT 3
*     #define REMOVE_FLOW_FROM_TIMEOUT 4
*
*outputs:
*  returns a SUCCESS or FAILURE
*
***************************************************************/
int BGClassifierObjectPriority::adjustFlowMap(int opCode)
{
  int rc;

#ifdef TRACEME
  printf("BGClassifierObjectPriority::adjustFlowMap)(%lf): init BGClassifierObject ID:%d, sizeMAP:%d, numberAggregateQueues:%d \n", Scheduler::instance().clock(),myBGID,maxSizeMAP,myBGObject->numberAggregateQueues);
#endif
  return rc;
}


/***********************************************************************
*function: int BGClassifierObjectPriority::initFlowMap(char *fileName)
*
*explanation:
*  This method is called to initialize the SF to Aggrgate Flow Queue 
*   mapping to an initial state.  The specific mapping determines
*   the classification.  
* 
*   The Priority type assumes Aggregate queue 0 is highest priority
*   and The last Aggregate queue in the list is lowest priority.
*   SFs are mapped based on the file contents.  
*   
*
*inputs:
*   char *fileName : specifies the input file that holds data.  Sometimes
*     this is not needed.  For example, the base class knows mapping
*     and so the fileName is assumed to be NULL.
*
*     If it is not NULL, we do read and use the file contents.
*
*outputs:
*
************************************************************************/
int BGClassifierObjectPriority::initFlowMap(char *fileName)
{
  int rc = SUCCESS;

#ifdef TRACEME
  printf("BGClassifierObjectPriority::initFlowMap()(%lf): initFlowMap  \n", Scheduler::instance().clock());
#endif
 
  return rc;
}


/***********************************************************************
*function: ListObj * BGClassifierObjectPriority::findAggregateQueue(Packet *p, serviceFlowObject *SFObj)
*
*explanation:
*  This method finds the aggregate flow queue that the SFObj has been mapped to.
*  For a two level priority queue, the matching is simple:
*  just use the priority field in the pkt ip header.
*  
*
*inputs:
*  Packet *p:  The packet to be classified
*  serviceFlowObject *SFObj: the SFObj associated with the flow
*
*outputs:
*  Returns a valid List or a NULL
*
************************************************************************/
aggregateSFObject * BGClassifierObjectPriority::findAggregateQueue(Packet *p, serviceFlowObject *SFObj)
{
  struct hdr_cmn *ch = HDR_CMN(p);
  struct hdr_ip *chip = HDR_IP(p);
  aggregateSFObject *aggSFObj = NULL;

//  int SFQIndex = myMap[SFObj->flowID].AggregateQueueID;
   int SFQIndex = 0;
   if (myBGObject->queueServiceDiscipline == FAIRSHARE)
    SFQIndex =  SFObj->flowPriority;
   else if (myBGObject->queueServiceDiscipline == FAIRSHAREAQM)
    SFQIndex =  SFObj->flowPriority;
   else if (myBGObject->queueServiceDiscipline == BROADBANDAQM)
    SFQIndex =  SFObj->flowPriority;
   else
    SFQIndex =  SFObj->flowPriority;

  if (SFQIndex > 1) {
    printf("BGClassifierObjectPriority::findAggregateQueue(%lf): HARD ERROR, pkt priority : %d  \n", Scheduler::instance().clock(),chip->prio_);
    exit(1);
  }
  aggSFObj = (aggregateSFObject *)&myBGObject->myAggSFObjects[SFQIndex];


#ifdef TRACEME
  printf("BGClassifierObjectPriority::findAggregateQueue(%lf): SFObj flowid:%d (discipline:%d) , SFQIndex=%d, return aggregate flowid:%d \n", 
      Scheduler::instance().clock(),SFObj->flowID, myBGObject->queueServiceDiscipline, SFQIndex, aggSFObj->flowID);
#endif

  return aggSFObj;
}

/***********************************************************************
*function: int BGClassifierObjectPriority::modifyFlowMap(serviceFlowObject *SFObj, int opCode, int opParam)
*
*explanation:
*  This method finds the aggregate flow queue that the SFObj has been mapped to.
*
*inputs:
*  serviceFlowObject *SFObj: the SFObj associated with the flow
*  int opCode:
*  int opParam:
*
*outputs:
*  Returns a SUCCESS or FAILURE
*
************************************************************************/
int BGClassifierObjectPriority::modifyFlowMap(serviceFlowObject *SFObj, int opCode, int opParam)
{
  int rc = SUCCESS;

  return rc;
}


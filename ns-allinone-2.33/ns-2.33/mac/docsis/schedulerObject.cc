/***************************************************************************
 * Module: schedulerObject
 *
 * Explanation:
 *   This file contains the class definition of the scheduler. 
 *
 * Revisions:
 *
 * $A807 :  change behavior of SF enqueue- any MGMT packet will be added at the head
 *   $A906 :v1 of FAIRSHARE AQM
 *   $A910 :v1 of BROADBAND AQM
 *
************************************************************************/


#include "schedulerObject.h"
#include "packetScheduler.h"
#include "hierarchicalPacketScheduler.h"
#include "DRRpacketScheduler.h"
#include "SCFQpacketScheduler.h"

#include "serviceFlowObject.h"
#include "serviceFlowMgr.h"
#include "bondingGroupMgr.h"
#include "macPhyInterface.h"
#include "medium.h"

#include "docsisDebug.h"
//#define TRACEME 0


/*************************************************************************
 ************************************************************************/
schedulerObject::schedulerObject()
{
  myPktScheduler = NULL;
}

/*************************************************************************
 ************************************************************************/
schedulerObject::~schedulerObject()
{
}



void schedulerObject::init(serviceFlowMgr * paramDSSFMgr, bondingGroupMgr *paramBGMgr)
{
  mySFMgr = paramDSSFMgr;
  myBGMgr = paramBGMgr;

  maxBurstSize = 1500; //units
  minBurstSize = 188;
  burstUnits = 1;     //units of bytes 

  stateFlag = SCHED_STATE_INITED;

  bzero((void *)&myStats,sizeof(struct schedulerStatsType));

}


/***********************************************************************
*function: int schedulerObject::adjustScheduler(int opCode)
*
*explanation:
*  This is periodically called requesting the scheduler to adjust or optimize
*  itself.
*
*inputs:
*  int opCode:  specifies the type of adjustment.
*
*outputs: Returns a SUCCESS or FAILURE
*
*********************************************************************/
int schedulerObject::adjustScheduler(int opCode)
{
  int rc = SUCCESS;

  return rc;
}


/***********************************************************************
*function: int schedulerObject::pickChannel(Packet *pkt, serviceFlowObject *mySF)
*
*explanation:
*  This is the packet scheduler.  Based on current information it selects
*  the channel that is to be used.
*
*inputs:
*
*outputs: Returns a valid channel number (0 or larger) on success
*         else a -1 on error which implies no channel is available
*         and the packet must be queued (or dropped) by the caller
*
************************************************************************/
int schedulerObject::pickChannel(Packet *pkt, serviceFlowObject *mySF)
{
  int rc;
  rc = myBGMgr->pickChannel(mySF);
#ifdef TRACEME
	printf("schedulerObject:pickChannel: selected this channel:%d  \n", rc);
#endif
  return rc;
}

/***********************************************************************
*function: int schedulerObject::makeSchedDecision(Packet *pkt, serviceFlowObject *mySF)
*
*explanation:
*  This is the packet scheduler.  Based on current information it selects
*  the channel that is to be used.
*
*inputs:
*
*outputs: Returns a pointer to a scheduler assignment data structure.
*
*         The caller MUST free the memory!!
*
*         A NULL indicates an error.
*
************************************************************************/
schedulerAssignmentType * schedulerObject::makeSchedDecision(Packet *pkt, serviceFlowObject *mySF)
{
struct schedulerAssignmentType *mySchedAssignment=NULL;
int i;

  printf("schedulerObject:makeSchedDecision:(%lf): ERROR: should not be called\n", Scheduler::instance().clock());


  //make the scheduler decision.....
  mySchedAssignment = (struct schedulerAssignmentType *) malloc(sizeof(struct schedulerAssignmentType));
  mySchedAssignment->maxBurstSize = maxBurstSize;
  mySchedAssignment->minBurstSize = minBurstSize;
  mySchedAssignment->burstUnits = burstUnits;
  mySchedAssignment->numberAssignments = 1;

  for (i=0;i<MAX_SINGLE_BURST_CHANNELS;i++) 
  {
    mySchedAssignment->channelAssignments[i] = -1; 
//$A801
    mySchedAssignment->dsidAssignment = -1;
  }
  if ( mySF->mySFMgr->IsFrameMgmt(pkt))
  {
	mySchedAssignment->channelAssignments[0] = 0;
//$A801
    mySchedAssignment->dsidAssignment = 1;
    return mySchedAssignment;
  }
#ifdef TRACEME
  printf("schedulerObject:makeSchedDecision:Calling pickChannel\n");
#endif

  if ((mySchedAssignment->channelAssignments[0] = pickChannel(pkt,mySF)) == -1) {
	 mySchedAssignment->numberAssignments = 0;
//$A801
     mySchedAssignment->dsidAssignment = -1;
  }
  
  return mySchedAssignment;
}

/***********************************************************************
*function: int schedulerObject::makeSchedDecision(Packet *pkt, serviceFlowObject *mySF)
*
*explanation:
*  This is the packet scheduler.  Based on current information it selects
*  the channel that is to be used.
*
*inputs:
*
*outputs: Returns a pointer to a scheduler assignment data structure.
*
*         The caller MUST free the memory!!
*
*         A NULL indicates an error.
*
************************************************************************/
schedulerAssignmentType * schedulerObject::makeSchedDecision(int channelNumber)
{
	struct schedulerAssignmentType *mySchedAssignment=NULL;
	return mySchedAssignment;
}

void schedulerObject::printStatsSummary()
{
  double topP = 0;
  double lowerP = 0;

  if (myStats.numberSchedulingDecisions > 0) {
    topP = 100.0 * ((double)myStats.topMakeSchedDecisionCount) / (double)myStats.numberSchedulingDecisions;
    lowerP = 100.0 * ((double)myStats.lowerMakeSchedDecisionCount) / (double)myStats.numberSchedulingDecisions;
  }

  printf("schedulerObject::printStatsSummary(%lf): numberDecisions:%d,  percentage toppath:%3.1f (count:%d), percentage lower path%3.1f (count:%d)\n",
      Scheduler::instance().clock(), myStats.numberSchedulingDecisions, topP, myStats.topMakeSchedDecisionCount,lowerP,myStats.lowerMakeSchedDecisionCount);
  printf("schedulerObject::printStatsSummary(%lf): topImmediateMgmtSchedAssignments:%12d,  topDeferredMgmtSchedAssignments:%12d: topImmediateDataSchedAssignments:%12d, topDeferredDataSchedAssignments:%12d \n",
      Scheduler::instance().clock(), myStats.topImmediateMgmtSchedAssignments, myStats.topDeferredMgmtSchedAssignments,
      myStats.topImmediateDataSchedAssignments, myStats.topDeferredDataSchedAssignments);



}

void schedulerObject::getSchedulerStats(struct  schedulerStatsType *schedStats)
{
}


DSschedulerObject::DSschedulerObject()
{
}

DSschedulerObject::~DSschedulerObject()
{
}

/***********************************************************************
*function: int schedulerObject::adjustScheduler(int opCode)
*
*explanation:
*  This is periodically called requesting the scheduler to adjust or optimize
*  itself.
*
*inputs:
*  int opCode:  specifies the type of adjustment.
*
*outputs: Returns a SUCCESS or FAILURE
*
*********************************************************************/
int DSschedulerObject::adjustScheduler(int opCode)
{
  int rc = SUCCESS;
#ifdef TRACEME
  printf("DSschedulerObject:adjustScheduler:(%lf): SchedulerMode:%d,invoke my packet scheduler's adjust method, opCode:%d\n", 
       Scheduler::instance().clock(),schedulerMode, opCode);
#endif

  rc =  myPktScheduler->adaptPacketScheduler(opCode);


  return rc;
}

/***********************************************************************
*function: void DSschedulerObject::configure(int schedulerModeParam, int defaultQSizeParam, int defaultQTypeParam)
*
*explanation:
*  This is configures the DS scheduler -  
*
*inputs:
*   int schedulerModeParam : the tcl DSSchedMode param:  RR, DRR, SCFQ, HDRR
*   int defaultQSizeParam
*   int defaultQTypeParam
*
*outputs:  none
*
************************************************************************/
void DSschedulerObject::configure(int schedulerModeParam, int defaultQSizeParam, int defaultQTypeParam)
{

  stateFlag = SCHED_STATE_CONFIGURED;
  schedulerMode = schedulerModeParam;
  defaultQSize = defaultQSizeParam;
  defaultQType = defaultQTypeParam;

//#ifdef TRACEME
  printf("DSschedulerObject:configure:(%lf): SchedulerMode:%d, defaultQSize:%d, defaultQType:%d \n", 
       Scheduler::instance().clock(),schedulerMode,defaultQSize,defaultQType);
//#endif

  myBGMgr->init(schedulerMode,defaultQSize,defaultQType);

  switch (schedulerMode) 
  {
    case FCFS:
      break;

    case RR:
      myPktScheduler = new RRpacketSchedulerObject();
      myPktScheduler->init(RR,mySFMgr,myBGMgr);
      break;

    case DRR:
      myPktScheduler = new DRRpacketSchedulerObject();
      myPktScheduler->init(DRR,mySFMgr,myBGMgr);
      break;

    case bestDRR:
      myPktScheduler = new pDRRpacketSchedulerObject();
      myPktScheduler->init(bestDRR,mySFMgr,myBGMgr);
      break;

    case globalDRR:
      myPktScheduler = new pDRRpacketSchedulerObject();
      myPktScheduler->init(globalDRR,mySFMgr,myBGMgr);
      break;

    case channelDRR:
      myPktScheduler = new pDRRpacketSchedulerObject();
      myPktScheduler->init(channelDRR,mySFMgr,myBGMgr);
      break;

    case bondingGroupDRR:
      myPktScheduler = new pDRRpacketSchedulerObject();
      myPktScheduler->init(bondingGroupDRR,mySFMgr,myBGMgr);
      break;

    case SCFQ:
      myPktScheduler = new SCFQpacketSchedulerObject();
      myPktScheduler->init(SCFQ,mySFMgr, myBGMgr);
      break;

    case globalSCFQ:
      myPktScheduler = new pSCFQpacketSchedulerObject();
      myPktScheduler->init(globalSCFQ,mySFMgr, myBGMgr);
      break;

    case channelSCFQ:
      myPktScheduler = new pSCFQpacketSchedulerObject();
      myPktScheduler->init(channelSCFQ,mySFMgr, myBGMgr);
      break;

    case bondingGroupSCFQ:
      myPktScheduler = new pSCFQpacketSchedulerObject();
      myPktScheduler->init(bondingGroupSCFQ,mySFMgr, myBGMgr);
      break;

    case HDRR:
      myPktScheduler = new hierarchicalPacketSchedulerObject();
      ((hierarchicalPacketSchedulerObject *)myPktScheduler)->init(schedulerMode,defaultQSizeParam,defaultQTypeParam,mySFMgr,myBGMgr);
      break;

    case HOSTBASED:
      myPktScheduler = new hierarchicalPacketSchedulerObject();
      ((hierarchicalPacketSchedulerObject *)myPktScheduler)->init(schedulerMode,defaultQSizeParam,defaultQTypeParam,mySFMgr,myBGMgr);
      break;

    case FAIRSHARE:
      myPktScheduler = new hierarchicalPacketSchedulerObject();
      ((hierarchicalPacketSchedulerObject *)myPktScheduler)->init(schedulerMode,defaultQSizeParam,defaultQTypeParam,mySFMgr,myBGMgr);
      break;

    case FAIRSHAREAQM:
      myPktScheduler = new hierarchicalPacketSchedulerObject();
      ((hierarchicalPacketSchedulerObject *)myPktScheduler)->init(schedulerMode,defaultQSizeParam,defaultQTypeParam,mySFMgr,myBGMgr);
      break;

    case BROADBANDAQM:
      myPktScheduler = new hierarchicalPacketSchedulerObject();
      ((hierarchicalPacketSchedulerObject *)myPktScheduler)->init(schedulerMode,defaultQSizeParam,defaultQTypeParam,mySFMgr,myBGMgr);
      break;


    default:
      printf("DSschedulerObject:configure:(%lf): HARD ERROR Bad Scheduler Mode (%d) \n", Scheduler::instance().clock(),schedulerMode);
      exit(1);
      break;
  }

}


/***********************************************************************
*function: int DSschedulerObject::makeSchedDecision(Packet *pkt, serviceFlowObject *mySF)
*
*explanation:
*  This is the packet scheduler.  Based on current information it selects
*  the channel that is to be used.
*
*inputs:
*
*outputs: Returns a pointer to a scheduler assignment data structure.
*
*         The caller MUST free the memory!!
*
*         A NULL indicates an error.
* Pseudo Code:
*	Find the SFObject
*	Find the BG identifier from the SFObject (so the SF must be assigned to a single BG)
*	-Get the list of DS channels in THAT BG
*	-Call macPhyInterface->getChannelStatus  iteratively to learn if there is a channel AVAILABLE
*	-If all channels are busy, just return a NULL assignment
*	-else select the first available channel that is IDLE and return this in the sched Assignment
*
************************************************************************/
schedulerAssignmentType * DSschedulerObject::makeSchedDecision(Packet *pkt, serviceFlowObject *mySF)
{
	struct schedulerAssignmentType *mySchedAssignment=NULL;
	int i;
	int channelRet;
	struct channelSet myChannelSet;
	int rc;
	struct hdr_cmn *ch = HDR_CMN(pkt);
    double curTime = Scheduler::instance().clock();
    int flowBG = mySF->getBondingGroup();

	myChannelSet.numberMembers = 0;
	myChannelSet.maxSetSize = MAX_CHANNEL_SET_SIZE;

//	mySF->myStats.numberByteArrivals += ch->size();
//	mySF->myStats.numberPacketArrivals++;
//	mySF->lastPacketTime = curTime;
    mySF->newArrivalUpdate(pkt);
 
    myStats.numberSchedulingDecisions++;
    myStats.topMakeSchedDecisionCount++;

	//make the scheduler decision.....
	mySchedAssignment = (struct schedulerAssignmentType *) malloc(sizeof(struct schedulerAssignmentType));
	mySchedAssignment->maxBurstSize = maxBurstSize;
	mySchedAssignment->minBurstSize = minBurstSize;
	mySchedAssignment->burstUnits = burstUnits;
	mySchedAssignment->numberAssignments =0;
//$A801
    mySchedAssignment->dsidAssignment = -1;
    mySchedAssignment->myPkt =  NULL;

#ifdef TRACEME
    printf("DSschedulerObject:makeSchedDecision:(%lf): Invoked with pkt : %x  \n", Scheduler::instance().clock(),pkt);
#endif

	for (i=0;i<MAX_SINGLE_BURST_CHANNELS;i++) {
		mySchedAssignment->channelAssignments[i] = -1; 
	}

    rc = myBGMgr->findSetofChannels(flowBG,&myChannelSet);
#ifdef TRACEME
    printf("DSschedulerObject:makeSchedDecision:(%lf): rc from findSetofChannels:%d, number in Set:%d (BG:%d) \n", 
			Scheduler::instance().clock(),rc,myChannelSet.numberMembers,flowBG);
    printf(" channels in Set:   " );
	for (i=0;i<myChannelSet.numberMembers;i++) {
      printf(" %d  ",myChannelSet.channels[i]);
	}
    printf(" \n" );
#endif

//$A807 For now, let's don't do this check
//    if (0) {
	if (mySF->mySFMgr->IsFrameMgmt(pkt)) {
		if (myMacPhyInterface->getChannelStatus(0) == CHANNEL_IDLE)	{	
		  //Sched decision to always locate mgt packets on channel 0
		  mySchedAssignment->channelAssignments[0] = 0;
	      mySchedAssignment->numberAssignments =1;
//$A801
// dsid 1 is reserved for mgt packets
          mySchedAssignment->dsidAssignment = 1;

	      mySchedAssignment->myPkt = pkt;
		  mySF->updateStats(pkt,mySchedAssignment->channelAssignments[0],0);
          myStats.topImmediateMgmtSchedAssignments++;

#ifdef TRACEME
          printf("DSschedulerObject:makeSchedDecision:(%lf): Schedule MGMT packet over channel 0 , myStats.topImmediateMgmtSchedAssignments=%d \n", 
		         Scheduler::instance().clock(),myStats.topImmediateMgmtSchedAssignments);
#endif
		} 
        else {
          myStats.topDeferredMgmtSchedAssignments++;
//#ifdef TRACEME
          printf("DSschedulerObject:makeSchedDecision:(%lf): HARD ERROR Schedule MGMT packet over channel 0 but it's BUSY , myStats.topDeferredMgmtSchedAssignments=%d \n", 
			          Scheduler::instance().clock(),myStats.topDeferredMgmtSchedAssignments);
//#endif
       }
	} else {  
      rc =  myPktScheduler->selectChannel(pkt,mySF,&myChannelSet,&channelRet);
#ifdef TRACEME
      if (rc == SUCCESS) 
        printf("DSschedulerObject:makeSchedDecision:(%lf): return from select packet with SUCCESS and channelRet=%d\n", 
			Scheduler::instance().clock(),channelRet);
      else
         printf("DSschedulerObject:makeSchedDecision:(%lf): return from select packet with FAILURE and channelRet=%d\n", 
			Scheduler::instance().clock(),channelRet);
#endif

	  if (rc == SUCCESS) {
        mySchedAssignment->channelAssignments[0] = channelRet;
        mySchedAssignment->numberAssignments =1;
//$A801
        mySchedAssignment->dsidAssignment = mySF->dsid;

        mySchedAssignment->myPkt = pkt;
        mySF->updateStats(pkt,mySchedAssignment->channelAssignments[0],mySF->getBondingGroup());
        myStats.topImmediateDataSchedAssignments++;
      }
      else
        myStats.topDeferredDataSchedAssignments++;
	}
#ifdef TRACEME
    printf("DSschedulerObject:makeSchedDecision:(%lf): return with this number assignments: %d, channel:%d\n", 
			Scheduler::instance().clock(),mySchedAssignment->numberAssignments,mySchedAssignment->channelAssignments[0]);
#endif
	return mySchedAssignment;
}


/***********************************************************************
*function: schedulerAssignmentType * DSschedulerObject::makeSchedDecision(int channelNumber)
*
*explanation:
*  This method is called when a channel has become free. There may
*  or may not be packets waiting in the SF queues.  If there are,
*  a packet is selected and sent down the stack for transmission.
*
*inputs:
* int channelNumber: The channel that has just gone idle
*
* outputs: Returns a pointer to a scheduler assignment data structure.
*
*         The caller MUST free the memory!!
*
*         A NULL indicates an error.
*
************************************************************************/
schedulerAssignmentType * DSschedulerObject::makeSchedDecision(int channelNumber)
{
	struct schedulerAssignmentType *mySchedAssignment=NULL;
	int i;
	Packet *pkt = NULL;
	serviceFlowObject *mySF = NULL;
	struct serviceFlowSet mySet;
	struct channelSet myChannelSet;
	int rc;

	mySet.numberMembers = 0;
	mySet.maxSetSize = MAX_SERVICE_FLOW_SET_SIZE;

	myChannelSet.numberMembers = 0;
	myChannelSet.maxSetSize = MAX_CHANNEL_SET_SIZE;

    myStats.numberSchedulingDecisions++;
    myStats.lowerMakeSchedDecisionCount++;

	//make the scheduler decision.....
	mySchedAssignment = (struct schedulerAssignmentType *) malloc(sizeof(struct schedulerAssignmentType));
	mySchedAssignment->maxBurstSize = maxBurstSize;
	mySchedAssignment->minBurstSize = minBurstSize;
	mySchedAssignment->burstUnits = burstUnits;
	mySchedAssignment->numberAssignments =0;
//$A801
    mySchedAssignment->dsidAssignment = -1;
    mySchedAssignment->myPkt =  NULL;

#ifdef TRACEME
    printf("DSschedulerObject:makeSchedDecision:(%lf): Invoked with NO pkt, and with channelNumber:%d \n", 
       Scheduler::instance().clock(),channelNumber);
#endif

	for (i=0;i<MAX_SINGLE_BURST_CHANNELS;i++) {
		mySchedAssignment->channelAssignments[i] = -1; 
	}
   

	//This obtains the set of SF objects that could be sent of this channel
    rc = myBGMgr->findSetofFlows(channelNumber,&mySet);
    //JJM WRONG If this is channel 0, what happens if there is another mgt message queued ?
    int SFSetSize = mySet.numberMembers;
    if (SFSetSize == 0) {
#ifdef TRACEME
      printf("DSschedulerObject:makeSchedDecision:(%lf): rc from findSetofFlow:%d, number in Set:%d \n", 
			Scheduler::instance().clock(),rc,mySet.numberMembers);
#endif
	  return mySchedAssignment;
    }
#ifdef TRACEME
    printf("DSschedulerObject:makeSchedDecision:(%lf): rc from findSetofFlow:%d, number in Set:%d \n", 
			Scheduler::instance().clock(),rc,mySet.numberMembers);
    printf(" Flowids in Set:   " );
	serviceFlowObject *SFObj;
	u_int16_t serviceFlowID;
    if (SFSetSize > 0) {
	  for (i=0; i<SFSetSize;i++) {
	    SFObj = mySet.SFObjPtrs[i];
	    serviceFlowID = SFObj->flowID;
	    printf("   %d ",serviceFlowID);
      }
      printf("  \n");
    } else
      printf("    EMPTY \n");
    fflush(stdout);
#endif

	//selects the packet to send next, NULL if there are no packets queued
    pkt =  myPktScheduler->selectPacket(channelNumber,&mySet);
    if (pkt != NULL) {
      mySF = mySFMgr->getServiceFlow(pkt);	
      if (mySF != NULL) {
        if (mySFMgr->IsFrameMgmt(pkt)) {
#ifdef TRACEME
    printf("DSschedulerObject:makeSchedDecision:(%lf):  selected packet is mgt, try to set to channel 0 \n", 
			Scheduler::instance().clock());
#endif
          //Sched decision to always locate mgt packets on channel 0
          if (myMacPhyInterface->myMedium->myChannel[0].getTxState() == CHANNEL_IDLE)	{	
             mySchedAssignment->channelAssignments[0] = 0;
             mySchedAssignment->numberAssignments =1;
//$A801
// dsid 1 is reserved for mgt packets
             mySchedAssignment->dsidAssignment = 1;
             mySchedAssignment->myPkt = pkt;
//JJM WRONG
             mySF->updateStats(pkt,mySchedAssignment->channelAssignments[0],0);
          } 
          else {
             printf("DSschedulerObject:makeSchedDecision:(%lf): HARD ERROR 1: Mgmt channel busy \n", Scheduler::instance().clock());
             exit(1);
          }
        } else {  
#ifdef TRACEME
    printf("DSschedulerObject:makeSchedDecision:(%lf):  selected packet is NOT mgt \n", 
			Scheduler::instance().clock());
#endif
          if ( channelNumber >= 0) {
            mySchedAssignment->channelAssignments[0] = channelNumber;
	        mySchedAssignment->numberAssignments =1;
//$A801
            mySchedAssignment->dsidAssignment = mySF->dsid;
	        mySchedAssignment->myPkt = pkt;
//JJM WRONG
            mySF->updateStats(pkt,mySchedAssignment->channelAssignments[0],mySF->getBondingGroup());
          }
          else {
             printf("DSschedulerObject:makeSchedDecision:(%lf): HARD ERROR 2: INVALID channel %d \n", Scheduler::instance().clock(),channelNumber);
             exit(1);
          }
        }
	  } else {
        printf("DSschedulerObject:makeSchedDecision:(%lf): HARD ERROR 3: got valid pkt (%x) but no SF Object \n", Scheduler::instance().clock(),pkt);
        exit(1);
      }
	} else {
#ifdef TRACEME
        printf("DSschedulerObject:makeSchedDecision:(%lf):  but no SF Object waiting \n", Scheduler::instance().clock(),pkt);
#endif
    }

#ifdef TRACEME
    printf("DSschedulerObject:makeSchedDecision:(%lf): return with this number assignments: %d, channel:%d\n", 
			Scheduler::instance().clock(),mySchedAssignment->numberAssignments,mySchedAssignment->channelAssignments[0]);
#endif
	return mySchedAssignment;
}

USschedulerObject::USschedulerObject()
{
}

USschedulerObject::~USschedulerObject()
{
}

void USschedulerObject::configure(int schedulerModeParam, int defaultQSizeParam, int defaultQTypeParam)
{
//#ifdef TRACEME
  printf("USschedulerObject:configure:(%lf): SchedulerMode:%d, defaultQSize:%d, defaultQType:%d \n", 
       Scheduler::instance().clock(),schedulerModeParam,defaultQSizeParam,defaultQTypeParam);
//#endif
  stateFlag = SCHED_STATE_CONFIGURED;
  schedulerMode = schedulerModeParam;
  defaultQSize = defaultQSizeParam;
  defaultQType = defaultQTypeParam;
}

/***********************************************************************
*function: int USschedulerObject::makeSchedDecision(Packet *pkt, serviceFlowObject *mySF)
*
*explanation:
*  This is the packet scheduler.  Based on current information it selects
*  the channel that is to be used.
*
*inputs:
*
*outputs: Returns a pointer to a scheduler assignment data structure.
*
*         The caller MUST free the memory!!
*
*         A NULL indicates an error.
* Pseudo Code:
*	Find the SFObject
*	Find the BG identifier from the SFObject (so the SF must be assigned to a single BG)
*	-Get the list of DS channels in THAT BG
*	-Call macPhyInterface->getChannelStatus  iteratively to learn if there is a channel AVAILABLE
*	-If all channels are busy, just return a NULL assignment
*	-else select the first available channel that is IDLE and return this in the sched Assignment
*
************************************************************************/
schedulerAssignmentType * USschedulerObject::makeSchedDecision(Packet *pkt, serviceFlowObject *mySF)
{
struct schedulerAssignmentType *mySchedAssignment=NULL;
int i;


  //make the scheduler decision.....
  mySchedAssignment = (struct schedulerAssignmentType *) malloc(sizeof(struct schedulerAssignmentType));
  mySchedAssignment->maxBurstSize = maxBurstSize;
  mySchedAssignment->minBurstSize = minBurstSize;
  mySchedAssignment->burstUnits = burstUnits;
  mySchedAssignment->numberAssignments =1;

  myStats.numberSchedulingDecisions++;
  myStats.topMakeSchedDecisionCount++;

  for (i=0;i<MAX_SINGLE_BURST_CHANNELS;i++) 
  {
    mySchedAssignment->channelAssignments[i] = -1; 
  }

  mySchedAssignment->channelAssignments[0] =pickChannel(pkt,mySF);

  return mySchedAssignment;

}


/***********************************************************************
*function: int USschedulerObject::makeSchedDecision(Packet *pkt, serviceFlowObject *mySF)
*
*explanation:
*  This is the packet scheduler.  Based on current information it selects
*  the channel that is to be used.
*
*inputs:
*
*outputs: Returns a pointer to a scheduler assignment data structure.
*
*         The caller MUST free the memory!!
*
*         A NULL indicates an error.
* Pseudo Code:
*	-If not true then just return (i.e., there are no packets waiting for transmission)
*	-else implement a round robin algorithm to determine which SF will be serviced next.  
*	Remember which SF was pulled from last.  Select the packet from the next SF Queue.  This will take a new method in the SFMgr.
*
************************************************************************/
schedulerAssignmentType * USschedulerObject::makeSchedDecision(int channelNumber)
{
struct schedulerAssignmentType *mySchedAssignment=NULL;
int i;


  //make the scheduler decision.....
  mySchedAssignment = (struct schedulerAssignmentType *) malloc(sizeof(struct schedulerAssignmentType));
  mySchedAssignment->maxBurstSize = maxBurstSize;
  mySchedAssignment->minBurstSize = minBurstSize;
  mySchedAssignment->burstUnits = burstUnits;
  mySchedAssignment->numberAssignments =1;
  myStats.numberSchedulingDecisions++;
  myStats.lowerMakeSchedDecisionCount++;

  for (i=0;i<MAX_SINGLE_BURST_CHANNELS;i++) 
  {
    mySchedAssignment->channelAssignments[i] = -1; 
  }

  //mySchedAssignment->channelAssignments[0] =pickChannel(pkt,mySF);

  return mySchedAssignment;
}


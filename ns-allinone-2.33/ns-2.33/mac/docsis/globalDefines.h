#ifndef ns_globalDefines_h
#define ns_globalDefines_h

#define SUCCESS 0
#define FAILURE 1

#define TRUE 1
#define FALSE 0

#define DATA_PKT           0 /* Packet passed from IFQ(Upper layers) */
#define MGMT_PKT           1 /* Packet generated at DOCSIS layer */

#define UPSTREAM           0	
#define DOWNSTREAM         1
    
#define SimStartupTime     10 /* time for the startup effects to subside*/
                            
/*specifies max channels over a single medium */
#define MAX_CHANNELS       256

/*specifies max bonding Groups */
#define MAX_BONDING_GROUPS  256
#define MAX_CHANNELS_PER_BG 8
#define MAX_SFS_PER_BG 256

#define MAX_ALLOWED_SERVICE_FLOWS_CM 8
#define MAX_ALLOWED_SERVICE_FLOWS 1024
#define MAX_SERVICE_FLOW_SET_SIZE 256
#define MAX_CHANNEL_SET_SIZE 64
#define MAX_BONDING_GROUP_SET_SIZE 256

#define INACTIVE_FLOWID 0

//Defines for service disciplines
//Also referred to as the SchedMode at times....
#define FCFS 0
#define RR   1
#define WRR  2
#define DRR  3
#define SCFQ  4
#define WFQ  5
#define W2FQ 6
//Hierachical DRR scheduler - general 
//#define HDRR 7
//Specific types of Hierarchical schedulers
//#define HOSTBASED 8
//#define FAIRSHARE 10
#define HDRR 16
#define HOSTBASED 17
#define FAIRSHARE 19
//$A906
#define FAIRSHAREAQM  20
#define BROADBANDAQM  21

#define globalDRR 32
#define channelDRR 33
#define bondingGroupDRR 34
#define bestDRR 35

#define globalSCFQ 64
#define channelSCFQ 65
#define bondingGroupSCFQ 66
#define bestSCFQ 67

//Defines for Q Types
#define FIFOQ       0
#define  OrderedQ   1
#define RedQ        2
#define  BlueQ      3
#define AdaptiveRedQ 4
#define DelayBasedRedQ 5
#define AQM3Q       6
//Prioritized Red
#define PRedQ       8
//Prioritized Blue
#define PBlueQ      9
//Broadband access AQM
#define BAQMQ      10
#define PBAQMQ     11
//$A906
#define FSAQM      12
//CoDeL AQM
#define CDAQM      13
#define PIAQM     14
#define BBAQMQ    15

#define ACK_SUPPRESSION_OFF 0
#define ACK_SUPPRESSION_ON 1

#define BG_DEFAULT_RATE 38000000.0
#define BG_MAX_RATE 10000000000.0

#define OPTIMIZER_HEARTBEAT  0
#define ADJUST_PACKET_SCHEDULER 1
#define ADJUST_BANDWIDTH_MANAGEMENT 2
#define LOAD_BALANCE 3

#define DEFAULT_FLOW_QUANTUM 1500
#define MIN_FLOW_QUANTUM 50

#define PRIORITY_HIGH 0
#define PRIORITY_LOW 1

#define DEFAULT_FILTER_TIME_CONSTANT 0.002


#define AQM_ADAPTATION_TIME 0.50
#define DELAYBASEDRED_TARGET_LATENCY 0.080
#define CoDel_TARGET_LATENCY 0.020
//#define CoDel_TARGET_LATENCY 0.080
#define CoDel_INTERVAL 0.100

//#define PIE_TARGET_LATENCY 0.080
#define PIE_TARGET_LATENCY 0.020
#define PIE_UPDATE 0.030
#define PIE_MAX_BURST 0.100

#endif /* ns_globalDefines_h__ */


/***************************************************************************
 * Module: mediumMonitor
 *
 * Explanation:
 *   This file contains the class definition of the medium Monitor.
 *
 * Revisions:
 *
 *  TODO:
************************************************************************/

#ifndef ns_mediumMonitor_h
#define ns_mediumMonitor_h

#include "packet.h"
#include "rtp.h"
#include "hdr-docsis.h"


/*************************************************************************
 ************************************************************************/
class mediumMonitor: public packetMonitor
{
public:
  mediumMonitor();

  init();

  int mediumMonitor::packetTrace(Packet *p);
  int mediumMonitor::packetTrace(Packet *p,char *preText);
  int mediumMonitor::packetTrace(Packet *p, u_int32_t numberid);

  void mediumMonitor::clearStats(Packet *p);
  void mediumMonitor::updateStats(Packet *p);   //updates long term and running stats
  u_int32_t mediumMonitor::getFrameByteCount();
  u_int32_t mediumMonitor::getFrameCount();

  void mediumMonitor::clearRunningStats(Packet *p);
  u_int32_t mediumMonitor::getRunningFrameByteCount();
  u_int32_t mediumMonitor::getRunningFrameCount();

private:
  //TBD - define medium stats
  u_int32_t byteCount;

  u_int32_t runningByteCount;
};


#endif /* __ns_mediumMonitor_h__ */

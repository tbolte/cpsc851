/********************************************************************************
 *
 *    This file contains c++ methods for implementing the Docsis MAC  timer classes
 *
 *  Revisions:
 *   $A304: the base timer start() and the RxPkt timer start() now do a check to see
 *          if the timer is currently is busy. Right now, we just print an ERROR message and exit.
 *          
 ******************************************************************************/

#include "mac-docsis.h"
#include "mac-docsistimers.h"
#include "medium.h"
#include "rtp.h"
#include "reseqFlowListElement.h"
#include "reseqMgr.h"

#include "docsisDebug.h"
//#define TRACEME 0
/*************************************************************************
*  Method:  void MacDocsisTimer::setMacPointer(MacDocsis *macPtr)
* 
* Function:
*
*  inputs:
*
*  outputs:
*************************************************************************/
void MacDocsisTimer::setMacPointer(MacDocsis *macPtr)
{
  mac = macPtr;
}

//$A400
void MacDocsisTimer::setMedPointer(medium *medium)
{
  myMedium = medium;
#ifdef TRACEME
  printf("MacDocsisTimer::setMedPointer: myMed has been set to %x this %x\n",myMedium,this);
#endif

}

void MacDocsisTimer::setNode(NodeListData *node)
{
  element = node;
}

/*************************************************************************
*  Method: void MacDocsisTimer::setChannelNumber(int channelNumber)
* 
* Function:
*
*  inputs:
*
*  outputs:
*************************************************************************/
void MacDocsisTimer::setChannelNumber(int channelNumber)
{
  channelNumber_ = channelNumber;
}



/*************************************************************************
Timers
*************************************************************************/
void MacDocsisTimer :: start(Packet * e, double time)
{
  Scheduler &s = Scheduler::instance();

  
  assert(busy_ == 0);
  
//$A304
 if (busy_ == 1)
  {
    struct hdr_cmn *ch = HDR_CMN(p);
    hdr_tcp *tcph = hdr_tcp::access(p);
    hdr_rtp* rh = hdr_rtp::access(p);

//#ifdef TRACEME
    printf("MacDocsisTimer:start(%lf)[channel:%d]: ERROR:  Timer already busy , current pkt size: %d, scheduled for %lf\n",
     Scheduler::instance().clock(),channelNumber_,ch->size(),(stime+rtime));
    if(ch->ptype_ == PT_TCP)
        printf(" ....TCP Sequence Num %d Ack No   %d\n",tcph->seqno(), tcph->ackno());
    if(ch->ptype_ == PT_UDP)
        printf(".....UDP Sequence Num %d \n", rh->seqno());
//#endif
    exit(1);
  }

  busy_ = 1;
  paused_ = 0;
  stime = s.clock();
  rtime = time;
  assert(rtime >= 0.0);
  
#ifdef TRACEME
  printf("DocsisTimer:start(%lf): schedule event in %lf seconds (at %lf) BEFORE e->uid_ = %d \n",
     Scheduler::instance().clock(),rtime, Scheduler::instance().clock() + rtime,e->uid_);
#endif

#ifdef TRACEME
  if(!e)
	  printf("MacDocsisTimer::start: %s:packet/event is NULL!!\n", mac->docsis_id);
#endif
  s.schedule(this, e , rtime);
#ifdef TRACEME
  printf("DocsisTimer:start(%lf): schedule event in %lf seconds (at %lf) AFTER e->uid_ = %d \n",
     Scheduler::instance().clock(),rtime, Scheduler::instance().clock() + rtime,e->uid_);
#endif
}

/*************************************************************************

*************************************************************************/
void MacDocsisTimer::stop(Packet * e) 
{
  Scheduler &s = Scheduler::instance();
  
  assert(busy_);
  
  if(paused_ == 0)
    s.cancel((Event *)e);
  
#ifdef TRACEME
  if(!e)
   printf("MacDocsisTimer::stop: %s:packet/event is NULL!!\n", mac->docsis_id);
#endif
 
  busy_ = 0;
  paused_ = 0;
  stime = 0.0;
  rtime = 0.0;
#ifdef TRACEME
  printf("DocsisTimer:stop:(%lf) stop event\n", Scheduler::instance().clock());
#endif
}

//$A301:  This is new
void RxPktDocsisTimer::start(Packet *myp,  double time)
{

//$A304
 if (busy_ == 1)
  {
    struct hdr_cmn *ch = HDR_CMN(p);
    hdr_tcp *tcph = hdr_tcp::access(p);
    hdr_rtp* rh = hdr_rtp::access(p);

    printf("RxPktDocsisTimer:start(%lf)[channel:%d]: ERROR:  Timer already busy , current pkt size: %d, scheduled for %lf\n",
     Scheduler::instance().clock(),channelNumber_,ch->size(),(stime+rtime));
    if(ch->ptype_ == PT_TCP)
        printf(" ....TCP Sequence Num %d Ack No   %d\n",tcph->seqno(), tcph->ackno());
    if(ch->ptype_ == PT_UDP)
        printf(".....UDP Sequence Num %d \n", rh->seqno());
    exit(1);
  }


  p = myp;
#ifdef TRACEME
  printf("RxPktDocsisTimer:(%lf): starting the timer for this mac %s \n", Scheduler::instance().clock(), mac->docsis_id);
  fflush(stdout);
  printf(" (channel:%d, myp:%x, e->uid:%x) \n", channelNumber_,p,rxintr_.uid_);
  fflush(stdout);
#endif
 MacDocsisTimer::start((Packet *)&rxintr_,time);
}

/*************************************************************************
Receive Timer
*************************************************************************/
void RxPktDocsisTimer::handle(Event *e) 
{ 
  busy_ = 0;
  paused_ = 0;
  stime = 0.0;
  rtime = 0.0;
#ifdef TRACEME
  printf("RxPktDocsisTimer:(%lf): calling base class recvHandler %s myMed %x\n", Scheduler::instance().clock(), mac->docsis_id, mac->myMedium);
  fflush(stdout);
#endif
  myMedium->myChannel[channelNumber_].setRxStateIDLE();
  myMedium->recvHandlerMAC(p,channelNumber_,mac); //$A400
}

/*************************************************************************
Send Timer
*************************************************************************/
void TxPktDocsisTimer::handle(Event *e) 
{       
  busy_ = 0;
  paused_ = 0;
  stime = 0.0;
  rtime = 0.0;

#ifdef TRACEME
  printf("TxPktDocsisTimer:(%lf): channel:%d: calling sendHandler %s n", Scheduler::instance().clock(), channelNumber_,mac->docsis_id);
  fflush(stdout);
#endif
  mac->sendHandler(e);
}

void ChTxPktDocsisTimer::handle(Event *e) 
{       
  busy_ = 0;
  paused_ = 0;
  stime = 0.0;
  rtime = 0.0;

#ifdef TRACEME
  //$A401
  printf("ChTxPktDocsisTimer:handle(%lf): calling medium->recvHandler for MAC %s myMed %x  this %x\n", Scheduler::instance().clock(), mac->docsis_id,mac->myMedium,this);
  fflush(stdout);
#endif
  //mac->myMedium->myChannel[channelNumber_].setRxStateRECV();
  //mac->myMedium->recvHandler(p,element); //$A400
  myMedium->myChannel[channelNumber_].setRxStateRECV();
  myMedium->recvHandler(p,channelNumber_,element); //$A400
}


void ChTxPktDocsisTimer::start(Packet *myp,  double time)
{

//$A304
 if (busy_ == 1)
  {
    struct hdr_cmn *ch = HDR_CMN(p);
    hdr_tcp *tcph = hdr_tcp::access(p);
    hdr_rtp* rh = hdr_rtp::access(p);

    printf("ChTxPktDocsisTimer:start(%lf)[channel:%d]: ERROR:  Timer already busy , current pkt size: %d, scheduled for %lf, this new time : %lf \n",
     Scheduler::instance().clock(),channelNumber_,ch->size(),(stime+rtime),time);
    if(ch->ptype_ == PT_TCP)
        printf(" ....TCP Sequence Num %d Ack No   %d\n",tcph->seqno(), tcph->ackno());
    if(ch->ptype_ == PT_UDP)
        printf(".....UDP Sequence Num %d \n", rh->seqno());
    exit(1);
    
   }

 p = myp;
#ifdef TRACEME
  printf("ChTxPktDocsisTimer:(%lf): starting the timer for this mac %s (channel:%d, myp:%x,to fire: %lf) \n", 
		   Scheduler::instance().clock(), mac->docsis_id,channelNumber_,p, (double)(Scheduler::instance().clock()+time));
  fflush(stdout);
#endif
 MacDocsisTimer::start((Packet *)&txintr_,time);
}

/*************************************************************************
Start the  Timer
*************************************************************************/
void CmtsTxPktDocsisTimer::start(Packet *myp,  double time)
{
	//$A401
#ifdef TRACEME
  printf("CmtsTxPktDocsisTimer: starting the timer for mac %x\n",mac);
#endif

//$A304
 if (busy_ == 1)
  {
    struct hdr_cmn *ch = HDR_CMN(myp);
    hdr_tcp *tcph = hdr_tcp::access(myp);
    hdr_rtp* rh = hdr_rtp::access(myp);

    printf("CmtsTxPktDocsisTimer:start(%lf)[channel:%d]: ERROR:  Timer already busy , current pkt size: %d, scheduled for %lf \n",
     Scheduler::instance().clock(),channelNumber_,ch->size(),(stime+rtime));
    if(ch->ptype_ == PT_TCP)
        printf(" ....TCP Sequence Num %d Ack No   %d\n",tcph->seqno(), tcph->ackno());
    if(ch->ptype_ == PT_UDP)
        printf(".....UDP Sequence Num %d \n", rh->seqno());
    exit(1);
  }
  p = myp;
#ifdef TRACEME
  printf("CmtsTxPktDocsisTimer:(%lf): starting the timer for this mac %s (channel:%d, myp:%x,to fire: %lf) \n", 
		   Scheduler::instance().clock(), mac->docsis_id,channelNumber_,p, (double)(Scheduler::instance().clock()+time));
  fflush(stdout);
#endif
 MacDocsisTimer::start((Packet *)&txintr_,time);
}


/*************************************************************************
Send Timer
*************************************************************************/
void CmtsTxPktDocsisTimer::handle(Event *e) 
{       
  busy_ = 0;
  paused_ = 0;
  stime = 0.0;
  rtime = 0.0;
  
#ifdef TRACEME
  printf("CmtsTxPktDocsisTimer:handle(%lf): calling CmtsSendHandler for this mac %s (channel:%d, myp:%x, e->uid:%x) \n", Scheduler::instance().clock(), mac->docsis_id,channelNumber_,p,txintr_.uid_);
  fflush(stdout);
#endif
  myMedium->myChannel[channelNumber_].setTxStateIDLE();
  mac->myPhyInterface->TxCompletionHandler(p, channelNumber_);
//  mac->CmtsSendHandler(channelNumber_,p);
}

void CmTxPktDocsisTimer::start(Packet *myp,  double time)
{
 p = myp;
#ifdef TRACEME
  printf("CmTxPktDocsisTimer:(%lf): starting the timer for this mac %s (channel:%d, myp:%x,to fire: %lf) myMed %x this %x time %lf\n", 
		   Scheduler::instance().clock(), mac->docsis_id,channelNumber_,p, (double)(Scheduler::instance().clock()+time), mac->myMedium, this, time);
  fflush(stdout);
#endif
 MacDocsisTimer::start((Packet *)&txintr_,time);
}


/*************************************************************************
Send Timer
*************************************************************************/
void CmTxPktDocsisTimer::handle(Event *e) 
{       
  busy_ = 0;
  paused_ = 0;
  stime = 0.0;
  rtime = 0.0;
  
  //$A401
#ifdef TRACEME
  printf("CmTxPktDocsisTimer:handle:(%lf): calling sendHandler %s channelNumber_ %d  myMed %x this %x\n", Scheduler::instance().clock(), mac->docsis_id, channelNumber_, mac->myMedium, this);
  fflush(stdout);
#endif
  myMedium->myChannel[channelNumber_].setTxStateIDLE();
  mac->myPhyInterface->TxCompletionHandler(p, channelNumber_);
//  mac->CmSendHandler(channelNumber_,p);
}

/*************************************************************************

*************************************************************************/
void MapDocsisTimer::handle(Event *e)
{
  busy_ = 0;
  paused_ = 0;
  stime = 0.0;
  rtime = 0.0;
  
#ifdef TRACEME
  printf("MAP timer fired\n");
#endif
  mac->CmtsMapHandler(e);
}

/*************************************************************************

*************************************************************************/
void CmtsUcdDocsisTimer::handle(Event *e)
{
  busy_ = 0;
  paused_ = 0;
  stime = 0.0;
  rtime = 0.0;
  
  mac->CmtsUcdHandler(e);
}

/*************************************************************************

*************************************************************************/
void CmtsRngDocsisTimer::handle(Event *e)
{
  busy_ = 0;
  paused_ = 0;
  stime = 0.0;
  rtime = 0.0;
  
  mac->CmtsRngHandler(e);
}



/*************************************************************************

*************************************************************************/
void CmtsSyncDocsisTimer::handle(Event *e)
{
  busy_ = 0;
  paused_ = 0;
  stime = 0.0;
  rtime = 0.0;
  
  mac->CmtsSyncHandler(e);
}

//#ifdef RATE_CONTROL//-----------------------------------------------------
/*************************************************************************

*************************************************************************/
void CmtsTokenDocsisTimer::handle(Event *e)
{
  busy_ = 0;
  paused_ = 0;
  stime = 0.0;
  rtime = 0.0;
  
#ifdef TRACE_CMTS //--------------------------------------------------------------------
      printf("CmtsTokenDocsisTimer:Handler:(%lf) popped, set busy_ to 0 \n",
              Scheduler::instance().clock());
#endif //-------------------------------------------------------------------------------
  mac->CmtsTokenHandler(e);
}

void CmtsSFTokenDocsisTimer::handle(Event *e)
{
    busy_ = 0;
    paused_ = 0;
    stime = 0.0;
    rtime = 0.0;

#ifdef TRACE_CMTS //--------------------------------------------------------------------
      printf("CmtsSFTokenDocsisTimer:Handler:(%lf) popped, set busy_ to 0 \n",
              Scheduler::instance().clock());
#endif //-------------------------------------------------------------------------------
    mac->CmtsSFTokenHandler(e);
}
//#endif//------------------------------------------------------------------

/*************************************************************************

*************************************************************************/
void CmRngDocsisTimer::handle(Event *e)
{
  busy_ = 0;
  paused_ = 0;
  stime = 0.0;
  rtime = 0.0;
  
  mac->MACstats.total_num_mgt_pkts_US ++;
  mac->MACstats.total_num_rng_pkts_US ++;

#ifdef TRACEME
  printf("CmRngDocsisTimer(%lf): timer expired \n",
     Scheduler::instance().clock());
#endif
  mac->CmRngHandler(e);
}

/*************************************************************************

*************************************************************************/
void CmStatusTimer::handle(Event *e)
{
  busy_ = 0;
  paused_ = 0;
  stime = 0.0;
  rtime = 0.0;

  mac->MACstats.total_num_mgt_pkts_US ++;
  mac->MACstats.total_num_status_pkts_US ++;
  
#ifdef TRACEME
  printf("CmStatusTimer(%lf): status timer expired \n",
     Scheduler::instance().clock());
#endif
  mac->CmStatusHandler(e);
}

/*************************************************************************
 This is the handler hooks for the sequence hold timer (docsis 3.0) TOMMY
*************************************************************************/
void CmSeqTimer::handle(Event *e)
{
  busy_ = 0;
  paused_ = 0;
  stime = 0.0;
  rtime = 0.0;
  
  mac->CmSeqHandler(e);
}

/*************************************************************************

*************************************************************************/
void CmServiceFlowSendTimer::handle(Event *e)
{
  busy_ = 0;
  paused_ = 0;
  stime = 0.0;
  rtime = 0.0;
  
  mac->CmSndTimerHandler(e);
}

/*************************************************************************

*************************************************************************/
void CmServiceFlowRequestTimer::handle(Event *e)
{
  busy_ = 0;
  paused_ = 0;
  stime = 0.0;
  rtime = 0.0;
  
  mac->CmReqTimerHandler(e);
}

/***********************************************************************
*function: reseqTimer::reseqTimer() : baseTimer() 
*
*explanation:  This is the constructor for a resequencing Timer.
*
*inputs:
*
*
*outputs:
*
*
************************************************************************/
reseqTimer::reseqTimer(reseqMgr *myReseqMgrParam, reseqFlowListElement *myElementParam) : MacDocsisTimer() 
{
#ifdef TRACEME
    printf("reseqTimer: constructor\n");
    fflush(stdout);
#endif
  myReseqMgr = myReseqMgrParam;
  myElement = myElementParam;
}

reseqTimer::~reseqTimer()
{
}

void reseqTimer::start(Event *e, double time)
{
  Scheduler &s = Scheduler::instance();

  if (busy_ == 1)
  {
    printf("reseqTimer:start(%lf): ERROR:  Timer already busy , scheduled for %lf \n",
     Scheduler::instance().clock(),(stime+rtime));
    exit(1);
  }

#ifdef TRACEME
  printf("reseqTimer:(%lf): starting the timer  to fire: %lf) \n", 
		   Scheduler::instance().clock(), (double)(Scheduler::instance().clock()+time));
  fflush(stdout);
#endif

  busy_ = 1;
  paused_ = 0;
  stime = s.clock();
  rtime = time;
  assert(rtime >= 0.0);
  
#ifdef TRACEME
  printf("reseqTimer:start(%lf): schedule event at %lf \n",
     Scheduler::instance().clock(),rtime);
#endif

#ifdef TRACEME
  if(!e)
	  printf("reseqTimer::start::packet/event is NULL!!\n", Scheduler::instance().clock());
#endif
  s.schedule(this, e , rtime);

}


void reseqTimer::handle(Event *e) 
{       
  busy_ = 0;
  paused_ = 0;
  stime = 0.0;
  rtime = 0.0;
  
#ifdef TRACEME
  printf("reseqTimer:(%lf): calling Handler\n", Scheduler::instance().clock());
#endif
  myElement->reseqTimerHandler(e);
}


/***********************************************************************
*function: reseqTimer::reseqTimer() : baseTimer() 
*
*explanation:  This is the constructor for a resequencing Timer.
*
*inputs:
*
*
*outputs:
*
*
************************************************************************/
optimizationTimer::optimizationTimer(resourceOptimizer *myOptimizerP) : MacDocsisTimer() 
{
#ifdef TRACEME
    printf("reseqTimer: constructor\n");
    fflush(stdout);
#endif
  myOptimizer =  myOptimizerP;
}

optimizationTimer::~optimizationTimer()
{
}

void optimizationTimer::start(Event *e, double time)
{
  Scheduler &s = Scheduler::instance();

  if (busy_ == 1)
  {
    printf("optimizationTimer:start(%lf): ERROR:  Timer already busy , scheduled for %lf \n",
     Scheduler::instance().clock(),(stime+rtime));
    exit(1);
  }

#ifdef TRACEME
  printf("optimizationTimer:(%lf): starting the timer  to fire: %lf) \n", 
		   Scheduler::instance().clock(), (double)(Scheduler::instance().clock()+time));
  fflush(stdout);
#endif

  busy_ = 1;
  paused_ = 0;
  stime = s.clock();
  rtime = time;
  assert(rtime >= 0.0);
  
#ifdef TRACEME
  printf("optimizationTimer:start(%lf): schedule event at %lf \n",
     Scheduler::instance().clock(),rtime);
#endif

#ifdef TRACEME
  if(!e)
	  printf("optimizationTimer::start::packet/event is NULL!!\n", Scheduler::instance().clock());
#endif
  s.schedule(this, e , rtime);

}


void optimizationTimer::handle(Event *e) 
{       
  int rc = 0;
  busy_ = 0;
  paused_ = 0;
  stime = 0.0;
  rtime = 0.0;
  
#ifdef TRACEME
  printf("otpimizationTimer:(%lf): calling Handler\n", Scheduler::instance().clock());
#endif

  rc = myOptimizer->optimizerTimerHandler(e);
}




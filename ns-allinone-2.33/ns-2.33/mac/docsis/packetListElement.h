/************************************************************************
* File:  packetListElement.h
*
* Purpose:
*   Inlude files for the packet List Elements
*
* Revisions:
***********************************************************************/
#ifndef packetListElement_h
#define packetListElement_h

#include "packet.h"
#include "rtp.h"
#include "hdr-docsis.h"

#include "ListElement.h"

class packetListElement: public  ListElement
{

private:
  Packet *myPkt;
//This will be set to the SFObject's flowID


public:
  packetListElement();
  packetListElement(Packet *p);
  packetListElement(Packet *p, int flowID);
  virtual ~packetListElement();
  int putPkt(Packet *pkt); 
  Packet * getPacket();

  int flowID;
  double packetEntryTime;

};

#endif


/***************************************************************************
 * Module: bandwidthMgr
 *
 * Explanation:
 *   This file contains the class definition of the bandwidth manager.
 *
 * Revisions:
 *
 *  TODO:
 *
************************************************************************/

#ifndef ns_bandwidthMgr_h
#define ns_bandwidthMgr_h

#include "packet.h"
#include "rtp.h"
#include "hdr-docsis.h"

#include "bandwidthObject.h"

/*************************************************************************
 ************************************************************************/
class bandwidthMgr:
{
public:
  bandwidthMgr();

  init();


private:

  medium *myMedium;
   *myMedium;

  //list of bonding groups
  bandwidthObject *myBGs

};


#endif /* __ns_bandwidthMgr_h__ */

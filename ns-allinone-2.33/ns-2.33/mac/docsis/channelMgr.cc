/***************************************************************************
 * Module: channelMgr
 *
 * Explanation:
 *   This file contains the class definition of the channel manager.
 *   The channel manager interfaces the MAC to the lower layer
 *   medium.  ARQ can optionally be supported.
 *
 *   It does this by providing an abstraction for a channel.
 *
 *   For an outbound channel it provides the TxQs for each channel
 *   that is in the channel set.
 *
 *  For an inbound channel, it provides the Inbound queues to be used
 *  for either ARQ or resequencing.  
 *  
 *   
 *
 * Revisions:
 *
 *  TODO:
 *
************************************************************************/


#include "channelMgr.h"

#include "packetListElement.h"

//$A500
#include "medium.h"
#include "macPhyInterface.h"

#include "docsisDebug.h"
//#define TRACEME 0
//#define TRACEMEFILE 0

channelMgr::channelMgr()
{
}

channelMgr::~channelMgr()
{
}

void channelMgr::init(int numberChannelsParam, int maxQSizeParam, medium *myMediumParam, macPhyInterface *myMacPhyInterfaceParam)
{
  numberChannels  = numberChannelsParam;
  maxQSize = maxQSizeParam;
  //$A401 
  myChannels = (struct channelInfo *) malloc (numberChannelsParam * sizeof(struct channelInfo));  
  int i;
  myMedium = myMediumParam;
  myMacPhyInterface = myMacPhyInterfaceParam;


#ifdef TRACEME 
  printf("channelMgr::init:(%lf): numberChannels:%d, maxQSize:%d\n",
	  Scheduler::instance().clock(),numberChannels,maxQSize);
#endif 
  for (i=0;i<numberChannels; i++)
  {
    myChannels[i].channelNumber = i;
    myChannels[i].channelState = CHANNEL_IDLE;
    myChannels[i].myPacketList = new ListObj();
	myChannels[i].myPacketList->initList(maxQSizeParam);
#ifdef TRACEME 
  printf("channelMgr::init:(%lf): Current list size: %d\n",
	  Scheduler::instance().clock(),myChannels[i].myPacketList->getListSize());
#endif 
  }

}

int channelMgr::transmitFrame(Packet *p, int channelNumber, MacDocsis *myMacPtr)
{
int rc = 0;
  return rc;
}

int channelMgr::TxCompletionHandler(Packet *p, int channelNumber)
{
int rc = 0;
  return rc;
}

void channelMgr::setmaxQSize(int qSize)
{
  maxQSize = qSize;
}


int channelMgr::getChannelQSize(int channelNumber)
{
  int size;

  size =  myChannels[channelNumber].myPacketList->getListSize();
  return size;
}


void channelMgr::setState(int newState, int channelNumber)
{
    myChannels[channelNumber].channelState = newState;
}

/***********************************************************************
  *function: int channelMgr::getState(int channelNumber)
  *
  *explanation:
  *  This method returns the channel state.
  *
  *inputs:
  *
  *outputs:
  *   Returns CHANNEL_IDLE, CHANNEL_RECV, CHANNEL_SEND
  ************************************************************************/
int channelMgr::getState(int channelNumber)
{
  int channelState;

//  channelState =  myChannels[channelNumber].channelState;
  //JJM WRONG -  this is not right for CM
  channelState = myMedium->myChannel[channelNumber].getTxState();

  return channelState;
}


outboundChannelMgr::outboundChannelMgr()
{
}

outboundChannelMgr::~outboundChannelMgr()
{
}

int outboundChannelMgr::transmitFrame(Packet *p, int channelNumber, MacDocsis *myMacPtr)
{
int rc = 0;

#ifdef TRACEME 
  printf("outboundChannelMgr::transmitFrame:(%lf):channelNumber:%d\n",
	  Scheduler::instance().clock(),channelNumber);
#endif 

  myMedium->transmitFrame(p,channelNumber, myMacPtr);
#ifdef TRACEMEFILE
  FILE *fp;
  fp = fopen("CS.out", "a+");
  fprintf(fp, "%lf %d CHANNEL_SEND %d \n",
	  Scheduler::instance().clock(),channelNumber,channelMgr::getState(channelNumber));
  fclose(fp);
#endif


  return rc;
}

/***********************************************************************
*function: int outboundchannelMgr::TxCompletionHandler(Packet *p, int channelNumber)
*
*explanation:  
*  This method is invoked by the medium when the transmission is complete.
*
*  -Free the packet
*  -If there is another packet queued, begin the transmission
*  -If there is NOT another packet queued, set the state to IDLE and
*    invoke the higher layers TxCompletionHandler
*
*inputs:
*  Packet *p : reference to the packet that just completed transmission
*
*outputs:
*  Returns a 0 if succcessful else a 1
*
************************************************************************/
int outboundChannelMgr::TxCompletionHandler(Packet *p, int channelNumber)
{
int rc = 0;

//  myMacPhyInterface->TxCompletionHandler(channelNumber);
  return rc;
}


inboundChannelMgr::inboundChannelMgr()
{
}



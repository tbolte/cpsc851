/*******************************************************************
 * This file contains:
 *   -the DOCSIS frame formats (i.e., headers)
 *   -the map structure
 *   -the upstream flow table structure
 *   -All global enum and #defines needed by the code
 *   -The struct of the status msg sent by the CMs to the CMTS 
 *
 * Revisions:
 *  $A305:  add docsis extended header
 *  $A700
 *     u_int32_t total_num_cslots;     Total number of contention req slots that were observed 
 *     u_int32_t total_num_dslots;     Total number of grants (slots) that were for this CM 
 *  $A701
 *     int   numberBytesInNextConcat_;
 *  $A702
 *
 ******************************************************************/

#ifndef ns_hdr_docsis_h
#define ns_hdr_docsis_h    

/*===============INCLUDE HEADER FILES====================*/

#include <arp.h>
#include <connector.h>
#include <delay.h>      
#include <ll.h>
#include <mac.h>
#include "mac-docsistimers.h"
#include <marshall.h>   
#include <packet.h>     
#include <queue.h>
#include <random.h>

/*===============END INCLUDE HEADER FILES====================*/


/*================ENUM & CONSTANT DEFINITIONS================*/


enum UpSchedType 
  {
    UGS         = 0x0000,
    RT_POLL	= 0x0001,
    BEST_EFFORT = 0x0002
  };

enum PhsType 
  {
    SUPRESS_ALL	  = 0x0000,
    SUPRESS_TCPIP = 0x0001,
    SUPRESS_UDPIP = 0x0002,
    SUPRESS_MAC	  = 0x0003,
    NO_SUPRESSION = 0x0004
  };

enum EventType 
  {
    PKT_ARRIVAL      = 0x0000,
    MAP_ARRIVAL      = 0x0001,
    SEND_TIMER       = 0x0002,
    REQ_TIMER        = 0x0003,
    SEND_PKT         = 0x0004,
    SEND_UREQ        = 0x0006,
    CONTENTION_ON    = 0x0007,
    CONTENTION_SLOTS = 0x0008,
    CONTENTION_BKOFF = 0x0009,
    UNLOCK_QUEUE     = 0x00010,
    PIGGYBACK_REQ  =   0x00011

  };

#define	OFF                0
#define ON                 1
#define BROADCAST_SID      1

//#define DATA_PKT           0 /* Packet passed from IFQ(Upper layers) */
//#define MGMT_PKT           1 /* Packet generated at DOCSIS layer */

/* Need to redefine it later */
#define SUPRESS_ALL_SIZE          100
#define SUPRESS_TCPIP_SIZE         40
#define SUPRESS_UDPIP_SIZE         28
#define SUPRESS_MAC_SIZE           28

#define DATA_GRANT                  0
#define UREQ_GRANT                  1
#define CONTENTION_GRANT            2
#define SM_GRANT                    3
#define ETHER_HDR_LEN               ((ETHER_ADDR_LEN << 1) + ETHER_TYPE_LEN)
#define DOCSIS_HDR_LEN              6
#define DOCSIS_FRAG_OVERHEAD        16
#define MAX_NUM_IE                240 /* Max num of IEs in a MAP */
#define MAX_GRANT                 255 /* Max num of mini-slots that 
					 can be assigned per service-flow */

#define MAX_RETRY                  10
#define SIZE_MGMT_HDR              24
#define SIZE_MAP_HDR                4
#define MANAGEMENT_MSG_PAYLOAD_SIZE 5 /*Size of payload of mgmt msgs */
#define NUM_EXTENDED_HDR_ELEMENTS   2 /* Max num of EH elements in EH */
#define MAX_NUM_DOWNFLOWS_PERCM    10 /* Max num of downstream flows 
					 a CM can create */

#define MAX_NUM_UPFLOWS_PERCM            10 /* Max num of upstream flows 
					       a CM can create */
#define NUM_DOCSIS_LANS 		  5
#define MAX_NUM_CMS			500 /* Max num of CMs in a mac-domain*/
#define MAX_NUM_RETRIES			 15
#define FRAG_ENABLE_BIT			  8
#define CONCAT_ENABLE_BIT		  7
#define FRAG_ON_BIT			  6
#define CONCAT_ON_BIT			  5
#define PIGGY_ENABLE_BIT		  4
#define NO_PIGGY_BIT			  3
#define PIGGY_NOT_SEND			  2
#define CONCAT_THRESHHOLD		  1
#define QUEUE_THRESHOLD			 25
#define UGSSTATES       		  4
#define RTPOLLSTATES    		  6
#define BEFFORTSTATES   		  8

#define UGS_IDLE        		  0
#define UGS_DECISION    		  1
#define UGS_TOSEND      		  2
#define UGS_WAITFORMAP  		  3
#define RTPOLL_IDLE             	  0
#define RTPOLL_DECISION         	  1
#define RTPOLL_TOSEND           	  2
#define RTPOLL_WAITFORMAP       	  3
#define RTPOLL_TOSENDREQ        	  4
#define RTPOLL_REQSENT          	  5
#define BEFFORT_IDLE            	  0
#define BEFFORT_DECISION        	  1
#define BEFFORT_TOSEND          	  2
#define BEFFORT_WAITFORMAP      	  3
#define BEFFORT_TOSENDREQ       	  4
#define BEFFORT_REQSENT         	  5
#define BEFFORT_CONTENTION      	  6
#define BEFFORT_RATECHECK                 7


/*================END ENUM  & CONSTANT DEFINITIONS===================== */       


/* ==================================================================          
   FRAME STRUCTURE DECLARATIONS. 
   ==================================================================*/

/*************************************************************************
*  Docsis Extended Service Header for Downstream Sequencing 
*  $A305
*************************************************************************/
struct docsis_dsehdr
{
  	static int offset_;
  
  	inline static int& offset()	
  	{ 
    		return offset_; 
  	}
  
  	inline static docsis_dsehdr*	access(const Packet * p) 
  	{
    		return (docsis_dsehdr*) p->access(offset_);
  	} 

//	unsigned dsid			:	20;
//	unsigned sequence_change_count	:	1;
//	unsigned blank			:	3;
	u_int16_t dsid;
	unsigned char  sequence_change_count;
	u_int16_t packet_sequence_number;
};

/*************************************************************************
Docsis Channel-Level Abstraction Header -- this is solely to keep 
up with channel usage. This would not exist in the real world, and
should not be factored in any transmission packet size considerations.
*************************************************************************/
struct docsis_chabstr
{
  	static int offset_;
  
  	inline static int& offset()	
  	{ 
    		return offset_; 
  	}
  
  	inline static docsis_chabstr*	access(const Packet * p) 
  	{
    		return (docsis_chabstr*) p->access(offset_);
  	} 
        char docsis_identifier[10];
	u_int16_t channelNum;
};


/*************************************************************************
Docsis header format
*************************************************************************/
struct docsis_frame_hdr 
{
  u_char    fc_type    : 2;
  u_char    fc_parm    : 5;
  u_char    ehdr_on    : 1;
  u_char    mac_param;		
  u_int16_t len;
};  

enum fc_parm_Type 
  {
    TIMING         = 0x0000,
    MANAGEMENT     = 0x0001,
    REQUEST        = 0x0002,
    FRAGMENT       = 0x0003,
    STATUS         = 0x0004,
    CONCATENATION  = 26
  };

enum fc_type_Type 
  {
    DATA           = 0x0000,
    MAC_SPECIFIC   = 0x0003
  };

/*************************************************************************
Docsis Extended header element
 
  eh_type:  0x0: null setting
            0x01: piggyback request, len: 3, data[0]:number grants req, data[1,2]:SID
            0x03:  fragment : len 5 data: not used,next 2:  mask top 2 bits = sSID,
                            data 4:#minislots piggy req
                            data 5: 00ab cdef
                                   bits ab: first flag, last frag
                                  last 4 bits:  frag seq number, increm. each frag
*************************************************************************/
struct docsis_extended_header_element 
{
  u_char     eh_type	 : 4;
  u_char     eh_len	 : 4;
  u_int16_t  eh_data[6];
};

/*=====================END FRAME STRUCTURE DECLARATIONS==================*/


/*===============NEW PACKET HEADERS FOR NS============*/

/*************************************************************************
 Docsis packet header
*************************************************************************/
struct hdr_docsis 
{
  struct docsis_frame_hdr dshdr_;
  int dt_conv_overhead_; /* Overhead added to
			    docsis pkt to account 
			    for data convergence layer*/
  
  static int offset_;
  
  inline static int& offset() 
  {
    return offset_;
  }
  
  inline static hdr_docsis* access(const Packet * p) 
  {
    return (hdr_docsis*) p->access(offset_);
  }    
  
  /* per-field member functions */  
  struct docsis_frame_hdr& dshdr() 
  { 
    return dshdr_;
  }
  
  int& dt_conv_overhead() 
  { 
    return dt_conv_overhead_;
  }
};

/*************************************************************************
Docsis extended packet header
*************************************************************************/
struct hdr_docsisextd 
{  
  struct docsis_extended_header_element exthdr_[NUM_EXTENDED_HDR_ELEMENTS];
  
  int num_hdr;         /* Num of extended headers on */
  static int offset_;
  
  inline static int& offset()	
  { 
    return offset_; 
  }
  
  inline static hdr_docsisextd*	access(const Packet * p) 
  {
    return (hdr_docsisextd*) p->access(offset_);
  } 
  
  /* per-field member functions */
  inline int& num_hdr_() 
  { 
    return num_hdr;
  }
  
  struct docsis_extended_header_element* exthdr() 
  { 
    return exthdr_;
  } 
};

/*************************************************************************
Docsis management packet header
*************************************************************************/
enum Mgt_Message_Type 
{
  MAPMGTMESSAGE  = 0x0001,
  RANGEMESSAGE   = 0x0002,
  SYNCMESSAGE     = 0x0003,
  UCDMESSAGE     = 0x0004,
  CMUPDATEMESSAGE = 0x0005
};

struct hdr_docsismgmt 
{
  int dstaddr;
  int srcaddr;
  u_int16_t newDsid;
  u_char msg_payload_[MANAGEMENT_MSG_PAYLOAD_SIZE];
  
  /* 
     The last field will be used to carry	   
     either SYNC or RNG or REG msg 
  */
  u_char type; /* To distinguish between MAP & non-MAP messages */

  static int offset_;
  
  inline static int& offset()	
  { 
    return offset_; 
  }
  
  inline static hdr_docsismgmt*	access(const Packet * p) 
  {
    return (hdr_docsismgmt*) p->access(offset_);
  }
  
  /* per-field member functions */
  inline int& dstaddr_() 
  { 
    return dstaddr;
  }
	
  inline int& srcaddr_() 
  { 
    return srcaddr;
  }
	
  inline u_char& type_() 
  { 
    return type;
  }	
};

struct CMUpdate_docsismgmt 
{
  int numberCMs;
  u_char newScaleParam[MAX_NUM_CMS]; 
};

struct status_docsismgmt 
{
  int cm_id;
  int USByteCount;
  int DSByteCount;
  int USCollisionRate; //multiplied by 1000
  int currentBkOffScale;
  int USRAD;        //US request access delay - in usecs
  int USNRAD;       //US normalized access delay - in usecs
  int timeSinceLastStatus;      //number of microseconds since last sent a status
};

//This is allocated by the cmts to track cm status data
struct cm_status_data 
{
  double USBW;
  double DSBW;
  double CR;
  double USRAD;
  double USNRAD;
  int numberSamples;
  int currentScaleParam;
  double lastResultsCalculation;
};

//This is allocated by the cmts to compute the new scale params
struct cm_status_update_data 
{
  double USBW;
  double DSBW;
  double CR;
  double USRAD;
  double USNRAD;
  int prevScaleParam;
  int nextScaleParam;
  double lastUpdateTime;
};

/*************************************************************************
Docsis map packet header
The information elements will be send in DATA portion of packet
*************************************************************************/
struct hdr_docsismap 
{
  double allocstarttime;
  double allocendtime;
  double acktime;
  u_char numIE; 
  u_char bkoff_start;
  u_char bkoff_end;
  
  static int offset_;
 
  inline static int& offset()
  {
    return offset_;
  }
  
  inline static hdr_docsismap*	access(const Packet * p) 
  {
    return (hdr_docsismap*) p->access(offset_);
  }
  
  /* per-field member functions */
  
  inline double& allocstarttime_()
  {
    return allocstarttime;
  }
  
  inline double& allocendtime_()
  {
    return allocendtime;
  }
  
  inline double& acktime_()   
  {
    return acktime;
  } 
  
  inline u_char& numIE_()   
  {
    return numIE;
  }
  
  inline u_char& bkoff_start_()
  {
    return bkoff_start;
  }
  
  inline u_char& bkoff_end_()      
  {
    return bkoff_end;
  }
};


/*=======================END PACKET HEADER DECLARATIONS================*/


/*========================DATA STRUCTURES DECLARATIONS=================*/

/*=======================Common data structures=========================*/

/*************************************************************************
This defines the classifier to be used for matching packets to 
upstream/downstream flows 
*************************************************************************/
struct flow_classifier 
{
  int32_t  src_ip;
  int32_t  dst_ip;
  packet_t pkt_type;
  int32_t  flowID; 
};

/*************************************************************************
 This structure defines attributes of downstream service flow of a CM 
*************************************************************************/
struct downstream_flow_record 
{
  struct flow_classifier classifier;
  u_int16_t flow_id; /* Downstream service flow id */
  PhsType PHS_profile; /* PHS to be applied */
  u_int32_t BondingGroup;  /* identifies the BG number */

  //#ifdef RATE_CONTROL//-------------------------------------------------
  PacketQueue *tokenq_; /* Token bucket queue */
  /* Will not be used in CM code as of now */

  double tokens_;
  double rate_;
  int bucket_;
  int tokenqlen_;
  double lastupdatetime_;
  int init_;
  Event	intr;
  char ratecontrol; /* To indicate whether rate-control is ON or not */
  //#endif //-------------------------------------------------------------
  
  /* Per SID stats */
  double util_total_bytes_DS;
  int util_total_pkts_DS;
  int total_pkts_dropped;
  int dropped_tokenq;
  
  double dsq_delay; /* Queuing delay in DS queue */
  int num_qsamples; /* Num of samples used to calculate dsq queuing delay */
  int dropped_dsq;  /* Number of packets dropped in DS queue for this SID */

  u_int16_t SchedQType;
  int SchedQSize;
  u_int16_t SchedServiceDiscipline;
  int flowPriority;
  int minRate;
  double minLatency;
  u_int16_t reseqFlag;
  int reseqWindow;
  double reseqTimeout;
  int flowQuantum;
  double flowWeight;
  double MAXp;
  int    adaptiveMode;

//$A801
  u_int16_t dsid;
  
};

/*************************************************************************
This structure defines attributes of an upstream service flow of a CM node
*************************************************************************/
struct upstream_flow_record 
{
  struct flow_classifier classifier;
  UpSchedType sched_type; /* UGS, RT-VBR, 
			     BEST-EFFORT*/
  PhsType PHS_profile;    /* PHS to be applied */
  u_int32_t BondingGroup;  /* identifies the BG number */
  
  double ginterval;       /* Grant interval for UGS flow */
  
  u_int16_t flow_id;      /* Upstream service flow id */
  u_int16_t gsize;        /* Grant size(bytes) for UGS flow */
  u_char flag;

  u_int16_t SchedQType;
  int SchedQSize;
  u_int16_t SchedServiceDiscipline;
  int flowPriority;
  int minRate;
  double minLatency;
  int flowQuantum;
  double flowWeight;
  double MAXp;
  int    adaptiveMode;
};

/*======================= End Common data structures====================*/


/*=========================CMTS DATA STRUCTURES=========================*/

/*************************************************************************
This structure contains all the information excluding scheduling algorithm 
information   about a CM  at the CMTS node
*************************************************************************/
struct up_flow_record 
{
  struct flow_classifier classifier;

  Packet* frag_pkt;
  UpSchedType sched_type; /* UGS, RT-VBR, BEST-EFFORT*/
  PhsType PHS_profile;    /* PHS to be applied */
  
  double ginterval;       /* Grant interval for UGS flow */
  
  u_int32_t frag_data;
  u_int16_t flow_id;      /* Upstream service flow id */
  u_int16_t gsize;        /* Grant size(bytes) for UGS flow */
  u_int16_t seq_num;      /* The next seq_num for fragmentation*/
  u_char flag;            /* last 4 bit positions are used as
			     flags for concatenation/fragmentation */    
  u_int16_t SchedQType;
  int SchedQSize;
  u_int16_t SchedServiceDiscipline;
  int flowPriority;
  int flowQuantum;
  double flowWeight;
  double MAXp;
  int    adaptiveMode;
  int minRate;
  double minLatency;
};

/*************************************************************************

*************************************************************************/
struct cm_record 
{
  struct downstream_flow_record d_rec[MAX_NUM_DOWNFLOWS_PERCM];
  struct up_flow_record 	u_rec[MAX_NUM_UPFLOWS_PERCM];
  
  int cm_macaddr;
  u_int16_t priority; /* Priority for best-effort flow */ 
  
  u_char SizeDnFlTable;
  u_char SizeUpFlTable;
  u_char default_upstream_index_;
  u_char default_downstream_index_;
};


/*************************************************************************
 The frequency of generation of management messages 
*************************************************************************/
struct mgmt_conf_param 
{
  double sync_msg_interval;
  double rng_msg_interval;
  double ucd_msg_interval;
};

/*************************************************************************

*************************************************************************/
struct map_conf_param 
{
  double time_covered;            /* Time covered by MAP(in ms), 
				     0 inidcates varying, >0 indicates 
				     the actual time to be covered by MAP */
  
  double map_interval;            /* Interval between two maps(in s)*/
  
  u_int32_t num_contention_slots; /* Number of contention slots per map */  
  u_int32_t num_sm_slots;         /* Number of station maintainence 
				     slots per map */  
  u_int32_t sgrant_limit;         /* Max number of bytes that can be
				     sent in short grant IE */  
  u_int32_t lgrant_limit;         /* Max number of bytes that can be
				     sent in long grant IE */  
  u_char bkoff_start;             /* Initial back-off window for 
				     contention data and request */
  u_char bkoff_end;               /* Final back-off window for 
				     contention data and request */
};

/*************************************************************************
This structure contains all the configurable parameters of the CMTS node
*************************************************************************/
struct cmts_conf_param 
{
  struct mgmt_conf_param mgtparam;
  struct map_conf_param	mapparam;  
};

/*************************************************************************
Simulation output statistics variable
*************************************************************************/

struct mac_statistics 
{

  u_int32_t total_num_sent_frames; 
  double total_num_sent_bytes; 

  double total_num_rx_bytes; 
  u_int32_t total_num_rx_frames; 
  u_int32_t total_num_rx_PDUs; 
  
  double total_num_BW_bytesUP;   /* total Num of bytes for BW calculation*/
  double total_num_BW_bytesDOWN; /* total Num of bytes for BW calculation*/


  
  double total_num_appbytesUS;   
  double total_num_appbytesDS;   

  u_int32_t total_num_appPktsUS;   
  u_int32_t total_num_appPktsDS;   
  
  /* total packets dropped for cmts, this is downstream, for cm, this is upstream*/
  u_int32_t total_packets_dropped;
  u_int32_t dropped_tokenq;
  
  double last_BWCalcTime;            /* Indicates the last time the BW was computed*/
  
  double last_rtime;                 /* Indicates the last time the avg was taken */
  

  u_int32_t total_num_mgt_pkts_US; /* total Num of mgt received  */
  u_int32_t total_num_rng_pkts_US; 
  u_int32_t total_num_status_pkts_US; 
  u_int32_t total_num_concat_pkts_US; 
  u_int32_t total_num_frag_pkts_US; 
  u_int32_t total_num_req_pkts_US; 
  u_int32_t total_num_plaindata_pkts_US; 
  u_int32_t total_num_concatdata_pkts_US; 
  u_int32_t total_num_frames_US; 
  u_int32_t total_num_BE_pkts_US; 
  u_int32_t total_num_RTVBR_pkts_US; 
  u_int32_t total_num_UGS_pkts_US; 
  u_int32_t total_num_OTHER_pkts_US; 
  
  u_int32_t avg_mgmtpkts;
  u_int32_t num_mgmtpkts;            /* Num of packets received per sec */
  u_int32_t avg_mgmtbytes;
  u_int32_t num_mgmtbytes;           /* Num of packets received per sec */
  double last_mmgmttime;             /* Indicates the last time the avg was taken */	
};

struct cm_statistics 
{
  double total_BW_bytesUP_status;
  double total_BW_bytesDOWN_status; /*count for status msg */
  double total_pktsSentUP_status;
  int total_collisions_status; /*count for CR msg */
  //Note: the real avg_req_stime is in the UpFlowTable.  We want aggregate stats
  int avg_req_stimeBYTES_status;    /* number of bytes counter for each request time sample */
  double avg_req_stime_status;      /* Avg time for
				                   a request to be serviced*/
  int num_delay_samples_status; /* Num of delay samples, used to 
		                        compute the avg access delay */

//$A700
  u_int32_t total_num_cslots;    /* Total number of contention req slots that were observed */
  u_int32_t total_num_dslots;    /* Total number of grants (slots) that were for this CM */
//$A1
/* Num of delay samples, used to compute the avg access delay */
  u_int32_t  channelDelayCountSamples; 
/* Avg time for a request to be serviced*/
  double channelDelayCount;
  double channelDelayBytes;
  

  u_int32_t total_num_req_denied;
  u_int32_t total_num_frag;
  u_int32_t total_num_collisions;
  u_int32_t total_num_bkoff;
		   
  u_int32_t total_collision_drops; /* Total number of packets dropped due 
											                         to transmission attempt exceeding 16 */

  u_int32_t total_queue_drops;     /* Total number of packets dropped due 
		                       to service-flow queue overflow */

};

struct cmts_statistics 
{
  double avg_interdpr_map;       /* Avg interdeparture between MAPs*/  
  u_int32_t avg_datagrants;      /* per MAP*/  
  u_int32_t num_req;             /* Number of requests for bandwidth per sec*/ 
  u_int32_t avg_contentionslots; /* Per map*/  
  u_int32_t data_sent;	         /* bytes sent per sec*/  
  u_int32_t fragmented_count;    /* Number of fragmented frames received*/  
  u_int32_t concatenation_count; /* Number of concatenated frames received*/  
  u_char num_IE;                 /* per MAP */  
  u_int32_t num_creq;            /* Num of contention request received */
  u_int32_t num_piggyreq;        /* Num of piggy request received */
  u_int32_t num_creqgrant;       /* Num of contention requests granted*/
  u_int32_t num_ureqgrant;       /* Num of Unicast (rtPS) requests granted*/
  u_int32_t num_preqgrant;       /* Num of partial requests granted*/
  u_int32_t num_creqdenied;      /* Num of contention request denied*/
  u_int32_t num_ureqdenied;      /* Num of unicast requests denied*/
  u_int32_t total_num_cslots;    /* Total number of contention slots that 
				    were assigned over the simulation */
  u_int32_t total_num_mgmtslots; /* Total number of management slots that 
				    were assigned over the simualtion */  
};

/*************************************************************************
Scheduling algorithm data structures
*************************************************************************/

typedef struct job* jptr;

struct job 
{
  double release_time;
  double period;
  double deadline;           /* Request should be satisfied by this time */
  u_int32_t mini_slots;      /* Number of minislots requested */
  jptr next;                 /* Next job in the queue */
  u_int16_t flow_id;         /* Flow id of requesting CM */
  UpSchedType sclass;        /* UGS or RT-VBR or BEST-EFFORT */
  u_char type;               /* Type of request, DATA or Unicast req slots */
  u_char flag;               /* To delete or not..*/
  u_char retry_count; 
  double ugsjitter;          /* The jitter in the grant, valid only 
				for UGS flows */
  int    jitterSamples;
  double last_jittercaltime; /* Last time avg jitter was calculated */
};

/*************************************************************************

*************************************************************************/
typedef struct map_list * mapptr;

struct map_list 
{
  double alloc_stime;   /* when can the allocation be started in MAP*/
  double alloc_etime;   /* when can the allocation be ended in MAP*/
  double release_time;
  double deadline;      /* Request should be satisfied by this time */
  u_int32_t nslots;
  mapptr next;
  u_int16_t flow_id;
  u_int16_t  flag;      /* To indicate whether this is hole or not 
			   This is the IUC*/
};

/*************************************************************************
List of token timer requests 
*************************************************************************/
//#ifdef RATE_CONTROL //-------------------------------------------------
typedef struct token_timer_list* tkptr;

struct token_timer_list 
{
  double expiration_time; /*stime + rtime */
  tkptr	next;
  int cindex;	/* cmindex of the DownFlowTable for which 
		   Token timer has been set */
  
  int findex;	/* flindex of the DownFlowTable for which 
		   Token timer has been set */
};

/*************************************************************************
Packet queue structure 
*************************************************************************/
typedef struct cmtsqnode * qlist;

struct cmtsqnode 
{
  Packet *pkt;
  double enq_time; /* Time at which this pkt was enqued */
  int cindex;
  int findex;
  qlist next;
};
//#endif //--------------------------------------------------------------

/*=======================END CMTS data structures=========================*/


/*=======================CM data structures=========================*/
/*************************************************************************

*************************************************************************/
typedef struct allocation_time * aptr;

struct allocation_time 
{
  double start_time;
  double end_time;
  u_int32_t num_slots;
  aptr next;
  u_char type; /* 0- Data, 1 - Unicast Req, 2- Contention-req */
  u_char used; /* Indicates whether this slot has already been 
		  allocated/used to some one */   
};

/*************************************************************************

*************************************************************************/
typedef struct snd_timer_list* sptr;

struct snd_timer_list 
{
  double expiration_time; /* stime + rtime */
  sptr next;
  u_char tindex; /* index of the UpFlowTable for which 
		    SEND timer has been set */
};

/*************************************************************************

*************************************************************************/
typedef struct req_timer_list* rptr;

struct req_timer_list 
{
  double expiration_time; /* stime + rtime */
  rptr next;
  u_char rindex; /* index of the UpFlowTable for which 
		    REQ timer has been set */
};

/*************************************************************************

*************************************************************************/
typedef struct pnode * plist;

struct pnode 
{
  Packet * pkt;
  double enq_time; /* Time at which this pkt was enqued */
  plist next;
};

/*************************************************************************

*************************************************************************/
struct upstream_sflow 
{
  struct upstream_flow_record upstream_record;
  struct allocation_time* alloc_list;
  //PacketQueue  *q_;
  plist	packet_list;
  u_int16_t max_qsize;       /* variable indicates the maximum
				queue size */
  Packet* pkt;
  Packet* frag_pkt;
//This is the untouched PKT....
  Packet* OriginalFragPkt;
  
  char debug;                /* To debug the flow */
  
  double map_acktime;
  double req_time;           /* Time when a request for 
				bandwidth has been sent */
  
  double avg_queuing_delay;  /* Avg queuing delay 
			        per packet for this service flow*/
  u_int32_t queuing_samples; /* Num of samples used to compute the 
				avg queuing delay */
  
  double avg_req_stimeBYTES;    /* number of bytes counter for each request time sample */
  
  double avg_req_stime;      /* Avg time for
				a request to be serviced*/
  
  double enque_time;         /* Time at which the packet being sent now 
				was enqued, used to compute access delay */
  
  u_int32_t  num_delay_samples; /* Num of delay samples, used to 
				   compute the avg access delay */


//$A1
/* Num of delay samples, used to compute the avg access delay */
  u_int32_t  channelDelayCountSamples; 
/* Avg time for a request to be serviced*/
  double channelDelayCount;
  double channelDelayBytes;
  
//$A701
  u_int32_t curr_reqsize;      /* This denotes how much the CM requested */
  u_int32_t curr_gsize;      /* This denotes how much data(in bytes) could be 
				sent when the SEND timer expires */
  
  u_int32_t num_pkt_snt; /* Number of packets for
			    which request for banwidth
			    has to be sent */
  u_int32_t drop_count;
  
  u_int32_t seq_num;
  u_int32_t frag_data;
  double    last_mfrtime;
  u_int32_t avg_pkts;    /* Avg number of frames
			    sent per sec */
  u_int32_t avg_bytes;   /* Avg number of bytes/s */ 
  u_int32_t num_bytes; 
  u_int32_t num_pkts;  
  
  /* STATISTICS */
  u_int32_t SID_num_sent_bytes; 
  u_int32_t SID_num_sent_pkts;  
  
  u_int32_t avg_slotspermap;     /* Avg number of slots
					per map */
  u_int32_t totalACKs;         /* Total num of TCP acks that arrived to be sent */
  u_int32_t totalACKsFiltered; /* Total num of TCP acks that were filtered/removed */
  
  u_int32_t total_piggyreq;      /* Total num of piggy req sent */
  u_int32_t total_creq;          /* Total num of contention req sent */
  
  u_int32_t total_fcoll;         /* Total num of first collisions */
  u_int32_t avg_fcont;           /* avg num of slots skipped during first 
				                    contention back-off */
  u_int32_t fcont_count;         /* Number of first contentions done */
  u_int32_t avg_TotalBackoff;           /* avg num of slots skipped during all bkoffs*/
  u_int32_t avg_TotalBackoffCount;         /* Number of backoffs sampled */

  u_int32_t avg_Backoff;           /* avg num of slots skipped during all bkoffs*/
  u_int32_t avg_BackoffTmp;      /* tmp count of # slots skipped for this incident */
  u_int32_t avg_BackoffCount;         /* Number of avg_Backoff samples */
  u_int32_t avg_BackoffTmpCount;      /* tmp count of # slots skipped for this incident */
  
//$A701
  u_int32_t total_drops_fragmentation; /* Total number of packets dropped
                                          because the CMTS issues a partial
                                          grant but the cm does not fragment*/

  u_int32_t total_collision_drops; /* Total number of packets droppeddue to 
					transmission attempt exceeding 16 */
  u_int32_t total_queue_drops;     /* Total number of packets dropped due to 
					service-flow queue overflow */
  u_int32_t total_num_req_denied; /* Total number of req denied by cmts */ 

  int cmts_addr;
  u_int16_t max_concat_threshhold;
  
  u_char state;                /* The current state for this service flow */

  u_int32_t bk_offwin;         /* Current bk-off window */
  u_int32_t bk_offcounter; 
  u_int32_t bk_offend; 
  u_int32_t bk_offstart;
  u_char    num_retries;
  u_char    max_retries; 
  u_char    contention_on;     /* (For best-effort flows only), 
					Indicates whether a req 
					has been sent in contention-slots */ 

  u_char    pending;           /* Denotes whether any data grant 
				  is pending for this flow */  
  Event	intr;
  Event rintr;

  double ugsjitter;         /* The jitter in the grant, 
				  valid only for UGS flows */
  int    jitterSamples;
  double last_granttime;    /* Last grant observed at this time
			       (to make sure that we do not use the 
			       same data grant again in the calculation) */
  
  double nominal_alloctime;    /* Grant shud have ben allocated at this time */
  double last_jittercaltime;   /* Last time avg jitter was calculated */
  double   acceptance_rate;
  double   prev_acceptance_rate;
  float    wt_factor;
  u_int32_t  num_slots_req;
  double     tokens_;
  double     rate_;
  int        bucket_;
  double     lastupdatetime_;
  int        init_;
  char  ratecontrol; /* To indicate whether rate-control is ON or not */
  int        numberPacketsInNextConcat_;
//$A701
  int        numberBytesInNextConcat_;
  int totalPacketsInConcatFrames;
  int totalConcatFrames;
};

/*************************************************************************
To store the Upstream flow information so that it could be sent to CMTS 
during the Registration Phase
*************************************************************************/
struct downstream_sflow 
{
  struct downstream_flow_record downstream_record;
};

/*=======================END CM data structures=========================*/

/*======================END DATA STRUCTURES DECLARATIONS==================*/

#endif /* end __hdr_docsis_h */















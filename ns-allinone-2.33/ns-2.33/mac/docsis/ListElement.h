/************************************************************************
* File:  ListElement.h
*
* Purpose:
*   Inlude files for the List Elements
*
* Revisions:
***********************************************************************/
#ifndef ListElement_h
#define ListElement_h


#define VALIDSAMPLE 1

class ListElement {

private:
  void *data;

public:
  ListElement();
  virtual ~ListElement();
  virtual int putData(void* DataParm); 
  virtual void * getData();
  ListElement *next;
  ListElement *prev;

};

class IntegerListElement {

private:
  int data;

public:
  IntegerListElement();
  virtual ~IntegerListElement();
  int putData(int DataParm); 
  int getData();
  IntegerListElement *next;
  IntegerListElement *prev;

};

class OrderedListElement : public ListElement 
{

private:

public:
  OrderedListElement();
  ~OrderedListElement();
  int getKey();
  int putKey(int key);
  int key;

};

#endif


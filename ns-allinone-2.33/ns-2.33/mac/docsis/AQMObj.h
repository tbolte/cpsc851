/************************************************************************
* File:  AQMObj.h
*
* Purpose:
*   Inlude files for a base list object. A AQMObj is a container
*   of AQM Elements.
*
*
* Revisions:
*
***********************************************************************/
#ifndef AQMObj_h
#define AQMObj_h

#include "ListObj.h"

class AQMObj : public ListObj {

public:
  virtual ~AQMObj();
  AQMObj();
  void initList(int maxAQMSize);
  void setAQMParams(int maxAQMSize, int minth, int maxth);
  virtual int addElement(ListElement& element);
  virtual int addElementatHead(ListElement& element);
  virtual ListElement *removeElement();    //dequeue and return
  virtual void adaptAQMParams();

protected:


private:

};


#endif

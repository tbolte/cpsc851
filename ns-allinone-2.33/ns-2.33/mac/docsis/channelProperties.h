/*************************************************************************** * Module: channelProperties
 *
 * Explanation:
 *   This file contains the class definition of the object that
 *   packages channel properties
 *
 * Revisions:
 *   
 *
************************************************************************/

#ifndef ns_channelProperties_h
#define ns_channelProperties_h



/*************************************************************************
 ************************************************************************/
struct channelPropertiesType
{
  int channelID;
  int channelMode;  //Upstream, Downstream,  both 
  u_int32_t channelCapacity;       /* (bits/sec) */
  u_int32_t bytes_pminislot;       
  double propDelay;
  double FECPercentage;   //The percentage of OH consumed by FEC
  u_int32_t maxBurstSize;
  u_int32_t maxFrameSize;
  u_int32_t overheadBytes;
  u_char ticks_p_minislot;
  double errorModelLossRate1;     
  double errorModelLossRate2;     
};

#endif /* __ns_channelProperties_h__ */

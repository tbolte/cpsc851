/************************************************************************
* File:  reseqFlowListElement.h
*
* Purpose:
*   Inlude file for the reseq flow  List Elements
*   This is a containing to hold all information associated with
*   service flows for arriving packets (arriving from the cable network)
*   so that resequencing can be performed.
*
* Revisions:
***********************************************************************/
#ifndef reseqFlowListElement_h
#define reseqFlowListElement_h

#include "packet.h"
#include "rtp.h"
#include "hdr-docsis.h"

#include "serviceFlowListElement.h"
#include "ListObj.h"
#include "serviceFlowObject.h"

class  reseqTimer;
class  reseqMgr;

//class reseqFlowListElement: public  serviceFlowListElement
class reseqFlowListElement: public  OrderedListElement
{

public:
  reseqFlowListElement();
  reseqFlowListElement(serviceFlowObject *mySF);
  virtual ~reseqFlowListElement();

  int putServiceFlow(serviceFlowObject *SFObjParam);
  serviceFlowObject * getServiceFlow();

  void init(reseqMgr *myReseqMgrParam, int numberChannels);

  int reseqTimerHandler(Event *e);

  double gapStartTime;
  u_int32_t gapCount;
  u_int32_t frameCount;
  u_int32_t outOfOrderArrivalCount;
  void printStatsSummary();
  int nextExpectedPSN;
  int reseqState;
  u_int32_t numberTimerHandlerEvents;

  serviceFlowObject *mySF;

  Event reseqEvent;
  reseqTimer *myReseqTimer;
  reseqMgr   *myReseqMgr;

  int numberChannelsAssigned;
  char *outOfOrderChannels;

private:





};

#endif


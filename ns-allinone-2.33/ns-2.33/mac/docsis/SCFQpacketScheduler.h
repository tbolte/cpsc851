/***************************************************************************
 * Module:  pSCFQpacketScheduler  Object
 *
 * Explanation:
 *   This file contains the class definition for the base packet scheduler
 *   and for several specific types of PS's.
 *
 * Revisions:
 *
 *  TODO:
 *
************************************************************************/

#ifndef ns_pSCFQpacketSchedulerObject_h
#define ns_pSCFQpacketSchedulerObject_h


#include "ranvar.h"
#include "packet.h"
#include "globalDefines.h"

#include "packetScheduler.h"


#define IDLE 100000000
#define CHANNEL_CAPACITY 38425000


class pSCFQpacketSchedulerObject: public packetSchedulerObject
{
public:
  pSCFQpacketSchedulerObject();
  ~pSCFQpacketSchedulerObject();

  void init(int serviceDiscipline, serviceFlowMgr*, bondingGroupMgr *);
  Packet *selectPacket(int channelNumber, struct serviceFlowSet *mySFSet); 
  int  selectChannel(Packet *pkt, serviceFlowObject *mySFObj, struct channelSet *myChannelSet, int *channelSelection);

private:

  double virTime[MAX_CHANNELS];
  double serviceTags[MAX_CHANNELS][MAX_ALLOWED_SERVICE_FLOWS];
  int channel_load[MAX_CHANNELS];

  UniformRandomVariable *pkt_rng;
  
  double getTxTime(serviceFlowObject *tmpSFObj, int channelNumber);

};

#endif /* __ns_pSCFQpacketSchedulerObject_h__ */

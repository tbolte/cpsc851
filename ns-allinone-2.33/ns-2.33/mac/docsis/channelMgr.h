/***************************************************************************
 * Module: channelMgr
 *
 * Explanation:
 *   This file contains the class definition of the channel manager.
 *   The channel manager interfaces the MAC to the lower layer
 *   medium.  ARQ can optionally be supported.
 *
 *   It does this by providing an abstraction for a channel.
 *
 *   For an outbound channel it provides the TxQs for each channel
 *   that is in the channel set.
 *
 *  For an inbound channel, it provides the Inbound queues to be used
 *  for either ARQ or resequencing.  
 *  
 *   
 *
 * Revisions:
 *
 *
************************************************************************/

#ifndef ns_channelMgr_h
#define ns_channelMgr_h

#include "mac-docsis.h"

#include "ListElement.h"
#include "ListObj.h"

//$A500
//#include "medium.h"
//#include "macPhyInterface.h"
class medium;
class macPhyInterface;


//#define CHANNEL_IDLE 0
//#define CHANNEL_SEND 1 
//#define CHANNEL_RECV 2



struct channelInfo {
	int channelNumber;
	int channelState;
//	TxPktDocsisTimer *TxPktTimer;
	ListObj *myPacketList;

};

/*************************************************************************
 ************************************************************************/
class channelMgr {


public:
  channelMgr();
  virtual ~channelMgr();

  virtual void init(int numberChannels,int maxQSizeParam, 
      medium *myMediumParam, macPhyInterface *myMacPhyInterfaceParam);
  void setmaxQSize(int qSize);
  int getChannelQSize(int channelNumber);
  virtual int transmitFrame(Packet *p, int channelNumber, MacDocsis *mymacPtr);
  int TxCompletionHandler(Packet *p, int channelNumber);

  void setState(int newState, int channelNumber);
  int getState(int channelNumber);
  struct channelInfo *myChannels;
  int maxQSize;
  medium *myMedium;
  macPhyInterface *myMacPhyInterface;


private:

  int numberChannels;
};

/*************************************************************************
 ************************************************************************/
class outboundChannelMgr: public  channelMgr
{
public:
  outboundChannelMgr();
  ~outboundChannelMgr();
  int transmitFrame(Packet *p, int channelNumber, MacDocsis *myMacPtr);
  int TxCompletionHandler(Packet *p, int channelNumber);

private:

};


/*************************************************************************
 ************************************************************************/
class inboundChannelMgr: public channelMgr
{
public:
  inboundChannelMgr();

private:

};

#endif /* __ns_channelMgr_h__ */

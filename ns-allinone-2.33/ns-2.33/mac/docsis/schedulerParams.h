
class DSSchedulerParams {
    public:
    	static inline DSSchedulerParams* instance() {
    	    return _instance;
    	}
    	
    	static void init();
    	
    	inline int getMode() { return mode; }

        inline int getMonitorPeriod() { return monitorPeriod; }

        inline float getPortCapacity() { return portCapacity; }
        inline float getNearCongestionStateThreshold() { return nearCongestionStateThreshold; }
        inline float getExtendedHighConsumptionStateThreshold() { return extendedHighConsumptionStateThreshold; }
        //inline float setExtendedHighConsumptionStateThreshold(float t) { extendedHighConsumptionStateThreshold = t; }
        inline float getExtendedHighConsumptionStateExitThreshold() { return extendedHighConsumptionStateExitThreshold; }
        //inline float setExtendedHighConsumptionStateExitThreshold(float t) { extendedHighConsumptionStateExitThreshold = t; }
        inline float getSubscriberProvisionedBW() { return subscriberProvisionedBW * 1000000; } // returns BW in bps
        inline float getPriorityAllocation() { return priorityAlloc; }
        inline int isAdaptiveFS() { return adaptiveFS; }
        inline float getTargetQL() { return targetQL; }
        inline float getTargetDL() { return targetDL; }
        inline int isMultiTier() { return multitier; }
        inline float getHiPriQueueDelayTarget() {return HiPriQueueDelayTarget; }
        inline float getLowPriQueueDelayTarget() {return LowPriQueueDelayTarget; }
        inline float getHiPriInterval() {return HiPriInterval; }
        inline float getLowPriInterval() {return LowPriInterval; }

    private:
    	static DSSchedulerParams* _instance;
    	
    	DSSchedulerParams() {
    	    mode = -1;
            
            monitorPeriod = 5;
            //$A901 monitorPeriod = 60;
            
            // fairshare default params
            portCapacity = 38000000.0;
            nearCongestionStateThreshold = 0.8;
            extendedHighConsumptionStateThreshold = 0.7;
            extendedHighConsumptionStateExitThreshold = 0.5;
            subscriberProvisionedBW = 5.5;
            //$A901 subscriberProvisionedBW = 10.0;
            priorityAlloc = 0.9;
            adaptiveFS = 0;
            targetQL = 0.0;
            targetDL = 0.0;
            multitier = 0;
            HiPriQueueDelayTarget = 0.020;
            LowPriQueueDelayTarget = 0.250;
            HiPriInterval = 0.100;
            LowPriInterval = 0.100;
    	}
    	
    	int mode;		// DRR,  Fairshare as set by TCL param 'DSSchedMode'

        int monitorPeriod;      // in seconds

        // fairshare
        float portCapacity;                                     // Port capacity (bonding group aggregate channel capacity)
        float nearCongestionStateThreshold;                     // 0 ~ 1.0 (e.g., 0.8 for 80% of capacity)
        float extendedHighConsumptionStateThreshold;            // 0 ~ 1.0 (e.g., 0.7 for 70% of provisioned BW)	
                                                                // we are considering to let this param to go beyond 1.0
                                                                // for adaptive mode -- the rationale is if the subscriber 
                                                                // provision BW is relatively low
        float extendedHighConsumptionStateExitThreshold;        // 0 ~ 1.0 (e.g., 0.5 for 50% of provisioned BW)
        float subscriberProvisionedBW;                          // provisioned BW in Mbps
                                                                // (in theory, each subscriber/flow has its own provisioned BW)
        float priorityAlloc;                                    // 0 ~ 1.0 (e.g., 0.9 for 90% allocated to PBE queue)
                                                                // we use WDRR to server PBE and BE queues
        int adaptiveFS;                                         // 1 or 0 to indicate if enabling adaptive Fairshare algorithm
        float targetQL;                                         // target queue level for high priority queue
                                                                // 0 for default operational mode (targeting half of the queue size)
                                                                // < 1.0	targeting ratio of the queue size (e.g., 0.07 for 7% of the queue size)
                                                                // >= 1		targeting this number of packets in the queue
        float targetDL;                                         // target delay in seconds for high priority queue
                                                                // 0 for default operational mode (no delay specified)
                                                                // when set, this parameter overrides targetDL setting if any
        int multitier;                                          // 1 or 0 to indicate if multi-tier services should be enabled
        float HiPriQueueDelayTarget;
        float LowPriQueueDelayTarget;
        float HiPriInterval;
        float LowPriInterval;

};


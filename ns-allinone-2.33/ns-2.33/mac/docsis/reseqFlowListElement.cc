/************************************************************************
* File:  reseqFlowListElement.cc
*
* Purpose:
*   Class for the reseq Flow List Element object
*
* Revisions:
***********************************************************************/

#include "reseqFlowListElement.h"

#include "serviceFlowObject.h"

#include "mac-docsistimers.h"
#include "reseqMgr.h"

#include "docsisDebug.h"
//#define TRACEME 0

/***********************************************************************
*function: reseqFlowListElement::reseqFlowListElement
*
*explanation:
*
*inputs:
*
*outputs:
************************************************************************/
reseqFlowListElement::reseqFlowListElement() : OrderedListElement()
{
#ifdef TRACEME
  printf("reseqFlowListElement: constructed reseq Flow List Element \n");
#endif
  mySF = NULL;
}

reseqFlowListElement::reseqFlowListElement(serviceFlowObject *mySFObjParam) : OrderedListElement()
{
#ifdef TRACEME
  printf("reseqFlowListElement: constructed service Flow List Element \n");
#endif
  mySF = mySFObjParam;

}



/***********************************************************************
*function: reseqFlowListElement::~reseqFlowListElement
*
*explanation:
*
*inputs:
*
*outputs:
************************************************************************/
reseqFlowListElement::~reseqFlowListElement()
{
  free(outOfOrderChannels);
}

/***********************************************************************
*function: void reseqFlowListElement::init(reseqMgr *myReseqMgrParam)
*
*explanation:
*
*inputs:
*
*outputs:
************************************************************************/
void reseqFlowListElement::init(reseqMgr *myReseqMgrParam, int numberChannels)
{

#ifdef TRACEME
  printf("reseqFlowListElement:init \n");
#endif

  gapStartTime=0;
  nextExpectedPSN = 0;
  gapCount=0;
  frameCount = 0;
  reseqState = ReseqStateIDLE;
  numberTimerHandlerEvents=0;

  myReseqMgr = myReseqMgrParam;

  numberChannelsAssigned = numberChannels;
  outOfOrderChannels = (char *) malloc (numberChannels * (sizeof (char)));
  bzero( (void *) outOfOrderChannels, (numberChannels * (sizeof (char))));
  
  myReseqTimer = new reseqTimer(myReseqMgrParam, this);
}


/***********************************************************************
*function: int reseqMgr::reseqTimerHandler(reseqFlowListElement *myElementParam)
*
*explanation:
*  This is called when a resequencing timeout occurs.
*  The following algorithm is run to determine
*  what packets are to be dropped and 
*  what packets are to be passed to the higher layers
*
*   WAIT_TIMER_HANDLER:
*             Begin loop:
*               nextExpectedPSN++
*               nextPkt=lowestPSN pkt in the queue
*               if nextPktPSN == nextExpectedPSN
*                 pass up the pkt, nextExpectedPSN++
*               else
*                 if queueSize>0 reset WAIT_TIMER
*                 break
*             End loop
*
*
*inputs:
*  reseqFlowListElement *myElementParam
*
*outputs:
*  returns a  SUCCESS or FAILURE
*
************************************************************************/
int reseqFlowListElement::reseqTimerHandler(Event *e)
{
  int rc = SUCCESS;
  double curr_time = Scheduler::instance().clock();
  int i;
  int queueSize = mySF->packetsQueued();
  docsis_dsehdr *chx = NULL;
  int channelNumber = 0;

  numberTimerHandlerEvents++;
  reseqState = ReseqStateIDLE;


  nextExpectedPSN++;

#ifdef TRACEME
  printf("reseqFlowListElement::reseqTimerHandler[%s](%lf): current reseq flow (flowID:%d)Q level:%d, Updated nextExpectedPSN:%d (numberTimerHanderEvents:%d) \n",
     myReseqMgr->myMac->docsis_id,curr_time,mySF->flowID,queueSize,nextExpectedPSN,numberTimerHandlerEvents);
#endif 


  //next, send up as many as we can -- until the pkt PSN > nextExepcted
  packetListElement *myLE;
  Packet *tmpPkt=NULL;
  //For  each packet in the queue,
  myLE = (packetListElement *)mySF->myPacketList->head;
  for (i=0;i<queueSize;i++) {
      if (myLE == NULL) {
#ifdef TRACEME 
         printf("reseqFlowListElement::reseqTimerHandler(%lf) (i:%d) reached end of List \n",
	           curr_time,i);
#endif
         break;
      }
      tmpPkt = myLE->getPacket();
      chx = docsis_dsehdr::access(tmpPkt);
#ifdef TRACEME 
      printf("reseqFlowListElement::reseqTimerHandler(%lf)(i:%d)  look at this pkt  PSN:%d\n",
	        curr_time,i, chx->packet_sequence_number);
#endif
      if (chx->packet_sequence_number == nextExpectedPSN) {
        tmpPkt = mySF->removePacket();
#ifdef TRACEME 
        printf("reseqFlowListElement::reseqTimerHandler(%lf)   service flowID:%d, send up PSN:%d\n",
		           curr_time, mySF->flowID, chx->packet_sequence_number);
#endif
        myLE = (packetListElement *)mySF->myPacketList->head;
        myReseqMgr->myMac->RecvFrame(tmpPkt,channelNumber);
        nextExpectedPSN++;
      }
      if (chx->packet_sequence_number > nextExpectedPSN) {
#ifdef TRACEME 
        printf("reseqMgr::reseqTimerHandler(%f) LOOP BREAK (i:%d)  flowID:%d\n",
                curr_time, i,mySF->flowID);
#endif
        break;
      }
  }

  queueSize = mySF->packetsQueued();
  if (queueSize > 0) {
    packetListElement *pktLE = (packetListElement *) mySF->myPacketList->head;
    double waitTime = curr_time - pktLE->packetEntryTime;
    double timerValue = mySF->reseqTimeout -waitTime;
#ifdef TRACEME 
    packetListElement *pktLE2 = (packetListElement *) mySF->myPacketList->tail;
    double waitTime2 = curr_time - pktLE2->packetEntryTime;
    printf("reseqFlowListElement::reseqTimerHandler(%lf,flowID:%d) Exit with packets in queue(%d): oldest packet:%lf (wait time:%3.6f), newest packet:%lf (wait time: %3.6f), timerValue:%3.6f \n",
		 curr_time, mySF->flowID, queueSize,pktLE->packetEntryTime, 
         waitTime, pktLE->packetEntryTime,waitTime2,timerValue);
#endif
    if (timerValue <= 0.0) {
      timerValue = .000001;
      printf("reseqFlowListElement::reseqTimerHandler(%lf,flowID:%d) ERROR:  Adjust timerValue to %3.6f \n",
		 curr_time, mySF->flowID, timerValue);
    }
    myReseqTimer->start( (Event *)&reseqEvent, timerValue);
  }

#ifdef TRACEME 
  printf("reseqFlowListElement::reseqTimerHandler(%lf)   (flowID:%d)  Exit with queue size:%d and rc:%d\n",
		 curr_time, mySF->flowID, queueSize,rc);
#endif

  return rc;
}


/***********************************************************************
*function: int serviceFlowListElement::putServiceFlow(serviceFlowObject *SFObjParam)
*
*explanation:
*
*inputs:
*
*outputs:
************************************************************************/
int reseqFlowListElement::putServiceFlow(serviceFlowObject *SFObjParam)
{
int rc = 0;

#ifdef TRACEME
  printf("reseqFlowListElement:putServiceFlow:  (service flow id :%d)\n",SFObjParam->flowID);
#endif
  if (mySF == NULL)
  {
    mySF =  SFObjParam;
  }
  else {
    printf("reseqFlowListElement:putServiceFlow: ERROR: mySF ptr is NOT NULL  \n");
    mySF =  SFObjParam;
  }

#ifdef TRACEME
  printf("reseqFlowListElement:putServiceFlow: put handle %0x\n",mySF);
#endif
  return rc;
}

/***********************************************************************
*function: serviceFlowObject * serviceFlowListElement::getServiceFlow()
*
*explanation:
*
*inputs:
*
*outputs:
************************************************************************/
serviceFlowObject * reseqFlowListElement::getServiceFlow()
{

#ifdef TRACEME
  printf("reseqFlowListElement:getServiceFlow: return mySF of %0x \n",mySF);
  if (mySF !=NULL)
    printf("        .......service flow id :%d\n",mySF->flowID);
  else
    printf("        ..... but mySF is NULL ??  \n");
#endif
  return mySF;
}








/************************************************************************
* File:  BaseRED.cc
*
* Purpose:
*  This module contains a basic list object.
*  
*  Important:  The rates that are monitored are the true arrival/departure
*      rates to the queue.  They are not the correct service flow rates.
*
*   The behavior of this object is set by this param:
*         adaptiveMode=0; // 0:RED, no adaptation; 1:adaptive RED; 2:delay based RED   3:FIFOQ - not RED!
*    This variable is set by whoever calls setAQMParams
*
* Revisions:
*   $A808: seg fault when computing quittime avgQ.
*           And, no longer support REDIO - to add uncomment REDIO
*           Use  filterTimeConstant instead of weightQ in queue avg monitor
*   $A809: FIFOQ supported by this object - adaptiveMode=3
************************************************************************/
#include "BaseRED.h"
#include <iostream>
#include <math.h>
#include <random.h>

#include "object.h"

#include "packetListElement.h"

#include "globalDefines.h"
#include "docsisDebug.h"
//#define TRACEME 0
//#define FILES_OK 0
//#define FILES_OK_AQM 0

//$A808
//Uncomment this to turn on RED priority handling
//#defne REDIO 0

BaseRED::BaseRED() : ListObj()
{
 stateFlag = 0;
 accessDelayMeasure =0;
 channelUtilizationMeasure=0;
 consumptionMeasure=0;
//Just set to this for now
 targetDelayLevel = DELAYBASEDRED_TARGET_LATENCY;
upperLatencyLimit = 0;
upperLatencyThresh = 0;
lowerLatencyThresh = 0;
#ifdef TRACEME
  printf("BaseRED: constructed list \n");
  fflush(stdout);
#endif
}

BaseRED::~BaseRED()
{
#ifdef TRACEME
  printf("BaseRED: destructed list \n");
#endif
}

void BaseRED::initList(int maxListSize)
{

  ListObj::initList(maxListSize);

  flowPriority = 0;
  minth = 0;
  maxth = maxListSize;
  maxp = 0.10;
  weightQ = 1.0;
  filterTimeConstant = 0.002;
  gentleMode = 1;
  adaptiveMode=0; // 0:RED, no adaptation; 1:adaptive RED; 2:delay based RED   3:FCFS - not RED!

//For Adaptive RED
  targetQueueLevel = 0;
  alpha = 0.0;
  beta = 0.0;
//For delayBasedRED
//Just set to this for now
  targetDelayLevel = DELAYBASEDRED_TARGET_LATENCY;
  lowerLatencyThresh = targetDelayLevel/2;
  upperLatencyThresh =  targetDelayLevel;
  upperLatencyLimit =  2 * upperLatencyThresh;

 
  
  count = -1;


  avgPacketLatency = 0.0;
  queueLatencyTotalDelay = 0.0;
  queueLatencySamplesCount = 0;
  queueLatencyMonitorTotalArrivalCount = 0;
  avgAvgQLatency= 0;
  avgAvgQLatencySampleCount= 0;
  lastQLatencySampleTime = 0;


  avgQLoss = 0;
  avgQLossCounter=0;
  avgQLossSampleCounter=0;
  lastQLossSampleTime = 0;

  avgAvgQLoss = 0;
  avgAvgQLossSampleCounter=0;

  lastRateSampleTime = 0;
  lastMonitorSampleTime = 0;


  avgQ = 0.0;
  dropP = 0.0;
  avgMaxP=0;
  avgMaxPSampleCount=0;
  avgDropP=0;
  avgDropPSampleCount=0;
  avgAvgQ = 0;
  avgAvgQPSampleCount= 0;
  q_time = 0.0;
  avgTxTime = (double)1500 * 8 / (double) 5000000;


 lastUpdateTime =0.0;
 stateFlag = 1;
 updateFrequency = .050;
 bytesQueued = 0;
 avgServiceRate=0;
 avgArrivalRate=0;
 byteArrivals = 0;
 byteDepartures = 0;
 rateWeight = .02;
 lastQLatencySampleTime =  0;

 BaseREDupdateFrequency = AQM_ADAPTATION_TIME;
 BaseREDUpdateCountThreshold = 0;
 BaseREDUpdateCount = 0;
 BaseREDLastSampleTime = 0;
 BaseREDByteArrivals =0;
 BaseREDByteDepartures = 0;
 avgBaseREDArrivalRate = 0;
 avgBaseREDServiceRate = 0;
 BaseREDRateWeight = .02;

 BaseREDQueueDelaySamples = 0;
 BaseREDQueueLevelSamples = 0;
 BaseREDAccessDelaySamples =0;
 BaseREDConsumptionSamples = 0;
 BaseREDChannelUtilizationSamples = 0;


//#ifdef TRACEME
  if (traceFlag == 1) {
      printf("BaseRED::init: ListID:%d,  adaptionMode:%d, Priority:%d, maxListSize:%d, targetDelayLevel:%3.6f, lowerLatencyThresh: %3.6f, upperLatencyThresh: %3.6f, upperLatencyLimit: %3.6f\n",
            listID, maxListSize, targetDelayLevel, lowerLatencyThresh, upperLatencyThresh, upperLatencyLimit);
  }
//#endif


}


/*************************************************************
* routine:
*   int  BaseRED::addElement(ListElement& element)
*
* Function: this routine inserts the ListElement to the tail of 
* the list.
*           
* inputs: 
*    ListElement& element : the element to be inserted
*
* outputs:
*  Returns a SUCCESS or FAILURE.
*        Possible failures:
*             -malloc fails
*             -list already has  > MAXLISTSIZE elements
*
***************************************************************/
int  BaseRED::addElement(ListElement& element)
{
  int rc = SUCCESS;
  double curTime =  Scheduler::instance().clock();
  int upperLimit  = 0;

  if (gentleMode == 1) 
    upperLimit = 2*maxth;
  else
    upperLimit = maxth;


#ifdef TRACEME
  if (traceFlag == 1) {
   printf("BaseRED:addElement(%lf) (adaptiveMode:%d) listsize:%d, avgQ:%3.1f, minth:%d, maxth:%d, upperLimit:%d,dropP:%3.3f, avgPacketLatency:%3.6f \n ",
      curTime,adaptiveMode,curListSize,avgQ,minth,maxth,upperLimit,dropP,avgPacketLatency);
   }
#endif

  Packet *p = ((packetListElement &)element).getPacket();
  struct hdr_cmn *ch = HDR_CMN(p);
  byteArrivals+=ch->size();
  BaseREDByteArrivals += ch->size();

//This needs to be every packet
  updateAvgQ();
  updateMonitors();
//  updateRates();
//  if (adaptiveMode > 0) {
//    channelStatus(accessDelayMeasure, channelUtilizationMeasure,  consumptionMeasure);
//  }

#ifdef TRACEME
  if (traceFlag == 1) {
   printf("BaseRED:addElement(%lf) UPDATE AvgQ listsize:%d, avgQ:%3.1f, minth:%d, maxth:%d (upperLimit:%d), dropP:%3.4f, count:%d, avgArrivalRate:%f, avgServiceRate:%f, avgPacketLatency:%f \n",
      curTime,curListSize,avgQ,minth,maxth,upperLimit,dropP,count,avgArrivalRate,avgServiceRate, avgPacketLatency);
   }
#endif

   //         adaptiveMode=0; // 0:RED, no adaptation; 1:adaptive RED; 2:delay based RED   3:FIFOQ - not RED!
  //if RED or Adaptive RED
  if (adaptiveMode < 2) {
    if ((minth<= avgQ) && (avgQ < upperLimit)) {
      count++;
      //update dropP;
      dropP = computeDropP();
      if ( packetDrop(dropP) == TRUE) {
        rc = FAILURE;
       }
    }
    else if (avgQ >= upperLimit) {
      count++;
      rc = FAILURE;
    }
    else
      count=-1;
  }
  else if (adaptiveMode == 2)  {

  //upperLatencyLimit will be set in init assuming gentle mode is on


#ifdef TRACEME
  if (traceFlag == 1) {
   printf("BaseRED:addElement(%lf) DELAYBASED:  rc:%d listsize:%d, avgQ:%3.1f, avgPacketLatency:%2.6f, targetDelayLatenyc:%2.6f,  upperLatencyLimit:%2.6f\n",
      curTime,rc,curListSize,avgQ,avgPacketLatency, targetDelayLevel, upperLatencyLimit);
   }
#endif


     if ((avgPacketLatency > lowerLatencyThresh) && ( avgPacketLatency < upperLatencyLimit)) {
      count++;
      dropP = computeDropPDelayBased();
      if (dropP > .90)
         dropP = .75;
#ifdef TRACEME
    if (traceFlag == 1) {
     printf("BaseRED:addElement(%lf) DRED1: dropP:%3.3f maxp:%3.3f listsize:%d, avgQ:%3.1f, avgPacketLatency:%2.6f, targetDelayLatenyc:%2.6f,  upperLatencyLimit:%2.6f\n",
        curTime,dropP, maxp, curListSize,avgQ,avgPacketLatency, targetDelayLevel, upperLatencyLimit);
     }
#endif
      if ( packetDrop(dropP) == TRUE) {
        rc = FAILURE;
       }
     } 
     else if (avgPacketLatency >=  upperLatencyLimit) {
      //to avoid stalling our monitors allow up to 3 pkts
#ifdef TRACEME
    if (traceFlag == 1) {
     printf("BaseRED:addElement(%lf) DRED2: dropP:%3.3f maxp:%3.3f,  listsize:%d, avgQ:%3.1f, avgPacketLatency:%2.6f, targetDelayLatenyc:%2.6f,  upperLatencyLimit:%2.6f\n",
        curTime,dropP,maxp,curListSize,avgQ,avgPacketLatency, targetDelayLevel, upperLatencyLimit);
     }
#endif
       if (curListSize > 3 ) {
         if ( packetDrop(0.75) == TRUE) {
           rc = FAILURE;
         }
       }
       else
           rc = SUCCESS;
     }
     else
       count=-1;
  }
  //Should be 3....which is NO RED - just FIFOQ
  //$A809
  else  {
    rc = SUCCESS;
#ifdef TRACEME
    if (traceFlag == 1) {
     printf("BaseRED:addElement(%lf) NOT RED:  FIFOQ  rc:%d listsize:%d, avgQ:%3.1f, avgPacketLatency:%2.6f, targetDelayLatenyc:%2.6f,  upperLatencyLimit:%2.6f\n",
        curTime,rc,curListSize,avgQ,avgPacketLatency, targetDelayLevel, upperLatencyLimit);
     }
#endif
  }

#ifdef TRACEME
  if (traceFlag == 1) {
   printf("BaseRED:addElement(%lf)  rc:%d listsize:%d, avgQ:%3.1f, mint:%d, maxt:%d, dropP:%3.3f \n ",
      curTime,rc,curListSize,avgQ,minth,maxth,dropP);
   }
#endif

  //rc is SUCCESS  indicting the pkt can be queued, else the caller should drop it
  if (rc == SUCCESS) {
    rc = ListObj::addElement(element);
    if (rc == SUCCESS) {
      Packet *p = ((packetListElement &)element).getPacket();
      struct hdr_cmn *ch = HDR_CMN(p);
      bytesQueued+=ch->size();
    }
  }
  else {
    count = 0;
  }

#ifdef TRACEME
  if (traceFlag == 1) {
   printf("BaseRED:addElement(%lf) exit rc=%d, listsize:%d, avgQ:%3.1f, min-t:%d, max-t:%d, dropP:%3.3f, bytesQueued:%d \n ",
      curTime,rc,curListSize,avgQ,minth,maxth,dropP, bytesQueued);
   }
#endif

  avgQLossSampleCounter++;
  if (rc == FAILURE) {
    avgQLossCounter++;
  }
//  updateAvgQLoss();
  return rc;
}

/*************************************************************
* routine: ListElement * ListObj::removeElement()
*
* Function:
*
* inputs: 
*
* outputs:
*   The element at the top of the list is removed and returned.
*
*   A NULL is returned if the list is empty
*
***************************************************************/
ListElement * BaseRED::removeElement()
{
  packetListElement *tmpPtr = NULL;
  double curTime =   Scheduler::instance().clock();
  double x = 0;

#ifdef TRACEME
  if (traceFlag == 1) {
  printf("BaseRED::removeElement:(%lf) start size: %d (bytesQueued:%d)\n", curTime, curListSize,bytesQueued);
   }
#endif

  tmpPtr = (packetListElement *)ListObj::removeElement();
  if (curListSize == 0) {
    q_time = curTime;
  }

  if (tmpPtr != NULL) {
    x = curTime - tmpPtr->packetEntryTime;

    Packet *p = ((packetListElement *)tmpPtr)->getPacket();
    struct hdr_cmn *ch = HDR_CMN(p);
    byteDepartures+=ch->size();
    BaseREDByteDepartures += ch->size();
    bytesQueued-=ch->size();
    if (bytesQueued < 0) {
      printf("BaseRED::removeElement:(%lf) TROUBLE : bytesQueued went negative with this pkt size: %d\n", curTime, ch->size());
      bytesQueued  = 0;
    }

    if (x > 0) {
      queueLatencyTotalDelay += x;
      queueLatencySamplesCount++;
//      updateAvgQ();
#ifdef TRACEME 
  if (traceFlag == 1) {
      printf("BaseRED::removePacket(%lf): ListID:%d CurListSize:%d Packet delay sample: %3.6f, queueLatencyAvg:%3.6f(#samples:%6.0f), avgPacketLatency:%3.6f \n",
            curTime,listID, curListSize, x, queueLatencyTotalDelay,queueLatencySamplesCount,avgPacketLatency);
   }
#endif
    }
      
  }
  return tmpPtr;
}



//Called only when a new packet arrives  (which might be dropped....)
void BaseRED::newArrivalUpdate(Packet *p)
{

 queueLatencyMonitorTotalArrivalCount++;

// We want to monitor the true arrival rate to the queue...
// so ignore when a new packet arrives ....we only
// caree when it gets queued.
  return;

  double curTime =  Scheduler::instance().clock();
  struct hdr_cmn *ch = HDR_CMN(p);
  byteArrivals+=ch->size();
  BaseREDByteArrivals += ch->size();
}


void BaseRED::updateStats()
{
  double curTime =   Scheduler::instance().clock();

#ifdef TRACEME
  printf("BaseRED:updateStats:(%lf), updated avg: %3.1f\n",curTime,avgQ);
#endif
}

void BaseRED::printStatsSummary()
{
  double curTime =   Scheduler::instance().clock();

//#ifdef TRACEME
  printf("BaseRED:ListObj:printStatsSummary:(%lf), avgAvgQ: %3.1f, avgDropP: %3.3f, avgQLatency:%3.6f\n",curTime,getAvgAvgQ(), getAvgDropP(),getAvgAvgQLatency());
//#endif
}


/*************************************************************
* routine: void BaseRED::setAQMParams(int maxAQMSizeP, int priorityP, int minthP, int maxthP, int adaptiveModeP, double maxpP, double filterTimeConstantP)
*
* Function: this routine adapts the AQM parameters.
*   The algorithm sets RED specific parameters.
*           
* inputs: 
*   int maxAQMSizeP
*   int priorityP 
*   int minthP 
*   int maxthP 
*   int adaptiveModeP
*         0: base RED
*         1: Floyd's Adaptive RED
*         2 : Delay Based  RED
*         3 : NOT RED - FIFOQ 
*   double maxpP
*   double filterTimeConstantP)
* outputs:
*
***************************************************************/
void BaseRED::setAQMParams(int maxAQMSizeP, int priorityP, int minthP, int maxthP, int adaptiveModeP, double maxpP, double filterTimeConstantP)
{

  flowPriority = priorityP;
  MAXLISTSIZE = maxAQMSizeP;
  minth = minthP;
  maxth = maxthP;
  maxp = maxpP;
  filterTimeConstant = filterTimeConstantP;

  adaptiveMode= adaptiveModeP;

//For the 'new' RED config
// OLD:   minth = 4;
//  minth = maxAQMSizeP / 10;
//  maxth = maxAQMSizeP / 2;
//  maxp = 0.5;
//  BaseREDRateWeight = 0.002;


//$A808 don't do this for now
//Also, won't work if doing FIFOQ
#ifdef REDIO 
  //If the priority is > 0, eleveate the priority using algorithm params
  if (priorityP > 0) {
    maxp = maxpP / 2;
     printf("BaseRED:setAQMParams: (listID:%d) WARNING HIGH PRIORITY  minth:%d  maxth:%d  maxp:%2.2f targetQueueLevel:%d filterTimeConstant:%2.4f,  priority:%d (configured priorityP:%d), adaptiveMode:%d,  alpha:%1.2f, beta:%1.2f \n",
      listID,minth,maxth,maxp,targetQueueLevel,filterTimeConstant,flowPriority, priorityP, adaptiveMode, alpha, beta);
  }
  if (priorityP < 0) {
    minth=1;
    maxth = 4;
//    minth=3;
//    maxth = 10;
    filterTimeConstant = 0.01;
     printf("BaseRED:setAQMParams: (listID:%d) WARNING LOW PRIORITY  minth:%d  maxth:%d  maxp:%2.2f targetQueueLevel:%d filterTimeConstant:%2.4f,  priority:%d (configured priorityP:%d), adaptiveMode:%d,  alpha:%1.2f, beta:%1.2f \n",
      listID,minth,maxth,maxp,targetQueueLevel,filterTimeConstant,flowPriority, priorityP, adaptiveMode, alpha, beta);
  }
#else

#endif


  targetQueueLevel = minth + (minth + maxth) / 2;
  if (targetQueueLevel <= minth)
    targetQueueLevel = minth+1;


  alpha = maxp / 4;
  if (alpha < 0.01)
    alpha = 0.01;
  beta = 0.90;

  lowerLatencyThresh = targetDelayLevel/2;
  upperLatencyThresh =  targetDelayLevel;
  upperLatencyLimit =  2 * upperLatencyThresh;

//  upperLatencyLimit = 5 * targetDelayLevel;
//  upperLatencyThresh = 3 * targetDelayLevel;
//  lowerLatencyThresh = targetDelayLevel/2;
//#ifdef TRACEME
  printf("BaseRED:setAQMParams: (adaptlistID:%d) minth:%d  maxth:%d (MAXLISTSIZE:%d)  maxp:%2.2f targetQueueLevel:%d  targetDelayLevel:%3.6f, upperLatencyLimit:%3.6f,  upperLatencyThresh:%3.6f, lowerLatencyThresh:%3.6f,  filterTimeConstant:%2.4f,  priority:%d (configured priorityP:%d), adaptiveMode:%d,  alpha:%1.2f, beta:%1.2f \n",
      listID,minth,maxth,MAXLISTSIZE,maxp,targetQueueLevel,targetDelayLevel, upperLatencyLimit, upperLatencyThresh, lowerLatencyThresh, filterTimeConstant,flowPriority, priorityP, adaptiveMode, alpha, beta);
//#endif

}

/*************************************************************
* routine: void BaseRED::adaptAQMParams()
*
* Function: this routine adapts the AQM parameters.
*   The algorithm is TBD.
*           
* inputs: 
*
* outputs:
*
***************************************************************/
void BaseRED::adaptAQMParams()
{

#ifdef TRACEME
  printf("BaseRED:adaptAQMParams: updated minth:%d  maxth:%d  maxp:%2.2f \n",minth,maxth,maxp);
#endif
}


double BaseRED::getAvgQ()
{
  return(avgQ);
}

double BaseRED::getAvgAvgQ()
{
  if (avgAvgQPSampleCount > 0)
    return(avgAvgQ/(double)avgAvgQPSampleCount);
  else
    return(0);
}

double BaseRED::getAvgAvgQLatency()
{
  if (avgAvgQLatencySampleCount > 0)
    return(avgAvgQLatency/(double)avgAvgQLatencySampleCount);
  else
    return(0);
}

double BaseRED::getAvgAvgQLoss()
{
  if (avgAvgQLossSampleCounter > 0)
    return(avgAvgQLoss/(double)avgAvgQLossSampleCounter);
  else
    return(0);
}


double BaseRED::getDropP()
{
  return(dropP);
}

double BaseRED::getAvgDropP()
{
  double tmpAvgDropP = 0.0;

  if (avgDropPSampleCount > 0)
    tmpAvgDropP = avgDropP / avgDropPSampleCount;
  
  return(tmpAvgDropP);
}

double BaseRED::getAvgMaxP()
{
  double tmpAvgMaxP = 0.0;

  if (avgMaxPSampleCount > 0)
    tmpAvgMaxP = avgMaxP / avgMaxPSampleCount;
  
  return(tmpAvgMaxP);
}



/*************************************************************
* routine: double BaseRED::updateAvgQLoss()
*
* Function: this routine computes and updates the loss monitor
*           
* inputs: 
*
* outputs:
*    returns the updated value
*
***************************************************************/
double BaseRED::updateAvgQLoss()
{
  double curTime =   Scheduler::instance().clock();
  double sampleAvg1= 0;
  double returnAvg = 0.0;
  double sampleTime = curTime - lastQLatencySampleTime;


//  if (sampleTime < updateFrequency)
//    return avgPacketLatency;
 
 if (avgQLossSampleCounter == 0)
   return avgQLoss;

 sampleAvg1 = avgQLossCounter / avgQLossSampleCounter;

//   avgPacketLatency  = (1-weightQ) * avgPacketLatency + weightQ * sampleAvg1;
  returnAvg  = (1-filterTimeConstant) * avgQLoss + filterTimeConstant * sampleAvg1;

#ifdef TRACEME
  if (traceFlag == 1) {
  printf("BaseRED:updateAvgQLoss:(%lf) listID:%d  curLen:%d, avgQ:%3.3f, lossSample%f, numberSamples:%f, MovingAvg:%f\n",
       curTime,listID,curListSize, avgQ,sampleAvg1,avgQLossSampleCounter,returnAvg);
   }
#endif


  avgAvgQLoss += returnAvg;
  avgAvgQLossSampleCounter++;

  lastQLossSampleTime = curTime;
  avgQLoss = returnAvg;
  avgQLossCounter=0;
  avgQLossSampleCounter=0;
  lastQLossSampleTime = curTime;

  return returnAvg;

}


/*************************************************************
* routine: double BaseRED::updateAvgQLatency()
*
* Function: this routine computes and updates the global
*      AvgQ variable.
*           
* inputs: 
*
* outputs:
*    returns the updated value
*
***************************************************************/
double BaseRED::updateAvgQLatency()
{
  double curTime =   Scheduler::instance().clock();
  double sampleAvg1= 0;
  double returnAvg = 0.0;
  double sampleTime = curTime - lastQLatencySampleTime;


//  if (sampleTime < updateFrequency)
//    return avgPacketLatency;
 
 if (queueLatencySamplesCount == 0)
   return avgPacketLatency;

 sampleAvg1 = queueLatencyTotalDelay / queueLatencySamplesCount;
#ifdef TRACEME
  if (traceFlag == 1) {
  printf("BaseRED:updateAvgQLatency:(%lf) listID:%d curListSize:%d, current avgQ:%3.3f queueLatencyTotalDelay:%f, samplesCount:%f, TotalArrivalCount:%f,avgPacketLatency:%3.3f \n",
       curTime,listID,curListSize,avgQ,queueLatencyTotalDelay,queueLatencySamplesCount,queueLatencyMonitorTotalArrivalCount,avgPacketLatency);
   }
#endif


//   avgPacketLatency  = (1-weightQ) * avgPacketLatency + weightQ * sampleAvg1;
  returnAvg  = (1-filterTimeConstant) * avgPacketLatency + filterTimeConstant * sampleAvg1;


  avgAvgQLatency+=returnAvg;
  avgAvgQLatencySampleCount++;
  lastQLatencySampleTime = curTime;

  avgPacketLatency = returnAvg;
  queueLatencyTotalDelay = 0.0;
  queueLatencySamplesCount = 0;
  queueLatencyMonitorTotalArrivalCount = 0;

#ifdef TRACEME
  if (traceFlag == 1) {
  printf("BaseRED:updateAvgQLatency:(%lf) listID:%d  curLen:%d, avgQ:%3.3f, sampleAvg:%3.6f, avgPacketLatency:%3.6f based on %f samples\n",
       curTime,listID,curListSize, avgQ,sampleAvg1,avgPacketLatency,queueLatencySamplesCount);
   }
#endif


  return returnAvg;

}



/*************************************************************
* routine: double BaseRED::updateAvgQ
*
* Function: this routine computes the global avgQ variable
*           
* inputs: 
*
* outputs:
*    returns the updated value.
*
***************************************************************/
double BaseRED::updateAvgQ()
{
  double curTime =   Scheduler::instance().clock();
  double returnAvg = 0.0;


#ifdef TRACEME
  if (traceFlag == 1) {
  printf("BaseRED:updateAvgQ:(%lf)  curListSize:%d, current avgQ:%3.3f, filterTimeConstant:%2.4f, q_time:%3.6f \n",
       curTime,curListSize,avgQ,filterTimeConstant,q_time);
  fflush(stdout);
   }
#endif


  if (curListSize == 0) {
///$A808
    double quietTime = 0;
    double power = 0;
    double x=0;
//    double quietTime = curTime - q_time;
//    double power = quietTime/avgTxTime;
//    double x=  pow((1-filterTimeConstant),power);
//    returnAvg = avgQ*x;
    returnAvg = 0;
#ifdef TRACEME
  if (traceFlag == 1) {
    printf("BaseRED:updateAvgQ:(%lf) case of empty queue quietTime: %6.6f, power:%3.6f, x:%3.6f, returnAvg:%3.6f \n",
       curTime,quietTime,power,x,returnAvg);
   }
#endif
  }
  else {
    returnAvg = (1-filterTimeConstant)*avgQ + filterTimeConstant*curListSize;
#ifdef TRACEME
  if (traceFlag == 1) {
    printf("BaseRED:updateAvgQ:(%lf) case of queue len:%d, current avgQ:%3.3f, updated avg:%3.3f \n",
       curTime,curListSize, avgQ,returnAvg);
   }
#endif
  }
  avgAvgQ+=returnAvg;
  avgAvgQPSampleCount++;

#ifdef TRACEME
  if (traceFlag == 1) {
  printf("BaseRED:updateAvgQ:(%lf) curLen:%d, avgQ:%3.3f, returnAvg:%3.3f avgAvgQ:%3.3f (filterTimeConstant:%3.3f)\n",
       curTime,curListSize, avgQ, returnAvg,avgAvgQ/avgAvgQPSampleCount,filterTimeConstant);
   }
#endif
  avgQ = returnAvg;
  return returnAvg;

}




void BaseRED::updateRates()
{
  double curTime =   Scheduler::instance().clock();
  double sampleTime = curTime - lastRateSampleTime;
  double ArrivalRateSample=0;
  double DepartureRateSample=0;

#ifdef TRACEME
  if (traceFlag == 1) {
  printf("BaseRED:updateRates:(%lf)  byteArrivals:%f, byteDepartures:%f, time since last rate sample:%f  \n", curTime,byteArrivals,byteDepartures,sampleTime);
   }
#endif

//  if (sampleTime < updateFrequency)
//    return;

  if (sampleTime > 0 ) {
    ArrivalRateSample = byteArrivals*8/sampleTime;
    DepartureRateSample = byteDepartures*8/sampleTime;
  }

  avgArrivalRate = (1-rateWeight)*avgArrivalRate + rateWeight*ArrivalRateSample;


  avgServiceRate = (1-rateWeight)*avgServiceRate + rateWeight*DepartureRateSample;

#ifdef TRACEME
  if (traceFlag == 1) {
    printf("BaseRED:updateRates:(%lf)  avgArrivalRate:%f (sample:%f),  avgServiceRate:%f(sample:%f) \n",
       curTime,avgArrivalRate,ArrivalRateSample,avgServiceRate,DepartureRateSample);
   }
#endif

  byteArrivals =0;
  byteDepartures =0;
  lastRateSampleTime = curTime;
}

/*************************************************************
* routine: double BaseRED::computeDropP()
*
* Function: this routine computes the drop probability
*    using the base RED or the gentle RED algorithm
* 
*  THe caller must properly call if using gentle mode
*           
* inputs: 
*
* outputs:
*
***************************************************************/
double BaseRED::computeDropP()
{
  double pb=0;
  double pa=0;
  double curTime =   Scheduler::instance().clock();
  double y=0;

#ifdef TRACEME
  if (traceFlag == 1) {
  printf("BaseRED:computDropP(%lf): avgQ:%2.2f, minth:%d, maxth:%d, count:%d \n",
         curTime,avgQ,minth,maxth,count);
   }
#endif

  if (gentleMode == 1) {
    if (avgQ > maxth) {
      pb = maxp +  (1-maxp) *  (avgQ - (double)maxth) / (double)maxth;
    }
    else {
      pb = maxp *  (avgQ - (double)minth) / (double)(maxth-minth);
    }
  } 
  else {
    pb = maxp *  (avgQ - (double)minth) / (double)(maxth-minth);
  }

  if (pb < 0)
    printf("BaseRED:computDropP(%lf): HARD ERROR  y: %2.4f  pa:%2.4f pb:%2.4f\n",curTime,y, pa,pb);

  //The count inflates the drop rate as the number of consecutive calls to compute drop increases
  y =  (1-(double)count*pb);
#ifdef TRACEME
  if (traceFlag == 1) {
  printf("BaseRED:computDropP(%lf): (gentle:%d) avgQ:%4.2f, minth:%d, maxth:%d, count:%d, pb:%3.4f, y=%3.4f \n",
         curTime,gentleMode,avgQ,minth,maxth,count,pb,y);
   }
#endif
  if (y < .00001) {
#ifdef TRACEME
    printf("BaseRED:computDropP(%lf): TROUBLE: y too small y: %2.4f  pa:%2.4f pb:%2.4f\n",curTime,y, pa,pb);
#endif
    y = .1;
  }
  pa = pb / y;
  //This can happen if count get > 20 or so....
  if (pa > 1.0) {
#ifdef TRACEME
    printf("BaseRED:computDropP(%lf): TROUBLE: pa greater than 1  pa: %2.4f  pb:%2.4f,   count:%d,  y:%2.4f\n",curTime,pa,pb, count, y);
#endif
    pa = 1.0;
  }

  //should never happen....
  if (count > 1000) {
#ifdef TRACEME
    printf("BaseRED:computDropP(%lf): TROUBLE: count:%d pb: %2.4f  pa:%2.4f\n",curTime,count,pb,pa);
#endif
    count = 10;
  }

  avgDropP+= pa;
  avgDropPSampleCount++;
 
#ifdef TRACEME
  if (traceFlag == 1) {
  printf("BaseRED:computDropP(%lf): gentle:%d, FINAL: pb: %2.4f  pa:%2.4f, count:%d, avg:%2.4f\n",curTime,gentleMode, pb,pa,count, avgDropP/avgDropPSampleCount);
   }
#endif

  return pa;

}


/*************************************************************
* routine: double BaseRED::computeDropPDelayBased()
*
* Function: this routine computes the drop probability
*    using the base RED or the gentle RED algorithm
* 
*  THe caller must properly call if using gentle mode
*           
* inputs: 
*
* outputs:
*   returns the computed drop P.  This method can never fail
***************************************************************/
double BaseRED::computeDropPDelayBased()
{
  double pb=0;
  double pa=0;
  double curTime =   Scheduler::instance().clock();
  double y=0;

#ifdef TRACEME
  if (traceFlag == 1) {
  printf("BaseRED:computDropPDelayBased(%lf): maxp:%3.3f avgQ:%2.2f, minth:%d, maxth:%d, count:%d \n",
         curTime,maxp,avgQ,minth,maxth,count);
   }
#endif

  if (gentleMode == 1) {
    if (avgPacketLatency > upperLatencyThresh) {
//      pb = maxp +  (1-maxp) *  (avgQ - (double)maxth) / (double)maxth;
      pb = maxp +  (1-maxp) *  (avgPacketLatency - upperLatencyThresh) / (upperLatencyLimit - upperLatencyThresh);
    }
    else {
//      pb = maxp *  (avgQ - (double)minth) / (double)(maxth-minth);
      pb = maxp *  (avgPacketLatency - lowerLatencyThresh) / (upperLatencyThresh - lowerLatencyThresh);
    }
  } 
  else {
//    pb = maxp *  (avgQ - (double)minth) / (double)(maxth-minth);
    pb = maxp *  (avgPacketLatency - lowerLatencyThresh) / (upperLatencyThresh - lowerLatencyThresh);
  }

  //If avg latency is is less than lowerLatencyThresh will be negative
  if (pb < 0)
    printf("BaseRED:computDropPDelayBased(%lf): HARD ERROR  y: %2.4f  pa:%2.4f pb:%2.4f\n",curTime,y, pa,pb);

  //The count inflates the drop rate as the number of consecutive calls to compute drop increases
  y =  (1-(double)count*pb);
#ifdef TRACEME
  if (traceFlag == 1) {
  printf("BaseRED:computDropPDelayBased(%lf): (gentle:%d) avgPacketLatency:%3.6f, lowerLatencyThresh:%f, upperLatencyThresh:%f,  count:%d, pb:%3.6f, y=%3.6f \n",
         curTime,gentleMode,avgPacketLatency,lowerLatencyThresh,upperLatencyThresh,count,pb,y);
   }
#endif
  if (y < .00001) {
#ifdef TRACEME
    printf("BaseRED:computDropPDelayBased(%lf): TROUBLE: y too small  y: %2.4f  pa:%2.4f pb:%2.4f\n",curTime,y, pa,pb);
#endif
    y = .1;
  }
  pa = pb / y;
  if (pa > 1.0) {
#ifdef TRACEME
    printf("BaseRED:computDropPDelayBased(%lf): TROUBLE: pa greater than 1 pa: %2.4f  pb:%2.4f,   count:%d,  y:%2.4f\n",curTime,pa,pb, count, y);
#endif
    pa = 1.0;
  }

  if (count > 1000) {
//#ifdef TRACEME
    printf("BaseRED:computDropPDelayBased(%lf): TROUBLE: count:%d pb: %2.4f  pa:%2.4f\n",curTime,count,pb,pa);
//#endif
    count = 10;
  }

  avgDropP+= pa;
  avgDropPSampleCount++;
 
#ifdef TRACEME
  if (traceFlag == 1) {
  printf("BaseRED:computDropPDelayBased(%lf): gentle:%d, FINAL: pb: %2.6f  pa:%2.6f, count:%d, avg:%2.6f\n",curTime,gentleMode, pb,pa,count, avgDropP/avgDropPSampleCount);
   }
#endif

  return pa;

}

int BaseRED::packetDrop(double dropP)
{
  int dropFlag = FALSE;
  double randNumber  = Random::uniform(0,1.0);
  double curTime =  Scheduler::instance().clock();

  if (randNumber <= dropP)
    dropFlag = TRUE;

#ifdef  FILES_OK_AQM
  if (traceFlag == 1) {
    FILE* fp = NULL;
    char traceString[32];
    char *tptr = traceString;
    sprintf(tptr,"traceAQM%d.out",listID);
    fp = fopen(tptr, "a+");
    if (avgDropPSampleCount > 0)  {
      fprintf(fp,"%lf\t%d\t%3.1f\t%d\t%2.3f\t%2.3f\t%2.3f\t%d\t%d\t%12.0f\t%12.0f\t%f\t%3.3f\tdrop \n",
        Scheduler::instance().clock(),listID,avgQ, curListSize, dropP, avgDropP/avgDropPSampleCount,maxp,dropFlag, bytesQueued, avgArrivalRate, avgServiceRate, avgPacketLatency,avgQLoss);
    }
    else {
      fprintf(fp,"%lf\t%d\t%3.1f\t%d\t%2.3f\t%2.3f\t%2.3f\t%d\t%d\t%12.0f\t%12.0f\t%f\t%3.3f\tdrop \n",
        Scheduler::instance().clock(),listID,avgQ, curListSize, dropP, 0,maxp,dropFlag, bytesQueued, avgArrivalRate, avgServiceRate, avgPacketLatency,avgQLoss);
    }
    fclose(fp);
  }
#endif
  
#ifdef TRACEME
  if (traceFlag == 1) {
  printf("BaseRED:packetDrop(%lf): uniform:%2.2f, dropP:  %2.2f dropFlag:%d \n",curTime,randNumber,dropP,dropFlag);
   }
#endif
  return dropFlag;

}


/*************************************************************
* routine:
*  void BaseRED::channelStatus(double accessDelayMeasure, double channelUtilizationMeasure, double consumptionMeasure)
*
* Function: this method is perioodicaly called to inform the AQM of current channel conditions. 
*        Based on this information, the routine might adapt its AQM operating settings.  It
*        can only do this if a  BaseREDupdateFrequency amount of time has passed since the last
*        update.
*  
*  NOTE: Currently this implements the Floyd ARED algorithm.  It does not look at the three
*        params passed to it.
*
* Inputs: 
*  double accessDelayMeasure
*  double channelUtilizationMeasure
*  double consumptionMeasure
*
* outputs:
*
*  This method will possible update the following:
*      -The minth or maxth range
*      - maxp
*      -
*****************************************************************/
void BaseRED::channelStatus(double accessDelayMeasure, double channelUtilizationMeasure, double consumptionMeasure)
{
  double curTime =   Scheduler::instance().clock();
  double sampleTime = curTime - BaseREDLastSampleTime;
  double ArrivalRateSample=0;
  double DepartureRateSample=0;


  if (sampleTime < BaseREDupdateFrequency)
    return;

//  updateAvgQ();

//  (void)  updateAvgQLatency();
  //reset Latency  monitor counts
//  queueLatencyTotalDelay = 0.0;
//  queueLatencySamplesCount = 0;
//  queueLatencyMonitorTotalArrivalCount = 0;

  updateRates();
//  byteArrivals =0;
//  byteDepartures =0;
//  lastRateSampleTime = curTime;

  updateAvgQLoss();
//  avgQLossCounter=0;
//  avgQLossSampleCounter=0;
//  lastQLossSampleTime = curTime;

  BaseREDLastSampleTime = curTime;


#ifdef TRACEME
  if (traceFlag == 1) {
    printf("BaseRED:channelStatus:(%lf)  avgBaseArrivalRate:%f, avgServiceRate:%f, avgQ:%6f, avgPacketLatency:%f, avgQLoss:%f\n",
       curTime,avgArrivalRate,avgServiceRate,avgQ,avgPacketLatency, avgQLoss);
 printf("BaseRED:chanelStatus(%lf): accessDelayM:%3.3f, channelUtilizationM:%3.3f, consumptionMeasure:%3.3f \n",curTime,accessDelayMeasure, channelUtilizationMeasure,consumptionMeasure);
   }
#endif

  if ((adaptiveMode > 0) && (adaptiveMode < 3)) {
#ifdef TRACEME
  if (traceFlag == 1) {
    printf("BaseRED:channelStatus:(%lf): adaptive mode: %d, avgQLoss:%3.3f, avgQ:%3.3f, minth:%d, maxth:%d, maxp:%2.3f, targetQueueLevel:%d  \n", 
          curTime, adaptiveMode, avgQLoss,avgQ, minth, maxth, maxp, targetQueueLevel);
    printf("BaseRED:channelStatus:(%lf)  avgBaseArrivalRate:%f, avgServiceRate:%f, avgQ:%6f, avgPacketLatency:%f, avgQLoss:%f\n",
       curTime,avgArrivalRate,avgServiceRate,avgQ,avgPacketLatency, avgQLoss);
//    printf("BaseRED:chanelStatus(%lf): accessDelayM:%3.3f, channelUtilizationM:%3.3f, consumptionMeasure:%3.3f \n",curTime,accessDelayMeasure, channelUtilizationMeasure,consumptionMeasure);
   }
#endif

     //if Floyd's adaptive RED...
    if (adaptiveMode == 1) {
      if ((avgQ > targetQueueLevel) && (maxp <=0.5)) {
        //increase maxp;
          maxp = maxp + alpha;
      }
      else if ((avgQ < targetQueueLevel) && (maxp >= 0.10)) {
      //decrease maxp;
        maxp = maxp * beta;
      }
      if (maxp > 0.50) 
        maxp = 0.50;
      if (maxp < 0.10) 
        maxp = 0.10;

#ifdef TRACEME
  if (traceFlag == 1) {
      printf("BaseRED:channelStatus:(%lf): Updated:  avgQ:%3.3f, minth:%d, maxth:%d, maxp:%2.3f   \n", 
          curTime, avgQ, minth, maxth, maxp);
   }
#endif
    }
    //If delay Based RED 
    if (adaptiveMode == 2) {
        if ((avgPacketLatency > targetDelayLevel) && (maxp <=0.5)) {
          //increase maxp;
            maxp = maxp + alpha;
        }
        else if ((avgPacketLatency < targetDelayLevel) && (maxp >= 0.10)) {
        //decrease maxp;
          maxp = maxp * beta;
        }
        if (maxp > 0.50) 
          maxp = 0.50;
        if (maxp < 0.10) 
          maxp = 0.10;
  
#ifdef TRACEME
  if (traceFlag == 1) {
      printf("BaseRED:channelStatus:(%lf): DRED3  Updated:  avgQ:%3.3f, minth:%d, maxth:%d, maxp:%2.3f   \n", 
          curTime, avgQ, minth, maxth, maxp);
   }
#endif
      if ((avgPacketLatency > targetDelayLevel) && (maxp <=0.5)) {
        //increase maxp;
          maxp = maxp + alpha;
      }
      else if ((avgPacketLatency < targetDelayLevel) && (maxp >= 0.10)) {
        //decrease maxp;
        maxp = maxp * beta;
      }
      if (maxp > 0.50) 
        maxp = 0.50;
      if (maxp < 0.10) 
        maxp = 0.10;

#ifdef TRACEME
  if (traceFlag == 1) {
      printf("BaseRED:channelStatus:(%lf): Updated:  avgQ:%3.3f, avgPacketLatency:%3.6f,  maxp:%2.3f   \n", 
          curTime, avgQ, avgPacketLatency,maxp);
   }
#endif
    }
  }

#ifdef  FILES_OK_AQM 
  if (traceFlag == 1) {
    FILE* fp = NULL;
    char traceString[32];
    char *tptr = traceString;
    sprintf(tptr,"traceAQM%d.out",listID);
    fp = fopen(tptr, "a+");
    if (avgDropPSampleCount > 0)  {
      fprintf(fp,"%lf\t%d\t%3.1f\t%d\t%2.3f\t%2.3f\t%2.3f\t%d\t%d\t%12.0f\t%12.0f\t%f\t%3.3f\tstatus \n",
          Scheduler::instance().clock(),listID,avgQ, curListSize, dropP, avgDropP/avgDropPSampleCount,maxp,0, bytesQueued, avgArrivalRate, avgServiceRate, avgPacketLatency, avgQLoss);
    }
    else
    {
      fprintf(fp,"%lf\t%d\t%3.1f\t%d\t%2.3f\t%2.3f\t%2.3f\t%d\t%d\t%12.0f\t%12.0f\t%f\t%3.3f\tstatus \n",
          Scheduler::instance().clock(),listID,avgQ, curListSize, dropP, 0.0 ,maxp,0, bytesQueued, avgArrivalRate, avgServiceRate,avgPacketLatency,avgQLoss);
    }
    fclose(fp);
  }
#endif

}


void BaseRED::channelIdleEvent()
{
  double curTime =   Scheduler::instance().clock();
  double updateTime = curTime - lastUpdateTime;


//10-12-2009 - ignore
return;

  updateRates();

  numberTotalChannelIdleEvents++;
#ifdef  FILES_OK_AQM 
  if (traceFlag == 1) {
    FILE* fp = NULL;
    char traceString[32];
    char *tptr = traceString;
    sprintf(tptr,"traceAQM%d.out",listID);
    fp = fopen(tptr, "a+");
    if (avgDropPSampleCount > 0)  {
      fprintf(fp,"%lf\t%d\t%3.1f\t%d\t%2.3f\t%2.3f\t%2.3f\t%d\t%d\t%f\t%f\t%f\t%f\tidle \n",
          Scheduler::instance().clock(),listID,avgQ, curListSize, dropP, avgDropP/avgDropPSampleCount,maxp,0, bytesQueued, avgArrivalRate, avgServiceRate, avgPacketLatency, avgQLoss);
    }
    else
    {
      fprintf(fp,"%lf\t%d\t%3.1f\t%d\t%2.3f\t%2.3f\t%2.3f\t%d\t%d\t%f\t%f\t%f\t%f\tidle \n",
          Scheduler::instance().clock(),listID,avgQ, curListSize, dropP, 0 ,maxp,0, bytesQueued, avgArrivalRate, avgServiceRate, avgPacketLatency,avgQLoss);
    }
    fclose(fp);
  }
#endif



#ifdef TRACEME
  printf("BaseRED:channelIdleEvent(%lf): avgQ:%2.2f, dropP:%lf, freeze_time:%lf,  time since last update:%lf \n",
         curTime,avgQ,dropP,freeze_time,updateTime);
#endif

  if (updateTime > freeze_time) {
    numberValidChannelIdleEvents++;
//   double randNumber = 0;
//   dropP = dropP - dec1-randNumber;

    numberDecrements++;
    lastUpdateTime = curTime;

    //make sure it does not go negative, if not reset lastUpdateTime
//    if (dropP < 0)
//     dropP = 0;
//    else
//      lastUpdateTime = curTime;

    avgDropP+= dropP;
    avgDropPSampleCount++;
#ifdef  FILES_OK_AQM 
    if (traceFlag == 1) {
      FILE* fp = NULL;
      char traceString[32];
      char *tptr = traceString;
      sprintf(tptr,"traceAQM%d.out",listID);
      fp = fopen(tptr, "a+");
      if (avgDropPSampleCount > 0)  {
        fprintf(fp,"%lf\t%d\t%3.1f\t%d\t%2.3f\t%2.3f\t%2.3f\t%d\t%d\t%f\t%f\t%f\t%f\tIdleIncr \n",
            Scheduler::instance().clock(),listID,avgQ, curListSize, dropP, avgDropP/avgDropPSampleCount,maxp,0, bytesQueued, avgArrivalRate, avgServiceRate, avgPacketLatency,avgQLoss);
      }
      else {
        fprintf(fp,"%lf\t%d\t%3.1f\t%d\t%2.3f\t%2.3f\t%d\t%2.3f\t%d\t%f\t%f\t%f\t%f\tIdleIncr \n",
            Scheduler::instance().clock(),listID,avgQ, curListSize, dropP, 0,maxp,0, bytesQueued, avgArrivalRate, avgServiceRate, avgPacketLatency,avgQLoss);
      }
      fclose(fp);
    }
#endif
  }

}

/*************************************************************
* routine: void BaseRED::updateMonitors()
*
* Function: this routine is called periodically (set 
*   by updateFrequency) to update the following stats:
*        -rates :
*        -Avg Q Loss
*        -Avg Q Level
*        -Avg Q Latency
*
*  After the update if then makes an entry in the traceAQM trace file.
*           
* inputs: 
*
* outputs:
*
***************************************************************/
void BaseRED::updateMonitors()
{
  double curTime =   Scheduler::instance().clock();
  double sampleTime = curTime - lastMonitorSampleTime;

#ifdef TRACEME
  if (traceFlag == 1) {
  printf("BaseRED:updateMonitors:(%lf)  byteArrivals:%f, byteDepartures:%f, time since last rate sample:%f  \n", curTime,byteArrivals,byteDepartures,sampleTime);
   }
#endif

  if (sampleTime < updateFrequency)
    return;

// this is done with each insert and remove
  updateAvgQ();
  (void)  updateAvgQLatency();
  updateRates();
//  if (adaptiveMode > 0) {
    channelStatus(accessDelayMeasure, channelUtilizationMeasure,  consumptionMeasure);
//  }
  lastMonitorSampleTime = curTime;

#ifdef  FILES_OK
  if (traceFlag == 1) {
    FILE* fp = NULL;
    char traceString[32];
    char *tptr = traceString;
    sprintf(tptr,"traceAQM%d.out",listID);
    fp = fopen(tptr, "a+");

    if (avgDropPSampleCount > 0)  {
      fprintf(fp,"%lf\t%d\t%3.1f\t%d\t%2.3f\t%2.3f\t%2.3f\t%d\t%d\t%12.0f\t%12.0f\t%f\t%3.3f\tupdate \n",
          Scheduler::instance().clock(),listID,avgQ, curListSize, dropP, avgDropP/avgDropPSampleCount,maxp,0, bytesQueued, avgArrivalRate, avgServiceRate, avgPacketLatency, avgQLoss);
    }
    else
    {
      fprintf(fp,"%lf\t%d\t%3.1f\t%d\t%2.3f\t%2.3f\t%2.3f\t%d\t%d\t%12.0f\t%12.0f\t%f\t%3.3f\tupdate \n",
          Scheduler::instance().clock(),listID,avgQ, curListSize, dropP, 0.0 ,maxp,0, bytesQueued, avgArrivalRate, avgServiceRate,avgPacketLatency,avgQLoss);
    }

    fclose(fp);
  }
#endif
}



/************************************************************************
* File:  REDQ.h
*
* Purpose:
*   Inlude files for a base list object. A REDQ is a container
*   of AQM Elements.
*
*
* Revisions:
*
***********************************************************************/
#ifndef REDQ_h
#define REDQ_h

#include "ListObj.h"

class Packet;

class REDQ : public ListObj {

public:
  virtual ~REDQ();
  REDQ();
  virtual void initList(int maxAQMSize);
  virtual void setAQMParams(int maxAQMSize, int priority, int minth, int maxth, int adaptiveMode, double maxp, double weightQ);
  virtual int addElement(ListElement& element);
  virtual ListElement *removeElement();    //dequeue and return
  virtual void adaptAQMParams();
  virtual void newArrivalUpdate(Packet *p);
  void updateStats();
  void printStatsSummary();
  double getAvgQ();
  double getAvgAvgQ();
  double getDropP();
  double getAvgDropP();

  virtual void channelIdleEvent();
  virtual void channelStatus(double accessDelayMeasure, double channelUtilizationMeasure, double consumptionMeasure);

  int maxth;
  int minth;
  double maxp;
  double weightQ;
  int flowPriority;

  double avgQ;
  double dropP;
  double q_time;
  double avgDropP;
  double avgDropPSampleCount;
  double avgAvgQ;
  double avgAvgQPSampleCount;
protected:


private:

  int count;
  int gentleMode;
  int adaptive1Mode;
  double avgTxTime;

  double updateAvgQ();
  double computeDropP();
  int packetDrop(double dropP);

};


#endif

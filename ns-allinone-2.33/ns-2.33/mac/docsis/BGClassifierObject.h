/***************************************************************************
 * Module: BGClassifierObject 
 *
 * Explanation:
 *   This file contains the class definition of an object 
 *   responsible for mapping SFs to Aggregate Flows. 
 *
 * Revisions:
 *
************************************************************************/

#ifndef BGClassifierObject_h
#define BGClassifierObject_h

class Packet;
class bondingGroupObject;
class serviceFlowObject;
class aggregateSFObject;

struct BGClassifierStatsType
{
  unsigned int totalPackets;
  double totalBytes;
};


struct FlowMapType 
{
  int SFID;
  int BGID;
  int AggregateQueueID;
  int accesses;
};

//Valid opcodes for modifying a flow map
#define SHUFFLE_FLOW_TO_BUCKET_MAPPING 1
#define CHANGE_TO_NEW_AGGREGATE_QEUEUE 2
#define PUT_FLOW_IN_TIMEOUT 3
#define REMOVE_FLOW_FROM_TIMEOUT 4


/*************************************************************************
 ************************************************************************/
class BGClassifierObject
{
public:


  BGClassifierObject();

  virtual ~BGClassifierObject();

  virtual void init(int myBGIDParam, int maxSizeMAPParam, int schedModeParam, bondingGroupObject *myBGObjectParam);
  void printStatsSummary();
  void getStats(struct BGClassifierStatsType *callersStats);
  virtual int initFlowMap(char *fileName);
  virtual aggregateSFObject *findAggregateQueue(Packet *p, serviceFlowObject *SFObj);
  virtual int modifyFlowMap(serviceFlowObject *SFObj, int opCode, int opParam);
  virtual int adjustFlowMap(int opCode);

  virtual void printFlowMap();

  struct BGClassifierStatsType myStats;
  int myBGID;
  bondingGroupObject *myBGObject;
  FlowMapType *myMap;
  int maxSizeMAP;
  int flowMAPSize;

private:
  int schedMode; //values are the globally defined service discipline types...eg, FCFS, WFQ, HOSTBASED,  FAIRSHARE, BROADBANDAQM
};

/*************************************************************************
 ************************************************************************/
class BGClassifierObjectHash: public BGClassifierObject
{
public:


  BGClassifierObjectHash();

  ~BGClassifierObjectHash();
  void init(int myBGIDParam, int maxSizeMAPParam, int schedModeParam, bondingGroupObject *myBGObjectParam);
  int initFlowMap(char *fileName);
  aggregateSFObject * findAggregateQueue(Packet *p, serviceFlowObject *SFObj);
  int modifyFlowMap(serviceFlowObject *SFObj, int opCode, int opParam);
  int adjustFlowMap(int opCode);

private:
  int randomMap(int numberBuckets);

};


/*************************************************************************
 ************************************************************************/
class BGClassifierObjectPriority: public BGClassifierObject
{
public:

  BGClassifierObjectPriority();

  ~BGClassifierObjectPriority();
  void init(int myBGIDParam, int maxSizeMAPParam, int schedModeParam, bondingGroupObject *myBGObjectParam);
  int initFlowMap(char *fileName);
  aggregateSFObject * findAggregateQueue(Packet *p, serviceFlowObject *SFObj);
  int modifyFlowMap(serviceFlowObject *SFObj, int opCode, int opParam);
  int adjustFlowMap(int opCode);

private:

};


#endif /* __BGClassifierObject_h_ */

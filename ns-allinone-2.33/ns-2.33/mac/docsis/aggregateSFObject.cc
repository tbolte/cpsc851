/***************************************************************************
 * Module: aggregateSFObject
 *
 * Explanation:
 *   This file contains the class definition of a service flow.
 * 
 *
 * Revisions:
 *  $A907  : Added CoDel AQM
 *  $A908  : Added PIE AQM
 *
************************************************************************/

#include "aggregateSFObject.h"
#include "bondingGroupObject.h"

#include "docsisDebug.h"
//#define TRACEME 0
//#define TRACEME1 0

#include "ListObj.h"
#include "AQMObj.h"
//#include "REDQ.h"
#include "BaseRED.h"
#include "BLUEQ.h"
#include "CoDelAQM-TD.h"
#include "BAQM.h"
#include "PIE-AQM.h"

aggregateSFObject::aggregateSFObject() :  DSserviceFlowObject()
{

  myBGObj  = NULL;
#ifdef TRACEME 
  printf("aggregateSFObject::constructor: \n");
#endif 
}

aggregateSFObject::~aggregateSFObject()
{
#ifdef TRACEME 
  printf("aggregateSFObject::destructor: \n");
#endif 
}


void aggregateSFObject::init(int directionParam, int aggregateFlowIDParam, MacDocsis *myMacParam, serviceFlowMgr *mySFMgrParam, bondingGroupObject * myBGObjParam)
{


  myBGObj = myBGObjParam;
  flowID = aggregateFlowIDParam; 

  myBGID = myBGObj->myBGID;

  serviceFlowType = AGGREGATED_SF;
#ifdef TRACEME1
  printf("aggregateSFObject::init:  aggregate flow id:  %d \n",flowID);
#endif 

  //We create the first, default List
  serviceFlowObject::init(directionParam,myMacParam, mySFMgrParam);

}

/***********************************************************************
*function: void aggregateSFObject::setQueueBehavior(int maxQSize, int QType, int priorityP, int QParam1, int QParam2, int QParam3, double QParam4, double QParam5)
*
*explanation:  This method sets the tcl params to the queue
*
*inputs:
* int maxQSize
* int QType
* int priorityP
* int QParam1 : minth
* int QParam2 : maxth
* int QParam3 : adaptiveMode
* double QParam4 : maxp
* double QParam5  : flowWeight
*          mySFObj->setQueueBehavior(mySFObj->SchedQSize, mySFObj->SchedQType, mySFObj->flowPriority, (int)y,(int)x,mySFObj->adaptiveMode,mySFObj->MAXp,mySFObj->flowWeight);
*
*outputs:
*
************************************************************************/
void aggregateSFObject::setQueueBehavior(int maxQSize, int QType, int priorityP, int QParam1, int QParam2, int QParam3, double QParam4, double QParam5)
{

  SchedQType = QType;
  SchedQSize = maxQSize;
  flowPriority = priorityP;

  if (myPacketList != NULL)
    delete myPacketList;

//#ifdef TRACEME 
//$A901
//  printf("aggregateSFObject::setQueueBehavior: flowID:%d, QSize:%d, QType:%d, priorityP:%d, param1:%d, param2:%d, param3:%d, param4:%2.2f, param5:%2.2f \n",
//      flowID,maxQSize,QType,priorityP,QParam1,QParam2,QParam3,QParam4,QParam5);
  if (direction == UPSTREAM)
    printf("aggregateSFObject::setQueueBehavior:  UPSTREAM DIRECTION flowID:%d, QSize:%d, QType:%d, priorityP:%d, param1:%d, param2:%d, param3:%d, param4:%2.2f, param5:%2.2f \n",
      flowID,maxQSize,QType,priorityP,QParam1,QParam2,QParam3,QParam4,QParam5);
  else
    printf("aggregateSFObject::setQueueBehavior:  DOWNSTREAM DIRECTION flowID:%d, QSize:%d, QType:%d, priorityP:%d, param1:%d, param2:%d, param3:%d, param4:%2.2f, param5:%2.2f \n",
      flowID,maxQSize,QType,priorityP,QParam1,QParam2,QParam3,QParam4,QParam5);

//#endif 
  switch (QType)
  {

    case FIFOQ:
//      myPacketList = new ListObj();
//      myPacketList->initList(maxQSize);
//      myPacketList->listID = flowID;
      myPacketList = new BaseRED();
      myPacketList->initList(maxQSize);
      myPacketList->listID = flowID;
      //For FIFOQ, need to set adaptiveMode to 3
      ((BaseRED *)myPacketList)->setAQMParams(maxQSize,flowPriority, QParam1,QParam2, 3, QParam4, QParam5);
      if ((flowID == TRACED_AggSFID1) || (flowID == TRACED_AggSFID2) || (flowID == TRACED_SFID) || (flowID == TRACED_SFID2) ) {
        ((BaseRED *)myPacketList)->traceFlag = 1;
//#ifdef TRACEME 
          printf("aggregateSFFObject::setQueueBehavior: FIFOQ  SET traceFlag to 1 flowID:%d, QSize:%d, QType:%d, priorityP:%d, param1:%d, param2:%d, param3:%d, param4:%2.2f, param5:%2.2f \n",
                flowID,maxQSize,QType,priorityP,QParam1,QParam2,QParam3,QParam4,QParam5);
//#endif 
      }
      break;

    case OrderedQ:
      myPacketList = new OrderedListObj();
      myPacketList->initList(maxQSize);
      myPacketList->listID = flowID;
      break;

    case RedQ:
//      myPacketList = new REDQ();
      myPacketList = new BaseRED();
      myPacketList->initList(maxQSize);
      myPacketList->listID = flowID;
//      myPacketList->setAQMParams(maxQSize,QParam1,QParam2,QParam3,QParam4);
      ((BaseRED *)myPacketList)->setAQMParams(maxQSize,flowPriority, QParam1,QParam2, QParam3, QParam4, QParam5);
      if ((flowID == TRACED_AggSFID1) || (flowID == TRACED_AggSFID2) || (flowID == TRACED_SFID) || (flowID == TRACED_SFID2) ) {
        ((BaseRED *)myPacketList)->traceFlag = 1;
      }
      break;

    case AdaptiveRedQ:
      myPacketList = new BaseRED();
      myPacketList->initList(maxQSize);
      myPacketList->listID = flowID;
//      myPacketList->setAQMParams(maxQSize,QParam1,QParam2,QParam3,QParam4);
      ((BaseRED *)myPacketList)->setAQMParams(maxQSize,flowPriority, QParam1,QParam2, QParam3, QParam4, QParam5);
      if ((flowID == TRACED_AggSFID1) || (flowID == TRACED_AggSFID2) || (flowID == TRACED_SFID) || (flowID == TRACED_SFID2) ) {
        ((BaseRED *)myPacketList)->traceFlag = 1;
      }
      break;

    case DelayBasedRedQ:
      myPacketList = new BaseRED();
      myPacketList->initList(maxQSize);
      myPacketList->listID = flowID;
//      myPacketList->setAQMParams(maxQSize,QParam1,QParam2,QParam3,QParam4);
      ((BaseRED *)myPacketList)->setAQMParams(maxQSize,flowPriority, QParam1,QParam2, QParam3, QParam4, QParam5);
      if ((flowID == TRACED_AggSFID1) || (flowID == TRACED_AggSFID2) || (flowID == TRACED_SFID) || (flowID == TRACED_SFID2) ) {
        ((BaseRED *)myPacketList)->traceFlag = 1;
      }
      break;

    case BlueQ:
      myPacketList = new BLUEQ();
      myPacketList->initList(maxQSize);
      myPacketList->listID = flowID;
      ((BLUEQ *)myPacketList)->setAQMParams(maxQSize,flowPriority,QParam1,QParam2, QParam3, QParam4, QParam5);
      if ((flowID == TRACED_AggSFID1) || (flowID == TRACED_AggSFID2) || (flowID == TRACED_SFID) || (flowID == TRACED_SFID2) ) {
        ((BLUEQ *)myPacketList)->traceFlag = 1;
      }
      break;

//$A907
    case CDAQM:
      myPacketList = new CoDelAQM();
      myPacketList->initList(maxQSize);
      myPacketList->listID = flowID;
      ((CoDelAQM *)myPacketList)->setAQMParams(maxQSize,flowPriority,QParam1,QParam2, QParam3, QParam4, QParam5);
      if ((flowID == TRACED_AggSFID1) || (flowID == TRACED_AggSFID2) || (flowID == TRACED_SFID) || (flowID == TRACED_SFID2) ) {
        ((CoDelAQM *)myPacketList)->traceFlag = 1;
      }
      break;
//
//$A908
    case PIAQM:
      myPacketList = new PIEAQM();
      myPacketList->initList(maxQSize);
      myPacketList->listID = flowID;
      ((PIEAQM *)myPacketList)->setAQMParams(maxQSize,flowPriority,QParam1,QParam2, QParam3, QParam4, QParam5);
      if ((flowID == TRACED_AggSFID1) || (flowID == TRACED_AggSFID2) || (flowID == TRACED_SFID) || (flowID == TRACED_SFID2) ) {
        ((PIEAQM *)myPacketList)->traceFlag = 1;
      }
      break;


    case BAQMQ:
      myPacketList = new BAQM();
      myPacketList->initList(maxQSize);
      myPacketList->listID = flowID;
      ((BAQM *)myPacketList)->setAQMParams(maxQSize,flowPriority, QParam1,QParam2, QParam3, QParam4, QParam5);
      if ((flowID == TRACED_AggSFID1) || (flowID == TRACED_AggSFID2) || (flowID == TRACED_SFID) || (flowID == TRACED_SFID2) ) {
        ((BAQM *)myPacketList)->traceFlag = 1;
      }
      break;


    default:
      myPacketList = new ListObj();
      myPacketList->initList(maxQSize);
      break;
  }

  return;
}

/***********************************************************************
*function: int aggregateSFObject::addPacket(Packet* p)
*
*explanation:  This method adds the pkt to the list of packets
* awaiting transmission 
*
*inputs:
*  Packet *p : reference to the packet that is ready for transmission
*
*outputs:
*  Returns a SUCCESS or FAILURe
*
************************************************************************/
int aggregateSFObject::addPacket(Packet* p)
{
  int rc = SUCCESS;


  rc = serviceFlowObject::addPacket(p);

  return(rc);
}

/***********************************************************************
*function: Packet *p aggregateSFObject::removePacket()
*
*explanation:  This method removes the pkt to the list of packets
* awaiting transmission 
*
*inputs:
*  Packet *p : reference to the packet that is ready for transmission
*
*outputs:
*  Returns a ptr to a packet, else a NULL
*
************************************************************************/
Packet * aggregateSFObject::removePacket()
{
  Packet *p = NULL;

#ifdef TRACEME 
  double curTime = Scheduler::instance().clock();
  printf("aggregateSFObject::removePacket(%lf): ListID:%d,  Remove next packet in SF Object list size:%d)\n",
	  curTime,myPacketList->listID,myPacketList->curListSize);
#endif 

  p = serviceFlowObject::removePacket();

  return(p);
}




/***********************************************************************
*function: int aggregateSFObject::packetsQueued()
*
*explanation: This method returns the number of pkts awaiting transmission
* 
*inputs:
*
*outputs:
* Returns the current list size
*
************************************************************************/
int aggregateSFObject::packetsQueued(void)
{
	return(myPacketList->curListSize);
}

/***********************************************************************
*function: int aggregateSFObject::match(Packet* p)
*
*explanation:  This method returns a 1 if the packet matches the
*  classification for this service flow
*
* $A800:  modified algorithm:
*    If PKT TYPE is WILD CARD, match everything
*    else just match by finding the first PKT TYPE MATCh
*
*
*inputs:
*  Packet *p : reference to the packet in question
*
*
*outputs:
*  Returns a 1 if the packet matches, else a 0
*
************************************************************************/
int aggregateSFObject::match(Packet* p)
{
int rc = 0;
struct hdr_cmn *ch = HDR_CMN(p);
struct hdr_ip *chip = HDR_IP(p);
//$A801
docsis_dsehdr *chx = docsis_dsehdr::access(p);
struct hdr_mac* mh = HDR_MAC(p);



#ifdef TRACEME 
  printf("aggregateSFObject::match:(%lf): Packet type = %d src-ip = %d dst-ip = %d(PT_UDP:%d, PT_CBR:%d)\n",
	  Scheduler::instance().clock(),ch->ptype(),chip->saddr(),chip->daddr(),PT_UDP,PT_CBR);
  printf("aggregateSFObject::match: Try to match pkt: (src,dst,type):%d,%d,%d) with flow %d,%d,%d (flow dsid:%d, pkt dsid:%d)\n",
            chip->saddr(), chip->daddr(), ch->ptype(), classifier.src_ip,classifier.dst_ip,classifier.pkt_type,dsid, chx->dsid);
#endif     

//$A800
//  if (chx->dsid == dsid) {
#if 0
    if (classifier.pkt_type == FLOW_WILDCARD) {
      rc = 1;
    }
    else if (ch->ptype() == classifier.pkt_type) {
      rc = 1;
    }
//  }
#endif

  if ((ch->ptype() == classifier.pkt_type) && 
        (chip->daddr() == classifier.dst_ip))
  {
    rc= 1;
  }
  else {
    if ((classifier.pkt_type == FLOW_WILDCARD) && 
        (chip->daddr() == classifier.dst_ip))
    {
      rc= 1;
    }
  }

  myStats.numberMatchCalls++;

#ifdef TRACEME 
  if (ch->direction() == UPSTREAM) {
  printf("aggregateSFObject::match(%f): UPSTREAM MATCH RETURN:%d: (src,dst,type):%d,%d,%d) with flow %d,%d,%d (flow dsid:%d, pkt dsid:%d)\n",
	  Scheduler::instance().clock(),rc, chip->saddr(), chip->daddr(), ch->ptype(), classifier.src_ip,classifier.dst_ip,classifier.pkt_type,dsid, chx->dsid);
  } else {
  printf("aggregateSFObject::match(%f): DOWNSTREAM MATCH RETURN:%d: (src,dst,type):%d,%d,%d) with flow %d,%d,%d (flow dsid:%d, pkt dsid:%d)\n",
	  Scheduler::instance().clock(),rc, chip->saddr(), chip->daddr(), ch->ptype(), classifier.src_ip,classifier.dst_ip,classifier.pkt_type,dsid, chx->dsid);
  }
#endif     

  return rc;
}


/***********************************************************************
*function: int aggregateSFObject::match(Packet* p)
*
*explanation:  This method returns a 1 if the packet matches the
*  classification for this service flow
*
*inputs:
*  Packet *p : reference to the packet in question
*
*
*outputs:
*  Returns a 1 if the packet matches, else a 0
*
************************************************************************/
int aggregateSFObject::matchMACAddress(Packet* p, int direction)
{
int rc = 0;
struct hdr_cmn *ch = HDR_CMN(p);
struct hdr_ip *chip = HDR_IP(p);
//$A801
docsis_dsehdr *chx = docsis_dsehdr::access(p);
struct hdr_mac* mh = HDR_MAC(p);


#ifdef TRACEME 
  printf("aggregateSFObject::matchMACAddress:(%lf): DIrection:%d flowID:%d  Packet type = %d src-ip = %d dst-ip = %d(PT_UDP:%d, PT_CBR:%d)\n",
	  Scheduler::instance().clock(),direction, chip->flowid(), ch->ptype(),chip->saddr(),chip->daddr(),PT_UDP,PT_CBR);
#endif 
  
#ifdef TRACEME 
  printf("aggregateSFObject::matchMACAddress: Try to match pkt: (src,dst,type):%d,%d,%d) with flow %d,%d,%d\n",
            chip->saddr(), chip->daddr(), ch->ptype(), classifier.src_ip,classifier.dst_ip,classifier.pkt_type);
#endif     

//$A801
  if (direction == DOWNSTREAM) {
    if (mh->macDA() == macaddr) 
      rc= 1;
  }
  else {
    if (mh->macSA() == macaddr) 
      rc= 1;
  }
#if 0
  if (direction == DOWNSTREAM) {
    if ((ch->ptype() == classifier.pkt_type) && 
        (chip->daddr() == classifier.dst_ip))
    {
      rc= 1;
    }
  }
  else {
//    if (ch->ptype() == classifier.pkt_type)  
    if ((ch->ptype() == classifier.pkt_type) && 
        (chip->saddr() == classifier.src_ip))
    {
      rc= 1;
    }
  }

#endif 

  myStats.numberMatchCalls++;

#ifdef TRACEME 
  if (ch->direction() == UPSTREAM) {
  printf("aggregateSFObject::match(%f): UPSTREAM MATCH RETURN:%d: (src,dst,type):%d,%d,%d) with flow %d,%d,%d (flow dsid:%d, pkt dsid:%d)\n",
	  Scheduler::instance().clock(),rc, chip->saddr(), chip->daddr(), ch->ptype(), classifier.src_ip,classifier.dst_ip,classifier.pkt_type,dsid, chx->dsid);
  } else {
  printf("aggregateSFObject::match(%f): DOWNSTREAM MATCH RETURN:%d: (src,dst,type):%d,%d,%d) with flow %d,%d,%d (flow dsid:%d, pkt dsid:%d)\n",
	  Scheduler::instance().clock(),rc, chip->saddr(), chip->daddr(), ch->ptype(), classifier.src_ip,classifier.dst_ip,classifier.pkt_type,dsid, chx->dsid);
  }
#endif     

  return rc;
}


/***********************************************************************
*function: int aggregateSFObject::matchAddress(Packet* p)
*
*explanation:  This method returns a 1 if the packet matches the
*  classification for this service flow
*
*inputs:
*  Packet *p : reference to the packet in question
*
*
*outputs:
*  Returns a 1 if the packet matches, else a 0
*
************************************************************************/
int aggregateSFObject::matchAddress(Packet* p)
{
  int rc = 0;
  struct hdr_cmn *ch = HDR_CMN(p);
  struct hdr_ip *chip = HDR_IP(p);

#ifdef TRACEME 
  printf("aggregateSFObject::matchAddress:(%lf): Packet type = %d src-ip = %d dst-ip = %d(PT_UDP:%d, PT_CBR:%d)\n",
	  Scheduler::instance().clock(),ch->ptype(),chip->saddr(),chip->daddr(),PT_UDP,PT_CBR);
#endif 
  
#ifdef TRACEME 
  printf("aggregateSFObject::matchAddress: Try to match pkt: (src,dst,type):%d,%d,%d) with flow %d,%d,%d\n",
              chip->saddr(), chip->daddr(), ch->ptype(), classifier.src_ip,classifier.dst_ip,classifier.pkt_type);
#endif     
  if ((chip->daddr() == classifier.dst_ip))
  {
    rc= 1;
  }

  myStats.numberMatchAddressCalls++;
  return rc;
}


/***********************************************************************
*function: int aggregateSFObject::matchAddress(Packet* p)
*
*explanation:  This method returns a 1 if the packet matches the
*  classification for this service flow
*
*inputs:
*  Packet *p : reference to the packet in question
*
*
*outputs:
*  Returns a 1 if the packet matches, else a 0
*
************************************************************************/
int aggregateSFObject::matchAddress(Packet* p, int direction)
{
  int rc = 0;
  struct hdr_cmn *ch = HDR_CMN(p);
  struct hdr_ip *chip = HDR_IP(p);

#ifdef TRACEME 
  printf("aggregateSFObject::matchAddress:(%lf): Direction: %d Packet type = %d src-ip = %d dst-ip = %d(PT_UDP:%d, PT_CBR:%d)\n",
	  Scheduler::instance().clock(),direction, ch->ptype(),chip->saddr(),chip->daddr(),PT_UDP,PT_CBR);
#endif 
  
#ifdef TRACEME 
  printf("aggregateSFObject::matchAddress: Try to match pkt: (src,dst,type):%d,%d,%d) with flow %d,%d,%d\n",
              chip->saddr(), chip->daddr(), ch->ptype(), classifier.src_ip,classifier.dst_ip,classifier.pkt_type);
#endif     
  if (direction == DOWNSTREAM) {
    if ((chip->daddr() == classifier.dst_ip))
    {
      rc= 1;
    }
  }
  else {
    if ((chip->saddr() == classifier.src_ip))
    {
      rc= 1;
    }

  }

  myStats.numberMatchAddressCalls++;
  return rc;
}

//$A801
/***********************************************************************
*function: int aggregateSFObject::matchPktType(Packet* p)
*
*explanation:  This method returns a 1 if the packet matches the
*  classification for this service flow
*
* $A800:  modified algorithm:
*    If PKT TYPE is WILD CARD, match everything
*    else just match by finding the first PKT TYPE MATCh
*
*
*inputs:
*  Packet *p : reference to the packet in question
*
*
*outputs:
*  Returns a 1 if the packet matches, else a 0
*
************************************************************************/
int aggregateSFObject::matchPktType(Packet* p)
{
int rc = 0;
struct hdr_cmn *ch = HDR_CMN(p);
struct hdr_ip *chip = HDR_IP(p);
//$A801
docsis_dsehdr *chx = docsis_dsehdr::access(p);
struct hdr_mac* mh = HDR_MAC(p);



#ifdef TRACEME 
  printf("aggregateSFObject::matchPkt:(%lf): flowID:%d  Packet type = %d src-ip = %d dst-ip = %d(PT_UDP:%d, PT_CBR:%d)\n",
	  Scheduler::instance().clock(),chip->flowid(), ch->ptype(),chip->saddr(),chip->daddr(),PT_UDP,PT_CBR);
  printf("aggregateSFObject::matchPktType: Try to match pkt: (src,dst,type):%d,%d,%d) with flow %d,%d,%d (flow dsid:%d, pkt dsid:%d)\n",
            chip->saddr(), chip->daddr(), ch->ptype(), classifier.src_ip,classifier.dst_ip,classifier.pkt_type,dsid, chx->dsid);
#endif     

    if (classifier.pkt_type == FLOW_WILDCARD) {
      rc = 1;
    }
    else if (ch->ptype() == classifier.pkt_type) {
      rc = 1;
    }

  myStats.numberMatchCalls++;

#ifdef TRACEME 
  if (ch->direction() == UPSTREAM) {
  printf("aggregateSFObject::matchPktType(%f): UPSTREAM MATCH RETURN:%d: (src,dst,type):%d,%d,%d) with flow %d,%d,%d (flow dsid:%d, pkt dsid:%d)\n",
	  Scheduler::instance().clock(),rc, chip->saddr(), chip->daddr(), ch->ptype(), classifier.src_ip,classifier.dst_ip,classifier.pkt_type,dsid, chx->dsid);
  } else {
  printf("aggregateSFObject::matchPktType(%f): DOWNSTREAM MATCH RETURN:%d: (src,dst,type):%d,%d,%d) with flow %d,%d,%d (flow dsid:%d, pkt dsid:%d)\n",
	  Scheduler::instance().clock(),rc, chip->saddr(), chip->daddr(), ch->ptype(), classifier.src_ip,classifier.dst_ip,classifier.pkt_type,dsid, chx->dsid);
  }
#endif     

  return rc;
}



void aggregateSFObject::channelStatus(double avgChannelAccesDelay, double avgTotalSlotUtilization, double avgMySlotUtilization)
{

  if (SchedQType ==  RedQ) {
    ((BaseRED *)myPacketList)->channelStatus(avgChannelAccesDelay, avgTotalSlotUtilization,  avgMySlotUtilization);
//    ((REDQ *)myPacketList)->channelStatus(avgChannelAccesDelay, avgTotalSlotUtilization,  avgMySlotUtilization);
  }
  if (SchedQType ==  AdaptiveRedQ) {
    ((BaseRED *)myPacketList)->channelStatus(avgChannelAccesDelay, avgTotalSlotUtilization,  avgMySlotUtilization);
  }
  if (SchedQType ==  BAQMQ) {
    ((BAQM *)myPacketList)->channelStatus(avgChannelAccesDelay, avgTotalSlotUtilization,  avgMySlotUtilization);
  }
}


void aggregateSFObject::channelIdleEvent()
{

  if (SchedQType ==  BlueQ) {
    ((BLUEQ *)myPacketList)->channelIdleEvent();
  }
  if (SchedQType ==  BAQMQ) {
    ((BAQM *)myPacketList)->channelIdleEvent();
  }
}



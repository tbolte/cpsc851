/* -*-	Mode:C++; c-basic-offset:8; tab-width:8; indent-tabs-mode:t -*- */

/*
 * Proportional Integral Controller - Enhanced (PIE) AQM Implementation
 *
 * Authors of the code:
 * Preethi Natarajan (prenatar@cisco.com)
 * Rong Pan (ropan@cisco.com)
 * Chiara Piglione (cpiglion@cisco.com)


 * Copyright (c) 2013, Cisco Systems, Inc.  
 * All rights reserved.
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following
 * conditions are met:
 *   -	Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *   -	Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer
 *      in the documentation and/or other materials provided with the distribution.
 *   -	Neither the name of Cisco Systems, Inc. nor the names of its contributors may be used to endorse or promote products derived 
 *      from this software without specific prior written permission.
 * ALL MATERIALS ARE PROVIDED BY CISCO AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, 
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, AND NONINFRINGEMENT ARE DISCLAIMED.  IN NO EVENT SHALL
 * CISCO OR ANY CONTRIBUTOR BE LIABLE FOR ANY DIRECT, INDIRECT, SPECIAL, EXEMPLARY, CONSEQUENTIAL, OR INCIDENTAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGES. 
 *
 *  
 */
 
#ifndef ns_pie_h
#define ns_pie_h

#undef setbit

#include "queue.h"
#include "trace.h"
#include "timer-handler.h"

#define	DTYPE_NONE	0	/* ok, no drop */
#define	DTYPE_FORCED	1	/* a "forced" drop */
#define	DTYPE_UNFORCED	2	/* an "unforced" (random) drop */

// Insert APPLE specific macro here
//
#if defined( __MACH__ ) && defined( __APPLE__ )
#undef setbit
#endif

/*
 * Early drop parameters, supplied by user
 */
struct edp_pie {
	/*
	 * User supplied.
	 */
	int mean_pktsize;	/* avg pkt size, linked into Tcl */
	int setbit;		/* true to set congestion indication bit */
	double a, b;		/* parameters to pie controller */
	double tUpdate;		/*sampling timer*/
	double qdelay_ref;	/* desired queue delay in ms */
	double mark_p;		/* when p < mark_p, mark chosen packets */
				/* when p > mark_p, drop chosen packets */
	int use_mark_p;		/* use mark_p only for deciding when to drop, */
				/* 	if queue is not full */
	int bytes; 		/* if this flag is set drop probability depends on pkt size */				
	edp_pie(): mean_pktsize(0), setbit(0), a(0.0), b(0.0), tUpdate(0.0), qdelay_ref(0.0), mark_p (0), use_mark_p (0), bytes (0) { }
};

/*
 * Early drop variables, maintained by PIE
 */
struct edv_pie {
	TracedDouble v_prob;	/* prob. of packet drop before "count". */
	int count;		/* # of packets since last drop */
	int count_bytes;	/* # of bytes since last drop */
	double qdelay_old;

	edv_pie() : v_prob(0.0), count(0), count_bytes(0), qdelay_old(0.0) { }
};

class LinkDelay;
class PIEQueue ; 

class PIECalcTimer : public TimerHandler {
public:
		PIECalcTimer(PIEQueue *a) : TimerHandler() { a_ = a; }
		virtual void expire(Event *e);
protected:
		PIEQueue *a_;
};

class PIEQueue : public Queue {
 
 friend class PIECalcTimer;
 public:	
	PIEQueue(const char * = "Drop");
 protected:
	int command(int argc, const char*const* argv);
	void enque(Packet* pkt);
	virtual Packet *pickPacketForECN(Packet* pkt);
	virtual Packet *pickPacketToDrop();
	Packet* deque();
	void reset();
	int drop_early(Packet* pkt, int qlen);
 	double calculate_p();
	PIECalcTimer CalcTimer;

	LinkDelay* link_;	/* outgoing link */
	int fifo_;		/* fifo queue? */
	PacketQueue *q_; 	/* underlying (usually) FIFO queue */
		
	int qib_;	/* bool: queue measured in bytes? */
	NsObject* de_drop_;	/* drop_early target */

	//added to be able to trace EDrop Objects - ratul
	//the other events - forced drop, enque and deque are traced by a different mechanism.
	NsObject * EDTrace;    //early drop trace
	char traceType[20];    //the preferred type for early drop trace. 
	                       //better be less than 19 chars long
	Tcl_Channel tchan_;	/* place to write trace records */
	TracedInt curq_;	/* current qlen seen by arrivals */
	void trace(TracedVar*);	/* routine to write trace records */

	edp_pie edp_;	        /* early-drop params */
	edv_pie edv_;		/* early-drop variables */

	int dq_count;		/*number of bytes departed since current measuremeent cycle starts*/
	double dq_start;	/*the start timestamp of current measurement cycle*/
	double burst_allowance;	/*current max burst size that is allowed before random drops kick in*/
	int dq_threshold;	/*threshold that needs to be across before a sample of the dequeue rate is measured */
	double max_burst;	/*maximum burst allowed before random early dropping kicks in*/
	double avg_dq_rate;	/*time averaged dequeue rate*/
	
};

#endif

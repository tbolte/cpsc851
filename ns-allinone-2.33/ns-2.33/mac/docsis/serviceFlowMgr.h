/***************************************************************************
 * Module: serviceFlowMgr
 *
 * Explanation:
 *   This file contains the class definition of the object that
 *   packages service flows.  
 *
 *   This object contains a default service flow that is used to identfy/track
 *   management flows. (MGMTServiceFlow)
 * 
 *   The higher layers might want to create two SFMgrs: one for
 *    US and one for DS
 *
 * Revisions:
 *   TODO:  get rid of  CmRecord as this is a historic CMTS thing
 *
************************************************************************/

#ifndef ns_serviceFlowMgr_h
#define ns_serviceFlowMgr_h

#include "packet.h"
#include "rtp.h"
#include "globalDefines.h"
#include "serviceFlowListElement.h"
#include "serviceFlowObject.h"
#include "docsisDebug.h"
#include "ListObj.h"
#include "packetListElement.h"
#include "bondingGroupMgr.h"

class macPhyInterface;
class MacDocsisCM;

struct serviceFlowMgrStatsType
{
  int numberServiceFlows;
  double avgArrivalRate;
  double avgServiceRate;
  double avgLossRate;
  double avgPacketDelay;
};

struct serviceFlowSet
{
  int  numberMembers;
  int  maxSetSize;
  serviceFlowObject *SFObjPtrs[MAX_SERVICE_FLOW_SET_SIZE];
};

struct channelSet
{
  int  numberMembers;
  int  maxSetSize;
  int  channels[MAX_CHANNEL_SET_SIZE];
};


/*************************************************************************
 ************************************************************************/
class serviceFlowMgr
{
public:
  serviceFlowMgr();

  void init(int direction);
  virtual serviceFlowObject *getServiceFlow(Packet *p);
  virtual serviceFlowObject *getServiceFlow(Packet *p, int type);
  int  addServiceFlow(int macaddr, int32_t src_ip, int32_t dst_ip, packet_t pkt_type, u_int16_t flowID, int myBGIDParam);
  serviceFlowObject *addServiceFlow(u_int16_t flowID);
  serviceFlowObject *  returnServiceFlowObject(u_int16_t flowID);


  int removeServiceFlow(u_int16_t flowID);
  int getHighestServiceFlowID();

  void setMacHandle(MacDocsis *myMacParam);
  void setMyPhyInterface(macPhyInterface *myPhyInterfaceParam);

  void setCMRecord(struct cm_record *CmRecord);
  int getNumberSFs();
  void dumpSFQueueLevels(char *outputFile);

  int IsFrameMgmt(Packet *p);
  int anyPackets();
  void setBGMgr(bondingGroupMgr *);

  void getStats(struct serviceFlowMgrStatsType *callersStats);
  void printStatsSummary(char *outputFile);
  void printShortSummary();

//$A807
  void resetSFPriority();
  int optimize(int opCode);

  macPhyInterface *myPhyInterface;
  bondingGroupMgr *myBGMgr;
  serviceFlowObject *MGMTServiceFlow;
  int direction;  //0 for UPSTREAM,  1 for DOWNSTREAM
//$A310
//  int next_flowid;
  int numberSFs;
  int highestFlowID;
//JJM WRONG  get rid of this
  int lastSFsent;

  //structure that contains list of service flows
  ListObj *mySFList;

  MacDocsis *myMac;



private:

   struct cm_record *CmRecord;
   struct serviceFlowMgrStatsType myStats;

};

/*************************************************************************
 ************************************************************************/
class CMserviceFlowMgr : public serviceFlowMgr
{
public:
  CMserviceFlowMgr();

  void init(int direction, int ackSuppressionFlag, int cm_idParam);

//$A801
  serviceFlowObject *getServiceFlow(Packet *p);
  serviceFlowObject *getServiceFlow(Packet *p, int type);

//$A510
  int insert_pkt(Packet *p, char tbindex);
  int len_queue(Packet *p, char tbindex);

  int look_at_queue(char tbindex);
  int filterACKPackets(Packet *pkt, char tbindex);
  int packetMatch(Packet *pkt1, Packet *pkt2);

  serviceFlowObject *nextSF();
  Packet * deque_pkt(serviceFlowObject *mySFObj);

  MacDocsisCM *myCMMac;

  void channelIdleEvent();
  void channelStatus(double avgChannelAccesDelay, double avgTotalSlotUtilization, double avgMySlotUtilization);

private:

  int ACK_SUPPRESSION;
  int cm_id;
  
};

class CMTSserviceFlowMgr: public serviceFlowMgr
{
public:
  CMTSserviceFlowMgr();

  void init(int direction);
//$A801
  serviceFlowObject *getServiceFlow(Packet *p);
  serviceFlowObject *getServiceFlow(Packet *p, int type);

};

#endif /* __ns_serviceFlowMgr_h__ */

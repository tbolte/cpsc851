/************************************************************************
* File:  ListObj.cc
*
* Purpose:
*  This module contains a basic list object.
*
* Revisions:
*
*  Remove the IntegerListObj....
*
* $8212010 : changed addElement for Ordered Obj, it allows elements with
*      same key
************************************************************************/
#include "ListObj.h"
#include <iostream>

#include "object.h"


#include "globalDefines.h"

#include "docsisDebug.h"
//#define TRACEME 0

ListObj::ListObj()
{
  MAXLISTSIZE = 0;     
  traceFlag = 0;
  listID  = -1;
#ifdef TRACEME
  printf("ListObj: constructed list \n");
#endif
}

ListObj::~ListObj()
{
#ifdef TRACEME
  printf("ListObj: destructed list \n");
#endif
  //JJM WRONG  delete all elements in the list
}

void ListObj::initList(int maxListSize)
{

  MAXLISTSIZE = maxListSize;
  traceFlag = 0;
  listID  = -1;

//  MAXLISTSIZE = 32000;     
//  MAXLISTSIZE = 3;  //for test   

  head = tail  = NULL;
  curListSize = 0;

  initTime = 0;
  integralTotal = 0;
  lastIntegralTime = initTime;

  monitorPktCount = 0;
  lastSample = 0;
  queueRunningAverage = 0;
  numberTotalSamples = 0;
  numberPeriodSamples = 0;
  monitorPeriod = 1.0;
  minQueueLen = 0;
  maxQueueLen = 0;

  totalNumberOverflowEvents = 0;
#ifdef TRACEME
  printf("ListObj:init: max list size: %d \n",MAXLISTSIZE);
#endif

}

/*************************************************************
* routine:
*   int  ListObj::addElement(ListElement& element)
*
* Function: this routine inserts the ListElement to the tail of 
* the list.
*           
* inputs: 
*    ListElement& element : the element to be inserted
*
* outputs:
*  Returns a SUCCESS or FAILURE.
*        Possible failures:
*             -malloc fails
*             -list already has  > MAXLISTSIZE elements
*
***************************************************************/
int  ListObj::addElement(ListElement& element)
{
int rc = SUCCESS;
ListElement *tmpPtr = NULL;
double curTime =   Scheduler::instance().clock();

#ifdef TRACEME
   printf("ListObj:addElement(listsize:%d): add element%d, head:%d, tail:%d \n ",
		   curListSize,&element,head,tail);
#endif

  if (curListSize < MAXLISTSIZE) {
//insert at the tail
    curListSize++;
    if (tail == NULL) {
      tail=head=&element;
	  head->prev = NULL;
      head->next = NULL;
    }
    else {
      tmpPtr = tail;
      tail->next = &element;
      tail = &element;
      tail->next = NULL;
	  tail->prev = tmpPtr;
    }

    double curTime =   Scheduler::instance().clock();
    integralTotal += ((double)curListSize) * (curTime - lastIntegralTime);
    if (integralTotal < 0)
      integralTotal = 0;
#ifdef TRACEME
      printf("ListObj::addElement(%lf): curListSize:%d, integralTotal:%f, (curTime-lastIntegralTime):%f\n",
            curTime,curListSize,integralTotal,(curTime-lastIntegralTime));
#endif
    lastIntegralTime = curTime;
    monitorPktCount+=curListSize;
    numberPeriodSamples++;
    if ((curTime - lastSample) >= monitorPeriod) {
      monitorPktCount = monitorPktCount/numberPeriodSamples;
      queueRunningAverage += monitorPktCount;
      numberTotalSamples++;
      lastSample = curTime;
#ifdef TRACEME
      printf("ListObj::addElement(%lf): queue RunningAverage:%f numberTotalSamples:%f ,this period avg:%f (min/max:%f,%f)\n ",
            curTime,queueRunningAverage,numberTotalSamples,monitorPktCount,minQueueLen,maxQueueLen);
#endif
      monitorPktCount=0;
      numberPeriodSamples=0;

      if (curListSize < minQueueLen) {
        minQueueLen = curListSize;
        if (minQueueLen < 0) {
           printf("ListObj::addElement(%lf): HARD ERROR minQueueLen= %d \n ",curTime,minQueueLen);
           exit(1);
        }
      }
      if (curListSize > maxQueueLen) {
        maxQueueLen = curListSize;
        if (maxQueueLen < 0) {
           printf("ListObj::addElement(%lf): HARD ERROR manQueueLen= %d \n ",curTime,maxQueueLen);
           exit(1);
        }
      }
    }

  }
  else {
#ifdef TRACEME
      printf("ListObj:addElement: list overflow , list size: %d\n ",curListSize);
#endif
    totalNumberOverflowEvents++;
    rc = FAILURE;
  }

#ifdef TRACEME
   printf("ListObj:addElement(%lf) return rc %d, curListSize:%d, head:%d, tail:%d \n ",
		   curTime,rc,curListSize,head,tail);
#endif
  return rc;
}

/*************************************************************
* routine: int  ListObj::addElementatHead(ListElement& element)
*
* Function: this routine inserts the ListElement to the head of
* the list.
*           
* inputs: 
*    ListElement& element : the element to be inserted
*
* outputs:
*   rc : 1 is failure, else a 0
*        Possible failures:
*             -malloc fails
*             -list already has  > MAXLISTSIZE elements
*
***************************************************************/
int  ListObj::addElementatHead(ListElement& element)
{
  int rc = SUCCESS;
  ListElement *tmpPtr = NULL;
  double curTime =   Scheduler::instance().clock();


  if (curListSize < MAXLISTSIZE + 10) {
//insert at the tail
    curListSize++;
    if (tail == NULL) {
      tail=head=&element;
	  head->prev = NULL;
      head->next = NULL;
    }
    else {
      tmpPtr = head;
      element.prev= NULL;
      element.next = head;
      head->prev = &element;
	  head = &element;
    }
    integralTotal += ((double)curListSize) * (curTime - lastIntegralTime);
    if (integralTotal < 0)
      integralTotal = 0;
#ifdef TRACEME
      printf("ListObj::addElementatHead(%lf): curListSize:%d, integralTotal:%f, (curTime-lastIntegralTime):%f\n",
            curTime,curListSize,integralTotal,(curTime-lastIntegralTime));
#endif
    lastIntegralTime = curTime;
    monitorPktCount+=curListSize;
    numberPeriodSamples++;
    if ((curTime - lastSample) >= monitorPeriod) {
      monitorPktCount = monitorPktCount/numberPeriodSamples;
      queueRunningAverage += monitorPktCount;
      numberTotalSamples++;
      lastSample = curTime;
#ifdef TRACEME
      printf("ListObj::addElementatHead(%lf): queue RunningAverage:%f numberTotalSamples:%f ,this period avg:%f (min/max:%f,%f)\n ",
            curTime,queueRunningAverage,numberTotalSamples,monitorPktCount,minQueueLen,maxQueueLen);
#endif
      monitorPktCount=0;
      numberPeriodSamples=0;

      if (curListSize < minQueueLen) {
        minQueueLen = curListSize;
        if (minQueueLen < 0) {
           printf("ListObj::addElementatHead(%lf): HARD ERROR minQueueLen= %d \n ",curTime,minQueueLen);
           exit(1);
        }
      }
      if (curListSize > maxQueueLen) {
        maxQueueLen = curListSize;
        if (maxQueueLen < 0) {
           printf("ListObj::addElementatHead(%lf): HARD ERROR manQueueLen= %d \n ",curTime,maxQueueLen);
           exit(1);
        }
      }
    }
  }
  else {
#ifdef TRACEME
      printf("ListObj:addElementatHead: list overflow , list size: %d\n ",curListSize);
#endif
    totalNumberOverflowEvents++;
    rc = FAILURE;
  }
#ifdef TRACEME
   printf("ListObj:addElementatHead(%lf) return rc %d, curListSize:%d, head:%d, tail:%d \n ",
		   curTime,rc,curListSize,head,tail);
#endif
  return rc;
}



/*************************************************************
* routine: 
int  ListObj::insertElementInFrontThisElement(ListElement& newElement, ListElement& existingElement)
*
* Function: this routine inserts the newElement ListElement 
*  in front of the  existingElement which should exist in the list.
*    If the queue is filled, we allow the element to be added
*           
* inputs: 
*    ListElement& newElement : the element to be inserted
*    ListElement& existingElement : marks where in the list the new element is going.
*            If this is NULL, then the list is currently empty
*
* outputs:
*  Returns a SUCCESS or FAILURE.
*        Possible failures:
*             -malloc fails
*             -list already has  > MAXLISTSIZE elements
*
***************************************************************/
int  ListObj::insertElementInFrontThisElement(ListElement& newElement, ListElement& existingElement)
{
int rc = SUCCESS;
ListElement *tmpPtr = NULL;
double curTime =   Scheduler::instance().clock();
int adjustedMAXLISTSIZE = 2 * MAXLISTSIZE;

#ifdef TRACEME
   printf("ListObj:insertElementInFrontThisElement(listsize:%d, MAX:%d, adjustedMAX:%d): add element%d, head:%d, tail:%d \n ",
		   curListSize,MAXLISTSIZE,adjustedMAXLISTSIZE,&newElement,head,tail);
#endif

  if (curListSize < adjustedMAXLISTSIZE) {

//insert at the tail
    curListSize++;
    if (tail == NULL) {
      tail=head=&newElement;
	  head->prev = NULL;
      head->next = NULL;
    }
    else {
      tmpPtr = tail;
      tail->next = &newElement;
      tail = &newElement;
      tail->next = NULL;
	  tail->prev = tmpPtr;
    }

    double curTime =   Scheduler::instance().clock();
    integralTotal += ((double)curListSize) * (curTime - lastIntegralTime);
    if (integralTotal < 0)
      integralTotal = 0;
#ifdef TRACEME
      printf("ListObj::insertElementInFrontThisElement(%lf): curListSize:%d, integralTotal:%f, (curTime-lastIntegralTime):%f\n",
            curTime,curListSize,integralTotal,(curTime-lastIntegralTime));
#endif
    lastIntegralTime = curTime;
    monitorPktCount+=curListSize;
    numberPeriodSamples++;
    if ((curTime - lastSample) >= monitorPeriod) {
      monitorPktCount = monitorPktCount/numberPeriodSamples;
      queueRunningAverage += monitorPktCount;
      numberTotalSamples++;
      lastSample = curTime;
#ifdef TRACEME
      printf("ListObj::insertElementInFrontThisElement(%lf): queue RunningAverage:%f numberTotalSamples:%f ,this period avg:%f (min/max:%f,%f)\n ",
            curTime,queueRunningAverage,numberTotalSamples,monitorPktCount,minQueueLen,maxQueueLen);
#endif
      monitorPktCount=0;
      numberPeriodSamples=0;

      if (curListSize < minQueueLen) {
        minQueueLen = curListSize;
        if (minQueueLen < 0) {
           printf("ListObj::insertElementInFrontThisElement(%lf): HARD ERROR minQueueLen= %d \n ",curTime,minQueueLen);
           exit(1);
        }
      }
      if (curListSize > maxQueueLen) {
        maxQueueLen = curListSize;
        if (maxQueueLen < 0) {
           printf("ListObj::insertElementInFrontThisElement(%lf): HARD ERROR manQueueLen= %d \n ",curTime,maxQueueLen);
           exit(1);
        }
      }
    }

  }
  else {
#ifdef TRACEME
      printf("ListObj:insertElementInFrontThisElement: list overflow , list size: %d\n ",curListSize);
#endif
    totalNumberOverflowEvents++;
    rc = FAILURE;
  }

#ifdef TRACEME
   printf("ListObj:insertElementInFrontThisElement(%lf) return rc %d, curListSize:%d, head:%d, tail:%d \n ",
		   curTime,rc,curListSize,head,tail);
#endif
  return rc;
}

/*************************************************************
* routine: ListElement * ListObj::removeElement()
*
* Function:
*
* inputs: 
*
* outputs:
*   The element at the top of the list is removed and returned.
*
*   A NULL is returned if the list is empty
*
***************************************************************/
ListElement * ListObj::removeElement()
{
  ListElement *tmpPtr = NULL;

#ifdef TRACEME
  printf("ListObj::removeElement: start size: %d\n", this->getListSize());
#endif

  if ((curListSize > 0) && (head != NULL)) {
    curListSize--;

    tmpPtr = head;
    head = tmpPtr->next;

    if (curListSize == 0)
      head = tail = NULL;
	else
      head->prev = NULL;

    double curTime =   Scheduler::instance().clock();
    integralTotal -= ((double)curListSize) * (curTime - lastIntegralTime);
    if(integralTotal < 0)
      integralTotal = 0;
#ifdef TRACEME
    printf("ListObj::removeElement(%lf): integralTotal:%f, curListSize:%d, (curTime-lastInteralTime):%f\n",
         curTime,integralTotal,curListSize,((double)curTime-lastIntegralTime));
#endif
    lastIntegralTime = curTime;
  }

#ifdef TRACEME
  if (tmpPtr == NULL)
    printf("ListObj:removeElement: List Empty!!\n ");
#endif

#ifdef TRACEME
  printf("ListObj::removeElement: exiting,   new list size: %d \n ",this->getListSize());
#endif

  return tmpPtr;
}

/*************************************************************
* routine: int ListObj::removeElement(ListElement *elementPtr)
*
* Function:
*   This method removes the element from the list
*
*   This method MUST delete the list element object.
*
* inputs: 
*   ListElemment *elementPtr:  the element to remove.
*
* outputs:
*   The element at the top of the list is removed and returned.
*
*   A NULL is returned if the list is empty
*
***************************************************************/
int ListObj::removeElement(ListElement *elementPtr)
{
  int i;
  ListElement *tmpPtr = NULL;
  ListElement *tmpPtr1 = NULL;
  ListElement *tmpPtr2 = NULL;
  int rc = FAILURE;

#ifdef TRACEME
printf("ListObj::removeElement: find and remove this element:%x , start size: %d\n", elementPtr,this->getListSize());
#endif

  if (curListSize > 0) {
    tmpPtr = head;
#ifdef TRACEME
    if (tmpPtr != NULL) 
      printf("ListObj::removeElement: headPtr:%x,  at tail: %x\n", head,tail);
#endif
    for (i=0; i<curListSize;i++) {
      if (tmpPtr == elementPtr) {
#ifdef TRACEME
        printf("ListObj::removeElement: found it, remove the index is %d (tmpPtr:%x) \n", i,tmpPtr);
#endif

		//case 1: remove only element
		if (curListSize == 1) {
          head = NULL;
          tail = NULL;
          goto finalStep1;
		} 
		else if (i == 0) {
		//case 2: remove first element
		  head = tmpPtr->next;
		  head->prev = NULL;
          goto finalStep1;
		}
		//case 3: reomve last element
		else if (i == (curListSize - 1)) {
          tail = tmpPtr->prev;
		  tail->next = NULL;
          goto finalStep1;
		}
		//case 4: remove interior element
        else {
          tmpPtr1 = tmpPtr->prev;
          tmpPtr2 = tmpPtr->next;
		  tmpPtr1->next = tmpPtr2;
		  tmpPtr2->prev = tmpPtr1;
          goto finalStep1;
        }

		//Final step, fre the list element
finalStep1:
        curListSize--;
        double curTime =   Scheduler::instance().clock();
        integralTotal -= ((double)curListSize) * (curTime - lastIntegralTime);
        if(integralTotal < 0)
          integralTotal = 0;
#ifdef TRACEME
         printf("ListObj::removeElement(%lf): integralTotal:%f, curListSize:%d, (curTime-lastInteralTime):%f\n",
         curTime,integralTotal,curListSize,((double)curTime-lastIntegralTime));
#endif
        lastIntegralTime = curTime;

        rc = SUCCESS;
		delete tmpPtr;
        if (curListSize == 0)
          head = tail = NULL;

		break;
      }
	  tmpPtr = tmpPtr->next;
	}
  }
#ifdef TRACEME
  if (tmpPtr == NULL)
    printf("ListObj:removeElement: List Empty!!\n ");
#endif

#ifdef TRACEME
  printf("ListObj::removeElement: exiting,   new list size: %d \n ",this->getListSize());
#endif

  return rc;

}


/*************************************************************
* routine: int ListObj::removeThisElement(ListElement *elementPtr)
*
* Function:
*   This method removes this element that is in the list.
*   The prev and next pointers should be valid.
*
*   This method MUST delete the list element object.
*
* inputs: 
*   ListElemment *elementPtr:  the element to remove.
*
* outputs:
*
* A SUCCESS or FAILURE is returned
*
***************************************************************/
int ListObj::removeThisElement(ListElement *elementPtr)
{
  int i;
  ListElement *tmpPtr = NULL;
  ListElement *tmpPtr1 = NULL;
  ListElement *tmpPtr2 = NULL;
  int rc = SUCCESS;

#ifdef TRACEME
printf("ListObj::removeThisElement: find and remove this element:%x , start size: %d\n", elementPtr,this->getListSize());
#endif

  if (curListSize > 0) {
    tmpPtr = elementPtr;

		//case 1: remove only element
		if (curListSize == 1) {
          head = NULL;
          tail = NULL;
          goto finalStep1;
		} 

		//case 2: remove first element
		else if (head == tmpPtr) {
		  head = tmpPtr->next;
		  head->prev = NULL;
          goto finalStep1;
		}

		//case 3: reomve last element
		else if (tail == tmpPtr) {
          tail = tmpPtr->prev;
		  tail->next = NULL;
          goto finalStep1;
		}
		//case 4: remove interior element
        else {
          tmpPtr1 = tmpPtr->prev;
          tmpPtr2 = tmpPtr->next;
		  tmpPtr1->next = tmpPtr2;
		  tmpPtr2->prev = tmpPtr1;
          goto finalStep1;
        }

		//Final step, free the list element
finalStep1:
        curListSize--;
        double curTime =   Scheduler::instance().clock();
        integralTotal -= ((double)curListSize) * (curTime - lastIntegralTime);
        if(integralTotal < 0)
          integralTotal = 0;
#ifdef TRACEME
         printf("ListObj::removeThisElement(%lf): integralTotal:%f, curListSize:%d, (curTime-lastInteralTime):%f\n",
         curTime,integralTotal,curListSize,((double)curTime-lastIntegralTime));
#endif
        lastIntegralTime = curTime;

        rc = SUCCESS;
		delete tmpPtr;
        if (curListSize == 0)
          head = tail = NULL;
  }
#ifdef TRACEME
  else
    printf("ListObj:removeThisElement: List Empty!!\n ");
#endif

#ifdef TRACEME
  printf("ListObj::removeThisElement: exiting,   new list size: %d \n ",this->getListSize());
#endif

  return rc;
}

int ListObj::getListSize()
{
  return curListSize;
}

void ListObj::setListSize(int newListSize)
{
  MAXLISTSIZE = newListSize;
#ifdef TRACEME
  printf("ListObj:setListSize:  new  max list size: %d \n",MAXLISTSIZE);
#endif
}

void ListObj::updateStats()
{
  double curTime =   Scheduler::instance().clock();
#ifdef TRACEME
  printf("ListObj:updateStats:(%lf)\n",curTime);
#endif
}

void ListObj::printStatsSummary()
{
  double curTime =   Scheduler::instance().clock();
  double finalQAvg = getAverageQueueLevel();

  printf("ListObj:printStatsSummary:(%lf), FinalQAvg: %6.1f, numberOverFlowEvents:%d \n", curTime,finalQAvg,totalNumberOverflowEvents);
}



unsigned int ListObj::getNumberOverFlowEvents()
{
  return totalNumberOverflowEvents;
}

/*************************************************************
* function:
*  int ListObj::displayListElements()
*
* explanation:  This method displays the
*   list elements to standard out.
*
* inputs: 
*
***************************************************************/
int ListObj::displayListElements()
{
/* unused - int i; */
int j;
//ListElement *tmpPtr;

  j = getListSize();

#ifdef TRACEME
  printf("ListObj::displayListElements: list size %d \n ",this->getListSize());
#endif

  return 0;
}

/*************************************************************
* function:
*  int ListObj::displayListElements()
*
* explanation:  This method displays the
*   list elements to standard out.
*
* inputs: 
*
***************************************************************/
double ListObj::getAverageQueueLevel()
{
  double curTime =   Scheduler::instance().clock();
  double lifeTime =  Scheduler::instance().clock() - initTime;
  double rc = 0;
  double meanQL =0;

  if (numberTotalSamples >  0)
    meanQL = queueRunningAverage  / numberTotalSamples;

  if (lifeTime > 0)
    rc = integralTotal / lifeTime;

#ifdef TRACEME
  printf("ListObj::getAverageQueueLevel(%lf): current list size:%d,  integralTotal:%f, lastSizeTime:%f, avgListSize:%f, lifeTime:%f, mean QL:%f \n ",
     curTime,curListSize,integralTotal,lastIntegralTime, rc,lifeTime, meanQL);
#endif

//  return rc;
  return meanQL; 
}

ListElement * ListObj::nextElement()
{
	ListElement *tmpPtr = NULL;

#ifdef TRACEME
	printf("ListObj::nextElement: start size: %d\n", this->getListSize());
#endif

	if ((curListSize > 0) && (head != NULL)) {

		      tmpPtr = curElement;
		      curElement = tmpPtr->next;
	}

	        return tmpPtr;
}

ListElement *ListObj::getElement(int elementNumber)  //returns ptr to specific element
{
  ListElement *tmpPtr = NULL;
  ListElement *returnPtr = NULL;

  int i;

#ifdef TRACEME
	printf("ListObj::getElement: list size:%d,  return number %d\n", this->getListSize(),elementNumber);
#endif

	if ((curListSize > 0) && (elementNumber+1 <= curListSize)) {
      //advance through the list to the elementNumber
	  tmpPtr = head;
	  for (i=0; i<elementNumber+1;i++) {
        returnPtr = tmpPtr;
#ifdef TRACEME
        printf("ListObj::getElement:i:%d, tmpPtr:%0x \n", i,returnPtr);
#endif
	    tmpPtr = tmpPtr->next;
	  }
	}

#ifdef TRACEME
	printf("ListObj::getElement: return handle: %0x\n", returnPtr);
#endif

	return returnPtr;
}

void ListObj::setCurHeadElement()
{	
	curElement = head;
}

ListElement * ListObj::accessCurrentElement()
{
	ListElement *tmpPtr = NULL;
	tmpPtr = curElement;
	
	return tmpPtr;

}


IntegerListObj::IntegerListObj()
{
  MAXLISTSIZE = 0;     
#ifdef TRACEME
  printf("IntegerListObj: constructed list \n");
#endif
}

IntegerListObj::~IntegerListObj()
{
#ifdef TRACEME
  printf("IntegerListObj: destructed list \n");
#endif

  //JJM WRONG  delete all elements in the list
}

void IntegerListObj::initList(int maxListSize)
{

  MAXLISTSIZE = maxListSize;

//  MAXLISTSIZE = 32000;     
//  MAXLISTSIZE = 3;  //for test   

  head = tail  = NULL;
  curListSize = 0;


  initTime = 0;
  integralTotal = 0;
  lastIntegralTime = initTime;

  monitorPktCount = 0;
  lastSample = 0;
  queueRunningAverage = 0;
  numberTotalSamples = 0;
  numberPeriodSamples = 0;
  monitorPeriod = 1.0;
  minQueueLen = 0;
  maxQueueLen = 0;


  totalNumberOverflowEvents = 0;
#ifdef TRACEME
  printf("IntegerListObj:init: max list size: %d \n",MAXLISTSIZE);
#endif

}

/*************************************************************
* routine:
*   int  ListObj::addElement(ListElement& element)
*
* Function: this routine inserts the ListElement to the tail of 
* the list.
*           
* inputs: 
*    ListElement& element : the element to be inserted
*
* outputs:
*   rc : 1 is failure, else a 0
*        Possible failures:
*             -malloc fails
*             -list already has  > MAXLISTSIZE elements
*
***************************************************************/
int  IntegerListObj::addElement(IntegerListElement& element)
{
int rc = SUCCESS;
IntegerListElement *tmpPtr = NULL;

#ifdef TRACEME
   printf("IntegerListObj:addElement(listsize:%d): add element%d, head:%d, tail:%d \n ",
		   curListSize,&element,head,tail);
#endif

  if (curListSize < MAXLISTSIZE) {
//insert at the tail
    curListSize++;
    if (tail == NULL) {
      tail=head=&element;
	  head->prev = NULL;
      head->next = NULL;
    }
    else {
      tmpPtr = tail;
      tail->next = &element;
      tail = &element;
      tail->next = NULL;
	  tail->prev = tmpPtr;
    }
    double curTime =   Scheduler::instance().clock();
    integralTotal += ((double)curListSize) * (curTime - lastIntegralTime);
    if (integralTotal < 0)
      integralTotal = 0;
#ifdef TRACEME
      printf("IntegerListObj::addElement(%lf): curListSize:%d, integralTotal:%f, (curTime-lastIntegralTime):%f\n",
            curTime,curListSize,integralTotal,(curTime-lastIntegralTime));
#endif
    lastIntegralTime = curTime;
    monitorPktCount+=curListSize;
    numberPeriodSamples++;
    if ((curTime - lastSample) >= monitorPeriod) {
      monitorPktCount = monitorPktCount/numberPeriodSamples;
      queueRunningAverage += monitorPktCount;
      numberTotalSamples++;
      lastSample = curTime;
#ifdef TRACEME
      printf("IntegerListObj::addElement(%lf): queue RunningAverage:%f numberTotalSamples:%f ,this period avg:%f (min/max:%f,%f)\n ",
            curTime,queueRunningAverage,numberTotalSamples,monitorPktCount,minQueueLen,maxQueueLen);
#endif
      monitorPktCount=0;
      numberPeriodSamples=0;

      if (curListSize < minQueueLen) {
        minQueueLen = curListSize;
        if (minQueueLen < 0) {
           printf("IntegerListObj::addElement(%lf): HARD ERROR minQueueLen= %d \n ",curTime,minQueueLen);
           exit(1);
        }
      }
      if (curListSize > maxQueueLen) {
        maxQueueLen = curListSize;
        if (maxQueueLen < 0) {
           printf("IntegerListObj::addElement(%lf): HARD ERROR manQueueLen= %d \n ",curTime,maxQueueLen);
           exit(1);
        }
      }
    }
  }
  else {
#ifdef TRACEME
      printf("IntegerListObj:addElement: list overflow , list size: %d\n ",curListSize);
#endif
    totalNumberOverflowEvents++;
    rc = FAILURE;
  }
#ifdef TRACEME
	printf("IntegerListObj::addElement: added handle: %0x to tail \n", tail);
#endif

  return rc;
}

/*************************************************************
* routine:
*   int  ListObj::insertToList(ListElement& element)
*
* Function: this routine inserts the ListElement to the head of
* the list.
*           
* inputs: 
*    ListElement& element : the element to be inserted
*
* outputs:
*   rc : 1 is failure, else a 0
*        Possible failures:
*             -malloc fails
*             -list already has  > MAXLISTSIZE elements
*
***************************************************************/
int  IntegerListObj::addElementatHead(IntegerListElement& element)
{
int rc = SUCCESS;
IntegerListElement *tmpPtr = NULL;


  if (curListSize < MAXLISTSIZE) {
//insert at the tail
    curListSize++;
    if (tail == NULL) {
      tail=head=&element;
	  head->prev = NULL;
      head->next = NULL;
    }
    else {
      tmpPtr = head;
      element.prev= NULL;
      element.next = head;
      head->prev = &element;
	  head = &element;
    }
    double curTime =   Scheduler::instance().clock();
    integralTotal += ((double)curListSize) * (curTime - lastIntegralTime);
    if (integralTotal < 0)
      integralTotal = 0;
#ifdef TRACEME
      printf("IntegerListObj::addElementatHead(%lf): curListSize:%d, integralTotal:%f, (curTime-lastIntegralTime):%f\n",
            curTime,curListSize,integralTotal,(curTime-lastIntegralTime));
#endif
    lastIntegralTime = curTime;
    monitorPktCount+=curListSize;
    numberPeriodSamples++;
    if ((curTime - lastSample) >= monitorPeriod) {
      monitorPktCount = monitorPktCount/numberPeriodSamples;
      queueRunningAverage += monitorPktCount;
      numberTotalSamples++;
      lastSample = curTime;
#ifdef TRACEME
      printf("IntegerListObj::addElementatHead(%lf): queue RunningAverage:%f numberTotalSamples:%f ,this period avg:%f (min/max:%f,%f)\n ",
            curTime,queueRunningAverage,numberTotalSamples,monitorPktCount,minQueueLen,maxQueueLen);
#endif
      monitorPktCount=0;
      numberPeriodSamples=0;

      if (curListSize < minQueueLen) {
        minQueueLen = curListSize;
        if (minQueueLen < 0) {
           printf("IntegerListObj::addElementatHead(%lf): HARD ERROR minQueueLen= %d \n ",curTime,minQueueLen);
           exit(1);
        }
      }
      if (curListSize > maxQueueLen) {
        maxQueueLen = curListSize;
        if (maxQueueLen < 0) {
           printf("IntegerListObj::addElementatHead(%lf): HARD ERROR manQueueLen= %d \n ",curTime,maxQueueLen);
           exit(1);
        }
      }
    }
  }
  else {
#ifdef TRACEME
      printf("IntegerListObj:addElementatHead: list overflow , list size: %d\n ",curListSize);
#endif
    totalNumberOverflowEvents++;
    rc = FAILURE;
  }

  return rc;
}


/*************************************************************
* routine: ListElement * ListObj::removeElement()
*
* Function:
*
* inputs: 
*
* outputs:
*   The element at the top of the list is removed and returned.
*
*   A NULL is returned if the list is empty
*
***************************************************************/
IntegerListElement * IntegerListObj::removeElement()
{
IntegerListElement *tmpPtr = NULL;

#ifdef TRACEME
printf("IntegerListObj::removeElement: start size: %d\n", this->getListSize());
#endif

  if ((curListSize > 0) && (head != NULL)) {
    curListSize--;
    double curTime =   Scheduler::instance().clock();
    integralTotal -= ((double)curListSize) * (curTime - lastIntegralTime);
    if(integralTotal < 0)
      integralTotal = 0;
#ifdef TRACEME
    printf("IntegerListObj::removeElement(%lf): integralTotal:%f, curListSize:%d, (curTime-lastInteralTime):%f\n",
         curTime,integralTotal,curListSize,((double)curTime-lastIntegralTime));
#endif
    lastIntegralTime = curTime;

    tmpPtr = head;
    head = tmpPtr->next;


    if (curListSize == 0)
      head = tail = NULL;
	else
	  head->prev = NULL;

    }

#ifdef TRACEME
  if (tmpPtr == NULL)
    printf("IntegerListObj:removeElement: List Empty!!\n ");
#endif

#ifdef TRACEME
  printf("IntegerListObj::removeElement: exiting,   new list size: %d \n ",this->getListSize());
#endif

  return tmpPtr;
}

/*************************************************************
* routine: ListElement * ListObj::removeElement()
*
* Function:
*
* inputs: 
*
* outputs:
*  Returns SUCCESS or FAILURE
*
***************************************************************/
int IntegerListObj::removeElement(int data)
{
  int i;
  IntegerListElement *tmpPtr = NULL;
  IntegerListElement *tmpPtr1 = NULL;
  IntegerListElement *tmpPtr2 = NULL;
  int rc = SUCCESS;

#ifdef TRACEME
printf("IntegerListObj::removeElement: find and remove this element:%d , start size: %d\n", data,this->getListSize());
#endif

  if (curListSize > 0) {
    tmpPtr = head;
#ifdef TRACEME
    if (tmpPtr != NULL) 
      printf("IntegerListObj::removeElement: Value at head: %d,  at tail: %d\n", head->getData(), tail->getData());
#endif
    for (i=0; i<curListSize;i++) {
      if (tmpPtr->getData() == data) {
#ifdef TRACEME
        printf("IntegerListObj::removeElement: found it, remove the index is %d (tmpPtr:%x) \n", i,tmpPtr);
#endif

		//case 1: remove only element
		if (curListSize == 1) {
          head = NULL;
          tail = NULL;
          goto finalStep2;
		} 
		else if (i == 0) {
		//case 2: remove first element
		  head = tmpPtr->next;
		  head->prev = NULL;
          goto finalStep2;
		}
		//case 3: reomve last element
		else if (i == (curListSize - 1)) {
          tail = tmpPtr->prev;
		  tail->next = NULL;
          goto finalStep2;
		}
		//case 4: remove interior element
        else {
          tmpPtr1 = tmpPtr->prev;
          tmpPtr2 = tmpPtr->next;
		  tmpPtr1->next = tmpPtr2;
		  tmpPtr2->prev = tmpPtr1;
          goto finalStep2;
        }

		//Final step, fre the list element
finalStep2:
        curListSize--;
        double curTime =   Scheduler::instance().clock();
        integralTotal -= ((double)curListSize) * (curTime - lastIntegralTime);
        if(integralTotal < 0)
        integralTotal = 0;
#ifdef TRACEME
        printf("IntegerListObj::removeElement(%lf): integralTotal:%f, curListSize:%d, (curTime-lastInteralTime):%f\n",
            curTime,integralTotal,curListSize,((double)curTime-lastIntegralTime));
#endif
        lastIntegralTime = curTime;

		delete tmpPtr;
        if (curListSize == 0)
          head = tail = NULL;

		break;
      }
	  tmpPtr = tmpPtr->next;
	}
  }
#ifdef TRACEME
  if (tmpPtr == NULL)
    printf("IntegerListObj:removeElement: List Empty!!\n ");
#endif

#ifdef TRACEME
  printf("IntegerListObj::removeElement: exiting,   new list size: %d \n ",this->getListSize());
#endif

  return rc;
}



int IntegerListObj::getListSize ()
{
  return curListSize;
}



void IntegerListObj::setListSize(int newListSize)
{
  MAXLISTSIZE = newListSize;
#ifdef TRACEME
  printf("IntegerListObj:setListSize:  new  max list size: %d \n",MAXLISTSIZE);
#endif
}


unsigned int IntegerListObj::getNumberOverFlowEvents()
{
  return totalNumberOverflowEvents;
}

/*************************************************************
* function:
*  int ListObj::displayListElements()
*
* explanation:  This method displays the
*   list elements to standard out.
*
* inputs: 
*
***************************************************************/
int IntegerListObj::displayListElements()
{
  int i;
  IntegerListElement *tmpPtr = NULL;
  IntegerListElement *returnPtr = NULL;
  int rc = SUCCESS;

#ifdef TRACEME
  printf("IntegerListObj::displayListElements: list size %d \n ",this->getListSize());
#endif


  if (curListSize > 0) {
    tmpPtr = head;
    printf("IntegerListObj::displayList: ");
    for (i=0; i<curListSize;i++) {
      printf(" %d, ", tmpPtr->getData());
	   tmpPtr = tmpPtr->next;
	}
    printf("  \n ");
  }
#ifdef TRACEME
  else
    printf("IntegerListObj:: the List is empty \n");
#endif

  return rc;
}

/*************************************************************
* function:
*  int IntegerListObj::isElement(int ChIDX)
*
* explanation:  This method returns TRUE if the param is
*   a member of the list.
*
* inputs: 
*   int ChIDX:  we want to know if thie channel is in the list
*
* outputs:
*   returns a TRUE or FALSE
*
***************************************************************/
int IntegerListObj::isElement(int ChIDX)
{
  int i;
  IntegerListElement *tmpPtr = NULL;
  IntegerListElement *returnPtr = NULL;
  int rc = FALSE;

#ifdef TRACEME
  printf("IntegerListObj::isElement: ChIDX:%d list size %d \n ",ChIDX,this->getListSize());
#endif


  if (curListSize > 0) {
    tmpPtr = head;
    for (i=0; i<curListSize;i++) {
       if (tmpPtr->getData() ==ChIDX) {
		  rc = TRUE;
		  break;
	   }
	   tmpPtr = tmpPtr->next;
	}
  }
#ifdef TRACEME
  else
    printf("IntegerListObj::isElement: the List is empty \n");
#endif

  return rc;
}

OrderedListObj::OrderedListObj() : ListObj()
{
#ifdef TRACEME
  printf("OrderedListObj: constructed list \n");
#endif
}


OrderedListObj::~OrderedListObj()
{
#ifdef TRACEME
  printf("OrderedListObj: destructed list \n");
#endif
}


/*************************************************************
* routine:
*  int  OrderedListObj::addElement(OrderedListElement& element)
*
* Function: this routine inserts the ListElement in the list
*  maintaining order based on the key.  The range of the key
*  is 0 (located at the top of the list) to something >0
*  THe first element will have the smallest key, the last
*  element will have the largest key.
*           
* inputs: 
*    ListElement& element : the element to be inserted
*
* outputs:
*   rc : 1 is failure, else a 0
*        Possible failures:
*             -malloc fails
*             -list already has  > MAXLISTSIZE elements
*
***************************************************************/
int  OrderedListObj::addElement(OrderedListElement& element)
{
  int rc = SUCCESS;
  OrderedListElement *tmpPtr = NULL;
  OrderedListElement *returnPtr = NULL;
  int key = element.getKey();
  int elementIndex = 0;
  int i;

#ifdef TRACEME
   printf("OrderedListObj:addElement(%lf):  listsize:%d, add Key:%d element%x, head:%x, tail:%x \n ",
     Scheduler::instance().clock(), curListSize,key, &element,head,tail);
#endif

  if (curListSize < MAXLISTSIZE) {
//insert at the tail
    if (tail == NULL) {
      tail=head=&element;
	  head->prev = NULL;
      head->next = NULL;
#ifdef TRACEME
     printf("OrderedListObj:addElement(%lf): List was empty, add this element add Key:%d element%x, head:%x, tail:%x \n ",
           Scheduler::instance().clock(), curListSize,key, &element,head,tail);
#endif
    }
    else {
      //else the list is not empty...

      tmpPtr = (OrderedListElement *)head;
#ifdef TRACEME
      printf("OrderedListObj:addElement(%lf): List IS NOT empty, Add to top? element key:%d  head key:%d \n ",
           Scheduler::instance().clock(), key,  tmpPtr->getKey());
#endif
      //Check for case of added to head
      if (key <= tmpPtr->getKey()) {
//* $8212010 : changed addElement for Ordered Obj, it allows elements with
// Looks like we just ignore it...
        if (tmpPtr->getKey() == key) {
          printf("OrderedListObj::addElement(%lf): ERROR:  Element exists at head? %d\n ",
              Scheduler::instance().clock(),tmpPtr->getKey());
        }
        element.prev = NULL;
        element.next = head;
        head->prev = &element;
        head = &element;

      }
      //Check for case of added to  tail
      tmpPtr = (OrderedListElement *)tail;
      if (key >= tmpPtr->getKey()) {
        if (tmpPtr->getKey() == key) {
          printf("OrderedListObj::addElement(%lf): ERROR:  Element exists at tail? %d\n ",
              Scheduler::instance().clock(),tmpPtr->getKey());
        }
        element.prev = tail;
        element.next = NULL;
        tail->next = &element;
        tail = &element;
      }
 
      //Else will insert in the middle
      tmpPtr = (OrderedListElement *)head;
      for (i=0; i<curListSize;i++) {
        if (tmpPtr->getKey() >= key) {
#ifdef TRACEME
          printf("OrderedListObj:addElement(%lf): Insert here: %d, current ptr is at Key:%d element%d, head:%d, tail:%d \n ",
              Scheduler::instance().clock(), i, key, &element,head,tail);
#endif
          //to insert ....
          // insert before the element pointed to my tmpPtr
          element.prev = tail;
          element.next = NULL;
          tmpPtr->prev = &element;
          tmpPtr->prev->next = &element;
          break;
        }
	    tmpPtr = (OrderedListElement *)tmpPtr->next;
	  }
    }
  }
  else {
#ifdef TRACEME
      printf("OrderedListObj:addElement: list overflow , list size: %d\n ",curListSize);
#endif
    totalNumberOverflowEvents++;
    rc = FAILURE;
  }

  if (rc == SUCCESS) {
    curListSize++;
    double curTime =   Scheduler::instance().clock();
    integralTotal += ((double)curListSize) * (curTime - lastIntegralTime);
    if (integralTotal < 0)
      integralTotal = 0;
#ifdef TRACEME
      printf("OrderedListObj::addElement(%lf): curListSize:%d, integralTotal:%f, (curTime-lastIntegralTime):%f\n",
            curTime,curListSize,integralTotal,(curTime-lastIntegralTime));
#endif
    lastIntegralTime = curTime;
    monitorPktCount+=curListSize;
    numberPeriodSamples++;
    if ((curTime - lastSample) >= monitorPeriod) {
      monitorPktCount = monitorPktCount/numberPeriodSamples;
      queueRunningAverage += monitorPktCount;
      numberTotalSamples++;
      lastSample = curTime;

      if (monitorPktCount < minQueueLen) {
        minQueueLen = monitorPktCount;
        if (minQueueLen < 0) {
           printf("OrderedListObj::addElement(%lf): HARD ERROR minQueueLen= %d \n ",curTime,minQueueLen);
           exit(1);
        }
      }
      if (monitorPktCount > maxQueueLen) {
        maxQueueLen = monitorPktCount;
        if (maxQueueLen < 0) {
           printf("OrderedListObj::addElement(%lf): HARD ERROR manQueueLen= %d \n ",curTime,maxQueueLen);
           exit(1);
        }
      }
#ifdef TRACEME
      printf("OrderedListObj::addElement(%lf): queue RunningAverage:%f numberTotalSamples:%f ,this period avg:%f (min/max:%f,%f)\n ",
            curTime,queueRunningAverage,numberTotalSamples,monitorPktCount,minQueueLen,maxQueueLen);
#endif
      monitorPktCount=0;
      numberPeriodSamples=0;
    }
  }
#ifdef TRACEME
	printf("OrderedListObj::addElement(%lf): exit rc:%d,  curListSize:%d, head:%x, tail:%x \n", 
          Scheduler::instance().clock(), rc, curListSize, head,tail);
#endif


  return rc;
}

/*************************************************************
* function:
*    int OrderedListObj::isElement(int keyMatch)
*
* explanation:  This method returns TRUE if the
*   list contains an element with the keyMatch key.
*
* inputs: 
*   int keyMatch:  The key for the match 
*
* outputs:
*   returns a TRUE or FALSE
*
***************************************************************/
int OrderedListObj::isElement(int keyMatch)
{
  int i;
  OrderedListElement *tmpPtr = NULL;
  OrderedListElement *returnPtr = NULL;
  int rc = FALSE;

#ifdef TRACEME
  printf("OrderedListObj::isElement: match key %d, list size %d \n ",keyMatch,this->getListSize());
#endif


  if (curListSize > 0) {
    tmpPtr = (OrderedListElement *)head;
    for (i=0; i<curListSize;i++) {
       if (tmpPtr->getKey() == keyMatch) {
		  rc = TRUE;
		  break;
	   }
	   tmpPtr = (OrderedListElement *)tmpPtr->next;
	}
  }
#ifdef TRACEME
  else
    printf("OrderedListObj::isElement: the List is empty \n");
#endif

  return rc;
}


/*************************************************************
* routine: int OrderedListObj::removeElement(OrderedListElement& elementParam)
*
* Function:
*   This method removes the element from the List.  THe
*    Element is DELETED!!!
*
* inputs: 
*
* outputs:
*  Returns SUCCESS or FAILURE
*
***************************************************************/
int OrderedListObj::removeElement(OrderedListElement& elementParam)
{
  int i;
  OrderedListElement *tmpPtr = NULL;
  OrderedListElement *tmpPtr1 = NULL;
  OrderedListElement *tmpPtr2 = NULL;
  int rc = SUCCESS;
  OrderedListElement *OrderedElementParam = (OrderedListElement *)&elementParam;
  int key = OrderedElementParam->getKey();



#ifdef TRACEME
printf("OrderedListObj::removeElement: find and remove this element:%d , start size: %d\n",key,this->getListSize());
#endif

  if (curListSize > 0) {
    tmpPtr = (OrderedListElement *) head;
#ifdef TRACEME
    if (tmpPtr != NULL) 
      printf("OrderedListObj::removeElement: Value at head: %d,  at tail: %d\n", tmpPtr->getKey(), tmpPtr->getKey());
#endif
    for (i=0; i<curListSize;i++) {
      if (tmpPtr->getKey() == key) {
#ifdef TRACEME
        printf("OrderedListObj::removeElement: found it, remove the index is %d (tmpPtr:%x) \n", i,tmpPtr);
#endif

		//case 1: remove only element
		if (curListSize == 1) {
          head = NULL;
          tail = NULL;
          goto finalStep2;
		} 
		else if (i == 0) {
		//case 2: remove first element
		  head = tmpPtr->next;
		  head->prev = NULL;
          goto finalStep2;
		}
		//case 3: reomve last element
		else if (i == (curListSize - 1)) {
          tail = tmpPtr->prev;
		  tail->next = NULL;
          goto finalStep2;
		}
		//case 4: remove interior element
        else {
          tmpPtr1 = (OrderedListElement *)tmpPtr->prev;
          tmpPtr2 = (OrderedListElement *)tmpPtr->next;
		  tmpPtr1->next = tmpPtr2;
		  tmpPtr2->prev = tmpPtr1;
          goto finalStep2;
        }

		//Final step, fre the list element
finalStep2:
        curListSize--;
        double curTime =   Scheduler::instance().clock();
        integralTotal -= ((double)curListSize) * (curTime - lastIntegralTime);
        if(integralTotal < 0)
          integralTotal = 0;
#ifdef TRACEME
        printf("OrderedListObj::removeElement(%lf): integralTotal:%f, curListSize:%d, (curTime-lastInteralTime):%f\n",
             curTime,integralTotal,curListSize,((double)curTime-lastIntegralTime));
#endif
        lastIntegralTime = curTime;

		delete tmpPtr;
        if (curListSize == 0)
          head = tail = NULL;

		break;
      }
	  tmpPtr = (OrderedListElement *) tmpPtr->next;
	}
  }
#ifdef TRACEME
  if (tmpPtr == NULL)
    printf("OrderedListObj:removeElement: List Empty!!\n ");
#endif

#ifdef TRACEME
  printf("OrderedListObj::removeElement: exiting,   new list size: %d \n ",this->getListSize());
#endif

  return rc;
}

/*************************************************************
* function:
*  int ListObj::displayListElements()
*
* explanation:  This method displays the
*   list elements to standard out.
*
* inputs: 
*
* outputs: always returns success
***************************************************************/
int OrderedListObj::displayListElements()
{
  int i;
  OrderedListElement *tmpPtr = NULL;
  OrderedListElement *returnPtr = NULL;
  int rc = SUCCESS;

  printf("OrderedListObj::displayListElement: list size %d \n ",this->getListSize());


  if (curListSize > 0) {
    tmpPtr = (OrderedListElement *)head;
    for (i=0; i<curListSize;i++) {
      printf("OrderedListObj::displayListElement: Element key: %d\n ",tmpPtr->getKey());
	  tmpPtr = (OrderedListElement *)tmpPtr->next;
	}
  }
  else
    printf("OrderedListObj::displayListElement: the List is empty \n");
  return rc;
}



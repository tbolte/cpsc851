#ifndef MM_H
#define MM_H

class Minmax {

public:
	Minmax(int num_flows, int num_channels, int **channel_map, int *flow_demands, int *channel_capacity);
	void init();
	int computeAllocation(int num_flows, int num_channels, int **channel_map, int *flow_demands, int *channel_capacity);

private:
#define WHITE 0
#define GRAY 1
#define BLACK 2
#define MAX_NODES 100
#define INFINITE 0x7fffffff

int nodes;                          /* number of nodes in flow network */
int edges;                          /* number of edges in flow network */
int capacity[MAX_NODES][MAX_NODES]; /* capacity matrix */
int flow[MAX_NODES][MAX_NODES];     /* flow matrix */
int color[MAX_NODES];               /* search matrix */
int augment[MAX_NODES];             /* augmenting path matrix */
int q[MAX_NODES+2];                 /* queue pointers for augmenting path search */
int head, tail;                     /* queue pointers */

	void make_flow_network(int num_flows, int num_channels, int channels[], int flows_connected[], int current_flow_demands[], 
				int **channel_map, int *flow_demands, int *channel_capacity);
	void find_allocation(int num_flows, int num_channels, int channels[], int flows_connected[], int current_flow_demands[], 
				int **channel_map, int *flow_demands, int *channel_capacity);
	int find_fair_allocation(int low, int high, int num_flows, int num_channels, int channels[], int flows_connected[], int current_flow_demands[]);
	int all_fair(int level);
	int bw_available(int num_flows, int num_channels, int channels[], int flows_connected[], int current_flow_demands[]);
	int flows_available(int num_flows, int num_channels, int channels[], int flows_connected[], int current_flow_demands[], 
				int **channel_map, int *flow_demands, int *channel_capacity);
	int find_maxflow(void);
	int augmenting_path(void);
	int min(int x, int y);
	void enqueue(int x);
	int dequeue(void);
	
};	

#endif

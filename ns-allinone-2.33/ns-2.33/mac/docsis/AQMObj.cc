/************************************************************************
* File:  AQMObj.cc
*
* Purpose:
*  This module contains a basic list object.
*
* Revisions:
*
*  Remove the IntegerAQMObj....
*
************************************************************************/
#include "AQMObj.h"
#include <iostream>

#include "object.h"


#include "globalDefines.h"

#include "docsisDebug.h"
//#define TRACEME 0

AQMObj::AQMObj() : ListObj()
{
#ifdef TRACEME
  printf("AQMObj: constructed list \n");
#endif
}

AQMObj::~AQMObj()
{
#ifdef TRACEME
  printf("AQMObj: destructed list \n");
#endif
}

void AQMObj::initList(int maxListSize)
{

  ListObj::initList(maxListSize);

#ifdef TRACEME
  printf("AQMObj:init: max list size: %d \n",MAXLISTSIZE);
#endif

}

void AQMObj::setAQMParams(int maxAQMSize, int minth, int maxth)
{

#ifdef TRACEME
  printf("AQMObj:setAQMParams: minth:%d  maxth:%d \n",minth,maxth);
#endif

}


/*************************************************************
* routine:
*   int  AQMObj::addElement(ListElement& element)
*
* Function: this routine inserts the ListElement to the tail of 
* the list.
*           
* inputs: 
*    ListElement& element : the element to be inserted
*
* outputs:
*  Returns a SUCCESS or FAILURE.
*        Possible failures:
*             -malloc fails
*             -list already has  > MAXLISTSIZE elements
*
***************************************************************/
int  AQMObj::addElement(ListElement& element)
{
int rc = SUCCESS;
ListElement *tmpPtr = NULL;

#ifdef TRACEME
   printf("AQMObj:addElement(listsize:%d): add element%d, head:%d, tail:%d \n ",
		   curListSize,&element,head,tail);
#endif

  if (curListSize < MAXLISTSIZE) {
//insert at the tail
    curListSize++;
    if (tail == NULL) {
      tail=head=&element;
	  head->prev = NULL;
      head->next = NULL;
    }
    else {
      tmpPtr = tail;
      tail->next = &element;
      tail = &element;
      tail->next = NULL;
	  tail->prev = tmpPtr;
    }
  }
  else {
#ifdef TRACEME
      printf("AQMObj:addElement: list overflow , list size: %d\n ",curListSize);
#endif
    totalNumberOverflowEvents++;
    rc = FAILURE;
  }

  return rc;
}

/*************************************************************
* routine: int  AQMObj::addElementatHead(ListElement& element)
*
* Function: this routine inserts the ListElement to the head of
* the list.
*           
* inputs: 
*    ListElement& element : the element to be inserted
*
* outputs:
*   rc : 1 is failure, else a 0
*        Possible failures:
*             -malloc fails
*             -list already has  > MAXLISTSIZE elements
*
***************************************************************/
int  AQMObj::addElementatHead(ListElement& element)
{
  int rc = SUCCESS;
  ListElement *tmpPtr = NULL;


  if (curListSize < MAXLISTSIZE) {
//insert at the tail
    curListSize++;
    if (tail == NULL) {
      tail=head=&element;
	  head->prev = NULL;
      head->next = NULL;
    }
    else {
      tmpPtr = head;
      element.prev= NULL;
      element.next = head;
      head->prev = &element;
	  head = &element;
    }
  }
  else {
#ifdef TRACEME
      printf("AQMObj:addElementatHead: list overflow , list size: %d\n ",curListSize);
#endif
    totalNumberOverflowEvents++;
    rc = FAILURE;
  }

}


/*************************************************************
* routine: ListElement * AQMObj::removeElement()
*
* Function:
*
* inputs: 
*
* outputs:
*   The element at the top of the list is removed and returned.
*
*   A NULL is returned if the list is empty
*
***************************************************************/
ListElement * AQMObj::removeElement()
{
  ListElement *tmpPtr = NULL;

#ifdef TRACEME
  printf("AQMObj::removeElement: start size: %d\n", this->getListSize());
#endif

  if ((curListSize > 0) && (head != NULL)) {
    curListSize--;

    tmpPtr = head;
    head = tmpPtr->next;

    if (curListSize == 0)
      head = tail = NULL;
	else
      head->prev = NULL;

  }

#ifdef TRACEME
  if (tmpPtr == NULL)
    printf("AQMObj:removeElement: List Empty!!\n ");
#endif

#ifdef TRACEME
  printf("AQMObj::removeElement: exiting,   new list size: %d \n ",this->getListSize());
#endif

  return tmpPtr;
}


/*************************************************************
* routine: void AQMObj::adaptAQMParams()
*
* Function: this routine adapts the AQM parameters.
*   The algorithm is TBD.
*           
* inputs: 
*
* outputs:
*
***************************************************************/
void AQMObj::adaptAQMParams()
{

#ifdef TRACEME
//  printf("AQMObj:adaptAQMParams: minth:%d  maxth:%d \n",minth,maxth);
#endif

}



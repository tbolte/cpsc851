/***************************************************************************
 * Module: reseqMgr
 *
 * Explanation:
 *   This file contains the class definition of the object that
 *   packages  the resequencing support. 
 *
 *   This object contains a default service flow that is used to identfy/track
 *   management flows. (MGMTServiceFlow)
 * 
 *
 * Revisions:
 *   
 *
************************************************************************/

#ifndef ns_reseqMgr_h
#define ns_reseqMgr_h

#include "packet.h"
#include "rtp.h"
#include "hdr-docsis.h"
#include "ListObj.h"
#include "packetListElement.h"
#include "reseqFlowListElement.h"
#include "serviceFlowMgr.h"
#include "serviceFlowObject.h"

class macPhyInterface;

#define ReseqStateIDLE 0
#define ReseqStateTimerActive 1




#define MAX_RESEQ_WAIT_TIME  .020
//In units of packet Seq numbers
#define MAX_GAP_ALLOWED 1000
#define MAX_RESEQ_QUEUE 1024

struct reseqMgrStatsType
{
  double  totalNumberArrivals;
  double  totalByteArrivals;
  double  numberQueued;
  double  numberFailures;
  double  numberFramesSentUp;
  double   totalOutOfOrderArrivalCount;
  double  numberGAPExceededEvents;
  double  numberFastRecoveryEvents;
};


/*************************************************************************
 ************************************************************************/
class reseqMgr : public serviceFlowMgr
{
public:
  reseqMgr();

  void init(int direction, macPhyInterface *myMacPhyInterfaceParam);

  int  addServiceFlow(int macaddr, int32_t src_ip, int32_t dst_ip, packet_t pkt_type, u_int16_t flowID,int myBGIDParam, int numberChannels);
  serviceFlowObject *addServiceFlow(u_int16_t flowID, int numberChannels);

  int frameArrival(Packet *p, int channelNumber);

//redefine this...
//  serviceFlowObject *getServiceFlow(Packet *p);
//$A801
//  reseqFlowListElement *getServiceFlow(Packet *p);
  reseqFlowListElement *getReseqServiceFlow(Packet *p);

  void getStats(struct reseqMgrStatsType *callersStats);
  void printStatsSummary(char *outputFile);

  //structure that contains list of service flows
  OrderedListObj *myOrderedSFList;


private:

 int maxListSize;
 double Max_Resequencing_Wait; //the timeout
 int maxGapAllowed;
 struct reseqMgrStatsType myStats;
 reseqFlowListElement MGMTServiceFlowLE;

 int flushReseqQueue(reseqFlowListElement * SFListElement);

};

#endif /* __ns_reseqMgr_h__ */

#ifndef ns_globalDefines_h
#define ns_globalDefines_h

#define SUCCESS 0
#define FAILURE 1

#define TRUE 1
#define FALSE 0

#define DATA_PKT           0 /* Packet passed from IFQ(Upper layers) */
#define MGMT_PKT           1 /* Packet generated at DOCSIS layer */
    

#endif /* ns_globalDefines_h__ */


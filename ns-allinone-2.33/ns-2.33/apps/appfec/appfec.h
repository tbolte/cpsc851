/* -*-	Mode:C++; c-basic-offset:8; tab-width:8; indent-tabs-mode:t -*- */
/*
 * Copyright (c) Xerox Corporation 1997. All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * Linking this file statically or dynamically with other modules is making
 * a combined work based on this file.  Thus, the terms and conditions of
 * the GNU General Public License cover the whole combination.
 *
 * In addition, as a special exception, the copyright holders of this file
 * give you permission to combine this file with free software programs or
 * libraries that are released under the GNU LGPL and with code included in
 * the standard release of ns-2 under the Apache 2.0 license or under
 * otherwise-compatible licenses with advertising requirements (or modified
 * versions of such code, with unchanged license).  You may copy and
 * distribute such a system following the terms of the GNU GPL for this
 * file and the licenses of the other code concerned, provided that you
 * include the source code of that other code when and as the GNU GPL
 * requires distribution of source code.
 *
 * Note that people who make modified versions of this file are not
 * obligated to grant this special exception for their modified versions;
 * it is their choice whether to do so.  The GNU General Public License
 * gives permission to release a modified version without this exception;
 * this exception also makes it possible to release a modified version
 * which carries forward this exception. 
 *  
 * @(#) $Header: /cvsroot/nsnam/ns-2/apps/udp.h,v 1.15 2005/08/26 05:05:28 tomh Exp $ (Xerox)
 */

#ifndef ns_appfec_h
#define ns_appfec_h

#include "agent.h"
#include "trafgen.h"
#include "packet.h"

//defines the max block timeout in seconds
#define BLOCK_TIMEOUT 4048
//#define BLOCK_TIMEOUT 250
//#define BLOCK_TIMEOUT 120.0
//#define MAX_BLOCK_LIST 1024
//#define MAX_BLOCK_OUTOFORDER_LIST 1024
//#define MAX_BLOCK_LIST 8192
//#define MAX_BLOCK_OUTOFORDER_LIST 8192
#define MAX_BLOCK_LIST 16384
#define MAX_BLOCK_OUTOFORDER_LIST 16384

enum typeFEC
{
  NO_FEC = 0,
  RS_FEC = 1
};

enum contentType
{
  MPEG4_I_FRAME  = 0x0001,
  MPEG4_B_FRAME  = 0x0002,
  MPEG4_P_FRAME  = 0x0003,
  FEC_OVERHEAD   = 0x0010,
  FEC_OVERHEAD_FIRST  = 0x0011,
  FEC_OVERHEAD_MIDDLE  = 0x0012,
  FEC_OVERHEAD_LAST  = 0x0130
};

enum streamType
{
  MPEG4_VIDEO  = 0x0000
};

enum state
{
  UNINITIALIZED = -1,
  ACTIVE = 1,
  DISABLED = 2
};

enum blockState
{
  INITIALIZED = 0,
  COMPLETED = 1,
  INPROGRESS = 2,
  TIMEDOUT = 3
};

//20+8 for IP/UDP
//RTP: approx as 8 *4 octets (32)
//bytes of overhead consumed per packet
#define APPFEC_PACKET_OVERHEAD 60

class AppFecAgent;

class AppFecBlockTimer : public TimerHandler {
public: 
        AppFecBlockTimer(AppFecAgent *a) : TimerHandler() { a_ = a; }
protected:
        virtual void expire(Event *e);
        AppFecAgent *a_;
};

class AppFecAgent : public Agent {
public:
//JJM
 int FECallocCount;
 int FECfreeCount;
	AppFecAgent();
	virtual void sendmsg(int nbytes, const char *flags = 0)
	{
		sendmsg(nbytes, NULL, flags);
	}
    virtual void init(int streamID, int N, int K);
	virtual void sendmsg(int nbytes, AppData* data, const char *flags = 0);
	virtual void recv(Packet* pkt, Handler*);
	virtual void blockTimerHandler(int);
	virtual int command(int argc, const char*const* argv);
    virtual void dumpFinalStats(char *outputFile);
protected:
	int seqno_;   //not used by sink
    u_int32_t rawSeqNo_;  //seq of raw stream, including FEC packets
    int blockIndex_;
    int blockno_;
    int numberBytesInBlock;
    int numberPacketsInBlock;
    int streamID;
    int N_;
    int K_;
    //If true then all blocks must be N_ large
    // else blocks can vary
    int fixedBlockMode_;  
    int typeFEC;
    int streamType;
    //For creating the MSG trace files
    char msgTraceString[32];
    char *msgptr;    
    //For creating the output trace files
    char traceString[32];
    char *tptr;
    FILE* fp;
    int state;   //-1 uninited, 1 inited
    double blockTimerDelay;
    int numberFECPackets;

    AppFecBlockTimer *myTimer;

    int numberBlockTimeouts;
    int totalPacketsSent;
    int totalPacketsRxed;
    int totalDataPacketsSent;
    int totalDataPacketsRxed;
    double totalBytesSent;
    double totalBytesRxed;
    double totalDataBytesSent;
    double totalDataBytesRxed;
    double totalRedundantDataBytesSent;
    double totalRedundantDataBytesRxed;
    int totalFECDrops;
    int totalFECRestores;

};

#endif

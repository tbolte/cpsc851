/* -*-	Mode:C++; c-basic-offset:8; tab-width:8; indent-tabs-mode:t -*- */
/*
 * Copyright (c) 1991-1997 Regents of the University of California.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *	This product includes software developed by the Computer Systems
 *	Engineering Group at Lawrence Berkeley Laboratory.
 * 4. Neither the name of the University nor of the Laboratory may be used
 *    to endorse or promote products derived from this software without
 *    specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * @(#) $Header: /usr/src/mash/repository/vint/ns-2/tcp-sink.h,v 1.11 1998/08/24 19:39:45 tomh Exp $ (LBL)
 *
 * Revisions:
 *   $A1 : 3/26/05: Added support for monitoring one way delay, jitter, loss.
 *   $A100 : 10/17/2011 - added MBL metric
 */

#ifndef ns_appfecsink_h
#define ns_appfecsink_h

#include <math.h>
#include "agent.h"
#include "trafgen.h"
#include "rtp.h"

#include "appfec.h"
#include "appfecListElement.h"
#include "appfecOrderedListElement.h"
#include "../../mac/docsis/ListObj.h"

//$A100
#define MAX_ARRAY_SIZE 5000000
#define FIXED_SAMPLE_INCREMENT 1.0

#define LOST 0
#define ARRIVED 1
#define NOT_SENT -1

class AppFecSink : public AppFecAgent {

public:
  AppFecSink();
  ~AppFecSink();
  virtual void init(int streamID, int N, int K);
  void recv(Packet* pkt, Handler*);
  virtual void blockTimerHandler(int);
  virtual int command(int argc, const char*const* argv);
  virtual void dumpFinalStats(char *outputFile);
  void processFECBlock();

protected:

    OrderedListObj *blockQ;
    OrderedListObj *OutOfOrderQ;
    int clearQueue(OrderedListObj *ListPtr);
    int clearQueueAndCorrectDrops(OrderedListObj *ListPtr);
    int transferBlock(OrderedListObj *srcList, OrderedListObj *dstList, int currentBlockNumber);
    void passPacketUp(Packet* pkt);

    int maxBlockListSize;
    int maxOutOfOrderListSize;
    int timedOutFlag;
    int blockState;
    int APFEChighestRawSeqNo;  //seq of raw stream, including FEC packets
    int APFEChighestSeqNo;   //block state that maintains the highest UDP/RTP seq number seen
                             // should be the same as that used by the higher layer monitor
    int currentBlockNumber;

    //For creating the MONITOR trace files
    char monitorTraceString[32];
    char *mptr;    //ptr to monitor before FEC trace file
    char rawVideoTraceString[32];
    char *rptr;    //ptr to monitor after FEC trace file
    unsigned int numberInOrderPacketArrivals;
    unsigned int numberOutOfOrderPacketArrivals;
    unsigned int numberBlockQDrops;
    unsigned int numberOutOfOrderQDrops;

    int OutOfOrderQThreshold;

    int UDPSinkTotalBytesDelivered;   
    int  UDPSinkThroughput_bytes_;   /*to monitor good put */
    int  UDPSinkNumberBytesDelivered_; /*to monitor total bytes receivec by app*/
    int UDPSinkNumberPktsOutOfOrder_; /*to monitor number of out of order pkts*/
    int UDPSinkTotalPktsReceived_; /*to monitor loss rate*/
    int UDPSinkNumberPktsDropped_;

	double numberSamples;
	double totalJitterCount;
	double totalLatencyCount;
	double avgJitter;
	double avgLatency;
    int OutOfOrderEvents; /*to monitor number of out of order pkts*/

    double jitter;                 //To monitor jitter
    double lastDelay;              //last one way delay
    int nextSeqNo;                 //to remember the next seqno
    int lossCount;                 //Number of packets dropped
    int totalPackets;              //Number of packets
    int  sink_id_;

    double totalPacketsPerBlock;
    double totalNumberBlocks;
    double totalBlocksUseful;

//$A100  for the MBL metric
    int MBLPacketArrival(Packet *pkt);
    double MBLCompute();
    int lastSentSnum;
    int lastRcvdSnum; //used by server to keep track of the losses
    long message_size;
    double send_rate;
    double packet_interval;
    long max_size_arrival_array;
    double last_arrival_at_server; //this records the time at which the last packet arrived at server
   
    int *packet_arrivals;          // keeps track of packet arrivals
    double *latency;               // keeps track of latency of each packet
    void dumpLossStats();  //called by tcl script to get the results


    int numberReceived;
    int numberLost;
    int numberDups;
    int numberOutOfOrder;

//$A100
    FILE * LOSS_MONITOR_out_file; //outout file to trace  incremental LOSS stats
    char LOSS_MONITOR_FILE_NAME[32];
    char *LMptr;
    double  incrementSize;  //fixed inc size in seconds
    double  lastLMUpdate;  //last sample
    int  highestSNUMInInterval;  //highest snum observed
    int  lowestSNUMInInterval;  //lowest snum observed
    int  numberPktsReceivedInInterval;
    int  numberBytesReceivedInInterval;
    double latestLRSample;
    double latestBWSample;


};

#endif

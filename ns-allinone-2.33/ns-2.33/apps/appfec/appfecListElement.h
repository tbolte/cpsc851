/************************************************************************
* File:  appfecListElement.h
*
* Purpose:
*   Inlude files for the packet List Elements
*
* Revisions:
***********************************************************************/
#ifndef appfecListElement_h
#define appfecListElement_h

#include "packet.h"
#include "rtp.h"

#include "../../mac/docsis/ListElement.h"

class appfecListElement: public  ListElement
{

private:
  Packet *myPkt;

public:
  appfecListElement();
  appfecListElement(Packet *p);
  virtual ~appfecListElement();
  int putPkt(Packet *pkt); 
  Packet * getPacket();
  int sentUpFlag;
  double packetEntryTime;

};

#endif


/************************************************************************
* File:  appfecOrderedListElement.cc
*
* Purpose:
*   Class for the packet List Element object
*
* Revisions:
***********************************************************************/

#include "appfecOrderedListElement.h"


//#define TRACEME 0

/***********************************************************************
*function: appfecOrderedListElement::appfecOrderedListElement
*
*explanation:
*
*inputs:
*
*outputs:
************************************************************************/
appfecOrderedListElement::appfecOrderedListElement() : OrderedListElement()
{
#ifdef TRACEME
  printf("appfecOrderedListElement: constructed packet List Element \n");
#endif
  myPkt = NULL;
  sentUpFlag=FALSE;
  packetEntryTime=0.0;
}


/***********************************************************************
*function: appfecOrderedListElement::appfecOrderedListElement
*
*explanation:
*
*inputs:
*
*outputs:
************************************************************************/
appfecOrderedListElement::appfecOrderedListElement(Packet *p) : OrderedListElement()
{
  myPkt =  p;
#ifdef TRACEME
  printf("appfecOrderedListElement: constructed packet List Element \n");
#endif
}

/***********************************************************************
*function: appfecOrderedListElement::~appfecOrderedListElement
*
*explanation:
*
*inputs:
*
*outputs:
************************************************************************/
appfecOrderedListElement::~appfecOrderedListElement()
{
}

/***********************************************************************
*function: int appfecOrderedListElement::putPkt(Packet *pkt)
*
*explanation:
*
*inputs:
*
*outputs:
************************************************************************/
int appfecOrderedListElement::putPkt(Packet *pkt)
{
int rc = 0;

  myPkt =  pkt;
  return rc;
}



/***********************************************************************
*function: Packet * appfecOrderedListElement::getPacket()
*
*explanation:
*
*inputs:
*
*outputs:
************************************************************************/
Packet * appfecOrderedListElement::getPacket()
{
//Packet *myPkt = NULL;
  return myPkt;
}







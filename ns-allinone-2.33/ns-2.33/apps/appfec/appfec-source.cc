/* -*-	Mode:C++; c-basic-offset:8; tab-width:8; indent-tabs-mode:t -*- */
/*
 * Copyright (C) Xerox Corporation 1997. All rights reserved.
 *  
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * Linking this file statically or dynamically with other modules is making
 * a combined work based on this file.  Thus, the terms and conditions of
 * the GNU General Public License cover the whole combination.
 *
 * In addition, as a special exception, the copyright holders of this file
 * give you permission to combine this file with free software programs or
 * libraries that are released under the GNU LGPL and with code included in
 * the standard release of ns-2 under the Apache 2.0 license or under
 * otherwise-compatible licenses with advertising requirements (or modified
 * versions of such code, with unchanged license).  You may copy and
 * distribute such a system following the terms of the GNU GPL for this
 * file and the licenses of the other code concerned, provided that you
 * include the source code of that other code when and as the GNU GPL
 * requires distribution of source code.
 *
 * Note that people who make modified versions of this file are not
 * obligated to grant this special exception for their modified versions;
 * it is their choice whether to do so.  The GNU General Public License
 * gives permission to release a modified version without this exception;
 * this exception also makes it possible to release a modified version
 * which carries forward this exception.
 */

#ifndef lint
static const char rcsid[] =
    "@(#) $Header: /cvsroot/nsnam/ns-2/apps/udp.cc,v 1.21 2005/08/26 05:05:28 tomh Exp $ (Xerox)";
#endif

#include "udp.h"
#include "rtp.h"
#include "random.h"
#include "address.h"
#include "ip.h"

#include "appfec-source.h"

//If defined tracing is enabled
//#include "../mac/docsis/docsisDebug.h"
//#define TRACEME 1
//#define FILES_OK 1
//#define SPECIAL_TEST 1


class AppFecSourceAgent;


static class AppFecSourceAgentClass : public TclClass {
public:
	AppFecSourceAgentClass() : TclClass("Agent/APFECSOURCE") {}
	TclObject* create(int, const char*const*) {
		return (new AppFecSourceAgent());
	}
} class_appfecsource_agent;

AppFecSourceAgent::AppFecSourceAgent() 
{
	bind("packetSize_", &size_);
#ifdef TRACEME
    printf("AppFecSourceAgent:(%lf): Constructed\n", Scheduler::instance().clock());
#endif
	bind("N_", &N_);
	bind("K_", &K_);
    blockTimerDelay = BLOCK_TIMEOUT;
//    blockTimerDelay = 5.0;
//    blockTimerDelay = 2.0;
//    blockTimerDelay = 0.200;
}

/************************************************************************
*
* Function: void AppFecSourceAgent::sendmsg(int nbytes, AppData* data, const char* flags)
*
* Inputs:
*    int nbytes
*    AppData* data
*    const char* flags
*   
* Explanation:
*  This sends nbytes of application data over the UDP socket.
*
*  pseudo code:
*  set n to number of packets required to send the message
*  for n packets 
*  {
*    if this packet is the FIRST data packet in a block
*      start the blockTimer
*
*    increment the seqno_
*    increment the blockIndex_
*    allocate and setup the packet (set contentType to VIDEO)
*    send the packet
*
*    if (blockIndex_ >= K_)  (if this packet is the LAST data packet in a block)
*      call sendFEC(numberFECPackets) to send the FEC data 
*      stop the blockTimer
*  }
*************************************************************************/
void AppFecSourceAgent::sendmsg(int nbytes, AppData* data, const char* flags)
{

//FOR TEST, stop the generator either after a certain number of packets or 
//    after a certain time
#ifdef SPECIAL_TEST
//  if (totalDataPacketsSent> 10000)
//  if (totalPacketsSent> 500000)
  if (totalDataPacketsSent> 20000)
  {
    idle();
    return;
  }
#endif
 

  if (state != ACTIVE)
    return;

  if (typeFEC == NO_FEC) {
    AppFecAgent::sendmsg(nbytes,data,flags);
  }
  else {

    Packet *p;
    int n =0;
    int rem = 0;
    hdr_cmn* h;
    hdr_ip* iph;
	double local_time = Scheduler::instance().clock();


	n = nbytes / size_;
	rem = nbytes % size_;

#ifdef TRACEME
  printf("AppFecSourceAgent:(%lf):sendmsg, Agent max packet size:%d,  number Packets:%d, last packet size:%d\n", Scheduler::instance().clock(),size_,n,rem);
  printf("AppFecSourceAgent:(%lf):sendmsg, N,K:(%d,%d), current block#:%d \n", Scheduler::instance().clock(),N_,K_,blockno_);
#endif


	if (nbytes == -1) {
		printf("Error:  sendmsg() for UDP should not be -1\n");
		return;
	}	

	// If they are sending data, then it must fit within a single packet.
	if (data && nbytes > size_) {
		printf("Error: data greater than maximum UDP packet size\n");
		return;
	}



#ifdef TRACEME
  printf("AppFecSourceAgent:(%lf)(%d):  Sending this many packets : %d plus a last packet of %d bytes \n", Scheduler::instance().clock(),streamID,n,rem);
#endif


    while ((n > 0) || (rem > 0)) {
      //Sending side of AP FEC.
      //

      //If we are starting an new block AND the time is not active, start it 
      if ((blockIndex_ == 0) && (myTimer->status() != TIMER_IDLE)) {
          printf("AppFecSourceAgent:(%lf)(%d):  WARNING: starting new block but TIMER not TIMER_IDLE \n", Scheduler::instance().clock(),streamID);
      }
      if ((blockIndex_ == 0) && (myTimer->status() == TIMER_IDLE)) {
#ifdef TRACEME
        printf("AppFecSourceAgent:(%lf)(%d): STARTING TIMER (%g)\n", Scheduler::instance().clock(),streamID,blockTimerDelay);
#endif
        myTimer->sched(blockTimerDelay);
      }

      blockIndex_++;
      p = allocpkt();


      h = hdr_cmn::access(p);
      iph = hdr_ip::access(p);
      hdr_rtp* rh = hdr_rtp::access(p);

      if ((n==0) && (rem>0)){
        //if last packet in message
        h->size() = rem;
        rem = 0;
      } 
      else
        h->size() = size_;

      h->size() += APPFEC_PACKET_OVERHEAD;

      numberBytesInBlock = numberBytesInBlock + h->size();
      numberPacketsInBlock++;
 
      rh->flags() = 0;
      //Only do this if NOT FEC
      rh->seqno() = ++seqno_;
      rh->rawSeqNo_ = ++rawSeqNo_;
      rh->blockno_=blockno_;
      rh->blockIndex_=blockIndex_;
      rh->timestamp_ = local_time;
      rh->N_ = N_;
      rh->K_ = K_;
//Send I frames, and occasionally a B frame
      double luck = Random::uniform();
      if (luck > .90)
        rh->contentType = MPEG4_B_FRAME;
      else if (luck > .80)
        rh->contentType = MPEG4_P_FRAME;
      else
        rh->contentType = MPEG4_I_FRAME;

      h->timestamp() = local_time;
#ifdef TRACEME
      printf("AppFecSourceAgent:(%lf):(%d) 1: Send UDP message: size:%d, seqno:%d,  hdr timestamp:%lf\n",
               Scheduler::instance().clock(),streamID,
               h->size(),rh->seqno(),
               h->timestamp());
#endif



      totalPacketsSent++;
      totalBytesSent+=h->size();
      totalDataBytesSent+=(h->size()-APPFEC_PACKET_OVERHEAD);
      totalDataPacketsSent++;

      p->setdata(data);

#ifdef FILES_OK 
      fp = fopen(tptr, "a+");
      fprintf(fp,"%f %d %d %d %d %d %d %d %d %d %d %d %d %f\n", local_time, streamID, rh->contentType, rh->seqno(), rh->rawSeqNo_,rh->rawSeqNo_,h->size(),totalPacketsSent,0,rh->blockno_,rh->blockIndex_,rh->N_,rh->K_, 0.0);
      fclose(fp);
#endif

  //traces UDP stream before fec
#ifdef FILES_OK 
      if (streamID == 12) {
        fp = fopen("APFECSEND12.out", "a+");
        fprintf(fp,"%f %d %d %d %d %d %d %d\n", local_time,  streamID, rh->contentType, rh->seqno(), rh->rawSeqNo_, rh->seqno(),(h->size()-APPFEC_PACKET_OVERHEAD),totalPacketsSent);
        fclose(fp);
      }
#endif

      int dropPackets = 0;
// DROP PACKETS
      if  (dropPackets == 0)
        target_->recv(p);
      else {
 //drop every second in block
//        if ((rh->seqno() % 10) ==2) {
//        if (rh->seqno() ==12) {
//        if ((rh->seqno() ==3) || (rh->seqno() ==7)) {
//        if ((rh->seqno() >2) && (rh->seqno() < 222)) {
//          Packet::free(p);
//        }
//        else
          target_->recv(p);
      }

      n--;


      //Check if we sent all application data in the block
      //But only if N>1 AND K<N
      if ((N_ > 1) || (K_  < N_)) {
        if (blockIndex_ >= K_){
          sendFEC(numberFECPackets);
          blockIndex_=0;
          blockno_++;
          numberBytesInBlock = 0;
          numberPacketsInBlock=0;
//Terminate the timer as we have succeeded to send a block
//          myTimer->resched(blockTimerDelay);
          myTimer->force_cancel();
        }
      }
      else {
        blockIndex_=0;
        blockno_++;
        numberBytesInBlock = 0;
        numberPacketsInBlock=0;
      }
    }
    idle();
  }
}


/************************************************************************
*
* Function: void AppFecSourceAgent::sendFEC(numberFECPackets)
*
* Inputs:
*    int numberFECPackets:  specifies how many packets of FEC data are to be sent.
*           The actual size of the FEC packets depends on how much data was sent.
*   
* Explanation:
*  This sends completes the FEC process for the current block
*  If do not have a full block
*    if fixedBlockMode_ is FALSE
*      scale N_ and K_ (numberFECPackets might be reduced)
*    else
*      keep K_ and N_ to the configured values
*  n = updatedN_- updatedK_;
*  find avg size of data pkt sent in block (nBytesPerPacket = numberBytesInBlock /  K_)
*  For n packets
*  {
*    increment the blockIndex (but not seqno)
*    create a packet (size : nBytesPerPacket)
*    label each packet's rh->contentType = FEC_OVERHEAD
*    send the packet
*  }
*************************************************************************/
void AppFecSourceAgent::sendFEC(int numberFECPackets)
{
Packet *p;
int nPackets =0;
int nBytesRedundantData = 0;
int nBytesPerPacket = 0;
hdr_cmn* h;
hdr_ip* iph;
int n = numberFECPackets;
int updatedN_ = N_;
int updatedK_ = K_;
double local_time = Scheduler::instance().clock();


#ifdef TRACEME
  printf("AppFecSourceAgent:sendFEC(%lf):(%d) send this many redundant packets: %d, we sent this many packets in the block so far:%d \n", 
     Scheduler::instance().clock(), streamID,numberFECPackets,numberPacketsInBlock);
#endif

   //if we don't have a full size block
  if (numberPacketsInBlock < K_) {
    if (fixedBlockMode_ == TRUE) {
      updatedN_ = N_;
      updatedK_ = K_;
      n = N_- numberPacketsInBlock;
#ifdef TRACEME
      printf("AppFecSourceAgent:sendFEC(%lf):(%d) Fixed BLOCKS:  NOT A FULL SIZE BLOCK blockIndex_%d, updatedN_:%d, updatedK_:%d, n:%d\n", 
       Scheduler::instance().clock(), streamID,blockIndex_,updatedN_,updatedK_,n);
#endif
    }
    else {
      double scale = (double)numberPacketsInBlock/(double)K_;
      updatedN_= (int) (  ceil(scale * (double)N_));
      updatedK_= (int) (  ceil(scale * (double)K_));
      n = updatedN_- updatedK_;
#ifdef TRACEME
      printf("AppFecSourceAgent:sendFEC(%lf):(%d) Variable BLOCKS:  NOT A FULL SIZE BLOCK scale:%2.6f, blockIndex_%d, updatedN_:%d, updatedK_:%d, n:%d\n", 
       Scheduler::instance().clock(), streamID,scale,blockIndex_,updatedN_,updatedK_,n);
#endif
    }
  }

  //At this point, we are to use updatedN_, updatedK, and n as the number of redundant pkts to send
  nBytesPerPacket = numberBytesInBlock /  K_;
  nBytesRedundantData =  n * numberBytesInBlock;
  if (nBytesPerPacket > size_) {
//    printf("AppFecSourceAgent:sendFEC(%lf):(%d) WARNING ?? nBytesPerPacket:%d exceeds size_:%d (ratio:%3.3f,numberBytesInBlock:%d,nBytesRedundantData:%d)\n", 
//     Scheduler::instance().clock(), streamID,nBytesPerPacket,size_, ((double)(N_-K_)/(double)N_),numberBytesInBlock,nBytesRedundantData);
    nBytesPerPacket = size_;
  }

  while (n > 0)
  {
    p = allocpkt();
    h = hdr_cmn::access(p);
    iph = hdr_ip::access(p);
    hdr_rtp* rh = hdr_rtp::access(p);

    h->size() = nBytesPerPacket + APPFEC_PACKET_OVERHEAD;
    rh->flags() = 0;
    //Do not increment the seqno for FEC data 
    rh->seqno() = seqno_;
    rh->rawSeqNo_ = ++rawSeqNo_;
    rh->blockno_=blockno_;

    //we do need to increment the index
    blockIndex_++;
    rh->blockIndex_=blockIndex_;
    rh->timestamp_ = local_time;
    rh->N_ = N_;
    rh->K_ = K_;

//    if (n == 1)
//      rh->contentType = FEC_OVERHEAD_LAST;
//    else if (n == numberFECPackets)
//      rh->contentType = FEC_OVERHEAD_FIRST;
//    else 
//      rh->contentType = FEC_OVERHEAD_MIDDLE;
    //For now, let's just call it FEC_OVERHEAD
    rh->contentType = FEC_OVERHEAD;

    h->timestamp() = local_time;
        
#ifdef TRACEME
    printf("AppFecSourceAgent:(%lf):SendFec:  Send FEC_OVERHEAD message: size:%d, seqno:%d,  hdr timestamp:%lf\n",
               Scheduler::instance().clock(),
               h->size(),rh->seqno(),
               h->timestamp());
#endif


    totalPacketsSent++;
    totalBytesSent+=h->size();
    totalRedundantDataBytesSent+=h->size();

#ifdef FILES_OK 
    fp = fopen(tptr, "a+");
      fprintf(fp,"%f %d %d %d %d %d %d %d %d %d %d %d %d %f \n", local_time, streamID, rh->contentType, rh->seqno(), rh->rawSeqNo_,rh->seqno(),h->size(),totalPacketsSent,0,rh->blockno_,rh->blockIndex_,rh->N_,rh->K_, 0.0);
    fclose(fp);
#endif

//DROP
//        if (rh->rawSeqNo_ ==19) {
//        if ((rh->seqno() ==3) || (rh->seqno() ==7)) {
//        if ((rh->seqno() >3) && (rh->seqno() < 7)) {
//          Packet::free(p);
//        }
//        else
          target_->recv(p);

    n--;
  }
}

int AppFecSourceAgent::command(int argc, const char*const* argv)
{
#ifdef TRACEME
  printf("AppFecSourceAgent:(%lf): command:  #Params:%d,   argv command:%s \n", Scheduler::instance().clock(),argc,argv[1]);
#endif
	if (argc == 3) {
		if (strcmp(argv[1], "dumpstats") == 0) {
          dumpFinalStats((char *)argv[2]);
          return (TCL_OK);
		}
	} else if (argc == 4) {
		if (strcmp(argv[1], "send") == 0) {
			PacketData* data = new PacketData(1 + strlen(argv[3]));
			strcpy((char*)data->data(), argv[3]);
			sendmsg(atoi(argv[2]), data);
			return (TCL_OK);
		}
	} else if (argc == 5) {
		if (strcmp(argv[1], "sendmsg") == 0) {
			PacketData* data = new PacketData(1 + strlen(argv[3]));
			strcpy((char*)data->data(), argv[3]);
			sendmsg(atoi(argv[2]), data, argv[4]);
			return (TCL_OK);
		}
        if (strcmp(argv[1], "initstream") == 0) {
            init(atoi(argv[2]), atoi(argv[3]), atoi(argv[4]));
            return (TCL_OK);
        }
	}
	return (AppFecAgent::command(argc, argv));
}


/************************************************************************
*
* Function: void AppFecSourceAgent::blockTimerHandler(int streamIDParam)
*
* Inputs:
*    int streamIDParam:  currently not used
*   
* Explanation:
*  This is invoked when the block timeout pops.  We need to send the correct
*   amount of FEC at this time.
*
*********************************************************************/
void AppFecSourceAgent::blockTimerHandler(int streamIDParam)
{

  numberBlockTimeouts++;
#ifdef TRACEME
  printf("AppFecSourceAgent:blockTimerHandler(%lf):(%d) POPPED (total:%d), send this number of FEC packets %d\n", 
      Scheduler::instance().clock(), streamID,numberBlockTimeouts,numberFECPackets);
#endif
  sendFEC(numberFECPackets);
  blockIndex_=0;
  blockno_++;
  numberBytesInBlock = 0;

}

/************************************************************************
*
* Function: void AppFecSourceAgent::dumpFinalStats(char *outputFile)
*
* Inputs:
*    char *outputFile
*   
* Explanation:
*  This is invoked via the tcl command dumpstats.  We dump a single line of stats to
*  the file defined as follows:
*     fprintf(fp,"%lf\t%d\t%d\t%9.0f\t%9.0f\t%2.2f \n",
*         curr_time,streamID,numberBlockTimeouts, BW, APPThroughput, Efficiency);
*            BW:  total number of bytes sent (including all overhead, AND FEC) 
*                       divided by current time.  So assumes the start of monitor
*                       time is 0.0 seconds.
*            APPThroughput : This is bandwidth consumed by just the data, and not FEC
*                     It does not include the UDP/IP or RTP overhead
*                
*            Efficiency : the ratio of data (not FEC) to total data sent
*                 Efficiency = totalRedundantDataBytesSent / totalBytesSent;
*
*********************************************************************/
void AppFecSourceAgent::dumpFinalStats(char *outputFile)
{
double curr_time = Scheduler::instance().clock();

#ifdef TRACEME
  printf("AppFecSourceAgent:dumpFinalStats(%f):(%d) dump to file  %s\n", curr_time, streamID,outputFile);
#endif

  fp = fopen(outputFile, "a+");

  double BW=0;
  double APPThroughput=0;
  double Efficiency = 0;

  if (totalBytesSent > 0) {
    BW= totalBytesSent * 8 / curr_time;
    Efficiency = totalDataBytesSent / totalBytesSent;
//    Efficiency = totalRedundantDataBytesSent / totalBytesSent;
  }
  if ( totalDataBytesSent > 0) {
    APPThroughput = totalDataBytesSent * 8 / curr_time;
  }
  fprintf(fp,"%lf\t%d\t%d\t%9.0f\t%9.0f\t%2.2f\t%d\t%d \n",
    curr_time,streamID,numberBlockTimeouts, BW, APPThroughput, Efficiency,totalPacketsSent,totalDataPacketsSent);

  fclose(fp);
}


void AppFecSourceAgent::init(int streamIDParam, int NParam, int KParam)
{

  AppFecAgent::init(streamIDParam,NParam,KParam);
  sprintf(tptr,"VideoTrace%dSent.out",streamID);
#ifdef TRACEME
  double curr_time = Scheduler::instance().clock();
  printf("AppFecSourceAgent:init(%f):(%d) N:%d, K:%d, Video trace file %s\n", curr_time, streamID,NParam,KParam,tptr);
#endif
}



/************************************************************************
* File:  appfecListElement.cc
*
* Purpose:
*   Class for the packet List Element object
*
* Revisions:
***********************************************************************/

#include "appfecListElement.h"


//#define TRACEME 0

/***********************************************************************
*function: appfecListElement::appfecListElement
*
*explanation:
*
*inputs:
*
*outputs:
************************************************************************/
appfecListElement::appfecListElement() : ListElement()
{
#ifdef TRACEME
  printf("appfecListElement: constructed packet List Element \n");
#endif
  myPkt = NULL;
  sentUpFlag=FALSE;
  packetEntryTime=0.0;
}


/***********************************************************************
*function: appfecListElement::appfecListElement
*
*explanation:
*
*inputs:
*
*outputs:
************************************************************************/
appfecListElement::appfecListElement(Packet *p) : ListElement()
{
  myPkt =  p;
#ifdef TRACEME
  printf("appfecListElement: constructed packet List Element \n");
#endif
}

/***********************************************************************
*function: appfecListElement::~appfecListElement
*
*explanation:
*
*inputs:
*
*outputs:
************************************************************************/
appfecListElement::~appfecListElement()
{
}

/***********************************************************************
*function: int appfecListElement::putPkt(Packet *pkt)
*
*explanation:
*
*inputs:
*
*outputs:
************************************************************************/
int appfecListElement::putPkt(Packet *pkt)
{
int rc = 0;

  myPkt =  pkt;
  return rc;
}



/***********************************************************************
*function: Packet * appfecListElement::getPacket()
*
*explanation:
*
*inputs:
*
*outputs:
************************************************************************/
Packet * appfecListElement::getPacket()
{
//Packet *myPkt = NULL;
  return myPkt;
}







/* -*-	Mode:C++; c-basic-offset:8; tab-width:8; indent-tabs-mode:t -*- */
/*
 * Copyright (c) 1991-1997 Regents of the University of California.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *	This product includes software developed by the Computer Systems
 *	Engineering Group at Lawrence Berkeley Laboratory.
 * 4. Neither the name of the University nor of the Laboratory may be used
 *    to endorse or promote products derived from this software without
 *    specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 *  Rev 
 *   $900 : 6-7-2011. Hit a trap - block timeout occurs, we try to restart timer but sched won't let us
 *   $901:  7-5-2011. Memory leak fixed    
 *   $A902: 7-9-2011: Fixed a logic bug in clearQueueAndCorrect  
 *   $A903: 7-25-2011: Really, fixed the memory leak
 */

#ifndef lint
static const char rcsid[] =
    "@(#) $Header: /usr/src/mash/repository/vint/ns-2/udp-sink.cc,v 1.28 1998/08/24 19:39:45 tomh Exp $ (LBL)";
#endif

#include "flags.h"
#include "ip.h"
#include "appfec-sink.h"
#include "../../mac/docsis/globalDefines.h"


//To trace this module 
//#define TRACEME 1
//#define FILES_OK 1

//Define this and we generate a single trace for the designated stream id
#define  MONITOR_FILES_OK  1
//I used the following to debug a bug where APFEC was not correctly passing up all source data
#define  MONITOR_RAW_OK  1
#define  MONITORED_STREAM 1

#include "../mac/docsis/docsisDebug.h"


#define TRACEID 1

//class Application;
class AppFecSink;

static class AppFecSinkClass : public TclClass {
public:
	AppFecSinkClass() : TclClass("Agent/APFECSINK") {}
	TclObject* create(int, const char*const*) {
		return (new AppFecSink());
	}
} class_appfecsink;

AppFecSink::AppFecSink() 
{
  UDPSinkThroughput_bytes_ = 0;
  bind("UDPSinkThroughput",&UDPSinkThroughput_bytes_);
  UDPSinkNumberBytesDelivered_ = 0;
  bind("UDPSinkNumberBytesDelivered",&UDPSinkNumberBytesDelivered_);
  sink_id_ = 0;
  bind("SinkId_",&sink_id_);
  UDPSinkTotalPktsReceived_ = 0;
  bind("UDPSinkTotalPktsReceived",&UDPSinkTotalPktsReceived_);
  UDPSinkNumberPktsOutOfOrder_ = 0;
  bind("UDPSinkPktsOutOfOrder",&UDPSinkNumberPktsOutOfOrder_);
  UDPSinkNumberPktsDropped_ = 0;
  bind("UDPSinkNumberPktsDropped",&UDPSinkNumberPktsDropped_);


  UDPSinkTotalBytesDelivered = 0;

  numberSamples = 0;
  totalJitterCount = 0;
  totalLatencyCount = 0;
  avgJitter = 0;
  avgLatency = 0;
  OutOfOrderEvents = 0;

  bind("UDPAvgJitter",&avgJitter);
  bind("UDPAvgLatency",&avgLatency);

  jitter = 0;
  lastDelay = 0;
  nextSeqNo = 0;
  lossCount = 0;
  totalPackets =0;

  totalPacketsPerBlock = 0;
  totalNumberBlocks = 0;
  totalBlocksUseful = 0;

  blockTimerDelay = BLOCK_TIMEOUT;
//  blockTimerDelay = 5.00;
//  blockTimerDelay = 2.00;
  OutOfOrderQThreshold = 16; //once this many out of order packets arrive, process current block and transfer
//  OutOfOrderQThreshold = 2; 

}


/************************************************************************
*
* Function: void AppFecSink::recv(Packet* p, Handler*)
*
* Inputs:
*    Packete *p : the packet that has arrived- this routine now owns the packet
*    Handler : the handler which is not used
*   
* Explanation:
*
*  This receives a packet, the method represents the sink of the UDP flow
*  Ownership of the packet object transfers to on this call.
*
*  Arriving data is monitored in two ways:
*    Raw: totalyBytesSent/Rxed .... see dumpFinalStats
*    After FEC:  UDPSinkNumberBytesDelivered... see tcl dumpFinalUDPStats
*         (PassPacketUp maintains the after FEC)
*
* pseudo code (given packet pkt):
*  if configured for no fec 
*    pass pkt up
*  else if pkt associated with prior blocks
*    throw out packet as it's a dup or not needed
*  else if pkt is in the current block
*    if pkt is a duplicate, just throw it away 
*       return;
*    if start of new block
*      start block timer
*      state = in progress
*      insert in blockQ
*    if pkt is data, clone packet and passPacketUp(pkt)
*    if current block is complete (if at least K_ arrived) 
*      process FEC block
*  else if pkt is NOT associated with current block
*    if pkt is a duplicate, just throw it away 
*       return
*    insert in OutOfOrderQ
*    if size of OutOfOrderQ exceeds threshold
*       process FEC block   //handles transferring Queues
*  
*************************************************************************/
void AppFecSink::recv(Packet* p, Handler* myHandler)
{
  int rc = SUCCESS;
  Packet *clonePkt = NULL;
  int blockN_ = 1;
  int blockK_ = 1;
  //will be set to indicate if one or more packets of application data are dropped
  int droppedPacketFlag = FALSE;
  double delay = 0;
  double now = Scheduler::instance().clock();


  if (p != NULL)
  {

  hdr_cmn *h = hdr_cmn::access(p);
  hdr_rtp* rh = hdr_rtp::access(p);
  hdr_ip* iph = hdr_ip::access(p);
  int fid_ = iph->flowid();

  delay = now - h->timestamp();

  totalPacketsRxed++;
  totalBytesRxed +=  h->size();

  if (rh->rawSeqNo_ > APFEChighestRawSeqNo) {
    APFEChighestRawSeqNo = rh->rawSeqNo_;
  }

#ifdef MONITOR_FILES_OK 
  if (sink_id_ == MONITORED_STREAM) {
     fp = fopen(mptr, "a+");
     fprintf(fp,"%f %d %d %d %d %d %d %d %d %d %d %d %d %d %d %f\n", now, streamID, rh->contentType, rh->seqno(),rh->rawSeqNo_,APFEChighestRawSeqNo,h->size(),totalPacketsRxed, lossCount, rh->blockno_,rh->blockIndex_,rh->N_,rh->K_,numberPacketsInBlock,numberBytesInBlock,delay);
     fclose(fp);
  }
#endif

#ifdef MONITOR_RAW_OK 
  if (rh->contentType != 16) {
    FILE* fp2 = fopen("Raw2.txt", "a+");
     fprintf(fp2,"%f %d \n", now, rh->seqno());
    fclose(fp2);
  }
#endif

// uncomment to see all arrivals
//     printf("APFECSINK:%f %d %d %d %d %d %d %d %d %d %d %d %d %d %d %f\n", now, streamID, rh->contentType, rh->seqno(),rh->rawSeqNo_,APFEChighestRawSeqNo,h->size(),totalPacketsRxed, lossCount, rh->blockno_,rh->blockIndex_,rh->N_,rh->K_,numberPacketsInBlock,numberBytesInBlock,delay);



  if (typeFEC == NO_FEC) {
    totalDataBytesRxed +=  (h->size()- APPFEC_PACKET_OVERHEAD);
    totalDataPacketsRxed++;
#ifdef FILES_OK 
     fp = fopen(tptr, "a+");
     fprintf(fp,"%f %d %d %d %d %d %d %d %d %d %d %d %d %d %d %f\n", now, streamID, rh->contentType, rh->seqno(),rh->rawSeqNo_,APFEChighestRawSeqNo,h->size(),totalPacketsRxed, lossCount, rh->blockno_,rh->blockIndex_,rh->N_,rh->K_,numberPacketsInBlock,numberBytesInBlock,delay);
     fclose(fp);
#endif
    passPacketUp(p);
  }
  else {

#ifdef FILES_OK 
     fp = fopen(tptr, "a+");
     fprintf(fp,"%f %d %d %d %d %d %d %d %d %d %d %d %d %d %d %f\n", now, streamID, rh->contentType, rh->seqno(),rh->rawSeqNo_,APFEChighestRawSeqNo,h->size(),totalPacketsRxed, lossCount, rh->blockno_,rh->blockIndex_,rh->N_,rh->K_,numberPacketsInBlock,numberBytesInBlock, delay);
     fclose(fp);
#endif
 
    

#ifdef TRACEME 
    printf("APFECSINK:recv:%d:sinkID:%d;(%lf): packet arrived:%d (rawSeqNo_:%d) (block#:%d,blockInde:%d), nextExpected:%d, currentBlock#:%d, size blockQ:%d, size OutOfOrderQ:%d \n",
           fid_,sink_id_,now,rh->seqno(),rh->rawSeqNo_,rh->blockno_,rh->blockIndex_,APFEChighestSeqNo,currentBlockNumber,blockQ->getListSize(),OutOfOrderQ->getListSize());
#endif
    if ( rh->contentType != FEC_OVERHEAD)
    {
      totalDataBytesRxed +=  (h->size()- APPFEC_PACKET_OVERHEAD);
      totalDataPacketsRxed++;
      //this flag indicates we have dropped a packet....so don't send this pkt up
      if (rh->seqno() !=  (APFEChighestSeqNo + 1)) {
        droppedPacketFlag = TRUE;
      }
    }
    else
      totalRedundantDataBytesRxed +=  h->size();

      if (timedOutFlag == 1) {
         currentBlockNumber = rh->blockno_;
         timedOutFlag = 0;
#ifdef TRACEME 
         printf("APFECSINK:recv:%d:sinkID:%d;(%lf) out of ORDER and detected timeout  set currentBlockNumber:%d \n",
           fid_,sink_id_,now, currentBlockNumber);
#endif
      }

    if (rh->blockno_ ==  currentBlockNumber) {
      //p is in current block 

      //if start of block
      if (blockState != INPROGRESS) {
         totalNumberBlocks++;
         blockState = INPROGRESS;
         numberPacketsInBlock=0;
         numberBytesInBlock=0;
         blockIndex_ = 0;

 //* $900
//Terminate the timer as we have succeeded to send a block
//          myTimer->resched(blockTimerDelay);
         myTimer->force_cancel();
         myTimer->sched(blockTimerDelay);
      }

      numberPacketsInBlock++;
      numberBytesInBlock +=  h->size();
      numberInOrderPacketArrivals++;
      blockN_ = rh->N_;
      blockK_ = rh->K_;

#ifdef TRACEME 
    printf("APFECSINK:recv:%d:sinkID:%d;(%lf)  IN ORDER (%d, raw:%d,nextExpected:%d), blockState:%d \n",
           fid_,sink_id_,now,rh->seqno(),rh->rawSeqNo_,(APFEChighestSeqNo+1),blockState);
#endif


      //enque the pkt and send it up but only if it's data
      appfecOrderedListElement *myListElement = new appfecOrderedListElement();
//      myListElement->key= rh->blockIndex_;
//      myListElement->key= rh->seqno();
      myListElement->key= rh->rawSeqNo_;
      //clone the packet
      if ( rh->contentType != FEC_OVERHEAD)
      {
        clonePkt = p->copy();

        //Queue to blockQ
        rc = myListElement->putPkt(clonePkt);
        if (rc == SUCCESS) {
          rc = blockQ->addElement(*(OrderedListElement *)myListElement);
          if (rc == SUCCESS) {
            myListElement->packetEntryTime = now;

            //To send a completed packet (after FEC) up the stack
            if (droppedPacketFlag  == FALSE) {
              myListElement->sentUpFlag = TRUE;
              passPacketUp(p);
            } 
            else {
              //else drop p  $A901
              Packet::free(p);
#ifdef TRACEME 
              printf("APFECSINK:recv:%d:sinkID:%d;(%lf) DROP 1 (seqno:%d, rawSeqNo:%d,nextExpected:%d), blockState:%d, lossCount:%d, numberBlockQDrops:%d \n",
                    fid_,sink_id_,now,rh->seqno(),rh->rawSeqNo_,(APFEChighestSeqNo+1),blockState,lossCount,numberBlockQDrops);
#endif
            }
          } 
          else { 
            numberBlockQDrops++;
            //$A901
            Packet::free(clonePkt);
            Packet::free(p);
            delete(myListElement);
#ifdef TRACEME 
            printf("APFECSINK:recv:%d:sinkID:%d;(%lf) DROP 2 (seqno:%d, rawSeqNo:%d,nextExpected:%d), blockState:%d \n",
                    fid_,sink_id_,now,rh->seqno(),rh->rawSeqNo_,(APFEChighestSeqNo+1),blockState);
#endif

#ifdef TRACEME 
            printf("APFECSINK:recv:%d:sinkID:%d;(%lf)  ERROR : Failed on blockQ->addElement(), blockQsize:%d, numberBlockQDrops:%d  (seqno:%d, rawSeqNo:%d,nextExpected:%d), blockState:%d \n",
             fid_,sink_id_,now,blockQ->getListSize(),numberBlockQDrops,rh->seqno(),rh->rawSeqNo_,(APFEChighestSeqNo+1),blockState);
#endif
          }
        } 
        else {
#ifdef TRACEME 
          printf("APFECSINK:recv:%d:sinkID:%d;(%lf)  ERROR : Failed to putPkt  (%d, raw:%d,nextExpected:%d), blockState:%d \n",
             fid_,sink_id_,now,rh->seqno(),rh->rawSeqNo_,(APFEChighestSeqNo+1),blockState);
#endif
          Packet::free(clonePkt);
          //$A901
         Packet::free(p);
#ifdef TRACEME 
           printf("APFECSINK:recv:%d:sinkID:%d;(%lf) DROP 3 (seqno:%d, rawSeqNo:%d,nextExpected:%d), blockState:%d \n",
                    fid_,sink_id_,now,rh->seqno(),rh->rawSeqNo_,(APFEChighestSeqNo+1),blockState);
#endif
          delete(myListElement);
        }
      } 
      else {
        //Queue to blockQ
        rc = myListElement->putPkt(p);
        if (rc == SUCCESS) {
          rc = blockQ->addElement(*(OrderedListElement *)myListElement);
          if (rc == SUCCESS) {
            myListElement->packetEntryTime = now;
            myListElement->sentUpFlag = FALSE;
          } 
          else {
            numberBlockQDrops++;
            Packet::free(p);
            delete(myListElement);
#ifdef TRACEME 
            printf("APFECSINK:recv:%d:sinkID:%d;(%lf) DROP 4 (seqno:%d, rawSeqNo:%d,nextExpected:%d), blockState:%d \n",
                    fid_,sink_id_,now,rh->seqno(),rh->rawSeqNo_,(APFEChighestSeqNo+1),blockState);
#endif
          } 
        } else {
        //$A901
            numberBlockQDrops++;
            Packet::free(p);
            delete(myListElement);
#ifdef TRACEME 
            printf("APFECSINK:recv:%d:sinkID:%d;(%lf) DROP 5 (seqno:%d, rawSeqNo:%d,nextExpected:%d), blockState:%d \n",
                    fid_,sink_id_,now,rh->seqno(),rh->rawSeqNo_,(APFEChighestSeqNo+1),blockState);
#endif
        }
      } 

      //If completed a block, call processFECBlock
      // (regardless of errors)
      if (blockQ->getListSize() >= blockK_) {
        processFECBlock();
      }
    }
    else if (rh->blockno_ >  currentBlockNumber) {

    //p is NOT associated with current block 
      numberOutOfOrderPacketArrivals++;
#ifdef TRACEME 
    printf("APFECSINK:%d:sinkID:%d;(%lf)  packet arrived out of order: %d (rawSeqNo_:%d), lastDelay:%lf, timestamp:%lf,  nextSeqNo:%d, APFEChighest seq no:%d, #dropped:%d, #SinkOutOfOrder:%d \n", 
           fid_,sink_id_,now,rh->seqno(),rh->rawSeqNo_,lastDelay, h->timestamp(),nextSeqNo, APFEChighestSeqNo,
           UDPSinkNumberPktsDropped_, UDPSinkNumberPktsOutOfOrder_);
    printf("APFECSINK:%d:sinkID:%d;(%lf)  (APFEChighestSeqNo+1):%d, currentBlockNumber:%d, size of blockQ:%d, size of OutOfOrderQ:%d \n",
           fid_,sink_id_,now,(APFEChighestSeqNo+1),currentBlockNumber,blockQ->getListSize(),OutOfOrderQ->getListSize());
#endif

      appfecOrderedListElement *myListElement = new appfecOrderedListElement();
//      myListElement->key= rh->seqno();
//      myListElement->key= rh->blockIndex_;
      myListElement->key= rh->rawSeqNo_;
      if ( rh->contentType != FEC_OVERHEAD)
      {
        //$A903 - we incorrectly cloned it...no need to do this
        //clone the packet
        //clonePkt = p->copy();
        //rc = myListElement->putPkt(clonePkt);

        //Queue to blockQ
        rc = myListElement->putPkt(p);
#ifdef TRACEME 
        printf("APFECSINK:%d:sinkID:%d;(%lf)  rc from putPkt: %d,  key : %d\n ", 
             fid_,sink_id_,now,rc, myListElement->key);
#endif
        if (rc == SUCCESS) {
          rc = OutOfOrderQ->addElement(*(OrderedListElement *)myListElement);
          if (rc == SUCCESS) {
            myListElement->packetEntryTime = now;
#ifdef TRACEME 
          printf("APFECSINK:recv(%lf): fid:%d sinkID:%d  Succeeded 1 to add this packet (seqnumber:%d, rawSeqNum:%d) to the queue (size:%d)\n", 
             now,fid_,sink_id_, rh->seqno(),rh->rawSeqNo_, OutOfOrderQ->getListSize());
#endif
          }
          else {
            numberOutOfOrderQDrops++;
            //Packet::free(clonePkt);
           //$A901
            Packet::free(p);
            delete(myListElement);
#ifdef TRACEME 
            printf("APFECSINK:recv:%d:sinkID:%d;(%lf) DROP 6 (seqno:%d, rawSeqNo:%d,nextExpected:%d), blockState:%d \n",
                    fid_,sink_id_,now,rh->seqno(),rh->rawSeqNo_,(APFEChighestSeqNo+1),blockState);
#endif
          }
        } else {
            numberOutOfOrderQDrops++;
            //Packet::free(clonePkt);
           //$A901
            Packet::free(p);
            delete(myListElement);
#ifdef TRACEME 
            printf("APFECSINK:recv:%d:sinkID:%d;(%lf) DROP 7 (seqno:%d, rawSeqNo:%d,nextExpected:%d), blockState:%d \n",
                    fid_,sink_id_,now,rh->seqno(),rh->rawSeqNo_,(APFEChighestSeqNo+1),blockState);
#endif
        }
      } else {
        //Queue to OutOfOrderQ 
        rc = myListElement->putPkt(p);
#ifdef TRACEME 
        printf("APFECSINK:%d:sinkID:%d;(%lf)  rc from putPkt: %d,  key : %d\n ", 
             fid_,sink_id_,now,rc, myListElement->key);
#endif
        if (rc == SUCCESS) {
          rc = OutOfOrderQ->addElement(*(OrderedListElement *)myListElement);
          if (rc == SUCCESS) {
            myListElement->packetEntryTime = now;
#ifdef TRACEME 
          printf("APFECSINK:recv(%lf): fid:%d sinkID:%d  Succeeded 2 to add this packet (seqnumber:%d, rawSeqNum:%d) to the queue (size:%d)\n", 
             now,fid_,sink_id_, rh->seqno(),rh->rawSeqNo_, OutOfOrderQ->getListSize());
#endif
          }
          else {
            numberOutOfOrderQDrops++;
           //$A901
            Packet::free(clonePkt);
            delete(myListElement);
#ifdef TRACEME 
            printf("APFECSINK:recv:%d:sinkID:%d;(%lf) DROP 8 (seqno:%d, rawSeqNo:%d,nextExpected:%d), blockState:%d \n",
                    fid_,sink_id_,now,rh->seqno(),rh->rawSeqNo_,(APFEChighestSeqNo+1),blockState);
#endif
          }
        } else {
          numberOutOfOrderQDrops++;
         //$A901
          Packet::free(clonePkt);
          delete(myListElement);
#ifdef TRACEME 
          printf("APFECSINK:recv:%d:sinkID:%d;(%lf) DROP 9 (seqno:%d, rawSeqNo:%d,nextExpected:%d), blockState:%d \n",
                    fid_,sink_id_,now,rh->seqno(),rh->rawSeqNo_,(APFEChighestSeqNo+1),blockState);
#endif
        }
      }

      //If too many packets in OutOfOrderQ,  processFECBlock
      if (OutOfOrderQ->getListSize() > OutOfOrderQThreshold) {
#ifdef TRACEME 
        printf("APFECSINK:%d:sinkID:%d;(%lf)  Out of Order Q (%d)  exceeds threshold (%d) \n",
           fid_,sink_id_,now,OutOfOrderQ->getListSize(),OutOfOrderQThreshold);
#endif

        //This call will result in packets from the out of order q to be transferred to the blockQ
        processFECBlock();
      }
    }
    // else pkt is from older block...and this is fine.  We just ignore as we have moved onto the next block
    else {
#ifdef TRACEME 
    printf("APFECSINK:%d:sinkID:%d;(%lf)  OLD packet arrived:  (seqNo:%d, rawSeqNo_:%d,blockno_:%d)  APFEChighestSeqNo:%d, currentBlockNumber:%d, size of blockQ:%d, size of OutOfOrderQ:%d \n",
           fid_,sink_id_,now, rh->seqno_, rh->rawSeqNo_, rh->blockno_, APFEChighestSeqNo,currentBlockNumber,blockQ->getListSize(),OutOfOrderQ->getListSize());
#endif
      numberOutOfOrderPacketArrivals++;
      Packet::free(p);

    }
#ifdef TRACEME 
    printf("\nAPFECSINK:%d:sinkID:%d;(%lf) On return:  (APFEChighestSeqNo+1):%d, currentBlockNumber:%d, size of blockQ:%d, size of OutOfOrderQ:%d \n",
           fid_,sink_id_,now,(APFEChighestSeqNo+1),currentBlockNumber,blockQ->getListSize(),OutOfOrderQ->getListSize());
#endif
  }
  }
  else {
    printf("APFECSINK:recv:%d:sinkID:%d;(%lf)  ERROR : BAD PACKET PTR  \n", fid_,sink_id_,now);
  } 
}

/************************************************************************
*
* Function: void AppFecSink::passPacketUp(Packet* p)
*
* Inputs:
*    Packete *p : the packet that has arrived
*   
* Explanation:
*  This method is passed packets that have completed
*  FEC and are ready to be passed up.
*
*  Note: ownership of the object transfers with the packet.  The caller
*  must NOT delete the packet.
*
*  Note: pkt size contains all overhead on entry.  We estimate APPFEC over
*   and take it into account in this routine
*
*************************************************************************/
void AppFecSink::passPacketUp(Packet* p)
{
  double now = Scheduler::instance().clock();
  double delay = 0;

  hdr_cmn* h = hdr_cmn::access(p);
  hdr_rtp* rh = hdr_rtp::access(p);
  hdr_ip* iph = hdr_ip::access(p);
  int fid_ = iph->flowid();

  if (p == NULL)
    return;

  if ((h->size()- APPFEC_PACKET_OVERHEAD) < 0) {
    printf("AppFecSink:passPacketUp:%d:sinkID:%d;(%lf) HARD ERROR  MALFORMED packet arrived, seqNo:%d,  size:%d \n", 
      fid_,sink_id_,now,rh->seqno(),(h->size()- APPFEC_PACKET_OVERHEAD));
    exit(1);
  }

  totalPackets++;

  UDPSinkTotalBytesDelivered  += h->size();
  UDPSinkThroughput_bytes_ += h->size();
  UDPSinkNumberBytesDelivered_ += (h->size()- APPFEC_PACKET_OVERHEAD);
  UDPSinkTotalPktsReceived_++;

  if (rh->seqno() > APFEChighestSeqNo) {
    APFEChighestSeqNo = rh->seqno();
  }


#ifdef TRACEME 
  int estimateLoss= APFEChighestSeqNo-totalPackets;
   printf("AppFecSink:passPacketUp:%d:sinkID:%d;(%lf)  packet arrived: %d (rawSeqNo:%d,Type:%d), lastDelay:%lf, timestamp:%lf,  nextSeqNo:%d, highest seq no:%d, #lost:%d \n", 
      fid_,sink_id_,now,rh->seqno(),rh->rawSeqNo_,rh->contentType,lastDelay, hdr_cmn::access(p)->timestamp(),nextSeqNo, APFEChighestSeqNo,estimateLoss);
   printf("AppFecSink:passPacketUp:%d:sinkID:%d;(%lf) UDPSinkNumberBytesDelivered:%d, #pkts received:%d, #pkts dropped:%d, #pkts out of order:%d\n", 
      fid_,sink_id_,now, UDPSinkNumberBytesDelivered_, UDPSinkTotalPktsReceived_, UDPSinkNumberPktsDropped_, UDPSinkNumberPktsOutOfOrder_);
#endif

//Done for first packet....
          if (lastDelay == 0) {
            lastDelay = now - h->timestamp();
            nextSeqNo = rh->seqno()+1;
#ifdef TRACEME 
            printf("AppFecSink:passPacketUp:(fid:%d, sinkID::%d)%lf Init nextSeqNo to %d, lastDelay:%lf, hdr TS:%lf\n", 
                fid_,sink_id_,now,nextSeqNo,lastDelay, hdr_cmn::access(p)->timestamp());
#endif
          }
          else 
          {
            if (rh->seqno() == nextSeqNo) {
              delay = now - h->timestamp();
              jitter = delay - lastDelay; 
              if (jitter < 0)
               jitter = -1 * jitter;
#ifdef TRACEME 
              printf("AppFecSink:passPacketUp:(sinkID:%d)JITTER:%lf (lastDelay:%lf, delay:%lf),timeNow:%lf,hdr timestamp:%lf\n", 
                sink_id_,jitter,lastDelay,delay,now, hdr_cmn::access(p)->timestamp());
#endif
        
              numberSamples++;
              totalJitterCount+=jitter;
              totalLatencyCount+=lastDelay;
              avgJitter = totalJitterCount / numberSamples;
              avgLatency = totalLatencyCount / numberSamples;
		 
              lastDelay = delay;
              nextSeqNo = rh->seqno()+1;
            }
            else if (rh->seqno() > nextSeqNo) {

              //Note: this breaks if packets arrive out of order....
              // We estimate the true number lost by tracking the
              // highest seq minus total received
              // We estimate the true out of order in the tcl dumpFinal script
              // 
#ifdef TRACEME 
              printf("AppFecSink:passPacketUp:(sinkID:%d)(fid:%d)(%lf): packet loss TRACE1 before adjusting lossCount:%d,seqno:%d, nextSeqNo:%d\n", 
                sink_id_,fid_,now,lossCount,rh->seqno(),nextSeqNo);
#endif
              lossCount = lossCount + (rh->seqno() - nextSeqNo);
              delay = now - h->timestamp();
              lastDelay = delay;
              nextSeqNo = rh->seqno()+1;

              OutOfOrderEvents++;
              UDPSinkNumberPktsDropped_ = APFEChighestSeqNo - UDPSinkTotalPktsReceived_ ;
			  if (UDPSinkNumberPktsDropped_ < 0)
                UDPSinkNumberPktsDropped_ = 0;
              UDPSinkNumberPktsOutOfOrder_=  OutOfOrderEvents - UDPSinkNumberPktsDropped_;
			  if (UDPSinkNumberPktsOutOfOrder_ < 0)
			    UDPSinkNumberPktsOutOfOrder_  = 0;

#ifdef TRACEME 
              printf("AppFecSink:passPacketUp:(sinkID:%d)(fid:%d)(%lf): packet loss (arrived:%d)  lossCount=%d,  totalPackets=%d, numberDropped:%lf\n", 
                sink_id_,fid_,now,rh->seqno(),lossCount,totalPackets,nextSeqNo,UDPSinkNumberPktsDropped_);
#endif
            }
            else {
              //SHOULD NOT SEE THIS....but if we do we will pass it up
//#ifdef TRACEME 
              printf("AppFecSink:passPacketUp:(sinkID:%d)(fid:%d)(%lf): POSSIBLE ERROR  OLD packet arrived - just ignore (seqNo::%d, rawSeqNo_:%d, blockno_:%d) APFEChighestSeqNo:%d,  lossCount:%d,  nextSeqNo:%d, numberDropped:%lf, #OutOfOrder:%d\n", 
                sink_id_,fid_,now,rh->seqno(),rh->rawSeqNo_,rh->blockno_,APFEChighestSeqNo,lossCount,totalPackets,nextSeqNo,UDPSinkNumberPktsDropped_,OutOfOrderEvents);
//#endif
            }
          }

#ifdef TRACEME 
        if (fid_ == TRACEID) {
           printf("AppFecSink:passPacketUp:(flowID:%d, sinkID:%d)(%f)Rxed uid: %d ,  New sinkthroughput is %d,timsstamp=%f\n", 
              fid_,sink_id_,now, hdr_cmn::access(p)->uid(), UDPSinkThroughput_bytes_,
               hdr_cmn::access(p)->timestamp());
        }
#endif
#ifdef MONITOR_FILES_OK 
            //traces UDP stream AFTER fec
            if (streamID == 1) {
              fp = fopen(rptr, "a+");
              fprintf(fp,"%f %d %d %d %d %d %d %d %d %d %d %d %d %d %d %f\n", now,  streamID, rh->contentType, rh->seqno(), rh->seqno(), APFEChighestSeqNo, (h->size()-APPFEC_PACKET_OVERHEAD),totalPackets, lossCount, rh->blockno_,rh->blockIndex_,rh->N_,rh->K_,numberPacketsInBlock,numberBytesInBlock,delay);
              fclose(fp);
            }
#endif

#ifdef MONITOR_RAW_OK 
            if (rh->contentType != 16) {
              FILE* fp2 = fopen("Raw3.txt", "a+");
              fprintf(fp2,"%f %d \n", now, rh->seqno());
              fclose(fp2);
            }
#endif

	if (app_) {
		app_->recv( (h->size() - APPFEC_PACKET_OVERHEAD) );
    }
	/*
	 * didn't expect packet (or we're a null agent?)
	 */
	Packet::free(p);
}

int AppFecSink::command(int argc, const char*const* argv)
{
#ifdef TRACEME
  printf("AppFecSink:(%lf): command:  #Params:%d,   argv command:%s \n", Scheduler::instance().clock(),argc,argv[1]);
#endif
	if (argc == 3) {
		if (strcmp(argv[1], "dumpstats") == 0) {
          dumpFinalStats((char *)argv[2]);
          return (TCL_OK);
		}
	} 
  if (argc == 5) {
        if (strcmp(argv[1], "initstream") == 0) {
            init(atoi(argv[2]), atoi(argv[3]), atoi(argv[4]));
            return (TCL_OK);
        }
    }

	return (AppFecAgent::command(argc, argv));
}


void AppFecSink::blockTimerHandler(int streamIDParam)
{

  numberBlockTimeouts++;
  timedOutFlag= 1;
#ifdef TRACEME
  printf("AppFecSink:blockTimerHandler(%lf):(%d) POPPED (total:%d) The currentBlockNumber:%d (numberPacketsInBlock:%d)\n",
      Scheduler::instance().clock(), streamID,numberBlockTimeouts,currentBlockNumber,numberPacketsInBlock);
#endif
  processFECBlock();
}

/************************************************************************
*
* Function: void AppFecSinkAgent::dumpFinalStats(char *outputFile)
*
* Inputs:
*    char *outputFile
*   
* Explanation:
*  This is invoked via the tcl command dumpstats.  We dump a single line of stats to
*  the file defined as follows:
*    curr_time,streamID,numberBlockTimeouts, BW, APPThroughput, Efficiency, rawLR, finalLR,
*    numberInOrderPacketArrivals, numberOutOfOrderPacketArrivals,
*    numberBlockQDrops, numberOutOfOrderQDrops,totalDataBytesRxed,totalRedundantDataBytesRxed, totalFECDrops,totalFECRestores, numberBlockTimeouts);
*
*            BW:  total number of bytes sent (including all overhead, AND FEC) 
*                       divided by current time.  So assumes the start of monitor
*                       time is 0.0 seconds.
*            APPThroughput : This is bandwidth consumed by just the data, and not FEC
*                     It does not include the UDP/IP or RTP overhead
*                
*            Efficiency : the ratio of data (not FEC) to total data sent
*                  Efficiency = (double) totalDataBytesRxed / (double) totalBytesRxed;
*
***********************************************************************/
void AppFecSink::dumpFinalStats(char *outputFile)
{
double curr_time = Scheduler::instance().clock();
#ifdef TRACEME
  printf("AppFecSink:dumpFinalStats(%f):(%d) dump to file  %s\n", curr_time, streamID,outputFile);
#endif


  fp = fopen(outputFile, "a+");

  double BW=0;
  double APPThroughput=0;
  double Efficiency = 0;
  double Efficiency1 = 0;
  double Efficiency2 = 0;
  double finalLossRate1 =  0;
  double finalLossRate2 =  0;
  double rawLossRate = 0.0;

  if (totalBytesRxed > 0) {
    BW= totalBytesRxed * 8 / curr_time;
    APPThroughput = totalDataBytesRxed * 8 / curr_time;
//Note:  we will use the first definition as the FEC efficiency
    Efficiency1 = (double) UDPSinkTotalBytesDelivered / (double) totalBytesRxed;
//    Efficiency1 = (double) totalDataBytesRxed / (double) totalBytesRxed;
//Note:  we will use the secon definition as the total efficiency
    Efficiency2 = (double) APPThroughput /  (double) BW;
  }
  if (APFEChighestRawSeqNo > 0) {
    rawLossRate =(double)(APFEChighestRawSeqNo-totalPacketsRxed) /(double)APFEChighestRawSeqNo;
  }

  if (APFEChighestSeqNo > 0){
//Note: thi matches exactly what we see in APFECRECV12.out
//   LR1 is roughly total lost / total sent
    finalLossRate2 = (double)lossCount / (double)APFEChighestSeqNo;
    if (finalLossRate2 < 0.0)
      finalLossRate2=0.0;
//Note:   BUT this matches exactly to UDPstats.out 
//    finalLossRate1 =(double)(APFEChighestSeqNo-UDPSinkTotalPktsReceived_) /(double)APFEChighestSeqNo;
//  Is too high if out of order packets occur
    finalLossRate1 =(double)(APFEChighestSeqNo-totalPackets) /(double)APFEChighestSeqNo;
    if (finalLossRate1 < 0.0)
      finalLossRate1=0.0;
  }

  if (typeFEC == NO_FEC) {
    rawLossRate = finalLossRate2;
  }

  if (rawLossRate > 0)
    Efficiency = 1 - finalLossRate1/rawLossRate;
  if (Efficiency > 1.0) 
    Efficiency = 1.0;

  if (Efficiency < 0) {
    printf("AppFecSink:dumpFinalStats(%f):(%d) OBSERVED AN EFFICIENCY <0: finalLossRate:%3.3f,  rawLossRate:%3.3f \n", curr_time,streamID, finalLossRate1, rawLossRate);
    Efficiency = 0;
  }
  
//#ifdef TRACEME
  printf("dumpFinalLossStats: %lf\t%d\t%d\t%9.0f\t%9.0f\t%2.3f\t%2.3f\t%2.3f\t%2.3f\t%2.3f\t%d\t%d\t%d\t%d\t%d\t%12.0f\t%12.0f\t%d\t%d\t%d\t%2.3f\t%d\t%d\n",
    curr_time,streamID,numberBlockTimeouts, BW, APPThroughput, Efficiency1,Efficiency2,rawLossRate, finalLossRate1, finalLossRate2,
    numberInOrderPacketArrivals, numberOutOfOrderPacketArrivals, numberBlockQDrops, numberOutOfOrderQDrops,
    totalPacketsRxed,totalDataBytesRxed,totalRedundantDataBytesRxed, totalFECDrops,totalFECRestores, numberBlockTimeouts,Efficiency,typeFEC,totalPackets);

  printf("AppFecSink:dumpFinalStats(%f):(%d)  UNCORRECTED: totalPacketsRxed:%d, total totalBytesRead:%f, APFEChighestRawSeqNo:%d\n", 
              curr_time, streamID,totalPacketsRxed, totalBytesRxed, APFEChighestRawSeqNo);
  printf("AppFecSink:dumpFinalStats(%f):(%d) CORRECTED: totalpackets:%d,  UDPSinkTotalPktsReceived:%d, UDPSinkTotalBytesDelivered:%d, APFEChighestSeqNo:%d \n", 
              curr_time, streamID,totalPackets, UDPSinkTotalPktsReceived_, UDPSinkTotalBytesDelivered, APFEChighestSeqNo);
  printf("AppFecSink:dumpFinalStats(%f):(%d) %f %d %d\n", curr_time, streamID,totalBytesRxed, UDPSinkTotalBytesDelivered, UDPSinkTotalPktsReceived_);

  printf("AppFecSink:dumpFinalStats(%f):(%d) rawLR1:%3.3f, highestAPFECRawSeqno:%d, totalPacketsRxed:%d \n", curr_time, streamID,
    rawLossRate, APFEChighestRawSeqNo, totalPacketsRxed);

  printf("AppFecSink:dumpFinalStats(%f):(%d) finalLR1:%3.3f, APFEChighestSeqNo:%d, totalPackets:%d numberOutOfOrderPacketArrivals:%d \n", curr_time, streamID,
                finalLossRate1, APFEChighestSeqNo,totalPackets, numberOutOfOrderPacketArrivals);

  printf("AppFecSink:dumpFinalStats(%f):(%d) finalLR2:%3.3f,lossCount:%d,  APFEChighestSeqNo:%d \n", curr_time, streamID,
                finalLossRate2,lossCount, APFEChighestSeqNo);

  double tmpVar = 0;
  if (totalNumberBlocks > 0)
     tmpVar = numberBlockTimeouts/totalNumberBlocks;
  printf("AppFecSink:dumpFinalStats(%f):(%d) Total Blocks seen:%f, totalBlocksUseful:%f, Percentage of Timeouts/TotalBlocks:%3.3f \n", curr_time, streamID, totalNumberBlocks, totalBlocksUseful,tmpVar);
//#endif

  fprintf(fp,"%lf\t%d\t%d\t%9.0f\t%9.0f\t%2.3f\t%2.3f\t%2.5f\t%2.5f\t%2.5f\t%d\t%d\t%d\t%d\t%d\t%12.0f\t%12.0f\t%d\t%d\t%d\t%2.3f\t%d\t%d\t%d\n",
    curr_time,streamID,numberBlockTimeouts, BW, APPThroughput, Efficiency1,Efficiency2,rawLossRate, finalLossRate1, finalLossRate2,
    numberInOrderPacketArrivals, numberOutOfOrderPacketArrivals, numberBlockQDrops, numberOutOfOrderQDrops,
    totalPacketsRxed,totalDataBytesRxed,totalRedundantDataBytesRxed, totalFECDrops,totalFECRestores, numberBlockTimeouts,Efficiency,typeFEC,totalPackets, totalPacketsRxed);

  fclose(fp);
}

/************************************************************************
*
* Function: void AppFecSink::processFECBlock()
*
* Inputs:
*   
* Explanation:
*  This is invoked when at least one FEC block is ready to be processed.
*  We call this on 4 occasions:
*   If enough pkts of the block are received
*   If a block timeout occurs
*   If there are enough out of block packets (from the next block) that have arrived 
*   Reentered(actually we just loop internally) to handle the case of
*    a second complete block exists in the out of order Q.
*
*  The method might exit with three possibilities:
*    With nothing in the blockQ
*    With some packets from the next block
*    With a complete new block in the blockQ
*
*  Method must maintain the raw stats including
*   totalFECDrops = 0;
*   totalFECRestores = 0;
*
*  Pseudo code
*
* while (blockQ->getListSize() > 0) 
*  {
*    stop block timer
*    set blockState to completed
*    get this blocks N and K
*    if N_ is 1, this is an error
*    if  all N_ received
*      loop through the blockQ
*      {
*        if an element is not yet sent up, clone it and passPacketUp(pkt)
*        delete the element
*      }
*    if K_ or more were received
*      loop through the blockQ
*      {
*        if the element is in order
*          if it has not been sent up yet, clone it and passPacketUp(pkt) 
*          delete the element from the Q
*        if an element is missing in the Q, regenerate and passPacketUp(pkt)
*      }
*    if less than K_ were received
*      loop through the blockQ
*      {
*         if an element is not yet sent up, clone it and passPacketUp(pkt)
*         delete the element
*      }
*    advance currentBlockNumber++;
*    reset block state variables such as numberPacketsInBlock and numberBytesInBlock 
*    if blockQ not empty, throw HARD ERROR
*    if OutOfOrderQ not empty
*      transfer any of the next block (if it exists) from the Outoforder to the blockQ
*      if transfered > 0
*        start block timer
*        set blockState to in progress
*        Get current blocks K
*        if (blockQ->getListSize < K_)
*         break;
*  }
*

* Strange case : if packet arrives and gets in out of orderq, but blockQ empty
*
*************************************************************************/
void AppFecSink::processFECBlock()
{
  double now = Scheduler::instance().clock();
  double delay = 0;
  int blockN_ = 1;
  int blockK_ = 1;
  appfecOrderedListElement *myListElement = NULL;
  Packet *p = NULL;
  int rc = 0;
  int packetBlockNumber;
  int loopFlag = TRUE;


  //Handle special case if we get a 
  if ((blockQ->getListSize()  == 0) && OutOfOrderQ->getListSize() > 0) {
#ifdef TRACEME
    printf("APFECSINK:processFECBlock(flowID:%d,sinkID:%d)(%lf)  blockQListSize:%d, OutOfOrderQListSize:%d \n",
           fid_,sink_id_,now,blockQ->getListSize(), OutOfOrderQ->getListSize());
#endif
    rc =  transferBlock(OutOfOrderQ,blockQ,currentBlockNumber);
#ifdef TRACEME
    printf("APFECSINK:processFECBlock(flowID:%d,sinkID:%d)(%lf)  AFTER SPECIAL transferBlock 0 (rc=%d): (APFEChighestSeqNo+1):%d, currentBlockNumber:%d, size of blockQ:%d, size of OutOfOrderQ:%d \n",
       fid_,sink_id_,now,rc,(APFEChighestSeqNo+1),currentBlockNumber,blockQ->getListSize(),OutOfOrderQ->getListSize());
#endif
  }

//Possibilities:
//blockQ>0, OutOfOrderQ=1
//blockQ>1, OutOfOrderQ=0
//blockQ>1, OutOfOrderQ=1
//It's possible blockQ is empty
  while (blockQ->getListSize() > 0)
  {

    
    if (myTimer->status() != TIMER_IDLE)
      myTimer->force_cancel();

    blockState= COMPLETED;

    //Get this block's N and K by looking at the last element
    myListElement = (appfecOrderedListElement *) blockQ->tail;
    p =  myListElement->getPacket();
    hdr_cmn *h = hdr_cmn::access(p);
    hdr_rtp* rh = hdr_rtp::access(p);
    blockN_ = rh->N_;
    blockK_ = rh->K_;
    packetBlockNumber = rh->blockno_;
//Update the currentBlockNumber
//$A901
    currentBlockNumber = rh->blockno_;
#ifdef TRACEME
    printf("APFECSINK:processFECBlock(flowID:%d,sinkID:%d)(%lf)  currentBlockNumber:%d, pktBlockNumber:%d (#pkts:%d, #bytes:%d), size of blockQ:%d, Block N_:%d, Block K_:%d \n",
           fid_,sink_id_,now,currentBlockNumber,packetBlockNumber,numberPacketsInBlock,numberBytesInBlock,blockQ->getListSize(),blockN_,blockK_);
#endif
   
    //if there is NO FEC then an error has occured
    if (blockN_ == 1) 
    {
      printf("APFECSINK:processFECBlock(flowID:%d,sinkID:%d)(%lf) HARD ERROR, NO FEC ?? \n",
           fid_,sink_id_,now);
      exit(1);
    }

    //if we received the entire block 
//      -loop through the List
//        *if an element is not yet sent up, do so
//        *delete that element
    if (blockQ->getListSize() == blockN_)
    {
#ifdef TRACEME
      printf("APFECSINK:processFECBlock(flowID:%d,sinkID:%d)(%lf)  Received Entire block,  currentBlockNumber:%d, size of blockQ:%d, size of OutOfOrderQ:%d \n",
           fid_,sink_id_,now,currentBlockNumber,blockQ->getListSize(),OutOfOrderQ->getListSize());
#endif
      totalBlocksUseful++;
      rc = clearQueue(blockQ);
 
    }
    //else if we  received enough of the block to complete FEC 
    else if (blockQ->getListSize() >= blockK_)
    {
      totalBlocksUseful++;
      rc = clearQueueAndCorrectDrops(blockQ);
    }
    //else  we did not receive enough packets to recover anything....just pass up what we received
    else
    {
#ifdef TRACEME
      printf("APFECSINK:processFECBlock(flowID:%d,sinkID:%d)(%lf)  Did not Received Entire block,  currentBlockNumber:%d, size of blockQ:%d, size of OutOfOrderQ:%d \n",
           fid_,sink_id_,now,currentBlockNumber,blockQ->getListSize(),OutOfOrderQ->getListSize());
#endif
      rc = clearQueue(blockQ);
    }
 
    //At this point, timer is stopped, state set to completed and now increment to expect next block Number
    currentBlockNumber++;
    if (currentBlockNumber < (packetBlockNumber+1))
      currentBlockNumber = packetBlockNumber+1;

    totalNumberBlocks++;
    numberPacketsInBlock=0;
    numberBytesInBlock=0;
    blockIndex_ = 0;


#ifdef TRACEME
    printf("APFECSINK:processFECBlock(flowID:%d,sinkID:%d)(%lf)  (APFEChighestSeqNo+1):%d, currentBlockNumber:%d, size of blockQ:%d, size of OutOfOrderQ:%d \n",
           fid_,sink_id_,now,(APFEChighestSeqNo+1),currentBlockNumber,blockQ->getListSize(),OutOfOrderQ->getListSize());
#endif
    //transfer any packets from other queue
    if (OutOfOrderQ->getListSize() > 0) {
        rc =  transferBlock(OutOfOrderQ,blockQ,currentBlockNumber);
        totalNumberBlocks++;
        blockState = INPROGRESS;
        myTimer->sched(blockTimerDelay);
        //Get this block's N and K by looking at the last element
        myListElement = (appfecOrderedListElement *) blockQ->tail;
        p =  myListElement->getPacket();
        hdr_rtp* rh = hdr_rtp::access(p);
        hdr_cmn *h = hdr_cmn::access(p);
        blockN_ = rh->N_;
        blockK_ = rh->K_;
        packetBlockNumber = rh->blockno_;
        currentBlockNumber = rh->blockno_;
#ifdef TRACEME
       printf("APFECSINK:processFECBlock(flowID:%d,sinkID:%d)(%lf)  AFTER transferBlock 0 (rc=%d): (APFEChighestSeqNo+1):%d, currentBlockNumber:%d, size of blockQ:%d, size of OutOfOrderQ:%d \n",
       fid_,sink_id_,now,rc,(APFEChighestSeqNo+1),currentBlockNumber,blockQ->getListSize(),OutOfOrderQ->getListSize());
#endif
        //just break and return if we don't have the full block
        if (blockQ->getListSize() < rh->K_) {
#ifdef TRACEME
           printf("APFECSINK:processFECBlock(flowID:%d,sinkID:%d)(%lf)  AFTER transferBlock 1: (APFEChighestSeqNo+1):%d, currentBlockNumber:%d, size of blockQ:%d, size of OutOfOrderQ:%d \n",
           fid_,sink_id_,now,(APFEChighestSeqNo+1),currentBlockNumber,blockQ->getListSize(),OutOfOrderQ->getListSize());
#endif
          break;
        }
#ifdef TRACEME
      printf("APFECSINK:processFECBlock(flowID:%d,sinkID:%d)(%lf)  AFTER transferBlock 2: (APFEChighestSeqNo+1):%d, currentBlockNumber:%d, size of blockQ:%d, size of OutOfOrderQ:%d \n",
           fid_,sink_id_,now,(APFEChighestSeqNo+1),currentBlockNumber,blockQ->getListSize(),OutOfOrderQ->getListSize());
#endif
    }
  }
#ifdef TRACEME
  if (blockQ->getListSize() > 0) {
  //THis is ok.....
      printf("APFECSINK:processFECBlock(flowID:%d,sinkID:%d)(%lf) AFTER transferBlock/processFEC: currentBlockNumber:%d, size of blockQ:%d, size of OutOfOrderQ:%d \n",
           fid_,sink_id_,now,currentBlockNumber,blockQ->getListSize(),OutOfOrderQ->getListSize());
  }
#endif
}


void AppFecSink::init(int streamIDParam, int NParam, int KParam)
{


  AppFecAgent::init(streamIDParam,NParam,KParam);
  rptr = rawVideoTraceString;
  sprintf(rptr,"RawVideoTrace%dArrived.out",streamID);
  mptr = monitorTraceString;
  sprintf(mptr,"MonitorVideoTrace%dArrived.out",streamID);
  //We are moving away from the VideoTrace* file - eventually remove it
  sprintf(tptr,"VideoTrace%dArrived.out",streamID);
  maxBlockListSize = MAX_BLOCK_LIST;
  maxOutOfOrderListSize = MAX_BLOCK_OUTOFORDER_LIST;
//  maxBlockListSize = 20;
//  maxOutOfOrderListSize = 100;
  blockQ = new OrderedListObj();
  blockQ->initList(maxBlockListSize);
  OutOfOrderQ = new OrderedListObj();
  OutOfOrderQ->initList(maxOutOfOrderListSize);

  timedOutFlag = 0;

  APFEChighestSeqNo= 0;
  APFEChighestRawSeqNo= 0;
  currentBlockNumber = 1;
  numberInOrderPacketArrivals = 0;
  numberOutOfOrderPacketArrivals = 0;
  blockState= INITIALIZED;
  numberBlockQDrops = 0;
  numberOutOfOrderQDrops = 0;

}

/************************************************************************
*
* Function: int  AppFecSink::clearQueue(ListObj *ListPtr)
*
* Inputs:
*    Ptr to the list to clear
*
* Output:
*    returns the number of packets removed
*   
* Explanation:
*    This method removes all elements in the List,
*     deleting the packet and element object of all elements
*    This method makes sure that all elements have been sent up....
*      if it encounters an element that has not, it sends it up
*         BUT ONLY IF IT IS NOT A FEC packet
*
*
*  pseudo code:
*
*************************************************************************/
int  AppFecSink::clearQueue(OrderedListObj *ListPtr)
{
  int rc = 0;
  appfecOrderedListElement *myListElement = NULL;
  double now = Scheduler::instance().clock();
  Packet *p = NULL;
  int packetsRemoved = 0;
  int nextExpectedSNUMInQueue = -1;
  hdr_cmn* h;
  hdr_rtp* rh;


#ifdef TRACEME
    printf("APFECSINK:clearQueue(%lf): On Entry List Size:%d, numberPacketsInBlock:%d,numberBytesInBlock:%d \n",
           now,ListPtr->getListSize(),numberPacketsInBlock,numberBytesInBlock);
#endif
  while (ListPtr->getListSize() > 0)
  {
    myListElement =  (appfecOrderedListElement *) ((ListObj *)ListPtr)->removeElement();
    if (myListElement != NULL)
    {
      p = myListElement->getPacket();
      if (p!=NULL)
      {
        h = hdr_cmn::access(p);
        rh = hdr_rtp::access(p);
        packetsRemoved++;
        if (myListElement->sentUpFlag == TRUE)
        {
          Packet::free(p);

        }
        else {
          if ((rh->contentType != FEC_OVERHEAD) && (rh->blockno_ == currentBlockNumber))
            passPacketUp(p);
          else{
//#ifdef TRACEME
             if (rh->contentType != FEC_OVERHEAD){
                  printf("APFECSINK:clearQueue(%lf):  ERROR blockQsize:%d but not passing up as packet blockno:%d not currentBlockNumber:%d \n",
                    now,ListPtr->getListSize(),rh->blockno_,currentBlockNumber);
             }
//#endif
            Packet::free(p);

           }
        }
        
      }
      delete(myListElement);
    }
  }

#ifdef TRACEME
  printf("\nAPFECSINK:clearQueue(%lf): On exit,  List Size:%d, number packetsRemoved:%d\n",
           now,ListPtr->getListSize(),packetsRemoved);
#endif
        numberPacketsInBlock=0;
        numberBytesInBlock=0;
  return packetsRemoved;
}

/************************************************************************
*
* Function: int  AppFecSink::clearQueueAndCorrectDrops(OrderedListObj *ListPtr)
*
* Inputs:
*    Ptr to the list to clear
*
* Output:
*    returns the number of packets removed
*   
* Explanation:
*    This method removes all elements in the List,
*     deleting the packet and element object of all elements
*    This method makes sure that all elements have been sent up....
*      if it encounters an element that has not, it sends it up.
*      it it finds a gap (dropped packets), it regenerates
*      the packet and sends it up
*      If the first packet is missing in the block, we correctly
*      regenerate it.
*
*  pseudo code:
*
*************************************************************************/
int  AppFecSink::clearQueueAndCorrectDrops(OrderedListObj *ListPtr)
{
  int rc = 0;
  appfecOrderedListElement *myListElement = NULL;
  double now = Scheduler::instance().clock();
  Packet *p = NULL;
  int packetsRemoved = 0;
  int nextExpectedIndexInQueue = -1;
  int lastInOrderSeqno=0;
  int lastInOrderRawSeqno = 0;
  int gapLength = 0;
  hdr_cmn* h;
  hdr_ip* iph;
  hdr_rtp* rh;
  int blockN_ = 1;
  int blockK_ = 1;
  int blockPacketSize;
  int numberDataPktsExpected = 0;
  int numberDataPktsSentUp = 0;
  Packet *gapPkt;
  //$A902
  int  ThisblockN_ = 0;
  int  ThisblockK_ = 0;
  int  ThisBlockNumber = 0;
  //This will count the number of src data pkts that have been passed up, or are passed up in this routine
  //This is different than numberDataPktsSentUp....
  //At the end of this routine, PassedUpCount had better be k else there was an error
  int  PassedUpCount = 0;


  if (numberPacketsInBlock == 0)
  {
    blockPacketSize = size_;
    printf("APFECSINK:clearQueueAndCorrectDrops(%lf): WARNING: On Entry List Size:%d, numberPacketsInBlock:%d,numberBytesInBlock:%d, avg packet Size:%d\n",
           now,ListPtr->getListSize(),numberPacketsInBlock,numberBytesInBlock,blockPacketSize);
  }
  else
   blockPacketSize = numberBytesInBlock/ numberPacketsInBlock;


    //$A902
    //Get this block's N and K by looking at the last element
    myListElement = (appfecOrderedListElement *) blockQ->tail;
    p =  myListElement->getPacket();
    h = hdr_cmn::access(p);
    rh = hdr_rtp::access(p);
    ThisblockN_ = rh->N_;
    ThisblockK_ = rh->K_;
    ThisBlockNumber = rh->blockno_;

#ifdef TRACEME
    printf("APFECSINK:clearQueueAndCorrectDrops(%lf): On Entry List Size:%d, currentBlockNumber:%d, tailInfo: block#:%d,N:%d, k:%d, numberPacketsInBlock:%d,numberBytesInBlock:%d, avg packet Size:%d\n",
           now,ListPtr->getListSize(),currentBlockNumber,ThisBlockNumber,ThisblockN_,ThisblockK_,numberPacketsInBlock,numberBytesInBlock,blockPacketSize);
#endif
  while (ListPtr->getListSize() > 0)
  {
    myListElement =  (appfecOrderedListElement *) ((ListObj *)ListPtr)->removeElement();
    if (myListElement != NULL)
    {
      p = myListElement->getPacket();
      if (p!=NULL)
      {
        packetsRemoved++;

        h = hdr_cmn::access(p);
        rh = hdr_rtp::access(p);
        //iph = hdr_ip::access(p);
        // only do this once
        if (nextExpectedIndexInQueue == -1){
          //if true, then we are missing the first index
          // The real first pkt in the block must have a blockIndex_ of 1
          if (rh->blockIndex_ > 1) {
            nextExpectedIndexInQueue = 1;
            //This will be unknown but will be fixed shortly
            lastInOrderSeqno= rh->seqno() - (rh->blockIndex_ );
            lastInOrderRawSeqno = rh->rawSeqNo_ - (rh->blockIndex_ );
          }
          else  {
            nextExpectedIndexInQueue = rh->blockIndex_;
            lastInOrderSeqno= rh->seqno();
            lastInOrderRawSeqno = rh->rawSeqNo_;
          }
          currentBlockNumber=rh->blockno_;
        }
#ifdef TRACEME
    printf("APFECSINK:clearQueueAndCorrectDrops(%lf): Removed pkt (new size:%d) Look at pkt seqno:%d, rawSeqNo:%d, nextExpectedIndex:%d, blockIndex_:%d, lastInOrderSeqno:%d\n",
           now,ListPtr->getListSize(),rh->seqno(),rh->rawSeqNo_,nextExpectedIndexInQueue,rh->blockIndex_,lastInOrderSeqno);
#endif

        //If this is NOT true we fix the these later....
        if (nextExpectedIndexInQueue == rh->blockIndex_)
        {
           lastInOrderSeqno =  rh->seqno();
           lastInOrderRawSeqno = rh->rawSeqNo_;
        }
#ifdef TRACEME
         printf("APFECSINK:clearQueueAndCorrectDrops(%lf):  APFEChighestSeqNo:%d, rh->seqno:%d (rawSeqNo_:%d),nextExpectedIndexInQueue:%d, lastInOrderSeqNo:%d, lastInOrderRawSeqNo:%d\n",
           now,APFEChighestSeqNo,rh->seqno(),rh->rawSeqNo_,nextExpectedIndexInQueue,lastInOrderSeqno,lastInOrderRawSeqno);
#endif

        blockN_ = rh->N_;
        blockK_ = rh->K_;

        numberDataPktsExpected = blockK_;
        gapLength = rh->blockIndex_ - nextExpectedIndexInQueue;
        if (gapLength < 0)
          gapLength = 0;

//And, if we are now in redundent data....we need to be careful.
//      how muhc of the data is in the block?
#ifdef TRACEME
         printf("APFECSINK:clearQueueAndCorrectDrops(%lf):  blockN_:%d,BlockK_:%d, blockPacketSize:%d,numberDataPktsExpected:%d, adj. gapLength:%d, APFEChighestSeqNo:%d, blockIndex:%d, nextExpectedInQ:%d\n",
           now,blockN_,blockK_,blockPacketSize,numberDataPktsExpected,gapLength,APFEChighestSeqNo,rh->blockIndex_,nextExpectedIndexInQueue);
#endif

        //First,  if the current packet is Redundent, then we should not pass this up
        while (gapLength-- > 0)
        {
#ifdef TRACEME
           printf("APFECSINK:clearQueueAndCorrectDrops(%lf):  Found PACKET DROP, blockIndex:%d, APFEChighestSeqNo:%d, nextExpectedIndex:%d\n",
           now,rh->blockIndex_,APFEChighestSeqNo,nextExpectedIndexInQueue);
#endif
          //$A901 - make sure we don't create a redundant pkt
          if (rh->blockIndex_ > blockK_) {
#ifdef TRACEME
            printf("APFECSINK:clearQueueAndCorrectDrops(%lf):  Break early as we are beyond source data (current blockIndex_:%d, blockK_:%d) \n",
            now,rh->blockIndex_,blockK_);
#endif
             break;
          }

          //Create a new packet and send it up
          gapPkt = allocpkt();

          h = hdr_cmn::access(gapPkt);
          //iph = hdr_ip::access(gapPkt);
          hdr_rtp* rh = hdr_rtp::access(gapPkt);
          h->size() = blockPacketSize;

          numberBytesInBlock += h->size();
          numberPacketsInBlock++;
          
          rh->flags() = 0;
          rh->seqno() = ++lastInOrderSeqno;
          rh->rawSeqNo_ = ++lastInOrderRawSeqno;
          rh->contentType = MPEG4_B_FRAME;

          rh->blockno_=currentBlockNumber;
          rh->blockIndex_=nextExpectedIndexInQueue++;
          rh->timestamp_ = now;
          rh->N_ = blockN_;
          rh->K_ = blockK_;

#ifdef TRACEME
           printf("APFECSINK:clearQueueAndCorrectDrops(%lf):  Pass up  blockIndex:%d seqno:%d (gapLength:%d), nextExpectedIndex is %d, lastInOrderSeqno:%d\n",
           now,rh->blockIndex_,rh->seqno(),gapLength,nextExpectedIndexInQueue,lastInOrderSeqno);
#endif

          //So if this is FEC, we won't pass it up
          if ((rh->contentType != FEC_OVERHEAD) && (rh->blockno_ == currentBlockNumber)){
#ifdef TRACEME
            printf("APFECSINK:clearQueueAndCorrectDrops(%lf): nextExpectedIndexInQueue:%d, packetsRemoved:%d, Packet (seqno_:%d,rawSeqNo:%d,blockNo:%d)  GAP PACKET SENT UP (index:%d) \n",
                  now,nextExpectedIndexInQueue,packetsRemoved, rh->seqno_, rh->rawSeqNo_, rh->blockno_,rh->blockIndex_);
#endif
            numberDataPktsSentUp++;
            totalFECRestores++;
            passPacketUp(gapPkt);
            PassedUpCount++;
           }
           else {
             if (rh->contentType != FEC_OVERHEAD){
                  printf("APFECSINK:clearQueueAndCorrectDrops(%lf):  ERROR 1 blockQsize:%d but not passing up as packet blockno:%d not currentBlockNumber:%d , or its FEC OVERHEAD:%d\n",
                    now,ListPtr->getListSize(),rh->blockno_,currentBlockNumber,rh->contentType);
             }
          }
        }
        nextExpectedIndexInQueue++;

//$A902 
        lastInOrderSeqno++;
        lastInOrderRawSeqno++;

        packetsRemoved++;
#ifdef TRACEME
        if (myListElement->sentUpFlag == TRUE)
          printf("APFECSINK:clearQueueAndCorrectDrops(%lf): nextExpectedIndexInQueue:%d, packetsRemoved:%d, Packet (seqno_:%d,rawSeqNo:%d,blockNo:%d)  ALREADY SENT UP (index:%d)\n",
                  now,nextExpectedIndexInQueue,packetsRemoved, rh->seqno_, rh->rawSeqNo_, rh->blockno_,rh->blockIndex_);
        else
          printf("APFECSINK:clearQueueAndCorrectDrops(%lf): nextExpectedIndexInQueue:%d, packetsRemoved:%d, Packet (seqno_:%d,rawSeqNo:%d,blockNo:%d) WILL BE SENT UP (index:%d)\n",
                  now,nextExpectedIndexInQueue,packetsRemoved, rh->seqno_, rh->rawSeqNo_, rh->blockno_,rh->blockIndex_);
#endif
        if (myListElement->sentUpFlag == TRUE)
        {
          PassedUpCount++;
          numberDataPktsSentUp++;
          Packet::free(p);

        }
        else {
          //So if this is FEC, we won't pass it up
          if ((rh->contentType != FEC_OVERHEAD) && (rh->blockno_ == currentBlockNumber))
          {
            numberDataPktsSentUp++;
            passPacketUp(p);
            PassedUpCount++;
          }
          else{
             if (rh->contentType != FEC_OVERHEAD){
                  printf("APFECSINK:clearQueueAndCorrectDrops(%lf):  ERROR 2 blockQsize:%d but not passing up as packet blockno:%d not currentBlockNumber:%d , or its FEC OVERHEAD:%d\n",
                    now,ListPtr->getListSize(),rh->blockno_,currentBlockNumber,rh->contentType);
             }
            Packet::free(p);

          }
        }
      }
      delete(myListElement);
    }
  }

#ifdef TRACEME
  printf("APFECSINK:clearQueueAndCorrectDrops(%lf): On exit,  List Size:%d, number packetsRemoved:%d\n",
           now,ListPtr->getListSize(),packetsRemoved);

  //$A902
  if (PassedUpCount != ThisblockK_) {
     printf("APFECSINK:clearQueueAndCorrectDrops(%lf):  WRONG NUMBER PASSED UP but this is OK   List Size:%d, number packetsRemoved:%d, PassedUpCount:%d, ThisblockK_:%d, blockK_%d\n",
           now,ListPtr->getListSize(),packetsRemoved,PassedUpCount,ThisblockK_,blockK_);
  }

#endif

  if (ListPtr->getListSize() > 0)
  {
     printf("APFECSINK:clearQueueAndCorrectDrops(%lf):  ERROR LIST NOT EMPTY  List Size:%d, number packetsRemoved:%d, PassedUpCount:%d, ThisblockK_:%d, blockK_%d\n",
           now,ListPtr->getListSize(),packetsRemoved,PassedUpCount,ThisblockK_,blockK_);
  }

  return packetsRemoved;
}


/************************************************************************
*
* Function: int  transferBlock(OrderedListObj *srcListPtr, OrderedListObj *dstListPtr, int currentBlockNumber)
*
* Inputs:
*  OrderedListObj *srcListPtr  : ptr to source list
*  OrderedListObj *dstListPtr : ptr to destination list
*  int currentBlockNumber : block number of interest
*
* Output:
*    returns the number of packets transferred.  
*    A 0 implies nothing was done.
*   
* Explanation:
*    This method transfers all elements of the speficied block number
*    from the src to to the dst list
*
*  pseudo code:
*
*************************************************************************/
int  AppFecSink::transferBlock(OrderedListObj *srcListPtr, OrderedListObj *dstListPtr, int currentBlockNumberP)
{
  int rc = 0;
  int sizeSrcQ  = srcListPtr->getListSize();
  int sizeDstQ  = dstListPtr->getListSize();
  appfecOrderedListElement *myListElement = NULL;
  double now = Scheduler::instance().clock();
  Packet *p;
  hdr_cmn* h;
  hdr_rtp* rh;
  int transferCount=0;

#ifdef TRACEME
    printf("APFECSINK:transferBlock(%lf): On Entry blockNumber:%d, src List Size:%d, dst List Size:%d \n",
           now,currentBlockNumberP,srcListPtr->getListSize(),dstListPtr->getListSize());
#endif

  while (sizeSrcQ-- > 0)
  {
    myListElement =  (appfecOrderedListElement *) ((ListObj *)srcListPtr)->removeElement();
    if (myListElement != NULL)
    {
      //We don't want to remove it if not in the currentBlockP
      p = myListElement->getPacket();
      h = hdr_cmn::access(p);
      rh = hdr_rtp::access(p);
      //case if the dst list is empty, we need to transfer at least one packet in to restart the block
      if (dstListPtr->getListSize() == 0) {
        currentBlockNumberP = rh->blockno_;
      }
      if (rh->blockno_ > currentBlockNumberP) {
        rc = srcListPtr->addElementatHead(*(OrderedListElement *)myListElement);
#ifdef TRACEME
        printf("APFECSINK:transferBlock(%lf): Can't tranfer this....blockNumber:%d, reinsert (rc:%d)  src List Size:%d, dst List Size:%d \n",
           now,rh->blockno_,rc,srcListPtr->getListSize(),dstListPtr->getListSize());
#endif
        break;
      }
      rc = dstListPtr->addElement(*(OrderedListElement *)myListElement);
      if (rc == SUCCESS) {
        myListElement->packetEntryTime = now;
        transferCount++;
        numberPacketsInBlock++;
        numberBytesInBlock += h->size();
       // how about blockIndex_ ? - right now we don't use it
       //blockIndex_=rh->blockindex_;
      }
      else
      {
         printf("APFECSINK:transferBlock(%lf): HARD ERROR (rc=%d) blockNumber:%d, src List Size:%d, dst List Size:%d \n",
           now,rc,currentBlockNumberP,srcListPtr->getListSize(),dstListPtr->getListSize());
         exit(1);
      }
    }
    else
      {
         printf("APFECSINK:transferBlock(%lf): HARD ERROR (Null ListElement)  blockNumber:%d, src List Size:%d, dst List Size:%d \n",
           now,currentBlockNumberP,srcListPtr->getListSize(),dstListPtr->getListSize());
         exit(1);
      }
  }
  //$A901  update the global currentBlockNumber
  currentBlockNumber = currentBlockNumber;
#ifdef TRACEME
  printf("APFECSINK:transferBlock(%lf): On Exit blockNumber:%d, src List Size:%d, dst List Size:%d \n",
           now,currentBlockNumberP,srcListPtr->getListSize(),dstListPtr->getListSize());
#endif
  if (rc == SUCCESS)
    return transferCount;
  else
    return rc;
}


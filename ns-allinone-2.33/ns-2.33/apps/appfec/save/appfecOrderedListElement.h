/************************************************************************
* File:  appfecOrderedListElement.h
*
* Purpose:
*   Inlude files for the packet List Elements
*
* Revisions:
***********************************************************************/
#ifndef appfecOrderedListElement_h
#define appfecOrderedListElement_h

#include "packet.h"
#include "rtp.h"

#include "../../mac/docsis/ListElement.h"

class appfecOrderedListElement: public  OrderedListElement
{

private:
  Packet *myPkt;

public:
  appfecOrderedListElement();
  appfecOrderedListElement(Packet *p);
  virtual ~appfecOrderedListElement();
  int putPkt(Packet *pkt); 
  Packet * getPacket();
  int sentUpFlag;
  double packetEntryTime;
};

#endif


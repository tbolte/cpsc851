/* -*-	Mode:C++; c-basic-offset:8; tab-width:8; indent-tabs-mode:t -*- */
/*
 * Copyright (C) Xerox Corporation 1997. All rights reserved.
 *  
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * Linking this file statically or dynamically with other modules is making
 * a combined work based on this file.  Thus, the terms and conditions of
 * the GNU General Public License cover the whole combination.
 *
 * In addition, as a special exception, the copyright holders of this file
 * give you permission to combine this file with free software programs or
 * libraries that are released under the GNU LGPL and with code included in
 * the standard release of ns-2 under the Apache 2.0 license or under
 * otherwise-compatible licenses with advertising requirements (or modified
 * versions of such code, with unchanged license).  You may copy and
 * distribute such a system following the terms of the GNU GPL for this
 * file and the licenses of the other code concerned, provided that you
 * include the source code of that other code when and as the GNU GPL
 * requires distribution of source code.
 *
 * Note that people who make modified versions of this file are not
 * obligated to grant this special exception for their modified versions;
 * it is their choice whether to do so.  The GNU General Public License
 * gives permission to release a modified version without this exception;
 * this exception also makes it possible to release a modified version
 * which carries forward this exception.
 *
 * Revisions:
 * 
 *   $A901 : trapped if set N_ to 0- we want to do this to disable FEC.
 */


#ifndef lint
static const char rcsid[] =
    "@(#) $Header: /cvsroot/nsnam/ns-2/apps/udp.cc,v 1.21 2005/08/26 05:05:28 tomh Exp $ (Xerox)";
#endif

#include "udp.h"
#include "rtp.h"
#include "random.h"
#include "address.h"
#include "ip.h"

#include "appfec.h"

//If defined tracing is enabled
#include "../mac/docsis/docsisDebug.h"
//#define TRACEME 1

//Normally KEEP this on
#define FILES_OK  1



static class AppFecAgentClass : public TclClass {
public:
	AppFecAgentClass() : TclClass("Agent/APFEC") {}
	TclObject* create(int, const char*const*) {
		return (new AppFecAgent());
	}
} class_appfecagent;

AppFecAgent::AppFecAgent() : Agent(PT_UDP), seqno_(-1)
{
	bind("packetSize_", &size_);
#ifdef TRACEME
  printf("AppFecAgent:(%lf): Constructed\n", Scheduler::instance().clock());
#endif
   state = UNINITIALIZED;
   seqno_=0;
   rawSeqNo_ = 0;
   blockIndex_=0;
   numberBytesInBlock = 0;
   numberPacketsInBlock = 0;
   blockno_ = 1;
   N_ = 1;
   K_ = 1;
   typeFEC = NO_FEC;
   streamType = MPEG4_VIDEO;
   fixedBlockMode_ == TRUE;
   fp = NULL; 
   numberFECPackets =0;
   myTimer = new AppFecBlockTimer(this);
   numberBlockTimeouts = 0;
   totalPacketsSent = 0;
   totalPacketsRxed = 0;
   totalDataPacketsSent = 0;
   totalDataPacketsRxed = 0;
   totalBytesSent = 0;
   totalBytesRxed = 0;
   totalDataBytesSent = 0;
   totalDataBytesRxed = 0;
   totalRedundantDataBytesSent = 0;
   totalRedundantDataBytesRxed = 0;
   totalFECDrops = 0;
   totalFECRestores = 0;
 
}



/************************************************************************
*
* Function: void AppFecAgent::sendmsg(int nbytes, AppData* data, const char* flags)
*
* Inputs:
*    int nbytes
*    AppData* data
*    const char* flags
*   
* Explanation:
*  This sends nbytes of application data over the UDP socket.
*  This method is called when FEC is NOT enabled.
*
*************************************************************************/
void AppFecAgent::sendmsg(int nbytes, AppData* data, const char* flags)
{
Packet *p;
int n;
int rem;
hdr_cmn* h;


//$A901
//	n = ceil( (double)nbytes / (double)size_);
	n =  nbytes / size_;
    rem = nbytes % size_;


#ifdef TRACEME
    printf("AppFecAgent:sendmsg(%lf): send this many packets %d, rem:%d,  (nbytes:%d, size_:%d)\n",
                    Scheduler::instance().clock(),n,rem,nbytes,size_);
#endif

	if (nbytes == -1) {
		printf("Error:  sendmsg() for UDP should not be -1\n");
		return;
	}	

	// If they are sending data, then it must fit within a single packet.
	if (data && nbytes > size_) {
		printf("Error: data greater than maximum UDP packet size\n");
		return;
	}

	double local_time = Scheduler::instance().clock();
	while (n-- > 0) {

		p = allocpkt();

		hdr_cmn::access(p)->size() = size_;
        h = hdr_cmn::access(p);
	 	hdr_rtp* rh = hdr_rtp::access(p);
		rh->flags() = 0;
		rh->seqno() = ++seqno_;

        hdr_cmn::access(p)->timestamp() = local_time;
        



		// add "beginning of talkspurt" labels (tcl/ex/test-rcvr.tcl)
		if (flags && (0 ==strcmp(flags, "NEW_BURST")))
			rh->flags() |= RTP_M;
#ifdef TRACEME
        printf("udp:(%lf):1: Send UDP message: size:%d, seqno:%d,  hdr timestamp:%lf\n",
               Scheduler::instance().clock(),
               hdr_cmn::access(p)->size(),rh->seqno(),
               hdr_cmn::access(p)->timestamp());
#endif

        totalPacketsSent++; 
        totalBytesSent+=h->size();
        totalDataBytesSent+=h->size();

//Creates the Video Trace file 
//NOTE:  GET RID OF ?
#ifdef FILES_OK 
      fp = fopen(msgptr, "a+");
      fprintf(fp,"%f %d %d %d %d %d %d %d %d %d %d %d %d \n", local_time, streamID, rh->contentType, rh->seqno(), rh->rawSeqNo_,rh->rawSeqNo_,h->size(),totalPacketsSent,0,rh->blockno_,rh->blockIndex_,rh->N_,rh->K_);
      fclose(fp);
#endif
  //traces UDP stream before fec
      if (streamID == 12) {
        fp = fopen("APFECSEND12.out", "a+");
        fprintf(fp,"%f %d %d %d %d %d %d %d\n", local_time,  streamID, rh->contentType, rh->seqno(), rh->rawSeqNo_, rh->seqno(),(h->size()-APPFEC_PACKET_OVERHEAD),totalPacketsSent);
        fclose(fp);
      }

      
		p->setdata(data);
		target_->recv(p);
	}
	n = nbytes % size_;
	if (n > 0) {
      p = allocpkt();
      hdr_cmn::access(p)->size() = n;
      hdr_rtp* rh = hdr_rtp::access(p);
      rh->flags() = 0;
      rh->seqno() = ++seqno_;
      hdr_cmn::access(p)->timestamp() = local_time;

      rh->N_ = N_;
      rh->K_ = K_;
//Send I frames, and occasionally a B frame
      double luck = Random::uniform();
      if (luck > .90)
        rh->contentType = MPEG4_B_FRAME;
      else if (luck > .80)
        rh->contentType = MPEG4_P_FRAME;
      else
        rh->contentType = MPEG4_I_FRAME;

//$A901
//      h->timestamp() = local_time;


		// add "beginning of talkspurt" labels (tcl/ex/test-rcvr.tcl)
      if (flags && (0 == strcmp(flags, "NEW_BURST")))
         rh->flags() |= RTP_M;
#ifdef TRACEME
      printf("udp:(%lf):2:Send UDP message: size:%d, seqno:%d,  hdr timestamp:%lf\n",
             Scheduler::instance().clock(),
             hdr_cmn::access(p)->size(),rh->seqno(),
             hdr_cmn::access(p)->timestamp());
#endif

        totalPacketsSent++; 
        totalBytesSent+=hdr_cmn::access(p)->size();
        totalDataBytesSent+=hdr_cmn::access(p)->size();

#ifdef FILES_OK 
     fp = fopen(msgptr, "a+");
          fprintf(fp,"%f %d %d %d %d %d %d \n", local_time, rh->contentType, rh->seqno(),hdr_cmn::access(p)->size(),rh->blockno_,rh->N_,rh->K_);
     fclose(fp);
#endif    


		p->setdata(data);
		target_->recv(p);
	}
	idle();
}

/************************************************************************
*
* Function: void AppFecAgent::recv(Packet* pkt, Handler*)
*
* Inputs:
*  Packet *pkt
*  Handler *
*   
* Explanation:
*    This method passes up the packet to the application
*
* NOTE:  This method is no longer used....see passPacketUp()
*
*************************************************************************/
void AppFecAgent::recv(Packet* pkt, Handler*)
{
	if (app_ ) {
		// If an application is attached, pass the data to the app
		hdr_cmn* h = hdr_cmn::access(pkt);
		app_->process_data(h->size(), pkt->userdata());
	} else if (pkt->userdata() && pkt->userdata()->type() == PACKET_DATA) {
		// otherwise if it's just PacketData, pass it to Tcl
		//
		// Note that a Tcl procedure Agent/Udp recv {from data}
		// needs to be defined.  For example,
		//
		// Agent/Udp instproc recv {from data} {puts data}

		PacketData* data = (PacketData*)pkt->userdata();

		hdr_ip* iph = hdr_ip::access(pkt);
                Tcl& tcl = Tcl::instance();
		tcl.evalf("%s process_data %d {%s}", name(),
		          iph->src_.addr_ >> Address::instance().NodeShift_[1],
			  data->data());
	}
	Packet::free(pkt);


}


void AppFecAgent::init(int streamIDParam, int NParam, int KParam)
{
  streamID=streamIDParam;
  tptr = traceString;
  msgptr = msgTraceString;
  seqno_=0;
  rawSeqNo_ = 0;
  blockIndex_=0;
  numberBytesInBlock = 0;
  numberPacketsInBlock = 0;
  blockno_ = 1;
  N_ = NParam;
  K_ = KParam;
  numberFECPackets = N_ - K_;
  if (numberFECPackets < 0)
    numberFECPackets = 0;
  if ((N_ > 1) && (K_ < N_)) {
    typeFEC = RS_FEC;
  }
  streamType = MPEG4_VIDEO;
  fp = NULL;
  state = ACTIVE;
  sprintf(msgptr,"MSGTrace%dSEND.out",streamID);
 
//#ifdef TRACEME
  if (typeFEC == NO_FEC)
    printf("AppFecAgent:init(%lf):  streamID %d, NO FEC \n", 
       Scheduler::instance().clock(),streamID);
  else
    printf("AppFecAgent:init(%lf):  streamID %d, N:%d, K:%d, numberFECPackets:%d \n", 
       Scheduler::instance().clock(),streamID,N_,K_,numberFECPackets);
//#endif
}

int AppFecAgent::command(int argc, const char*const* argv)
{
#ifdef TRACEME
  printf("AppFecAgent:(%lf): command:  #Params:%d,   argv command:%s \n", Scheduler::instance().clock(),argc,argv[1]);
#endif
	if (argc == 5) {
        if (strcmp(argv[1], "initstream") == 0) {
            init(atoi(argv[2]), atoi(argv[3]), atoi(argv[4]));
			return (TCL_OK);
        }
    }
	return (Agent::command(argc, argv));
}

void AppFecAgent::blockTimerHandler(int streamIDParam)
{
#ifdef TRACEME
  printf("AppFecAgent:blockTimerHandler(%lf):(%d) streamIDParam: %d\n", Scheduler::instance().clock(), streamID,streamIDParam);
#endif
}

void AppFecAgent::dumpFinalStats(char *outputFile)
{
#ifdef TRACEME
  printf("AppFecAgent:dumpFinalStats(%f):(%d) dump to file  %s\n", Scheduler::instance().clock(), streamID,outputFile);
#endif
}


void AppFecBlockTimer::expire(Event* e) {

  a_->blockTimerHandler(0);

}

        


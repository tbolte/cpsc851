// Copyright (c) 2000 by the University of Southern California
// All rights reserved.
//
// Permission to use, copy, modify, and distribute this software and its
// documentation in source and binary forms for non-commercial purposes
// and without fee is hereby granted, provided that the above copyright
// notice appear in all copies and that both the copyright notice and
// this permission notice appear in supporting documentation. and that
// any documentation, advertising materials, and other materials related
// to such distribution and use acknowledge that the software was
// developed by the University of Southern California, Information
// Sciences Institute.  The name of the University may not be used to
// endorse or promote products derived from this software without
// specific prior written permission.
//
// THE UNIVERSITY OF SOUTHERN CALIFORNIA makes no representations about
// the suitability of this software for any purpose.  THIS SOFTWARE IS
// PROVIDED "AS IS" AND WITHOUT ANY EXPRESS OR IMPLIED WARRANTIES,
// INCLUDING, WITHOUT LIMITATION, THE IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
//
// Other copyrights might apply to parts of this software and are so
// noted when applicable.
//
// $Header: /nfs/jade/vint/CVSROOT/ns-2/apps/ping.cc,v 1.5 2002/11/07 00:18:35 haldar Exp $

/*
 * File: Code for the Loss_mon  Agent Class for the ns
 *       network simulator
 * Author: Marc Greis (greis@cs.uni-bonn.de), May 1998
 *
 * IMPORTANT: Incase of any changes made to this file , 
 * tutorial/examples/ping.cc file (used in Greis' tutorial) should
 * be updated as well.
 */

/*
*   The Loss_Monitor was a CPSC854 assignment Fall 2004.  This is a portion of the 
*   program requirements:
*
The parameters for the application are: 
• messageSize:  Specifies the number of bytes of data added to the packet 
• burstSize: Specifies the number of packets in each burst 
• iterationDelay: Specifies the number of microseconds between bursts 
• burstInterpacketDelay: Specifies the number of microseconds between packet 
transmissions within a burst. 
 
The objective is for the server to compute several types of one-way loss rate statistics: 
• Total loss rate based on total drops divided by total packets sent. 
• Conditioned loss rate: the probability that given a first loss event within a 
block, at least 1 additional packet in that block is  also dropped. 
• Correlated loss rate:  given that one packet (e.g., with seqno j) is dropped in a 
burst, what is the probability that each of the next X’th sequential packets 
within the burst are dropped.   In other words, if there are 20 packets in a burst 
and we set X to 5, we want the conditioned loss rate for the 5 packets following 
the loss.  At the end of the test you should indicate the conditioned loss rate for 
each of the 5 packets. If the loss occurs at the end of the block (say at index 16 
through 20) then only update the correlated loss counters for those packets that 
remain.  This means, for this example, that the number of events involving the 
5’th packet following a loss will typically be few than the number of events 
involving the 1’st packet following a loss.  By looking at this data, we will get 
an assessment of how correlated the loss rate is over a specified number of lags. 

Revisions:
   3/20/2011 - changed tcl binding from Loss_mon to VOIP_mon.  
               TODO:  Change the class name and filename to VOIP_Mon_Class
 
*/

#include <cmath>
#include "loss_monitor.h"

//Uncomment to see debug printfs
#include "../mac/docsis/docsisDebug.h"
//#define TRACE_DEBUG 1
//#define TRACEME 1

//If this is defined we dump a record of each packet (lost or not) at the server side
//#define TRACEDATA 1
//Define this to create a Video trace file
//#define MAKEVIDEOTRACEDATA 1
//2 is normally the upstream
#define TRACED_LMId  2

#define CODEC_G711  1
#define CODEC_G729a 2

#define USE_CODEC   CODEC_G711

int hdr_loss_mon::offset_;
static class Loss_mon_HeaderClass : public PacketHeaderClass {
public:
//$A810
//	Loss_mon_HeaderClass() : PacketHeaderClass("PacketHeader/Loss_mon", 
	Loss_mon_HeaderClass() : PacketHeaderClass("PacketHeader/VOIP_mon", 
					      sizeof(hdr_loss_mon)) {
		bind_offset(&hdr_loss_mon::offset_);
	}
} class_Loss_mon_hdr;


static class Loss_mon_Class : public TclClass {
public:
//$A810
//	Loss_mon_Class() : TclClass("Agent/Loss_mon") {}
	Loss_mon_Class() : TclClass("Agent/VOIP_mon") {}
	TclObject* create(int, const char*const*) {
		return (new Loss_Monitor());
	}
} class_loss_mon;


Loss_Monitor::Loss_Monitor() : Agent(PT_LOSS_MON)
{
//  bind("packetSize_", &size_);
  bind("LMId", &LMId_);
    
  lastSentSnum = 0;
  lastRcvdSnum = 0;
  message_size=64;
  size_ = message_size;
  burst_size=1;
  echo_mode =0;
  last_arrival_at_server=0.0;
  numberLost = 0;
  numberDups = 0;
  numberOutOfOrder = 0;
  numberReceived = 0;
  numberSent= 0;
  out_file = NULL;
  VIDEO_out_file = NULL;
  tptr = VIDEO_FILE_NAME;
}

int Loss_Monitor::command(int argc, const char*const* argv)
{
  if (argc == 2) {
    if (strcmp(argv[1], "send") == 0) {
      // Create a new packet
      Packet *pkt;

      if(message_size!=0)
      	 pkt = allocpkt(message_size);
      else
      	 pkt = allocpkt();

      // Access the loss_mon header for the new packet:
      hdr_loss_mon* hdr = hdr_loss_mon::access(pkt);
      // Set the 'ret' field to 0, so the receiving node
      // knows that it has to generate an echo packet
      hdr->ret = 0;



      // Store the current time in the 'send_time' field
      hdr->send_time = Scheduler::instance().clock();
      // Send the packet


      lastSentSnum++;
      hdr->snum = lastSentSnum; //the sequence number is starting from 1
      struct hdr_cmn* ch = HDR_CMN(pkt);        /* Common header part */
 

#ifdef TRACE_DEBUG
      tracePoint("TRACEME:command:send: ",lastSentSnum, LMId_);
#endif

#ifdef TRACEME
//      if (LMId_ == TRACED_LMId) 
       printf("TRACEME(%f):(LMId_:%d):send snum : %d (ch->size()=%d,message_size:%d\n)", 
             Scheduler::instance().clock(),LMId_,lastSentSnum,ch->size(),message_size);
#endif

      numberSent++;
      send(pkt, 0);
      // return TCL_OK, so the calling function knows that
      // the command has been processed
      return (TCL_OK);
    }
    else if(strcmp(argv[1],"close-file")==0){
      fclose(out_file);
      fclose(VIDEO_out_file);
      return(TCL_OK);
    }
  }
  else if(argc==3)
  {
    if(strcmp(argv[1],"data-size")==0){
      message_size=atol(argv[2]);
      size_ = message_size;
      return(TCL_OK);
    }
    else if(strcmp(argv[1], "burst-size")==0){
      burst_size=atoi(argv[2]);
      return(TCL_OK);
    }
    else if(strcmp(argv[1], "echo-mode")==0){
      echo_mode=atoi(argv[2]);
      return(TCL_OK);
    }
    else if(strcmp(argv[1], "set-outfile")==0){
      out_file=fopen(argv[2],"w");
#ifdef MAKEVIDEOTRACEDATA 
//      sprintf(tptr,"LMVideoTrace%dArrived.out",LMId_);
      sprintf(tptr,"VOIPTrace%dArrived.out",LMId_);
      VIDEO_out_file=fopen(tptr,"w");
#endif
      return(TCL_OK);
    }
    else if(strcmp(argv[1], "dumpresults")==0){
      int x=atoi(argv[2]);
      dumpLossStats(x);
      return(TCL_OK);
    }
   }
  // If the command hasn't been processed by Loss_Monitor()::command,
  // call the command() function for the base class
  return (Agent::command(argc, argv));
}


void Loss_Monitor::recv(Packet* pkt, Handler*)
{
  double recvTime = Scheduler::instance( ).clock( );
  // Access the IP header for the received packet:
  int loopCount = 0;

  numberReceived++;

  struct hdr_cmn* ch = HDR_CMN(pkt);
  hdr_ip* hdrip = hdr_ip::access(pkt);

  // Access the loss_mon header for the received packet:
  hdr_loss_mon* hdr = hdr_loss_mon::access(pkt);
  
#ifdef TRACE_DEBUG
      tracePoint("TRACEME:recv: ",lastRcvdSnum, LMId_);
#endif

#ifdef TRACEME
     printf( "TRACEME::recv(%f):(LMid:%d) packet arrived:(ret field:%d)  snum:: %d (lastRxSnum:%d)  size:%d, send Time:%lf \n", 
          recvTime, LMId_, hdr->ret, hdr->snum, lastRcvdSnum, ch->size(),hdr->send_time);
#endif

  // Is the 'ret' field = 0  then this pkt has gone one way so far
  if (hdr->ret == 0) {
    // Send an 'echo'. First save the old packet's send_time
    double stime = hdr->send_time;
    int snum = hdr->snum;
    double thisLatency;


    if (snum  <= lastRcvdSnum) {
      if (snum  == lastRcvdSnum) {
        numberDups++;
#ifdef TRACEME
         printf( "TRACEME::recv(%f):(LMid:%d) packet arrived DUPLICATE :(ret field:%d)  snum:: %d (lastRxSnum:%d)  size:%d, send Time:%lf \n", 
          recvTime, LMId_, hdr->ret, snum, lastRcvdSnum, ch->size(),hdr->send_time);
#endif
      }
      else {
        numberOutOfOrder++;
#ifdef TRACEME
         printf( "TRACEME::recv(%f):(LMid:%d) packet arrived OUT OF ORDER :(ret field:%d)  snum:: %d (lastRxSnum:%d)  size:%d, send Time:%lf \n", 
          recvTime, LMId_, hdr->ret, snum, lastRcvdSnum, ch->size(),hdr->send_time);
#endif
      }
    }
    else  {
    loopCount = 0;
    while(1) {

      loopCount++;
#ifdef TRACEME
       printf( "TRACEME::recv(%f):(LMid:%d)  (ret:%d) Iterate .... pktSnum:%d, pktSize:%d, lastRcvdSnum:%d (loopCount=%d)\n", 
          recvTime, LMId_, hdr->ret, snum, ch->size(),lastRcvdSnum,loopCount);
#endif

      if (loopCount > 1000) {
         printf( "TRACEME::recv(%f):(LMid:%d)  (ret:%d) TRACEME HARD ERROR  lastRcvdSnum:%d lastSentSnum:%d, pkt size:%d  (loopCount=%d)\n", 
          recvTime, LMId_, hdr->ret, lastRcvdSnum,lastSentSnum,ch->size(),loopCount);
//         exit(1);

           lastRcvdSnum = snum;
           *seq_iter=ARRIVED;
           seq_iter++;
           last_arrival_at_server = recvTime;
	       thisLatency = recvTime - ch->timestamp( );
           latency.push_back( thisLatency );
           break;
      }

      if( ( lastRcvdSnum % burst_size ) == 0 ) {
#ifdef TRACEME
         printf( "TRACEME::recv(%f):(LMid:%d)  (ret:%d) TRACEME NEW BURST  (modulo %d)  lastRcvdSnum:%d lastSentSnum:%d, pkt size:%d  (loopCount=%d)\n", 
          recvTime, LMId_, hdr->ret, burst_size,lastRcvdSnum,lastSentSnum,ch->size(),loopCount);
#endif
        push_vector();
        burst_iter=bursts.end();
        seq_iter=(*(--burst_iter)).begin();//starting pointer to the current vec
      }
    
      lastRcvdSnum++;

      if( lastRcvdSnum == snum ) {
        *seq_iter=ARRIVED;
        seq_iter++;
        last_arrival_at_server = recvTime;
	    thisLatency = recvTime - ch->timestamp( );
        latency.push_back( thisLatency );

#ifdef MAKEVIDEOTRACEDATA
//	if (LMId_ == TRACED_LMId) {
// The appfec trace     fprintf(fp,"%f %d %d %d %d %d %d %d %d %d %d %d %d %d %d\n", now, streamID, rh->contentType, rh->seqno(),rh->rawSeqNo_,APFEChighestRawSeqNo,h->size(),totalPacketsRxed, lossCount, rh->blockno_,rh->blockIndex_,rh->N_,rh->K_,numberPacketsInBlock,numberBytesInBlock);
      fprintf(VIDEO_out_file,"%f %d %d %d %d %d %d %d %d %d %d %d %d %d %d %3.6f\n", recvTime, LMId_, 0, snum, snum,lastRcvdSnum, ch->size(),numberReceived, numberLost, 0 ,0 ,0 ,0 ,0, 0, thisLatency);
//      }
#endif

#ifdef TRACEDATA
//	if (LMId_ == TRACED_LMId) {
          fprintf(out_file,"%10f %10d %10d %10.8f\n",last_arrival_at_server, lastRcvdSnum, 0, thisLatency); 
          fflush(out_file);
//        }
#endif
        break;
      }
      else
      {
        *seq_iter=LOST;
        //actualy, this is the number out of order.  
        numberLost++;
#ifdef MAKEVIDEOTRACEDATA
//	if (LMId_ == TRACED_LMId) {
// The appfec trace     fprintf(fp,"%f %d %d %d %d %d %d %d %d %d %d %d %d %d %d\n", now, streamID, rh->contentType, rh->seqno(),rh->rawSeqNo_,APFEChighestRawSeqNo,h->size(),totalPacketsRxed, lossCount, rh->blockno_,rh->blockIndex_,rh->N_,rh->K_,numberPacketsInBlock,numberBytesInBlock);
      fprintf(VIDEO_out_file,"%f %d %d %d %d %d %d %d %d %d %d %d %d %d %d %3.6f\n", recvTime, LMId_, 0, snum, snum,lastRcvdSnum, ch->size(),numberReceived, numberLost, 0 ,0 ,0 ,0 ,0, 0, 1);
//      }
#endif

#ifdef TRACEDATA
//	if (LMId_ == TRACED_LMId) {
          fprintf( out_file, "%10f %10d %10d 0\n", ( (last_arrival_at_server + recvTime)/2 ), lastRcvdSnum, 1);
          fflush(out_file);
//        }
#endif
         seq_iter++;
//$A510
//        break;
      }
    }
    }

    // Discard the packet
    Packet::free(pkt);

    if (echo_mode == 1) {
     // Create a new packet
     Packet *pktret;
     if(message_size!=0)
   	  pktret = allocpkt(message_size);
     else
   	  pktret = allocpkt();

      // Access the Loss_Monitor header for the new packet:
      hdr_loss_mon* hdrret = hdr_loss_mon::access(pktret);
      // Set the 'ret' field to 1, so the receiver won't send
      // another echo
      hdrret->ret = 1;
      // Set the send_time field to the correct value
      hdrret->send_time = stime;

      hdrret->snum = snum;
#ifdef TRACEME
//      if (LMId_ == TRACED_LMId) 
        printf( "TRACEME(%f):return echo with snum : %d and size:%d\n", recvTime, snum, ch->size() );
#endif

      // Send the packet
      send(pktret, 0);
    }
  } else {
    // A packet was received by the sender. Use tcl.eval to call the Tcl
    // interpreter with the ping results.
    // Note: In the Tcl code, a procedure
    // 'Agent/Loss_Mon recv {from rtt}' has to be defined which
    // allows the user to react to the ping result.
    char out[100];
    // Prepare the output to the Tcl interpreter. Calculate the
    // round trip time

    sprintf(out, "%s recv %d %d %3.1f %d", name(),
            hdrip->src_.addr_ >> Address::instance().NodeShift_[1], hdr->snum,
            (Scheduler::instance().clock()-hdr->send_time) * 1000, LMId_);

    Tcl& tcl = Tcl::instance();
#ifdef TRACEME
//      if (LMId_ == TRACED_LMId) 
        printf("TRACEME(%f): got echo with snum : %d (LMId_=%d and TRACED_LMId=%d\n", Scheduler::instance().clock(),hdr->snum,LMId_,TRACED_LMId);
#endif
    tcl.eval(out);
    // Discard the packet
    Packet::free(pkt);
  }
}

void Loss_Monitor ::  push_vector()
{
//changes on 09202005 initialized the vector before pushing
  vector<int> v(burst_size, NOT_SENT);
  bursts.push_back(v);
}

/************************************************************************
*
* void Loss_Monitor::dumpLossStats(int Xparm)
*
* params:
*    Xparm:  This is the X param for the correlated loss metric   
*   
* This method dumps the results from the monitor.  A complete record of
* the packets (arrived or not arrived) is in the bursts Vector.
*
* The tracefile output:
*   F1 F2 F3 F4 F5 F6  F6+1 F6+2 .... F6+Xparm
*   Note that the number of fields depends on the Xparm
*
* F1: Total pkts
* F2: Total drops
* F3: Total drop rate : F1/F2
*
* F4: Bursts with Loss
* F5: Bursts with 2 or more losses
* F6: Conditioned LR F5 / F4
*
* F(6+1) through F(6+Xparm) : The individual Cond loss rates
*    for each lag after a first loss event.
*
***********************************************************************/
void Loss_Monitor::dumpLossStats( int Xparm ) 
{
  double  F1 = 0.0;
  double  F2 = 0.0;
  double  F3 = 0.0;
  double  F4 = 0.0;
  double  F5 = 0.0;
  double  F6 = 0.0;
  // It looks like these are not used...delete?
  double  F7 = 0.0;
  double  F8 = 0.0;
  double  F9 = 0.0;
  double F10 = 0.0;
  double F11 = 0.0;

  int total_pkts = 0;
  int lost_pkts  = 0;

  double currTime = Scheduler::instance( ).clock( ); 

//#ifdef TRACEME
//  if (LMId_ == TRACED_LMId) 
   printf("TRACEME:dumpLossStats(%f): stats for LMId:%d: numberSent:%d, numberReceived:%d, numberLost:%d, numberDups:%d, numberOutOfOrder:%d \n", 
       currTime, LMId_, numberSent, numberReceived, numberLost, numberDups, numberOutOfOrder);
   printf("             size of data vector:  #bursts:%d, burst size:%d \n", 
       burst_size,bursts.size());
//#endif


  for( int rows = 0; rows < bursts.size( ); rows++ ) {
    for( int col = 0; col < burst_size; col++ ) {
      if(bursts[rows][col]!=NOT_SENT) {
        total_pkts++;
      }
      if(bursts[rows][col]==LOST) {
          lost_pkts++;
      }
    }
  }

  F1 = (double)total_pkts;
  F2 = (double)lost_pkts;
  if( total_pkts != 0 ) {
    F3 = F2 / F1;
  }

//#ifdef TRACEME
//  if (LMId_ == TRACED_LMId) 
  if (total_pkts > 0) {
    printf("TRACEME:dumpLossStats(%f): total lost pkts:%f, total pkts:%f, loss rate:%f \n", currTime,(double)lost_pkts,(double)total_pkts,(double)lost_pkts/(double)total_pkts);
  }
  else {
    printf("TRACEME:dumpLossStats(%f): total lost pkts:%f, total pkts:%f, loss rate:0 \n", currTime,(double)lost_pkts,(double)total_pkts);
  }
//#endif
  //
  // Calculating Conditioned loss rate
  //
  int num_of_losses = 0;
  int snum          = 1;
  double bursts_with_loss         = 0.0;
  double bursts_with_2ormore_loss = 0.0;

 for(burst_iter=bursts.begin();burst_iter!=bursts.end();burst_iter++)
 {
     for(seq_iter=(*burst_iter).begin();seq_iter!=(*burst_iter).end();seq_iter++)
     {
         if(*seq_iter==LOST) //this packet is lost
         {
           num_of_losses++;
#ifdef TRACEME
//         if (LMId_ == TRACED_LMId) 
            printf("TRACEME:dumpLossStats(%f):The lost snum : %d\n", currTime,snum);
#endif
         }
         snum++;
     }
     if(num_of_losses>1)
     {
         bursts_with_loss++;
         bursts_with_2ormore_loss++;
     }
     else if(num_of_losses==1)
         bursts_with_loss++;
     num_of_losses=0;
  }
  double cond_loss_rate=0;
  if(bursts_with_loss!=0)
    cond_loss_rate=bursts_with_2ormore_loss/bursts_with_loss;

  F4 = bursts_with_loss;
  F5 = bursts_with_2ormore_loss;
  F6 = cond_loss_rate;

#ifdef TRACEME
//  if (LMId_ == TRACED_LMId) 
   printf("TRACEME:dumpLossStats(%f): bursts with loss:%f, bursts with >1 loss:%f, CLR:\n", currTime,F4,F5,F6);
#endif


  fprintf(out_file,"%f %f %f %f %f %f %f ",currTime,F1,F2,F3,F4,F5,F6);
  fflush(out_file);

int x=Xparm;
int prev_pkt=ARRIVED, curr_pkt=ARRIVED;
double first_loss=0,corel_loss=0;
double CorLR = 0;  

  for(int var=1;var<=x;var++)
  {
    for(int rows=0;rows<bursts.size();rows++)
    {
      curr_pkt=ARRIVED;
      for(int col=0;col<burst_size-var;col++)
      {
        prev_pkt=curr_pkt;
        curr_pkt=bursts[rows][col];
        if(prev_pkt==ARRIVED && curr_pkt==LOST)
        {
          first_loss++;
          if(bursts[rows][col+var]==LOST)
          {
            corel_loss++;
            for(int index1=0;index1<=var;index1++)
            {
              if(bursts[rows][col+index1]==ARRIVED) 
              {
                corel_loss--;
                break;
              }
            }
          }
        }
      }
    }

    if (first_loss >0)
      CorLR = corel_loss/first_loss;
    else
      CorLR = 0;

    fprintf(out_file,"%f ",CorLR);
    fflush(out_file);

#ifdef TRACEME
//    if (LMId_ == TRACED_LMId) 
        if (first_loss > 0)
             printf("TRACEME:dumpLossStats(%f):For X:%d,number losses in this lag:%f, TotalFirst losses:%f, Corr LR:%f \n",
                currTime,var,corel_loss,first_loss,corel_loss/first_loss);
        else
             printf("TRACEME:dumpLossStats(%f):For X:%d,number losses in this lag:%f, TotalFirst losses:%f, Corr LR:0 \n",
                currTime,var,corel_loss,first_loss);
#endif

    first_loss=corel_loss=0;
  }

  int voipCount = latency.size( );
  int voipFail = 0;
  double voipLossRate = 0.0;
  double sumLatency   = 0.0;
  double avgLatency   = 0.0;
  double lossRate     = F3;
  double edj          = 0.0; 
  double avgLat_ms    = 0.0;
  double i_D;
  double i_EF = 0.0;
  double R_value, MOS_value; 
  double LR_2  = cond_loss_rate;
  if( voipCount > 1 ) {
    double currVal = 0.0;
    vector<double>::iterator iter = latency.begin( );
    double prevVal = *iter;
    sumLatency += prevVal;
    iter++;
//We assume a static dejitter buffer of 12 * .1 second chunks of voice data.
// So if a VoIP packet arrives with an interarrival delay > 120/2 seconds,
// we assume it has arrived too late.
    for( ; iter != latency.end( ); iter++ ) {
      currVal = *iter;
      sumLatency += currVal;
      if( currVal > ( prevVal + 0.060 ) ) {
        voipFail++;
      }
      prevVal = currVal;
    }

  } // End of if( there are latency lags to process )

  if( voipCount > 0 ) {
    avgLatency = sumLatency / voipCount;
    avgLat_ms = 1000.0 * avgLatency;


    i_D = 0.024 *  avgLat_ms;
    double excess = avgLat_ms - 177.3;
    if( excess > 0.0 ) {
      i_D += 0.11 * excess;
    }

    edj = voipFail / F1;
    double expon = lossRate + ( ( 1.0 - lossRate ) * edj );
    int codecType = USE_CODEC;

//Need to experiment with this....but basic behavior is that
// there is some bursty loss thrshold where perf.
//   experiences a single 'step' down 
//  I want to expose a param that allow this "knob" to be adjusted...
    double BurstyLossFactor = 1;
    if (LR_2 > .5) {
      BurstyLossFactor = 1.3;
      if (LR_2 > .85) {
        BurstyLossFactor = 1.8;
      }
    }
//For now, do not do this...
    BurstyLossFactor = 1;


    switch( codecType ) {
    case CODEC_G711:
      i_EF = BurstyLossFactor * (( LR_2 * 19.0 * log( 1.0 + 70 * expon ) ) + 
             ( (1.0 - LR_2) * 30.0 * log( 1.0 + 15.0 * expon ) ) ); 
#ifdef TRACEME
      printf("loss_monitor:dumpLossStats(%lf) :  CODEC_G711: LR=%lf,  Factor:%lf,  iEF:%lf \n",currTime,LR_2, BurstyLossFactor,i_EF);
#endif
      break;
    case CODEC_G729a:
      i_EF = BurstyLossFactor * (11.0 + ( LR_2 * 19.0 * log( 1.0 + 70 * expon ) ) + 
                    ( (1.0 - LR_2) * 40.0 * log( 1.0 + 10.0 * expon ) ) );
#ifdef TRACEME
      printf("loss_monitor:dumpLossStats(%lf) :  CODEC_G729a: LR=%lf,  Factor:%lf,  iEF:%lf \n",currTime,LR_2, BurstyLossFactor,i_EF);
#endif
      break;
    default:
      break;
    } // End of switch( codecType ) block


    R_value = 94.2 - i_D - i_EF;

    // MOS calculation is from Equation 1 in Cole paper
    if( R_value < 0.0 ) {
      R_value = 0;
      MOS_value = 1.0;
    }
    else if( R_value > 100.0 ) {
      MOS_value = 4.5;
    }
    else {
      MOS_value = 1.0 + 0.035 * R_value + 0.000007 * R_value * ( R_value - 60.0 ) * ( 100 - R_value );
    }
  }

//  fprintf( out_file, "\n    VOIP perf. data: %6d %10.8f %6d %10.8f %10.8f %10.7f %10.7f %10.7f %10.7f\n", 
//           voipCount, avgLatency, voipFail, lossRate, edj, i_D, i_EF, R_value, MOS_value );
  fprintf( out_file, "%6d %10.8f %6d %10.8f %10.8f %10.7f %10.7f %10.7f %10.7f \n", 
           voipCount, avgLatency, voipFail, lossRate, edj, i_D, i_EF, R_value, MOS_value );
  fflush(out_file);

} // End of dumpLossStats( ) method


//$A510
void Loss_Monitor::tracePoint(char *s1, int param1, int param2)
{
  FILE* fp = NULL;
  double curTime= Scheduler::instance().clock();
  char traceString[256];
  char *tptr = traceString;


  sprintf(tptr,"%lf %s %d %d ",curTime,s1,param1,param2);
  fp = fopen("tracePoint.out", "a+");
  fprintf(fp,"%s\n",tptr);
  fflush(stdout);
  fclose(fp);
}




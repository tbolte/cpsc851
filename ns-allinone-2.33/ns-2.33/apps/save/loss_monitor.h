// Copyright (c) 2000 by the University of Southern California
// All rights reserved.
//
// Permission to use, copy, modify, and distribute this software and its
// documentation in source and binary forms for non-commercial purposes
// and without fee is hereby granted, provided that the above copyright
// notice appear in all copies and that both the copyright notice and
// this permission notice appear in supporting documentation. and that
// any documentation, advertising materials, and other materials related
// to such distribution and use acknowledge that the software was
// developed by the University of Southern California, Information
// Sciences Institute.  The name of the University may not be used to
// endorse or promote products derived from this software without
// specific prior written permission.
//
// THE UNIVERSITY OF SOUTHERN CALIFORNIA makes no representations about
// the suitability of this software for any purpose.  THIS SOFTWARE IS
// PROVIDED "AS IS" AND WITHOUT ANY EXPRESS OR IMPLIED WARRANTIES,
// INCLUDING, WITHOUT LIMITATION, THE IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
//
// Other copyrights might apply to parts of this software and are so
// noted when applicable.
//
// $Header: /nfs/jade/vint/CVSROOT/ns-2/apps/ping.h,v 1.3 2000/09/01 03:04:06 haoboy Exp $

/*
 * File: Header File for a new 'Ping' Agent Class for the ns
 *       network simulator
 * Author: Marc Greis (greis@cs.uni-bonn.de), May 1998
 *
 * IMPORTANT: Incase of any changes made to this file ,
 * tutorial/examples/ping.h  (used in Greis' tutorial) should
 * be updated as well.
 */


#ifndef ns_loss_mon_h
#define ns_loss_mon_h

//changes on 09202005
//#define TRACEDATA

//changes on 09202005 , we have 3 states for a packet
#define LOST 0
#define ARRIVED 1
#define NOT_SENT -2

#include "agent.h"
#include "tclcl.h"
#include "packet.h"
#include "address.h"
#include "ip.h"
#include <vector>

struct hdr_loss_mon {
  int ret;
  double send_time;
  int snum;

	// Header access methods
  static int offset_; // required by PacketHeaderManager
  inline static int& offset() { return offset_; }
  inline static hdr_loss_mon* access(const Packet* p) {
     return (hdr_loss_mon*) p->access(offset_);
   }
};

class Loss_Monitor : public Agent {
public:
	Loss_Monitor();
	virtual int command(int argc, const char*const* argv);
	virtual void recv(Packet*, Handler*);
    void tracePoint(char *s1, int param1, int param2);
	int LMId_;
    int lastSentSnum;
    int lastRcvdSnum; //used by server to keep track of the losses
    long message_size;
    int burst_size;
    int echo_mode;   //if true, we echo else it's one way
    double last_arrival_at_server; //this records the time at which the last packet arrived at server
    vector<vector<int> >::iterator burst_iter; //iterator to the vector
    vector<vector<int> > bursts; //keeps track of number of losses in each burst
    vector<int>::iterator seq_iter;

    vector<double> latency;

    void  push_vector(); //this function pushes an int vector onto the main vector
    FILE * out_file; //outout file to write the results
    FILE * VIDEO_out_file; //outout file to trace VIDEO packet arrivals 
    void dumpLossStats(int x);  //called by tcl script to get the results
    char VIDEO_FILE_NAME[32];
    char *tptr;
 
   int numberReceived;
   int numberSent;
   int numberLost;
   int numberDups;
   int numberOutOfOrder;
};

#endif // ns_loss_mon_h

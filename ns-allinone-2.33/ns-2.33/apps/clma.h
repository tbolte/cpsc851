// Copyright (c) 2000 by the University of Southern California
// All rights reserved.
//
// Permission to use, copy, modify, and distribute this software and its
// documentation in source and binary forms for non-commercial purposes
// and without fee is hereby granted, provided that the above copyright
// notice appear in all copies and that both the copyright notice and
// this permission notice appear in supporting documentation. and that
// any documentation, advertising materials, and other materials related
// to such distribution and use acknowledge that the software was
// developed by the University of Southern California, Information
// Sciences Institute.  The name of the University may not be used to
// endorse or promote products derived from this software without
// specific prior written permission.
//
// THE UNIVERSITY OF SOUTHERN CALIFORNIA makes no representations about
// the suitability of this software for any purpose.  THIS SOFTWARE IS
// PROVIDED "AS IS" AND WITHOUT ANY EXPRESS OR IMPLIED WARRANTIES,
// INCLUDING, WITHOUT LIMITATION, THE IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
//
// Other copyrights might apply to parts of this software and are so
// noted when applicable.
//
// $Header: /nfs/jade/vint/CVSROOT/ns-2/apps/ping.h,v 1.3 2000/09/01 03:04:06 haoboy Exp $

/*
 * File: 
 *
 *
 *  Revisions:
 *    $A100:  10-16-2011  Changed the server so that it monitors the loss rate per fixed time increments
*/


#ifndef ns_corr_loss_mon_h
#define ns_corr_loss_mon_h

//changes on 09202005
//#define TRACEDATA

//changes on 09202005 , we have 3 states for a packet
#define LOST 0
#define ARRIVED 1
#define NOT_SENT -1


#include "agent.h"
#include "tclcl.h"
#include "packet.h"
#include "address.h"
#include "ip.h"

class Correlated_Loss_Monitor;

class PacketTimer: public Handler {
public:
         PacketTimer(Correlated_Loss_Monitor* c) : agent(c) {}
         void handle(Event*);
private:
         Correlated_Loss_Monitor *agent;
         Event intr;
}; 

struct hdr_corr_loss_mon {
  double send_time;
  int snum;

	// Header access methods
  static int offset_; // required by PacketHeaderManager
  inline static int& offset() { return offset_; }
  inline static hdr_corr_loss_mon* access(const Packet* p) {
     return (hdr_corr_loss_mon*) p->access(offset_);
   }
};

class Correlated_Loss_Monitor : public Agent {
public:
    Correlated_Loss_Monitor();
    ~Correlated_Loss_Monitor();
    virtual int command(int argc, const char*const* argv);
    virtual void recv(Packet*, Handler*);
    int lastSentSnum;
    int lastRcvdSnum; //used by server to keep track of the losses
    long message_size;
    double send_rate;
    double packet_interval;
    long max_size_arrival_array;
    double last_arrival_at_server; //this records the time at which the last packet arrived at server
   
    int LMId_;
    int *packet_arrivals;          // keeps track of packet arrivals
    double *latency;               // keeps track of latency of each packet

    FILE * out_file; //outout file to write the results
    void sendPackets();     // Creates a CBR packet and transmits it
    void dumpLossStats();  //called by tcl script to get the results


    FILE * VIDEO_out_file; //outout file to trace VIDEO packet arrivals 
    char VIDEO_FILE_NAME[32];
    char *tptr;
 
    int numberReceived;
    int numberSent;
    int numberLost;
    int numberDups;
    int numberOutOfOrder;
    int stop_traffic;

//$A100
    FILE * LOSS_MONITOR_out_file; //outout file to trace  incremental LOSS stats
    char LOSS_MONITOR_FILE_NAME[32];
    char *LMptr;
    double  incrementSize;  //fixed inc size in seconds
    double  lastLMUpdate;  //last sample
    int  highestSNUMInInterval;  //highest snum observed
    int  lowestSNUMInInterval;  //lowest snum observed
    int  numberPktsReceivedInInterval;
    int  numberBytesReceivedInInterval;
    double latestLRSample;
    double latestBWSample;


protected:
    PacketTimer ptimer;
};

#endif // ns_corr_loss_mon_h

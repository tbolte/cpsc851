// Copyright (c) 2000 by the University of Southern California
// All rights reserved.
//
// Permission to use, copy, modify, and distribute this software and its
// documentation in source and binary forms for non-commercial purposes
// and without fee is hereby granted, provided that the above copyright
// notice appear in all copies and that both the copyright notice and
// this permission notice appear in supporting documentation. and that
// any documentation, advertising materials, and other materials related
// to such distribution and use acknowledge that the software was
// developed by the University of Southern California, Information
// Sciences Institute.  The name of the University may not be used to
// endorse or promote products derived from this software without
// specific prior written permission.
//
// THE UNIVERSITY OF SOUTHERN CALIFORNIA makes no representations about
// the suitability of this software for any purpose.  THIS SOFTWARE IS
// PROVIDED "AS IS" AND WITHOUT ANY EXPRESS OR IMPLIED WARRANTIES,
// INCLUDING, WITHOUT LIMITATION, THE IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
//
// Other copyrights might apply to parts of this software and are so
// noted when applicable.
//
// $Header: /nfs/jade/vint/CVSROOT/ns-2/apps/ping.cc,v 1.5 2002/11/07 00:18:35 haldar Exp $

/*
 * File: 
*   The Corr_Loss_Monitor was a CPSC854 assignment Spring 2011.  This is a portion of the 
*   program requirements:
*
The parameters for the application are: 
messageSize:  Specifies the number of bytes of data added to the packet 
bsend_rate: Specifies the rate at which packets are to be generated
packet_interval: Specifies the number of seconds between packets
max_arrival_array_size: Specifies the maximum number of packets that are to be generated
 
The objective is for the server to compute several types of one-way loss rate statistics: 
Total loss rate - based on total drops divided by total packets sent. 
Average Latency - average of all latencies experienced by each packet
MBL - Average Maximum Burst Length corresponding to packet drops (or Average Bad Run Length)
MILD - Average Interloss Distance between packet drops (or Average Good Run Length)
*
*  Revisions:
*    $A100:  10-16-2011  Changed the server so that it monitors the loss rate per fixed time increments
*             Also changed the default size of the arrays to something much larger (MAX_ARRAY_SIZE)
*/

#include <cmath>
#include "clma.h"

//#define TRACEME 1

//Define this to create a Video trace file
//#define MAKEVIDEOTRACEDATA 1

//Set this to 1 to log the fixed increment loss monitor (i.e., each increment we observed this loss rate...)
//#define  MAKELOSSMONITORTRACEDATA 1
#define FIXED_SAMPLE_INCREMENT 1.0


int hdr_corr_loss_mon::offset_;
static class Correlated_Loss_Mon_HeaderClass : public PacketHeaderClass {
public:
	Correlated_Loss_Mon_HeaderClass() : PacketHeaderClass("PacketHeader/Corr_Loss_Mon", sizeof(hdr_corr_loss_mon)) {
		bind_offset(&hdr_corr_loss_mon::offset_);
	}
} class_Correlated_Loss_Mon_hdr;


static class Corr_Loss_Mon_Class : public TclClass {
public:
	Corr_Loss_Mon_Class() : TclClass("Agent/Corr_Loss_Mon") {}
	TclObject* create(int, const char*const*) {
		return (new Correlated_Loss_Monitor());
	}
} class_Correlated_Loss_Mon;

Correlated_Loss_Monitor::~Correlated_Loss_Monitor() 
{
  fclose(VIDEO_out_file);
  fclose(out_file);
}


Correlated_Loss_Monitor::Correlated_Loss_Monitor() : Agent(PT_CORR_LOSS_MON),
                                                     ptimer(this) {
  lastSentSnum = 0;
  lastRcvdSnum = 0;
  message_size=1400;
  send_rate= 224.0;
  size_ = message_size;
  packet_interval = (message_size*8.0)/send_rate;
  max_size_arrival_array = 1000;
  packet_arrivals = new int[max_size_arrival_array+1];
  latency = new double[max_size_arrival_array+1];
  for (int i = 0; i < max_size_arrival_array+1; i++) {
      packet_arrivals[i] = NOT_SENT;
      latency[i] = 0.0;
  }
  last_arrival_at_server=0.0;
  numberLost = 0;
  numberDups = 0;
  numberOutOfOrder = 0;
  numberReceived = 0;
  numberSent= 0;
  stop_traffic = 0;
  LMId_ = 1;
  out_file = NULL;
  VIDEO_out_file = NULL;
  tptr = VIDEO_FILE_NAME;
//$A100
  LOSS_MONITOR_out_file = NULL;
  LMptr = LOSS_MONITOR_FILE_NAME;
  incrementSize = FIXED_SAMPLE_INCREMENT;
  lastLMUpdate = -1.0;  //last sample
  highestSNUMInInterval = 0;  //highest snum observed
  lowestSNUMInInterval = 0;  //lowest snum observed
  numberPktsReceivedInInterval = 0;
  numberBytesReceivedInInterval = 0;
  latestLRSample = 0.0;
  latestBWSample = 0.0;
}

void PacketTimer::handle(Event*) {
  agent->sendPackets();
  Scheduler::instance().schedule(this, &intr, agent->packet_interval);
}

int Correlated_Loss_Monitor::command(int argc, const char*const* argv)
{
  if (argc == 2) {
    if (strcmp(argv[1], "start") == 0) {
       ptimer.handle((Event*) 0);
       return (TCL_OK);
    }
    if (strcmp(argv[1], "stop") == 0) {
       stop_traffic = 1;
       return (TCL_OK);
    }
    else if(strcmp(argv[1],"close-file")==0){
//Move to destructor
//      fclose(out_file);
      return(TCL_OK);
    }
    else if(strcmp(argv[1], "dumpresults")==0){
      dumpLossStats();
      return(TCL_OK);
    }
  }
  else if(argc==3)
  {
    if(strcmp(argv[1],"data-size")==0){
      message_size=atol(argv[2]);
      size_ = message_size;
      packet_interval = (message_size*8.0)/send_rate;
      return(TCL_OK);
    }
    else if(strcmp(argv[1],"send-rate")==0){
      send_rate=atof(argv[2]);
      packet_interval = (message_size*8.0)/send_rate;
      return(TCL_OK);
    }
    else if(strcmp(argv[1],"max-size-arrival-array")==0) {
      //Rahul  - maybe free the previous array  and rebuild a new one?
      int old_array_size = max_size_arrival_array+1;
      max_size_arrival_array=atol(argv[2]);
      int *temp_array1 = new int[max_size_arrival_array+1];
      double *temp_array2 = new double[max_size_arrival_array+1];
      for (int k = 0; k < old_array_size; k++) {
          temp_array1[k] = packet_arrivals[k];
          temp_array2[k] = latency[k];
      }
      delete [] packet_arrivals;
      delete [] latency;
      packet_arrivals = temp_array1;
      latency = temp_array2;
      for (int i = old_array_size; i < max_size_arrival_array+1; i++) {
          packet_arrivals[i] = NOT_SENT;
          latency[i] = 0.0;
      }
      return(TCL_OK);
    }
    else if(strcmp(argv[1], "set-outfile")==0){
      out_file=fopen(argv[2],"w");
#ifdef MAKEVIDEOTRACEDATA 
      sprintf(tptr,"LMVideoTrace%dArrived.out",LMId_);
      VIDEO_out_file=fopen(tptr,"w");
//      VIDEO_out_file=fopen("CLMVideoTrace1Arrived.out","w");
#endif

#ifdef MAKELOSSMONITORTRACEDATA 
//Make this APFECCLMATRACE.out if want this in a larger aggregate trace with other flows
//      sprintf(LMptr,"APFECCLMATRACE.out");
      sprintf(LMptr,"CLMATRACE%d.out",LMId_);
#endif

      return(TCL_OK);
    }
  }
  // If the command hasn't been processed by Correlated_Loss_Monitor()::command,
  // call the command() function for the base class
  return (Agent::command(argc, argv));
}


void Correlated_Loss_Monitor::sendPackets() {
   
   if (stop_traffic == 1)
   {
      return;
   }
   
   // Create a new packet
   Packet *pkt;

   if(message_size!=0)
      pkt = allocpkt(message_size);
   else
      pkt = allocpkt();

   // Access the loss_mon header for the new packet:
   hdr_corr_loss_mon* hdr = hdr_corr_loss_mon::access(pkt);
   struct hdr_cmn* ch = HDR_CMN(pkt);

   // Store the current time in the 'send_time' field
   hdr->send_time = Scheduler::instance().clock();

   lastSentSnum++;
   hdr->snum = lastSentSnum; //the sequence number is starting from 1

   numberSent++;
#ifdef TRACEME
   printf("Correlated_Loss_Monitor::sendPackets(%f):(LMId_:%d):send snum : %d (ch->size()=%d,message_size:%d)\n", 
           Scheduler::instance().clock(),LMId_,lastSentSnum,ch->size(),message_size);
#endif
   send(pkt, 0);
}
  
void Correlated_Loss_Monitor::recv(Packet* pkt, Handler*)
{
  double recvTime = Scheduler::instance().clock();
  // Access the IP header for the received packet:
  int loopCount = 0;

  numberReceived++;
  struct hdr_cmn* ch = HDR_CMN(pkt);
  hdr_ip* hdrip = hdr_ip::access(pkt);

  // Access the loss_mon header for the received packet:
  hdr_corr_loss_mon* hdr = hdr_corr_loss_mon::access(pkt);
  
  // Send an 'echo'. First save the old packet's send_time
  double stime = hdr->send_time;
  int snum = hdr->snum;
  double thisLatency;
//$A100
  if (lastLMUpdate == -1.0)
  {
    lastLMUpdate = recvTime;
    lowestSNUMInInterval = snum;
  }


#ifdef TRACEME
  printf( "Correlated_Loss_Monitor::recv(%f):(LMid:%d) packet arrived: snum:: %d (lastRxSnum:%d)  size:%d, send Time:%lf \n", 
          recvTime, LMId_,  snum, lastRcvdSnum, ch->size(),hdr->send_time);
#endif

//$A100
  if (snum > highestSNUMInInterval)
    highestSNUMInInterval = snum;
  if (snum < lowestSNUMInInterval)
    lowestSNUMInInterval = snum;

  numberPktsReceivedInInterval++;
  numberBytesReceivedInInterval = numberBytesReceivedInInterval + ch->size();
#ifdef TRACEME
    printf( "Correlated_Loss_Monitor::recv(%f):(LMid:%d) SNUM (%d)  lowestSNUMInInterval:%d, highest:%d, numberPkts:%d  \n", 
          recvTime, LMId_,  snum, lowestSNUMInInterval,highestSNUMInInterval,numberPkts);
#endif
  if ((recvTime - lastLMUpdate) > FIXED_SAMPLE_INCREMENT)
  {
    int expectedNumberPkts = highestSNUMInInterval - lowestSNUMInInterval +1;
    int numberLost = expectedNumberPkts-numberPktsReceivedInInterval;

    latestBWSample = ((double)numberBytesReceivedInInterval)*8 / (recvTime-lastLMUpdate);
    if (expectedNumberPkts > 0)
    {
      latestLRSample = ( (double)numberLost)/ ((double)expectedNumberPkts);
    } else
    {
      latestLRSample = 0.0;
    }
#ifdef TRACEME
    printf( "Correlated_Loss_Monitor::recv(%f):(LMid:%d) SNUM (%d)  LR SAMPLE: expectedNumberPkts:%d, Actual:%d, LR:%2.3f, BW:%9.0f  \n", 
          recvTime, LMId_,  snum, expectedNumberPkts,numberPktsReceivedInInterval,latestLRSample,latestBWSample);
#endif
#ifdef MAKELOSSMONITORTRACEDATA 
    LOSS_MONITOR_out_file=fopen(LMptr,"a+");
    fprintf(LOSS_MONITOR_out_file,"%f %d %2.6f %9.0f %d %d %d %d\n", recvTime, 1002, latestLRSample, latestBWSample, numberPktsReceivedInInterval,numberLost,lowestSNUMInInterval,highestSNUMInInterval);
    fclose(LOSS_MONITOR_out_file);
#endif
    //reset state for next interval
    lastLMUpdate = recvTime;
    numberPktsReceivedInInterval = 0;
    numberBytesReceivedInInterval = 0;
    lowestSNUMInInterval = (highestSNUMInInterval+1);
  }

  if (snum  >  max_size_arrival_array) {
    stop_traffic = 1;
#ifdef TRACEME
    printf( "Correlated_Loss_Monitor::recv(%f):(LMid:%d) SNUM (%d)  exceeds max_size_arrival_array (%d)  \n", 
          recvTime, LMId_,  snum, max_size_arrival_array);
#endif
    // Discard the packet
    Packet::free(pkt);
    return;
  }

  if (snum  <= lastRcvdSnum) {
     if (packet_arrivals[snum] == ARRIVED) {
        numberDups++;
#ifdef TRACEME
        printf( "Correlated_Loss_Monitor::recv(%f):(LMid:%d) DUPLICATE  packet arrived: snum:: %d (lastRxSnum:%d)  size:%d, send Time:%lf \n", 
          recvTime, LMId_,  snum, lastRcvdSnum, ch->size(),hdr->send_time);
#endif
     }
     else {
        numberOutOfOrder++;  // The packet has arrived out of order.
        numberLost--;        // We assumed this packet was lost. But now it has arrived.
        packet_arrivals[snum] = ARRIVED;
        thisLatency = recvTime - ch->timestamp();
        latency[snum] = thisLatency;
#ifdef TRACEME
        printf( "Correlated_Loss_Monitor::recv(%f):(LMid:%d) OUT OF ORDER packet arrived: snum:: %d (lastRxSnum:%d)  size:%d, send Time:%lf \n", 
          recvTime, LMId_,  snum, lastRcvdSnum, ch->size(),hdr->send_time);
#endif
     }
  }
  else {
     //else it's a new packet that advances lastRcvdSnum
     loopCount = 0;
#ifdef TRACEME
       printf( "Correlated_Loss_Monitor::recv(%f):(LMid:%d) NEW PACKET - difference from snum and lastRcvdSnum:%d  snum:: %d (lastRxSnum:%d)  size:%d, send Time:%lf \n", 
          recvTime, LMId_, (snum-lastRcvdSnum), snum, lastRcvdSnum, ch->size(),hdr->send_time);
#endif
     while(1) {
        loopCount++;
        if (loopCount > 1000) {
           printf( "CORRLOSSMON::recv(%f): CORRLOSSMON HARD ERROR snum:%d, lastRcvdSnum:%d lastSentSnum:%d, pkt size:%d  (loopCount=%d)\n", 
                    recvTime, snum, lastRcvdSnum, lastSentSnum, ch->size(), loopCount);
//           exit(1);
           lastRcvdSnum = snum;
           packet_arrivals[lastRcvdSnum]=ARRIVED;
           last_arrival_at_server = recvTime;
	       thisLatency = recvTime - ch->timestamp();
           latency[lastRcvdSnum] = thisLatency;
           break;
        }

        lastRcvdSnum++;

#ifdef TRACEME
       printf( "Correlated_Loss_Monitor::recv(%f):(LMid:%d) WHILE LOOP loopCount:%d, snum:%d, lasterRcvdSnum:%d \n", recvTime, LMId_, (snum-lastRcvdSnum), snum, lastRcvdSnum);
#endif
        if( lastRcvdSnum == snum ) {
           packet_arrivals[lastRcvdSnum]=ARRIVED;
           last_arrival_at_server = recvTime;
	       thisLatency = recvTime - ch->timestamp();
           latency[lastRcvdSnum] = thisLatency;
           break;
        }
        else
        {
           packet_arrivals[lastRcvdSnum]=LOST;
           //actually, this packet can possibly arrive later as an out of order packet.
           // But for now, we treat it as a lost packet. We fix this stat when the packet actually arrives.  
           numberLost++;
        }
     } // end while
   } // end else

#ifdef MAKEVIDEOTRACEDATA
//  if (LMId_ == TRACED_LMId) {
// The appfec trace     fprintf(fp,"%f %d %d %d %d %d %d %d %d %d %d %d %d %d %d\n", now, streamID, rh->contentType, rh->seqno(),rh->rawSeqNo_,APFEChighestRawSeqNo,h->size(),totalPacketsRxed, lossCount, rh->blockno_,rh->blockIndex_,rh->N_,rh->K_,numberPacketsInBlock,numberBytesInBlock);
      fprintf(VIDEO_out_file,"%f %d %d %d %d %d %d %d %d %d %d %d %d %d %d %3.6f\n", recvTime, LMId_, 0, snum, snum,lastRcvdSnum, ch->size(),numberReceived, numberLost, 0 ,0 ,0 ,0 ,0, 0, thisLatency);
//      }
#endif

    // Discard the packet
    Packet::free(pkt);
}

/************************************************************************
*
* void Correlated_Loss_Monitor::dumpLossStats()
*
* This method dumps the results from the monitor.  A complete record of
* the packets (arrived or not arrived) is in the packet_arrivals array.

* At this point, we stop the traffic generator.  It's either do this
* or cleanly reset the monitor state.
*
* $A900 -  we no longer assume max_array are required to arrive. 
*
***********************************************************************/
void Correlated_Loss_Monitor::dumpLossStats() 
{
  double  F1 = 0.0;
  double  F2 = 0.0;
  double  F3 = 0.0;

  int total_pkts = 0;
  int lost_pkts  = 0;

  double currTime = Scheduler::instance().clock(); 

  printf("CORRLOSSMON:dumpLossStats(%f): numberSent:%d, numberReceived:%d, numberLost:%d, numberDups:%d, numberOutOfOrder:%d \n", 
       currTime, numberLost+numberReceived, numberReceived, numberLost, numberDups, numberOutOfOrder);


//  total_pkts = max_size_arrival_array;

  //$A901
  //We do not want to do this as we do not properly allow packets in flight to arrive....
//  for (int i = 1; i < max_size_arrival_array+1; i++)
//  {
//      if (packet_arrivals[i] == NOT_SENT)
//          numberLost++;
//  }

//lost_pkts = numberLost;


  double totalLatency = 0.0;
  double avgLatency = 0.0;
  int counter = 0;
  for (int i = 1; i < max_size_arrival_array+1; i++)
  {
      if (latency[i] > 0.0)
      {
         totalLatency = totalLatency + latency[i];
         counter++;
      }
      if ( (packet_arrivals[i] == ARRIVED) || (packet_arrivals[i] == LOST)) 
      {
        total_pkts++;
      }
  }
  if (counter > 0)
  {
     avgLatency = totalLatency/counter;
  }

  int MBL[max_size_arrival_array+1];
  int MILD[max_size_arrival_array+1];
  int mbl_counter = 0;
  int mild_counter = 0;
  int uncounted=0;

  int finalArraySize = total_pkts;
  lost_pkts = numberLost;
  F1 = (double)total_pkts;
  F2 = (double)lost_pkts;
  if( total_pkts != 0 ) {
    F3 = F2 / F1;
  }

  if (total_pkts > 0) {
    printf("CORRLOSSMON:dumpLossStats(%f): total lost pkts:%f, total pkts:%f, loss rate:%f \n", currTime,F2,F1,F2/F1);
  }
  else {
    printf("CORRLOSSMON:dumpLossStats(%f): total lost pkts:%f, total pkts:%f, loss rate:0 \n", currTime,F2,F1);
  }

  for (int i = 0; i < max_size_arrival_array+1; i++)
  {
      MBL[i] = 0;
      MILD[i] = 0;
  }

  uncounted=0;
  for (int i = 1; i < max_size_arrival_array+1; i++)
  {
      if (packet_arrivals[i] == ARRIVED)
      {
         mild_counter++;
         if (mbl_counter != 0)
         {
            MBL[mbl_counter] = MBL[mbl_counter] + 1;
         }
         mbl_counter = 0;
      }
      else if (packet_arrivals[i] == LOST)
      {
         mbl_counter++;
         if (mild_counter != 0)
         {
            MILD[mild_counter] = MILD[mild_counter] + 1;
         }
         mild_counter = 0;
      }
      else 
      {
        uncounted++;
      }
   }

   if (mbl_counter != 0)
   {
      MBL[mbl_counter] = MBL[mbl_counter] + 1;
   }

   if (mild_counter != 0)
   {
      MILD[mild_counter] = MILD[mild_counter] + 1;
   }

   double MILD_numerator = 0.0;
   double MILD_denominator = 0.0;
   double MBL_numerator = 0.0;
   double MBL_denominator = 0.0;
   double MILDmetric = 0.0;
   double MBLmetric = 0.0;


   printf("CORRLOSSMON:dumpLossStats(%f): totalpkts:%d, finalArraySize:%d, configured max array size:%d, uncounted:%d \n", currTime,total_pkts,finalArraySize,max_size_arrival_array,uncounted);
   
   for (int i=1; i < max_size_arrival_array+1; i++)
   {
      MILD_numerator = MILD_numerator + (i * MILD[i]);
      MILD_denominator = MILD_denominator + MILD[i];
      MBL_numerator = MBL_numerator + (i * MBL[i]);
      MBL_denominator = MBL_denominator + MBL[i];
   }

   if (MILD_denominator > 0)
   {
      MILDmetric = (double) (MILD_numerator)/MILD_denominator;
   }

   if (MBL_denominator > 0)
   {
      MBLmetric = (double) (MBL_numerator)/MBL_denominator;
   }
 

   fprintf(out_file, "%d %d %d %d %d %10.8f %10.8f %10.8f %10.8f\n", numberLost+numberReceived, numberReceived, numberLost, numberDups, numberOutOfOrder, avgLatency, F2/F1, MBLmetric, MILDmetric);
   fflush(out_file);

   stop_traffic = 1;

//#ifdef TRACEME
   printf("CORRLOSSMETRIC::dumpLossStats(%f): LMID:%d :\t SUMMARY RESULTS total_pkts:%d, LR:%3.3f, MBL: %d %d %d %d %d %d %d %d %d %d %d %d \n", 
        currTime,LMId_, total_pkts,F3,  MBL[0], MBL[1], MBL[2], MBL[3], MBL[4], MBL[5], MBL[6], MBL[7], MBL[8], MBL[9], MBL[10], MBL[11]); 
//#endif

   /* Reset all the paramaters */
/*   
   lastSentSnum = 0;
   lastRcvdSnum = 0;
   for (int i = 0; i < max_size_arrival_array+1; i++) {
       packet_arrivals[i] = -1;
       latency[i] = 0;
   }
   last_arrival_at_server=0.0;
   numberLost = 0;
   numberDups = 0;
   numberOutOfOrder = 0;
   numberReceived = 0;
   numberSent= 0;
*/

} // End of dumpLossStats() method

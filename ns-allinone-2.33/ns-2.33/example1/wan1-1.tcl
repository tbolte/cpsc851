#
# File:  wan1-1.tcl
#
# Model:  simple 2 link WAN - designed so the bottleneck is n0->n2 
#
#                                      
#                 LINKSPEED1     LINKSPEED2  
#tcp1 sink ...  n2 ------    n0 ------------- n1 ... tcp1 source
#ping (echo)      PROPDELAY1     PROPDELAY2      ... ping (request)
#
#

#Invoke a new ns object
set ns [new Simulator]

exec rm -f xnetLR.out
exec rm -f thru1.out  
exec rm -f tcpsend1.out  
exec rm -f queue1.out
exec rm -f trafficxnet.out
exec rm -f TCPstats.out
exec rm -f tcpTrace.out
exec rm -f ping1.out
exec rm -f snumack.out


set QUEUECAPACITY 20
set LINKSPEED1 1500000
set LINKSPEED2 100000000
set PROPDELAY1 0.024
set PROPDELAY2 0.002

#Sets the max TCP send window
set WINDOW  20

#This contains a bunch of helper scripts 
source networks.tcl
source ping-helper.tcl
#PROTID
#   1: Reno
#   2: NewReno
#   8: Full TCP
#   9: TCP Sack1


#set the TCP protocol for the WRT metric cx
set WRTprotID 9

set numFlows [lindex $argv 0]

set n0 [$ns node]

for {set i 0} {$i < $numFlows} {incr i} {
	set startNodes($i) [$ns node]
	set endNodes($i) [$ns node]
	$ns duplex-link $n0 $startNodes($i) $LINKSPEED2 $PROPDELAY2 DropTail
	$ns duplex-link $n0 $endNodes($i) $LINKSPEED1 $PROPDELAY1 DropTail
}

###}

#set n1 [$ns node]
#set n2 [$ns node]

set stoptime 500.2
set printtime 500.1
set testname test-wan1-1


#$ns duplex-link $n0 $n1 $LINKSPEED2 $PROPDELAY2 DropTail
#$ns duplex-link $n0 $n2 $LINKSPEED1 $PROPDELAY1 DropTail

#Set the queue buffer size in packets
#$ns queue-limit $n2 $n0 $QUEUECAPACITY
#$ns queue-limit $n0 $n2 $QUEUECAPACITY


#The following creates an artificial packet loss process
#on all packets from n0 to n2.
#This does nothing if the rate_ is 0.00
set loss_modulexnet [new ErrorModel]
$loss_modulexnet set errModelId 2

# .03 is 3%
#$loss_modulexnet set rate_ 0.05
$loss_modulexnet set rate_ 0.00

$loss_modulexnet unit pkt
$loss_modulexnet ranvar [new RandomVariable/Uniform]
$loss_modulexnet drop-target [new Agent/Null]

for {set i 0} {$i < $numFlows} {incr i} {
	$ns lossmodel $loss_modulexnet $n0 $endNodes($i)
}

if { $WRTprotID == 1 } {
  puts "Create a Reno test flow.."
  set tcp1 [new Agent/TCP/Reno]
}
if { $WRTprotID == 2 } {
  puts "Create a NewReno test flow.."
   set tcp1 [new Agent/TCP/Newreno]
}
if { $WRTprotID == 3 } {
  puts "Create a VegasReno test flow.."
  set tcp1 [new Agent/TCP/Vegas]
}
if { $WRTprotID == 8 } {
  puts "Create a fullTCP test flow.."
  set sink1 [new Agent/TCP/FullTcp/Sack]
  set tcp1 [new Agent/TCP/FullTcp/Sack]
#  set sink1 [new Agent/TCP/FullTcp]
#  set tcp1 [new Agent/TCP/FullTcp]
}

if { $WRTprotID == 9 } {
  puts "Create a SACk test flow.."
   	for {set i 0} {$i < $numFlows} {incr i} {
		set tcp($i) [new Agent/TCP/Sack1]
}
#set tcp1 [new Agent/TCP/Sack1]
}


if {$WRTprotID != 8} {
  if { $WRTprotID == 9 } {
    puts "Create a Sack1/DelAck sink"
    	for {set i 0} {$i < $numFlows} {incr i} {
		set sink($i) [new Agent/TCPSink/Sack1/DelAck]
	}
    #set sink1 [new Agent/TCPSink/Sack1/DelAck]
  } else {
    #set sink1 [new Agent/TCPSink]
    	for {set i 0} {$i < $numFlows} {incr i} {
		set sink($i) [new Agent/TCPSink/DelAck]
	}
    #set sink1 [new Agent/TCPSink/DelAck]
  }
}


#$tcp1 set cxId_ 1
for {set i 0} {$i < $numFlows} {incr i} {
	$tcp($i) set cxId_ $i
}
#$sink1 set sinkId_ 1
for {set i 0} {$i < $numFlows} {incr i} {
	$sink($i) set sinkId_ $i
}

for {set i 0} {$i < $numFlows} {incr i} {
	$ns attach-agent $startNodes($i) $tcp($i)
	$ns attach-agent $endNodes($i) $sink($i)
	$ns connect $tcp($i) $sink($i)
	$tcp($i) set window_ [expr $WINDOW/$numFlows]
	$tcp($i) set maxcwnd_ [expr $WINDOW/$numFlows]
	$sink($i) set maxcwnd_ [expr $WINDOW/$numFlows]
	$sink($i) set window_ [expr $WINDOW/$numFlows]
	$tcp($i) set packetSize_ 1460
}
#$ns attach-agent $n1 $tcp1
#$ns attach-agent $n2 $sink1
#$ns connect $tcp1 $sink1

if {$WRTprotID == 8} {
  $sink1 listen
}

#$tcp1 set window_ $WINDOW
#$tcp1 set maxcwnd_ $WINDOW
#$sink1 set maxcwnd_ $WINDOW
#$sink1 set window_ $WINDOW


#$tcp1 set packetSize_ 1460

for {set i 0} {$i < $numFlows} {incr i} {
	set ftp($i) [new Application/FTP]
	$ftp($i) attach-agent $tcp($i)
	$ns at 1.2 "$ftp($i) start"
  #set ftp1 [new Application/FTP]
  #$ftp1 attach-agent $tcp1
  #$ns at 1.2 "$ftp1 start"
}

for {set i 0} {$i < $numFlows} {incr i} {
	TraceThroughput $ns $sink($i)  1.0 thru1.out
	TCPTraceSendRate $ns $tcp($i)  .1  tcpsend1.out
	$ns at $printtime "dumpFinalTCPStats  $i 1.0 $tcp($i)  $sink($i) TCPstats.out"
	TraceTCP $ns $tcp($i) .01 tcpTrace.out

	#TraceThroughput $ns $sink1  1.0 thru1.out
	#TCPTraceSendRate $ns $tcp1  .1  tcpsend1.out

	#$ns at $printtime "dumpFinalTCPStats  $i 1.0 $tcp1  $sink1 TCPstats.out"

	#See this script in networks.tcl.  We use it
	#to monitor internal TCP state variables such as the cwnd
	#TraceTCP $ns $tcp1 .01 tcpTrace.out
}

for {set i 0} {$i < $numFlows} {incr i} {
	set qmon($i) [$ns monitor-queue $n0 $endNodes($i) ""]
	set integ($i) [$qmon($i) get-bytes-integrator]
}
#set integ [$qmon get-pkts-integrator]


#Create two ping agents and attach them to the nodes n0 and n2
for {set i 0} {$i < $numFlows} {incr i} {
	set p0($i) [new Agent/Ping]
	$ns attach-agent $startNodes($i) $p0($i)

	set p1($i) [new Agent/Ping]
	$ns attach-agent $endNodes($i) $p1($i)

#Connect the two agents
	$ns connect $p0($i) $p1($i)

#get the ping process going ...
#$ns at 2 "$p0 send"

#can call startPingProcess which does a ping every interval
#or have the recv schedule doPing ....
	startPingProcess $ns $p0($i) .5

	$ns at $printtime "doPingStats"

	TraceQueueLossRate $ns xnetLR  $qmon($i) 20 xnetLR.out

	TraceQueueSize $ns $i  $n0 $endNodes($i) .1 queue1.out

	TraceQueueTraffic $ns $i  $n0 $endNodes($i) 1 trafficxnet.out $LINKSPEED1
}
$ns at $stoptime "finish"


proc finish {} {
global ns 
    $ns flush-trace
    exit 0
}

#Start the simulator
$ns run


exit 0



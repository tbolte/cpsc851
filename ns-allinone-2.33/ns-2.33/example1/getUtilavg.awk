#Change the number in the third paran to indicate the column in the data file to operate upon.
#If the columns are separated with tabs, change the first paran to "\t"
BEGIN { FS = " "} {nl++} {s=s+$3} END {print "average utilization: " s/nl}

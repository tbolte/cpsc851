/* -*-	Mode:C++; c-basic-offset:8; tab-width:8; indent-tabs-mode:t -*- */
/*
 * Copyright (c) Xerox Corporation 1997. All rights reserved.
 *  
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * Linking this file statically or dynamically with other modules is making
 * a combined work based on this file.  Thus, the terms and conditions of
 * the GNU General Public License cover the whole combination.
 *
 * In addition, as a special exception, the copyright holders of this file
 * give you permission to combine this file with free software programs or
 * libraries that are released under the GNU LGPL and with code included in
 * the standard release of ns-2 under the Apache 2.0 license or under
 * otherwise-compatible licenses with advertising requirements (or modified
 * versions of such code, with unchanged license).  You may copy and
 * distribute such a system following the terms of the GNU GPL for this
 * file and the licenses of the other code concerned, provided that you
 * include the source code of that other code when and as the GNU GPL
 * requires distribution of source code.
 *
 * Note that people who make modified versions of this file are not
 * obligated to grant this special exception for their modified versions;
 * it is their choice whether to do so.  The GNU General Public License
 * gives permission to release a modified version without this exception;
 * this exception also makes it possible to release a modified version
 * which carries forward this exception.
 */

// Archaic garbage--updated because I have a soft spot for consistency...
#ifndef lint
static const char rcsid[] =
    "@(#) $Header: /cvsroot/nsnam/ns-2/tools/poisson.cc,v 1.00 2010/02/08 15:23:30 jjuecks Exp $ (CU)";
#endif

// System library
#include <stdlib.h>

// ns2-specific includes
#include "random.h"
#include "trafgen.h"
#include "ranvar.h"


// Traffic generator realizing an ideal Poisson process;
// sends a packet of fixed size after a delay generated
// by an IID exponential RV; repeats forever...
class PoissonProcess_Traffic : public TrafficGenerator {
 public:
 	// Ctor
	PoissonProcess_Traffic();

	// Next-packet scheduler
	virtual double next_interval(int&);
 protected:
	// Poisson-process parameters
	int	packet_size_;		// Fixed packet size (bytes)
	double	lambda_;		// Rate in packets-per-second

	// Internal setup
	void init();

	// Exponential RV used for generating
	// interarrival delays
	ExponentialRandomVariable inter_packet_;
};

// Ctor implementation
PoissonProcess_Traffic::PoissonProcess_Traffic() : inter_packet_(0.0) {
	bind("packet_rate", &lambda_);
	bind("packet_size", &packet_size_);
}

// Internal initializer implementation
void PoissonProcess_Traffic::init() {
	// Set up exponential RV...
	inter_packet_.setavg(1.0 / lambda_);
}

// Schedule next packet implementation
double PoissonProcess_Traffic::next_interval(int& size) {
	// Fixed packet size--yay!
	size = packet_size_;

	// Generate next delay from an RV and return
	return inter_packet_.value();
}


// Inform ns2 of our new class, its position in
// the class hierarchy, and its OTcl name.
static class PoissonTrafficClass : public TclClass {
 public:
	PoissonTrafficClass() : TclClass("Application/Traffic/PoissonProcess") {}
	TclObject* create(int, const char*const*) {
		return (new PoissonProcess_Traffic());
	}
} class_poisson_traffic;


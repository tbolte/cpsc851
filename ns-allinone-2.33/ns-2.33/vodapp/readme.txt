--------------------------------------------------------------------------------------
   TCP-Based Video Streaming Traffic Model for ns2

   (this readme file last updated:  6/27/2013)

1. Installation
2. Usage examples
3. Design discussion
--------------------------------------------------------------------------------------


--------------------------------------------------------------------------------------
1. Installation

 1.1  Create a directory ./vodapp and unpack the vodapp.tar.g
 1.2 Edit ~/common/ns-process.h and add the line the VODAPP_DATA  line 
      (excerpt below)

  //Diffusion ADU
    DIFFUSION_DATA,

    // Netflix simulator ADU
    VODAPP_DATA,

    // Last ADU
    ADU_LAST

    Add the object files to the Makefile (excert below)
       vodapp/vodapp.o  vodapp/vodappClient.o vodapp/vodappServer.o \


--------------------------------------------------------------------------------------

--------------------------------------------------------------------------------------
2. Usage examples

   ns testvod.tcl

--------------------------------------------------------------------------------------
3. Design discussion

   To be added



/*
 * filename: vodapp.cc
 * description: simulate the adaptive streaming over HTTP
 *
 * Author: Yunhui Fu
 * 2011/12/16
 *
 * Revisions:
 *  $A1 : 5-15-2012 : Added support for dumpFinalStats
 *  $A2:  1-23-2013 : remove server rate limit
 *
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <assert.h>
#include <arpa/inet.h> // htonl

#include "timer-handler.h"
#include "packet.h"
#include "app.h"
#include "webcache/tcpapp.h"

#include "tclcl.h"
//#include "address.h"
//#include "ip.h"

#include "vodappServer.h"

extern uint32_t bitrate_levels[];

#define FILE_DEFAULT stdout

// The size of the client's request packet
#define SZ_REQUEST 59


//#define TRACEME 1


class StreamingServer;

// StreamingServer
static class StreamingServerClass : public TclClass {
public:
    StreamingServerClass() : TclClass("Application/TcpApp/StreamingServer") {}
    TclObject* create(int argc, const char*const* argv) {
        if (argc != 5)
            return NULL;
        Agent *tcp = (Agent *)TclObject::lookup(argv[4]);
        if (tcp == NULL)
            return NULL;
        return (new StreamingServer(tcp));
    }
} class_streamingserver_app;



StreamingServer::StreamingServer (Agent *tcp)
    : StreamingApplication(tcp)
{
    message_size=1400;
    send_rate= 224.0;
}


void
StreamingServer::init ()
{
    TRACE ("BEGIN\n");
    StreamingApplication::init ();
    TRACE ("message_size=%d, send rate=%f\n", message_size, send_rate);

    packet_interval = ((double)message_size * 8.0) / (double)send_rate;
    TRACE ("packet_interval=%f\n", packet_interval);

    buf_cur_size = 0.0;

    latest_aavg = 0.0;

    TRACE ("init aavg=%f\n", aavg);
//#ifdef TRACEME 
    printf ("StreamingServer:init()(%f), myID:%d,  begin!  message_size to %d bytes,  rate:%lf  \n", Scheduler::instance().clock(), myID, message_size,send_rate);
//#endif
}


int
StreamingServer::command(int argc, const char*const* argv)
{
#ifdef TRACEME 
    printf ("StreamingServer:(%f), number argc:%d, first arg:%s \n", Scheduler::instance().clock(), argc, (char *)argv[1]);
#endif
    if (argc == 2) {
        if (strcmp(argv[1], "dumpstats") == 0) {
          dumpFinalStats();
          return (TCL_OK);
        } else if(strcmp (argv[1], "start") == 0) {
            TRACE_VA ("start\n");
            start ();
            return (TCL_OK);

        } else if (strcmp (argv[1], "stop") == 0) {
            TRACE_VA ("stop\n");
            stop ();
            return (TCL_OK);
        }

    } else if(argc == 3) {
        if(strcmp (argv[1],"send-rate") == 0) {
            send_rate = atof(argv[2]);
            TRACE ("send_rate=%f\n", send_rate);
//#ifdef TRACEME 
    printf ("StreamingServer:(%f), set send rate to %lf bps \n", Scheduler::instance().clock(), send_rate);
//#endif
            return(TCL_OK);

        } else if(strcmp (argv[1],"message-size") == 0) {
            message_size = atol(argv[2]);
            TRACE ("message-size=%d\n", message_size);
//#ifdef TRACEME 
    printf ("StreamingServer:(%f), set message_size to %d bytes \n", Scheduler::instance().clock(), message_size);
//#endif
            return(TCL_OK);

        }
    }
    // If the command hasn't been processed by StreamingApplication()::command,
    // call the command() function for the base class
    return (StreamingApplication::command(argc, argv));
}


/************************************************************************
*  
* Function: void StreamingServer::respondRequests()
*  
* Inputs:
*   
* Explanation:
*  This is called periodically to process player requests for more
*   video data.
* 
*  pseudo code :
*
*  
*************************************************************************/
void StreamingServer::respondRequests()
{
    TRACE_VA ("BEGIN\n");
    if (! running_) {
        TRACE_VA ("Warning: not running!\n");
        printf ("StreamingServer:respondRequests(%f),myID:%d, HARD ERROR,not running  \n", Scheduler::instance().clock(), myID);
        return;
    }
#ifdef TRACEME 
    printf ("StreamingServer:respondRequests(%f),myID:%d, there are %d requests queued \n", Scheduler::instance().clock(), myID, last_requests.size());
#endif

    // fetch the packet from queue
    for (; ! last_requests.empty(); ) {
        //TRACE_VA ("last_requests.size=%d, front.bitrate=%d, front.consumed=%d, front.size=%d!\n"
            //, last_requests.size()
            //, last_requests.front().bitrate
            //, last_requests.front().consumed
            //, last_requests.front().size
            //);
        if (last_requests.front().consumed >= last_requests.front().size) {
            last_requests.pop_front ();
        } else {
            break;
        }
    }
    if (last_requests.empty()) {
        TRACE_VA ("last_requests.size=%d\n", last_requests.size());
        return;
    }
    assert (last_requests.front().size > last_requests.front().consumed);
    size_t packet_size = last_requests.front().size - last_requests.front().consumed;

    /* the message_size is the maximum size of the packet that the server can send out one time (call send() once),
       the server will call send() multiple time to send the packets(with the size packet_size) until
       reaching to the required length(packet_size) if the 
     */
    if (message_size < packet_size) {
        packet_size = message_size;
    }

    StreamingApplicationData * p = new StreamingApplicationData (packet_size, getSequence(), last_requests.front().bitrate, packet_size);
    assert ((packet_size > 0) && (p != NULL));
    //TRACE_VA ("send data, bitrate=%d, size=%d\n", last_requests.front().bitrate, packet_size);

    last_requests.front().consumed += packet_size;
    if (last_requests.front().consumed >= last_requests.front().size) {
        last_requests.pop_front ();
    }
    //TRACE_VA ("buf_queue.size=%d, last_requests.size=%d\n", buf_queue.size(), last_requests.size());

    assert (buf_cur_size >= packet_size);
    buf_cur_size -= packet_size;
#ifdef TRACEME1 
    fprintf (fp_buflog, "%f VASVRBUF %f %d\n", Scheduler::instance().clock(), buf_cur_size, packet_size);
#endif
#ifdef TRACEME 
    printf ("StreamingServer:respondRequests(%f),myID:%d, sending this much data:%d ,buf cur size:%f\n", Scheduler::instance().clock(), myID, packet_size,buf_cur_size);
#endif

    // the packet is a reponse message, so the size should be the size of video content
    //$A0
    totalBytesSent += packet_size;
    if (firstSendTime == 0)
      firstSendTime = Scheduler::instance().clock();

    send (packet_size, p);

    calculateAavg ();
}


/************************************************************************
*  
* Function: void StreamingServer::process_data (int size, AppData* data)
*  
* Inputs:
*   
* Explanation:
*  This is called periodically to process player requests for more
*   video data.
* 
*  pseudo code :
*
*  
*************************************************************************/
void StreamingServer::process_data (int size, AppData* data)
{
    TRACE_VA ("process_data sz=%d\n", size);
    if (data == NULL) {
        printf ("StreamingServer:process_data(%f),myID:%d, HARD ERROR, NOT RUNNING ? \n", Scheduler::instance().clock(), myID);
        TRACE_VA ("Error in data!\n");
        return;
    }
    StreamingApplicationData *pvd = (StreamingApplicationData *) data;
#ifdef TRACEME1 
    fprintf (fp_pkglog, "%f VASVRPROCPKG %d %d %d %d\n", Scheduler::instance().clock(),
        pvd->getSequence(), pvd->getReqSize(), pvd->getBitrate(), pvd->getSelfSize());
#endif

    // Access the header for the received packet:
    if (pvd->getMagic() != MAGIC_VIDHDR) {
          printf ("StreamingServer:process_data(%f),myID:%d, HARD ERROR, received: 0x%08X \n", Scheduler::instance().clock(), myID, pvd->getMagic());
        TRACE_VA ("Error in header magic! received: 0x%08X, expected: 0x%08X\n", pvd->getMagic(), MAGIC_VIDHDR);
        return;
    }

    // server
    video_content_t cnt;
    cnt.bitrate = pvd->getBitrate();
    cnt.size = pvd->getReqSize();
    cnt.consumed = 0;
    buf_cur_size += cnt.size;
#ifdef TRACEME 
    printf ("StreamingServer:process_data(%f),myID:%d, request arrived, bitrate:%d, size:%d, cur buf size:%f  \n", Scheduler::instance().clock(), myID, cnt.bitrate,cnt.size,buf_cur_size);
#endif
#ifdef TRACEME1
    fprintf (fp_buflog, "%f VASVRBUF %f %d\n", Scheduler::instance().clock(), buf_cur_size, cnt.size);
#endif
    //TRACE_VA ("recv request sz=%d, bitrate=%d, reqsize=%d\n", pvd->getSelfSize (), cnt.bitrate, cnt.size);
    last_requests.push_back (cnt);
}


/************************************************************************
*
* Function: void StreamingApplication::dumpFinalStats(char *outputFile)
*
* Inputs:
*    char *outputFile
*   
* Explanation:
*  This is invoked via the tcl command dumpstats.  We dump a single line of stats to
*  the file defined as follows:
***********************************************************************/
void StreamingServer::dumpFinalStats()
{
  double curr_time = Scheduler::instance().clock();
#ifdef TRACEME
  printf("StreamingServer::dumpFinalStats(%f):(%d) \n", curr_time, myID);
#endif


  double Rxthroughput =  (totalBytesRxed*8)/(curr_time  - firstRxTime);
  double Sendthroughput =  (totalBytesSent*8)/(curr_time  - firstSendTime);

  FILE *fp = fopen("vodappSERVERRESULTS.out", "a+");

  fprintf(fp,"%d %9.0f %9.0f %9.0f %9.0f %9.6f \n",myID,Rxthroughput,Sendthroughput, totalBytesRxed, totalBytesSent, (curr_time-firstRxTime));
  printf("StreamingSERVER RESULTS:(id:%d) RxThroughput: %9.0f, SendThroughput: %9.0f, totalBytesRxed: %9.0f, totalBytesSent: %9.0f, RxTime: %9.6f \n",myID,Rxthroughput,Sendthroughput, totalBytesRxed, totalBytesSent, (curr_time-firstRxTime));

  fclose(fp);

}



# Test script for vodapp
# 
#app1(client)                app2 (server side)
# tcp1-->                   <---tcp2
# n1 --------- n2(Router) -----n3
#  BW_LINK1             BW_LINK2

source dash.tcl

set stopTime 2000
set startTime 0.20

#set BW_LINK [lindex $argv 0];  # the bandwidth of link
set BW_LINK1 10000000;  # the bandwidth of link
set BW_LINK2 1000000000;  # the bandwidth of link

#set tracefile   out.tr

set ns   [new Simulator]

# open trace files and enable tracing
#set f [open $tracefile w]
#$ns trace-all $f


set node_(1) [$ns node]
set node_(2) [$ns node]
set node_(3) [$ns node]


$ns simplex-link $node_(1) $node_(2) $BW_LINK1 10ms DropTail
$ns simplex-link $node_(2) $node_(1) $BW_LINK1 10ms DropTail
$ns queue-limit $node_(1) $node_(2) 200
$ns queue-limit $node_(2) $node_(1) 200

$ns simplex-link $node_(2) $node_(3) $BW_LINK2 10ms DropTail
$ns simplex-link $node_(3) $node_(2) $BW_LINK2 10ms DropTail
$ns queue-limit $node_(2) $node_(3) 1000
$ns queue-limit $node_(3) $node_(2) 1000


set tcp1 [new Agent/TCP/FullTcp/Sack]
set tcp2 [new Agent/TCP/FullTcp/Sack]
# Set TCP parameters here, e.g., window_, iss_, . . .
$tcp1 set window_ 100
$tcp1 set fid_ 1
$tcp2 set window_ 100
$tcp2 set fid_ 2
$tcp2 set iss_ 1224

$ns attach-agent $node_(1) $tcp1
$ns attach-agent $node_(3) $tcp2
$ns connect $tcp1 $tcp2
$tcp2 listen

set numberID 1
set app1 [new Application/TcpApp/StreamingClient $tcp1]
set app2 [new Application/TcpApp/StreamingServer $tcp2]


$app1  player-interval  $vodapp_player_interval
$app1  request-interval $vodapp_segment_size
$app1  set-ID $numberID
$app1  max-requests $vodapp_outstanding_requests
$app1  buffer-size $clientbuffersize
$app1  buffer-threshold-bitrate [expr {$clientbuffersize / 9}]
$app1  buffer-threshold-player $vodapp_player_threshold

$app1  aavg-delta $aavgdelta
$app1  aavg-interval $aavginterval

$app1  switching-interval-increase $vodapp_switching_interval_increase
$app1  switching-interval-reduction $vodapp_switching_interval_reduction


$app1 set-reqlog "out.vaclireq$numberID"
$app1 set-buflog "out.vaclibuf$numberID"
$app1 set-statlog "out.vaclistat$numberID"

$app1 set-bitrates $vodapp_bitrates_list
$app1 ratio-threshold-reduction $vodapp_ratio_threshold_reduction
$app1 ratio-threshold-increase  $vodapp_ratio_threshold_increase
$app1 min-stabilize-time  $vodapp_player_min_stabilize_time
$app1 dash-mode  $DASHMODE
$app1 player-fetch-ratio-threshold $vodapp_player_fetch_ratio_threshold
$app1 adaptation-sensitivity $vodapp_adaptation_sensitivity

    # (Server only) the send rate of the data
$app2 send-rate $BW_LINK2

    # (Server only) the message size in each packet
$app2 message-size 42000
$app2 aavg-delta 0.8
$app2 aavg-interval $aavginterval

$app2 set-ID $numberID
    # set log file for server's buffer (totalsize, changedsize)
$app2 set-buflog "out.vasvrbuf$numberID"
    # set log file for server's internal variables (avg bw, 2sec bw, buffer size)
$app2 set-varlog "out.vasvrvar$numberID"

$ns at $stopTime "$app1 dumpstats"
$ns at $stopTime "$app2 dumpstats"
$ns at $stopTime "$app1 stop"
$ns at $stopTime "$app2 stop"

$app1 connect $app2

proc finish {} {
    global app1 app2
    $app1 stop
    $app2 stop

    global ns f 
#    $ns flush-trace
#    close $f
    exit 0
}

#Simulation Scenario
$ns at $startTime "$app2 start"
$ns at $startTime "$app1 start"

$ns at $stopTime "finish"

$ns run
exit 0

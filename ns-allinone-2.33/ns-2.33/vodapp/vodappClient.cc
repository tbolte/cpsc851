/*
 * filename: vodapp.cc
 * description: simulate the adaptive streaming over HTTP
 *
 * Author: Yunhui Fu
 * 2011/12/16
 *
 * Revisions:
 *  $A1 : 5-15-2012 : Added support for dumpFinalStats
 *  $A2 : 6-26-2012 : Added support for higher bitrate encodings
 *  $A3 : 11-3-2012 : start out at higher quality 
 *  $A4 : 1-16-2013 : minor changes 
 *  $A5 : added monitoring of the control algorithm.
 *  $A6 : 1/30/2013 : minor changes
 *  $A7 : 2/6/2013 : minor changes
 *  $A8 : 2/13/2013 : tcl minStabilizeTime
 *  $A9 : 2/22/2013 : changed PSNR and metrics. Don't compute until after first 20 seconds
 *  $A14: 3/22/2013: Added buffering state
 *  $A16: 3/23/2013: Added rebufferEventRate metric
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <assert.h>
#include <arpa/inet.h> // htonl

//$A5
#include <math.h>

#include <deque>

#include "timer-handler.h"
#include "packet.h"
#include "app.h"
#include "webcache/tcpapp.h"

#include "tclcl.h"
//#include "address.h"
//#include "ip.h"

#include "vodappClient.h"

//#define TRACEME 0 

/*
 315 kbps --  320x180
 705 kbps --  640x360
1482 kbps --  640x360
2435 kbps -- 1280x720
3401 kbps -- 1280x720
*/

//static uint32_t bitrate_levels0[] = {350000,500000,840000,1024000,1520000,2048000,2350000,2750000};
//static uint32_t bitrate_levels0[] = {350000,500000,840000,1000000,1520000,2048000,2350000,
//static uint32_t bitrate_levels0[]= {64000,128000, 500000,1000000,1500000,2600000,3500000, 3750000};//, 4800000};
//static uint32_t bitrate_levels0[] = {500000,1000000,1500000,2600000,3500000, 3750000};
static uint32_t bitrate_levels0[] = {64000,128000, 500000,1000000,1500000,2600000,3500000, 3750000};//, 4800000};

#define NUM_TYPE(p) (sizeof (p) / sizeof (p[0]) )

#define FILE_DEFAULT stdout

// The size of the client's request packet
#define SZ_REQUEST 59


class StreamingClient;

// StreamingClient
static class StreamingClientClass : public TclClass {
public:
    StreamingClientClass() : TclClass("Application/TcpApp/StreamingClient") {}
    TclObject* create(int argc, const char*const* argv) {
        if (argc != 5)
            return NULL;
        Agent *tcp = (Agent *)TclObject::lookup(argv[4]);
        if (tcp == NULL)
            return NULL;
        return (new StreamingClient(tcp));
    }
} class_streamingclient_app;


//StreamingClient::StreamingClient ()
//{
//    for (int i = 0; i < NUM_TYPE(bitrate_levels0); i ++) {
//        bitrate_levels.push_back (bitrate_levels0[i]);
//    }
//}

StreamingClient::StreamingClient (Agent *tcp)
    : StreamingApplication(tcp)
{
    is_player_run = 0.0;
    request_interval = SEC_REQUEST;
    max_requests = 2;
    buf_max_video = 0.0;
    dynamicVideoBufferMax=buf_max_video;
    buf_thres_bitrate = 0.0;
    buf_thres_player = 0.0;
    abuf = 0;
    switching_interval_increase = SEC_LOWER;
    switching_interval_reduction = SEC_LOWER;
    time_lower = 0.0;
    time_higher = 0.0;
    //$A1
    bufferUnderruns=0;
    //$A16; init to 1 as we start out in rebuffering mode
    rebufferingEvents=1;
    totalTimeRebuffering = 0.0;
    rebufferingStartTime = 0.0;
    rebufferingCounter=0;  //init to 0 as we count number of transitions out of rebuffering

    numberOutages=0;
    videoQualityMeasure= 0.0;  //Ratio of final average  Relative to a HD stream 
    videoPlayTime=0.0;
    videoOutageTime=0.0;
    outageQualityMeasure=0.0; //  1-videoOutageTime/videoPlayTime; 
    // perceived quality =   x * videoQualityMeasure +  (1-x) * outageQualityMeasure;
    perceivedQuality=0.0;

    is_live = false;
    ratio_threshold_reduction = 0.5;
    ratio_threshold_increase = 0.5;
    for (int i = 0; i < NUM_TYPE(bitrate_levels0); i ++) {
        bitrate_levels.push_back (bitrate_levels0[i]);
    }
    fp_statlog = FILE_DEFAULT;
    player_fetch_ratio_threshold = 30;
//$A13
    bufferState= GREEN;
    bufRedYellowThreshold=0;
    bufYellowGreenThreshold=0;
    dynamicVideoBufferMax=0;
    lastBufferScale=0;
}

// output the stat info.
StreamingClient::~StreamingClient ()
{
//$A4
//    if (FILE_DEFAULT != fp_statlog) {
//        calcStat ();
//        fclose (fp_statlog);
//    }
}


void
StreamingClient::init ()
{
    TRACE ("BEGIN\n");
    StreamingApplication::init ();

//$A13
    if (buf_max_video < request_interval * 2) {
        buf_max_video = request_interval * 2;
        dynamicVideoBufferMax=buf_max_video;
//#ifdef TRACEME 
        fprintf (stderr, "Warning: buffer size not enough, adjusted to %f seconds\n", buf_max_video);
//#endif
    }
    TRACE ("packet_interval=%f\n", packet_interval);

    buf_cur_size = 0.0;
    time_lower = 0.0;
    time_higher = 0.0;
    latest_aavg = 0.0;
    is_player_run = false;

    abuf = 0;

    buf_queue.erase (buf_queue.begin(), buf_queue.end());
    TRACE ("init aavg=%f\n", aavg);

    frame_error_interval = 2.0;
    frame_error_last_time = Scheduler::instance().clock();
    frame_error_cnt = 0;
    frame_error_base = 0;
    frame_error_rate = 0;
    frame_error_delta = 0.8;
//A5
    bitRateSwitchCount = 0;
//$A7
    dynamicAdaptTimeWait = 2.0;
    bitRateSwitchTime = Scheduler::instance().clock();
    minRateDownTime = 10;
    minRateUpTime = 10;
//  To override the tcl config setting, uncomment
//    minStabilizeTime=100.0;
    adaptState = AGGRESSIVE_UP;

    last_player_read = Scheduler::instance().clock();
    printf("StreamingClient:init(%lf) myID:%d  init minStabilizeTime:%lf\n", Scheduler::instance().clock(),myID,minStabilizeTime);

}

/************************************************************************
*
* Function: int StreamingClient::calcStat (int *metric1, int *metric2, int *metric3, int *metric4, int *metric5, int *metric6, int *avgBufferSize) {
*
* Inputs:
*   Reference to the metric results
* 
* Outputs:
*   Outputs by reference the results.  
*   returns:  0 for no error, -1 on error
*   
* Explanation:
*  This is invoked to compute the streaming metric.
   The playback buffer is depleted  periodically based on vodapp_player_interval 
*  We record how close to the maximal possible (assuming highest quality)
*  We also obtain the following stats:
*    -Number of runs where a run is defined as a unique run of one or more impaired
*      intervals
*    -Avg size (in time) of the runs
*    -percentage of time player is impaired
*    -zappingTime: an estimate of once the buffer is empty how long it takes before
*         the video begins playback
*    
*   Define the following metrics:
*  Metric1:  average playback rate based on N samples   
*  Metric2:  PSNR : values < 10 are BAD;  values 30 and above are GOOD;  values approaching 50 are VERY GOOD (best possible is about 50)
*  Metric3:  Average number of underruns per hour 
*  Metric4:  Average number of bitrate switches per hour (>2 bad)
*  Metric5:  Channel zapping time (time it takes from time 0 until when the player first starts
*  Metric6:  R value : weighted composite metric  - based on normalized metric's 2,3,4
*              W2: .80
*              W3: 0.10
*              W4:
*              W5
*
*            Step 1: multiple Metric2 by 5, max value of 100 to scale [0,100]
*            Step 2: subtract 3 points for underrun #/hr =1, subtract 3*metric3 if metric3 > 1 
#                            Max value 80
*            Step 3: subtract 0.5*metric4 if metric3 >  5
#                            Max value 20
*
*  avgBufferSize : The avg playback buffer size based on samples by the player
*
*  MetricMOS: TBD
*             MOS= f( a*Metric2 -b*Metric3 - 4*Metric5 (0 bad, 3 tolerable, 4 good, 5 is best)
*
***********************************************************************/
//$A9
int StreamingClient::calcStat (int *metric1, int *metric2, int *metric3, int *metric4, int *metric5, int *metric6, int *avgBuffer, int *rebufferRateParam, double *avgRebufferingTimeParam, double *percentageTimeRebufferingParam) {
    char line [1024];
    char lineString [100];
    double tmval= 0.0;
    int stateFlag=0;
//$A5
    int blockbytes = 0;
    double maxbytes, ratio;
    double frameRateSample = 0.0;
    double frameRateSampleSum = 0.0;
    double maxFrameRate;
    double errorSAMPLE = 0.0;
    double MSESUM = 0.0;
    int   numberSamples=0;
    double curTime =  Scheduler::instance().clock();

    double  countPlayingTime=0.0;
    double  countNOTPlayingTime=0.0;
    double  totalTime=0.0;
    double sum = 0;
    size_t cnt = 0;
    size_t cnt_run = 0;
    double ratio_pre = 0;
    double tmval_pre = 0;
    double last_tmval = 0.0;
    double tmsum = 0;
    double ratio_weight = 0;
    double avg1 = 0.0;
    double avg2 = 0.0;
    double bufSize = 0.0;
    double bufSizeSum =0.0;
    double avgBufSize=0.0;
    double timeToStartPlaying=0.0;

//$A6
    double lastBitRate=0.0;
    double avgBitRate=0.0;

//$A10
    int   localID;

    int part1 = 0;
    int part2 = 0;
    int part3 = 0;

    bzero( (void *)&line, 1024);
    bzero( (void *)&lineString, 100);

//$A4
    int rc = 0;

    *metric1=0;
    *metric2=0;
    *metric3=0;
    *metric4=0;
    *metric5=0;
    *metric6=0;
    *avgBuffer=0;
    *rebufferRateParam = 0;
    *avgRebufferingTimeParam = 0.0;
    *percentageTimeRebufferingParam=0;
    double weightA = 0.40;
    double weightB = 0.40;
    double weightC = 0.20;

    blockbytes = 0;
    maxbytes= 0.0;  
//$A6
//    ratio=0.0;

    frameRateSample=0.0;
    frameRateSampleSum=0.0;
    maxFrameRate= bitrate_levels[bitrate_levels.size() - 1];


#ifdef TRACEME 
    printf ("calcStat(%lf) begin:  maxFrameRate: %lf\n", curTime,  maxFrameRate);
#endif

    TRACE_VA ("calcStat() begin\n");
// fprintf (fp_varlog, "%f VACLISTAT %d %f %f\n", Scheduler::instance().clock(), total_size, maxbyte, ratio);
// the avg run size
    fseek (fp_statlog, 0, SEEK_SET);
    while ( fgets ( line, sizeof line, fp_statlog ) != NULL ) {

//    fprintf (fp_statlog, "%f %d %d %d %f %d %f %f %f %f %d %d\n", curTime,1, myID, total_size, maxbyte, last_bitrate, frameRateSample, avg, aavg,buf_cur_size,bufferUnderruns, adaptState);
      sscanf (line, "%lf %d %d %d %lf %lf %lf %lf %lf %lf \n", &tmval, &stateFlag, &localID,  &blockbytes, &maxbytes, &lastBitRate, &frameRateSample, &avg1, &avg2, &bufSize);
//$A10   
      if (localID == myID) { 
//$A9  don't consider the PSNR during startup period
//        if (tmval > 20.0) {
        if (tmval > 0.0) {
          if (tmval_pre == 0)
            tmval_pre = tmval;
          if (last_tmval == 0)
            last_tmval = tmval;

          sum += lastBitRate;
          cnt ++;
//A5
          numberSamples++;
          errorSAMPLE = maxFrameRate - frameRateSample;
          MSESUM +=  (errorSAMPLE*errorSAMPLE);
          bufSizeSum += bufSize;
          frameRateSampleSum += frameRateSample;

          double timeDiff = tmval-last_tmval;
          if (timeDiff < 0) {
            printf ("calcStat(%lf)  WARNING:   tmval:%lf, last_tmval:%lf, diff:%lf \n", curTime,tmval,last_tmval, timeDiff);
            timeDiff=0.0;
          }
          if (stateFlag == 3) {
            countPlayingTime += timeDiff;
          } else {
            countNOTPlayingTime += timeDiff;
          }
          totalTime += timeDiff;

          last_tmval = tmval;
        }

        //Detect when the player first starts
        if (timeToStartPlaying == 0) {
          //assume the sample needs to be something larger than 0 bps
          if (frameRateSample > 10000)
            timeToStartPlaying =totalTime;
        }
      }
#ifdef TRACEME 
        printf ("calcStat(%lf) line: ratio sample:%lf, frameRateSample: %lf, maxFrameRate:%lf  errorSample:%lf\n", curTime,ratio, frameRateSample, maxFrameRate,errorSAMPLE);
#endif
    }
    if (cnt > 0) {
        avgBitRate = sum / (double)cnt;
        *avgBuffer = (int) bufSizeSum/cnt;
    }
    if (tmsum <= 0.0) {
        tmsum = (tmval - tmval_pre);
    }
    if (tmsum <= 0.0) {
        ratio_weight = 0;
    } else {
        ratio_weight = ratio_weight/tmsum;
    }

    fseek (fp_statlog, 0, SEEK_END);

#ifdef TRACEME 
    printf ("calcStat(%lf) line: MSESUM:%lf, sum:%lf, bufSizeSum:%f, avgBuf:%d\n", curTime,MSESUM,sum,bufSizeSum, *avgBuffer);
#endif


//$A4, $A5
//    *metric1=(int) avgBitRate * 100;
    double MSEValue = 0.0;
    double avgFrameRate = 0.0;
    if (numberSamples > 0) {
      MSEValue = MSESUM / numberSamples;
      avgFrameRate = frameRateSampleSum / numberSamples;
    }

//$A16
    int rebufferEventRate=0;
    double avgRebufferingEventTime=0.0;
    double percentageTimeRebuffering=0.0;
//$A11
    if (totalTime > 0) {
      *metric3 =  (int) (3600.0* bufferUnderruns / totalTime);
      rebufferEventRate= (int) (3600.0 * rebufferingEvents)/totalTime;
      percentageTimeRebuffering= (100*totalTimeRebuffering)/totalTime;
    } else {
      *metric3 =  0;
      printf ("StreamingClient:calcStat(%f),myID:%d  HARD ERROR totalTime 0 \n", curTime,myID);
    }
  
    if (rebufferingCounter >0)
      avgRebufferingEventTime= totalTimeRebuffering/rebufferingCounter;


    *rebufferRateParam = rebufferEventRate;
    *avgRebufferingTimeParam = avgRebufferingEventTime;
    *percentageTimeRebufferingParam=percentageTimeRebuffering;

    
    *metric1=(int) avgFrameRate;
    double PSNR  = 0.0;
    double L = maxFrameRate;
    double x = L * L;
    double y = (MSEValue);
//$A7
    //An MSE less than 10 will be as good as it gets....
    //This bounds PSNR to something <200 for most cases. 
    if (y < 100.0) {
     y=100.0;
     printf("calcStat(%lf) WARNING:  MSE too low, set to %y \n", curTime, y);
    }
    double z = x / y;
    PSNR  = 10 * log10(z);
//    double y = sqrt(MSEValue);
//    PSNR  = 20 * log10(x/y);

#ifdef TRACEME 
    printf("calcStat(%lf) MSEValue:%lf, x:%f, y:%f, z:%f, L:%lf,  PSNR:%lf,  maxFrameRate:%lf \n", curTime, MSEValue,x,y,z,L,PSNR,maxFrameRate);
#endif

    *metric2=(int) PSNR;

//switches per hour seconds
    if (totalTime > 0) {
      *metric4 =  (int) (3600.0* bitRateSwitchCount / totalTime);
    } else {
      *metric4 =  0;
    }
#ifdef TRACEME 
    printf("calcStat(%lf) number of bitRateSwitches: %lf, totalTime:%lf, switches/hr:%d  \n", curTime, bitRateSwitchCount, totalTime, *metric4);
#endif

    *metric5 =  (int) timeToStartPlaying;
#ifdef TRACEME 
    printf("calcStat(%lf) timeToStartPlaying : %lf  (%d) \n", curTime, timeToStartPlaying,*metric5);
#endif

    part1 = 5*(*metric2);
    if (part1 > 100)
       part1=100;

    if (*metric3 > 1)
      part2 = 3* *metric3;
    if (part2 > 80)
      part2 = 80;


    if (*metric4 > 6)
      part3 = 1* *metric4;
    if (part3 > 20)
      part3 = 20;

//            Step 1: multiple Metric2 by 5, max value of 100 to scale [0,100]
//            Step 2: subtract 3 points for underrun #/hr =1, subtract 3*metric3 if metric3 > 1 
//                            Max value 80
//            Step 3: subtract 0.5*metric4 if metric3 >  5
//                            Max value 20
    *metric6=  part1 - part2 - part3; 
    if (*metric6 <= 0)
      *metric6 = 0;
    if (*metric6 >100)
      *metric6 = 100;

#ifdef TRACEME 
    printf("calcStat(%f) ID:%d, DPSNR:%d, part1:%d, part2:%d, part3:%d,  metric6:%d \n",curTime,myID, *metric2, part1,part2,part3,*metric6);
//$A4
    fprintf (fp_statlog, "Metric1:%d, Metric2:%d, Metric3:%d; Metric4:%d, Metric5:%d, Metric6:%d, avgBufferSize:%\n", *metric1,*metric2,*metric3,*metric4,*metric5,*metric6,*avgBuffer);

    fprintf(fp_statlog, "Client BitRate Average: %f, sum=%f, cnt=%d; Run Time: weighted_avg=%f, timsum=%f, #run=%d\n", avgBitRate, sum, cnt,  ratio_weight, tmsum, cnt_run);
    fprintf(fp_statlog,"calcStat(%lf) MSEValue:%lf, L:%lf,  PSNR:%lf,  maxFrameRate:%lf \n", curTime, MSEValue,L,PSNR,maxFrameRate);
    printf("Metric1:%d, Metric2:%d, Metric3:%d; Metric4:%d, Metric5:%d, Metric6:%d, avgBufferSize:%d\n", *metric1,*metric2,*metric3,*metric4,*metric5,*metric6,*avgBuffer);
    printf("Client BitRate Average: %f, sum=%f, cnt=%d; Run Time: weighted_avg=%f, timsum=%f, #run=%d\n", avgBitRate, sum, cnt,  ratio_weight, tmsum, cnt_run);
    printf("calcStat(%lf) MSEValue:%lf, L:%lf,  PSNR:%lf,  maxFrameRate:%lf \n", curTime, MSEValue,L,PSNR,maxFrameRate);
#endif

/*  Metric1:  average playback rate based on N samples   
*  Metric2:  PSNR : values < 10 are BAD;  values 30 and above are GOOD;  values approaching 50 are VERY GOOD (best possible is about 50)
*  Metric3:  Average number of underruns per hour 
*  Metric4:  Average number of bitrate switches per hour (>2 bad)
*  Metric5:  Channel zapping time (time it takes from time 0 until when the player first starts
*  Metric6:  R value : weighted composite metric  - based on normalized metric's 2,3,4
*/
    printf("CALCSTAT(%lf):  bufferRate:%d, rebufferingRate:%d,bufferUnderruns:%d, rebufferingEvents:%d, totalTimeRebuffering:%lf, avgRebufferingEventTime:%3.3f(numberEvents:%d), percentageTimeBuffering:%3.3f \n", curTime, *metric3, rebufferEventRate, bufferUnderruns, rebufferingEvents,totalTimeRebuffering,avgRebufferingEventTime,rebufferingCounter,percentageTimeRebuffering);

    return rc;

}


void
StreamingClient::stop()
{
    TRACE_VA ("cli BEGIN\n");

//$A4
//    fflush (fp_statlog);
//    calcStat ();
    StreamingApplication::stop ();
}

int
StreamingClient::command(int argc, const char*const* argv)
{
#ifdef TRACEME 
    printf ("StreamingClient:(%f), number argc:%d, first arg:%s \n", Scheduler::instance().clock(), argc, argv[1]);
#endif
    if (argc == 2) {
        if (strcmp(argv[1], "dumpstats") == 0) {
          dumpFinalStats();
          return (TCL_OK);
        } else if(strcmp (argv[1], "start") == 0) {
            TRACE_VA ("start\n");
            start ();
            return (TCL_OK);
        } else if (strcmp (argv[1], "stop") == 0) {
            TRACE_VA ("stop\n");
            stop ();
            return (TCL_OK);
        }

    } else if(argc==3) {
        if(strcmp (argv[1],"request-interval") == 0) {
            request_interval = atof(argv[2]);
            TRACE ("request_interval=%f\n", request_interval);
            return(TCL_OK);

        } else if(strcmp (argv[1],"player-interval") == 0) {
            packet_interval = atof(argv[2]);
            TRACE ("packet_interval=%f\n", packet_interval);
            return(TCL_OK);

        } else if(strcmp (argv[1],"buffer-size") == 0) {
            buf_max_video = atof (argv[2]);
            dynamicVideoBufferMax=buf_max_video;
            TRACE ("buf_max_video=%f\n", buf_max_video);
            return(TCL_OK);

        } else if(strcmp (argv[1],"buffer-threshold-bitrate") == 0) {
            buf_thres_bitrate = atof (argv[2]);
            TRACE ("buf_thres_bitrate=%f\n", buf_thres_bitrate);
            return(TCL_OK);

        } else if(strcmp (argv[1],"buffer-threshold-player") == 0) {
            buf_thres_player = atof (argv[2]);
            TRACE ("buf_thres_player=%f\n", buf_thres_player);
            return(TCL_OK);

        } else if(strcmp (argv[1],"switching-interval-increase") == 0) {
            switching_interval_increase = atof(argv[2]);
            TRACE ("switching_interval_increase=%f\n", switching_interval_increase);
            return(TCL_OK);

        } else if(strcmp (argv[1],"switching-interval-reduction") == 0) {
            switching_interval_reduction = atof(argv[2]);
            TRACE ("switching_interval_reduction=%f\n", switching_interval_reduction);
            return(TCL_OK);

        } else if (strcmp (argv[1], "max-requests") == 0) {
            max_requests = atoi (argv[2]);
            TRACE ("max_requests=%d\n", max_requests);
            return(TCL_OK);

        } else if (strcmp (argv[1], "adaptation-sensitivity") == 0) {
            adaptation_sensitivity = atof (argv[2]);
            TRACE ("adaptation-sensitivity=%f\n", adaptation_sensitivity);
            return(TCL_OK);

        } else if (strcmp (argv[1], "ratio-threshold-increase") == 0) {
            ratio_threshold_increase = atof (argv[2]);
            TRACE ("ratio-threshold-increase=%f\n", ratio_threshold_increase);
            return(TCL_OK);

        } else if (strcmp (argv[1], "ratio-threshold-reduction") == 0) {
            ratio_threshold_reduction = atof (argv[2]);
            TRACE ("ratio-threshold-reduction=%f (argv 2 == %s)\n", ratio_threshold_reduction, argv[2]);
            return(TCL_OK);

        } else if (strcmp (argv[1], "player-fetch-ratio-threshold") == 0) {
            player_fetch_ratio_threshold = atof (argv[2]);
            TRACE ("player_fetch_ratio_thresholdn=%f\n", player_fetch_ratio_threshold);
            return(TCL_OK);
//$A8
        } else if(strcmp (argv[1], "min-stabilize-time") == 0) {
            minStabilizeTime= atof (argv[2]);
            TRACE ("min-stabilize-time:%d\n", min-stabilize-time);
            return(TCL_OK);
//$A13
        } else if(strcmp (argv[1], "dash-mode") == 0) {
            DASHMode = atoi (argv[2]);
            TRACE ("DASHMode:%d\n", DASHMode);
            return(TCL_OK);

        } else if(strcmp (argv[1], "set-statlog") == 0) {
            FILE *fp = fopen (argv[2], "w+");
            if (NULL == fp) {
                TRACE ("Error in open file '%s'\n", argv[2]);
                return(TCL_ERROR);
            }
            if (FILE_DEFAULT != fp_statlog) {
                fclose (fp_statlog);
            }
            fp_statlog = fp;
            TRACE ("stat variables log file '%s'\n", argv[2]);
            return(TCL_OK);

        }
    }

    if (strcmp (argv[1], "set-bitrates") == 0) {
        char *endptr;
        char *nextptr;
        char *startptr;
        uint32_t val;
        int cnt = 0;
        TRACE ("argc=%d\n", argc);
        bitrate_levels.erase (bitrate_levels.begin(), bitrate_levels.end());
        printf ("StreamingClient:(%f), bitrates:   ", Scheduler::instance().clock());
        for (int i = 0; i < argc - 2; i ++) {
            TRACE ("argv[%d]=%s\n", i+2, argv[i+2]);

            //long int strtol ( const char * str, char ** nextptr, int base );
            endptr = (char *)argv[i + 2] + strlen (argv[i + 2]);
            for (startptr = nextptr = (char *)argv[i + 2]; (nextptr != NULL) && (nextptr < endptr); ) {
                val = strtoul (startptr, &nextptr, 0);
                if ((0 == val) && (startptr == nextptr)) {
                    break;
                }
                bitrate_levels.push_back (val);
                TRACE ("bitrates[%d]=%d\n", cnt, bitrate_levels[cnt]);
                printf ("%d, ", val);
                cnt ++;
                startptr = nextptr + 1;
            }
            printf (" \n");
        }
        return(TCL_OK);
    }

    // If the command hasn't been processed by StreamingApplication()::command,
    // call the command() function for the base class
    return (StreamingApplication::command(argc, argv));
}


void
StreamingClient::start()
{
    TRACE_VA ("BEGIN\n");
//$A13
//These are only used in DASHMode 2
    if (DASHMode == 2) {
      bufferState= GREEN;
      buf_max_video = dynamicVideoBufferMax / 2;
      bufRedYellowThreshold=buf_max_video/3;
      bufYellowGreenThreshold = 2 * bufRedYellowThreshold;
      lastBufferScale= Scheduler::instance().clock();
    }
    StreamingApplication::start ();
    //$A3
    last_bitrate = 6000000;
    last_bitmap_index = 0;
    clientUpdate ();
    printf ("StreamingClient:start()(%f): myID:%d, bitrate_level.size: %d, request_interval:%lf, packet_interval:%lf, max_requests:%d, buf_max_video:%lf, buf_thres_bitrate:%lf, buf_thres_player:%lf \n",
                   Scheduler::instance().clock(), myID, bitrate_levels.size(),request_interval, packet_interval, max_requests, buf_max_video, buf_thres_bitrate, buf_thres_player);

//$A8
    printf ("StreamingClient:start()(%f): ratio_threshold_increase:%lf, ratio_threshold_reduction:%lf, switching_interval:reduction:%lf, increase:%lf, adaptation_sensitivity:%lf, min-stabilize-time:%lf  \n",
                   Scheduler::instance().clock(), ratio_threshold_increase, ratio_threshold_reduction, switching_interval_reduction,switching_interval_increase, adaptation_sensitivity, minStabilizeTime);


}

double
StreamingClient::getSchedualInterval() {
    double interval = StreamingApplication::getSchedualInterval();
    while (interval > frame_error_interval / 3) {
        interval /= 4;
    }
    return interval;
}



bool
StreamingClient::periodic_work () {
    TRACE ("cal_frame_error () ...\n");

    calculateAavg ();

    cal_frame_error (false);
    TRACE ("StreamingApplication::periodic_work () ...\n");
    if (StreamingApplication::periodic_work()) {
        TRACE ("consumeVideo () ...\n");
        consumeVideo ();
        return true;
    }
    return false;
}

void
StreamingClient::cal_frame_error (bool iserror)
{
    bool flg_cal = false;
    double this_time = Scheduler::instance().clock();
    frame_error_base ++;
    if (iserror) {
        frame_error_cnt ++;
    }
    if (this_time >= frame_error_last_time + frame_error_interval) {
        flg_cal = true;
    }
    if (flg_cal) {
        frame_error_rate = frame_error_rate * (1 - frame_error_delta) + ((double)frame_error_cnt * frame_error_delta / frame_error_base);
//        fprintf (fp_varlog, "%f VACLIFERR %f\n", Scheduler::instance().clock(), frame_error_rate);
        frame_error_cnt = 0;
        frame_error_base = 0;
        frame_error_last_time = this_time;
    }
}

//void
//fetch_content (double time_at, double time_len)
//{
//}

/************************************************************************
*  
* Function: void StreamingClient::consumeVideo()
*  
* Inputs:
*   
* Explanation:
*  This is called when the 'player' needs more data.
*  The 'player' consumes video periodically - set by tcl vodapp_player_interval
*  Once it's done it calls clientupdate to update monitors and see
*   it the client should adapt
* 
*  pseudo code :
*    states:  buffering or playing (and still buffering)
*             is_player_run == 0 : buffering
*             is_player_run == 1 : player is running
*
*             This is tracked in out.vaclist as follows:
*              stateFlag = 3 means running
*              stateFlag = 1 or 2  means buffering or underrun
*
*
*    transition to buffering by:
*      init,  player is running and obtains less than a segment of data 
*    transition to run:
*      player tick occurs and there is now at least 2 segs of data buffered
*    
*    Helpful info:
*
*      is_player_run == 0 (false): buffering
*                      1 (true) : player is running
*             The idea is that we might want the player to
*             not run if the buffer is below a minimum.
*      request_interval: this is the configured segment size
*      packet_interval - this is the configured player interval.
*          by default we set this to request_interval/4
*      sets the buffering/playing threshold :  buf_thres_player
*  
*************************************************************************/
void StreamingClient::consumeVideo()
{
//$A5
    double curTime =  Scheduler::instance().clock();
    double avg = 0.0;
    // the size of the block under the max bitrate in getInterval() seconds
    double maxbyte = (double) getInterval() * bitrate_levels[bitrate_levels.size() - 1] / 8;
    double ratio = 0.0;
    double frameRateSample  = 0.0;
    size_t total_size = 0;

    TRACE_VA ("BEGIN\n");

    // byte size consumed = bitrate * interval
#ifdef TRACEME 
    printf ("StreamingClient:consumeVideo:(%f), buf_cur_size:%f, buf_thresh_player:%f, buf_max_video:%f,  last_bitrate:%d , updated average : %6.6f  \n",  curTime,buf_cur_size,buf_thres_player,buf_max_video, last_bitrate,aavg);
#endif

    //If buffer is empty
    if (buf_queue.empty()) {
      bufferUnderruns++;
      if (is_player_run == true){
        is_player_run = false;
        rebufferingStartTime = curTime;
        rebufferingEvents++;
//#ifdef TRACEME 
        printf ("StreamingClient:consumeVideo:(%f): MOVE TO BUFFERING MODE 1  buf_cur_size:%lf, bufferUnderruns:%d, rebufferingEvents:%d \n", curTime,buf_cur_size,bufferUnderruns,rebufferingEvents);
//#endif
      }
//#ifdef TRACEME 
      printf ("StreamingClient:consumeVideo:(%f), BUFFER UNDERRUN 1, myID: %d, total: %d \n", curTime, myID, bufferUnderruns);
//#endif
//      fprintf (fp_statlog, "%f VACLISTAT1 %d %d %f %f %f %f %f %d\n", curTime,1, total_size, maxbyte, ratio, frameRateSample, avg, aavg,bufferUnderruns);
      //VACLISTAT1
      fprintf (fp_statlog, "%f %d %d %d %f %d %f %f %f %f %d %d\n", curTime,0, myID, total_size, maxbyte, last_bitrate,frameRateSample, avg, aavg,buf_cur_size,bufferUnderruns, adaptState);
      last_player_read = curTime;

//$A13
//INCREASE SCALE
      scalePlaybackBuffer(UNDERRUN_SCALE_EVENT);

      abuf = abuf * aavg_delta + (double)buf_cur_size;
      TRACE_VA ("wait for buffer full, buf_cur_size=%f, buf_thres_player=%f.\n", buf_cur_size, buf_thres_player);
//$A7
      clientUpdate ();
      return;
    }

//JJM TEMP
   //If this is true, we underrun  and go into buffering 
   if (buf_cur_size < packet_interval ) {
     fprintf (fp_statlog, "%f %d %d %d %f %d %f %f %f %f %d %d\n", curTime,1, myID, total_size, maxbyte, last_bitrate, frameRateSample, avg, aavg,buf_cur_size,bufferUnderruns, adaptState);
     bufferUnderruns++;
     last_player_read = curTime;
     abuf = abuf * aavg_delta + (double)buf_cur_size;
     TRACE_VA ("wait for buffer full, buf_cur_size=%f, buf_thres_player=%f.\n", buf_cur_size, buf_thres_player);
//$A15: go into buffering mode
      if (is_player_run == true){
        is_player_run = false;
        rebufferingStartTime = curTime;
        rebufferingEvents++;
//#ifdef TRACEME 
        printf ("StreamingClient:consumeVideo:(%f): MOVE TO BUFFERING MODE 2  buf_cur_size:%lf, bufferUnderruns:%d, rebufferingEvents:%d \n", curTime,buf_cur_size,bufferUnderruns,rebufferingEvents);
//#endif
      }
     scalePlaybackBuffer(UNDERRUN_SCALE_EVENT);
     clientUpdate ();
     return;
   }


    //if player is NOT running....we need to see if we should move out of buffering mode
    //If still in buffering, it's an underrun
    if (is_player_run == false) {
        //And if the buffer has less than the min amount the player needs
        //To remove buffering mode, set buf_thres_player to packet_interval
//        if (buf_cur_size < packet_interval ) {
//        if (buf_cur_size < 2 *request_interval ) {
        if (buf_cur_size < buf_thres_player) {
//            fprintf (fp_statlog, "%f VACLISTAT1 %d %d %f %f %f %f %f %d\n", curTime, 1, total_size, maxbyte, ratio, frameRateSample, avg, aavg,bufferUnderruns);
      //VACLISTAT2

            fprintf (fp_statlog, "%f %d %d %d %f %d %f %f %f %f %d %d\n", curTime,2, myID, total_size, maxbyte, last_bitrate, frameRateSample, avg, aavg,buf_cur_size,bufferUnderruns, adaptState);

            last_player_read = curTime;
            abuf = abuf * aavg_delta + (double)buf_cur_size;
            TRACE_VA ("wait for buffer full, buf_cur_size=%f, buf_thres_player=%f.\n", buf_cur_size, buf_thres_player);
//JJM ?
//$A13
            bufferUnderruns++;
            scalePlaybackBuffer(UNDERRUN_SCALE_EVENT);
//#ifdef TRACEME 
            printf ("StreamingClient:consumeVideo:(%f): BUFFER UNDERRUN 2 (total%d), myID: %d, buf_cur_size:%lf, packet_interval:%lf   \n", curTime,bufferUnderruns, myID, buf_cur_size,packet_interval);
//#endif
//$A7
            clientUpdate ();
            return;
        }
        else {
            is_player_run = true;
            double tmpTime = curTime - rebufferingStartTime;
            if (tmpTime > 0) {
              totalTimeRebuffering += tmpTime;
              rebufferingCounter++;
            }
            else {
              printf ("StreamingClient:consumeVideo:(%f): HARD ERROR, tmpTime is %lf \n", curTime,tmpTime);
            }
//#ifdef TRACEME 
            printf ("StreamingClient:consumeVideo:(%f): END BUFFERING MODE  buf_cur_size:%lf, bufferUnderruns:%d, rebufferingEvents:%d \n", curTime,buf_cur_size,bufferUnderruns,rebufferingEvents);
//#endif
        }
    }

    //Ignore
    for (; ! buf_queue.empty(); ) {
        if (buf_queue.front().consumed >= buf_queue.front().size) {
            buf_queue.pop_front ();
        } else {
            break;
        }
    }

    if (is_live) {
        // it is live streaming, so
        // remove the old content.
        // ....
        for (; ! buf_queue.empty(); ) {
            if (buf_queue.front().consumed >= buf_queue.front().size) {
                buf_queue.pop_front ();
            } else {
                break;
            }
        }
    }
    if (is_player_run) {
        double timeleft = getInterval();
        double timeconsumed = 0.0;
        size_t packet_size0 = 0;
        size_t packet_size = 0;
        int lastbitrate = 1;
#ifdef TRACEME 
        printf ("StreamingClient:consumeVideo:(%f),loop, drain queue until have removed %6.6f seconds of video data \n", curTime,timeleft);
#endif
        for (; (timeleft > 0.0) && (! buf_queue.empty());) {
            if (lastbitrate == buf_queue.front().bitrate) {
                TRACE_VA ("packet_size0(%d) - packet_size(%d)= %d!\n", packet_size0, packet_size, packet_size0 - packet_size);
                packet_size0 -= packet_size;
            } else {
                // step to new bitrate
                assert (lastbitrate >= 1);
                double thistime = ((double)packet_size * 8 / (double)lastbitrate);
                TRACE_VA ("packet_size(%d) * 8 / lastbitrate(%d)= %f!\n", packet_size, lastbitrate, thistime);
                timeconsumed += thistime;
                timeleft -= thistime;
                if (timeconsumed < 0) {
                  timeconsumed = 0.0;
                  printf ("StreamingClient:consumeVideo:(%f),WARNING : reset a negative timeconsumed to 0.0 \n", curTime);
                }
                TRACE_VA ("timeleft = %f!\n", timeleft);
                // calculate the packet size to be fetched
                packet_size0 = timeleft * buf_queue.front().bitrate / 8;
                TRACE_VA ("next packet_size0=%d!\n", packet_size0);
            }
            lastbitrate = buf_queue.front().bitrate;
            packet_size = buf_queue.front().size - buf_queue.front().consumed;
            TRACE_VA ("packet_size = %d, packet_size0 = %d!\n", packet_size, packet_size0);
            if (packet_size < 0) {
              packet_size= 0;
              printf ("StreamingClient:consumeVideo:(%f),WARNING : reset a negative packet_size to 0.0 \n", curTime);
            }
            if (packet_size > packet_size0) {
                packet_size = packet_size0;
                timeleft = 0.0;
                TRACE_VA ("buf_queue.front().bitrate=(%d); lastbitrate(%d)\n", buf_queue.front().bitrate, lastbitrate);
                TRACE_VA ("packet_size(%d) * 8 / lastbitrate(%d)= %f!\n", packet_size, lastbitrate, ((double)packet_size * 8 / (double)lastbitrate));
                timeconsumed += ((double)packet_size * 8 / (double)lastbitrate);
            }
            total_size += packet_size;
            buf_queue.front().consumed += packet_size;
            if (buf_queue.front().consumed >= buf_queue.front().size) {
                TRACE_VA ("pop front!\n");
                buf_queue.pop_front ();
            }
           if ((timeleft > 0.0) && (buf_queue.empty())) {
              bufferUnderruns++;
              scalePlaybackBuffer(UNDERRUN_SCALE_EVENT);
//#ifdef TRACEME 
              printf ("StreamingClient:consumeVideo:(%f): BUFFER UNDERRUN 3 (total%d)  (timeleft :%lf \n", curTime,bufferUnderruns,timeleft);
//#endif
            }

        }
#ifdef TRACEME 
        printf ("StreamingClient:consumeVideo:(%f),Ended draining queue, consumed %d bytes of data, curbufsize:%6.6f, timeconsumed:%6.6f \n", curTime,total_size,buf_cur_size,timeconsumed);
#endif
        if (timeleft > 0) {
            TRACE_VA ("buf_cur_size(%f) set to 0!\n", buf_cur_size);
            buf_cur_size = 0.0;
            while (! buf_queue.empty()) {
                buf_queue.pop_front ();
            }
            cal_frame_error (true);
        } else {
            timeleft = 0.0;
            TRACE_VA ("buf_cur_size(%f) - timeconsumed(%f)!\n", buf_cur_size, timeconsumed);
            buf_cur_size -= timeconsumed;
            cal_frame_error (false);
        }
#ifdef TRACEME 
//Show current size in seconds and amount of time removed
        fprintf (fp_buflog, "%f VACLIBUFREMOVE %f %f\n", curTime, buf_cur_size, timeconsumed);
#endif
//What is avg used for.  
//what is the difference between avg_bytes and total_bytes ?
        avg = 0;
        if (curTime > latest_aavg) {
            avg = (double)avg_bytes * 8 / (curTime - latest_aavg);
        }
        TRACE_VA ("avg=%f, avg_bytes=%d, thistime=%f, latest_aavg=%f\n", avg, avg_bytes, curTime, latest_aavg);

        TRACE_VA ("after consume size %d, buf_cur_size=%f!\n", total_size, buf_cur_size);
        // the size consumed / max size
        ratio = (double) total_size / maxbyte;
        abuf = abuf * aavg_delta + (double)buf_cur_size;
        //$A5
//        fprintf (fp_statlog, "%f VACLISTAT %d %f %f \n", Scheduler::instance().clock(), total_size, maxbyte,ratio);
        frameRateSample =  (double (total_size * 8)) / (curTime - last_player_read);
//        fprintf (fp_statlog, "%f VACLISTAT2 %d %f %f %f %f %f %d\n", curTime, total_size, maxbyte, ratio, frameRateSample, avg, aavg, bufferUnderruns);
        //VACLISTAT3

        fprintf (fp_statlog, "%f %d %d %d %f %d %f %f %f %f %d %d\n", curTime,3, myID, total_size, maxbyte, last_bitrate, frameRateSample, avg, aavg,buf_cur_size,bufferUnderruns, adaptState);

        last_player_read = curTime;
//$A13
        if (buf_cur_size < bufRedYellowThreshold)
          bufferState= RED;
        else if (buf_cur_size < bufYellowGreenThreshold)
          bufferState= YELLOW;
        else
          bufferState= GREEN;

    } else {
        printf ("StreamingClient:consumeVideo:(%f) (myID:%d) HARD ERROR 1 \n", Scheduler::instance().clock(),myID);
    }

    clientUpdate ();
}


/************************************************************************
*  
* Function: void StreamingClient::clientUpdate()
*  
* Inputs:
*   
* Explanation:
*  This does two things.  First, it determines if an adaptation is needed.
*  Second, it determines if a new client request should be issued
* 
*  pseudo code :
*     last_requests: A structure that queues pending client requests.
*
*******************************************************************/
void StreamingClient::clientUpdate()
{
    double this_time = Scheduler::instance().clock();
    double timeSinceLastChange = this_time - bitRateSwitchTime;
    double switching_interval_real = 0;
    int atMaxBitRate = 0;  //set to 1 if we are at the max quality


    TRACE_VA ("BEGIN\n");

    if (! running_) {
        TRACE_VA ("Warning: not running!\n");
        return;
    }

    //DASHMode 0 and 3 here
    if (DASHMode == 0) {
      int req = 0;  //number of packets requested but not yet recieved
      double sec_unreceived = 0.0;    //length of packets (in seconds) already requested but not yet recieved
      for (std::deque<video_content_t>::iterator it = last_requests.begin(); it != last_requests.end(); it ++) {
        sec_unreceived += ((double)(it->size - it->consumed) * 8 / (double)it->bitrate);
        req++;
      }
      
      if (buf_cur_size + request_interval + sec_unreceived > buf_max_video) {
        return; // if we dont have buffer space for another video segment, dont request another
      }
      
      video_content_t cnt;
      
      cnt.bitrate = bitrate_levels.back();
      cnt.size = request_interval * cnt.bitrate / 8;
      cnt.consumed = 0;
      
      last_bitrate = cnt.bitrate;
      StreamingApplicationData * p = new StreamingApplicationData (SZ_REQUEST, getSequence(), cnt.bitrate, cnt.size);
      
      // request the packet
      last_requests.push_back (cnt);
      
      totalBytesSent += SZ_REQUEST;
      totalMessagesSent++;
      if (firstSendTime == 0)
        firstSendTime = this_time;
      
      send (SZ_REQUEST, p);
      
      return;
      
    } else if (DASHMode == 3) {
      int req = 0;  //number of packets requested but not yet recieved
      double sec_unreceived = 0.0;    //length of packets (in seconds) already requested but not yet recieved
      for (std::deque<video_content_t>::iterator it = last_requests.begin(); it != last_requests.end(); it ++) {
        sec_unreceived += ((double)(it->size - it->consumed) * 8 / (double)it->bitrate);
        req++;
      }
      
      if (buf_cur_size + request_interval + sec_unreceived > buf_max_video || req != 0) {
        return; // if we dont have buffer space for another video segment, dont request another
      }
      
      int rate = dash3();    //the datarate the next piece should be requested at
      video_content_t cnt;
      
      cnt.bitrate = rate;
      cnt.size = request_interval * cnt.bitrate / 8;
      cnt.consumed = 0;
      
      last_bitrate = cnt.bitrate;
      StreamingApplicationData * p = new StreamingApplicationData (SZ_REQUEST, getSequence(), cnt.bitrate, cnt.size);
      
      // request the packet
      last_requests.push_back (cnt);
      
      totalBytesSent += SZ_REQUEST;
      totalMessagesSent++;
      if (firstSendTime == 0)
        firstSendTime = this_time;
      
      send (SZ_REQUEST, p);
      
      return;
    }
    
    
    
    
//$A6
//   double switching_interval_real = (1 - adaptation_sensitivity) * 10.0 * switching_interval_reduction + adaptation_sensitivity * switching_interval_reduction / 10.0;

    if (last_requests.size () > 0) {
        last_aavg = aavg;
    //} else {
        //time_lower = 0.0;
        //time_higher = 0.0;
    }
#ifdef TRACEME 
    printf ("StreamingClient:clientUpdate(%f), myID:%d, state:%d, last_bitrate:%d , updated average : %6.6f, switching interval:%lf, time since Last:%lf  \n", this_time,myID, adaptState, last_bitrate,aavg, switching_interval_real, (this_time- bitRateSwitchTime));
#endif

    // check the buffer threshold
    if (! last_requests.empty()) {
        last_bitrate = last_requests.back().bitrate;
    }
    //TRACE_VA ("last_req.size=%d\n", last_requests.size());
    TRACE_VA ("last_bitrate=%d bps\n", last_bitrate);

    //TRACE_VA ("aavg=%f bps\n", aavg);
    TRACE_VA ("last_aavg=%f bps\n", last_aavg);
    TRACE_VA ("time_lower 1 =%f\n", time_lower);
    TRACE_VA ("buf_cur_size=%f seconds\n", buf_cur_size);
    //TRACE_VA ("buf_thres_bitrate=%f seconds\n", buf_thres_bitrate);
    //TRACE_VA ("buf_thres_player=%f seconds\n", buf_thres_player);

    int i;
    for (i = 0; i < bitrate_levels.size(); i ++) {
        if (bitrate_levels[i] >= last_bitrate) {
            break;
        }
    }
    if (i >= bitrate_levels.size()) {
        i = bitrate_levels.size() - 1;
    }
    if (i == bitrate_levels.size()-1) {
      atMaxBitRate = 1;  
    }
    //TRACE_VA ("bitrate level[%d]=%d\n", i, bitrate_levels[i]);
    bool flg_levelchg = false;
//$A8
//    size_t bitrate_threshold_increase;
    int bitrate_threshold_increase;
    if (atMaxBitRate == 1){
      //in this case, we will never want to increase...but we might want to decrease
      bitrate_threshold_increase =  2 * (double)(bitrate_levels[i]);
    } else {
      bitrate_threshold_increase = (1 - ratio_threshold_increase) * (double)(bitrate_levels[i])
                                    + ratio_threshold_increase * (double)(bitrate_levels[i + 1]);
    }

//$A8
//    size_t bitrate_threshold_reduction;
    int bitrate_threshold_reduction;
    if (i == 0){
      //in this case we should not decrease
      bitrate_threshold_reduction =  2 * (double)(bitrate_levels[i]);
    } else {
      bitrate_threshold_reduction = (1 - ratio_threshold_reduction) * (double)(bitrate_levels[i])
          + ratio_threshold_reduction * (double)(bitrate_levels[i - 1]);
    }
#ifdef TRACEME 
    printf ("StreamingClient:clientUpdate(%f), state:%d (atMaxBitRate:%d), CONSIDER SWITCHING(current bitrate:%d) last_aavg:%9.6f, bitrate_threshold_increase:%d, bitrate_threshol_decrease:%d \n", this_time,adaptState,atMaxBitRate, bitrate_levels[i],last_aavg,bitrate_threshold_increase,bitrate_threshold_reduction);
#endif
    //If our current aavg exceeds our UP threshold, consider switching to a higher bit rate.
    if (last_aavg >= bitrate_threshold_increase) {
        TRACE ("last_aavg(%f) >= bitrate_threshold_increase(%f) \n", last_aavg, bitrate_threshold_increase);

        // let \beta = adaptation_sensitivity
        // let \alpha = 10
        // let T = switching_interval
        // interval = (1 - \beta) \alpha T + \beta \frac{T}{\alpha}
        switching_interval_real = (1 - adaptation_sensitivity) * 10.0 * switching_interval_increase + adaptation_sensitivity * switching_interval_increase / 10.0;
#ifdef TRACEME 
        printf ("StreamingClient:clientUpdate(%f), state:%d,,i:%d, MAYBE SWITCH 1,  updated average : %6.6f, switching interval:%lf, time since Last change::%lf, dynamicAdaptTimeWait:%lf  \n", this_time,adaptState, i,  aavg, switching_interval_real, timeSinceLastChange,dynamicAdaptTimeWait);
#endif

        TRACE_VA ("reset time_lower to 0 \n");
        time_lower = 0.0;
        if (0.0 == time_higher) {
            TRACE_VA ("time_higher =%f replaced with %f\n", time_higher, this_time);
            time_higher = this_time;
        } else if (time_higher + switching_interval_real <= this_time) {
            // the process should wait a small mount of time to make sure the bw is 'really' increased.
            TRACE_VA ("time_higher 4 =%f\n", time_higher);
            // switch to higher bitrate
            if (i < bitrate_levels.size() - 1) {
                // add a higer request
                flg_levelchg = true; // postpone setting the time_lower=0 to later while() loop
                i ++;
                TRACE_VA ("bitrate[%d]=%d\n", i, bitrate_levels[i]);
            }
        }

    } else if (last_aavg <= bitrate_threshold_reduction) {
        TRACE ("last_aavg(%f) <= bitrate_threshold_reduction(%f) \n", last_aavg, bitrate_threshold_reduction);
        TRACE_VA ("reset time_higher to 0 \n");
        time_higher = 0.0;
        if (0.0 == time_lower) {
            TRACE_VA ("time_lower =%f replaced with %f\n", time_lower, this_time);
            time_lower = this_time;
        } else {
            double timeleftavg = buf_cur_size;

        // let \beta = adaptation_sensitivity
        // let \alpha = 10
        // let T = switching_interval
        // interval = (1 - \beta) \alpha T + \beta \frac{T}{\alpha}
         switching_interval_real = (1 - adaptation_sensitivity) * 10.0 * switching_interval_reduction + adaptation_sensitivity * switching_interval_reduction / 10.0;
#ifdef TRACEME 
        printf ("StreamingClient:clientUpdate(%f), state:%d,,i:%d, MAYBE SWITCH 2,  updated average : %6.6f, switching interval:%lf, time since Last:%lf, dynamicAdaptTimeWait:%lf  \n", this_time,adaptState, i,  aavg, switching_interval_real, timeSinceLastChange,dynamicAdaptTimeWait);
        printf ("StreamingClient:clientUpdate(%f), state:%d,time_lower:%lf,(lower+interval:%lf), buf_thresh_bitrate::%lf, laste_bitrate:%d, request_interval:%f  \n", this_time,adaptState, time_lower, (time_lower+ switching_interval_real),buf_thres_bitrate, last_bitrate, request_interval);
#endif

            //for (i = 0; i < buf_queue.size (); i ++) {
                //timeleftavg += ((double)(buf_queue[i].size - buf_queue[i].consumed) / (double)buf_queue[i].bitrate)
            //}
            TRACE_VA ("time_lower=%f; switching_interval_real=%f, this_time=%f\n", time_lower, switching_interval_real, this_time);
            TRACE_VA ("buf_cur_size=%f; buf_thres_bitrate=%f, last_bitrate=%d, request_interval=%f\n", buf_cur_size, buf_thres_bitrate, last_bitrate, request_interval);
            if (time_lower + switching_interval_real <= this_time) {
                // the process should wait a small mount of time to make sure the bw is 'really' dropped.
                // switch to lower bitrate
                if (i > 0) {
                    flg_levelchg = true; // postpone setting the time_lower=0 to later while() loop
                    i --;
                }
                TRACE_VA ("bitrate[%d]=%d\n", i, bitrate_levels[i]);
           }
        }
        if (! flg_levelchg) {
         //if necessary, lower the bitrate as our buffer is about full
        if (abuf <= (double)buf_thres_bitrate + request_interval) {
            if (i > 0) {
//$A7
            flg_levelchg = true;
             i --;
            if (i<0)
             i=0;
            TRACE_VA ("bitrate[%d]=%d\n", i, bitrate_levels[i]);
           }
        }
      }
    }


    double sec_unreceived = 0.0;
    for (std::deque<video_content_t>::iterator it = last_requests.begin(); it != last_requests.end(); it ++) {
        sec_unreceived += ((double)(it->size - it->consumed) * 8 / (double)it->bitrate);
    }
    //TRACE_VA ("sec_unreceived=%f\n", sec_unreceived);
//Check
    if (i >= bitrate_levels.size()) 
      printf ("StreamingClient:clientUpdate(%f), myID:%d HARD ERROR 2, indexed beyond max bitrate level ?? \n", this_time,myID);
    if (i < 0) 
      printf ("StreamingClient:clientUpdate(%f), myID:%d HARD ERROR 3, indexed below 0 bitrate level ?? \n", this_time, myID);

#ifdef TRACEME 
//        if (flg_levelchg == true) {
          printf ("StreamingClient:clientUpdate(%f), state:%d,flg_levelchg:%d, MAYBE SWITCH 3,  updated average : %6.6f, switching interval:%lf, time since Last:%lf, dynamicAdaptTimeWait:%lf  \n", this_time,adaptState, flg_levelchg,  aavg, switching_interval_real, timeSinceLastChange,dynamicAdaptTimeWait);
//       }
          printf ("StreamingClient:clientUpdate(%f), last_requests.size:%d, max_requests:%d, (buf_cur_size+sec_unreceived):%f, buf_max_video:%lf_  \n", 
                 this_time,last_requests.size(),max_requests,(buf_cur_size+sec_unreceived), buf_max_video);
#endif

    while ((last_requests.size() < max_requests) && (buf_cur_size + sec_unreceived < buf_max_video)) {
        // check if the number of request > max_requests, then return

        video_content_t cnt;
        cnt.bitrate = bitrate_levels[i];
//$A7
        //Check if we really want to adapt - if we did 
        if (flg_levelchg == true) {
//          double timeSinceLastChange = this_time - bitRateSwitchTime;
          if (last_bitrate - cnt.bitrate > 0 ) {
#ifdef TRACEME 
            printf ("StreamingClient:clientUpdate(%f), state:%d, i:%d, SWITCH DOWN:  last_bitrate:%d , new bitrate:%d, updated average : %6.6f, switching interval:%lf, time since Last:%lf, dynamicAdaptTimeWait:%lf  \n", 
                     this_time,adaptState, i, last_bitrate,cnt.bitrate, aavg, switching_interval_real, timeSinceLastChange,dynamicAdaptTimeWait);
            printf ("StreamingClient:clientUpdate:       SWITCH DOWN:  last_bitrate-new bitrate:%d, as double: %f \n", 
               (last_bitrate-cnt.bitrate), ((double) last_bitrate  - (double) cnt.bitrate));
             printf ("StreamingClient:clientUpdate(%f), BEGIN CONTROLLER SWITCH DOWN myID:%d state:%d, i:%d, DASHMode:%d, bufferState:%d  \n", this_time,myID,adaptState, i,DASHMode,bufferState);
#endif
            switch (adaptState) {

//#define AGGRESSIVE_DOWN 0
//#define AGGRESSIVE_UP 1
//#define CAUTIOUS_UP 2
//#define CAUTIOUS_DOWN 3

              case  CAUTIOUS_DOWN:
#ifdef TRACEME 
                printf ("StreamingClient:clientUpdate(%f), myID:%d state:%d, i:%d, HARD ERROR 4, move to CAUTIOUS_DOWN,DASHMode:%d, bufferState:%d  \n", this_time,myID,adaptState, i,DASHMode,bufferState);
#endif
                if (DASHMode == 2){
                  if (bufferState== RED) {
                    i= i-2;
                    if (i < 0)
                     i=0;
                  }
                  else if (bufferState== YELLOW) {
                    i--;
                    if (i < 0)
                     i=0;
                  }
                }

                break;
 
              case  CAUTIOUS_UP:
                adaptState = CAUTIOUS_DOWN;
                bitRateSwitchTime = this_time;
                dynamicAdaptTimeWait = minStabilizeTime;
                if (DASHMode == 2){
                  if (bufferState== RED) {
                    i= i-2;
                    if (i < 0)
                     i=0;
                  }
                  else if (bufferState== YELLOW) {
                    i--;
                    if (i < 0)
                     i=0;
                  }
                }
                break;

              default:
                dynamicAdaptTimeWait = minRateDownTime;
                bitRateSwitchTime = this_time;
                adaptState = CAUTIOUS_DOWN;
                if (DASHMode == 2){
                  if (bufferState== RED) {
                    i= i-2;
                    if (i < 0)
                     i=0;
                  }
                  else if (bufferState== YELLOW) {
                    i--;
                    if (i < 0)
                     i=0;
                  }
                }
                break;
            }

#ifdef TRACEME 
           printf ("StreamingClient:clientUpdate(%f), END CONTROLLER SWITCH DOWN myID:%d state:%d, i:%d, DASHMode:%d, bufferState:%d  \n", this_time,myID,adaptState, i,DASHMode,bufferState);
#endif

            if (timeSinceLastChange < dynamicAdaptTimeWait) {
              //override
              i++;
              if (i >= bitrate_levels.size()) {
                i=bitrate_levels.size()-1;
              } else
                flg_levelchg = false;
            }
            else { 
              dynamicAdaptTimeWait = 2 * dynamicAdaptTimeWait;
              if (dynamicAdaptTimeWait >  3 * minStabilizeTime)
                 dynamicAdaptTimeWait = minStabilizeTime;
            }
#ifdef TRACEME 
           printf ("StreamingClient:clientUpdate(%f), END CONTROLLER 2 SWITCH DOWN myID:%d state:%d, i:%d, DASHMode:%d, bufferState:%d  \n", this_time,myID,adaptState, i,DASHMode,bufferState);
#endif
        } else {
#ifdef TRACEME 
            printf ("StreamingClient:clientUpdate(%f), state:%d,,i:%d, SWITCH UP:  last_bitrate:%d , new bitrate:%d, updated average : %6.6f, switching interval:%lf, time since Last:%lf, dynamicAdaptTimeWait:%lf  \n", this_time,adaptState, i, last_bitrate,cnt.bitrate, aavg, switching_interval_real, timeSinceLastChange,dynamicAdaptTimeWait);
#endif
            switch (adaptState) {

              case  CAUTIOUS_DOWN:
                adaptState = CAUTIOUS_UP;
                bitRateSwitchTime = this_time;
                dynamicAdaptTimeWait = minStabilizeTime;
                break;
 
              case  CAUTIOUS_UP:
                break;
 
              default:
                printf ("StreamingClient:clientUpdate(%f), myID:%d, state:%d, i:%d, HARD ERROR 5 move to CAUTIOUS_UP  \n", this_time,myID,adaptState, i);
                dynamicAdaptTimeWait = minRateUpTime;
                bitRateSwitchTime = this_time;
                adaptState = CAUTIOUS_UP;
                break;
            }
            if (timeSinceLastChange < dynamicAdaptTimeWait) {
              //override
              i--;
              flg_levelchg = false;
            } else {
                dynamicAdaptTimeWait -= dynamicAdaptTimeWait/4;
                if (dynamicAdaptTimeWait < minRateUpTime)
                   dynamicAdaptTimeWait = minRateUpTime;
//                dynamicAdaptTimeWait = 2 * dynamicAdaptTimeWait;
            }
          }

        }

        cnt.bitrate = bitrate_levels[i];
        cnt.size = request_interval * cnt.bitrate / 8;
        cnt.consumed = 0;

        TRACE_VA ("bitrate[%d]=%d\n", i, cnt.bitrate);
        TRACE_VA ("buf_cur_size(%f sec) + sec_unreceived(%f) + request_interval(%f) =%f\n", buf_cur_size, sec_unreceived, request_interval, buf_cur_size + sec_unreceived + request_interval);
        TRACE_VA ("buf_max_video=%f\n", buf_max_video);

        //We reach this point all set to issue a new request to the server. 
        // We first need to find out if a request_interval will fit in our queue
        if (buf_cur_size + sec_unreceived + request_interval > buf_max_video) {
            if (buf_max_video > sec_unreceived + buf_cur_size) {
                cnt.size = (size_t)((double)((buf_max_video - sec_unreceived - buf_cur_size) / 8) * (double)cnt.bitrate);
            }
        }
#ifdef TRACEME 
        printf ("StreamingClient:clientUpdate(%f), state:%d, i:%d, cnt size:%d, cnt.bitrate:%d, request_interval:%lf, sec_unreceived:%lf, buf_max_video:%lf,buf_cur_size:%lf  \n", this_time,adaptState, i, cnt.size, cnt.bitrate, request_interval,sec_unreceived,buf_max_video,buf_cur_size);
        printf ("StreamingClient:clientUpdate(%f),   last_bitrate:%d , updated average : %6.6f  timeSinceLastChange:%lf, dynamicAdaptTimeWait:%lf \n", 
            this_time, last_bitrate,aavg, timeSinceLastChange, dynamicAdaptTimeWait);
#endif

        //What does it mean if we reeach here and this is true??
        if (cnt.size < 1) {
            break;
        }

        // request the packet
        last_requests.push_back (cnt);

        if (flg_levelchg == true) {
          bitRateSwitchCount++;
          bitRateSwitchTime = this_time;
          timeSinceLastChange = 0.0;
#ifdef TRACEME 
            printf ("StreamingClient:clientUpdate(%f),myID:%d, state:%d, REALLY SWITCH last_bitrate:%d , new bitrate:%d, updated average : %6.6f, switching interval:%lf, time since Last:%lf, dynamicAdaptTimeWait:%lf, bitrate_threshold_increase:%d, bitrate_threshol_decrease:%d \n", this_time,myID,adaptState, last_bitrate,cnt.bitrate, aavg, switching_interval_real, timeSinceLastChange,dynamicAdaptTimeWait,bitrate_threshold_increase,bitrate_threshold_reduction);
#endif
        }

        last_bitrate = cnt.bitrate;

        StreamingApplicationData * p = new StreamingApplicationData (SZ_REQUEST, getSequence(), cnt.bitrate, cnt.size);
        TRACE_VA ("cnt.size=%d; p=0x%08X\n", cnt.size, p);
        assert ((cnt.size > 0) && (p != NULL));
        // the packet is a request message, so the size should less than 20 bytes
        TRACE_VA ("send request (pktsize=%d), bitrate=%d, size=%d\n", SZ_REQUEST, cnt.bitrate, cnt.size);
        TRACE_VA ("buf_queue.size=%d, last_requests.size=%d\n", buf_queue.size(), last_requests.size());

        //$A0
        totalBytesSent += SZ_REQUEST;
        totalMessagesSent++;
        if (firstSendTime == 0)
          firstSendTime = this_time;

#ifdef TRACEME 
        printf ("clientUpdate: (%f) After adjustment,  VACLIREQ bitrate:%d size:%d, \n", this_time, cnt.bitrate, cnt.size);
//        fprintf (fp_clireqlog, "%f VACLIREQ %d %d\n", this_time, cnt.bitrate, cnt.size);
#endif
//$A6

        fprintf (fp_clireqlog, "%f %d %d %d %lf %lf %d %lf %lf %d %d %d\n", this_time, myID,  cnt.bitrate, cnt.size, switching_interval_real, switching_interval_reduction, last_bitrate,buf_cur_size,aavg,adaptState,bitrate_threshold_increase,bitrate_threshold_reduction);
//        fprintf (fp_clireqlog, "%f %d %d %d %lf %lf %lf %lf %lf %d %d\n", this_time,1, cnt.size, cnt.size, cnt.bitrate, cnt.bitrate, aavg, aavg,buf_cur_size,bufferUnderruns, adaptState);
//            fprintf (fp_statlog, "%f %d %d %f %d %f %f %f %f %d %d\n", curTime,2, total_size, maxbyte, last_bitrate, frameRateSample, avg, aavg,buf_cur_size,bufferUnderruns, adaptState);

        send (SZ_REQUEST, p);
        if (flg_levelchg) {
            time_lower = 0.0;
            time_higher = 0.0;
            TRACE_VA ("set time_lower and time_higher to 0.0\n");
        }

        for (std::deque<video_content_t>::iterator it = last_requests.begin(); it != last_requests.end(); it ++) {
            sec_unreceived += ((double)(it->size - it->consumed) * 8 / (double)it->bitrate);
        }
        TRACE_VA ("sec_unreceived=%f\n", sec_unreceived);
    }

//$A13
    if (bufferState== GREEN)
      scalePlaybackBuffer(GREEN_SCALE_EVENT);
    if (bufferState== RED)
      scalePlaybackBuffer(RED_SCALE_EVENT);
}

/*
void
StreamingClient::recv (int size)
{
    fprintf (fp_pkglog, "%f VACLIRECVPKG %d %d\n", Scheduler::instance().clock(),
        data->getSequence(), data->getReqSize(), data->getBitrate(), data->getSelfSize());

    StreamingApplication::recv (size);
}
*/

/*
 * Implements the streaming alg from "A Buffer Based Approach to Rate Adaptation"
 * 
 * An entirely buffer based algorithm for determining when to change bitrates.
 * To implement, need to change StreamingClient class, alter UpdateClient function, declare last_bitmap_index in init
 */
int StreamingClient::dash3() {
  /* Key variables from elsewhere:
   * buf_cur_size - the current size of the buffer in seconds
   * buf_max_video - max size of buffer in seconds
   * bitrate_levels[] - the bitrates available for the video, scales with buf_max_video and number of bitrates available
   * last_bitrate - the last bitrate used
   * last_bitmap_index - the index of the bitrate_map value we were last at... 
   */
  //double bitrate_map[] = {90.0, 100.0, 120.0, 130.0, 140.0, 150.0, 160.0, 180.0}; //the buffer levels in seconds to change datarate at ##NEED TO FILL REAL VALUES 15.75s## SHOULD BE 1:1 with bitrate_levels[]
  //double buf_reservoir = 90.0;  //the "safety zone" of the buffer in seconds. if buffer level falls below this, switch to minimum bitrate
  int next_rate = 0;  //the rate at which we will request the next video segment
  int rate_up;
  int rate_down;
  
  int numBitRates = bitrate_levels.size();
  double bitrate_map[numBitRates];
  double buf_reservoir = buf_max_video * 0.375;
  double buf_upper_reservoir = buf_max_video * 0.90;
  double time_spacing = (buf_upper_reservoir - buf_reservoir) / (numBitRates - 1);
  
  bitrate_map[0] = buf_reservoir;
  bitrate_map[numBitRates-1] = buf_upper_reservoir;
  
  //currently this is remade every time it is called, would be better to have bitrate_map in vodappClient.h
  //unfortunately, the setup of the model doesn't allow this at this time DO THIS
  int i;
  for (i = 1; i < numBitRates-1; i++){
    bitrate_map[i] = bitrate_map[i-1] + time_spacing;
  }
  
  if (! last_requests.empty()) {
    last_bitrate = last_requests.back().bitrate;
  }

  // have current buffer amount
  // have bitrate map for the exact buffer levels to switch
  if (last_bitmap_index == numBitRates - 1) {
    rate_up = last_bitmap_index;
  } else {
    rate_up = last_bitmap_index + 1;
  }
  
  if (last_bitmap_index == 0) {
    rate_down = 0;
  } else {
    rate_down = last_bitmap_index - 1;
  }
  
  //printf("StreamingClient::dash3(): buf_cur_size: %lf; time: %f\n", buf_cur_size, Scheduler::instance().clock());

  if (buf_cur_size < buf_reservoir) {
    next_rate = bitrate_levels[0];
    last_bitmap_index = 0;
  } else if (buf_cur_size >= bitrate_map[numBitRates-1]) {
    next_rate = bitrate_levels[numBitRates-1];
    last_bitmap_index = numBitRates-1;
  } else if (buf_cur_size >= bitrate_map[rate_up]) { 
    next_rate = bitrate_levels[rate_up];
    last_bitmap_index = rate_up;
  } else if(buf_cur_size < bitrate_map[rate_down]) {
    next_rate = bitrate_levels[rate_down];
    last_bitmap_index = rate_down;
  } else {
    next_rate = last_bitrate;
  }
  
  if (last_bitrate != next_rate) {
    bitRateSwitchCount++;
    printf ("StreamingClient::dash3(): id: %d, New bitrate: %d; Current Buffer Amount: %lf; Max Buffer Size: %f; Time: %f\n", myID, next_rate, buf_cur_size, buf_max_video, Scheduler::instance().clock());
  }
  
  if (buf_cur_size > buf_max_video) {
    printf("StreamingClient::dash3(): Buffer overrun! Time: %f; buf_cur_size: %lf; buf_max_video: %lf\n", Scheduler::instance().clock(), buf_cur_size, buf_max_video);
  }
  
  return next_rate;
}

/************************************************************************
*
* Function: void StreamingClient::process_data (int size, AppData* data)
*
* Inputs:
*   size : amount of data that arrived
*   AppData *data : ptr to data
*   
* Explanation:
*   This method is invoked when data has arrived at the client.
*
***********************************************************************/
void StreamingClient::process_data (int size, AppData* data)
{
    TRACE_VA ("process_data sz=%d\n", size);
#ifdef TRACEME 
    printf ("StreamingClient:process_data(%f), %d bytes arrived\n", Scheduler::instance().clock(),size);
#endif
    if (data == NULL) {
        TRACE_VA ("Error in data!\n");
        return;
    }
    StreamingApplicationData *pvd = (StreamingApplicationData *) data;
    // format of the package line: clock, VACLIRECVPKG/VACLIPROCPKG, sequecne, reqestsize, bitrate, size
#ifdef TRACEME 
    fprintf (fp_pkglog, "%f VACLIPROCPKG %d %d %d %d\n", Scheduler::instance().clock(),
        pvd->getSequence(), pvd->getReqSize(), pvd->getBitrate(), pvd->getSelfSize());
#endif

    // Access the header for the received packet:
    if (pvd->getMagic() != MAGIC_VIDHDR) {
        TRACE_VA ("Error in header magic! received: 0x%08X, expected: 0x%08X\n", pvd->getMagic(), MAGIC_VIDHDR);
        return;
    }

    // client
    // get the last item in queue
    bool is_exist = false;
    /* if you want to merge the record, then open this comment:
       but you have to parse all of the array to calculate the time buffered in the buffer.*/
    if (! buf_queue.empty()) {
        if (buf_queue.back().bitrate == pvd->getBitrate ()) {
            is_exist = true;
        }
    }/**/
    if (! is_exist) {
        // push a new buffer
        video_content_t cnt;
        cnt.bitrate = pvd->getBitrate();
        cnt.size = 0;
        cnt.consumed = 0;
        buf_queue.push_back (cnt);
    } else {
        // re-calculat the total time in buffer
        double sec_received = 0.0;
        video_content_t acuinfo;
        memset (&acuinfo, 0, sizeof (acuinfo));
        for (std::deque<video_content_t>::iterator it = buf_queue.begin(); it != buf_queue.end(); it ++) {
            if (acuinfo.bitrate == 0) {
                acuinfo.bitrate = it->bitrate;
                acuinfo.consumed = it->consumed;
                acuinfo.size = it->size;
            } else if (acuinfo.bitrate == it->bitrate) {
                acuinfo.consumed += it->consumed;
                acuinfo.size += it->size;
            } else {
                sec_received += ((double)(acuinfo.size - acuinfo.consumed) * 8 / (double)acuinfo.bitrate);
                acuinfo.bitrate = it->bitrate;
                acuinfo.consumed = it->consumed;
                acuinfo.size = it->size;
            }
        }
        if (acuinfo.bitrate != 0) {
            sec_received += ((double)(acuinfo.size - acuinfo.consumed) * 8 / (double)acuinfo.bitrate);
        }
        buf_cur_size = sec_received;
    }
    assert (! buf_queue.empty());
    buf_queue.back().size += pvd->getSelfSize ();
    buf_cur_size += ((double)pvd->getSelfSize () * 8 / (double)pvd->getBitrate());
    TRACE_VA ("recv video data, bitrate=%d, size=%d, buf_cur_size=%f sec\n", pvd->getBitrate(), pvd->getSelfSize (), buf_cur_size);

    // check the requests list, remove the finished task
    assert (! last_requests.empty());
    assert (pvd->getBitrate() == last_requests.front().bitrate);
    last_requests.front().consumed += pvd->getSelfSize ();

    for (; ! last_requests.empty(); ) {
        if (last_requests.front().consumed >= last_requests.front().size) {
            TRACE_VA ("last_requests.front().consumed=%d, last_requests.front().size=%d\n", last_requests.front().consumed, last_requests.front().size);
            fflush (fp_clireqlog); fflush (fp_buflog); fflush (fp_varlog);
            assert (last_requests.front().consumed == last_requests.front().size);
            last_requests.pop_front ();
        } else {
            break;
        }
    }
#ifdef TRACEME 
    printf ("StreamingClient:process_data(%f), selfsize of data:%d, bitrate:%d, cur buf size:%6.6f, amount added:%6.6f \n", Scheduler::instance().clock(),pvd->getSelfSize(), pvd->getBitrate(), buf_cur_size,((double)pvd->getSelfSize () *8  / (double)pvd->getBitrate() ) );
//Show current size in seconds and amount of time added
    fprintf (fp_buflog, "%f VACLIBUFADD %f %f\n", Scheduler::instance().clock(), buf_cur_size, ((double)pvd->getSelfSize () * 8 / (double)pvd->getBitrate() ));
#endif

    clientUpdate ();

    //TRACE_VA ("buf_queue.size=%d, last_requests.size=%d\n", buf_queue.size(), last_requests.size());
    //TRACE_VA ("END\n");
}


/************************************************************************
*
* Function: void StreamingApplication::dumpFinalStats()
*
* Inputs:
*    char *outputFile
* Outputs:  dumps a line of results in :
*      FILE *fp = fopen("vodappCLIENTRESULTS.out", "a+");
*
*  ID of session
*  Number seconds it was active
*  startup Delay
*  Avg BW consumed
*  Avg playbackVideoRate
*  Efficiency
*  D-PSNR
*  Expected number of artifacts/stalls per hour
*  Expected number of adaptations  per hour
%  Avg playbackBuffer size
*  
*   
* Explanation:
*  This is invoked via the tcl command dumpstats.  We dump a single line of stats to
*  the file defined as follows:
***********************************************************************/
void StreamingClient::dumpFinalStats()
{
  double curr_time = Scheduler::instance().clock();
  int metric1 = 0;
  int metric2 = 0;
  int metric3 = 0;
  int metric4 = 0;
  int metric5 = 0;
  int metric6 = 0;
  int avgBufferSize = 0;
  int rebufferingRate = 0;
  double avgRebufferingTime=0.0;
  double percentageTimeRebuffering=0.0;

  int rc = 0;

#ifdef TRACEME
  printf("StreamingClient::dumpFinalStats(%f):(%d) dump to file \n", curr_time, myID);
#endif

//$A4
  if (FILE_DEFAULT != fp_statlog) {
// Metric1:  average playback rate based on N samples   
// Metric2:  PSNR : values < 10 are BAD;  values 30 and above are GOOD;  values approaching 50 are VERY GOOD (best possible is about 50)
//  Metric3:  Average number of underruns per hour 
//  Metric4:  Average number of bitrate switches per hour (>2 bad)
//  Metric5:  Channel zapping time (time it takes from time 0 until when the player first starts
//  Metric6:  R value : weighted composite metric  - based on normalized metric's 2,3,4
//int StreamingClient::calcStat (int *metric1, int *metric2, int *metric3, int *metric4, int *metric5, int *metric6, int *avgBuffer, int *rebufferRateParam, double *avgRebufferingTimeParam, double *percentageTimeRebufferingParam) {
    rc = calcStat (&metric1,&metric2,&metric3,&metric4,&metric5, &metric6, &avgBufferSize,&rebufferingRate,&avgRebufferingTime,&percentageTimeRebuffering);
    fclose (fp_statlog);
  }

  double Rxthroughput =  (totalBytesRxed*8)/(curr_time  - firstRxTime);
  double Sendthroughput =  (totalBytesSent*8)/(curr_time  - firstSendTime);
  double Efficiency = 0.0;
  if (Rxthroughput > 0)
    Efficiency = ((double)metric1)/(double)Rxthroughput;



  FILE *fp = fopen("vodappCLIENTRESULTS.out", "a+");


//  fprintf(fp,"%d %9.0f %9.0f %9.0f %9.0f %9.6f %d %3d %3d %3d %3d %3d %3d %d %3.2f\n",
//        myID,Rxthroughput,Sendthroughput, totalBytesRxed, totalBytesSent, (curr_time-firstRxTime), bufferUnderruns, metric1,metric2,metric3,metric4,metric5,metric6, avgBufferSize, Efficiency);

  fprintf(fp,"%d %9.0f %d %12.0f %3d %3.3f %3d %3d %3d %3d %3d %3.2f %3.2f %3d\n",
        myID,(curr_time-firstRxTime), metric5, Rxthroughput, metric1, Efficiency, metric2, metric3, metric4, avgBufferSize,rebufferingRate,avgRebufferingTime,percentageTimeRebuffering,metric6);
//  myID: ID of flow
//  Time the flow was active
//  Metric5:  Channel zapping time (time it takes from time 0 until when the player first starts
//  Rxthroughput: total bytes received / Time flow was active
//  Metric1:  average playback rate based on N samples   
//  Efficiency : metric1/Rxthroughput
//  Metric2:  PSNR : values < 10 are BAD;  values 30 and above are GOOD;  values approaching 50 are VERY GOOD (best possible is about 50)
//  Metric3:  Average number of underruns per hour 
//  Metric4:  Average number of bitrate switches per hour (>2 bad)
//  Avg playback buffer size (in seconds)
//  Metric6:  R value : weighted composite metric  - based on normalized metric's 2,3,4

  printf("streamingClient RESULTS: %d %9.0f %d %12.0f %3d %3.3f %3d %3d %3d %3d %3d %3.2f %3.2f %3d\n",
        myID,(curr_time-firstRxTime), metric5, Rxthroughput, metric1, Efficiency, metric2, metric3, metric4, avgBufferSize,rebufferingRate,avgRebufferingTime,percentageTimeRebuffering,metric6);

  fclose(fp);
}


/************************************************************************
*
* Function: int scalePlaybackBuffer(unsigned int eventType)
*
* Inputs:
*    unsigned int eventType :  UNDERRUN_EVENT, GREEN_REDUCTION_EVENT
*
* Outputs:
*   returns:  0 for no error, -1 on error
*
* Explanation:
*  This is called to possibly scale/alter the size of the playback buffer
*
***********************************************************************/
int StreamingClient::scalePlaybackBuffer(unsigned int eventType)
{
int rc = 0;

 if (DASHMode == 2) {
  double curr_time = Scheduler::instance().clock();

  if ((curr_time - lastBufferScale) > 5 * request_interval) {
//#ifdef TRACEME
  printf("StreamingClient::scalePlaybackBuffer(%f):(%d) CURRENT (EVENT:%d) buf_cur_size:%lf,  bufferState:%d, buf_max_video:%lf, RYThresh:%lf, YGThresh:%lf  \n", 
        curr_time, myID, eventType, buf_cur_size, bufferState, buf_max_video,bufRedYellowThreshold,bufYellowGreenThreshold);
//#endif
    switch (eventType) {

      //Double size
      case UNDERRUN_SCALE_EVENT:
        buf_max_video =  2 * buf_max_video;
        if (buf_max_video > dynamicVideoBufferMax)
          buf_max_video = dynamicVideoBufferMax;
        bufRedYellowThreshold=buf_max_video/3;
        bufYellowGreenThreshold = 2 * bufRedYellowThreshold;
        lastBufferScale=curr_time;
        break;

      //Reduce linearly by 10%
      case GREEN_SCALE_EVENT:
        buf_max_video -=  0.10 * buf_max_video;
        if (buf_max_video < 2 * request_interval)
          buf_max_video = 2 * request_interval;
 
        bufRedYellowThreshold=buf_max_video/3;
        bufYellowGreenThreshold = 2 * bufRedYellowThreshold;
        lastBufferScale=curr_time;
        break;

      //Increase by 50%
      case RED_SCALE_EVENT:
        buf_max_video +=  0.5 * buf_max_video;
        if (buf_max_video > dynamicVideoBufferMax)
          buf_max_video = dynamicVideoBufferMax;
 
        bufRedYellowThreshold=buf_max_video/3;
        bufYellowGreenThreshold = 2 * bufRedYellowThreshold;
        lastBufferScale=curr_time;
        break;


      default:
            printf("StreamingClient::scalePlaybackBuffer(%f):(%d) ERROR  bufferState:%d, bad event: %d\n", curr_time, myID,bufferState,eventType);
    }
    if (buf_cur_size < bufRedYellowThreshold)
      bufferState= RED;
    else if (buf_cur_size < bufYellowGreenThreshold)
      bufferState= YELLOW;
    else
      bufferState= GREEN;
//#ifdef TRACEME
  printf("StreamingClient::scalePlaybackBuffer(%f):(%d) UPDATED buf_cur_size:%lf,  bufferState:%d, buf_max_video:%lf, RYThresh:%lf, YGThresh:%lf  \n", 
        curr_time, myID, buf_cur_size, bufferState, buf_max_video,bufRedYellowThreshold,bufYellowGreenThreshold);
//#endif
  }
 }

  return rc;
}


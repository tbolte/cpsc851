/*
 * filename: vodapp.h
 * description: simulate the adaptive streaming over HTTP
 *
 * Author: Yunhui Fu
 * 2011/12/16
 *
 * Revisions:
 *  $A0 : 5-17-2012 - added stats and dumpFinalStats()
 */
#ifndef VOD_APP_SERVER_H
#define VOD_APP_SERVER_H

#include "vodapp.h"


class StreamingServer : public StreamingApplication {
public:
    StreamingServer (Agent*);
    virtual bool periodic_work() {
#ifdef TRACEME 
         printf ("StreamingServer:periodic_work(%f),myID:%d \n", Scheduler::instance().clock(), myID);
#endif
        if (StreamingApplication::periodic_work()) {
           respondRequests (); 
           return true;
        } 
        return false;}
    virtual int command (int argc, const char*const* argv);
    //virtual void start();       // Start running the program
    //virtual void stop();        // Stop running the program
    virtual void init ();
    virtual void process_data (int size, AppData* data);
    //virtual void recv (int size);
    virtual void dumpFinalStats();

private:
    void respondRequests ();
    size_t message_size; // $S_m$
    double send_rate;    // $b_s$
    size_t next_sequence; // the sequence number of the 'atomic content'
    double atom_length; // the time length of the 'atomic content'
};

#endif // VOD_APP_SERVER_H

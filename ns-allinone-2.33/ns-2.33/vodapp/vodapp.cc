/*
 * filename: vodapp.cc
 * description: simulate the adaptive streaming over HTTP
 *
 * Author: Yunhui Fu
 * 2011/12/16
 *
 * Revisions:
 *  $A1 : 5-15-2012 : Added support for dumpFinalStats
 *  $A12: 2/28/2013:  BUG fix- need to init last_consumed
 *  $A13 : 3/11/2013: Support DASH modes 2 and 3
 *
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <assert.h>
#include <arpa/inet.h> // htonl

#include <deque>

#include "timer-handler.h"
#include "packet.h"
#include "app.h"
#include "webcache/tcpapp.h"

#include "tclcl.h"
//#include "address.h"
//#include "ip.h"


#include "vodapp.h"

//#define TRACEME 0

#define FILE_DEFAULT stdout

// The size of the client's request packet
#define SZ_REQUEST 59

void StreamingApplicationTimer::expire (Event*)
{
    //TRACE ("StreamingApplicationTimer expire!!!\n");
#ifdef TRACEME 
//    printf ("StreamingApplicationTimer:Expire:(%f)\n", Scheduler::instance().clock());
#endif
    t_->periodic_work ();
    //TRACE ("(%s) set interval=%f\n", t_->isServer()?"server":"client", t_->getInterval());
    double myInterval = t_->getSchedualInterval();
#ifdef TRACEME 
    printf ("StreamingApplicationTimer:Expire:(%f), myID:%d  reschedule in %f seconds \n", Scheduler::instance().clock(),t_->myID, myInterval);
#endif
    resched (myInterval);
//    resched (t_->getSchedualInterval());
}

double StreamingApplication::getSchedualInterval() {
    double interval = packet_interval;
    while (interval > aavg_interval / 3) {
        interval /= 4;
    }
    return interval;
}

StreamingApplication::StreamingApplication (Agent *tcp)
    : TcpApp(tcp), data_timer_(this), running_(false)
{
    sequence = 0;
    packet_interval = 2;
    buf_cur_size = 0.0;
    last_bitrate = 0;
    last_aavg = 0;
    aavg_delta = DELTA;
    aavg_interval = SEC_AAVG;
    latest_aavg = 0;
    avg_bytes = 0;
    aavg = 0.0;
    fp_clireqlog = FILE_DEFAULT;
    fp_buflog = FILE_DEFAULT;
    fp_varlog = FILE_DEFAULT;
    fp_pkglog = FILE_DEFAULT;
    //$A1
    totalMessagesSent = 0;
    totalBytesSent = 0;
    totalBytesRxed = 0;
    firstSendTime=0.0;
    firstRxTime =  0.0;
    myID = 0;
//$A12
    last_consumed=0.0;

//$A13
   DASHMode = 1;

}

StreamingApplication::~StreamingApplication ()
{
    TRACE_VA ("BEGIN\n");
    if (FILE_DEFAULT != fp_clireqlog) {
        fclose (fp_clireqlog);
    }
    if (FILE_DEFAULT != fp_buflog) {
        fclose (fp_buflog);
    }
    if (FILE_DEFAULT != fp_varlog) {
        fclose (fp_varlog);
    }
    if (FILE_DEFAULT != fp_pkglog) {
        fclose (fp_pkglog);
    }
}

void
StreamingApplication::init ()
{
    TRACE ("BEGIN\n");
    buf_cur_size = 0.0;

    latest_aavg = 0.0;

    last_bitrate = 0;
    avg_bytes = 0;

    //aavg = (double) bitrate_levels[bitrate_levels.size() / 2];
    //aavg = (double) bitrate_levels[0];
    aavg = 500000;

    last_requests.erase (last_requests.begin(), last_requests.end());
    TRACE ("init aavg=%f\n", aavg);
}

int
StreamingApplication::command(int argc, const char*const* argv)
{
#ifdef TRACEME 
    printf ("StreamingApplication:(%f), number argc:%d, first arg:%s \n", Scheduler::instance().clock(), argc, argv[1]);
#endif
    if (argc == 2) {
        if (strcmp(argv[1], "dumpstats") == 0) {
          dumpFinalStats();
          return (TCL_OK);
        } else if(strcmp (argv[1], "start") == 0) {
            TRACE_VA ("start\n");
            start ();
            return (TCL_OK);
        } else if (strcmp(argv[1], "stop") == 0) {
            TRACE_VA ("stop\n");
            stop ();
            return (TCL_OK);
        }

    } else if(argc==3) {
        if(strcmp (argv[1],"aavg-delta") == 0) {
            aavg_delta = atof(argv[2]);
            TRACE ("aavg delta=%f\n", aavg_delta);
            return(TCL_OK);

        } else if(strcmp (argv[1],"aavg-interval") == 0) {
            aavg_interval = atof(argv[2]);
            TRACE ("aavg_interval=%f\n", aavg_interval);
            return(TCL_OK);

        } else if(strcmp (argv[1], "set-reqlog") == 0) {
            FILE *fp = fopen (argv[2], "w");
            if (NULL == fp) {
                TRACE ("Error in open file '%s'\n", argv[2]);
                return(TCL_ERROR);
            }
            if (FILE_DEFAULT != fp_clireqlog) {
                fclose (fp_clireqlog);
            }
            fp_clireqlog = fp;
            TRACE ("client req log file '%s'\n", argv[2]);
            return(TCL_OK);

        } else if(strcmp (argv[1], "set-buflog") == 0) {
            FILE *fp = fopen (argv[2], "w");
            if (NULL == fp) {
                TRACE ("Error in open file '%s'\n", argv[2]);
                return(TCL_ERROR);
            }
            if (FILE_DEFAULT != fp_buflog) {
                fclose (fp_buflog);
            }
            fp_buflog = fp;
            TRACE ("buffer log file '%s'\n", argv[2]);
            return(TCL_OK);

        } else if(strcmp (argv[1], "set-varlog") == 0) {
            FILE *fp = fopen (argv[2], "w");
            if (NULL == fp) {
                TRACE ("Error in open file '%s'\n", argv[2]);
                return(TCL_ERROR);
            }
            if (FILE_DEFAULT != fp_varlog) {
                fclose (fp_varlog);
            }
            fp_varlog = fp;
            TRACE ("inter variables log file '%s'\n", argv[2]);
            return(TCL_OK);

        } else if(strcmp (argv[1], "set-pkglog") == 0) {
            FILE *fp = fopen (argv[2], "w");
            if (NULL == fp) {
                TRACE ("Error in open file '%s'\n", argv[2]);
                return(TCL_ERROR);
            }
            if (FILE_DEFAULT != fp_pkglog) {
                fclose (fp_pkglog);
            }
            fp_pkglog = fp;
            TRACE ("package log file '%s'\n", argv[2]);
            return(TCL_OK);

        } else if (strcmp(argv[1], "set-ID") == 0) {
          myID = atoi(argv[2]);
          printf ("StreamingApplication:(%f), create stream ID:%d \n", Scheduler::instance().clock(), myID);
          return (TCL_OK);

        //} if (strcmp(argv[1], "attach-agent") == 0) {
            //Tcl& tcl = Tcl::instance();
            //agent_ = (Agent*) TclObject::lookup(argv[2]);
            //if (agent_ == 0) {
                //tcl.resultf("no such agent %s", argv[2]);
                //return(TCL_ERROR);
            //}
            //agent_->attachApp (this);
            //return(TCL_OK);

        }
    }
    // If the command hasn't been processed by StreamingApplication()::command,
    // call the command() function for the base class
    return (TcpApp::command(argc, argv));
}

void
StreamingApplication::start()
{
    TRACE_VA ("BEGIN\n");
    init ();
    running_ = true;
    TRACE_VA ("resched %f ...\n", getInterval());
    printf ("StreamingApplication:start(%f),myID:%d, data_timer interval: %lf\n", Scheduler::instance().clock(), myID,getInterval());
    data_timer_.resched (getInterval());
}

void
StreamingApplication::stop()
{
    TRACE_VA ("BEGIN\n");
    running_ = false;
    data_timer_.cancel ();
    fflush (fp_clireqlog);
    fflush (fp_buflog);
    fflush (fp_varlog);
}


bool
StreamingApplication::calculateAavg()
{
//$A7
    double this_time = Scheduler::instance().clock();
    double avg = 0.0;
    TRACE_VA ("Time: this_time=%f, latest_aavg(%f) + aavg_interval(%f) = %f !\n", this_time, latest_aavg, aavg_interval, latest_aavg + aavg_interval);
    /*if (! hasUnfinishedRequests ()) {
        latest_aavg = this_time;
        return;
    }*/
    bool flg_cal = false;
    if (last_requests.empty()) {
        if (avg_bytes > 0) {
            flg_cal = true;
        }
    } else if (this_time >= latest_aavg + aavg_interval) {
        flg_cal = true;
#ifdef DEBUG
        if (this_time >= latest_aavg + aavg_interval * 2) {
            printf ("Waring: the timer for aavg interval did not work well!!!");
        }
#endif
    }

    if (flg_cal) {
        assert (this_time > latest_aavg);

        // calculate the aavg
        // the avg in 2 seconds
        avg = (double)avg_bytes * 8 / (this_time - latest_aavg);
        //TRACE_VA ("avg=%f, avg_bytes=%d, thistime=%f, latest_aavg=%f\n", avg, avg_bytes, this_time, latest_aavg);
        if (latest_aavg <= 0) {
            aavg = avg;
            //TRACE_VA ("change aavg=%f\n", aavg);
        } else {
            aavg = aavg_delta * aavg + (1 - aavg_delta) * avg;
            //TRACE_VA ("change aavg=%f\n", aavg);
        }
        avg_bytes = 0;
        latest_aavg = this_time;
        //TRACE_VA ("latest_aavg=%f\n", latest_aavg);

        //fprintf (fp_varlog, "%f VASVRVAR %f %f %f\n", Scheduler::instance().clock(), aavg, avg, buf_cur_size);

//        fprintf (fp_varlog, "%f VACLIVAR %f %f %f\n", Scheduler::instance().clock(), aavg, avg, buf_cur_size);

    }
#ifdef TRACEME
    if (flg_cal)
      if (myID == 1)
       printf("vodapp:calculateaAvg(%lf): (myID:%d), avg:%lf,  aavg UPDATE: %lf\n",this_time,myID,avg,aavg);
#endif

    return flg_cal;
}


void
StreamingApplication::recv (int size)
{
    avg_bytes += size;
    //$A0
    totalBytesRxed += size;
    if (firstRxTime == 0)
      firstRxTime = Scheduler::instance().clock();

//#ifdef TRACEME
//    double curTime = Scheduler::instance().clock();
//    if (curTime > 4100) {
//      printf("vodapp:recv(%lf): (myID:%d),  size:%d\n", Scheduler::instance().clock(),myID,size);
//    }
//#endif

    //TRACE_VA ("size=%d, avg_bytes=%d\n", size, avg_bytes);
    calculateAavg ();
    TcpApp::recv (size);
}

/************************************************************************
*
* Function: void StreamingApplication::dumpFinalStats()
*
* Inputs:
*    char *outputFile
*   
* Explanation:
*  This is invoked via the tcl command dumpstats.  We dump a single line of stats to
*  the file defined as follows:
***********************************************************************/
void StreamingApplication::dumpFinalStats()
{
  double curr_time = Scheduler::instance().clock();
#ifdef TRACEME
  printf("StreamingApplication::dumpFinalStats(%f):(%d) \n", curr_time, myID);
#endif


  double Rxthroughput =  (totalBytesRxed*8)/(curr_time  - firstRxTime);
  double Sendthroughput =  (totalBytesSent*8)/(curr_time  - firstSendTime);

  printf("StreamingApplication RESULTS:(id:%d) RxThroughput: %9.0f, SendThroughput: %9.0f, totalBytesRxed: %9.0f, totalBytesSent: %9.0f, RxTime: %9.6f \n",myID,Rxthroughput,Sendthroughput, totalBytesRxed, totalBytesSent, (curr_time-firstRxTime));

}




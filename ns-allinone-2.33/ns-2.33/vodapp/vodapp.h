/*
 * filename: vodapp.h
 * description: simulate the adaptive streaming over HTTP
 *
 * Author: Yunhui Fu
 * 2011/12/16
 *
 * Revisions:
 *  $A0 : 5-17-2012 - added stats and dumpFinalStats()
 *  $A13 : 3-11-2012 - support Dash Modes 2 and 3 (Dash Mode 1 is default)
 */
#ifndef VOD_APP_H
#define VOD_APP_H

#include <stdio.h>
#include <deque>

//#define TRACEME 1

#if TRACEME
#define TRACE printf ("[%f][%s()]:%d: ", Scheduler::instance().clock(), __func__, __LINE__); printf
#define TRACE_VA printf ("[%f][%s()]:%d ", Scheduler::instance().clock(), __func__, __LINE__); printf
#else
#define TRACE(...)
#define TRACE_VA(...)
#endif

typedef struct _video_content_t {
//$A8
//    size_t bitrate; // the bitrate of the data
    int bitrate; // the bitrate of the data
    size_t size; // the size of video
    size_t consumed; // the consumed size of the data
} video_content_t;

class StreamingApplication;

class StreamingApplicationTimer: public TimerHandler {
public:
    StreamingApplicationTimer(StreamingApplication* t) : t_(t) {}
    virtual void expire (Event*);
private:
    StreamingApplication *t_;
};

// the packet structure of the messages
// [magic][bitrate][size][type][padding]
// header prefix: 4BBE, 0x39d1abc3
// bitrate: bps
// size: the size of this package, about 1MB each request.
// type: CBR or VBR
// padding: the random value.
#define MAGIC_VIDHDR 0x39d1abc3

// User-level packets
// AppData: common/ns-process.h
class StreamingApplicationData : public AppData {
private:
    uint32_t magic;
    uint32_t selfsize;
    uint32_t sequence;
    uint32_t bitrate;
    uint32_t reqsize;

public:
    StreamingApplicationData() : AppData(VODAPP_DATA) {}
    StreamingApplicationData(StreamingApplicationData& d) : AppData(d) {
        magic = d.magic;
        sequence = d.sequence;
        bitrate = d.bitrate;
        reqsize = d.reqsize;
        selfsize = d.selfsize;
    }
    StreamingApplicationData( uint32_t selfsize1,
                uint32_t sequence1,
                uint32_t bitrate1,
                uint32_t reqsize1)
        : AppData(VODAPP_DATA)
    {
        magic = MAGIC_VIDHDR;
        sequence = sequence1;
        bitrate = bitrate1;
        reqsize = reqsize1;
        selfsize = selfsize1;
    }

  /**
   *
   * This method will be called by ns2::TcpApp::recv() and pass the value
   * as the first parameter of ns2::TcpApp::process_data() which in turn is
   * StreamingApplication::process_data()
   *
   */
    virtual int size() const { return selfsize; }
    virtual StreamingApplicationData* copy() { return (new StreamingApplicationData(*this)); }

    uint32_t getMagic () { return magic; }
    uint32_t getSequence () { return sequence; }
    uint32_t getBitrate () { return bitrate; }
    uint32_t getReqSize () { return reqsize; }
    uint32_t getSelfSize () { return selfsize; }

};

class StreamingApplication : public TcpApp {

public:
    StreamingApplication (Agent *tcp);
    ~StreamingApplication ();

    virtual void process_data (int size, AppData* data) {}
    virtual void recv(int size);

    virtual int command (int argc, const char*const* argv);
    virtual void start();       // Start running the program
    virtual void stop();        // Stop running the program

    virtual bool periodic_work () {
        calculateAavg ();
        double curTime =  Scheduler::instance().clock();
#ifdef TRACEME 
        printf ("StreamingServer:periodic_work(%f),myID:%d, last consumed time:%f, plus interval:%f \n", Scheduler::instance().clock(), myID, last_consumed, (last_consumed+getInterval()));
#endif
        if (curTime < last_consumed + getInterval()) {
            return false;
        }
        last_consumed = curTime;
        return true;
    }

    virtual void init ();
    virtual void dumpFinalStats();


    double getInterval() { return packet_interval;}

    virtual double getSchedualInterval();

    double packet_interval; // $I_p$

    bool calculateAavg ();

    uint32_t sequence;
    uint32_t getSequence() {return sequence ++;}

    // if there's any unfinished requests.
    //bool hasUnfinishedRequests() {return last_requests.size () > 0;}

    StreamingApplicationTimer data_timer_;
    bool running_;

    double buf_cur_size; // $s_b$, the current size of buffer, seconds

    int last_bitrate; // $b(t)$, the bitrate last sent.
//$A8
//    size_t last_bitrate; // $b(t)$, the bitrate last sent.
    double last_aavg; // $a(t)$, the aavg before the client stop request

#define SEC_LOWER   1 // the delay for checking the bitrate that lower than current bitrate used
#define SEC_REQUEST 10 // the video length (in seconds) for each request.
//#define SEC_REQUEST 20 // the video length (in seconds) for each request.
#define SEC_AAVG 0.2 // the seconds to calculate the avg
#define DELTA 0.2
    double aavg_delta;     //$\delta$, the delta value to be used to calculate the value aavg
    double aavg_interval;  //the interval for calculating the value aavg

    double latest_aavg; // $\tau (t)$, the time of latest aavg calculation
    size_t avg_bytes;   // the total size received in 2 seconds
    double aavg; // ~A(i)
    std::deque<video_content_t> last_requests; // the records of last request.

    FILE * fp_clireqlog;  // the log file for client's request,       "time VACLIREQ bitrate size"
    FILE * fp_buflog;     // the log file for client/server's buffer, "time VACLIBUF totalsize changedsize"
    FILE * fp_varlog;     // the log file for client/server's internal variables, "time VACLIVAR avg bw, 2sec bw, buffer size"
    FILE * fp_pkglog;     // the log file for client/server's received packets, "time VACLIPROCPKG sequence,requestsize,bitrate,size"

    unsigned int myID;
    double totalMessagesSent;
    double totalBytesSent;
    double totalBytesRxed;
    double firstSendTime;
    double firstRxTime;

//$A13
  unsigned int DASHMode;

protected:
    double last_consumed;

};

#endif // VOD_APP_H

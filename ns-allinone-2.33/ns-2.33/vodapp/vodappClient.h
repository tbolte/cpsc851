/*
 * filename: vodapp.h
 * description: simulate the adaptive streaming over HTTP
 *
 * Author: Yunhui Fu
 * 2011/12/16
 *
 * Revisions:
 *  $A0 : 5-17-2012 - added stats and dumpFinalStats()
 */
#ifndef VOD_APP_CLIENT_H
#define VOD_APP_CLIENT_H

#include <vector>
#include "vodapp.h"

//Fill the buffer fast
#define AGGRESSIVE_DOWN 0

//Fill the buffer fast
#define AGGRESSIVE_UP 1

//In steady state and 
#define CAUTIOUS_UP 2
#define CAUTIOUS_DOWN 3

//Life is good
#define AT_CAPACITY 4

//Unknown...don't do anything
#define STABILIZE 5

//States for buffer
#define GREEN 1
#define YELLOW 2
#define RED 3

#define UNDERRUN_SCALE_EVENT 1
#define GREEN_SCALE_EVENT 2
#define RED_SCALE_EVENT 3

class StreamingClient : public StreamingApplication {
public:
    StreamingClient (Agent*);
    ~StreamingClient ();

    virtual bool periodic_work ();
    virtual void init ();

    virtual int command (int argc, const char*const* argv);
    virtual void start();       // Start running the program
    virtual void stop();        // Stop running the program
    virtual void process_data (int size, AppData* data);
    virtual void dumpFinalStats();
    virtual double getSchedualInterval();

private:
    void clientUpdate ();
    int dash3();
    void consumeVideo ();

    bool is_player_run;

    double request_interval; // $I_r$, the interval between requests
    size_t max_requests; //$R_{max}$, the maxinum number of request in the client's request queue

    int last_bitmap_index;
    double buf_max_video; //$M_{max}$, the maxinum size of video buffer, seconds
    double buf_thres_bitrate; // the threshold for switch to lower bitrate, seconds
        // If the size of buffer less than the threshold, the bitrate will be switched down.
    double buf_thres_player; //$T_{p}$, the threshold of the buffer size for playing video, seconds
        // If the size of buffer large than the threshold, the player start fetch data and play.

    double abuf; // the average size of buffer, abuf=aavg_delta * abuf + currentbuffersize
    double switching_interval_increase; // the delay time for checking the bitrate that lower than current bitrate used
    double switching_interval_reduction; // the delay time for checking the bitrate that lower than current bitrate used
    double time_lower;  // the first time to try to switch to lower bitrate. to be used to delay the switching
    double time_higher;  // the first time to try to switch to higher bitrate. to be used to delay the switching

    //$A0
    unsigned int bufferUnderruns;
    //$A16
    unsigned int rebufferingEvents;
    double totalTimeRebuffering;
    double rebufferingStartTime;
    unsigned rebufferingCounter;

    unsigned int numberOutages;
    double videoQualityMeasure;  //Ratio of final average  Relative to a HD stream 
    double videoPlayTime;
    double videoOutageTime;
    double outageQualityMeasure; //  1-videoOutageTime/videoPlayTime; 
    // perceived quality =   x * videoQualityMeasure +  (1-x) * outageQualityMeasure;
    double perceivedQuality;
 

//$A5
    double bitRateSwitchCount;
//$A7
    double dynamicAdaptTimeWait;
    double bitRateSwitchTime;
    double minRateDownTime;
    double minRateUpTime;
    double minStabilizeTime;
    int adaptState;

//$A13
//Red, yellow, green
    int bufferState;
    double bufRedYellowThreshold;
    double bufYellowGreenThreshold;
//$A13
    double dynamicVideoBufferMax; 
    double lastBufferScale;




    double last_player_read;
    double frame_error_interval;
    double frame_error_last_time;
    size_t frame_error_cnt;  // the times of frame error
    size_t frame_error_base; // the total number of frame got during the time segment
    double frame_error_rate; // average frame error rate
    double frame_error_delta;
    void cal_frame_error (bool iserror); // calculate the frame error rate

    bool is_live; // if the stream is live media. (live/vod). If it is vod, then the media will continue to be played from last paused position.
       // if it is live media, then the player start at 'current time', drop the content with time length of pausing.
    //size_t seq_num; // the sequence number of the content
    // desgin:
    // use 'piece' as the unit of atom data, for example, 1/24 second for both video and/or audio, or 1 second for audio.
    // all of the packets sent from server have sequence number of these pieces. The client can calculat the time of the content by those information.
    // the packets also include bit-rate, frame-rate(pieces/second) etc.
    // * So client should request the content by the sequence #.
    //   it can drop the content of live streaming after pausing playing, in which the atom data is detected by the sequence number.

    std::deque<video_content_t> buf_queue;
    std::vector<uint32_t> bitrate_levels;
    double ratio_threshold_reduction; // 0% ~ 100% of the (bitrate[i] - bitrate[i-1]) which current bit-rate lower than it will drop to [i-1] bitrate requests.
    double ratio_threshold_increase;  // 0% ~ 100% of the (bitrate[i+1] - bitrate[i])
    double adaptation_sensitivity;    // 0% ~ 100%

    double player_fetch_ratio_threshold; // 0 ~ 100, to calculate the run size
    FILE * fp_statlog;     // the log file for client stat info, "time VACLIPROCPKG blockbytes,maxbytes,ratio"
    int calcStat (int *metric1, int *metric2, int *metric3, int *metric4, int *metric5, int *metric6, int *avgBuffer, int *rebufferRateParam, double *avgRebufferingTimeParam, double *percentageTimeRebufferingParam);

//$A13
    int scalePlaybackBuffer(unsigned int eventType);
};


#endif // VOD_APP_CLIENT_H
